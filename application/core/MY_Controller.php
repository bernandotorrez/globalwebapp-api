<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
	  // cek status session login user jika "jika ! $_SESSION maka session di jalankan "	
       if( ! $_SESSION)
		{
    		session_start();
		}  
        
        // cek status login user jika "false maka kembali kemu nlogin"
        if ($this->session->userdata('login') == FALSE)
        {
            redirect('login');			
        }
    } 
		
		
}
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */