<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if ( ! function_exists('terbilang'))
{      
   
	
	//-----------------------------------------------------------------
/*function konversi($x){
   
  $x = abs($x);
  $angka = array ("","satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
  $temp = "";
   
  if($x < 12){
   $temp = " ".$angka[$x];
  }else if($x<20){
   $temp = konversi($x - 10)." belas";
  }else if ($x<100){
   $temp = konversi($x/10)." puluh". konversi($x%10);
  }else if($x<200){
   $temp = " seratus".konversi($x-100);
  }else if($x<1000){
   $temp = konversi($x/100)." ratus".konversi($x%100);   
  }else if($x<2000){
   $temp = " seribu".konversi($x-1000);
  }else if($x<1000000){
   $temp = konversi($x/1000)." ribu".konversi($x%1000);   
  }else if($x<1000000000){
   $temp = konversi($x/1000000)." juta".konversi($x%1000000);
  }else if($x<1000000000000){
   $temp = konversi($x/1000000000)." milyar".konversi($x%1000000000);
  }
   
  return $temp;
 }
   
 function tkoma($x){
	 $a ="" ; //tambahan
     $str = stristr($x,".");
	 $ex = explode('.',$x);
	 
	// print_r($ex)  ;
	 
		if(($ex[0]/10) >= 1){  //$ex[0]=> adalah index array.
			$a = abs($ex[0]);
		 } 
		  
	  $string = array("nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan","sembilan","sepuluh", "sebelas");
	  
	  $temp = "";	  
	  $a2 = $ex[0]/10;
	  $pjg = strlen($str);
	  $i =1;
		 
	   
	 if($a>=1 && $a< 12){   
		   $temp .= " ".$string[$a];
		  }else if($a>12 && $a < 20){   
		   $temp .= konversi($a - 10)." belas";
		  }else if ($a>20 && $a<100){   
		   $temp .= konversi($a / 10)." puluh". konversi($a % 10);
		  }else{
			if($a2<1){		 
				while ($i<$pjg){     
				 $char = substr($str,$i,1);     
				 $i++;
				 $temp .= " ".$string[$char];
				}
			 }
		   }  
		  return $temp;
	 }
	  
 function terbilang($x){
	  $poin =""; //tambahan	 	
	  
	  if( $x < 0){
	  	  $hasil = "minus ".trim(konversi($x))." koma ".trim(tkoma($x));
	  }else{
		  $poin = trim(tkoma($x));
		  $hasil = trim(konversi($x));		  
		 // print_r($hasil);		 
	  }
	     
	 if($poin){
	    $hasil = $hasil." koma ".$poin;
     }else{
	    $hasil = $hasil;
	 }
	 return $hasil;  
 } */
 
 function konversi($x){
   
  $x = abs($x);
  $angka = array ("","Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
  $temp = "";
   
   if($x < 12){
   $temp = " ".$angka[$x];
  }else if($x<20){
   $temp = konversi($x - 10)." Belas";
  }else if ($x<100){
   $temp = konversi($x/10)." Puluh".konversi(fmod($x,10));
  }else if($x<200){
   $temp = " Seratus".konversi($x-100);
  }else if($x<1000){
   $temp = konversi($x/100)." Ratus".konversi(fmod($x,100));   
  }else if($x<2000){
   $temp = " seribu".konversi($x-1000);
  }else if($x<1000000){
   $temp = konversi($x/1000)." Ribu".konversi(fmod($x,1000));   
  }else if($x<1000000000){
   $temp = konversi($x/1000000)." Juta".konversi(fmod($x,1000000));
  }else if($x<1000000000000){   	  
   $temp = konversi($x/1000000000)." Milyar".konversi(fmod($x,1000000000));  
   //echo  $temp;
   //echo  $x;
  }
   
  return $temp;
 }
   
 function tkoma($x){
	 $a ="" ; //tambahan
     $str = stristr($x,".");
	 $ex = explode('.',$x);
	 
	// print_r($ex);
	 
	 
	if(($ex[0]/10) >= 1){  //$ex[0]=> adalah index array.
	/*	$a = abs($ex[0]); //array nilai seblum koma
	}else{ */
		$a = abs($ex[1]); // array nilai sesudah koma
	}
		  
	  $string = array("nol", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan","sembilan","sepuluh", "sebelas");
	  
	  $temp = "";	  
	  $a2 = $ex[0]/10;
	  $pjg = strlen($str);
	  $i =1;
		 
	   
	 if($a>=1 && $a< 12){   	      
		   $temp .= " ".$string[$a];
		  }else if($a>12 && $a < 20){   		  
		   $temp .= konversi($a - 10)." belas";
		  }else if ($a>20 && $a<100){   		 
		   $temp .= konversi($a / 10)." puluh". konversi($a % 10);
		  }else{
			if($a2<1){				    
				while ($i<$pjg){     
				 $char = substr($str,$i,1);     
				 $i++;
				 $temp .= " ".$string[$char];
				}
			 }
		   }  
		  return $temp;		  
	 }
	  
 function terbilang($x){
	  $ex1 = explode('.',$x);
	  $poin =""; //tambahan	 
	  $nilai_array_ex = $ex1[0] ;
	  	  	  
	  if( $x < 0){
	  	  $hasil = "minus ".trim(konversi($x))." koma ".trim(tkoma($x));
	  }else{
		  $poin = trim(tkoma($x));
		  $hasil = trim(konversi($x));		  			 		  
	  }
	 
	   
	 if($poin){	
	    if($nilai_array_ex == 0 ){	
		   $hasil = "Nol"." koma ".$poin; 	
		}else{
	       $hasil = $hasil." koma ".$poin;
		}
     }else{		
	    $hasil = $hasil;
	 }
	 return $hasil; 
 }
	
	
}