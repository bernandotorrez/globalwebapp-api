<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if ( ! function_exists('http_request_get')) {      
   
    function http_request_get($url, $data = null) {
        $ci = &get_instance();

        //load the session library
        $ci->load->library('session');

        $token = $ci->session->userdata('api_token'); //this line throws errow
        $authorization = "Authorization: Bearer $token";

        // create curl resource
        $ch = curl_init();

        if($data == null) {
            $api_url = $url;
        } else {
            $api_url = $url.'?'.http_build_query($data);
        }
        
        // set url
        curl_setopt($ch, CURLOPT_URL, $api_url);

        //Set your auth headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            $authorization
        ));

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);     

        // Check apakah API nya ada
        $check = json_decode($output, true);
        if($check['httpStatus'] == '401') {
            echo $check['message'];
            echo '<br>';
            echo '<a href='.base_url('/login/logout').'>Click Here to Exit</a>';
            die();
        } else {
            return $output;
        }

        
    }

}

if ( ! function_exists('http_request_post')) {      
   
    function http_request_post($url, $data) {
        // set post fields

        $ci = &get_instance();

        //load the session library
        $ci->load->library('session');

        $token = $ci->session->userdata('api_token'); //this line throws errow
        $authorization = "Authorization: Bearer $token";

        $ch = curl_init();
        
        $url = $url;
        curl_setopt($ch,CURLOPT_URL,$url);
        //Set your auth headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            $authorization
        ));
        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        $response = curl_exec($ch);
        curl_close ($ch);

        // Check apakah API nya ada
        $check = json_decode($response, true);
        if($check['httpStatus'] == '401') {
            echo $check['message'];
            echo '<br>';
            echo '<a href='.base_url('/login/logout').'>Click Here to Exit</a>';
            die();
        } else {
            return $response;
        }
    }

}

if ( ! function_exists('http_request_put')) {      
   // untu update data
    function http_request_put($url, $data) {
        // set post fields

        $ci = &get_instance();

        //load the session library
        $ci->load->library('session');

        $token = $ci->session->userdata('api_token'); //this line throws errow
        $authorization = "Authorization: Bearer $token";

        $ch = curl_init();
        
        $url = $url;
        curl_setopt($ch,CURLOPT_URL,$url);
         //Set your auth headers
         curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            $authorization
        ));

        curl_setopt($ch,CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        $response = curl_exec($ch);
        curl_close ($ch);

        // Check apakah API nya ada
        $check = json_decode($response, true);
        if($check['httpStatus'] == '401') {
            echo $check['message'];
            echo '<br>';
            echo '<a href='.base_url('/login/logout').'>Click Here to Exit</a>';
            die();
        } else {
            return $response;
        }
    }

}