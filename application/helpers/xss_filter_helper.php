<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
// Added by Bernand

if ( ! function_exists('xss_filter')) {      
    function xss_filter($word) {
        $clean_word = trim($word);
        $clean_word = str_replace("'", "", $word);
		$clean_word = str_replace('"', "", $clean_word);
        $clean_word = str_replace(array('<script>', '</script>'), '', $clean_word);
        $clean_word = trim($clean_word);
        
        return $clean_word;
    }
}

if ( ! function_exists('checkNULL')) {      
    function checkNULL($value) {
        if(!$value || empty($value) || $value == '' || $value == null) {
            return '-';
        } else {
            return $value;
        }
    }
}