<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_currency extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_currency');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_currency->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}
	 	}
		
	public function index()
		{

		$data['currency']=$this->M_currency->get_all_currency();
		$data['total']=$this->M_currency->get_count_id();
		$data['show_view'] = 'currency_form/V_currency';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function ajax_list()
	{
		$list = $this->M_currency->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_curr) {
			$chk_idmaster ='<div align="center"><input id="checkcurr" name="checkcurr" type="checkbox" value='.$tbl_curr->id_curr.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_curr->id_curr;
			$row[] = $tbl_curr->currency;
			$row[] = $tbl_curr->date_create;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_currency->count_all(),
						"recordsFiltered" => $this->M_currency->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}	
		
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_curr');
		$this->db->from('tbl_curr');
		$this->db->where('status','1');
		$total = $this->db->count_all_results();
		//$total=$this->db->count_all_results("qv_vendor");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_curr",$search);
		$this->db->where('status','1');
		$this->db->or_like("currency",$search);
		$this->db->where('status','1');
		$this->db->or_like("description",$search);
		$this->db->where('status','1');
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		$this->db->where('status','1');
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_curr','ASC');
		$query=$this->db->get('tbl_curr');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("id_curr",$search);
		$jum=$this->db->get('tbl_curr');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $tbl_curr) {
		$chk_idmaster ='<div align="center"><input id="checkvendor" name="checkvendor" type="checkbox" value='.$tbl_curr["id_curr"].' class="editRow ace" req_id_del='.$tbl_curr["id_curr"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($tbl_curr['id_curr'],$tbl_curr['currency'],
									$tbl_curr['description'],
									$chk_idmaster
									);

		}

		echo json_encode($output);


	}
		
	public function add_currency()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_add = date('Y-m-d H:i:s');

		if(empty($_POST["currency"])){
			die ("Field Currency must be filled in!! ");
		}
		else{
			$data = array(
					'id_curr' => $this->input->post('id_curr'),
					'currency' => $this->input->post('currency'),
					'date_create' => $date_add,
					'remarks' => $remarks_add
				);
			$insert = $this->M_currency->add_currency($data);
		  }
		  echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_currency->get_by_id($id);
			echo json_encode($data);
		}

	public function update_currency()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_update = date('Y-m-d H:i:s');

		if(empty($_POST["currency"])){
			die ("Field Currency must be filled in!! ");
		}
		else{
		$data = array(
					'currency' => $this->input->post('currency'),
					'date_update' => $date_update,
					'remarks' => $remarks_update
			);
		$this->M_currency->update_currency(array('id_curr' => $this->input->post('id_curr')), $data);
		  }
		  echo 'Insert';
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_delete = 'Action : Delete | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_delete = date('Y-m-d H:i:s');

		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
						'date_update' => $date_delete,
						'remarks' => $remarks_delete
				);	
				$this->db->where('id_curr', $id);
				$this->db->update('tbl_curr', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}



}
