<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<style>
 .outer {
    width: 210px;
    color: black;
    border: 2px solid red;
    padding: 5px;
 }
</style> 
									<div>
									<button class="btn bg-purple btn-flat" onclick="#"><i class="fa fa-wechat"></i> Chat</button>
									<button onclick="disconnect()" id="deleteTriger" name="deleteTriger" class="btn bg-maroon btn-flat" disabled><i class="fa fa-trash"></i> Disconnect</button>
									</div>
                                    <br/>
									<div class="row">
									<div class="col-xs-12" style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">                               
	                                <table  class="table table-striped table-bordered table-hover" id="myTable" >
	                                    <thead class="text-warning">
											<th width="10%" style="text-align:center">
												<label class="pos-rel">
                                                    <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
                                                    <span class="lbl"></span>
                                                </label>
                                            </th>
											<th>No</th>
	                                        <th>Id</th>
											<th>Name</th>
											<th>Username</th>
											<th>Dept</th>
											<th>Status</th>
											<th>Last Login</th>
											<th>IP</th>
											<th>Modul</th>
	                                    </thead>
                                     </table> 
      </div>                               
</div>      
  
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-80p">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">×</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    User Form
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body" style="padding-top:10px">       
			 <form action="#" id="form" class="form-horizontal">

        <div class="form-group form-group-sm">
            <!-- left column -->
            <div class="col-sm-6">
			
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">ID</label>
					<div class="col-sm-8">
					<?php
					$totaldata = $total[0]->totaldata+1;
					?>
					<input name="id" placeholder="Id" class="form-control" type="text" readonly value="<?php echo $totaldata ?>">
					</div>
				</div>
				
                <div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Name User</label>
				  <div class="col-sm-8">
					<input name="name" placeholder="BRIAN" class="form-control" type="text">
				  </div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Username</label>
				  <div class="col-sm-8">
					<input name="username" id="username" placeholder="brian123" class="form-control" type="text">
				  </div>
				</div>

				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Phone</label>
				  <div class="col-sm-8">
					<input name="phone" placeholder="08119955599" class="form-control" type="text">
				  </div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Email User</label>
				  <div class="col-sm-8">
					<input name="email" placeholder="brian.yunanda@eurokars.co.id" class="form-control" type="text">
				  </div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Head</label>
				  <div class="col-sm-8">
					<input name="sign_head" placeholder="Ahmad Zulfikar" class="form-control" type="text">
				  </div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Email Head</label>
				  <div class="col-sm-8">
					<input name="email_head" placeholder="ahmad.zulfikar@eurokars.co.id" class="form-control" type="text">
				  </div>
				</div>
				
            </div>
		
            <!-- right column -->
            <div class="col-sm-6">
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Email CC</label>
				  <div class="col-sm-8">
					<input name="email_cc" placeholder="itteam@eurokars.co.id" class="form-control" type="text">
				  </div>
				</div>
				
                <div class="form-group">
					<label class="col-sm-3 control-label" style="padding-right:10px">User Group</label>
					<div class="col-sm-8">
                        <select class="form-control" name="id_group" id="id_group">
						<option  value="">Choose Group</option> 
						<?php foreach($group as $row) { ?>
						<option value="<?php echo $row->id_group;?>"><?php echo $row->name_grp;?>
						</option>
						<?php } ?>				
						</select>    
                    </div>
                </div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Company</label>
					<div class="col-sm-8">
						<select class="form-control" name="id_company" id="id_company">
						<?php foreach($company as $row) { ?>
						<option value="<?php echo $row->id_company;?>"><?php echo $row->company;?>
						</option>
						<?php } ?>				
						</select>    
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Branch</label>
					<div class="col-sm-8">
					   <select class="form-control" name="branch_id" id="branch_id">
						<option  value="">Choose Branch</option>
						<?php foreach($company2 as $bbb) { 
						$sqlpo	  = "select * from tbl_branches where id_company='".$bbb->id_company."'";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo 	  = $qrypo->result();					
						foreach($adapo as $aaa) { ?>
						<option value="<?php echo $aaa->branch_id;?>"><?php echo $aaa->name_branch;?>
						</option>
						<?php } }?>	
						</select>    
					</div>
				</div>
			
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Department</label>
					<div class="col-sm-8">
					   <select class="form-control" name="id_dept" id="id_dept">
						<option  value="">Choose Department</option> 
						<?php foreach($department as $row) { ?>
						<option value="<?php echo $row->id_dept;?>"><?php echo $row->dept;?>
						</option>
						<?php } ?>				
						</select>    
					</div>
				</div>
			
                <div class="form-group">
                    <label class="col-sm-3 control-label" style="padding-right:10px">Status App</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="aproval_flag" id="aproval_flag">
							<option  value="99">Choose Status</option>
                            <option value="0">User</option>
                            <option value="1">B.O.D</option>
							<option value="2">F.C</option>
							<option value="3">Head</option>
							<option value="4">Purchasing</option>
                        </select>
                    </div>
                </div>

				 <div class="form-group">
                    <label class="col-sm-3 control-label" style="padding-right:10px">Status Level</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="give_righ_flag" id="give_righ_flag">
							<option  value="99">Choose Level</option>
                            <option value="1">User</option>
                            <option value="2">Manager</option>
							<option value="3">Administrator</option>
                        </select>
                    </div>
                </div>
				
                </div>
		
				<div class="col-sm-10">
				<div class="form-group">
				<label class="col-sm-2 control-label" style="padding-right:10px">Status Purchase</label>
					<div class="col-sm-3 outer" id="checkboxlist">
						<input type="checkbox" id="purchase" name="sample" class="selectall"> All
						<?php 
						$arrSelVedTask = array();
						foreach ($purchase as $row) { 
						if(isset($arrSelVedTask[$row->id_flagpur])) {
                      ?>  <input type="checkbox" id="purchase" name="purchase[]" value="<?php echo $row->id_flagpur ;?>" >  <?php echo $row->type_purchase; ?><?php
                  }
                  else{
                      ?>  <input type="checkbox" id="purchase" name="purchase[]" value="<?php echo $row->id_flagpur ;?>" >  <?php echo $row->type_purchase; ?><?php
                  }
               }  
                  ?> 
					</div>
				</div>
				</div>
				</fieldset>
                <!-- End contact information -->
            </div>
        </div>
      <!-- Modal Footer -->
            <div class="modal-footer">
			<button type="button" class="btn btn-warning" onclick="reset_pass()"><i></i>Reset Password</button>
            <button type="button" id="btnSave" name="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div><!-- End Modal Footer -->
    </form>
		</div> <!-- End modal body div -->
      </div> <!-- End modal content div -->
  </div> <!-- End modal dialog div -->
</div> <!-- End modal div -->
<script>
$('.selectall').click(function() {
    if ($(this).is(':checked')) {
        $('div input').attr('checked', true);
    } else {
        $('div input').attr('checked', false);
    }
});
</script>
<script>


//$('#myTable').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
var tableUsers = $('#myTable').DataTable({
        "lengthMenu": [[-1], ["All"]],
		"dom": 'Brti',
		"buttons": [
            {
		   "extend":    'copy',
		   "text":      '<i class="fa fa-files-o"></i> Copy',
		   "titleAttr": 'Copy'
		   },
		   {
		   "extend":    'print',
		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
		   "titleAttr": 'Print',
		   "orientation": 'landscape',
           "pageSize": 'LEGAL'
		   },
		   {
		   "extend":    'excel',
		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
		   "titleAttr": 'Excel'
		   },
		   {
		   "extend":    'pdf',
		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
		   "titleAttr": 'PDF',
		   "orientation": 'landscape',
           "pageSize": 'LEGAL'
		   }
        ],
		"autoWidth" : false,
		"scrollY" : '260',
		"scrollX" : true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('monitoring_form/C_monitoring/ajax_list') ?>",
            "type": "POST"
        },
		'columnDefs': [
		{
        'targets': [0],
		'orderable': false,
		}
   ],

      'order': [[2, 'desc']],
        });

//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>


<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>

<script>
        	$(document).ready(function(){
	            $("#id_company").change(function (){
	                var url = "<?php echo site_url('monitoring_form/C_monitoring/pos_branch');?>/"+$(this).val();
	                $('#branch_id').load(url);
	                return false;
	            })
	        });
    	</script>

	<script type="text/javascript">
	var save_method; //for save method string
    var table;
	
	function disconnect()
	{
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		$.ajax({
        url : "<?php echo site_url('monitoring_form/C_monitoring/ajax_disconnect/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {
		alert('Disconnect User Berhasil!!!');
		location.reload();
		},
		
	});
}
	
	function add_monitoring()
    {
      save_method = 'add';
      $('#modal_form').modal('show');; // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
	
	
	function reset_pass()
	{
		var url;
		if(save_method == 'update')
		{
          url = "<?php echo site_url('monitoring_form/C_monitoring/reset_pass')?>";
		}
		else
		{
		  alert("Just One Allowed Data!!!");
		}
		  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
		
	}
	
	function edit_monitoring()
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('monitoring_form/C_monitoring/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {
			var aaa = data.status_pur.split(",");
			$.each(aaa, function(i, item) {
			if (item == "1") {
			$("#purchase[value='1']" ).prop("checked", true);
			} 
			if (item == "2") {
			$("#purchase[value='2']").prop("checked", true);
			} 
			if (item == "3") {
			$("#purchase[value='3']").prop("checked", true);
			} 
			if (item == "4") {
			$("#purchase[value='4']").prop("checked", true);
			} 
			if (item == "5") {
			$("#purchase[value='5']").prop("checked", true);
			}
			
			});
            $('[name="id"]').val(data.id);
            $('[name="name"]').val(data.name);
			$('[name="username"]').val(data.username);
			$('[name="phone"]').val(data.phone);
			$('[name="email"]').val(data.email);
			$('[name="sign_head"]').val(data.sign_head);
			$('[name="email_head"]').val(data.email_head);
			$('[name="email_cc"]').val(data.email_cc)
			$('[name="id_group"]').val(data.id_group);
			$('[name="id_company"]').val(data.id_company);
			$('[name="branch_id"]').val(data.branch_id);
			$('[name="id_dept"]').val(data.id_dept);
			$('[name="aproval_flag"]').val(data.aproval_flag);
			$('[name="give_righ_flag"]').val(data.give_righ_flag);
			
            $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

     function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('monitoring_form/C_monitoring/add_monitoring')?>";
      }
      else
      {
		   url = "<?php echo site_url('monitoring_form/C_monitoring/update_monitoring')?>";
      }
		  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }

     function delete_monitoring()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('monitoring_form/C_monitoring/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
			
		

      
    }
	
  </script>

  

</html>
