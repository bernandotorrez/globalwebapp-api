<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_table_aproval_pr extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_table_aproval_pr','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();	
		$this->load->helper('api_helper');						
	}		  	
		
  public function index()
  {					    	
  		 
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	// $tampil_table_aproval= $this->M_table_aproval_pr->tampil_add_pp()->result();	
	// $total_rows =$this->M_table_aproval_pr->tampil_add_pp()->num_rows();	  										  	
	 $dept  = $this->session->userdata('dept') ;	
	 $branch  = $this->session->userdata('name_branch') ; 
	 $company = $this->session->userdata('short') ;		
	 $data['intno'] = ""; //variable buat looping no table.				
	 $data['header'] ="Approval Head P.R"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	 //$data['ceck_row'] = $total_rows;	
	// $data['tampil_company']=$this->M_table_aproval_pr->get_company()->result();																															
	// $data['tampil_dept']=$this->M_table_aproval_pr->get_dept()->result();																															
	 $data['show_view'] = 'approval_pr_form/V_table_aproval_pr';		
	 $this->load->view('dashboard/Template',$data);					
				 
  }
  
  public function get_idtrans_modal()
  {
         // $tampungmaster = $this->M_create_pp->m_mastergetid_pp();
	     //$tampungdetail = $this->M_create_pp->m_getid_pp();
	 
	     $status_aktif_record = "1" ; // 1 = masih aktif , 0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //       
	   
	     //$tampungmaster = $this->db->query("select id_master,remarks,gran_total,currency,ppn,pph,gran_totalppn from qv_head_pp_complite where id_master ='".$nopp."'");  
		 //$tampungdetail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		
		 // Call API PRForm/getPP and getDetailPP
		 $parameter = array('id_master' => $nopp);

		 $tampungmaster = http_request_get(API_URL.'/PRForm/getPP', $parameter);
		 $tampungmaster = json_decode($tampungmaster);

		 $tampungdetail = http_request_get(API_URL.'/PRForm/getDetailPP', $parameter);
		 $tampungdetail = json_decode($tampungdetail);
	    
		 $no="1";
         echo "<p>";
		
		 // echo "<div style='overflow-x:auto;'>" ;
		 echo "<div class='table-responsive'>" ;
		 echo "<table class='table table-striped table-bordered table-hover' width='70%'>";
		 echo  "<tr style='font-weight:bold; font-size:11px' class='btn-info'>";
		 echo  "<td width='1%' align='center'>No</td>";
		 echo  "<td width='20%'  align='center'>Item Type</td>";
		 echo  "<td width='20%'  align='center' >Description</td>";
		 // echo  "<td width='10%'>P.O Reff</td>";
		 // echo  "<td width='10%'>C.O.A</td>";
		 // echo  "<td width='10%'>No Actifity</td>";
		 echo  "<td  align='center' >Qty</td>";
		 echo  "<td width='15%' align='center'>Prices</td>";
		 echo  "<td width='15%' align='center' >Total Prices</td>";
		/* echo  "<td width='2%'>PPN Type</td>";
		 echo  "<td width='2%'>PPH Type</td>";
		 echo  "<td width='15%'>PPN Amount</td>";
		 echo  "<td width='20%'>PPH Amount</td>"; */
		 echo  "</tr> ";
			foreach ($tampungdetail->data as $row_jq) {
				echo  "<tr style='font-size:12px'> ";
				echo '<td align="center">'.$no++.'</td>';
				echo '<td>'.$row_jq->desc.'</td>';
				echo '<td>'.$row_jq->spec.'</td>';
				//echo '<td>'.$row_jq->po_reff.'</td>';
				//echo '<td>'.$row_jq->coa.'</td>';
				//echo '<td>'.$row_jq->no_act.'</td>';
				echo '<td align="center" style="text-align: center;">'.$row_jq->qty.'</td>';
				echo '<td align="right" style="text-align: right;">'.number_format($row_jq->harga,2,'.',',').'</td>';
				echo '<td align="right" style="text-align: right;">'.number_format($row_jq->total,2,'.',',').'</td>';
				/*echo '<td>'.$row_jq->tax_type.'</td>';
				echo '<td>'.$row_jq->tax_typepph.'</td>';
				echo '<td>'.number_format($row_jq->tax_detail,2,'.',',').'</td>';
				echo '<td>'.number_format($row_jq->tax_detailpph,2,'.',',').'</td>';
				echo "</tr>"; */
	
			}
	
		 foreach ($tampungmaster->data as $row_jm) {
			 echo  "<tr style='font-weight:bold' class='btn-success'>";
			 echo  "<td width='27%' colspan='2'>Remarks :</td>";
			 echo  "<td width='40%' colspan='6'>".$row_jm->remarks."</td>";
			 echo  "</tr> ";
			 echo  "<tr style='font-weight:bold' class ='btn-danger' >";
			 echo  "<td width='25%' colspan='5' align='right'>Total (".$row_jm->currency.") : </td>";
			 echo  "<td width='40%' align='right' style='text-align: right;'>".number_format($row_jm->gran_total,2,'.',',')."</td>";
			 echo  "</tr> ";
			/* echo  "<tr style='font-weight:bold' class ='btn-light'>";
			 echo  "<td width='25%' colspan='9' align='right'>PPN Amount :</td>";
			 echo  "<td width='40%'>".number_format($row_jm->ppn,2,'.',',')."</td>";
			 echo  "</tr> ";
			 echo  "<tr style='font-weight:bold' class ='btn-light'>";
			 echo  "<td width='25%' colspan='9' align='right'>PPH Amount :</td>";
			 echo  "<td width='40%'>".number_format($row_jm->pph,2,'.',',')."</td>";
			 echo  "</tr> ";
			 echo  "<tr style='font-weight:bold' class ='btn-danger'>";
			 echo  "<td width='25%' colspan='9' align='right'>Total + Tax :</td>";
			 echo  "<td width='40%'>".number_format($row_jm->gran_totalppn,2,'.',',')."</td>";
			 echo  "</tr> ";*/
			 echo "</table>";
			 echo "</div>";
		  }
}
  
  
  
  
  
  public function do_aproval()	
  {
		if ($this->M_table_aproval_pr->give_aproval())
		{	 	
			$this->session->set_flashdata('pesan_aproval','1');	
		    $this->kirim_email_aprove(); 				
		    redirect('approval_pr_form/c_table_aproval_pr'); //riderct ke menu utama  			
			//print_r($this->session->userdata("result_id_master"));	
			
		}else{
			$this->session->set_flashdata('pesan_aproval','0');		
			redirect('approval_pr_form/c_table_aproval_pr');   
		}
  }
  
  public function do_rejected()	
  {
		if ($this->M_table_aproval_pr->give_rejected())
		{	 			    
			$this->session->set_flashdata('pesan_reject','1');	
			$this->kirim_email_reject();		
			redirect('approval_pr_form/c_table_aproval_pr');   
		}else{			
			$this->session->set_flashdata('pesan_reject','0');		
			redirect('approval_pr_form/c_table_aproval_pr');     
		}
  }
  
  
//    public function do_search_pp()	
//   {		
//   		 $tampil_table_aproval= $this->M_table_aproval_pr->get_search()->result();	
// 	     $total_rows =$this->M_table_aproval_pr->tampil_add_pp()->num_rows();
  	
// 		if ($tampil_table_aproval)
// 		{	 	
// 			$dept  = $this->session->userdata('dept') ;	
// 			$branch  = $this->session->userdata('name_branch') ; 
// 			$company = $this->session->userdata('short') ;					
// 			$data['intno'] = ""; //variable buat looping no table.	
// 			$data['header'] ="Approval Form"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				
// 			$data['ceck_row'] = $total_rows;	
// 			$data['pp_view'] =  $tampil_table_aproval;
// 			$data['tampil_company']=$this->M_table_aproval_pr->get_company()->result();
// 			$data['tampil_dept']=$this->M_table_aproval_pr->get_dept()->result();																																
// 			$data['show_view'] = 'approval_pr_form/V_table_aproval_pr';		
// 			$this->load->view('dashboard/Template',$data);					
// 	  }else{	
// 			$dept  = $this->session->userdata('dept') ;	
// 			$branch  = $this->session->userdata('name_branch') ; 
// 			$company = $this->session->userdata('short') ;	
// 			$data['intno'] = ""; //variable buat looping no table.					
// 			$data['header'] ="Aproval Form"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
// 			$data['ceck_row'] = $total_rows;	
// 			$data['pp_view'] =  $tampil_table_aproval;
// 			$data['tampil_company']=$this->M_table_aproval_pr->get_company()->result();	
// 			$data['tampil_dept']=$this->M_table_aproval_pr->get_dept()->result();																															
// 			$data['pesan'] = 'Data PP aproval table is empty';				
// 			$data['show_view'] = 'approval_pr_form/V_table_aproval_pr';
// 			$this->load->view('dashboard/Template',$data);					
// 	  } 
		
//   }
  
   
  public function multiple_submit() //fungsi submit dengan banyak button dalam satu form
  {  
	
		if ($this->input->post('btnaprove')){	
			$this->do_aproval();
		}else{
			if ($this->input->post('btnreject')){	//dari modal popup
				$this->do_rejected();							
			}else{
				if ($this->input->post('btncari')){	
					$this->do_search_pp();
				}else{		
					redirect('approval_pr_form/c_table_aproval_pr');
				}
			}
		}
	

	}

//------------------------------------pashing result to modal popup-------------------
	// public function get_nopp_modal() {    
	//      $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	//      $nopp = $this->input->post('idmaster',TRUE); //       
	//      $query_master = $this->db->query("select id_master,remarks,gran_total,currency,ppn,pph,gran_totalppn from qv_head_pp_complite where id_master ='".$nopp."'");  
    //      $query_detail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		   		  
	// 	   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   
		   
	// 	      $data['str_pp_master']=$query_master->result();
	// 		  $data['str_pp_detail']=$query_detail->result();
	// 		  $data['intno']="";			  			  			  
	// 		  $data['phasing_nopp'] = $nopp ;
	// 		  $tampil_detail = $this->load->view('approval_pr_form/V_conten_detail',$data,true);
			 
	// 		  echo $tampil_detail ;			 
	// 	   }							   
    // }      	
						

public function kirim_email_reject(){          
 		   
		   $this->load->library('email');				 								   		   		  	        
		   //konfigurasi email		
		  /* $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;		
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.gmail.com';
		   
		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';		  		  
		   $config['smtp_port']  = '5220';	
		   //$config['smtp_port']  = '465'; portgmail
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   
		   //$config['smtp_user']= "epurchaseeuro@gmail.com"; //Gmail
		   //$config['smtp_pass']= "#purchas3"; 
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5"; */
		   //--------------------------------	
		   
		   $config['charset']    = 'utf-8';
           $config['newline']    = "\r\n";
           $config['mailtype'] = 'html'; 
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!"; 
           $config['validation'] = TRUE;      
           $this->email->initialize($config);   		  
		    		   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			$str_status = '1' ; //aktif ;
						
			if ($strponumber != '') :						  	
				//Select untuk email master PP----------------------------
				//$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,ppn,pph,gran_total,gran_totalppn,remarks,status_send_aprove,aprove_bod,reason_reject,email_pp,email_head_pp,email_cc_pp from qv_head_pp_complite where id_master ='".$strponumber."' and status ='".$str_status."'");	
				$parameter = array('id_master' => $strponumber);
				$query = http_request_get(API_URL.'/PRForm/getPP', $parameter);
				$query = json_decode($query);			
			  if ($query->length > 0 ) :
			      $data_email['str_pp_master'] = $query->data;		
				  foreach ($query->data as $row_ceck) :	
				     //simpan row alamat email kedalam variable..
					 	 $struseremail = $row_ceck->email_pp ; //addrees for to email
					     $strheademail = $row_ceck->email_head_pp; //addrees for to email
						 $strccemail = $row_ceck->email_cc_pp ; //addrees for to email
					 //end-----------------------------------------	
					 		   
				     //Select untuk email deteail PP--------------------------------
					 //$query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."' and status ='".$str_status."'");		
					 $query_detail = http_request_get(API_URL.'/PRForm/getDetailPP', $parameter);
					 $query_detail = json_decode($query_detail);
				     if ($query_detail->length > 0 ) :										 
					    $data_email['str_pp_detail'] = $query_detail->data;	
					    $data_email['intno'] = ""; //untuk counter angka table																									
					    $message = $this->load->view('approval_pr_form/V_content_email_aproval',$data_email,true);														
					  endif;
					  //end-----------------------------------------------------------					 
																																											
							$subject = "PR_REJECTED -- Eurokarsgroup Indonesia";		
							$result = $this->email ;							   
							$this->email->initialize($config);  
							$this->email->set_newline("\r\n"); 
						   
							
							$sender_email = "helpdesk@eurokars.co.id"; 					   
							$sender_name = "Purchase Request Notification";					   					    
							
							$to = $struseremail.",".$strheademail.",".$strccemail; //email address,user,head, and CC
							 
							$this->email->from($sender_email, $sender_name);
							$this->email->to($to);
							$this->email->subject($subject);  
							
							//$this->email->message(print_r($message, true));			   
						  
							$this->email->message($message);// tanpa array	
																	 
							if ($this->email->send()) :		  	
								$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
								//redirect(current_url());
							else:
								show_error($this->email->print_debugger());
							endif; 										   						   
					   
					  endforeach;							    
				 endif;
			 endif;				 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
			 
	      
 }
 
 public function kirim_email_aprove(){          
 		   
		   $this->load->library('email');				 								   		   		  	        
		   //konfigurasi email		
		  /* $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;		
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.gmail.com';
		   
		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';		  		  
		    $config['smtp_port']  = '5220';	
		   //$config['smtp_port']  = '465'; port gmail
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   
		   //$config['smtp_user']= "epurchaseeuro@gmail.com"; //Gmail
		   //$config['smtp_pass']= "#purchas3"; 
		    $config['smtp_user']= "helpdesk@eurokars.co.id";
		    $config['smtp_pass']= "#ur0kar5";  */
			
		   //--------------------------------		   		  
		   $config['charset']    = 'utf-8';
           $config['newline']    = "\r\n";
           $config['mailtype'] = 'html'; 
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!"; 
           $config['validation'] = TRUE;      
           $this->email->initialize($config);
					   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			$str_status = '1' ; //aktif ;
			
			if ($strponumber != '') :						  	
				//Select untuk email master PP----------------------------
				//$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,ppn,ppn,gran_total,gran_totalppn,remarks,status_send_aprove,aprove_bod,reason_reject,email_pp,email_head_pp,email_cc_pp, email_purchase_pp from qv_head_pp_complite where id_master ='".$strponumber."' and status ='".$str_status."'");
				$parameter = array('id_master' => $strponumber);
				$query = http_request_get(API_URL.'/PRForm/getPP', $parameter);
				$query = json_decode($query);					
			  if ($query->length > 0 ) :
			      $data_email['str_pp_master'] = $query->data;		
				  foreach ($query->data as $row_ceck) :	
				  			   				     	
					 //simpan row alamat email kedalam variable..
					 	$struseremail = $row_ceck->email_pp ;
						$strheademail = $row_ceck->email_head_pp;
						$strccemail = $row_ceck->email_cc_pp ;
						$email_purchase = $row_ceck->email_purchase_pp;
					 //end-----------------------------------------
					
				     //Select untuk email detail PP--------------------------------
					 //$query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."' and status ='".$str_status."'");		
					 $query_detail = http_request_get(API_URL.'/PRForm/getDetailPP', $parameter);
					 $query_detail = json_decode($query_detail);
				     if ($query_detail->length > 0 ) :					 																					
					    $data_email['str_pp_detail'] = $query_detail->data;	
					    $data_email['intno'] = ""; //untuk counter angka table																									
					    $message = $this->load->view('approval_pr_form/V_content_email_aproval',$data_email,true);														
					  endif;				
					  //end---------------------------------------------------------
					  		
																 																									
									$subject = "PR Head APPROVED -- Eurokarsgroup Indonesia";	
									$result = $this->email ;							   
									$this->email->initialize($config);  
									$this->email->set_newline("\r\n"); 
								   					
									
									$sender_email = "helpdesk@eurokars.co.id"; 					   
									$sender_name = "Purchase_Request_Notification";					   					    
									
									$to = $struseremail.",".$strheademail.",".$strccemail.','.$email_purchase; //email address,user,head, and CC
									 
									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);  
									
									//$this->email->message(print_r($message, true));			   
								  
									$this->email->message($message);// tanpa array	
																			 
									if ($this->email->send()) :		  	
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
										//redirect(current_url());
									//else:
									//	show_error($this->email->print_debugger());
									endif;			    
					      endforeach;							    
			         endif;
			     endif;				 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
			 
	      
 }
 

function cath_data_fordatatables(){
	
	     $str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
		 $str_idcompany = $this->session->userdata('id_company'); 
		 $str_iddept = $this->session->userdata('id_dept'); 
		 
		 $start_date = $_POST['start_date'];
		 $end_date = $_POST['end_date'];
		 $status_approve = $_POST['status_approve'];
		
		 $str_status_send = "1";
		 $str_status_approved = "1";
		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		@$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		@$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		@$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		@$search=$_REQUEST['search']["value"];

        //order short column
		$column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top");
		
		/*Menghitung total desa didalam database*/

		
		if  ($str_flag_approval == "3" or $str_flag_approval == "0" ){	
			$param_count = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => 'date_send_pr', 'dir' => 'desc',  'is_date_search' => $_POST["is_date_search"], 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'approve_head' => $_POST["status_approve"]);
			$param_count = http_request_get(API_URL.'/approvalPR/countAllResult', $param_count);
			$param_count = json_decode($param_count, true);
			$total = $param_count['length'];
			}
	
		$output=array();

	
		$output['draw']=$draw;

	
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		$output['data']=array();

		if($search!=""){
			    
			
		}

		if($search!=""){
			$param_search = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => 'date_send_pr', 'dir' => 'desc',  'is_date_search' => $_POST["is_date_search"], 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'approve_head' => $_POST["status_approve"]);
			$param_search = http_request_get(API_URL.'/approvalPR/datatableSearch', $param_search);
			// balikan si API
			$param_search = json_decode($param_search, true);
			$output['recordsTotal']=$output['recordsFiltered']=$param_search['length'];
		}

		if($_POST["is_date_search"] == "yes") {
			$parameter = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'is_date_search' => 'yes', 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'approve_head' => $_POST["status_approve"]);
		} else {
			$parameter = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => 'date_send_pr', 'dir' => 'desc',  'is_date_search' => $_POST["is_date_search"], 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'approve_head' => $_POST["status_approve"]);
		}
	
		// proses paling awal saat load, dan saat user ada klik pada colum di table nya
		if (isset($_REQUEST["order"])){
			$parameter = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => $column_order[$_REQUEST["order"][0]["column"]], 'dir' => $_REQUEST["order"][0]['dir'],  'is_date_search' => $_POST["is_date_search"], 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'approve_head' => $_POST["status_approve"]);
		}else{
			$parameter = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => 'date_send_pr', 'dir' => 'desc',  'is_date_search' => $_POST["is_date_search"], 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'approve_head' => $_POST["status_approve"]);
		}
	
			$query = http_request_get(API_URL.'/approvalPR/datatableSearch', $parameter);
			$query = json_decode($query, true);
	
		
		// if($_POST["is_date_search"] == "yes") {
		// 	if($_POST["start_date"] != "" && $_POST["end_date"] != '' && $_POST["status_approve"] == '-') {
		// 		$start_date = date("Y-m-d", strtotime($start_date));
		// 		$end_date = date("Y-m-d", strtotime($end_date));
		// 		//$this->db->where('date_create BETWEEN "'.$start_date. '" and "'.$end_date.'"');

		// 		$this->db->where('date_send_pr >=', $start_date.' 00:00:00');
		// 		$this->db->where('date_send_pr <=', $end_date.' 23:59:59');

		// 		//$this->db->where('aprove_fc',$status_approve);
		// 		$this->db->order_by("date_send_pr");
		// 	} elseif($_POST["start_date"] != "" && $_POST["end_date"] != '' && $_POST["status_approve"] != '-') {
		// 		$start_date = date("Y-m-d", strtotime($start_date));
		// 		$end_date = date("Y-m-d", strtotime($end_date));
		// 		//$this->db->where('date_create BETWEEN "'.$start_date. '" and "'.$end_date.'"');

		// 		$this->db->where('date_send_pr >=', $start_date.' 00:00:00');
		// 		$this->db->where('date_send_pr <=', $end_date.' 23:59:59');
		// 		$this->db->where('aprove_head', $status_approve);
		// 		$this->db->order_by("date_send_pr");
		// 	} elseif($_POST["start_date"] == "" && $_POST["end_date"] == '' && $_POST["status_approve"] != '-') {
		// 		$this->db->where('aprove_head', $status_approve);
		// 		$this->db->order_by("date_send_pr");
		// 	}
		// }

		// 	/*Lanjutkan pencarian ke database*/
		// 	 $this->db->limit($length,$start);		
		// 	 $this->db->where("flag_send_pr",$str_status_send) ;
		// 	 $this->db->where("id_company",$str_idcompany);
		// 	 $this->db->where("id_dept",$str_iddept);	
		// 	 $this->db->where("status","1");

		//  /*Urutkan dari alphabet paling terkahir*/
		// 	 $this->db->where("status","1");
		// // $this->db->order_by('date_pp','DESC');
		//  //order column
		
		//  if (isset($_REQUEST["order"])){
		// 	  $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
		//  }else{
		// 	  $this->db->order_by('aprove_head','desc');
		// 	  $this->db->order_by('approve_purchasing','desc');
		// 	  $this->db->order_by('aprove_fc','desc');
		// 	  $this->db->order_by('aprove_bod','desc');
	 	//  }
		  
        //   $this->db->order_by('date_send_pr','DESC');  
		//   $query=$this->db->get('qv_head_pp_complite');


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		
		
		foreach ($query['data'] as $row_tbl) {
			
			 if ($row_tbl['flag_send_pr'] == "-1")	
			 {
				   $info_aproval = "Reject";
			 }else{				
				   $info_aproval = "New Crated";				
			 }
			 
			 If ($row_tbl['attach_quo'] != "")
			  {  
			   $attach_quo = '<div  align="center"><a onClick="PDFPopup(this)" req_id='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
			 }else{
				$attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
			 }
		
		   
		   
		     $btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i></a></center>';//button detail
		   
		    If ($row_tbl['aprove_head'] == "1")
			 {
				$head_approval = "<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";
				
			 }else{
				  If ($row_tbl['flag_send_pr'] == "-1")
				  {
					 $head_approval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
				  }else{
					 $head_approval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
				  }
			 }
			 
			 If ($row_tbl['approve_purchasing'] == "1")
			 {
				   //$purchase_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				   $purchase_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
			   } elseif($row_tbl['approve_purchasing'] == "-2") {
				   $purchase_approval = '<div style=" color:#428BCA">'."Revised by Purchasing".' <br> '.date('d-m-Y H:i:s', strtotime($row_tbl['date_reject_purchasing'])).'</div>' ;
			   } else{
					 If ($row_tbl['flag_send_pr'] == "-1")
					 {
					   $purchase_approval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
					 }else{
					   $purchase_approval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
					 }
					 
			  }
			   
			   If ($row_tbl['aprove_fc'] == "1")
			  {
					//$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
					$fc_aproval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
				 }else{
					  If ($row_tbl['flag_send_pr'] == "-1")
					  {
						$fc_aproval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
					  }else{
						$fc_aproval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
					  }
			   }
			   
				
			   
 				 If ($row_tbl['aprove_bod'] == "1")
				 {
						//$bod_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
						 $bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
					 }else{
						  If ($row_tbl['status_send_aprove'] == "-1")
						  {
							//$bod_approval =  '<img src="'.base_url("asset/images/reject.png").'">' ;
						  	  $bod_approval = "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
						  }else{
							$bod_approval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
						  }
				  }
				 
			

 $strheadaprove = $row_tbl["aprove_head"] ;	
 
 $chk_idmaster = "";
 $str_reject="";
 
 if ($row_tbl["aprove_head"]=="1" ) {			
	  $str_reject ='<div align="center" style="color:#2292d8"> Approved</div>' ;	
	  $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";		
  }else{
	    $str_reject =  '<div align="center"><a href="#" class="btn btn-app btn-danger btn-xs radius-8 reject " id="reject" style="text-decoration:none" req_id='.$row_tbl["id_master"].'><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></a></div>'; //button reject 	
						  
					 //checkbox aktiff belum teraproval.
			         $chk_idmaster ='<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value='.$row_tbl["id_master"].' class="ace" req_id_del='.$row_tbl["id_master"].' />
           <span class="lbl"></span> ';				
	}
  
 //Pembedaan Warna pada type purchase.
		   if ($row_tbl['flag_purchase'] == "1") {
		       $type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
		   }else{
			     if ($row_tbl['flag_purchase'] == "2") {
		     		  $type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
		  		 }else{
			   		  if ($row_tbl['flag_purchase'] == "3") {
		     			  $type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
		  			 }else{
			    			 if ($row_tbl['flag_purchase'] == "4") {
		      					 $type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
		  					 }else{
		       					 $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
							 };
					 };
				 };   
		     };
//end------------------------------			 

//date--------------
if ($row_tbl['date_aprove_head'] ==""){
   $datehead = "" ;
}else{
   $datehead  = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_head']))."</div>";
}

	if ($row_tbl['date_approve_purchasing'] ==""){
	   $datepur = "" ;
	}else{
	   $datepur = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_approve_purchasing']))."</div>";
	}

	if ($row_tbl['date_aprove_fc'] ==""){
		$datefc = "" ;
	}else{
		$datefc= "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_fc']))."</div>";
	}

	if ($row_tbl['date_aprove_bod'] ==""){
	   $datebod = "" ;
	}else{
	   $datebod = "<div style='color:#EB293D'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_bod']))."</div>";
	}

//end date-----------------

//label colum 1 idepp
$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP
//end label

$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";

if($row_tbl['term_top'] > 1) {
	$day = 'Days';
} else {
	$day = 'Day';
}	
		 
			$output['data'][]=array(
			                        $chk_idmaster,
									//$row_tbl['id_master'],
									$idmas,
									$btn_view_detail,
									$attach_quo,															
									$str_reject	,
									$row_tbl['short'],
									$row_tbl['user_submission'],
									$row_tbl['dept'],
									//$header_desc ,//$row_tbl['header_desc'],
									$row_tbl['vendor'],																								
									date('d-m-Y', strtotime($row_tbl['date_send_pr'])),	
									$row_tbl['currency'],
									$type_purchase, //$row_tbl['type_purchase'],
									//$row_tbl['nomor_coa'],
									number_format($row_tbl['gran_total'],2,'.',','),
									//number_format($row_tbl['ppn'],2,'.',','),
								//	number_format($row_tbl['gran_totalppn'],2,'.',','),
									$row_tbl['term_top']. ' '.$day,
									$head_approval.$datehead,
									$purchase_approval.$datepur,
									$fc_aproval.$datefc,
									$bod_approval.$datebod,	
																	
							  );			
		}
		echo json_encode($output);
	}	
	
	
	 		
}

	
