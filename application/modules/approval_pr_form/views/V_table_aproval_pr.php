
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>
      
<?php   	
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>      
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>                        
                <?php  echo "Successful Grant Approval"	?>  
		  </div>		
<?php } ?>

<?php   	
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>                
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>                        
			<?php  echo "Send Approval failed"	?>  
        </div>		      
<?php } ?>	

<?php   	
	If ($this->session->flashdata('pesan_reject') == "1"){ ?>                
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>                        
			<?php  echo "Approval Rejected Succesful"	?>  
        </div>		      
<?php } ?>	
  

  
          
   
          
 <?php    
    // echo $this->session->userdata("aproval_flag");
	 echo form_open('approval_pr_form/c_table_aproval_pr/multiple_submit',array('id' => 'form-tableprovepr')); 
  ?>     
 
 <table>
     <tr>
      <td>
        
        <button class="btn btn-app btn-success btn-xs radius-4 btnref btn-clean" type="submit" id="btnref" name="btnref"  value ="btnref"   >
            <i class="ace-icon fa fa-exchange bigger-160"></i>
            Refresh 
		</button> 
        
      </td>
       <td colspan="3">&nbsp;</td> 
       <td colspan="3">                                      
         
        <button class="btn btn-app btn-primary btn-xs radius-4 btnref btn-clean btn_approved" type="button" id="btnaprove" name="btnaprove"  value ="btnaprove"  disabled="disabled" >
            <i class="ace-icon fa fa-check-square-o bigger-160"></i>
            Approve
		</button> 
         	           
         </td>         
      </tr> 
    </table>   
 </br>

  <div class="table-header btn-info"> <?php echo " ".$header ;?>   </div>    
  
  <div class="form-group col-xs-6">
  <p>
        <div class="col-md-11">
            <div class="form-inline ">
                <div class="form-group">Date start</div>
                
                <div class="form-group">
                    <div class="input-group date">
                       <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                       </div>
                       
                       <input type="text" size="10" class="form-control input-daterange" id="start_date" name="start_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                    </div> <!-- /.input-group datep -->       
                </div>
               
               <div class="form-group ">
                       <label for="From" class="col-md-1 control-label">To</label>
               </div>
              
               <div class="form-group">
                      <div class="input-group date">
                               <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                               </div>
                                     <input type="text" size="10" class="form-control input-daterange" id="end_date" name="end_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                       </div>  
               </div>

			   <div class="form-group ">
                       <label for="From" class="col-md-1 control-label">Status</label>
               </div>
              
               <div class="form-group">
							   <select name="status_approve" id="status_approve" class="form-control">
										<option value="-">- Status Approve - </option>
										<option value="1">Approved</option>
										<option value="0">Not Approved</option>
									</select>
                 
                                    
									
               </div>
                
            </div>
        </div>
      </div> 

		<table id="myTable"  cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped ">
	    <thead class="text-warning">
         <th>Action</th>
		 <th>PR No </th>	
         <th>Detail</th> 	
         <th>Quote</th> 	
		 <th>Reject</th> 
		 <th>Company</th>	
         <th>Requester</th> 
         <th>Dept</th>							
         <th>Vendor</th>			
         <th>Date Sent</th> 
         <th>Curr</th> 
         <th>Type</th> 
		 <th>Total </th> 
         <th>T.O.P</th> 
		 <th>Dept Head</th>
		 <th>Purchasing</th>  
         <th>F.C</th> 
		 <th>B.O.D</th> 						
      </thead>
	</table>	
       
  
 <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close btnclose" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Info & Remarks</h4>
                    </div>
                    
                    <div class="modal-body">
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->            
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class=" btn btn-app btn-primary btnclose btn-xs radius-8" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal -->  	

<!-- Modal Buat Reject -->

  <div class="modal fade" id="myModal-reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Remarks For Reject Status</h4>
                    </div>
                    
                    <div class="modal-body-reject">
                       <table width="90%" class="table-condensed" align="center">
                            <tr> 
                                <td> <label for="PR Number"  class="col-xs-8">PP Number</label></td> 
                             
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtppno" name="txtppno" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                             <tr> 
                                <td ><label for="Reject Reason"  class="col-xs-9">Reject Reason</label></td> 
                                <td>
                                  <div class="col-xs-12">
                                    <textarea id="txtreject" name="txtreject"  class="form-control"></textarea>
                                  </div>  
                                </td>    
                             </tr>
                             <tr> 
                                <td></td> 
                                <td>
                                   <div class="col-xs-12">
                                   
                                   </div>        
                                </td>    
                             </tr>
                            </table> 
           
                    </div> 
                    
                     <div class="modal-footer">
                        <button class="btn btn-app btn-danger btn-xs radius-4 btnref btn-clean btnreject" type="button" id="btnreject" name="btnreject"  value ="btnreject"   >
            <i class="ace-icon fa fa-check-square-o bigger-160"></i>
            Reject
		</button> 
                  <!---   <input type="submit" name="btnreject" value=" Reject" class="btn btn-app btn-danger btn-xs radius-8"/> -->
                     <button type="button" class="btn btn-app btn-primary btn-xs radius-8 btn-xs radius-8" data-dismiss="modal">
                     <i class="ace-icon glyphicon glyphicon-remove  bigger-160"></i>
                     Close</button>                       
                    </div>                  
                </div>
            </div>           
        </div>
    
<!-- Modal Buat Reject -->  	

<!-- Content Popup -->
<div id="dialog" style="display: none;">
    <div>
        <iframe id="frame" width="750px" height="550px"></iframe>
    </div>
</div>
<!-- Content Popup -->

 
<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
/* $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    }); */
</script> 


<script>
//--------------------function disbaled enabled button with check book

<?php if($this->session->userdata('aproval_flag') != '1' || $this->session->userdata('aproval_flag') != '2' || $this->session->userdata('aproval_flag') != '5' || $this->session->userdata('aproval_flag') != '6' || $this->session->userdata('aproval_flag') != '7' || $this->session->userdata('aproval_flag') != '8' || $this->session->userdata('aproval_flag') != '4' )   : ?>
var counterCheckapproved = 0;
	$('body').on('change', 'input[type="checkbox"]', function() {
		this.checked ? counterCheckapproved++ : counterCheckapproved--;
		counterCheckapproved >= 1 ? $('#btnaprove').prop("disabled", false): $('#btnaprove').prop("disabled", true);
	});
<?php else: ?>
var counterCheckapproved = 0;
	$('body').on('change', 'input[type="checkbox"]', function() {
		this.checked ? counterCheckapproved++ : counterCheckapproved--;
		counterCheckapproved == 1 ? $('#btnaprove').prop("disabled", false): $('#btnaprove').prop("disabled", true);
	});
<?php endif ?>
//-----------------------------------------------------
  
//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});		  


</script>

<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.detail',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("approval_pr_form/c_table_aproval_pr/get_idtrans_modal");// echo site_url("approval_pr_form/c_table_aproval/get_nopp_modal"); ?>';
		
		$("#myModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});
	
</script>

<script>


$(function(){ // Show modal pop up send REJECT
  $(document).on('click','.reject',function(e){
		var req_id = $(this).attr('req_id'); 										
		$("#myModal-reject").modal('show');
		$('#txtppno').val(req_id); //menampilkan isi dari  idmaster kedalam inputbox 	
		$('#txtreject').val(''); //mengkosongkan txtremark reject 			  				
	   });																					
});		
</script>


<script>

$(function(){
    $('#myform').on('submit', function(e){ // id #myform di buat untuk diferentsiasi dengan form open code igniter
        e.preventDefault();
		
		var url = '<?php echo base_url("approval_pr_form/c_table_aproval_pr/multiple_submit"); ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), // myform di buat untuk diferentsiasi dengan form open code igniter 
            success: function(data){
                 alert($('#txtppno').val() + ',' + $('#txtreject').val())								 
            }
        });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function(){ 
	$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data('no'); //
	
		function fetch_data(is_date_search,start_date='', end_date='', status_approve =''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			"lengthChange": false, //hide  Entries dropdown
			
				"ajax":{
				    url:  "<?php echo base_url('approval_pr_form/c_table_aproval_pr/cath_data_fordatatables') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date, status_approve: status_approve
					},
					
					"columnDefs": [ {
						 "targets": [0,2,3,4], /* column index */
				          "orderable": false, /* true or false */		
					} ],
				 }
			 });
		  }
	
	//proses change date-----------------------------------------------
	$('#start_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
			var status_approve = $('#status_approve').val();
		
			if(start_date != '' && end_date != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date, status_approve);
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
		$('#end_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
			var status_approve = $('#status_approve').val();
		
			if(start_date != '' && end_date != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date, status_approve);		
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});

		$('#status_approve').change(function(){
			var status_approve = $('#status_approve').val();
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(status_approve != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date, status_approve);
			} else if(status_approve != ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
		
 //end change date----------------------------------------------------------------------	
 
 
 
 //onblur date-------------------------------------------------------------------------
 
 		// $('#start_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
		
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		  fetch_data('no', start_date, end_date);
		// 	} 
			
		// });
 
 
 	
		// $('#end_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
			
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		 fetch_data('no', start_date, end_date);
		// 	} 
		// });
 //end onblur-------------------------------------		
	
	
	
	
	
});


</script>

<script>

	function PDFPopup(e) {
		var url = $(e).attr('req_id');
		$("#dialog").dialog({
			width: 'auto',
			height: 'auto',
			resize: 'auto',
			autoResize: true
		});
		$("#frame").attr("src", url + "#toolbar=0");
        
    };

</script>

<script>
//button approved
$(function(){
    $(document).on('click','.btn_approved',function(e){
	     var m =  $('#ViewloadinglModal');	
		 var url = "<?php echo base_url('approval_pr_form/c_table_aproval_pr/do_aproval'); ?>";	 
		 var formData = new FormData(document.getElementById('form-tableprovepr'));
		
	  
			  if (confirm('Are you sure to Approved'))
			  {
				 function callBeforeAjax(){
					 m.modal('show');
				 }																																											      
				  $.ajax({
					   beforeSend: callBeforeAjax,	
					   url:url,      
					   method:'POST',				
					   data: formData,										
					   processData: false,															
					   async: false,
					   processData: false,
					   contentType: false,		
					   cache : false,	
					   success:function(data){
							   m.modal('hide');
					   }, 	   
				   
					   complete:function(data) {  
						   alert('Approval Successfully !!');   
						   location.reload();		  	               		  
					   }
					 
				  }); 
				}//endif	
	});  	 
});
//end-----------------------------------------------

//button reject-------------------------------------
$(function(){
    $(document).on('click','.btnreject',function(e){
	     var m =  $('#ViewloadinglModal');	
		 var url = "<?php echo base_url('approval_pr_form/c_table_aproval_pr/do_rejected'); ?>";		 
		 var formData = new FormData(document.getElementById('form-tableprovepr'));	  		       
		
		
		
		  if ($('#txtreject').val()==""){
			  alert("Remark reject Info Must be Required") ;
			  $('#txtreject').focus();
		  }else{	
				  function callBeforeAjax(){
					 m.modal('show');
				  }																																											      
						  
				  $.ajax({
					   beforeSend: callBeforeAjax,	
					   url:url,      
					   method:'POST',				
					   data: formData,										
					   processData: false,															
					   async: false,
					   processData: false,
					   contentType: false,		
					   cache : false,	
					   success:function(data){
							   m.modal('hide');
					   }, 	    
				   
					   complete:function(data) {  
						   alert('Reject Successfully !!');   
						   location.reload();		  	               		  
					   }
					 
				  }); 	
		    }
	  
	});  	 
});
//-------------------------------------------------------
</script>

  <div class="modal fade" id="ViewloadinglModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">            
                    <div class="modal-body-load-spiner" align="center">
                            <img src="<?php echo base_url('assets/img/loading_spinner.gif') ; ?>" />
                    </div> 
                    
                </div>
            </div>
 </div>

<?php if ($this->session->userdata('aproval_flag')==4 ||  $this->session->userdata('aproval_flag')==2) {     ?>

<script> 

var counterCheckaprove = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckaprove++ : counterCheckaprove--;
    counterCheckaprove == 1 ? $('#btnaprove').prop("disabled", false): $('#btnaprove').prop("disabled", true);
});

</script>



<?php } ?>


