
<h2>
	<?php
	    $strcom = $this->session->userdata('company');
		$strbranch = $this->session->userdata('name_branch');
		$strdept = $this->session->userdata('dept');
		 
	    echo  'Company : '.$strcom. " | " ." Location : " .$strbranch. " | " ."Dept: ".$strdept;
	 ?>
</h2>

<table border="1" >
 <?php foreach ($str_pp_master as $row)  :  ?>
    <tr >
      <td >PP Number :</td>
      <td><?php echo $this->session->userdata("ses_idmaster");?></td>
    </tr>
    <tr >
      <td >Requester :</td>
      <td><?php echo $row->user_submission ?></td>
    </tr>
    <tr >
      <td >Date Sent Approval : </td>
      <td><?php echo date('d-m-Y', strtotime($row->date_send_aproval)) ?></td>
    </tr>
    <tr >
    <td  >Type :</td>
       <td><?php echo $row->type_purchase ?></td>
    </tr>
   <!--  <tr>
    <td>C.O.A :</td>
       <td><?php //echo $row->nomor_coa ?></td>
    </tr> --> 
    <tr>   
      <td   >T.O.P :</td>
       <td><?php echo $row->term_top ?></td>
    </tr>
     <tr >
       <td >Remarks:</td>
       <td><?php echo $row->remarks ?></td>
    </tr>
    <tr>
       <td ><?php echo "Grand Total : "." ".$row->currency ?></td>
       <td ><?php echo number_format($row->gran_total,2,'.',',')?></td>
    <tr>  
        <td ><?php echo "PPN : "." ".$row->currency ?></td>
        <td ><?php echo number_format($row->ppn,2,'.',',')?></td>
    </tr>
    <tr>  
        <td ><?php echo "PPH : "." ".$row->currency ?></td>
        <td ><?php echo number_format($row->pph,2,'.',',')?></td>
    </tr>
     <tr>
        <td ><?php echo "Grand Total + Tax : "." ".$row->currency ?></td>
       <td ><?php echo number_format($row->gran_totalppn,2,'.',',')?></td>              
    </tr>
 <?php endforeach; ?>   
</table>

</br>

<h2>Detail PP</h2>
<table  width="70%" border="1">    
    <tr >
        <td>No</td>
        <td>Description</td>
        <td>Spec</td>
       <!-- <td>P.O Reff</td> -->
        <td>Qty</td>
        <td>Harga </td>							
        <td>Total Harga</td>
    </tr>
    <?php foreach ($str_pp_detail as $row_detail)  : $intno = $intno +1;  ?>
   		<tr>	
          <td><?php echo $intno ?></td>							  
          <td><?php echo $row_detail->desc ?></td>							  
          <td><?php echo $row_detail->spec ?></td>	
         <!--  <td><?php //echo $row_detail->po_reff ?></td>	-->																																																			
          <td><?php echo number_format($row_detail->qty) ?></td>							
          <td><?php echo number_format($row_detail->harga,2,'.',',') ?></td>							
          <td><?php echo number_format($row_detail->total,2,'.',',') ?></td> 
        </tr>
    <?php endforeach; ?>
</table>