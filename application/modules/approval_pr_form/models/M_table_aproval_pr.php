<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_table_aproval_pr extends CI_Model {	

	public function __construct() {
		parent::__construct();

		$this->load->helper('api_helper');
	}
				    
    public $db_tabel = 'qv_head_pp_complite';
	 
    // public function tampil_add_pp(){ //tampil table untuk ngececk num_row	
	
	//      if ( $this->session->userdata('aproval_flag')=="3" or $this->session->userdata('aproval_flag')=="0"){ //aproval falg 1= bod, 2 = fc , 3 = head. 
		     
	// 		 $ses_id_dept = $this->session->userdata('id_dept'); 
	// 	     $ses_id_company = $this->session->userdata('id_company');  	
	// 	     $ses_id_branch = $this->session->userdata('branch_id');		 		 					 
	// 		 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject									
	// 		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	// 		 $str_flag_purchase = "1"; //jika untuk flag unit
			 			 
			  
	// 		 $this->load->database();		
	// 		 $this->db->select('*');		
	// 		 $this->db->where('flag_purchase',$str_flag_purchase);
	// 		 $this->db->where('id_company',$ses_id_company);					
	// 		 $this->db->where('branch_id',$ses_id_branch );	
	// 		 $this->db->where('id_dept',$ses_id_dept);
	// 		 $this->db->where('status',$status_delete);
	// 		 $this->db->where('status_send_aprove',$send_aprove);													
	// 		 $this->db->order_by('date_send_aproval','desc');
	// 		 $result = $this->db->get('qv_head_pp_complite');			     		 
			 
	// 		 return $result;
			 
	// 	 }else{
	// 	    if ($this->session->userdata('aproval_flag') == "4"){	 
	// 			 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
	// 		     $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	// 		     $flag_head	=  "1"; //jika  1 sudah di aprove head dept di tampilkan di view FC 
	// 		     $flag_purchase_stat	=  "0"; //jika  0 belum di aprove fc di maka tampilkan di view FC
	// 		     //  $flag_aprove_fc =  "1"; //jika 1 sudah di aprove fc maka di tampilkan di view FC
	// 		   	 $flag_bod	=  "0"; //jika  0 belum di aprove bod maka di tampilkan di view FC 
	// 			 $str_flag_purchase = "1"; //jika untuk flag unit
				 			 
	// 		     $this->load->database();		
	// 		     $this->db->select('*');					
				 
	// 			 $this->db->where('flag_purchase',$str_flag_purchase);					 
	// 		     $this->db->where('aprove_head',$flag_head); //flag aprove Head
	// 			 $this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a				
	// 		    // $this->db->where('aprove_fc',$flag_fc);   //flag aprove purchase approve_purchasing				
	// 		    // $this->db->or_where('aprove_fc',$flag_aprove_fc);   //flag aprove FC			
	// 		     $this->db->where('status_send_aprove',$send_aprove);
	// 		     $this->db->where('status',$status_delete);
	// 		     $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D		
											 
	// 			 $this->db->order_by('date_send_aproval','desc');
	// 			 $result = $this->db->get('qv_head_pp_complite');			     		 
	// 			 return $result;
				 
	// 		}else{
				
	// 			 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
	// 			 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	// 			 $flag_head	=  "1"; //jika 1 sudah di aprove head dept dan..(kebawah) 
	// 			 $flag_purchase_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
	// 			 $flag_fc_approved_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
	// 			 $flag_bod	=  "0"; //jika 0 belum di aprove B.O.D ,maka di tampilkan di view BOD 
	// 			 //$str_flag_purchase = "1"; //jika untuk flag unit
				  
	// 			 $this->load->database();		
	// 			 $this->db->select('*');								
	// 			 //$this->db->where('flag_purchase',$str_flag_purchase);
	// 			 $this->db->where('aprove_head',$flag_head); //flag aprove Head
	// 			 $this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a				
	// 		     $this->db->where('aprove_fc',$flag_fc_approved_stat);   //flag aprove purchase a		
	// 			 $this->db->where('status_send_aprove',$send_aprove);
	// 		     $this->db->where('status',$status_delete);
	// 		     $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D		
											 
	// 			 $this->db->order_by('date_send_aproval','desc');
	// 			 $result = $this->db->get('qv_head_pp_complite');			     		 
	// 			 return $result;
				
				
	// 		}			 
	// 	 }
	// }	

	// public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 
		 
	//    if ( $this->session->userdata('aproval_flag')=="3"){    
	//          $ses_id_company = $this->session->userdata('id_company'); 				 
	// 	     $ses_id_branch = $this->session->userdata('branch_id');	
	// 	     $ses_id_dept = $this->session->userdata('id_dept'); //manggili session id dept					 			  		
	// 		 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
	// 		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
	// 		 $str_flag_purchase = "1"; //jika untuk flag inderect	 
			  
	// 		 $this->load->database();		
	// 		 $this->db->select('*');	
	// 		 $this->db->where('flag_purchase',$str_flag_purchase);			
	// 		 $this->db->where('id_company',$ses_id_company);					
	// 	     $this->db->where('branch_id',$ses_id_branch );	
	// 	     $this->db->where('id_dept',$ses_id_dept);
	// 		 $this->db->where('status',$status_delete);
	// 		 $this->db->where('status_send_aprove',$send_aprove);				 			
	// 		 $this->db->order_by('date_send_aproval','desc');
	// 		 $this->db->limit($start_row, $limit);					     	 	   		
	// 		 $result = $this->db->get('qv_head_pp_complite')->result() ;		 		   
	// 		 return $result ;
    //    }else{	
	//    	   if ($this->session->userdata('aproval_flag') == "4"){	 
	// 		   $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject						
	// 		   $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	// 		   $flag_head	=  "1"; //jika  1 sudah di aprove head dept di tampilkan di view FC 
	// 		   $flag_purchase_stat	=  "0"; //jika  0 belum di aprove fc di maka tampilkan di view FC				
	// 		   $str_flag_purchase = "1"; //jika untuk flag inderect tampilkan di view FC 
	// 		   $flag_bod = "0";
			   				 
	// 		   $this->load->database();		
	// 		   $this->db->select('*');					
	// 		   $this->db->where('flag_purchase',$str_flag_purchase);			
	// 		   $this->db->where('status_send_aprove',$send_aprove);						 
	// 		   $this->db->where('aprove_head',$flag_head); //flag aprove Head
	// 		   $this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a					
	// 		  // $this->db->or_where('aprove_fc',$flag_aprove_fc);   //flag aprove FC						   
	// 		   $this->db->where('status',$status_delete);
	// 		   $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D													
																								 
	// 		   $this->db->order_by('date_send_aproval','desc'); 
	// 		   $this->db->limit($start_row, $limit);					     	 	   		
	// 		   $result = $this->db->get('qv_head_pp_complite')->result();		 		   
	// 		   return $result ;
			   
	// 	   }else{
			   
	// 		    $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
	// 		    $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	// 		    $flag_head	=  "1"; //jika 1 sudah di aprove head dept dan..(kebawah) 
	// 		    $flag_purchase_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
	// 		    $flag_fc_approved_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
	// 		    $flag_bod	=  "0"; //jika  0 sudah di aprove head dept dan sudah di aprove fc tetapi belum di aprove bod
							 
	// 		    $this->load->database();		
	// 		    $this->db->select('*');					
			   			   
	// 		   // $this->db->where('flag_purchase','1');		//indirect
	// 		    $this->db->where('aprove_head',$flag_head); //flag aprove Head
	// 			$this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a				
	// 		    $this->db->where('aprove_fc',$flag_fc_approved_stat);   //flag aprove purchase a							   			   
	// 		    $this->db->where('status_send_aprove',$send_aprove);
	// 		    $this->db->where('status',$status_delete);
	// 		    $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D		
				 
																												 
	// 		    $this->db->order_by('date_send_aproval','desc'); 
	// 		    $this->db->limit($start_row, $limit);					     	 	   		
	// 		    $result = $this->db->get('qv_head_pp_complite')->result();		 		   
	// 		    return $result ;
	// 	   }
	//    }
	// }	
	
	
	
	function give_aproval()						  		
	{
	       $strid_master =$this->input->post('msg'); 			  		 	
		   $flag_aproved = "1" ; // flag 1 aproved
		 
									  
			   for ($i=0; $i < count($strid_master) ; $i++) { 										
	
						$data = array('aprove_head'=>$flag_aproved,
									  'date_aprove_head' =>date('Y-m-d H:i:s'), 	
									  'date_aproval' =>date('Y-m-d H:i:s'),
									  'id_master' => $strid_master[$i]
						);	
							  
			   //$this->db->where('id_master',$strid_master[$i]);
			   //$this->db->update('tbl_master_pp',$data); 

			   // Call API /updateMasterPP
			   $query = http_request_put(API_URL.'/PRForm/updateMasterPP', $data);
			
			   $buff_ses_idmaster = array('ses_idmaster'=>$strid_master[$i]);
			   $this->session->set_userdata($buff_ses_idmaster);				
			
			  $value[] = $strid_master[$i] ;	//memasukan beberapa id master yg di select ke dalam array.				
			  $this->session->set_userdata('result_id_master',$value); //simpan session array					
														
		    }				 				  				 				  				
		  
		  return true;	
			 
	  
		  
	}
	
	function give_rejected()						  		
	{	     		  		 	
		 $strid_master = $this->input->post('txtppno');
		 $str_name_reject = "By"." ".$this->session->userdata('name')." " . ":";
		 $strid_reason_reject =  $str_name_reject." ".$this->input->post('txtreject');
		 $flag_zero_aprove = "0" ; // menggembailkan flag approve menjadi 0 	 
		 $flag_reject = "-1" ; // flag -1 flag_reject pada field status_send_approval
				
		//   $query = $this->db->select("id_master") //check id booking pada table_parent 								
		// 					->where('id_master',$strid_master )
		// 					->get('tbl_master_pp') ;
		
		$parameter = array('id_master' => $strid_master);
		$query = http_request_get(API_URL.'/PRForm/getPP', $parameter);
		$query = json_decode($query);


		  if ($query->length == 1)
		  {				
			
										
			$data = array('flag_send_pr'=>$flag_reject,
						  'reason_reject'=> $strid_reason_reject,
						  'aprove_head' => $flag_zero_aprove,
						  'date_reject_head' => date('Y-m-d H:i:s'),
						  'id_master' => $strid_master
						  ); 																																							
			//$this->db->where('id_master',$strid_master);
			//$this->db->update('tbl_master_pp',$data); 

			// Call API PRForm/updateMasterPP
			$query = http_request_put(API_URL.'/PRForm/updateMasterPP', $data);
			
			$buff_ses_idmaster = array('ses_idmaster'=>$strid_master);
			$this->session->set_userdata($buff_ses_idmaster);
			
			return true;
		  }		  
			 
	 }
}
