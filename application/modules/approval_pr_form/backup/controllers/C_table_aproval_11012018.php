<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_table_aproval extends MY_Controller
{
  public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('M_table_aproval','',TRUE);
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_table_aproval->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}
	}

  public function index()
  {

		$this->show_table(); //manggil fungsi show_table
  }

  public function show_table()
  {
	 $tampil_table_aproval= $this->M_table_aproval->tampil_add_pp()->result();
	 $total_rows =$this->M_table_aproval->tampil_add_pp()->num_rows();
	 $dept  = $this->session->userdata('dept') ;
	 $branch  = $this->session->userdata('name_branch') ;
	 $company = $this->session->userdata('short') ;
	 $data['intno'] = ""; //variable buat looping no table.
	 $data['header'] ="Approval Form"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	 $data['ceck_row'] = $total_rows;
	 $data['tampil_company']=$this->M_table_aproval->get_company()->result();
	 $data['tampil_dept']=$this->M_table_aproval->get_dept()->result();
	 $data['show_view'] = 'approval_form/V_table_aproval';
	 $this->load->view('dashboard/Template',$data);


  }

  public function do_aproval()
  {

	    if ($this->session->userdata('aproval_flag') == "1") //bod
		{
		   $this->M_table_aproval->give_aproval_bod();
		   $this->kirim_email_aprove();
		}else{
			if ($this->M_table_aproval->give_aproval())
			{
				$this->session->set_flashdata('pesan_aproval','1');	// succes

				if ($this->session->userdata('aproval_flag')=="2") {
					$this->kirim_email_to_bod();
				}
				 if ($this->session->userdata('aproval_flag')=="4") {
					$this->kirim_email_to_fc();
				}

				redirect('approval_form/c_table_aproval'); //riderct ke menu utama
				//print_r($this->session->userdata("result_id_master"));

			}else{
				$this->session->set_flashdata('pesan_aproval','0');
				redirect('approval_form/c_table_aproval');
			}
		}
  }

  public function do_rejected()
  {
		if ($this->M_table_aproval->give_rejected())
		{
			$this->session->set_flashdata('pesan_reject','1');
			$this->kirim_email_reject();
			redirect('approval_form/c_table_aproval');
		}else{
			$this->session->set_flashdata('pesan_reject','0');
			redirect('approval_form/c_table_aproval');
		}
  }


   public function do_search_pp()
  {
  		 $tampil_table_aproval= $this->M_table_aproval->get_search()->result();
	     $total_rows =$this->M_table_aproval->tampil_add_pp()->num_rows();

		if ($tampil_table_aproval)
		{
			$dept  = $this->session->userdata('dept') ;
			$branch  = $this->session->userdata('name_branch') ;
			$company = $this->session->userdata('short') ;
			$data['intno'] = ""; //variable buat looping no table.
			$data['header'] ="Approval Form"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
			$data['ceck_row'] = $total_rows;
			$data['pp_view'] =  $tampil_table_aproval;
			$data['tampil_company']=$this->M_table_aproval->get_company()->result();
			$data['tampil_dept']=$this->M_table_aproval->get_dept()->result();
			$data['show_view'] = 'approval_form/V_table_aproval';
			$this->load->view('dashboard/Template',$data);
	  }else{
			$dept  = $this->session->userdata('dept') ;
			$branch  = $this->session->userdata('name_branch') ;
			$company = $this->session->userdata('short') ;
			$data['intno'] = ""; //variable buat looping no table.
			$data['header'] ="Aproval Form"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
			$data['ceck_row'] = $total_rows;
			$data['pp_view'] =  $tampil_table_aproval;
			$data['tampil_company']=$this->M_table_aproval->get_company()->result();
			$data['tampil_dept']=$this->M_table_aproval->get_dept()->result();
			$data['pesan'] = 'Data PP aproval table is empty';
			$data['show_view'] = 'approval_form/V_table_aproval';
			$this->load->view('dashboard/Template',$data);
	  }

  }


  public function multiple_submit() //fungsi submit dengan banyak button dalam satu form
  {

		if ($this->input->post('btnaprove')){
			$this->do_aproval();
		}else{
			if ($this->input->post('btnreject')){	//dari modal popup
				$this->do_rejected();
			}else{
				if ($this->input->post('btncari')){
					$this->do_search_pp();
				}else{
					redirect('approval_form/c_table_aproval');
				}
			}
		}


	}

//------------------------------------pashing result to modal popup-------------------
	public function get_nopp_modal() {
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //
	     $query_master = $this->db->query("select id_master,remarks,gran_total,currency,ppn,pph,gran_totalppn from qv_head_pp_complite where id_master ='".$nopp."'");
         $query_detail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");

		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){

		      $data['str_pp_master']=$query_master->result();
			  $data['str_pp_detail']=$query_detail->result();
			  $data['intno']="";
			  $data['phasing_nopp'] = $nopp ;
			  $tampil_detail = $this->load->view('approval_form/V_conten_detail',$data,true);

			  echo $tampil_detail ;
		   }
    }


public function kirim_email_reject(){

		   $this->load->library('email');
		   //konfigurasi email
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T";
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.googlemail.com';
		  $config['smtp_host']    = 'ssl://googlemail-smtp.l.google.com';

		   //-----------exchange
		   //$config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';
		   //$config['smtp_port']  = '5220';
		   $config['smtp_port']  = '465'; //port gmail
		   $config['newline']    = "\r\n";
		   $config['mailtype']   ="html" ;
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!";
		   //$config['smtp_user']= "helpdesk@eurokars.co.id";
		   //$config['smtp_pass']= "";
		   //--------------------------------

			$strponumber =$this->session->userdata("ses_idmaster");
			$str_status = '1' ; //aktif ;

			if ($strponumber != '') :
				//Select untuk email master PP----------------------------
				$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,ppn,pph,gran_total,gran_totalppn,remarks,remark_aprov,status_send_aprove,aprove_bod,reason_reject,email_pp,email_head_pp,email_cc_pp from qv_head_pp_complite where id_master ='".$strponumber."' and status ='".$str_status."'");
			  if ($query->num_rows() > 0 ) :
			      $data_email['str_pp_master'] = $query->result();
				  foreach ($query->result() as $row_ceck) :
				     //simpan row alamat email kedalam variable..
					 	 $struseremail = $row_ceck->email_pp ; //addrees for to email
					     $strheademail = $row_ceck->email_head_pp; //addrees for to email
						 $strccemail = $row_ceck->email_cc_pp ; //addrees for to email
					 //end-----------------------------------------

				     //Select untuk email deteail PP--------------------------------
				     $query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."' and status ='".$str_status."'");
				     if ($query_detail->num_rows() > 0 ) :
					    $data_email['str_pp_detail'] = $query_detail->result();
					    $data_email['intno'] = ""; //untuk counter angka table
					    $message = $this->load->view('approval_form/V_content_email_aproval',$data_email,true);
					  endif;
					  //end-----------------------------------------------------------
						  if ($row_ceck->status_send_aprove =="-1")	:
								$subject = "PP REJECTED-- Eurokars Motor Indonesia";
								$result = $this->email ;
								$this->email->initialize($config);
								$this->email->set_newline("\r\n");

								//Exchange
								//konfigurasi pengiriman

								$sender_email = "helpdesk@eurokars.co.id";
								$sender_name = "Epurchasing Notification";

								//GMAIL-----
								//$sender_email = "epurchaseeuro@gmail.com";
								//$sender_name = "Epurchasing Eurokarsgroup";
								//$to = 'brian.yunanda@eurokars.co.id';

							    $to = $struseremail.",".$strheademail.",".$strccemail; //email address,user,head, and CC

								$this->email->from($sender_email, $sender_name);
								$this->email->to($to);
								$this->email->subject($subject);

								//$this->email->message(print_r($message, true));

								$this->email->message($message);// tanpa array

								if ($this->email->send()) :
									$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
									//redirect(current_url());
								else:
									show_error($this->email->print_debugger());
								endif;
						      endif;
					      endforeach;
			         endif;
			     endif;

			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent');
			 $this->session->unset_userdata('ses_grandtotal');
			 $this->session->unset_userdata('ses_term');
			 $this->session->unset_userdata('ses_remarks');
			 $this->session->unset_userdata('ses_idmaster');


 }

 public function kirim_email_aprove(){

		   $this->load->library('email');
		   //konfigurasi email
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T";
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.googlemail.com';
		   $config['smtp_host']    = 'ssl://googlemail-smtp.l.google.com';

		   //-----------exchange
		   //$config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';
		   //$config['smtp_port']  = '5220';
		   $config['smtp_port']  = '465'; //port gmail
		   $config['newline']    = "\r\n";
		   $config['mailtype']   ="html" ;
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!";
		   //$config['smtp_user']= "helpdesk@eurokars.co.id";
		   //$config['smtp_pass']= "";
		   //--------------------------------

			$strponumber =$this->session->userdata("ses_idmaster");
			$str_status = '1' ; //aktif ;

			if ($strponumber != '') :
				//Select untuk email master PP----------------------------
				$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,ppn,ppn,gran_total,gran_totalppn,remarks,remark_aprov,status_send_aprove,aprove_bod,reason_reject,email_pp,email_head_pp,email_cc_pp from qv_head_pp_complite where id_master ='".$strponumber."' and status ='".$str_status."'");
			  if ($query->num_rows() > 0 ) :
			      $data_email['str_pp_master'] = $query->result();
				  foreach ($query->result() as $row_ceck) :

					 //simpan row alamat email kedalam variable..
					 	$struseremail = $row_ceck->email_pp ;
						$strheademail = $row_ceck->email_head_pp;
						$strccemail = $row_ceck->email_cc_pp ;
					 //end-----------------------------------------

				     //Select untuk email detail PP--------------------------------
				     $query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."' and status ='".$str_status."'");
				     if ($query_detail->num_rows() > 0 ) :
					    $data_email['str_pp_detail'] = $query_detail->result();
					    $data_email['intno'] = ""; //untuk counter angka table
					    $message = $this->load->view('approval_form/V_content_email_aproval',$data_email,true);
					  endif;
					  //end---------------------------------------------------------

							 if ($row_ceck->status_send_aprove =="1" and $row_ceck->aprove_bod == "1")	:
									$subject = "PP APPROVED -- Eurokargroup Company";
									$result = $this->email ;
									$this->email->initialize($config);
									$this->email->set_newline("\r\n");

									//Exchange
									//konfigurasi pengiriman

									$sender_email = "helpdesk@eurokars.co.id";
									$sender_name = "Epurchasing Notification";

									//GMAIL-----
									//$sender_email = "epurchaseeuro@gmail.com";
									//$sender_name = "Epurchasing Eurokarsgroup";					   					    								//end--------------------------

									//simpan session alamat email kedalam variable..

								   //end-----------------------------------------

									//$to = 'brian.yunanda@eurokars.co.id';

									$to = $struseremail.",".$strheademail.",".$strccemail; //email address,user,head, and CC

									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);

									//$this->email->message(print_r($message, true));

									$this->email->message($message);// tanpa array

									if ($this->email->send()) :
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
										//redirect(current_url());
									//else:
									//	show_error($this->email->print_debugger());
									endif;
								 endif;
					      endforeach;
			         endif;
			     endif;

			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent');
			 $this->session->unset_userdata('ses_grandtotal');
			 $this->session->unset_userdata('ses_term');
			 $this->session->unset_userdata('ses_remarks');
			 $this->session->unset_userdata('ses_idmaster');


 }

 public function kirim_email_to_fc(){

		   $this->load->library('email');
		   //konfigurasi email
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T";
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.googlemail.com';
		   $config['smtp_host']    = 'ssl://googlemail-smtp.l.google.com';

		   //-----------exchange
		   //$config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';
		   //$config['smtp_port']  = '5220';
		   $config['smtp_port']  = '465'; //port gmail
		   $config['newline']    = "\r\n";
		   $config['mailtype']   ="html" ;
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!";
		   //$config['smtp_user']= "helpdesk@eurokars.co.id";
		   //$config['smtp_pass']= "";
		   //--------------------------------

			$strponumber =$this->session->userdata("ses_idmaster");

			if ($strponumber != '')
			 {
				//Select untuk email master PP----------------------------
				$query =$this->db->query("select user_submission,header_desc,date_send_aproval,dept,term_top,currency,gran_total,ppn,pph,type_purchase,gran_totalppn,flag_purchase,remarks,remark_aprov from qv_head_pp_complite where id_master ='".$strponumber."'and status = '1'");
			    if ($query->num_rows() > 0 ) {
					$data_email['str_pp_master'] = $query->result();

					$query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."'and status = '1'");
					if ($query_detail->num_rows() > 0 ) {
					   $data_email['str_pp_detail'] = $query_detail->result();
					   $data_email['intno'] = ""; //untuk counter angka table

					   $message = $this->load->view('approval_form/V_content_email_to_bod_fc',$data_email,true);

					}
			   }
			}

			$result = $this->email ;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");



			$sender_email = "helpdesk@eurokars.co.id";
			$sender_name = "Epurchasing Notification";

									$result = $this->email ;
									$this->email->initialize($config);
									$this->email->set_newline("\r\n");

									$to = 'andri.widjaja@mazda.co.id' ;
									//$to = 'brian.yunanda@eurokars.co.id';
									$subject = 'submission Approval F.C';

									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);

									$this->email->message($message);// tanpa array

									if ($this->email->send()) :
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
									/*	redirect(current_url());
									else:
										show_error($this->email->print_debugger()); */
									endif;

			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent');
			 $this->session->unset_userdata('ses_grandtotal');
			 $this->session->unset_userdata('ses_term');
			 $this->session->unset_userdata('ses_remarks');
			 $this->session->unset_userdata('ses_idmaster');


 }


 public function kirim_email_to_bod(){

		   $this->load->library('email');
		   //konfigurasi email
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T";
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.googlemail.com';
		  $config['smtp_host']    = 'ssl://googlemail-smtp.l.google.com';

		   //-----------exchange
		   //$config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';
		   //$config['smtp_port']  = '5220';
		   $config['smtp_port']  = '465'; //port gmail
		   $config['newline']    = "\r\n";
		   $config['mailtype']   ="html" ;
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!";
		   //$config['smtp_user']= "helpdesk@eurokars.co.id";
		   //$config['smtp_pass']= "";
		   //--------------------------------

			$strponumber =$this->session->userdata("ses_idmaster");

			if ($strponumber != '')
			 {
				//Select untuk email master PP----------------------------
				$query =$this->db->query("select user_submission,header_desc,date_send_aproval,dept,term_top,currency,gran_total,ppn,pph,type_purchase,gran_totalppn,flag_purchase,remarks,remark_aprov from qv_head_pp_complite where id_master ='".$strponumber."'and status = '1'");
			    if ($query->num_rows() > 0 ) {
					$data_email['str_pp_master'] = $query->result();

					$query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."'and status = '1'");
					if ($query_detail->num_rows() > 0 ) {
					   $data_email['str_pp_detail'] = $query_detail->result();
					   $data_email['intno'] = ""; //untuk counter angka table

					   $message = $this->load->view('approval_form/V_content_email_to_bod_fc',$data_email,true);

					}
			   }
			}

			$result = $this->email ;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");



			$sender_email = "helpdesk@eurokars.co.id";
			$sender_name = "Epurchasing Notification";

									$result = $this->email ;
									$this->email->initialize($config);
									$this->email->set_newline("\r\n");

									$to = 'roy.arfandy@mazda.co.id' ;
									//$to = 'brian.yunanda@eurokars.co.id';
									$subject = 'submission Approval B.O.D';

									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);

									$this->email->message($message);// tanpa array

									if ($this->email->send()) :
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
									/*	redirect(current_url());
									else:
										show_error($this->email->print_debugger());*/
									endif;

			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent');
			 $this->session->unset_userdata('ses_grandtotal');
			 $this->session->unset_userdata('ses_term');
			 $this->session->unset_userdata('ses_remarks');
			 $this->session->unset_userdata('ses_idmaster');


 }




function cath_data_fordatatables(){

	     $str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase
		 $str_idcompany = $this->session->userdata('id_company');
		 $str_id_divisi =$this->session->userdata('id_divisi');
	     $str_iddept = $this->session->userdata('id_dept');
		 $str_status_send = "1";
		 $str_status_approved = "1";

		 //$str_status_waiting = "";
		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_master');
		$this->db->from('qv_head_pp_complite');
		if  ($str_flag_approval == "3" or $str_flag_approval == "0" ){
			     $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->like("id_master",$search);
				 $this->db->or_like("user_submission",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->or_like("header_desc",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->or_like("vendor",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");

			}else{
					if ($str_flag_approval == "4"){
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->like("id_master",$search);
						 $this->db->or_like("user_submission",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->or_like("header_desc",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->or_like("vendor",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");

				 }else{
					 if  ($str_flag_approval == "2"){
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");
						 $this->db->like("id_master",$search);
						 $this->db->or_like("user_submission",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");
						 $this->db->or_like("header_desc",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");
						 $this->db->or_like("vendor",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");
					 }else{
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");
							 $this->db->like("id_master",$search);
							 $this->db->or_like("user_submission",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");
							 $this->db->or_like("header_desc",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");
							 $this->db->or_like("vendor",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");
					 }
				 }
			}
		$total = $this->db->count_all_results();

		//$total=$this->db->count_all_results("qv_vendor");
		//$total=$this->db->count_all_results("qv_head_pp_complite");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
			if  ($str_flag_approval == "3" or $str_flag_approval == "0" ){
			     $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->like("id_master",$search);
				 $this->db->or_like("user_submission",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->or_like("header_desc",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->or_like("vendor",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
			}else{
					if  ($str_flag_approval == "4"){
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->like("id_master",$search);
						 $this->db->or_like("user_submission",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->or_like("header_desc",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->or_like("vendor",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
				 }else{
					 if  ($str_flag_approval == "2"){
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");
						 $this->db->like("id_master",$search);
						 $this->db->or_like("user_submission",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");
						 $this->db->or_like("header_desc",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");
						 $this->db->or_like("vendor",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");
					 }else{
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");
							 $this->db->like("id_master",$search);
							 $this->db->or_like("user_submission",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");
							 $this->db->or_like("header_desc",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");
							 $this->db->or_like("vendor",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");
					 }
				 }
			}
		}

		/*Lanjutkan pencarian ke database*/
		 $this->db->limit($length,$start);
		 if  ($str_flag_approval == "3" or $str_flag_approval == "0"){
			 $this->db->where("status_send_aprove",$str_status_send);
			 $this->db->where("id_company",$str_idcompany);
			 $this->db->where("id_dept",$str_iddept);
		 }else{
			  if  ($str_flag_approval == "4"){
				   $this->db->where("status_send_aprove",$str_status_send);
				   $this->db->where("aprove_head",$str_status_approved );
				    $this->db->where("id_divisi",$str_id_divisi);
			  }else{
				  if ($str_flag_approval == "2"){
					   $this->db->where("status_send_aprove",$str_status_send);
					   $this->db->where("aprove_bod",$str_status_approved);
				   }else{
					    $this->db->where("status_send_aprove",$str_status_send);
					    $this->db->where("aprove_fc",$str_status_approved );
				   }
			  }
		 }

		 /*Urutkan dari alphabet paling terkahir*/
		 $this->db->where("status","1");
		// $this->db->order_by('date_pp','DESC');
		 $this->db->order_by('aprove_head','ASC');
		 $this->db->order_by('aprove_bod','ASC');
		 $this->db->order_by('aprove_fc','ASC');
		 $this->db->order_by('aprove_bod','ASC');
		 $query=$this->db->get('qv_head_pp_complite');


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/

		if($search!=""){
			 if  ($str_flag_approval == "3" or $str_flag_approval == "0" ){
			     $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->like("id_master",$search);
				 $this->db->or_like("user_submission",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->or_like("header_desc",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->or_like("vendor",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");

			}else{
					if  ($str_flag_approval == "4"){
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->like("id_master",$search);
						 $this->db->or_like("user_submission",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->or_like("header_desc",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->or_like("vendor",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );
						  $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");

				 }else{
					 if  ($str_flag_approval == "2"){
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");
						 $this->db->like("id_master",$search);
						 $this->db->or_like("user_submission",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");
						 $this->db->or_like("header_desc",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");
						 $this->db->or_like("vendor",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_bod",$str_status_approved );
						 $this->db->where("status","1");

					 }else{
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");
							 $this->db->like("id_master",$search);
							 $this->db->or_like("user_submission",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");
							 $this->db->or_like("header_desc",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");
							 $this->db->or_like("vendor",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("aprove_fc",$str_status_approved );
							 $this->db->where("status","1");

					 }
				 }
			}

			$jum=$this->db->get('qv_head_pp_complite');
			$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}



		foreach ($query->result_array() as $row_tbl) {

			 if ($row_tbl['status_send_aprove'] == "-1")
			 {
				   $info_aproval = "Reject";
			 }else{
				   $info_aproval = "New Request";
			 }

			 If ($row_tbl['attach_quo'] != "")
			  {
			   $attach_quo = '<a href='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-primary">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".'View'.'</a>' ;
			 }else{
				$attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';
			 }

			 $chk_idmaster ='<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value='.$row_tbl["id_master"].' class="chkid ace" req_id_del='.$row_tbl["id_master"].' />
           <span class="lbl"></span> ';


		   $btn_view_detail = '<a href="#" class="btn btn-primary detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <span class="glyphicon glyphicon-list" aria-hidden="true"></span>Detail</a>';

		    If ($row_tbl['aprove_head'] == "1")
			 {
				$head_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
			 }else{
				  If ($row_tbl['status_send_aprove'] == "-1")
				  {
					 $head_approval = '<div style=" color:#EB293D">'."Rejected".'</div>' ;
				  }else{
					 $head_approval = '<div style=" color:#EB293D">'."Pending Approval".'</div>' ;
				  }
			 }

			  If ($row_tbl['aprove_bod'] == "1")
			  {
					$bod_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 }else{
					  If ($row_tbl['status_send_aprove'] == "-1")
					  {
						$bod_approval = '<img src="'.base_url("asset/images/reject.png").'">' ;
					  }else{
						$bod_approval = '<div style=" color:#EB293D">'."Pending Approval".'</div>' ;
					  }
			   }

			   If ($row_tbl['aprove_fc'] == "1")
			  {
					$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 }else{
					  If ($row_tbl['status_send_aprove'] == "-1")
					  {
						$fc_aproval = '<img src="'.base_url("asset/images/reject.png").'">' ;
					  }else{
						$fc_aproval = '<div style=" color:#EB293D">'."Pending Approval".'</div>' ;
					  }
			   }



 				 If ($row_tbl['aprove_presdir'] == "1")
				 {
						$presdir_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
					 }else{
						  If ($row_tbl['status_send_aprove'] == "-1")
						  {
							$presdir_approval =  '<img src="'.base_url("asset/images/reject.png").'">' ;
						  }else{
							$presdir_approval = '<div style=" color:#EB293D">'."Pending Approval".'</div>' ;
						  }
				  }


 $presdir_approval = $row_tbl["aprove_presdir"] ;
 $strbodaprove = $row_tbl["aprove_bod"] ;
 $strfcaprove   = $row_tbl["aprove_fc"] ;
 $strheadaprove = $row_tbl["aprove_head"] ;


 if ($this->session->userdata("aproval_flag")== "3" and $row_tbl["aprove_head"]=="1" ) {
	 $str_reject =  'Has been Aproved head' ;
 }else{
   if ($this->session->userdata("aproval_flag")== "4" and $row_tbl["aprove_bod"]=="1" ) {
	   $str_reject =  'Has been Aproved B.O.D' ;
   }else{
	 if ($this->session->userdata("aproval_flag")== "2" and $row_tbl["aprove_fc"]=="1" ) {
		  $str_reject =  'Has been Aproved fc' ;
 	 }else{
		if ($this->session->userdata("aproval_flag") == "1" and $row_tbl["aprove_bod"] ) {
			/* $str_reject = '<a href="#" class="btn btn-danger btn-setting reject" id="reject" style="text-decoration:none" req_id='.$row_tbl["id_master"].'><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>Reject</a>
			'; */
			 $str_reject =  'Has been Aproved All' ;
		}else{
			  $str_reject =  '<a href="#" class="btn btn-danger btn-setting reject" id="reject" style="text-decoration:none" req_id='.$row_tbl["id_master"].'><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>Reject</a>
								';
				/*if ($this->session->userdata("aproval_flag")== "3" or $this->session->userdata("aproval_flag")== "4"  or $this->session->userdata("aproval_flag")== "2" or $this->session->userdata("aproval_flag")== "0") {
				   if ($strbodaprove == "1" and $strfcaprove == "1" and $strpurchaseaprove == "1" and $strheadaprove == "1" ) {
					   $str_reject =  'Has been Aproved All' ;
				   }else{
					  $str_reject =  '<a href="#" class="btn btn-danger btn-setting reject" id="reject" style="text-decoration:none" req_id='.$row_tbl["id_master"].'><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>Reject</a>
								';

				   }
			   }	*/
		   }
	   }
     }
  }


		if($row_tbl['date_aprove_head'] ===NULL || $row_tbl['date_aprove_head'] === '0000-00-00 00:00:00') {
		    $datehead =" ";
		}else{
			$datehead =  date('d-m-Y h:i:sa', strtotime($row_tbl['date_aprove_head']));
		}
		if($row_tbl['date_aprove_bod'] ===NULL || $row_tbl['date_aprove_bod']==='0000-00-00 00:00:00') {
		   $datebod =" ";
		}else{
		   $datebod  = date('d-m-Y h:i:sa', strtotime($row_tbl['date_aprove_bod']));
		}

	    if($row_tbl['date_aprove_fc'] ===NULL || $row_tbl['date_aprove_fc']==='0000-00-00 00:00:00' ) {
		   $datefc =" ";
	    }else{
		   $datefc   = date('d-m-Y h:i:sa', strtotime($row_tbl['date_aprove_fc']));
		}

		if($row_tbl['date_aprove_presdir'] ===NULL  || $row_tbl['date_aprove_presdir']==='0000-00-00 00:00:00') {
		  $presdir =" ";
	    }else{
		  $presdir  = date('d-m-Y h:i:sa', strtotime($row_tbl['date_aprove_presdir']));
		}



			$output['data'][]=array(
			                        $chk_idmaster,
									$row_tbl['id_master'],
									$head_approval.$datehead,
									$bod_approval.$datebod,
									$fc_aproval.$datefc,
									$presdir_approval.$presdir,
									$row_tbl['short'],
									$row_tbl['dept'],
									$row_tbl['vendor'],
									$row_tbl['header_desc'],
									substr($row_tbl['remarks'],0,25),
									date('d-m-Y', strtotime($row_tbl['date_pp'])),
									$row_tbl['currency'],
									$row_tbl['type_purchase'],
									//$row_tbl['nomor_coa'],
									number_format($row_tbl['gran_total'],2,'.',','),
									number_format($row_tbl['ppn'],2,'.',','),
									number_format($row_tbl['gran_totalppn'],2,'.',','),
									$row_tbl['term_top'],
									$row_tbl['user_submission'],
									$attach_quo,
									$btn_view_detail,
									$str_reject
							  );
		}
		echo json_encode($output);
	}

	public function disconnect()
	{
		$ddate = date('d F Y');
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user

		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');

  }

}
