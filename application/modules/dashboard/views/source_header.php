

<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css')?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/4.5.0/css/font-awesome.min.css') ?>" />

<!-- page specific plugin styles -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery-ui.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery-ui.custom.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/chosen.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker3.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-timepicker.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/daterangepicker.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-colorpicker.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/fonts.googleapis.com.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/buttons.dataTables.min.css'); ?>"> 
<link rel="stylesheet" href="<?php echo base_url('assets/css/fullcalendar.min.css'); ?>" />


  

<!--select2 combo box css --->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<!--end select2 combo box css --->


<link rel="stylesheet" href="<?php echo base_url('assets/css/ace.min.css') ?>" class="ace-main-stylesheet" id="main-ace-style" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/ace-skins.min.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/ace-rtl.min.css') ?>" />


<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>" />
<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css')?>" />  

<!--Choosen combo box css --->
<link rel="stylesheet" href="<?php echo base_url('assets/css/prism.css'); ?>"> 
<link rel="stylesheet" href="<?php echo base_url('assets/css/chosen.css'); ?>"> 
<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-multiselect.min.css') ?>" />
<!--end css --->    
         
<script type="text/javascript" src="<?php echo base_url()?>assets/js/ace-extra.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-2.1.4.min.js" charset="UTF-8"></script> 
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.2.1.min.js');?>" charset="UTF-8"  ></script>



<!--Choosen combo box js --->
<script type="text/javascript" src="<?php echo base_url('assets/js/chosen.jquery.js');?>"></script> 
<script  type="text/javascript" src="<?php echo base_url('assets/js/docsupport/prism.js');?>" ></script>
<script  type="text/javascript" src="<?php echo base_url('assets/js/docsupport/init.js');?>"></script>
<!--Chosen combo box end --->

<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-ui-1.12.1.js" charset="UTF-8"></script> 
    
<script type="text/javascript">
if('ontouchstart' in document.documentElement) 
       document.write("<script src='<?php echo base_url()?>assets/js/jquery.mobile.custom.min.js' charset='UTF-8'>"+"<"+"/script>");
</script>	    

<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ace-elements.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/ace.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js');?>"  ></script>	
<script type="text/javascript" src="<?php echo base_url('assets/js/dataTables.scroller.min.js');?>"  ></script>	
		
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>	
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.form.min.js');?>" ></script>	

<script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap-datepicker.min.js');?>" ></script>	
<script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap-timepicker.min.js');?>" ></script>	
<script type="text/javascript" src="<?php echo base_url('asset/js/daterangepicker.min.js');?>" ></script>	
<script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap-datetimepicker.min.js');?>" ></script>	



<!--select2 combo box js --->
<script type="text/javascript" src="<?php echo base_url('assets/select2/select2.min.js');?>" charset="UTF-8"  ></script>
<!--end select2 combo box js --->

<!------gruter msgbox ---------->
 <script  src="<?php echo base_url('assets/js/jquery.gritter.min.js'); ?>"></script>
<!------gruter msgbox ---------->

<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>		

<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.32/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.32/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>

<style>
.grad_header{
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#6db3f2+0,54a3ee+50,3690f0+51,1e69de+100;Blue+Gloss+%233 */
background: rgb(109,179,242); /* Old browsers */
background: -moz-linear-gradient(top, rgba(109,179,242,1) 0%, rgba(84,163,238,1) 50%, rgba(54,144,240,1) 51%, rgba(30,105,222,1) 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, rgba(109,179,242,1) 0%,rgba(84,163,238,1) 50%,rgba(54,144,240,1) 51%,rgba(30,105,222,1) 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, rgba(109,179,242,1) 0%,rgba(84,163,238,1) 50%,rgba(54,144,240,1) 51%,rgba(30,105,222,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#6db3f2', endColorstr='#1e69de',GradientType=0 ); /* IE6-9 */
}

.grad_log {
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#f2f6f8+0,d8e1e7+50,b5c6d0+51,e0eff9+100;Grey+Gloss+%232 */
background: rgb(242,246,248); /* Old browsers */
background: -moz-linear-gradient(top, rgba(242,246,248,1) 0%, rgba(216,225,231,1) 50%, rgba(181,198,208,1) 51%, rgba(224,239,249,1) 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, rgba(242,246,248,1) 0%,rgba(216,225,231,1) 50%,rgba(181,198,208,1) 51%,rgba(224,239,249,1) 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, rgba(242,246,248,1) 0%,rgba(216,225,231,1) 50%,rgba(181,198,208,1) 51%,rgba(224,239,249,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f6f8', endColorstr='#e0eff9',GradientType=0 ); /* IE6-9 */
}
</style>  

<style>
.grad_navi{
background: #ffffff;
background: -moz-linear-gradient(top, #ffffff 0%, #f1f1f1 50%, #e1e1e1 51%, #f6f6f6 100%); 
background: -webkit-linear-gradient(top, #ffffff 0%,#f1f1f1 50%,#e1e1e1 51%,#f6f6f6 100%); 
background: linear-gradient(to bottom, #ffffff 0%,#f1f1f1 50%,#e1e1e1 51%,#f6f6f6 100%); Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f6f6f6',GradientType=0 ); /* IE6-9 */			
}


.grad_sub{
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#cedbe9+0,aac5de+17,6199c7+50,3a84c3+51,419ad6+59,4bb8f0+71,3a8bc2+84,26558b+100;Blue+Gloss */
background: rgb(206,219,233); /* Old browsers */
background: -moz-linear-gradient(top, rgba(206,219,233,1) 0%, rgba(170,197,222,1) 17%, rgba(97,153,199,1) 50%, rgba(58,132,195,1) 51%, rgba(65,154,214,1) 59%, rgba(75,184,240,1) 71%, rgba(58,139,194,1) 84%, rgba(38,85,139,1) 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, rgba(206,219,233,1) 0%,rgba(170,197,222,1) 17%,rgba(97,153,199,1) 50%,rgba(58,132,195,1) 51%,rgba(65,154,214,1) 59%,rgba(75,184,240,1) 71%,rgba(58,139,194,1) 84%,rgba(38,85,139,1) 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, rgba(206,219,233,1) 0%,rgba(170,197,222,1) 17%,rgba(97,153,199,1) 50%,rgba(58,132,195,1) 51%,rgba(65,154,214,1) 59%,rgba(75,184,240,1) 71%,rgba(58,139,194,1) 84%,rgba(38,85,139,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#cedbe9', endColorstr='#26558b',GradientType=0 ); /* IE6-9 */
}
</style>