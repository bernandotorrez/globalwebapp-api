<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Webapp Eurokarsgroup</title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="shortcut icon" href="<?php echo base_url('asset/images/mazda.ico');?>" >
 
 		<?php $this->load->view('source_header'); ?>      
	</head>

	<body class="no-skin" >
		<div id="navbar" class="navbar navbar-default navbar-collapse h-navbar ace-save-state grad_header">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<div class="navbar-header pull-left" >
					<img src="<?php echo base_url('assets/images/logo/logonew.png'); ?>" />
					 
					

					<button class="pull-right navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#sidebar">
						<span class="sr-only">Toggle sidebar</span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>
					</button>
				</div>
               

				<div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse grad_header" role="navigation">
					<ul class="nav ace-nav">
                       <li>
                        <a class="btn-success">
                         <label  style="font-weight:bold;">
                         Username :  <?php echo $this->session->userdata("username") ?> </label>
                          </a>
                       </li> 
						<li class="light-blue dropdown-modal user-min">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle btn-danger">								
								<span class="user-info">
									<small>Welcome,</small>
									<?php echo $this->session->userdata('name'); ?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>
							
							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#">
										<i class="ace-icon fa fa-cog"></i>
										Settings
									</a>
								</li>

								<li>
									<a href="profile.html">
										<i class="ace-icon fa fa-user"></i>
										Profile
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="#">
										<i class="ace-icon fa fa-power-off"></i>                                      
										<?php echo anchor('login/logout','Log Out'); ?>
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>

				
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

			<!--	<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large" hidden="true">
					<small>Welcome,</small> -->
							<!-- ?>php echo $this->session->userdata('name'); ?> -->
				<!--	</div>
				</div>< /.sidebar-shortcuts -->
                
                 <ul class="nav nav-list "><!-- navigasi view -->
				     <?php $this->load->view('dashboard/navigasi')	; ?>
                 </ul><!-- /.nav-list -->
                        
			</div>
            <div class="table-header">
			</div>
					
		<!--<div class="main-content " > -->
				<div class="main-content-inner">
					<div class="page-content">													
						<div class="row" >
							
								<!-- PAGE CONTENT BEGINS -->                 
									  <?php  $this->load->view($show_view); ?>
								<!-- PAGE CONTENT ENDS -->
							
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
		<!--	</div><!-- /.main-content -->

			<div class="footer btn-info">
				<div class="footer-inner ">
					<div class="widget-header widget-header-small ">
                      <h6 class="widget-title">
                			<span class="blue bolder">Copyright I.T Team Eurokars Group Indonesia</span>
                 			<?php echo "@"." ".date("Y"); ?>
                       </h6>
					</div>								
				</div>				
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="assets/js/jquery-2.1.4.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->
		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
			 var $sidebar = $('.sidebar').eq(0);
			 if( !$sidebar.hasClass('h-sidebar') ) return;
			
			 $(document).on('settings.ace.top_menu' , function(ev, event_name, fixed) {
				if( event_name !== 'sidebar_fixed' ) return;
			
				var sidebar = $sidebar.get(0);
				var $window = $(window);
			
				//return if sidebar is not fixed or in mobile view mode
				var sidebar_vars = $sidebar.ace_sidebar('vars');
				if( !fixed || ( sidebar_vars['mobile_view'] || sidebar_vars['collapsible'] ) ) {
					$sidebar.removeClass('lower-highlight');
					//restore original, default marginTop
					sidebar.style.marginTop = '';
			
					$window.off('scroll.ace.top_menu')
					return;
				}
			
			
				 var done = false;
				 $window.on('scroll.ace.top_menu', function(e) {
			
					var scroll = $window.scrollTop();
					scroll = parseInt(scroll / 4);//move the menu up 1px for every 4px of document scrolling
					if (scroll > 17) scroll = 17;
			
			
					if (scroll > 16) {			
						if(!done) {
							$sidebar.addClass('lower-highlight');
							done = true;
						}
					}
					else {
						if(done) {
							$sidebar.removeClass('lower-highlight');
							done = false;
						}
					}
			
					sidebar.style['marginTop'] = (17-scroll)+'px';
				 }).triggerHandler('scroll.ace.top_menu');
			
			 }).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			
			 $(window).on('resize.ace.top_menu', function() {
				$(document).triggerHandler('settings.ace.top_menu', ['sidebar_fixed' , $sidebar.hasClass('sidebar-fixed')]);
			 });
			
			
			});
		</script>
	</body>
</html>
