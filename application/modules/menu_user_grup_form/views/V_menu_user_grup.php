<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<style>
/* for autocomplitee */
.ui-autocomplete {
    max-height: 200px;
    overflow-y: auto;    
    overflow-x: hidden;   
    padding-right: 20px;
}

* html .ui-autocomplete {
    height: 200px;
	
}

* html .ui-autocomplete-loading {
background:url('<?php echo base_url('asset/images/loading.gif') ?>') no-repeat right center ;
}
</style>
<div>
		<button onclick="add_menu_user_grup()" class="btn btn-app btn-primary btn-xs radius-4 caddnew" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
        Add
		</button>
		<button onclick="delete_menu_user_grup()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit" id="deleteTriger" name="deleteTriger"  value ="btndel"  disabled="disabled" >
      <i class="ace-icon fa fa-ban bigger-160"></i>
        Delete
		</button>
		<button onclick="edit_menu_user_grup()" class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="editTriger"  name="editTriger"  value ="btnedit"  disabled="disabled" >
      <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
        Edit
		</button> 
</div>
<br>
<div class="table-header btn-info"> <?php echo " ".$header ;?>   </div>  
	<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">    

	<div class="form-group col-xs-6">
        <div class="col-xs-10">
            <div class="form-inline ">
                <div class="form-group">Date start</div>
                
                <div class="form-group">
                    <div class="input-group date">
                       <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                       </div>
                       
                       <input type="text" class="form-control input-daterange" id="start_date" name="start_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                    </div> <!-- /.input-group datep -->       
                </div>
               
               <div class="form-group">
                       <label for="From" class="col-xs-1 control-label">To</label>
               </div>
              
               <div class="form-group">
                      <div class="input-group date">
                               <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                               </div>
                                     <input type="text" class="form-control input-daterange" id="end_date" name="end_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                       </div>  
               </div>
                
            </div>
        </div>
      </div>

		<table id="myTable" cellpadding="0" cellspacing="0" width="100%"  class="table table-striped table-bordered table-hover">
			<thead class="text-warning" > 
		<th width="5%" style="text-align:center">
		  <label class="pos-rel">
            <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
            <span class="lbl"></span>
          </label>
        </th>
		<th>No</th>
	    <th>Id</th>
	    <th>Group</th>
		<th>Parent</th>
		<th>Child</th>
		<th>Subchild</th>
		<th>Can View</th>
		<th>Can Add</th>
		<th>Can Edit</th>
		<th>Can Delete</th>
		<th>Date Create</th>
	    </thead>
    </table> 
</div>  
				
                                 	                       
<!-- Bootstrap modal -->
<form action="#" id="form" class="form-horizontal">
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Menu User Group Form</h3>
      </div>
      <div class="modal-body form">
          <input type="hidden" value="" name="id_menu_user_group"/>
          <div class="form-body" style="padding-top:10px">
            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">ID</label>
              <div class="col-md-8">
				<?php
				$totaldata = $total[0]->totaldata+1;
				?>
                <input name="id_menu_user_group" placeholder="Id_Menu_User_Group" class="col-md-9" type="text" readonly value="<?php echo $totaldata ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Name Group</label>
              <div class="col-md-8">
			    <select class="col-md-9" name="id_group" id="id_group">
                <option  value="">Choose Group</option> 
				<?php foreach($group as $row) { ?>
                <option value="<?php echo $row->id_group;?>"><?php echo $row->name_grp;?>
				</option>
				<?php } ?>				
				</select>    
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">ID Parent</label>
              <div class="col-md-8">
			   <select class="col-md-9" name="id_parent" id="id_parent">
                <option  value="">Choose Parent</option> 
				<?php foreach($parent as $row) { ?>
                <option value="<?php echo $row->id_parent;?>"><?php echo $row->parent_menu_desc;?>
				</option>
				<?php } ?>				
				</select>    
              </div>
            </div>
			<div class="form-group">
			<label class="control-label col-md-4" style="padding-right:10px">Child</label>
             <div class="col-md-8">
				 <select class="col-md-9" name="id_child" id="id_child">
						<option  value="">Choose Child</option>
						<?php foreach($parent2 as $bbb) { 
						$sqlpo	  = "select * from tbl_child_menu where id_parent='".$bbb->id_parent."'";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo 	  = $qrypo->result();					
						foreach($adapo as $aaa) { ?>
						<option value="<?php echo $aaa->id_child;?>"><?php echo $aaa->desc_child;?>
						</option>
						<?php } }?>	
				 </select>    
            </div>
            </div>
			<div class="form-group">
			<label class="control-label col-md-4"  style="padding-right:10px">Subchild</label>
             <div class="col-md-8">
				<select class="col-md-9" name="id_subchild" id="id_subchild">
						<option  value="">Choose Sub Child</option>
						<?php foreach($child as $nnn) { 
						$sqlpo	  = "select * from tbl_sub_child_menu where id_child='".$nnn->id_child."'";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo1   = $qrypo->result();					
						foreach($adapo1 as $mmm) { ?>
						<option value="<?php echo $mmm->id_subchild;?>"><?php echo $mmm->desc_sub;?>
						</option>
						<?php } }?>	
				 </select> 
            </div>
            </div>			
			<div class="form-group">
            <label class="control-label col-md-4" style="padding-right:10px">View</label>
             <div class="col-md-8">
				<select class="col-md-9" name="can_view" id="can_view">
				  <option value="1">Yes</option>
				  <option value="0">No</option>
				</select>
             </div>
            </div>
			<div class="form-group">
            <label class="control-label col-md-4" style="padding-right:10px">Add</label>
             <div class="col-md-8">
				<select class="col-md-9" name="can_add" id="can_add">
				  <option value="1">Yes</option>
				  <option value="0">No</option>
				</select>
             </div>
            </div>
			<div class="form-group">
            <label class="control-label col-md-4" style="padding-right:10px">Edit</label>
             <div class="col-md-8">
				<select class="col-md-9" name="can_edit" id="can_edit">
				  <option value="1">Yes</option>
				  <option value="0">No</option>
				</select>
             </div>
            </div>
			<div class="form-group">
            <label class="control-label col-md-4" style="padding-right:10px">Delete</label>
             <div class="col-md-8">
				<select class="col-md-9" name="can_del" id="can_del">
				  <option value="1">Yes</option>
				  <option value="0">No</option>
				</select>
             </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
</form>
  <!-- End Bootstrap modal -->
</body>
  <script>


//$('#myTable').dataTable();
//-----------------------------------------data table custome----
// $(document).ready(function () {
// var rows_selected = [];
// var tableUsers = $('#myTable').DataTable({
// 		"destroy" : 'true',
//         "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
// 		"dom": 'lfBrtip',
// 		"buttons": [
//             {
// 		   "extend":    'copy',
// 		   "text":      '<i class="fa fa-files-o"></i> Copy',
// 		   "titleAttr": 'Copy'
// 		   },
// 		   {
// 		   "extend":    'print',
// 		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
// 		   "titleAttr": 'Print',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   },
// 		   {
// 		   "extend":    'excel',
// 		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
// 		   "titleAttr": 'Excel'
// 		   },
// 		   {
// 		   "extend":    'pdf',
// 		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
// 		   "titleAttr": 'PDF',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   }
//         ],
// 		"autoWidth" : false,
// 		"scrollY" : '250',
// 		"scrollX" : true,
//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.

//         // Load data for the table's content from an Ajax source
//         "ajax": {
//             "url": "<?php echo base_url('menu_user_grup_form/C_menu_user_grup/ajax_list') ?>",
//             "type": "POST"
//         },
// 		'columnDefs': [
// 		{
//         'targets': [0],
// 		'orderable': false,
// 		}
//    ],

//       'order': [[2, 'desc']],
//         });	
// });	
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
	if($('#checkAll').not(':checked').length){
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
	}
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>

<!-- Datatable -->
<script>
$(document).ready(function(){ 
		$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data('no'); //
	
		function fetch_data(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
				"ajax":{
				    url: "<?php echo base_url('menu_user_grup_form/C_menu_user_grup/ajax_list') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
				 
				 'columnDefs': [
							{
							'targets': [0],
							'orderable': false,
							}
					],

						'order': [[2, 'desc']],
			 });
		  }
		  	  
		  
//proses change date-----------------------------------------------
		$('#start_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
		$('#end_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);		
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	
 
 
 
 //onblur date-------------------------------------------------------------------------
 
 		// $('#start_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
		
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		  fetch_data('no', start_date, end_date);
		// 	} 
			
		// });
 
 
 	
		// $('#end_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
			
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		 fetch_data('no', start_date, end_date);
		// 	} 
		// });
 //end onblur-------------------------------------	

  }); //end document on ready	
		
</script>

<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>

<script>
        	$(document).ready(function(){
	            $("#id_parent").change(function (){
	                var url = "<?php echo site_url('menu_user_grup_form/C_menu_user_grup/pos_parent');?>/"+$(this).val();
	                $('#id_child').load(url);
	                return false;
	           })
	  
				$("#id_child").change(function (){
	                var url = "<?php echo site_url('menu_user_grup_form/C_menu_user_grup/pos_child');?>/"+$(this).val();
	                $('#id_subchild').load(url);
	                return false;
	            })
	        });
    	</script>

	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
    function add_menu_user_grup()
    {
      save_method = 'add';
      $('#modal_form').modal('show');; // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
	
	function edit_menu_user_grup(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('menu_user_grup_form/C_menu_user_grup/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_menu_user_group"]').val(data.id_menu_user_group);
            $('[name="id_group"]').val(data.id_group);
			$('[name="id_parent"]').val(data.id_parent);
			$('[name="id_child"]').val(data.id_child);
			$('[name="id_subchild"]').val(data.id_subchild);
			$('[name="can_view"]').val(data.can_view);
			$('[name="can_add"]').val(data.can_add);
			$('[name="can_edit"]').val(data.can_edit);
			$('[name="can_del"]').val(data.can_del);
            
            $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Menu User Group'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



     function save()
    {
      var url;
      if(save_method == 'add')
      {
		  url = "<?php echo site_url('menu_user_grup_form/C_menu_user_grup/add_menu_user_grup')?>";
      }
      else
      {
		   url = "<?php echo site_url('menu_user_grup_form/C_menu_user_grup/update_menu_user_grup')?>";
      }
		  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }

     function delete_menu_user_grup()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('menu_user_grup_form/C_menu_user_grup/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
			
		

      
    }
	
  </script>

  

</html>
