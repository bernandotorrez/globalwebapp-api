<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_company extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_company');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_company->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}	
	 	}
		
	public function index()
		{

		$data['company']=$this->M_company->get_all_company();
		$data['com']=$this->M_company->get_all_com();
		$data['brand']=$this->M_company->get_all_brand();
		$data['data']=$this->M_company->get_all_data();
		$data['total']=$this->M_company->get_count_id();
		$data['show_view'] = 'company_form/V_company';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function ajax_list()
	{
		$list = $this->M_company->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_company) {
			$chk_idmaster ='<div align="center"><input id="checkuser" name="checkuser" type="checkbox" value='.$tbl_company->id_company.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_company->id_company;
			$row[] = $tbl_company->company;
			$row[] = $tbl_company->short;
			$row[] = $tbl_company->date_create;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_company->count_all(),
						"recordsFiltered" => $this->M_company->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
		public function ajax_list2()
	{
		$list = $this->M_company->get_datatables2();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $qv_det_company) {
			$chk_idmaster ='<div align="center"><input id="checkuser" name="checkuser" type="checkbox" value='.$qv_det_company->id_det_com.' class="editRow2 ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $qv_det_company->id_det_com;
			$row[] = $qv_det_company->company;
			$row[] = $qv_det_company->short;
			$row[] = $qv_det_company->brand_name;
			$row[] = $qv_det_company->date_create_company_det;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_company->count_all2(),
						"recordsFiltered" => $this->M_company->count_filtered2(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_company');
		$this->db->from('tbl_company');
		$this->db->where('status','1');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_company",$search);
		$this->db->where('status','1');
		$this->db->or_like("company",$search);
		$this->db->where('status','1');
		$this->db->or_like("short",$search);
		$this->db->where('status','1');
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		$this->db->where('status','1');
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_company','ASC');
		$query=$this->db->get('tbl_company');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("id_company",$search);
		$jum=$this->db->get('tbl_company');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $tbl_company) {
		$chk_idmaster ='<div align="center"><input id="checkcompany" name="checkcompany" type="checkbox" value='.$tbl_company["id_company"].' class="editRow ace" req_id_del='.$tbl_company["id_company"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($tbl_company['id_company'],$tbl_company['company'],
									$tbl_company['short'],
									$chk_idmaster
									);

		}

		echo json_encode($output);


	}
	
	function ambil_data2(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_det_com');
		$this->db->from('qv_det_company');
		$this->db->where('status','1');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_det_com",$search);
		$this->db->where('status','1');
		$this->db->or_like("company",$search);
		$this->db->where('status','1');
		$this->db->or_like("short",$search);
		$this->db->where('status','1');
		$this->db->or_like("brand_name",$search);
		$this->db->where('status','1');
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		$this->db->where('status','1');
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_det_com','ASC');
		$query=$this->db->get('qv_det_company');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("id_det_com",$search);
		$jum=$this->db->get('qv_det_company');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $qv_det_company) {
		$chk_idmaster2 ='<div align="center"><input id="checkdetcompany" name="checkdetcompany" type="checkbox" value='.$qv_det_company["id_det_com"].' class="editRow2 ace" req_id_del='.$qv_det_company["id_det_com"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($qv_det_company['id_det_com'],$qv_det_company['company'],
									$qv_det_company['short'],$qv_det_company['brand_name'],
									$chk_idmaster2
									);

		}

		echo json_encode($output);


	}
	
	
	public function add_company()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_add = date('Y-m-d H:i:s');

		if(empty($_POST["company"])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_POST["short"])){
			die ("Field Short must be filled in!! ");
		}
		else{
			$data = array(
					'id_company' => $this->input->post('id_company'),
					'company' => $this->input->post('company'),
					'short' => $this->input->post('short'),
					'status' => '1',
					'date_create' => $date_add,
					'remarks' => $remarks_add
				);
			$insert = $this->M_company->add_company($data);
		  }
		  echo 'Insert';
		}
		
	public function add_company2()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_add = date('Y-m-d H:i:s');

		if(empty($_POST["company2"])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_POST["brand2"])){
			die ("Field Brand must be filled in!! ");
		}
		else {
			$company = $this->input->post('company2');
			$brand = $this->input->post ('brand2');
			
			$query = $this->db->query("select * from tbl_company_det where id_company = '".$company."' and id_brand = '".$brand."'");
			
			if ($query->num_rows()==1) {
				die ("Data Already Exist");
			
			}
			else{
			$data = array(
					'id_company' => $company,
					'id_brand' => $brand,
					'status' => '1',
					'date_create' => $date_add,
					'remarks' => $remarks_add
				);
			$this->db->insert('tbl_company_det', $data);
			}
			}
			echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_company->get_by_id($id);
			echo json_encode($data);
		}
		
	public function ajax_edit2($id)
		{
			$data = $this->M_company->get_by_id2($id);
			echo json_encode($data);
		}

	public function update_company()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_update = date('Y-m-d H:i:s');

		if(empty($_POST["company"])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_POST["short"])){
			die ("Field Short must be filled in!! ");
		}
		else {
		$data = array(
					'company' => $this->input->post('company'),
					'short' => $this->input->post('short'),
					'status' => '1',
					'date_update' => $date_update,
					'remarks' => $remarks_update
			);
		$this->M_company->update_company(array('id_company' => $this->input->post('id_company')), $data);
		}
	    echo 'Insert';
	}
	
	public function update_company2()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_update = date('Y-m-d H:i:s');

		if(empty($_POST["company2"])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_POST["brand2"])){
			die ("Field Brand must be filled in!! ");
		}
		else {
			$id_det_com = $this->input->post('id_det_com');
			$company = $this->input->post('company2');
			$brand = $this->input->post ('brand2');
			
			$query = $this->db->query("select * from tbl_company_det where id_company = '".$company."' and id_brand = '".$brand."'");
			if ($query->num_rows()==1) {
				die ("Data Already Exist");
			
			}
			else{
			$data = array(
					'id_company' => $company,
					'id_brand' => $brand,
					'status' => '1',
					'date_update' => $date_update,
					'remarks' => $remarks_update
				);
		
		$this->M_company->update_company2(array('id_det_com' => $this->input->post('id_det_com')), $data);
		}
	}
	  echo 'Insert';
}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_delete = 'Action : Delete | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_delete = date('Y-m-d H:i:s');

		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
						'date_update' => $date_delete,
						'remarks' => $remarks_delete
				);
				$this->db->where('id_company', $id);
				$this->db->update('tbl_company', $data);
			}
		}
	}
	
    public function deletedata2 (){
		$data_ids = $_REQUEST['ID'];

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_delete = 'Action : Delete | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_delete = date('Y-m-d H:i:s');

		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
						'date_update' => $date_delete,
						'remarks' => $remarks_delete
				);
				$this->db->where('id_det_com', $id);
				$this->db->update('tbl_company_det', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}




}
