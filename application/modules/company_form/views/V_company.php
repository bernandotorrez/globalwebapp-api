<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<div>
		<button onclick="add_company()" class="btn btn-app btn-primary btn-xs radius-4 caddnew" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
        Add
		</button>
		<button onclick="delete_company()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit" id="deleteTriger" name="deleteTriger"  value ="btndel"  disabled="disabled" >
      <i class="ace-icon fa fa-ban bigger-160"></i>
        Delete
		</button>
		<button onclick="edit_company()" class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="editTriger"  name="editTriger"  value ="btnedit"  disabled="disabled" >
      <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
        Edit
		</button> 
</div>
<br/>
<div class="table-header btn-info"> <?php echo " ".$header ;?> </div>
<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">

	<div class="form-group col-xs-6">
		<div class="col-xs-10">
			<div class="form-inline ">
				<div class="form-group">Date start</div>

				<div class="form-group">
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>

						<input type="text" class="form-control input-daterange" id="start_date_company"
							name="start_date" data-date-format="dd-mm-yyyy" autocomplete="off">
					</div> <!-- /.input-group datep -->
				</div>

				<div class="form-group">
					<label for="From" class="col-xs-1 control-label">To</label>
				</div>

				<div class="form-group">
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="text" class="form-control input-daterange" id="end_date_company" name="end_date"
							data-date-format="dd-mm-yyyy" autocomplete="off">
					</div>
				</div>

			</div>
		</div>
	</div>

	<table id="myTable" cellpadding="0" cellspacing="0" width="100%"
		class="table table-striped table-bordered table-hover">
		<thead class="text-warning">
			<th width="5%" style="text-align:center">
				<label class="pos-rel">
					<input type="checkbox" class="ace ace-checkbox-1" id="checkAll" />
					<span class="lbl"></span>
				</label>
			</th>
			<th>No</th>
			<th>Id</th>
			<th>Company</th>
			<th>Short</th>
			<th>Date Create</th>
		</thead>
	</table>
</div>
</div>
<br />
<div>
		<button onclick="add_company2()" class="btn btn-app btn-primary btn-xs radius-4 caddnew" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
      	Add
		</button>
		<button onclick="delete_company2()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit" id="deleteTriger2" name="deleteTriger2"  value ="btndel"  disabled="disabled" >
      <i class="ace-icon fa fa-ban bigger-160"></i>
        Delete
		</button>
		<button onclick="edit_company2()" class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="editTriger2"  name="editTriger2"  value ="btnedit"  disabled="disabled" >
      <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
      	Edit
		</button> 
</div>									
<br/>
<div class="table-header btn-info"> <?php echo " ".$header ;?> </div>
<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">

<div class="form-group col-xs-6">
		<div class="col-xs-10">
			<div class="form-inline ">
				<div class="form-group">Date start</div>

				<div class="form-group">
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>

						<input type="text" class="form-control input-daterange" id="start_date_company_det"
							name="start_date" data-date-format="dd-mm-yyyy" autocomplete="off">
					</div> <!-- /.input-group datep -->
				</div>

				<div class="form-group">
					<label for="From" class="col-xs-1 control-label">To</label>
				</div>

				<div class="form-group">
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="text" class="form-control input-daterange" id="end_date_company_det" name="end_date"
							data-date-format="dd-mm-yyyy" autocomplete="off">
					</div>
				</div>

			</div>
		</div>
	</div>

	<table id="myTable2" cellpadding="0" cellspacing="0" width="100%"
		class="table table-striped table-bordered table-hover">
		<thead class="text-warning">
			<th width="5%" style="text-align:center">
				<label class="pos-rel">
					<input type="checkbox" class="ace ace-checkbox-1" id="checkAll" />
					<span class="lbl"></span>
				</label>
			</th>
			<th>No</th>
			<th>Id Company Detail</th>
			<th>Company</th>
			<th>Short</th>
			<th>Brand Name</th>
			<th>Date Create</th>
		</thead>
	</table>
</div>
</div>
									
									 
<!-- Bootstrap modal -->
<form action="#" id="form" class="form-horizontal">
	<div class="modal fade" id="modal_form" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Company Form</h3>
      </div>
    <div class="modal-body form">
			<div class="widget-body">
				<div class="widget-main">
          <input type="hidden" value="" name="id_company"/>
					<div>
						<label for="id_company">ID Company</label>
						<?php
							$totaldata = $total[0]->totaldata+1;
						?>
            <input class="form-control" id="id_company" name="id_company" placeholder="Id_Company" type="text" readonly value="<?php echo $totaldata ?>"/>
					</div>
					<div>
						<label for="company">Name Company</label>
            <input name="company" placeholder="Autorama Euroasia" class="form-control" id="company" type="text"/>
					</div>
					<div>
						<label for="short">Short</label>
            <input name="short" placeholder="ARE" class="form-control" id="short" type="text"/>
					</div>
		  	</div>
			</div>
		</div>
          <div class="modal-footer">
							<button onclick="save()" id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
								<i class="ace-icon fa fa-floppy-o bigger-160"></i>
									Save
							</button>
							<button class="btn btn-app btn-danger btn-xs radius-4" type="submit" data-dismiss="modal">
								<i class="ace-icon fa fa-close bigger-160"></i>
									Cancel
							</button>
          </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  <!-- Bootstrap modal Detail -->
<div class="modal fade" id="modal_form2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Detail Company</h3>
      </div>
    <div class="modal-body form">
			<div class="widget-body">
				<div class="widget-main">
          <input type="hidden" value="" name="id_det_com"/>
					<div>
              <label for="form-field-select-3">Company</label>
			    		<select class="myselect form-control" name="company2" id="company2" tabindex="2" style="width: 100%;">
                <option  value="">Choose Company</option> 
										<?php foreach($com as $aaa) { ?>
                <option value="<?php echo $aaa->id_company;?>"><?php echo $aaa->company;?>
								</option>
										<?php } ?>
							</select>			
        	</div>
					<div>
              <label for="form-field-select-3">Brand Name</label>
			    		<select class="myselect form-control" name="brand2" id="brand2" tabindex="2" style="width: 100%;">
                <option  value="">Choose Brand</option> 
										<?php foreach($brand as $bbb) { ?>
                <option value="<?php echo $bbb->id_brand;?>"><?php echo $bbb->brand_name;?>
								</option>
										<?php } ?>
							</select>			
        	</div>
				</div>
			</div>
		</div>
					<div class="modal-footer">
							<button onclick="save2()" id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
								<i class="ace-icon fa fa-floppy-o bigger-160"></i>
									Save
							</button>
							<button class="btn btn-app btn-danger btn-xs radius-4" type="submit" data-dismiss="modal">
								<i class="ace-icon fa fa-close bigger-160"></i>
									Cancel
							</button>
          </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->
 </form>  
 
 <script type="text/javascript">
      $(".myselect").select2();
</script>

<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>

 
   <script>

   
//$('#myTable').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
// var tableUsers = $('#myTable').DataTable({
// 		"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
// 		"dom": 'lfBrtip',
// 		"buttons": [
//             {
// 		   "extend":    'copy',
// 		   "text":      '<i class="fa fa-files-o"></i> Copy',
// 		   "titleAttr": 'Copy'
// 		   },
// 		   {
// 		   "extend":    'print',
// 		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
// 		   "titleAttr": 'Print',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   },
// 		   {
// 		   "extend":    'excel',
// 		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
// 		   "titleAttr": 'Excel'
// 		   },
// 		   {
// 		   "extend":    'pdf',
// 		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
// 		   "titleAttr": 'PDF',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   }
//         ],
// 		"autoWidth" : false,
// 		"scrollY" : '250',
// 		"scrollX" : true,
//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.

//         // Load data for the table's content from an Ajax source
//         "ajax": {
//             "url": "<?php echo base_url('company_form/C_company/ajax_list') ?>",
//             "type": "POST"
//         },
// 		'columnDefs': [
// 		{
//         'targets': [0],
// 		'orderable': false,
// 		}
// 		],

//       'order': [[2, 'desc']],
//         });
/*var tableUsers = $('#myTable').DataTable({
            searching:true,
			//width:'100%',
			autoWidth : false,
  		    responsive : true,
			"scrollY" : '250',
			"scrollX" : true,
			ordering: false,
			/*oLanguage: {
				sProcessing: "<img src='' />",
			},
			processing: true,
			serverSide: true,
			ajax: {
			  url: "<?php echo base_url('company_form/C_company/ambil_data') ?>",
			  type:'POST',
	}
			
        });		*/
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>

<!-- Datatable Company-->
<script>
$(document).ready(function(){ 
		$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data_company('no'); //
	
		function fetch_data_company(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
				"ajax":{
				    url: "<?php echo base_url('company_form/C_company/ajax_list') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
				 
				 'columnDefs': [
							{
							'targets': [0],
							'orderable': false,
							}
					],

						'order': [[2, 'desc']],
			 });
		  }
		  	  
		  
//proses change date-----------------------------------------------
		$('#start_date_company').change(function(){
			var start_date_company =    $('#start_date_company').val();
			var end_date_company   =    $('#end_date_company').val();
		
			if(start_date_company != '' && end_date_company != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data_company('yes', start_date_company, end_date_company);
			} else if(start_date_company == '' && end_date_company == ''){ 
				$('#myTable').DataTable().destroy();
				fetch_data_company('no');
			}
		});
		
		$('#end_date_company').change(function(){
			var start_date_company =    $('#start_date_company').val();
			var end_date_company   =    $('#end_date_company').val();
		
			if(start_date_company != '' && end_date_company != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data_company('yes', start_date_company, end_date_company);		
			} else if(start_date_company == '' && end_date_company == ''){ 
				$('#myTable').DataTable().destroy();
				fetch_data_company('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	

  }); //end document on ready	
		
</script>

   
   <script>

//$('#myTable2').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
// var tableUsers = $('#myTable2').DataTable({
// 		"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
// 		"dom": 'lfBrtip',
// 		"buttons": [
//             {
// 		   "extend":    'copy',
// 		   "text":      '<i class="fa fa-files-o"></i> Copy',
// 		   "titleAttr": 'Copy'
// 		   },
// 		   {
// 		   "extend":    'print',
// 		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
// 		   "titleAttr": 'Print',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   },
// 		   {
// 		   "extend":    'excel',
// 		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
// 		   "titleAttr": 'Excel'
// 		   },
// 		   {
// 		   "extend":    'pdf',
// 		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
// 		   "titleAttr": 'PDF',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   }
//         ],
//         "autoWidth" : false,
// 		"scrollY" : '250',
// 		"scrollX" : true,
//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.
//         "order": [], //Initial no order.

//         // Load data for the table's content from an Ajax source
//         "ajax": {
//             "url": "<?php echo base_url('company_form/C_company/ajax_list2') ?>",
//             "type": "POST"
//         },
// 		'columnDefs': [
// 		{
//         'targets': [0],
// 		'orderable': false,
// 		}
// 		],

//       'order': [[2, 'desc']],
//         });
/*var tableUsers = $('#myTable2').DataTable({
            searching:true,
			//width:'100%',
			autoWidth : false,
  		    responsive : true,
			"scrollY" : '250',
			"scrollX" : true,
			ordering: false,
			/*oLanguage: {
				sProcessing: "<img src='' />",
			},
			processing: true,
			serverSide: true,
			ajax: {
			  url: "<?php echo base_url('company_form/C_company/ambil_data2') ?>",
			  type:'POST',
	}
			
        });		*/		
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll2').change(function(){
	
	$('#editTriger2').prop("disabled", true);
	var table = $('#myTable2').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit2 = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit2++ : counterCheckededit2--;
    counterCheckededit2 == 1 ? $('#editTriger2').prop("disabled", false): $('#editTriger2').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked2 = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked2++ : counterChecked2--;
    counterChecked2 > 0 ? $('#deleteTriger2').prop("disabled", false): $('#deleteTriger2').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>

<!-- Datatable Company-->
<script>
$(document).ready(function(){ 
		$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data_company_det('no'); //
	
		function fetch_data_company_det(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable2').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
				"ajax":{
				    url: "<?php echo base_url('company_form/C_company/ajax_list2') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
				 
				 'columnDefs': [
							{
							'targets': [0],
							'orderable': false,
							}
					],

						'order': [[2, 'desc']],
			 });
		  }
		  	  
		  
//proses change date-----------------------------------------------
		$('#start_date_company_det').change(function(){
			var start_date_company_det =    $('#start_date_company_det').val();
			var end_date_company_det   =    $('#end_date_company_det').val();
		
			if(start_date_company_det != '' && end_date_company_det != ''){
				 $('#myTable2').DataTable().destroy();
				 fetch_data_company_det('yes', start_date_company_det, end_date_company_det);
			} else if(start_date_company_det == '' && end_date_company_det == ''){ 
				$('#myTable2').DataTable().destroy();
				fetch_data_company_det('no');
			}
		});
		
		$('#end_date_company_det').change(function(){
			var start_date_company_det =    $('#start_date_company_det').val();
			var end_date_company_det   =    $('#end_date_company_det').val();
		
			if(start_date_company_det != '' && end_date_company_det != ''){
				$('#myTable2').DataTable().destroy();
				 fetch_data_company_det('yes', start_date_company_det, end_date_company_det);		
			} else if(start_date_company_det == '' && end_date_company_det == ''){ 
				$('#myTable2').DataTable().destroy();
				fetch_data_company_det('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	

  }); //end document on ready	
		
</script>

	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
    function add_company()
    {
      save_method = 'add';
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_company(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('company_form/C_company/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_company"]').val(data.id_company);
            $('[name="company"]').val(data.company);
			$('[name="short"]').val(data.short);
			$('[name="status"]').val(data.status);
            
            $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Company'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

	
    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('company_form/C_company/add_company')?>";
      }
      else
      {
		   url = "<?php echo site_url('company_form/C_company/update_company')?>";
      }
	    var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
               if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }

    function delete_company()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('company_form/C_company/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
			
		

      
    }

	var save_method2; //for save method string
    var table2;
		
    function add_company2()
    {
      save_method2 = 'add';
      $('#modal_form2').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }

    function edit_company2(id)
    {
      save_method2 = 'update'
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow2:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow2:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('company_form/C_company/ajax_edit2/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_det_com"]').val(data.id_det_com);
			$('[name="company2"]').val(data.id_company);
			$('[name="brand2"]').val(data.id_brand);
            
            $('#modal_form2').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Detail Company'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

	
    function save2()
    {
      var url;
      if(save_method2 == 'add')
      {
          url = "<?php echo site_url('company_form/C_company/add_company2')?>";
		  //alert $('#company2').value();
      }
      else
      {
		   url = "<?php echo site_url('company_form/C_company/update_company2')?>";
      }
	  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form2').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }

    function delete_company2()
    {
		
			if( $('.editRow2:checked').length >= 1 ){
				var ids = [];
				$('.editRow2').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('company_form/C_company/deletedata2')?>',{ID:ids_string},function(result){ 
						  $('#modal_form2').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
      
    }

  </script>
</html>