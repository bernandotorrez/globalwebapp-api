<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_customer_bmw extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('m_customer_bmw/M_customer_bmw','',TRUE);	
		$this->load->library('form_validation');	   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();							
	}		  	
		
  public function index()
  {					    																
	$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_cust= $this->M_customer_bmw ->tampil_vendor()->result();	
	  $total_rows =$this->M_customer_bmw ->tampil_vendor()->num_rows();
	  									
	  if ($tampil_table_cust)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(5);
		$per_page =15;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'bmw_stock_sales/c_customer_bmw/c_customer_bmw/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 5;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['pp_cust'] =$this->M_customer_bmw ->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
			
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;		
		$data['intno'] = ""; //variable buat looping no table.				
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;	
		$data['ceck_row'] = $total_rows;														
		$data['intno'] = ""; //variable buat looping no table.														
		$data['show_view'] = 'v_customer_bmw/V_customer';		
		$this->load->view('dashboard/Template',$data);				
	  }else{	
	    $dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;	
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data Vendor table is empty';		
		$data['show_view'] = 'v_customer_bmw/V_customer';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }
 
 public  function do_save()
 {
        $this->form_validation->set_rules('txtcust','txtcust','required');
		$this->form_validation->set_rules('txtphone','txtphone','required');		
		$this->form_validation->set_rules('txtalamat','txtalamat','required');
		$this->form_validation->set_rules('txtalamat','txtalamatdelivery','required');
		$this->form_validation->set_rules('txtemail','txtemail','valid_email');
		
		
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan','Insert Failed  some data must be required!!');
		   redirect('bmw_stock_sales/c_customer_bmw/c_customer_bmw'); 	
		}else{   		 
			if ($this->M_customer_bmw ->save_data())
			{	 	
				$this->session->set_flashdata('pesan_succes','Insert Vendor Succesfully');			
				redirect('bmw_stock_sales/c_customer_bmw/c_customer_bmw');   
			}else{
				$this->session->set_flashdata('pesan','Insert Vendor Failed');		
				redirect('bmw_stock_sales/c_customer_bmw/c_customer_bmw');   
			}
		}
 } 
 
  public  function do_update()
 {
	    $this->form_validation->set_rules('txtcust','txtcust','required');
		$this->form_validation->set_rules('txtphone','txtphone','required');		
		$this->form_validation->set_rules('txtalamat','txtalamat','required');
		$this->form_validation->set_rules('txtalamat','txtalamatdelivery','required');
		$this->form_validation->set_rules('txtemail','txtemail','valid_email');
		
		
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan','Update Failed some data must be required!!');
		   redirect('bmw_stock_sales/c_customer_bmw/c_customer_bmw'); 	
		}else{   		 
			if ($this->M_customer_bmw ->edit_data())
			{	 	
				$this->session->set_flashdata('pesan_succes','Update Customer Succesfully');			
				redirect('bmw_stock_sales/c_customer_bmw/c_customer_bmw');   
			}else{
				$this->session->set_flashdata('pesan','Update Customer Failed');		
				redirect('bmw_stock_sales/c_customer_bmw/c_customer_bmw');   
			}
		}
 } 
 
 public function do_delete()
 {
	 if ($this->M_customer_bmw ->delete_data())
	{	 	
		$this->session->set_flashdata('pesan','Delete Customer Succesfully');			
		redirect('bmw_stock_sales/c_customer_bmw/c_customer_bmw');   
	}else{
		$this->session->set_flashdata('pesan','Update Customer Failed');		
		redirect('bmw_stock_sales/c_customer_bmw/c_customer_bmw');   
	}
 }
 
 public function do_search()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_customer_bmw ->search_data(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;
		   $data['intno'] = ""; //variable buat looping no table.						
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;			   		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['pp_cust'] =$tampung_cari;
		   $data['pesan'] = 'Customer Data Not a found'; 		 		  
		   $data['show_view'] = 'v_customer_bmw/V_customer';			
		   $this->load->view('dashboard/Template',$data);					
		}else{					   
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;		
			$data['intno'] = ""; //variable buat looping no table.				
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;	
		    $data['intno'] = ""; //variable buat looping no table.
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['pp_cust'] =$tampung_cari;			   			
			$data['show_view'] = 'v_customer_bmw/V_customer';		
		    $this->load->view('dashboard/Template',$data);		
		}	   
    }
 
 public function multiple_submit()
 {
	 if ($this->input->post('btnsave')) {
		 $this->do_save();
	 }else{
		 if ($this->input->post('btncari')) {
			 $this->do_search();
	 	 }else{
			 if ($this->input->post('btndel')) {
			     $this->do_delete();
	 	     }else{
				  if ($this->input->post('btnupdate')) {
			          $this->do_update();
	 	     	  }else{
					  redirect('bmw_stock_sales/c_customer_bmw/c_customer_bmw');
				  }			
			 }
		 }
	 }
		 
 }
 
 //------------------------------------pashing result to modal popup View Edit vendor-------------------
	
	public function get_idcust_modal_update() {    	       			
	     $id_cust = $this->input->post('id_customer',TRUE); //       
	     $query = $this->db->query("select * from tbl_customer_bmw where id_cust ='".$id_cust."'");  	    
		
		 foreach ($query->result() as $row)		
		 {
			$data['id_cust']      		= $row->id_cust;
			$data['cust_name']    		= $row->cust_name;
			$data['address']      		= $row->address;
			$data['address_delivery']   = $row->address_delivery;
			$data['email']      		= $row->email;
			$data['phone']        		= $row->phone; 	
		 }
	    
		 echo json_encode($data); //masukan kedalam jasson jquery untuk menampilkan data	   		  			   		      		
    }      					
      						 		
	
}

