<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_sales_bmw extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_sales_bmw/M_sales_bmw','',TRUE);	
		$this->load->library('form_validation');	   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();							
	}		  	
		
  public function index()
  {					    																
	$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_sales= $this->M_sales_bmw->tampil_sales()->result();	
	  $total_rows =$this->M_sales_bmw->tampil_sales()->num_rows();
	  									
	  if ($tampil_table_sales)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(5);
		$per_page =5;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'bmw_stock_sales/c_sales_bmw/c_sales_bmw/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 5;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['sales_view'] =$this->M_sales_bmw->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
				
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;		
		$data['intno'] = ""; //variable buat looping no table.				
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;	
		$data['ceck_row'] = $total_rows;														
		$data['intno'] = ""; //variable buat looping no table.														
		$data['show_view'] = 'v_sales_bmw/V_sales';		
		$this->load->view('dashboard/Template',$data);				
	  }else{		    
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;	
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data Sales table is empty';		
		$data['show_view'] = 'v_sales_bmw/V_sales';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }
 
 public  function do_save()
 {
        $this->form_validation->set_rules('txtsales','txtsales','required');
		$this->form_validation->set_rules('txtphone','txtphone','required');		
		$this->form_validation->set_rules('txtalamat','txtalamat','required');
		
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan','Insert Failed  some data must be required!!');
		   redirect('bmw_stock_sales/c_sales_bmw/c_sales_bmw'); 	
		}else{   		 
			if ($this->M_sales_bmw->save_data())
			{	 	
				$this->session->set_flashdata('pesan_succes','Insert Sales Succesfully');			
				redirect('bmw_stock_sales/c_sales_bmw/c_sales_bmw');   
			}else{
				$this->session->set_flashdata('pesan','Insert Sales Failed');		
				redirect('bmw_stock_sales/c_sales_bmw/c_sales_bmw');   
			}
		}
 } 
 
  public  function do_update()
 {
	    $this->form_validation->set_rules('txtsales','txtsales','required');
		$this->form_validation->set_rules('txtphone','txtphone','required');		
		$this->form_validation->set_rules('txtalamat','txtalamat','required');
		
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan','Update Failed some data must be required!!');
		   redirect('bmw_stock_sales/c_sales_bmw/c_sales_bmw'); 	
		}else{   		 
			if ($this->M_sales_bmw->edit_data())
			{	 	
				$this->session->set_flashdata('pesan_succes','Update Sales Succesfully');			
				redirect('bmw_stock_sales/c_sales_bmw/c_sales_bmw');   
			}else{
				$this->session->set_flashdata('pesan','Update Sales Failed');		
				redirect('bmw_stock_sales/c_sales_bmw/c_sales_bmw');   
			}
		}
 } 
 
 public function do_delete()
 {
	 if ($this->M_sales_bmw->delete_data())
	{	 	
		$this->session->set_flashdata('pesan','Delete Sales Succesfully');			
		redirect('bmw_stock_sales/c_sales_bmw/c_sales_bmw');   
	}else{
		$this->session->set_flashdata('pesan','Delete Sales Failed');		
		redirect('bmw_stock_sales/c_sales_bmw/c_sales_bmw');   
	}
 }
 
 public function do_search()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_sales_bmw->search_data(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null){		 
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;
		   $data['intno'] = ""; //variable buat looping no table.						
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;			   		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['sales_view'] =$tampung_cari;
		   $data['pesan'] = 'Sales Data Not a found'; 		 		  
		   $data['show_view'] = 'v_sales_bmw/V_sales';			
		   $this->load->view('dashboard/Template',$data);					
		}else{					  
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;		
			$data['intno'] = ""; //variable buat looping no table.				
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;	
		    $data['intno'] = ""; //variable buat looping no table.
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['sales_view'] =$tampung_cari;			   			
			$data['show_view'] = 'v_sales_bmw/V_sales';		
		    $this->load->view('dashboard/Template',$data);		
		}	   
    }
 
 public function multiple_submit()
 {
	 if ($this->input->post('btnsave')) {
		 $this->do_save();
	 }else{
		 if ($this->input->post('btncari')) {
			 $this->do_search();
	 	 }else{
			 if ($this->input->post('btndel')) {
			     $this->do_delete();
	 	     }else{
				  if ($this->input->post('btnupdate')) {
			          $this->do_update();
	 	     	  }else{
					  redirect('bmw_stock_sales/c_sales_bmw/c_sales_bmw');
				  }			
			 }
		 }
	 }
		 
 }
 
 //------------------------------------pashing result to modal popup View Edit vendor-------------------
	
	public function get_sales_modal_update() {    	       			
	     $id_sales = $this->input->post('id_sales',TRUE); //       
	     $query = $this->db->query("select * from tbl_sales_bmw where id_sales ='".$id_sales."'");  	    
		
		 foreach ($query->result() as $row)		
		 {
			$data['id_sales'] = $row->id_sales;
			$data['sal_name']    = $row->sal_name;
			$data['phone'] = $row->phone;
			$data['alamat'] = $row->alamat; 
			$data['postion_class'] = $row->postion_class; 			
		 }
	    
		 echo json_encode($data); //masukan kedalam jasson jquery untuk menampilkan data	   		  			   		      		
    }      					
      						 		
	
}

