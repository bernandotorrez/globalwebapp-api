<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_crud_stock_bmw extends MY_Controller
{
  public function __construct()
        {
            parent::__construct();		
			$this->load->helper(array('form', 'url'));    	   
			$this->load->model('m_slsstock_bmw/M_crud_stock_bmw','C_crud_stock_bmw',TRUE);		
			
			/* di koment karena library fpdf deprecated, by bernand
				$this->load->library('fpdf'); 
			*/
			$this->load->library('fpdf'); 
            $this->load->library('form_validation');
			$this->load->helper('terbilang');
			$this->load->helper('form');
			$this->load->helper('url');			
		    $this->load->database();					
        }		  	
		
  public function index()
	{	 
	
	  $data['tampil_lokasi']=$this->C_crud_stock_bmw->get_lokasi();	  
	  //autocomplete jquery----- 
	  $data['tampil_type_vehicle']=$this->C_crud_stock_bmw->get_type_vehicle(); 		     	  		
	  $data['tampil_warna']="";
	  //---------------------------	  	   
	  $data['tampil_bank']=$this->C_crud_stock_bmw->get_bank();
	  $data['tampil_curr']=$this->C_crud_stock_bmw->get_currency();		  	  
	  $data['stocknumber']=$this->C_crud_stock_bmw->getstocknumber();
	  $data['show_view']='v_slsstock_bmw/V_form_addnew_stock';
	  $this->load->view('dashboard/Template',$data);		
	}
		
	function select_warna_combo_jquery(){
		 		
		 $result_warna= $this->C_crud_stock_bmw->get_color(); //from model							 
		 foreach ($result_warna->result() as $row_warna):
			 echo '<option value="'.$row_warna->id_colour.'">'.$row_warna->colour.'</option>';
		 endforeach;	
    } 
	
	function doget_price_unit_jquery()
	{
		$result_sellprice = $this->C_crud_stock_bmw->get_price_unit();//from model							  		     		 		 					
			foreach ($result_sellprice->result() as $row_price):				
				$data['price_cost'] = $row_price->price_cost;
				$data['currency'] = $row_price->currency;
			    $data['production'] = $row_price->production; 
				//$data['price_offroad'] = $row_price->price_offroad;			
			endforeach;	
			
		echo json_encode($data);
	}
	
	function doget_interior_colour_jquery()
	{
		$result_colour_int = $this->C_crud_stock_bmw->get_color_interior();//from model							  		     		 		 					
			foreach ($result_colour_int->result() as $row_int):				
				$data['interior_colour'] = $row_int->interior_colour;			
			endforeach;	
			
		echo json_encode($data);
	}
	
	public function do_upload()
	{		
		$str_branch = $this->session->userdata("branch_short");		
		$stryear =  substr(date('Y'),-3);
		$str_date = $stryear."-".date("m")."-".date("d");
	    $str_count = substr($this->session->userdata("ses_nostock"),-3);	
		$str_nostock =$this->session->userdata("ses_nostock") ;	    		
		$str_nofotostock ="M"."-".$str_branch."-".$str_date."-".$str_count;

       //  echo './asset/upload_foto/'.$str_branch;					    					           
		//$config['upload_path'] = './asset/uploads/'.substr($str_com,0,2);		
		$config['upload_path'] = './asset/upload_foto/'.$str_branch;		
		$config['allowed_types'] = 'pdf';	
		$config['max_size']	= '40000kb';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$config['overwrite'] = TRUE;
		$config['file_name']= $str_nofotostock; 

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('pesan_fail',$str_nostock.implode(" ",$error));			
		}
		else
		{			
			$data = array('upload_data' => $this->upload->data());		
			$this->session->set_flashdata('pesan_upload_succes',"1");
			$this->C_crud_stock_bmw->insert_address_foto();//menjalankan insert alamat qutation yg nantinya akan di insert. 
		}
				
					
	}	
	
	
	public function do_insert_stock()	
	{		
	    $this->form_validation->set_rules('cbotype','cbotype','required');	
		$this->form_validation->set_rules('cbowarna','cbowarna','required');
		$this->form_validation->set_rules('cbointake','cbointake','required');	
		//$this->form_validation->set_rules('price_unit','price_unit','required');		
		$this->form_validation->set_rules('txtcsiyear','txtcsiyear','required');
		$this->form_validation->set_rules('txtcsi','txtcsi','required');
		$this->form_validation->set_rules('txtvin','txtvin','required');
		$this->form_validation->set_rules('txtengine','txtengine','required');
		$this->form_validation->set_rules('txtdatericived','txtdatericived','required');
		$this->form_validation->set_rules('txtdatetommi','txtdatetommi','required');
		$this->form_validation->set_rules('cbo_demo_unit','cbo_demo_unit','required');
		$this->form_validation->set_rules('price_unit','price_unit','required');
		$this->form_validation->set_rules('price_offroad','price_offroad','required');
		
		if($this->form_validation->run()==FALSE):			
		   echo $this->session->set_flashdata('pesan_fail','Type unit,VIN,Engine,Date B.I, Invoice B.I,date Recieved,Dpp Must Be Required, Insert can not be prosses!!');
		   redirect('bmw_stock_sales/c_slsstock_bmw/c_crud_stock_bmw'); 			   
		else:
			if ($this->C_crud_stock_bmw->check_duplicate_vin()):
				echo $this->session->set_flashdata('pesan_fail','VIN Number Duplicate, Insert can not be prosses!');
				 redirect('bmw_stock_sales/c_slsstock_bmw/c_crud_stock_bmw');
			else:
			   if ($this->C_crud_stock_bmw->check_duplicate_engine()):
				   echo $this->session->set_flashdata('pesan_fail','Engine Number Duplicate, Insert can not be prosses!');
				   redirect('bmw_stock_sales/c_slsstock_bmw/c_crud_stock_bmw');
			   else:					
					if ($this->C_crud_stock_bmw->insert_master_stock()) :					 
					       $this->do_upload(); //manggil fungsi Upload File				    
						  if ($this->session->flashdata('pesan_upload_succes') !=""):				
							  $str_upload_success = "& Upload Foto";
						  else:
							  $str_upload_success = "";
						  endif;
						  				  
						  $this->C_crud_stock_bmw->counterstocknumber();  //add and manggil counter numberpp	 					
						  $this->session->set_flashdata('pesan_succces','Insert Stock Unit Successfull..!!');				 							
						  redirect('bmw_stock_sales/c_slsstock_bmw/c_crud_stock_bmw');  
					else:		
						  echo $this->session->set_flashdata('pesan_insert_fail','fail! ');
						  redirect('bmw_stock_sales/c_slsstock_bmw/c_crud_stock_bmw');  			
					endif;
				endif;	
			endif;
		endif;	
	}			 

public function show_form_edit()
 {
	$tampil_edit_stock = $this->C_crud_stock_bmw->getedit_stock();	
	$tampil_cur = $this->C_crud_stock_bmw->get_currency();	
	$tampil_bank_edit = $this->C_crud_stock_bmw->get_bank(); 		
	
	//get warna berdasarkan type-------------------
	
 	  foreach ($tampil_edit_stock as $row_getidtype) :
			  $buff_idtype =  $row_getidtype->id_type ;
	  endforeach;	
	  
	  //echo  $buff_idtype ;		
	  
	  $query_warna = $this->db->query("select * from tbl_colour where id_type  ='".$buff_idtype."'");	
	  $hasil_query_warna = $query_warna->result();		 		
	  		
	//end-------------------		
				  	   		
  	if ($tampil_edit_stock == null)
	{   	   	  
		echo $this->session->set_flashdata('pesan_fail','Data Edit Empty !');				
	    $url=  base_url('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw');		         
	    echo "<script type='text/javascript'> window.location='" . $url . "'; </script>"; 		
     }else{		 	 
	 	$data_edit['check_row'] = "1"; // jika  ketemu				  
	    $data_edit['ino'] = ""; // variable kosong buat nocounter perulangan detail		
		$data_edit['tampil_edit_stock'] = $tampil_edit_stock;				
		$data_edit['tampil_type_vehicle']=$this->C_crud_stock_bmw->get_type_vehicle(); 		     	  		
	    $data_edit['tampil_warna']="";		
		$data_edit['warna_sesuai_type'] = $hasil_query_warna; //tarik warana berdasarkan id_type	
		$data_edit['tampil_lokasi']=$this->C_crud_stock_bmw->get_lokasi();	  
		$data_edit['tampil_bank']  = $tampil_bank_edit ;
	    $data_edit['tampil_curr'] = $tampil_cur;
		$data_edit['show_view'] ='v_slsstock_bmw/V_form_edit_stock';
		$this->load->view('dashboard/Template',$data_edit);
     }
	   
 }
	
public function do_edit_stock() //lakukan submit edit data	
{	
      
	    $this->form_validation->set_rules('cbotype','cbotype','required');	
		$this->form_validation->set_rules('cbowarna','cbowarna','required');
		$this->form_validation->set_rules('cbointake','cbointake','required');	
		//$this->form_validation->set_rules('price_unit','price_unit','required');		
		$this->form_validation->set_rules('txtcsiyear','txtcsiyear','required');
		$this->form_validation->set_rules('txtcsi','txtcsi','required');
		$this->form_validation->set_rules('txtvin','txtvin','required');
		$this->form_validation->set_rules('txtengine','txtengine','required');
		//$this->form_validation->set_rules('txtdatericived','txtdatericived','required');
		$this->form_validation->set_rules('txtdatetommi','txtdatetommi','required');
		$this->form_validation->set_rules('cbo_demo_unit','cbo_demo_unit','required');
		$this->form_validation->set_rules('price_unit','price_unit','required');
		$this->form_validation->set_rules('price_offroad','price_offroad','required');
	  
				
		if($this->form_validation->run()==FALSE):			
		   echo $this->session->set_flashdata('pesan','Type unit,VIN,Engine,Date B.I,Intake ,date Recieved,Dpp Must Be Required, Insert can not be prosses!!');
		   redirect('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw'); 			   
		else:
			if ($this->C_crud_stock_bmw->check_edit_duplicate_vin()):
				echo $this->session->set_flashdata('pesan','VIN Number Duplicate, Insert can not be prosses!');
				 redirect('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw');
			else:
			   if ($this->C_crud_stock_bmw->check_edit_duplicate_engine()):
				   echo $this->session->set_flashdata('pesan','Engine Number Duplicate, Insert can not be prosses!');
				 redirect('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw');
			   else:					
					if ($this->C_crud_stock_bmw->edit_master_stock()) :	
					    
					    $this->do_upload(); //manggil fungsi Upload File				    
						
						  if ($this->session->flashdata('pesan_upload_succes') !=""):				
							  $str_upload_success = "& Upload Foto";
						  else:
							  $str_upload_success = "";
						  endif;							    		  				  		       				  								
						
						 $this->session->set_flashdata('pesan_succes','update stock  Successfull..!!');										
						 $url=  base_url('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw');		         
						 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";   
				    else:						 
						 $this->session->set_flashdata('pesan_succces','Update stock Failed!!!!!! ....'); 					
						 $url=  base_url('bmw_stock_sales/c_slsstock_bmw/c_crud_stock_bmw');		         
						 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>"; 
				   endif;		
				endif;
			endif;
		endif;	 
}

	 
public function do_delete_stock()	 //lakukan submit delete data
{		
	 if ($this->C_crud_stock_bmw->delete_with_edit_flag())
	 { 	 	
		 $this->session->set_flashdata('pesan','Delete Successfully....');		
		 redirect('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw');   
	  }else{
		  $this->session->set_flashdata('pesan','No Data Selected To Remove!!');		  
		  redirect('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw');
	 }
}			

public function do_search_data()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->C_crud_stock_bmw->search_data(); // manggil hasil cari di model 	
		$tampil_branch = $this->C_crud_stock_bmw->tampil_cabang_mazda()->result();
		 
		 $total_rows_stock =$this->C_crud_stock_bmw->hitung_jumlah_stock()->num_rows();
		 $total_rows_dp =$this->C_crud_stock_bmw->hitung_jumlah_dp()->num_rows();
		 $total_rows_wrs =$this->C_crud_stock_bmw->hitung_jumlah_wrs()->num_rows();				
		//print_r($tampung_cari);		
		
		$branch  = $this->session->userdata('name_branch') ; 
	    $company = $this->session->userdata('company') ;		
		
		if($tampung_cari == null){		   		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = "0";	
			$data['count_row_stock'] = $total_rows_stock;
		    $data['count_row_dp'] = $total_rows_dp;
		    $data['count_row_wrs'] = $total_rows_wrs;	
			$data['stock_view']  = "";
			$data['tampil_branch'] = $tampil_branch ;
			$data['show_view'] = 'v_slsstock_bmw/V_table_stock';
			$this->load->view('dashboard/Template',$data);			
		}else{				   		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = "1";	
			$data['count_row_stock'] = $total_rows_stock;
		    $data['count_row_dp'] = $total_rows_dp;
		    $data['count_row_wrs'] = $total_rows_wrs;	
			$data['tampil_branch'] = $tampil_branch ;
			$data['stock_view']  = $tampung_cari;				
			$data['show_view'] = 'v_slsstock_bmw/V_table_stock';
			$this->load->view('dashboard/Template',$data);	 
		}	  
    }
	

public function do_search_date()  	
	{
	  $tampung_cari= $this->C_crud_stock_bmw->get_search_date()->result();	
	  $total_rows =$this->C_crud_stock_bmw->get_search_date()->num_rows();
	  
		  if ($tampung_cari)
		  {
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('company') ;		
		    $data['bmk_view'] =$tampung_cari;	
			$data['intno'] = ""; //variable buat looping no table.	
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;														
			$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
			$data['show_view'] = 'v_bmk/V_table_bmk';			
			$this->load->view('dashboard/Template',$data);				
		  }else{	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('company') ;	
			$data['intno'] = ""; //variable buat looping no table.					
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;	
			$data['pesan'] = 'Data PP table is empty';		
		    $data['show_view'] = 'v_bmk/V_table_bmk';		
			$this->load->view('dashboard/Template',$data);				
		  } 
	}	
	
public function multiple_submit() //fungsi submit dengan banyak button dalam satu form
{  
	
	if ($this->input->post('btnadd')){	
		redirect('bmw_stock_sales/c_slsstock_bmw/c_crud_stock_bmw');
	}else{
		if ($this->input->post('btnsave')){	
			$this->do_insert_stock();
		}else{			
			 if ($this->input->post('btncaridate')){
				$this->do_search_date();
			 }else{
				if ($this->input->post('btncari')){	
					$this->do_search_data();
				}else{   		
					if ($this->input->post('btndel')){
						$this->do_delete_stock();
					}else{
						if ($this->input->post('btnedit')){
							$this->show_form_edit();
						}else{	
							if ($this->input->post('btncross-send')){
								$this->do_send_cross_selling();
							}else{																		
								redirect('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw');  												
							}
						 }
					  }
				   }
			   }
		   }
	  }

 }
 
 public function do_send_cross_selling()
 {
    $this->form_validation->set_rules('cbobranchutama','cbobranchutama','required');	
    $this->form_validation->set_rules('txtremarks_cross','txtremarks_cross','required');		
	if ($this->form_validation->run() == FALSE) :			
		 echo $this->session->set_flashdata('pesan','Destination Branch,Remarks cross selling, Must Be Required, approval can not be prosses!!');
		   redirect('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw'); 			   
	else:	 
			 if ($this->C_crud_stock_bmw->update_send_cross_selling()) :
				  $this-> kirim_email();			 
			 	  $this->session->set_flashdata('pesan_succes','sending approval Cross Selling  Successfull..!!');	
				   redirect('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw');
			 else:
			 	 echo $this->session->set_flashdata('pesan','sending approval Cross Selling fail !!');
				  redirect('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw');			 
			 endif;
	endif;		 
 }
 public function kirim_email(){          
 		   
		   $this->load->library('email');				 								   		   		  	        
		  		
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;		
		  
		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   $config['smtp_port']  = '587';		  		  
		 
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   
		 
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5"; 
		   //--------------------------------		   		  
		    		   		 		          				   				     		   				  									
			$strid_stock_no =$this->session->userdata("ses_srtock_no");	
			$status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete		
					
		   if ($strid_stock_no  != '') :			        	  	
				//Select untuk email master sales----------------------------
				   $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
				$query =$this->db->query("select * from qv_complite_master_stockin where stock_no ='".$strid_stock_no."'and status ='".$status_aktif_record."'");				
			   if ($query->num_rows() > 0 ) :					   					   										   					
					
					 $data_email['str_cross_selling'] = $query->result();
					 $message = $this->load->view('v_slsstock_bmw/V_content_email',$data_email,true);	
				endif;	   
			endif;
					 
			$result = $this->email ;							   
			$this->email->initialize($config);  
			$this->email->set_newline("\r\n"); 
		   
		    //Exchange						
			
			$sender_email = "helpdesk@eurokars.co.id"; 					   
			$sender_name = "Stock Notification";					   					    
			
			
		   //simpan session alamat email kedalam variable..
				$struseremail = $this->session->userdata('email');
				$strheademail = $this->session->userdata('email_head');
				$strfaemail = $this->session->userdata('email_fa');
			//end-----------------------------------------
		   
			//$to = 'brian.yunanda@eurokars.co.id';
			
			$to = $struseremail.",".$strheademail.",".$strfaemail;
			
			$subject = "Request ApProval Cross Selling -- Eurokar Surya Utama";				   
			$this->email->from($sender_email, $sender_name);
			$this->email->to($to);
			$this->email->subject($subject);  
			
			//$this->email->message(print_r($message, true));			   
		  
		    $this->email->message($message);// tanpa array	
												  	 
		     if ($this->email->send()) :		  	
			    $this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
			    //redirect(current_url());
		     //} else {
			//    show_error($this->email->print_debugger());
		     endif; 
								
		
			 //Destroy session per variable			 $this->session->unset_userdata('ses_noppnew');		

	 }
}

