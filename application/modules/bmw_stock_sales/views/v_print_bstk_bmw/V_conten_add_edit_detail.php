<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js');?>" ></script>
      
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />    
   -->
<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">


<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
    <div class="panel panel-default" align="center" >
      <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" ><?php echo "Add / Edit Detail Payment Customer BMW Unit" ; ?> </div>
        <div class="panel-body" >   
    <label>Header Info</label>
    <?php  echo form_open_multipart('c_print_bstk_bmw/c_print_bstk_bmw/multiple_submit',array('class'=>'form-multi'),'id = my_form');  ?> 
        <?php 
		    echo '<table  class="table-striped  table-hover table-condensed table-bordered" width="95%" >';				 
		?>
 <tr align="center" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);font-weight:bold;font-size: 12px">
    <?php
		echo '<td align="center">SPK NO</td>';
		echo '<td align="center">TYPE</td>';
		echo '<td align="center">COLOUR</td> ';
		echo '<td align="center">VIN</td>';
		echo '<td align="center">ENGINE</td>' ;
		echo '<td align="center  width="6%">CUSTOMER</td>' ; 
		echo '<td align="center">SALES</td>';
		
		echo '</tr>';	
		foreach ($str_trans_master as $row_head) : 
		  echo'<tr style="font-size:12px">';		
		  ?>
		  <td align="center" hidden="true" >
			  <input value="<?php echo $row_head->id_trans_sales ?>" name="txtidtrans" />
		  </td>			
		 <?php					
		 echo'<td align="center">'.$row_head->nospk.'</td>';			
		 echo'<td>'.$row_head->desc_type.'</td>';
		 echo'<td align="center">'.$row_head->colour.'</td>';
		 echo'<td align="center">'.$row_head->vin.'<input name="txtvin" hidden="true" value="'.$row_head->vin.'"></td>';				
		 echo'<td align="center">'.$row_head->engine.'<input name="txtengin" hidden="true" value="'.$row_head->engine.'"></td>';
		 echo'<td align="center">'.$row_head->cust_name.'</td>';
		 echo'<td align="center">'.$row_head->sal_name.'</td>';
		 echo '</tr>';						       
		 endforeach;
		 echo '</table>'; 
	 ?>

       
<label>Detail </label>   
 
<table class="table-striped table-bordered" width="95%">
 <thead align="center" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);font-weight:bold;font-size: 12px">
      <td width="2%">No</td>
      <td width="30%" >Description</td>        
      <td width="16%">Date</td>
      <td width="34%">Payment</td>        
      <td width="2%">Evidence / Upload PDF</td> 
      <td >Action</td>            
    </thead>    
    
<tbody class="detail">        
                                       
    <?php
	    if ($str_trans_detail != "") :
		  foreach ($str_trans_detail as $row_detail)  : $intno = $intno +1;  
	?>
        <tr style="font-size:13px;">	
          <td class="no" align="center"><?php echo $intno ?>
           <input name ="txtiddet[]" value="<?php echo $row_detail->id_detail ?>"  class="catchtxtiddet"  hidden="true" />		
          </td>							 		 
          <td><input name="txdescdet[]" class="form-control desc_det" id="txdescdet[]" placeholder="Description" value="<?php echo $row_detail->desc_det ?>" /></td>							
          <td align="center"><input name="txtdatedet[]" class="form-control datepicker" id="txtdatedet[]"  placeholder="Date" value="<?php 
		      if ($row_detail->pay_date  !=""):
		  		echo  date('d-m-Y', strtotime($row_detail->pay_date)); 
			  endif;	
		  ?>"  size="24" maxlength="70" readonly   data-date-format="dd-mm-yyyy" />
          </td>																																																			
          <td align="center">
          <input name="txtpaydet[]" value="<?php echo  number_format($row_detail->amount,2,'.',',') ?>" placeholder="0.00"  size="35"  class="form-control price" maxlength="70" onkeypress="return isNumberKey(event)">
          </td>	
          <td align="center"  > 
            <?php
			if ($row_detail->attach_trans!= "") :
		       echo anchor_popup($row_detail->attach_trans,"<span class='glyphicon glyphicon-search ' aria-hidden='true'>".'View'."</span>");		
			 else:
			 	echo "No Data";  	
		  	 endif; 
			 ?>              
          </td>  
          <td align="center">         
           <button class="remove"><img src="<?php echo base_url('asset/images/delete.png'); ?>" /></button>        
		   
          </td>                             
        </tr>
       <?php endforeach; ?> 
   <?php endif; ?>   
   <tr style=" background:#FAFECB; color:#C00114">       
    <td class="no" align="center"><?php echo $intno + 1 ?>
    <input name ="txtiddet[]" hidden="true" />
    </td>							  
          <td>
             <input id ="descdet_ksosong" name="txdescdet[]" placeholder="Description"  class="form-control desc_det" />
          </td>							
          <td align="center">
            <input name="txtdatedet[]" placeholder="Date" size="24" class="form-control datepicker"   data-date-format="dd-mm-yyyy" />
          </td>																																																			
          <td align="center">
            <input name="txtpaydet[]" placeholder="0.00"  size="35"  class="form-control price" maxlength="70" onkeypress="return isNumberKey(event)">
          </td>	
          <td >  
           <div class="btn-success">                           
               <input type="file"  name="userfile" value="Upload" class="btn-success" />
            </div>   
          </td>  
          <td align="center"> 
             <button class="remove"><img src="<?php echo base_url('asset/images/delete.png'); ?>" /></button>
          </td>                           
     </tr>
  </tbody>   
</table>


<br>

<table width="95%" class="table-striped  table-hover table-condensed table-bordered"  >
  <?php foreach ($str_trans_master as $row) : ?>
       <tr style="background:#EFEFEF; font-size:13px;"  >
           <td   style="font-weight:bold;color:#C00114;">Payment Type : 		    
            <input hidden="true" type="text" id="txtbuffid_det" name="txtbuffid_det" class="buffiddetdel"  />
           </td>
           <td style="font-weight:bold;color:#C00114;"><?php  echo $row->desc_pay ; ?>  </td>    
      </tr>    
     <tr style="background:#FFCACB; font-size:13px;"  >
           <td  style="font-weight:bold" width="25%">Price Unit Offroad:</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":"; ?> 
           <input name="txtunitprice" align="right" value="<?php  echo number_format($row->price_curent,2,'.',',') ?>" class="unit_price" readonly />
           </div>
           </td>

    </tr>
    
     <tr style="background:#FFCACB; font-size:13px;"  >
           <td  style="font-weight:bold" width="20%">Discount :</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":" ;?>
           <input name="txtdiscount" align="right" value="<?php echo number_format($row->discount,2,'.',',') ?>" class="discount" readonly />
           </div>
           </td>
    </tr>

    
    <tr style="background:#FFCACB; font-size:13px;"  >
           <td  style="font-weight:bold" width="25%">PPH 22:</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":"; ?> 
           <input name="txtpph" align="right" value="<?php  echo number_format($row->pph22_sell,2,'.',',') ?>" class="pph" readonly />
           </div>
           </td>

    </tr>
    
    <tr style="background:#FFCACB; font-size:13px;"  >
           <td  style="font-weight:bold" width="25%">BBN:</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":"; ?> 
           <input name="txtbbn" align="right" value="<?php  echo number_format($row->bbn,2,'.',',') ?>" class="bbn" readonly />
           </div>
           </td>

    </tr>
    
    <tr style="background:#FFCACB; font-size:13px;"  >
           <td  style="font-weight:bold" width="25%">Sell Price/ Onroad:</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":"; ?> 
           <input name="txtunitprice" align="right" value="<?php  echo number_format($row->price_onroad,2,'.',',') ?>" class="price_onroad" readonly />
           </div>
           </td>

    </tr>
    
    
    <tr style="background:#EFEFEF; font-size:13px;"  >
           <td  style="font-weight:bold" width="20%">Poles-Optional :</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":" ;?>
           <input name="txtpoles" align="right" value="<?php echo number_format($row->opt_poles,2,'.',',') ?>" class="poles" readonly />
           </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF; font-size:13px;"  >
           <td  style="font-weight:bold" width="20%">PDI-Optional :</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":" ;?>
           <input name="txtpdi" align="right" value="<?php echo number_format($row->opt_pdi,2,'.',',') ?>" class="pdi" readonly />
           </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF; font-size:13px;"  >
           <td  style="font-weight:bold" width="20%">Surat Jalan-Optional :</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":" ;?>
           <input name="txtsurjal" align="right" value="<?php echo number_format($row->opt_surtjalan,2,'.',',') ?>" class="surjal" readonly />
           </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF; font-size:13px;"  >
           <td  style="font-weight:bold" width="20%">Kaca Film-Optional:</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":" ;?>
           <input name="txtkafilm" align="right" value="<?php echo number_format($row->opt_kacfilm,2,'.',',') ?>" class="kafilm" readonly />
           </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF; font-size:13px;"  >
           <td  style="font-weight:bold" width="20%">Others-Optional:</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":" ;?>
           <input name="txtother" align="right" value="<?php echo number_format($row->opt_other,2,'.',',') ?>" class="other" readonly />
           </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF; font-size:13px;"  >
           <td  style="font-weight:bold" width="20%">Insentif Sales:</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":" ;?>
           <input name="txtother" align="right" value="<?php echo number_format($row->insetivfe_sales,2,'.',',') ?>" class="other" readonly />
           </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF; font-size:13px;"  >
           <td  style="font-weight:bold" width="20%">Suport B.I:</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":" ;?>
           <input name="txtsuport" align="right" value="<?php echo number_format($row->suport_disc,2,'.',',') ?>" class="suportbi" readonly />
           </div>
           </td>
    </tr>
    
     <tr style="background:#EFEFEF; font-size:13px;"   >
           <td  style="font-weight:bold" width="20%">Total Payment Customer</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":" ; ?> 
           <input name="txttotalpayment" value="<?php echo number_format($row->total_paymet_customer,2,'.',',') ?>" class="amount" readonly />
           </div>
           </td>
    </tr>
    
     <tr style="background:#EFEFEF; font-size:13px;"   >
           <td  style="font-weight:bold" width="20%">Gross Profit(GP)</td>
           <td  style="font-weight:bold;" width="80%" align="left">
           <div class="col-xs-10">
		   <?php echo $row->curr_sell.":" ; ?> 
           <input name="grossprofit" value="<?php echo number_format($row->gross_profit,2,'.',',') ?>" class="grossprofit" readonly />
           </div>
           </td>
    </tr>
                        
    <tr style="background:#FAFECB; font-size:13px;"   >
           <td style="font-weight:bold;color:#C00114" width="20%"><span style="font-weight:bold">Remaining Payment Customer   :</span></td>
           <td width="80%" style="font-weight:bold;color:#C00114;" align="left" >
		    <div class="col-xs-10">
		   <?php echo $row->curr_sell.":" ?> <input name="txtpcr" value="<?php echo number_format($row->remaining_amount_customer,2,'.',',') ?>" readonly class="r_payment" >
           <?php if ($row->remaining_amount_customer =="0.00") :?>
             <span id="imglunas"><img src="<?php 
			 echo base_url("asset/images/success_ico.png") ?>">
             <label>Ready To Send Approval D.O / BSTK</label>
             </span>
           <?php else :  ?>   
           	 <span id="imglunas" hidden="true"><img src="<?php 
			 echo base_url("asset/images/success_ico.png") ?>"></span>	
           <?php endif; ?></div>          
		   
           </td>           
    </tr> 
  <?php endforeach  ?>
  <tr>
  <td colspan="3">
  	  <input type="submit" name="btnsavedetail" id="btnsavedetail" value="Submit " class="btn btn-primary"/>
  	  <a href="<?php echo base_url('c_slssell_unit_bmw/c_slsssell_bmw');?>" style="text-decoration:none;"><input id="btnback" name="btnback" type="button" value="Back To Table" class="btn btn-warning" /> </a>
	      </tr>  
</table> 
 <?php form_close(); ?>  
    </div><!--panel  -->
  </div> <!--panel  -->
</div><!--col-md-12  -->


<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- jQuery_ui -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->



<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 


<script type="text/javascript">
//--------------function number only
 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	

</script>

<script>
$(function() {  
    $('body').delegate('.r_payment','change',function(){ 
      //var tr =  $(this).parent().parent();	  		 
	  //tr.find('.price').;  		
		  if ($('.r_payment').val("0.00")){
			alert('lunas') 
		  };
	 });	  	  
});		 
</script>

<script script type="text/javascript">
//calculate and add field--------------
$(function(){		
	
	 	
	 
	  $('body').delegate('.remove','click',function(){				 				 
	 	   $(this).parent().parent().remove();
		   var tr =  $(this).parent().parent();	  		 
		   var payment_cust_det = numeral().unformat(tr.find('.price').val());
		 
		 //mangmabil detali paymain yg baru dinput.
		   var get_iddet =  tr.find('.catchtxtiddet').val();	
		 //------------------------------------------------------  
		  
		   var total_payment_cust = numeral().unformat($('.amount').val());
		   var rem_payment_cust = numeral().unformat($('.r_payment').val());
		  	
		   hasil_total_payment_cust = total_payment_cust - payment_cust_det ;	   		
		   r_payment  = rem_payment_cust + payment_cust_det ;		
		 
		   
		   $('.amount').val(numeral(hasil_total_payment_cust).format('0,0.00')) ;
		   $('.r_payment').val(numeral(r_payment).format('0,0.00')) ;
		   
		    var  buff_iddet =$('.buffiddetdel').val()			
		    if  (buff_iddet == "") { 	
			    $('.buffiddetdel').val(get_iddet);
			}else{
		        $('.buffiddetdel').val($('.buffiddetdel').val() + "," + get_iddet );
			}
		   
		    if (r_payment != 0) {
		      $('#imglunas').hide("slow"); //hiden image lunas aka checklist
		    }
		      						   			  		  			 		 
	 });
	 
	 $('body').delegate('.price','change',function(){  	
	       var tr =  $(this).parent().parent();	  		 
		   
		   //reamining payment :harga offroad+bbn)-discount-dp/paycust)---
		   var priceoffroad = numeral().unformat($('.unit_price').val());
		   var bbn = numeral().unformat($('.bbn').val());		 	
		   var discount = numeral().unformat($('.discount').val());		  
		   //----------------------------------------------
		   		   
		   //akumulasi payment customer-----------------------------------
		   var t=0;
		   var jmlbaris=parseInt($('.remove').length);
		   		  		   
		   for(var i=0;i < jmlbaris;++i){
			   t+=parseFloat(numeral().unformat($('.price').eq(i).val()));
		   }		  
		   //---------------------------------------------------
		   
		   var total_payment_customer = t ;	//akumulasi detail pay cust	    								
		   var unit_price_current_result =(priceoffroad+bbn)-discount-t;
		   $('.amount').val(total_payment_customer);
		   $('.r_payment').val(unit_price_current_result);
 	 });
	 
	 	$('body').delegate('.price','blur',function(){  	
		   var tr =  $(this).parent().parent();	  	 
		   var price=tr.find('.price').val();
		   var r_payment=$('.r_payment').val();
		   var amount=$('.amount').val();
		  
		   	   
		   tr.find('.price').val(numeral(price).format('0,0.00')) ;		  	
		   $('.r_payment').val(numeral(r_payment).format('0,0.00')) ;
		   $('.amount').val(numeral(amount).format('0,0.00')) ;	
		  ;		  		 		 		 
 	 });  	
	 		
	 	  	
});

</script>

<script type="text/javascript">
$(function(){
	//jquery untuk post submit save / edit
    $('#myform').on('submit', function(e){ //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
        e.preventDefault();		
		var url = '<?php  echo base_url('c_slssell_unit/c_crud_sell/do_insert_edit_detail_sell');  ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
            success: function(data){              
            }
        });
    });
}); 
</script>

<script >

function openWin(img) {
    window.open(img);
}
</script>
