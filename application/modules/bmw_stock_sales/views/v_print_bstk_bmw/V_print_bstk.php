<!--<script type="text/javascript" src="<?php //echo base_url('asset/js/jquery.js');?>" ></script>
<script> -->
<script type="text/javascript" src="<?php echo base_url('asset/js/jquery-3.1.0.min.js');?>" ></script> <!-- jequry buata upload file  --->


<script type="text/javascript">
// fungsi untik checklist semua otomatis
function CheckAll()
{
    l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = true;		
		}	
		document.getElementById('btnedit').disabled = true;	
		document.getElementById('btndel').disabled = false;	
		document.getElementById('btnsendtab').disabled = true;	
}

function UncheckAll()
{
   l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = false;			 
		}
		document.getElementById('btnedit').disabled = true;
		document.getElementById('btndel').disabled = true;
		document.getElementById('btnsendtab').disabled = true;
}


//--------------------function disbaled enabled button with check book

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_bstk').removeAttr('disabled');			
        }
        else{
            $('.btn_bstk').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_send').removeAttr('disabled');			
        }
        else{
            $('.btn_send').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btn_delete').removeAttr('disabled');			
        }
        else{
            $('.btn_delete').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_edit').removeAttr('disabled');			
        }
        else{
            $('.btn_edit').attr('disabled', 'disabled');								
        }
    });
});
//-------------------------------------------------------------------------		 	

//-------------------------------------------------------------------------		 	
  
//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});		  
//set focus ----------------------------  
 
</script>

 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_fail');?>  
        </div>	
<?php } ?>

      
  
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" ><?php echo "Table Print BSTK "." | ".$header ; ?> </div>
   <div class="panel-body">               
   
          
 <?php  echo form_open('bmw_stock_sales/c_print_bstk_bmw/c_print_bstk_bmw/multiple_submit','id = form_my');  ?>     
 
    <!-- Menu Button Start -->
    <div class="row text-right">
        <button id="btnrefresh" name="btnrefresh" class="btn btn-app btn-warning btn-xs radius-4" value="Refresh Table"
            type="submit">
            <i class="ace-icon fa fa-refresh bigger-160"></i>
            Refresh
        </button>

        <button id="btnbstk" name="btnbstk" class="btn btn-app btn-primary btn-xs radius-4 btn_bstk" value="Print BSTK"
            type="submit" disabled>
            <i class="ace-icon fa fa-print bigger-160"></i>
            Print BSTK
        </button>

        <button id="btnaddeditdetail" name="btnaddeditdetail" class="btn btn-app btn-success btn-xs radius-4 btn_edit"
            value="Repayment Leasing / C.O.P" type="submit" disabled>
            <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
            Edit
        </button>

        <button id="btn-givereprint" name="btn-givereprint" class="btn btn-app btn-danger btn-xs radius-4" hidden="true"
            value="Re-print BSTK" type="submit" disabled>
            <i class="ace-icon fa fa-print bigger-160"></i>
            BSTK
        </button>


        <!-- Menu Button End -->

        <!-- Search Menu Start -->
        <table align="left">

            <td>
                <label>Selected: </label>
            </td>
            <td>&nbsp;</td>
            <td>
                <select name="cbostatus" id="cbostatus" class="form-control">
                    <option value="nospk">NO SPK</option>
                    <option value="vin">VIN</option>
                    <option value="engine">ENGINE</option>
                    <option value="stock_no">STOCK NO</option>
                    <option value="cust_name">CUSTOMER</option>
                    <option value="csi">CSI</option>
                    <option value="location_stock">LOCATION</option>
                </select>
            </td>
            <td>&nbsp;</td>

            <td>
                <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari"
                    value="<?php echo $this->session->flashdata('cari'); ?>">
            </td>

            <td>

                <button id="btncari" name="btncari" class="btn btn-app btn-primary btn-xs radius-4" value="Search"
                    type="submit">
                    <i class="ace-icon fa fa-search bigger-160"></i>
                    Search
                </button>

                <!-- <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF" />               -->

            </td>
            </tr>
        </table>
    </div>
    <!-- Search Menu Start -->

    <div class="space-12"></div>
        
 
<!-- Table Responsive -->
<div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>NO SPK</th>
                        <th>Type</th>
                        <th>Vin</th>
                        <th>Engine</th>
                        <th>Colour</th>
                        <th>Date Alloc</th>
                        <th>Due Date SPK</th>
                        <th>Customer</th>
                        <th>Sales</th>
                        <th>WS Date</th>
                        <th>Payment Status</th>
                        <th>View Detail</th>
                        <th>Upload Faktur</th>
                        <th hidden="true">Add/Edit Detail</th>
                        <th><a href="javascript:void(0)" onclick="CheckAll()" style="text-decoration:none"> All</a>
                            \
                            <a href="javascript:void(0)" onclick="UncheckAll()" style="text-decoration:none">Uncheck</a>

                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($ceck_row >= 1) :		  
		foreach($sell_view as $row):		 
		  
		  if ( $row->flag_print_bstk != '1' ) :
		  	  echo '<tr style="font-size:11px;">'; 
		  else:
		  	  echo '<tr style="font-size:11px;" class="btn-warning">';   
		  endif	; 
     ?>       
     		                    		
                <td width="8%" align="center"><?php echo $row->nospk; ?></td>            
                <td><?php echo $row->desc_type ; ?></td>		
                <td><?php echo $row->vin ; ?></td>	
                <td align="center"><?php echo $row->engine ; ?></td>	
                <td><?php echo $row->colour ; ?></td>
                <td><?php echo date('d-m-Y', strtotime($row->date_aloc)) ; ?></td>
                <td align="center"><?php echo date("d-m-Y",strtotime($row->date_reminder_spk)) ; ?></td>               
                <td  align="center" ><?php echo $row->cust_name ; ?></td>				
                <td  align="center" ><?php echo $row->sal_name ; ?></td>
                <td  align="center">
				     <?php 
					  if ($row->ws_date == null) :
					     echo "-" ;
					  else:
						 echo date('d-m-Y', strtotime($row->ws_date)) ; 
					  endif;  	
				  	 ?>
                </td>              
                 <?php 				
				 $strtmppay = $row->remaining_amount_customer ;					  
				 if  ($strtmppay==0.00) :					 
					 echo '<td align="center" style="color:blue">'."Balance(lunas)".'</td>'  ;
				 else:
					 echo '<td align="center" >'."X".'</td>'  ;
				 endif;	 
				?>
               </td>
                                    
                 <td align="center" >
                	<a href="#" class="btn btn-primary btn-setting print" id="print" style="text-decoration:none" req_id="<?php echo $row->id_trans_sales ; ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>View Detail</a>
                </td>  
             <?php  if ($row->flag_upload_faktur == "1") { ?>
                 <td align="center" ><a href="<?php echo base_url($row->path_up_fatktur); ?>" style="color:blue;text-decoration:none;"><span class='glyphicon glyphicon-search' aria-hidden='true'></span>View Faktur</a>
		      	</td>  
            <?php }else{  ?>  
                 <td align="center" >
                  <input id="fileSelect" name="fileSelect" type="file" accept=".pdf" class="btn btn-default" req_id="<?php echo $row->id_trans_sales; ?>" />
                </td>  
              <?php } ?>      
                 <td align="center" hidden="true">
                	<a href="#" class="btn btn-primary btn-setting add_detail" id="add_detail" style="text-decoration:none" req_id_add_edit_detail="<?php echo $row->id_trans_sales ; ?>" ><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>Add Detail</a>
                </td>                                         
                <td align="center">
              			 	
			 	 <div align="center">                 
                   <input id="checkchild" name="msg[]" type="checkbox" value="<?php echo $row->id_trans_sales ; ?>"/>                                 
    		<?php form_close(); ?>             			            					
                </div>
            </td>
		  </tr> 	  
		 <?php 		    
		  endforeach;
 	   endif;
   echo'</table>';	  				
		 ?> 
                </tbody>
            </table>
        </div>
        <!-- Table Responsive -->
		 
		<?php		   			
		     if (! empty($pagination))			
			 { 
		  	   echo '<center><table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table></center>';				 
			 }			  	  						
		?>
        
 </div>  
</div> 
      
          
  </div>
 </div>
   
</div>


<!-- Modal For Detail -->
 
      <div class="modal fade" id="ViewDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Selling Unit Info</h4>
                    </div>
                    
                    <div class="modal-body">
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->            
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
       
<!-- Modal -->  	

<!-- Modal For Detail -->
      <div class="modal fade" id="AddEditDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Selling Unit Info</h4>
                    </div>
                    
                    <div class="modal-body">                    
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->            
                    </div> 
                    
                     <div class="modal-footer">                    
                       <input type="submit" name="btnsavedetail" id="btnsavedetail" value="Submit " class="btn btn-primary"/>
                       <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal -->  	

<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- jQuery_ui -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->


<script>
//get data table remarks to modal popup.
$(function(){
  $(document).on('click','.print',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("bmw_stock_sales/c_print_bstk_bmw/c_print_bstk_bmw/get_idtrans_modal"); ?>';
		
		$("#ViewDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_trans_sales:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});
</script>


<script>
//get data table remarks to modal popup. add edit
/*
$(function(){
  $(document).on('click','.add_detail',function(e){
		var req_id_add_edit_detail = $(this).attr('req_id_add_edit_detail'); //atribut from <a ref 	php echo $row->id_master 
		var url = '<?php echo site_url("c_slssell_unit_bmw/c_slsssell_bmw/get_add_edit_idtrans_modal"); ?>';
		
		//$("#AddEditDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_trans_sales_edit:req_id_add_edit_detail},
			success: function(xx) {
				$('#tempmodal').html(xx);
				$("#testmodal").modal('show');
			}
						
		}); 				
		
   });	
});
*/
</script>


<script type="text/javascript">

$(function(){
	//jquery untuk post submit save / edit
    $('#myform').on('submit', function(e){ //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
        e.preventDefault();		
		var url = '<?php  echo base_url('c_slssell_unit_bmw/c_crud_sell_bmw/multiple_submit');  ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL			
            type: 'POST', //or POST
            data: $('#myform').serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
            success: function(data){              
            }
        });
    });
}); 
</script>


<script type="text/javascript">
//upload menggunakan userfile type file
$(document).ready(function(){
	 $('#fileSelect').bind('change',function(event){
	  event.preventDefault();
	  var id_trans_sales = $(this).attr('req_id'); //atribut from <a ref php echo $row->id_master ;	
	 
	   //var formData = new FormData(document.getElementById('form_my')); //method new  1 upload
	   //method old 2 ---------------------------------------------------
	   var formData = new FormData();
	   formData.append('fileSelect', $('input[type=file]')[0].files[0]); 
	   formData.append('id_trans_sales',id_trans_sales);
       //----------------------------------------------------------------
		   $.ajax({
					url:"<?php echo base_url('bmw_stock_sales/c_print_bstk_bmw/c_print_bstk_bmw/do_upload_pdffaktur');  ?>",
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					cache : false,
				    success: function(data) {
						console.log(data);
						location.reload();
						
					},
					complete: function(XMLHttpRequest) {
						var data = XMLHttpRequest.responseText;
						console.log(data);
					},
					error: function() {
						alert("ERROR");
					}
		     });
			
    });
		
});

</script>
