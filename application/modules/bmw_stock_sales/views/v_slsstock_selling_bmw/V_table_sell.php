<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js');?>" ></script>
<script>


// fungsi untik checklist semua otomatis
function CheckAll()
{
    l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = true;		
		}	
		document.getElementById('btnedit').disabled = true;	
		document.getElementById('btndel').disabled = false;	
		document.getElementById('btnsendtab').disabled = true;	
}

function UncheckAll()
{
   l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = false;			 
		}
		document.getElementById('btnedit').disabled = true;
		document.getElementById('btndel').disabled = true;
		document.getElementById('btnsendtab').disabled = true;
}


//--------------------function disbaled enabled button with check book

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_edit').removeAttr('disabled');			
        }
        else{
            $('.btn_edit').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_send').removeAttr('disabled');			
        }
        else{
            $('.btn_send').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btn_delete').removeAttr('disabled');			
        }
        else{
            $('.btn_delete').attr('disabled', 'disabled');								
        }
    });
});

//-------------------------------------------------------------------------		 	

//-------------------------------------------------------------------------		 	
  
//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});		  
//set focus ----------------------------  
 
</script>

 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_fail');?>  
        </div>	
<?php } ?>

      
  
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" ><?php echo "Table Sales Unit "." | ".$header ; ?> </div>
   <div class="panel-body">               
   
          
 <?php  echo form_open('bmw_stock_sales/c_slssell_unit_bmw/c_crud_sell_bmw/multiple_submit','id = form_my');  ?>     
 
    <!-- Menu Button Start -->
    <div class="row text-right">
      
        <button id="btnrefresh" name="btnrefresh" class="btn btn-app btn-warning btn-xs radius-4" value="Refresh" type="submit">
            <i class="ace-icon fa fa-refresh bigger-160"></i>
              Refresh
          </button>

          <button id="btnadd" name="btnadd" class="btn btn-app btn-primary btn-xs radius-4" value="Add Sales" type="submit">
            <i class="ace-icon fa fa-book bigger-160"></i>
              Add New
          </button>  

          <button id="btnedit" name="btnedit" class="btn btn-app btn-success btn-xs radius-4 btn_edit" value="Edit Sales" type="submit" disabled>
            <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
              Edit
          </button> 

          <button id="btndel" name="btndel" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" value="Delete" type="submit" onclick="return confirm('are you sure want to delete this data!')" disabled>
            <i class="ace-icon fa fa-ban bigger-160"></i>
              Delete
          </button>  

          <button id="btnsendaproval" name="btnsendaproval" class="btn btn-app btn-primary btn-xs radius-4 btn_edit" value="Send Approval D.O" type="submit" disabled>
            <i class="ace-icon fa fa-money bigger-160"></i>
              Send D.O
          </button> 

          <button id="btnaddeditdetail" name="btnaddeditdetail" class="btn btn-app btn-success btn-xs radius-4 btn_edit" value="Add/Edit Detail" type="submit" disabled>
            <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
              Detail
          </button>  
      
    
    <!-- Menu Button End -->

    <!-- Search Menu Start -->
    <table align="left">

        <td >
            <label>Selected: </label>
        </td>
        <td>&nbsp;</td>
        <td>
        <select name="cbostatus" id="cbostatus" class="form-control">                          
             <option value="nospk">NO SPK</option>
             <option value="vin">VIN</option>
             <option value="engine">ENGINE</option>
             <option value="stock_no">STOCK NO</option>
             <option value="cust_name">CUSTOMER</option>
             <option value="sal_name">SALES</option>
             <option value="csi">CSI</option>             
             <option value="location_stock">LOCATION</option>             
       </select>
        </td>
        <td>&nbsp;</td>

        <td>
            <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari"
                value="<?php echo $this->session->flashdata('cari'); ?>">
        </td>

        <td>

            <button id="btncari" name="btncari" class="btn btn-app btn-primary btn-xs radius-4" value="Search"
                type="submit">
                <i class="ace-icon fa fa-search bigger-160"></i>
                Search
            </button>

            <!-- <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF" />               -->

        </td>
        </tr>
        </table>
        </div>
        <!-- Search Menu Start -->

        <div class="space-12"></div>

 
    <!-- Table Responsive -->
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>NO SPK</th>
            <th>Type</th>
            <th>Vin</th>
            <th>Engine</th>
            <th>Colour</th>
            <th>Date Alloc</th>
            <th>Due Date SPK</th>
            <th>Customer</th>
            <th>Sales</th>
            <th>WRS Date</th>
            <th>Payment Status</th>
            <th>Payment Type</th>
            <th>View Detail</th>
            <th hidden="true" align="center">Add/Edit Detail</th>
            <th><a href="javascript:void(0)" onclick="CheckAll()" style="text-decoration:none"> All</a>
              /
              <a href="javascript:void(0)" onclick="UncheckAll()" style="text-decoration:none">Uncheck</a></td>
            </th>

          </tr>
        </thead>
        <tbody class="detail">
          <?php if ($ceck_row >= 1) :		  
              foreach($sell_view as $row):		 
              if ($row->status_flag_sell == 0) : 	  		   
                echo '<tr style="font-size:11px;" class="btn-info">'; 
              else:			    			
                  if ($row->date_dudate_spk <= date('Y-m-d')) :
                  if ($row->remaining_amount_customer != "0.00") : 				   
                      echo '<tr style="font-size:11px;" class="btn-warning">'; 
                  else:
                    echo '<tr style="font-size:11px;">'; 	   
                  endif;	
                else:	
                  if ($row->flag_send_approval == -1) : 	  		   
                    echo '<tr style="font-size:11px;" class="btn-danger">'; 
                  else:			  
                    echo '<tr style="font-size:11px;">'; 
                  endif;	
                endif;	
              endif;		 
              ?>
          <td width="8%" align="center"><?php echo $row->nospk; ?></td>
          <td><?php echo $row->desc_type ; ?></td>
          <td><?php echo $row->vin ; ?></td>
          <td align="center"><?php echo $row->engine ; ?></td>
          <td><?php echo $row->colour ; ?></td>
          <td><?php echo date('d-m-Y', strtotime($row->date_aloc)) ; ?></td>
          <td align="center"><?php echo date("d-m-Y",strtotime($row->date_reminder_spk)) ; ?></td>
          <td align="center"><?php echo $row->cust_name ; ?></td>
          <td align="center"><?php echo $row->sal_name ; ?></td>
          <td align="center">
            <?php 
						if ($row->ws_date == null || $row->ws_date == ""  ) :
						    echo "-" ;
				        else:
							echo date('d-m-Y', strtotime($row->ws_date)) ;
						endif;	    
						 
					  ?>
          </td>
          <?php 				
				 $strtmppay = $row->remaining_amount_customer ;					  
				 if  ($strtmppay==0.00) :		
				   if ($row->flag_send_approval != -1) :			 
					   echo '<td align="center" style="color:blue">'."Balance(lunas)".'</td>'  ;
					else:
					   echo '<td align="center">'."Rejected".'</td>'  ;
					endif; 
				 else:
				     if ($row->date_dudate_spk <= date('Y-m-d')) :
					    if ($row->status_flag_sell == 2) :					  
					 	   echo '<td align="center" style="color:red">'."Due Date SPK".'</td>'  ;
						 endif;  
					 else :					 										     		 					
						 echo '<td align="center"  style="color:red">'."Wating Payment".'</td>'  ;
					 endif;	 
				 endif;	 
				?>
          </td>

          <?php 
					  if ($row->desc_pay == "Cash") :
						 echo '<td align="center" class ="btn-primary">'.'CASH'.'</td>';
					  else :	    
					  	if ($row->desc_pay == "Leasing") :
						   echo '<td align="center" class = "btn-success">'.'LEASING'.'</td>' ;
						else :
						   echo '<td align="center" class ="btn-info">'.'COP / Fleet'.'</td>' ;
						endif;
					endif;	
						  
					?>
          </td>
          <td align="center">
            <a href="#" class="btn btn-primary btn-setting print" id="print" style="text-decoration:none"
              req_id="<?php echo $row->id_trans_sales ; ?>"><span class="glyphicon glyphicon-eye-open"
                aria-hidden="true"></span>View Detail</a>
          </td>
          <td align="center" hidden="true">
            <a href="#" class="btn btn-primary btn-setting add_detail" id="add_detail" style="text-decoration:none"
              req_id_add_edit_detail="<?php echo $row->id_trans_sales ; ?>"><span class="glyphicon glyphicon-eye-open"
                aria-hidden="true"></span>Add Detail</a>
          </td>
          <td align="center">
            <?php if ($row->status_flag_sell !=0) : 	      ?>
            <div align="center"><input id="checkchild" name="msg[]" type="checkbox"
                value="<?php echo $row->id_trans_sales ; ?>" />
              <?php else: ?>
              <label>D.O / BSTK</label>
              <?php endif ?>

              <?php form_close(); ?>
            </div>
          </td>
          </tr>
          <?php 		    
		  endforeach;
 	   endif;  				
		 ?>

      </table>
    </div>
    <!-- Table Responsive -->
 
    
		 
		<?php		   			
		     if (! empty($pagination))			
			 { 
		  	   echo '<center><table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table></center>';				 
			 }			  	  						
		?>
        
 </div>  
</div> 
      
          
  </div>
 </div>
   
</div>


<!-- Modal For Detail -->
 
      <div class="modal fade" id="ViewDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Detail Selling Unit Info</h4>
                    </div>
                    
                    <div class="modal-body">
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->            
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>  
                    </div>              
                </div>
            </div>
        </div>
       
<!-- Modal -->  	

<!-- Modal For Detail -->
      <div class="modal fade" id="AddEditDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Selling Unit Info</h4>
                    </div>
                    
                    <div class="modal-body">                    
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->            
                    </div> 
                    
                     <div class="modal-footer">                    
                       <input type="submit" name="btnsavedetail" id="btnsavedetail" value="Submit " class="btn btn-primary"/>
                      
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal -->  	

<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- jQuery_ui -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>


<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->



<script>
//get data table remarks to modal popup.
$(function(){
  $(document).on('click','.print',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("bmw_stock_sales/c_slssell_unit_bmw/c_slsssell_bmw/get_idtrans_modal"); ?>';
		
		$("#ViewDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_trans_sales:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});
</script>

<script>
//get data table remarks to modal popup. add edit
$(function(){
  $(document).on('click','.add_detail',function(e){
		var req_id_add_edit_detail = $(this).attr('req_id_add_edit_detail'); //atribut from <a ref 	php echo $row->id_master 
		var url = '<?php echo site_url("bmw_stock_sales/c_slssell_unit_bmw/c_crud_sell_bmw/get_add_edit_idtrans_modal"); ?>';
		
		$("#AddEditDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_trans_sales_edit:req_id_add_edit_detail},
			success: function(msg) {
				$('.modal-body').html(msg);				
			}
						
		}); 				
		
   });	
});
</script>

<script>
//get data table remarks to modal popup. add edit
/*
$(function(){
  $(document).on('click','.add_detail',function(e){
		var req_id_add_edit_detail = $(this).attr('req_id_add_edit_detail'); //atribut from <a ref 	php echo $row->id_master 
		var url = '<?php echo site_url("bmw_stock_sales/c_slssell_unit/C_slsssell/get_add_edit_idtrans_modal"); ?>';
		
		//$("#AddEditDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_trans_sales_edit:req_id_add_edit_detail},
			success: function(xx) {
				$('#tempmodal').html(xx);
				$("#testmodal").modal('show');
			}
						
		}); 				
		
   });	
});
*/
</script>


<script type="text/javascript">

$(function(){
	//jquery untuk post submit save / edit
    $('#myform').on('submit', function(e){ //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
        e.preventDefault();		
		var url = '<?php  echo base_url('bmw_stock_sales/c_slssell_unit_bmw/c_crud_sell_bmw/multiple_submit');  ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL			
            type: 'POST', //or POST
            data: $('#myform').serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
            success: function(data){              
            }
        });
    });
}); 
</script>


<?php if ($this->session->userdata('id_group') != 'admin001'): ?>
   <script>
	$(document).ready(function() { 	
		$('#btndel').hide();		
	});
	</script>
<?php endif; ?>