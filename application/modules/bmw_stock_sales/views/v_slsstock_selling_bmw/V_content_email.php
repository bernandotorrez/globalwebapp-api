<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.css');?>" />  
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" />        
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />      

<h2>
	<?php
	    $strcom = $this->session->userdata('company');
		$strbranch = $this->session->userdata('name_branch');
		$strdept = $this->session->userdata('dept');
		 
	    echo  'Company : '.$strcom. " | " ." Cabang : " .$strbranch ;
	 ?>
</h2>

<label>Submission Approval D.O</label>
 
<table width="70%" class="table-striped table-bordered table-hover table-condensed" >
 <?php foreach ($str_selling_master as $row)  :  ?>
    <tr style="background:#FBF5B7">
      <td width="30%" style="font-weight:bold;">SPK NO  :</td>
      <td><?php echo $row->nospk ;?></td>
    </tr>
    <tr >
      <td width="30%" style="font-weight:bold;">Pay type :</td>
      <td style="font-weight:bold;color:RED">
	        <?php			 
				echo $row->desc_pay ; 			
	  		?>
      </td>
    </tr>
    <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;">User Submission :</td>
      <td><?php echo $row->user_sell ; ?></td>
    </tr>
     <tr style="font-weight:bold;" >
      <td width="30%" style="font-weight:bold;">Customer :</td>
      <td><?php echo $row->cust_name ; ?></td>
    </tr>
     <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;">Sales :</td>
      <td><?php echo $row->sal_name ; ?></td>
    </tr>
    <tr >
      <td width="30%" style="font-weight:bold;">Date Sent Approval : </td>
      <td><?php echo date('d-m-Y') ; ?></td>
    </tr>
    <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;" >Model :</td>
       <td><?php echo $row->desc_model ; ?></td>
    </tr>
     <tr >
      <td width="30%" style="font-weight:bold;" >Type :</td>
       <td><?php echo $row->desc_type ;?></td>
    </tr>
     <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;" >Colour :</td>
       <td><?php echo $row->colour ; ?></td>
    </tr>
     <tr >
      <td width="30%" style="font-weight:bold;" >Vin :</td>
       <td><?php echo $row->vin ;?></td>
    </tr>
     <tr style="background:#FBF5B7">
       <td width="30%" style="font-weight:bold;">Engine:</td>
       <td><?php echo $row->engine ; ?></td>
    </tr>
     <tr >
       <td width="30%" style="font-weight:bold;">Year:</td>
       <td><?php echo $row->year ;?></td>
    </tr>
       <tr style="background:#FFCACB;"  >
           <td  style="font-weight:bold" width="40%">Price Unit Offroad :</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
                <label>
               		 <?php echo $row->curr_sell." ".number_format($row->price_curent,2,'.',',') ?>
                </label>
            </div>
           </td>
    </tr>
    
     <tr style="background:#FFCACB;"  >
           <td  style="font-weight:bold" width="40%">BBN :</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
                <label>
               		 <?php echo $row->curr_sell." ".number_format($row->bbn,2,'.',',') ?>
                </label>
            </div>
           </td>
    </tr>
    
    <tr style="background:#FFCACB;"  >
           <td  style="font-weight:bold" width="40%">Price Onroad :</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
                <label>
               		 <?php echo $row->curr_sell." ".number_format($row->price_onroad,2,'.',',') ?>
                </label>
            </div>
           </td>
    </tr>
    
    
    <tr style="background:#EFEFEF;"  >
           <td  style="font-weight:bold" width="40%">Discount :</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
		      <label>
		       <?php echo $row->curr_sell." ".number_format($row->discount,2,'.',',') ?>
             </label>
           </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF;"   >
           <td  style="font-weight:bold" width="40%">Poles-Optional:</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
		    <label><?php echo $row->curr_sell." ".number_format($row->opt_poles,2,'.',',') ?></label>
            </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF;"   >
           <td  style="font-weight:bold" width="40%">PDI-Optional:</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
		    <label><?php echo $row->curr_sell." ".number_format($row->opt_pdi,2,'.',',') ?></label>
            </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF;"   >
           <td  style="font-weight:bold" width="40%">Surat Jalan-Optional:</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
		    <label><?php echo $row->curr_sell." ".number_format($row->opt_surtjalan,2,'.',',') ?></label>
            </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF;"   >
           <td  style="font-weight:bold" width="40%">Kaca Film-Optional:</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
		    <label><?php echo $row->curr_sell." ".number_format($row->opt_kacfilm,2,'.',',') ?></label>
            </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF;"   >
           <td  style="font-weight:bold" width="40%">Others-Optional:</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
		    <label><?php echo $row->curr_sell." ".number_format($row->opt_other,2,'.',',') ?></label>
            </div>
           </td>
    </tr>
    
     <tr style="background:#EFEFEF;"   >
           <td  style="font-weight:bold" width="40%">Suport B.I:</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
		    <label><?php echo $row->curr_sell." ".number_format($row->suport_disc,2,'.',',') ?></label>
            </div>
           </td>
    </tr>
     <tr style="background:#FFCACB;"  >
           <td  style="font-weight:bold" width="40%">Remaining Payment Customer :</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
                <label>
               		 <?php echo $row->curr_sell." ".number_format($row->remaining_amount_customer,2,'.',',') ?>
                </label>
            </div>
           </td>
    </tr>   
 <?php endforeach; ?>   
</table>

</br>

<h2>Detail Payment</h2>
<table class="table-striped table-bordered table-hover table-condensed" width="70%">    
    <tr style="background:#CCC;">
        <td width="2%" style="font-weight:bold;">No</td>
        <td width="28%" style="font-weight:bold;">Description</td>
        <td width="28%" style="font-weight:bold;">Pay Date</td>
        <td width="5%" style="font-weight:bold;">Amount</td>     
    </tr>
    <?php foreach ($str_selling_detail as $row_detail)  : $intno = $intno +1;  ?>
   		<tr>	
          <td><?php echo $intno ?></td>							  
          <td><?php echo $row_detail->desc_det ; ?></td>							  
          <td><?php echo date('d-m-Y', strtotime($row_detail->pay_date)) ;?></td>																																																				
          <td ><?php echo number_format($row_detail->amount,2,'.',',') ;?></td> 
        </tr>
    <?php endforeach; ?>
</table>