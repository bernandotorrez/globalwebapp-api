<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.css');?>" />  
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" />        
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />      

<h2>
	<?php
	    $strcom = $this->session->userdata('company');
		$strbranch = $this->session->userdata('name_branch');
		$strdept = $this->session->userdata('dept');
		 
	    echo  'Company : '.$strcom. " | " ." Cabang : " .$strbranch ;
	 ?>
</h2>

<table width="70%" class="table-striped table-bordered table-hover table-condensed" >
 <?php foreach ($str_cross_selling as $row)  :  ?>
   
    <tr style="background:#FBF5B7" >
      <td  style="font-weight:bold;">Info Cross Selling</td>
      <td  style="font-weight:bold;">Request Approval</td>
    </tr>
    <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;">User Stock Admin :</td>
      <td><?php echo $row->user_stock ; ?></td>
    </tr>   
    <tr >
      <td width="30%" style="font-weight:bold;">Date Sent Approval : </td>
      <td><?php echo date('d-m-Y') ; ?></td>
    </tr>
    <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;" >Model :</td>
       <td><?php echo $row->desc_model ; ?></td>
    </tr>
     <tr >
      <td width="30%" style="font-weight:bold;" >Type :</td>
       <td><?php echo $row->desc_type ;?></td>
    </tr>
     <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;" >Colour :</td>
       <td><?php echo $row->colour ; ?></td>
    </tr>
     <tr >
      <td width="30%" style="font-weight:bold;" >Vin :</td>
       <td><?php echo $row->vin ;?></td>
    </tr>
     <tr style="background:#FBF5B7">
       <td width="30%" style="font-weight:bold;">Engine:</td>
       <td><?php echo $row->engine ; ?></td>
    </tr>
     <tr >
       <td width="30%" style="font-weight:bold;">Year:</td>
       <td><?php echo $row->year ;?></td>
    </tr>   
      <tr style="background:#FBF5B7" >
       <td width="30%" style="font-weight:bold;">Unit From:</td>
       <td><?php echo $row->name_branch ;?></td>
    </tr>  
     <tr >
       <td width="30%" style="font-weight:bold;">Unit Destination:</td>
       <td><?php echo $row->dst_cross_sell ;?></td>
    </tr>    
     <tr style="background:#FBF5B7" >
       <td width="30%" style="font-weight:bold;">Remark Cross Sell :</td>
       <td><?php echo $row->remarks_cross_sell ;?></td>
    </tr>  
 <?php endforeach; ?>   
</table>

</table>