<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_model_brand_bmw extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_vehicle_model_bmw';
	 
    public function tampil_sales(){ //tampil table untuk ngececk num_row						     	     		  	    			
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		
		   		 		 				 
		 $this->load->database();		
		 $this->db->select('*');									 
		 $this->db->where('status',$status_delete);				
		 $this->db->order_by('id_model','desc');
		 $result = $this->db->get('qv_complite_vehicle_model_bmw');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    				 				
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  	     		 
	     $this->load->database();		
		 $this->db->select('*');											
		 $this->db->where('status',$status_delete);				  		 
		 $this->db->order_by('id_model','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_vehicle_model_bmw')->result();		 		   
		 return $result ;
	}							  		
		
		
  public function save_data()
  {
	
	$str_model = strtoupper($this->input->post('txtmodel'));  
	$strid_brand = "4";			
	$str_status = "1";  
	
	$insert_data = array(
	  				"id_brand" => $strid_brand,
					"desc_model" => $str_model,																
					"status" =>$str_status,
					);
	$this->db->insert('tbl_brand_model_bmw',$insert_data);		
	return true;							
  }		
  
    public function edit_data()
	{		 		
		$str_idmodel = $this->input->post('txtid');  				
		$str_model = strtoupper($this->input->post('txtmodel'));  			
		
		if ($str_idmodel !='') {
			$edit_data = array( "desc_model" => $str_model	);						 																														
			$this->db->where('id_model',$str_idmodel);								
			$this->db->update('tbl_brand_model_bmw',$edit_data); 															
			return true;		
		}else{
			return false;		
		}
								
	  }		
	  
	    public function delete_data()
	    {		 		
			$delete = $this->input->post('msg');  
			$str_status_delete = "0" ;
								
			if ($delete !='') {						
				for ($i=0; $i < count($delete) ; $i++) {	
					$del_data = array("status" => $str_status_delete,);																																																
					
					$this->db->where('id_model',$delete[$i]);
					$this->db->update('tbl_brand_model_bmw',$del_data); 
				}		
				return true;		
			}else{
				return false;		
			}								
	  }		
				
		public function search_data() 	// fungsi cari data vendor
		{
		   $cari = $this->input->post('txtcari');
		   $kategory =  $this->input->post('cbostatus');	  		   		  		  
		   $status_del = "1";	   
		   
		   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
		   {
			   redirect('bmw_stock_sales/c_model_brand_bmw/c_model_brand_bmw');  //direct ke url C_sales/C_sales
		   }else{	
			   $this->load->database();		   			  	
			   $this->db->where('status', $status_del);
			   $this->db->like($kategory,$cari);			  
			   $result = $this->db->get('qv_complite_vehicle_model_bmw');		 
			   return $result->result(); 
		   }
		} 		    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}