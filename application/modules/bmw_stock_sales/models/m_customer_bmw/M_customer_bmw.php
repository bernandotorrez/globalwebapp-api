<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_customer_bmw extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_customer_bmw';
	 
    public function tampil_vendor(){ //tampil table untuk ngececk num_row						     	     		  
	     $ses_branch_id = $this->session->userdata('branch_id'); //manggili session id 			
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		  		 		 		
		 
		 $this->load->database();		
		 $this->db->select('*');	
		 $this->db->where('branch_id',$ses_branch_id);								 
		 $this->db->where('status',$status_delete);				
		 $this->db->order_by('id_cust','desc');
		 $result = $this->db->get('qv_complite_customer_bmw');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	     
		 $ses_branch_id = $this->session->userdata('branch_id'); //manggili session id 				 				
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  	     		 
	     $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('branch_id',$ses_branch_id);		
		 $this->db->where('status',$status_delete);				  		 
		 $this->db->order_by('id_cust','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_customer_bmw')->result();		 		   
		 return $result ;
	}							  		
		
		
  public function save_data()
  {
	
	$ses_branch_id = $this->session->userdata('branch_id'); //manggili session id 	
	
	$str_customer_name 		= strtoupper($this->input->post('txtcust'));  
	$str_phone  			= $this->input->post('txtphone');  	
	$str_alamat 			= $this->input->post('txtalamat');
	$str_alamat_delivery 	= $this->input->post('txtalamatdelivery'); 
	$str_email 				= $this->input->post('txtemail');  
	$str_status 			= "1";  
	
	$insert_data = array(
	  				"cust_name" 		=> $str_customer_name,
					"address" 			=> $str_alamat,
					"address_delivery" 	=> $str_alamat_delivery,
					"email"				=> $str_email,
					"phone" 			=> $str_phone,				
					"branch_id" 		=> $ses_branch_id,
					"status" 			=>$str_status,					
					);
	$this->db->insert('tbl_customer_bmw',$insert_data);		
	return true;							
  }		
  
    public function edit_data()
	{		 		
	    $ses_branch_id 			= $this->session->userdata('branch_id'); //manggili session id 	
		$str_idcust 			= $this->input->post('txtid');  				
		$str_customer_name 		= strtoupper($this->input->post('txtcust'));  
		$str_phone  			= $this->input->post('txtphone');  	
		$str_alamat 			= $this->input->post('txtalamat'); 
		$str_alamat_delivery 	= $this->input->post('txtalamatdelivery');  	
		$str_email 				= $this->input->post('txtemail');  
		
		if ($str_idcust !='') {
			$edit_data = array(
							"cust_name" 		=> $str_customer_name,
							"address" 			=> $str_alamat,
							"address_delivery" 	=> $str_alamat_delivery,
							"email"				=> $str_email,
							"phone" 			=> $str_phone,																						
							);			
			$this->db->where('branch_id',$ses_branch_id);				
			$this->db->where('id_cust',$str_idcust);
			$this->db->update('tbl_customer_bmw',$edit_data); 															
			return true;		
		}else{
			return false;		
		}
								
	  }		
	  
	    public function delete_data()
	    {		 		
			$delete = $this->input->post('msg');  
			$str_status_delete = "0" ;
								
			if ($delete !='') {						
				for ($i=0; $i < count($delete) ; $i++) {	
					$del_data = array("status" => $str_status_delete,);																																																
					
					$this->db->where('id_cust',$delete[$i]);
					$this->db->update('tbl_customer_bmw',$del_data); 
				}		
				return true;		
			}else{
				return false;		
			}								
	  }		
				
		public function search_data() 	// fungsi cari data vendor
		{
		   $cari = $this->input->post('txtcari');
		   $kategory =  $this->input->post('cbostatus');	  		   		  
		   $ses_branch_id = $this->session->userdata('branch_id');
		   $status_del = "1";	   
		   
		   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
		   {
			   redirect('bmw_stock_sales/c_customer_bmw/c_customer_bmw');  //direct ke url c_vendor/c_vendor
		   }else{	
			   $this->load->database();		   			  
			   $this->db->where('branch_id',$ses_branch_id);
			   $this->db->where('status', $status_del);
			   $this->db->like($kategory,$cari);			  
			   $result = $this->db->get('qv_complite_customer_bmw');		 
			   return $result->result(); 
		   }
		} 		    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}