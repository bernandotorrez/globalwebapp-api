<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_control_history_wrs_bmw extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_master_stockin';
	 
    public function tampil_wrs_history_stock(){ //tampil table untuk ngececk num_row						     	     		  	   			
		 $flag_wrs = "1" ; //flag still wrs
		 $flagwrs_on_move_stock = "0"; //flag already move to stock
		 $status_delete = "1";
		 
		 $this->load->database();		
		 $this->db->select('*');						 		 		 				
		 $this->db->where('delete_flag',$status_delete);		 		
		 $this->db->where("(status='".$flag_wrs."'  OR status='".$flagwrs_on_move_stock."' )", NULL, FALSE); 
		 $this->db->order_by('date_wrs_bodong','desc');
		 $result = $this->db->get('qv_complite_wrs_bodong_bmw');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    
		 		 
		 $flag_wrs = "1" ; //flag still wrs
		 $flagwrs_on_move_stock = "0"; //flag already move to stock	 		 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');										 
		 $this->db->where('delete_flag',$status_delete);		 
		 $this->db->where("(status='".$flag_wrs."'  OR status='".$flagwrs_on_move_stock."' )", NULL, FALSE); 		
		 $this->db->order_by('date_wrs_bodong','desc');	
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_wrs_bodong_bmw')->result();		 		   
		 return $result ;
	}
	
	 public function hitung_jumlah_history_wrs(){ //
	 						     	 
	     $strbranch =  $this->input->post('cbobranchutama');		  	   			 		 	 		
		 $flag_history_wrs = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
	    
				 if  ($strbranch =="" ) : // jika combo branch tidak ada
					 $this->load->database();		
					 $this->db->select('*');									 		 		 				
					 $this->db->where('delete_flag',$status_delete);		 
					 $this->db->where('status',$flag_history_wrs); 
					 $this->db->order_by('date_wrs_bodong','desc');
					 $result = $this->db->get('qv_complite_wrs_bodong_bmw');			     		 
					 return $result;
				 else:	        
					 $this->load->database();		
					 $this->db->select('*');									 		 		 				
					 $this->db->where('branch_id',$strbranch);	
					 $this->db->where('delete_flag',$status_delete);		 
					 $this->db->where('status',$flag_history_wrs); 
					 $this->db->order_by('date_wrs_bodong','desc');
					 $result = $this->db->get('qv_complite_wrs_bodong_bmw');			     		 
					 return $result;
			endif;	 
		 
		  	 
	}
	
	 public function hitung_jumlah_wrs_move_to_stock(){ //tampil table untuk ngececk num_row						     	 
	     $strbranch =  $this->input->post('cbobranchutama');	    		  	   			 		 	 			
		 $flag_history_wrs_move_stock = "0" ; 			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 
		 if  ($strbranch =="" ) : // jika combo branch tidak ada	 
			 $this->load->database();		
			 $this->db->select('*');		 						 		 		 							
			 $this->db->where('delete_flag',$status_delete);		 
			 $this->db->where('status',$flag_history_wrs_move_stock); 
		     $this->db->order_by('date_wrs_bodong','desc');
			 $result = $this->db->get('qv_complite_wrs_bodong_bmw');			     		 
			 return $result;			 
		 else:	 		  
						 
			 $this->load->database();		
			 $this->db->select('*');	
			 $this->db->where('branch_id',$strbranch);		 						 		 		
			 $this->db->where('delete_flag',$status_delete);		 
			 $this->db->where('status',$flag_history_wrs_move_stock); 
		     $this->db->order_by('date_wrs_bodong','desc');
			 $result = $this->db->get('qv_complite_wrs_bodong_bmw');			     		 
			 return $result;			 
	     endif;		 
	}	
		 
	
	public function tampil_cabang_mazda(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 		
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $id_company = "3"; //idcompany 3 MAZDA
		 
		 $this->load->database();		
		 $this->db->select('branch_id,name_branch,place,');									 		 		
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('id_company',$id_company); 
		 $this->db->order_by('branch_id','desc');
		 $result = $this->db->get('tbl_branches');			     		 
		 return $result;
	}							  		
		
	public function get_search_date()
	{
	    $strdatestart =$this->input->post('txtdatestart');	
	    $strdatend =$this->input->post('txtdateend');	
	    $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	    $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal		
		
		 $flag_wrs = "1" ; //flag still wrs
		 $flagwrs_on_move_stock ="0"	; 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif	 
		 
	    if($strdatestart !="" and $strdatend !="" ){	
		   $this->load->database();		
	       $this->db->select('*');
		   $this->db->where('delete_flag',$status_delete);		 		
		   $this->db->where("(status='".$flag_wrs."'  OR status='".$flagwrs_on_move_stock."' )", NULL, FALSE); 
		   $this->db->where('date_wrs_bodong BETWEEN "'.$datestart.'" and "'.$dateend.'"');	
		   $this->db->order_by('date_wrs_bodong','desc');
		   $result = $this->db->get('qv_complite_wrs_bodong_bmw');
		  	  	
	       return $result;		 			  		 	   	 
	    }else{
		    redirect('bmw_stock_sales/c_control_history_wrs_bmw/c_control_history_wrs_bmw');	
		}
		  
	}
	
	function total_history_wrs_date()
	{
		 $strdatestart =$this->input->post('txtdatestart');	
		 $strdatend =$this->input->post('txtdateend');
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif	
		  
		 $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
		 $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		 $flag_history_wrs = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		  
	   if($strdatestart !="" and $strdatend !="" ):
		  $this->load->database();		
		  $this->db->select('*');								 		 		 				
		  $this->db->where('delete_flag',$status_delete);		 		
		  $this->db->where("status",$flag_history_wrs); 
		  $this->db->where('date_wrs_bodong BETWEEN "'.$datestart.'" and "'.$dateend.'"');	
		  $this->db->order_by('date_wrs_bodong','desc');
		  $result = $this->db->get('qv_complite_wrs_bodong_bmw');			     		 
		  return $result;
		endif;
		
	}		
	
	function total_wrs_move_stock_date()
	{
		  $strdatestart =$this->input->post('txtdatestart');	
		  $strdatend =$this->input->post('txtdateend');	
		  $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  
		  $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
		  $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		  $flag_history_wrs_move_stock = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		  
		if($strdatestart !="" and $strdatend !="" ):
			 $this->load->database();		
			 $this->db->select('*');								 		 		 				
			 $this->db->where('delete_flag',$status_delete);		 		
			 $this->db->where("status",$flag_history_wrs_move_stock); 
			 $this->db->where('date_wrs_bodong BETWEEN "'.$datestart.'" and "'.$dateend.'"');	
			 $this->db->order_by('date_wrs_bodong','desc');
			 $result = $this->db->get('qv_complite_wrs_bodong_bmw');				     		 
			 return $result;	
		endif;	 
	}		
	
	function search_data() 	// fungsi cari data pp model
	{
	  // $strbranch          =  $this->input->post('cbobranch');	 	   	   	  	  
	   $kategory = $this->input->post('cbostatus');
	   $cari = $this->input->post('txtcari');
	   
	     $flag_wrs = "1" ; //flag still wrs
		 $flagwrs_on_move_stock = "0"; //flag already move to stock	 		 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	
	   	   	   
	   if ($cari == null or $kategory =='--Choose--') 
	   {
		   redirect('bmw_stock_sales/c_control_history_wrs_bmw/c_control_history_wrs_bmw');  
	   }else{	
	     $this->load->database();			  	   		  		 	    
	     $this->db->where('delete_flag',$status_delete);
	     $this->db->where("(status='".$flag_wrs."'  OR status='".$flagwrs_on_move_stock."' )", NULL, FALSE);
	     $this->db->like($kategory,$cari);		    		   
	     $result = $this->db->get('qv_complite_wrs_bodong_bmw');		 	   
	     $this->session->set_flashdata('cari',$cari);	
		 
	     return  $result;
	   }
	} 	
	
	function total_history_wrs_cari_data()
	{
		$strcari = $this->input->post('txtcari');
		$strcategory = $this->input->post('cbostatus');
		$str_flag_history_wrs= '1';
		$str_flag_delete = '1';
					
		$this->load->database();
		$this->db->select('*') ;
		$this->db->where($strcategory,$strcari);
		$this->db->where('delete_flag',$str_flag_delete);		
		$this->db->where('status',$str_flag_history_wrs);
				
		$result = $this->db->get('qv_complite_wrs_bodong_bmw');
		
		return $result;
			
	} 			  	   			 	
	
	function total_history_wrs_move_stock_cari_data()
	{
		$strcari = $this->input->post('txtcari');
		$strcategory = $this->input->post('cbostatus');
		$str_flag_history_wrs_move_stock = '0';
		$str_flag_delete = '1';				
		
		$this->load->database();
		$this->db->select('*') ;
		$this->db->where($strcategory,$strcari);
		$this->db->where('delete_flag',$str_flag_delete);		
		$this->db->where('status',$str_flag_history_wrs_move_stock);	
		
		$result = $this->db->get('qv_complite_wrs_bodong_bmw');
		
		return $result;
	} 					
	
	function shorting_table_branch() 	// fungsi cari data pp model
	{	   
	   $str_tampil_branch =  $this->input->post('cbobranchutama');	 	   	   	  	  	   	   	   
	   $flag_wrs = "1" ; //flag still wrs
	   $flagwrs_on_move_stock = "0"; //flag already move to stock	 		 		
	   $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	   
	   	   	   
	  if ($str_tampil_branch == "") 
	   {
		   redirect('bmw_stock_sales/c_control_history_wrs_bmw/c_control_history_wrs_bmw');  
	   }else{	
	       $this->load->database();			  	   		  		 	    
		   $this->db->where('branch_id',$str_tampil_branch);
	       $this->db->where('delete_flag',$status_delete);
	       $this->db->where("(status='".$flag_wrs."'  OR status='".$flagwrs_on_move_stock."' )", NULL, FALSE); 	      	    		   		   $this->db->order_by('date_wrs_bodong','desc');	
	       $result = $this->db->get('qv_complite_wrs_bodong_bmw');		 	   	       
		 
	       return  $result;
	   } 
	} 			
	
	function get_type_vehicle()
	{			      
	     $this->load->database();		
		 $this->db->select('id_type,desc_type');			 
		 $this->db->where('status','1');			
		 $result = $this->db->get('tbl_brand_type');			     		 
		 return $result->result();	 	
	}	
						
	
		  	   			 	
						
//EXCEL ALL -----------------------------------------------------------------------	  	   			 	
	 function ToExcelAll(){ // buat narik table to excel						    
	 			 
		 $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		 $str_cbobranch =$this->input->post("cbobranch");
		 
		 $flag_wrs = "1" ; //flag still wrs
		 $flagwrs_on_move_stock = "0"; //flag already move to stock		
		 
		 $tglstart = $this->input->post('txtrptdtstart');
		 $tglend =  $this->input->post('txtrptdtend'); 
		 
		 $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	     $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal 
	   
	  	 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
				 		           		    			 
			 $this->load->database();				 	
			 $this->db->select('*');								 		 
			 
			 if ($str_cbobranch != "") :			 		  
			 	 $this->db->where('branch_id',$str_cbobranch);
			 endif;		
			 						 	
			 if ($str_cbotype != "") :			 		  
			 	 $this->db->where('id_type',$str_cbotype);
			 endif;	 
			 							 	 
			 $this->db->where('delete_flag',$status_delete );			 			 			
			 $this->db->where('date_wrs_bodong BETWEEN "'. $datestart. '" and "'.$dateend.'"');	
			 $this->db->where("(status='".$flag_wrs."' OR status='".$flagwrs_on_move_stock."' )", NULL, FALSE); 			
			 $this->db->order_by('date_wrs_bodong','desc');
			 $result = $this->db->get('qv_complite_wrs_bodong_bmw');
			 			
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
				
		 }else{
				 return null ;				
		 } 		 						 		  								 
	}	 
	
	 function ToExcelAll_total_history_wrs(){ // buat narik table to excel						    
	 			 
		 $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		 $str_cbobranch =$this->input->post("cbobranch");
		 
		 $tglstart = $this->input->post('txtrptdtstart');
		 $tglend =  $this->input->post('txtrptdtend'); 
		 
		 $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	     $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal 
		 
		 $str_flag_history_wrs = '1'; // flag stock berari 1				   
	  	 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
				 		           		    			 
			 $this->load->database();				 	
			 $this->db->select('*');								 		 
			 
			 if ($str_cbobranch != "") :			 		  
			 	 $this->db->where('branch_id',$str_cbobranch);
			 endif;		
			 						 	
			 if ($str_cbotype != "") :			 		  
			 	 $this->db->where('id_type',$str_cbotype);
			 endif;	 
			 							 	 
			 $this->db->where('delete_flag',$status_delete );	
			 $this->db->where('status', $str_flag_history_wrs  );	
			 $this->db->where('date_wrs_bodong BETWEEN "'. $datestart. '" and "'.$dateend.'"');			 			 								
			 $this->db->order_by('date_wrs_bodong','desc');
			 $result = $this->db->get('qv_complite_wrs_bodong_bmw');
			 					
		     return  $result;				
		 
	 }
				
	 function ToExcelAll_total_wrs_move_stock(){ // buat narik table to excel						    
	 			 
		 $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		 $str_cbobranch =$this->input->post("cbobranch");
		 
		 $tglstart = $this->input->post('txtrptdtstart');
		 $tglend =  $this->input->post('txtrptdtend'); 
		 
		 $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	     $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal 
		 
		 $str_flag_wrs_move_stock = '0'; // flag wrs yg sudah move ke stock				   
	  	 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
				 		           		    			 
			 $this->load->database();				 	
			 $this->db->select('*');								 		 
			 
			 if ($str_cbobranch != "") :			 		  
			 	 $this->db->where('branch_id',$str_cbobranch);
			 endif;		
			 						 	
			 if ($str_cbotype != "") :			 		  
			 	 $this->db->where('id_type',$str_cbotype);
			 endif;	 
			 							 	 
			 $this->db->where('delete_flag',$status_delete );	
			 $this->db->where('status', $str_flag_wrs_move_stock  );	
			 $this->db->where('date_wrs_bodong BETWEEN "'. $datestart. '" and "'.$dateend.'"');			 		 			 								
			 $this->db->order_by('date_wrs_bodong','desc');
			 $result = $this->db->get('qv_complite_wrs_bodong_bmw');
			 			
		 	return  $result;
	 }			    	  	
	 
	 
//end------------------------------------------------------------------------------------------------------------------			
				 
}