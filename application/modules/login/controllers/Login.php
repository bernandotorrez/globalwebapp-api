<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{
    public $data = array('pesan'=> '');

	public function __construct()
    {
		parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
		$this->load->model('M_login','login', TRUE);
		$this->load->helper('api_helper');
	}
	
	public function index()
    {
				
		// status user login = BENAR, pindah ke halaman menu
        if ($this->session->userdata('login') == TRUE)
        {			 
			 $session_username = $this->session->userdata('username');	
			 redirect('main_menu/C_main_menu');			
			 //redirect('dashboard/C_dashboard');			
			 
		}
        // status login salah, tampilkan form login
        else
        {
            // validasi sukses
            if($this->login->validasi())
            {                					  
				 //jika user biasa cek di database sukses
				 if($this->login->cek_user())
				 {  
				    redirect('main_menu/c_main_menu');	    				 
					//redirect('dashboard/C_dashboard');																						
				 }else{	
				        // cek database gagal				
						$this->data['pesan'] = 'Incorrect Username And Password.';
						$this->load->view('login/Vlogin', $this->data);
				 }			                         
			}else{
                // validasi gagal
                $this->load->view('login/Vlogin', $this->data);
            }
		}
	}

	
	public function logout()
	{
        $this->login->logout();
		redirect('login');
	}
}
/* End of file login.php */
/* Location: ./application/controllers/login.php */