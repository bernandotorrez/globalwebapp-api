<head>

<title>login User</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <script src="<?php echo base_url('asset/js/jquery.min.js');?>"></script>    
       
    <link rel="shortcut icon" href="<?php echo base_url('asset/images/mazda.ico');?>" >
	<link href="<?php echo base_url('asset/css/bootstrap.css') ?>" rel="stylesheet" >     
    <link href="<?php echo base_url('asset/css/login.css') ?>" rel="stylesheet" >       
    
			        
</head>
<style>
.slideshow
{
    position: relative;
    width: 100%;
    height: 100%;
	
}
.slideshow img
{
    position: absolute;
    width: 100%;
    height: 100%;
    z-index:-1;
} 
    
</style>

<script type="text/javascript">

var images=new Array('<?php echo base_url('asset/images/login_background/bg_login1.jpg'); ?>','<?php echo base_url('asset/images/login_background/bg_login2.jpg'); ?>','<?php echo base_url('asset/images/login_background/bg_login3.jpg'); ?>');
var nextimage=0;

doSlideshow();

function doSlideshow()
{
    if($('.slideshowimage').length!=0)
    {
        $('.slideshowimage').fadeOut(3000,function(){slideshowFadeIn();$(this).remove()});
    }
    else
    {
        slideshowFadeIn();
    }
}

function slideshowFadeIn()
{
    $('.slideshow').prepend($('<img class="slideshowimage" src="'+images[nextimage++]+'" style="display:none">').fadeIn(3000,function(){setTimeout(doSlideshow,3000);}));
    if(nextimage>=images.length)
        nextimage=0;
}

</script>


<body class="slideshow">
<!--- <div class="logo_show"></div> --->
<div class="container"  align="center" style="padding-top: 150px;">
  <div class="row">
     <div class="col-md-4 col-md-offset-4">
       <div class="panel panel-default transparan" id="backgroun_rounded" >
          <div class="panel-body">
            
<?php
	$attributes = array('name' => 'login_form',
						  'id' => 'login_form',
						  'class' => '');
	echo form_open('login', $attributes);		
?>

    
 
 
  <!-- <img src="<?php //echo base_url('assets/images/logo/logonew.png'); ?>"/> -->
  <h4>Webapp EurokarsGroup</h4>   
  <label for="User Name" class="sr-only">User Name</label>    
  <div class="input-group">
  
      <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>   
  <input name="username" type="text" class="form-control" value="<?php echo set_value('username');?>" placeholder="User Name"><br />
   </div>    
	
    <div style="color:#FF1A1F"> <?php echo form_error('username', '<p class="field_error">', '</p>');?>	</div>	
    
	<p>    
      <label for="Password" class="sr-only">Password</label> 
      <div class="input-group">
      		<div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div> 
      <input name="password" type="password" class="form-control" value="<?php echo set_value('password');?>" placeholder="Password">
      </div>
    </p>
       
       <div style="color:#FF1A1F"><?php echo form_error('password', '<p class="field_error">', '</p>');?></div>		
       
    <p>    
    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-off"></span> Login</button>
    </p>
    <div class="label_copyright">Copyright @<?php  echo date("Y"); ?></div>   
    </form>   
      <!-- pesan start -->
       <?php if (! empty($pesan)) : ?>
            <p id="message">
                <div class="label_error"><?php echo $pesan; ?></div>
            </p><?php endif ?> 
        <!-- pesan end -->        
             </div>
          </div>
        </div>
    </div>    
</div>

</body>
</html>
