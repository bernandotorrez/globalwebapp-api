<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_login extends CI_Model {

	public $db_tabel = 'qv_complite_user';
	
	public function __construct() {
		parent::__construct();

		$this->load->helper('api_helper');
	}

    public function load_form_rules()
    {
        $form_rules = array(
                            array(
                                'field' => 'username',
                                'label' => 'Username',
                                'rules' => 'required'
                            ),
                            array(
                                'field' => 'password',
                                'label' => 'Password',
                                'rules' => 'required'
                            ),
        );
        return $form_rules;
    }

    public function validasi()
    {
        $form = $this->load_form_rules();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    // cek status user, login atau tidak?
    public function cek_user()
    {
        $username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$param = array('username' => $username, 'password' => $password);
		
		// With API
		$query = http_request_post(API_URL.'/login/doLogin', $param);
		$query = json_decode($query, true);

        if ($query['message'] == 'success')
        {
			
			//jika login benar
			 $time = time();
			 $ip = $_SERVER['REMOTE_ADDR'];
			 $location = $_SERVER['PHP_SELF'];
			 $data1 = array(
				 	'username' => $username,
					'waktu' => $time,
					'ip' => $ip
					);

			$username = $query['data']['username'];

			$update = http_request_put(API_URL.'/login/updateSuccessLogin', $data1);
			$update = json_decode($update, true);

             $data = array('username' => $username,
			 'login' => TRUE);

			//panggil field field yg dibutuhkan untuk disimpan pada session{
				$data['id']=$query['data']['id'];
				$data['name']=$query['data']['name'];
				$data['username']=$query['data']['username'];
				$data['head_user']=$query['data']['sign_head'];
				$data['id_group']=$query['data']['id_group'];
				$data['name_group']=$query['data']['name_grp'];
				$data['branch_id']=$query['data']['branch_id'];
				$data['email']=$query['data']['email'];
				$data['email_head']=$query['data']['email_head'];
				$data['email_cc']=$query['data']['email_cc'];
				$data['email_fc']=$query['data']['email_fc'];
				$data['email_bod']=$query['data']['email_bod'];
				$data['email_fa']=$query['data']['email_fa'];
				$data['phone']=$query['data']['phone'];
				$data['name_branch']=$query['data']['name_branch'];
				$data['branch_short']=$query['data']['branch_short'];
				$data['address_branch']=$query['data']['address'];
				$data['phone_branch']=$query['data']['phone_company'];
				$data['fax_branch']=$query['data']['phone_company'];
				$data['city']=$query['data']['city'];
				$data['id_company']=$query['data']['id_company'];
				$data['company']=$query['data']['company'];
				$data['short']=$query['data']['short'];
				//$data['id_divisi']=$query['data']['id_divisi;
				$data['id_dept']=$query['data']['id_dept'];
				$data['id_initial']=$query['data']['id_initial'];
				$data['dept']=$query['data']['dept'];
				$data['apps_acces']=$query['data']['apps_acces'];
				$data['aproval_flag']=$query['data']['aproval_flag'];	//1=B.o.D 2=F.C 3=Manager up 4 = purchase
				$data['give_righ_flag']=$query['data']['give_righ_flag'];	//1=B.o.D 2=F.C 3=Manager up
				$data['status']=$query['data']['status'];
				$data['sign_head']=$query['data']['sign_head'];
				$data['sign_fc']=$query['data']['sign_fc'];
				$data['sign_bod']=$query['data']['sign_bod'];

				$data['sign_penerima'] =$query['data']['sign_penerima'];
				$data['sign_finance'] =$query['data']['sign_finance'];
				$data['sign_accounting'] =$query['data']['sign_accounting'];
				$data['sign_pemeriksa1'] =$query['data']['sign_pemeriksa1'];
				$data['sign_pemeriksa2'] =$query['data']['sign_pemeriksa2'];
				$data['sign_pimpinan'] =$query['data']['sign_pimpinan'];
				$data['sign_giro1'] =$query['data']['sign_giro1'];
				$data['sign_giro2'] =$query['data']['sign_giro2'];
				$data['sign_penerima_bmk'] =$query['data']['sign_penerima_bmk'];
				$data['apps_accsess']=$query['data']['apps_access'];
				$data['level_access']=$query['data']['level_access'];
				$data['login_terakhir']=$query['data']['login_terakhir'];
				$data['approval_democar']=$query['data']['flag_approval_democar'];
				$data['flag_inv_receipt'] = $query['data']['flag_inv_receipt'];
				$data['flag_deligated'] = $query['data']['flag_deligated'];
				$data['flag_com_inv_receipt'] = $query['data']['flag_com_inv_receipt'];
				$data['flag_dept_inv_receipt'] = $query['data']['flag_dept_inv_receipt'];
				$data['email_purchase'] = $query['data']['email_purchase'];
				$data['api_token'] = $query['data']['api_token'];
				//$buff_aproval_flag = $query['data']['aproval_flag;
			 
			    // buat data session data yg diatas.
                $this->session->set_userdata($data);
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }


  public function logout()
  {
   //mengatur jam sesuai zona negara,lalu disimpan dalam variable untuk diupdate ke ke kolum login terakhir

		date_default_timezone_set('Asia/Bangkok'); 
		$ddate = date('d F Y');
		$strfudate= $ddate.", ".date('H:i:s');
		$strid_user =$this->session->userdata('id');

		$data=array('id' => $strid_user, 'login_terakhir'=>$strfudate, 'waktu'=>'');
		//$this->db->where('id',$strid_user);
		//$this->db->update('tbl_user',$data); // update login terakhir user

		$update = http_request_put(API_URL.'/login/doLogout', $data);
		$update = json_decode($update, true);

        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
        $this->session->sess_destroy();
  }

}
/* End of file login_model.php */
/* Location: ./application/models/login_model.php */
