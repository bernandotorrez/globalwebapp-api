<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_create_bpk extends CI_Model {	
				    
    public $db_tabel = 'qv_head_pp_complite';
	 
    public function tampil_add_pp(){ //tampil table untuk ngececk num_row						     	     		  
	     $ses_id_dept = $this->session->userdata('id_dept'); //manggili session id 
		 $ses_id_company = $this->session->userdata('id_company');  	
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 
		 $short_company = $this->session->userdata('short');	 		 		
		 $ses_branch_short = $this->session->userdata('branch_short'); 
		 $ses_dept = $this->session->userdata('dept');	 		 
		 
		  		 		 
		 $str_flag_bpk = "1" ; //flag flag_bpk jika 1 sudah di input dari form bpk	
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('id_company',$ses_id_company);	
		 $this->db->where('branch_id',$ses_id_branch );						 
		 $this->db->where('status',$status_delete);
		 $this->db->where('flag_bpk',$str_flag_bpk);			
		 $this->db->order_by('date_pp','desc');
		 $result = $this->db->get('qv_head_pp_complite');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	     $ses_id_dept = $this->session->userdata('id_dept'); 
		 $ses_id_company = $this->session->userdata('id_company');  	
		 $ses_id_branch = $this->session->userdata('branch_id');
		 $short_company = $this->session->userdata('short');	 		 		
		 $ses_branch_short = $this->session->userdata('branch_short'); 
		 $ses_dept = $this->session->userdata('dept');	 		 		
		
		 $str_flag_bpk = "1" ; //flag flag_bpk jika 1 sudah di input dari form bpk		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  	     		 
	     $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('id_company',$ses_id_company);					
		 $this->db->where('branch_id',$ses_id_branch );	
		
		 $this->db->where('status',$status_delete);
		 $this->db->where('flag_bpk',$str_flag_bpk);					  		 
		 $this->db->order_by('date_pp','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_head_pp_complite')->result();		 		   
		 return $result ;
	}	
		
   /*  public function select_query_rpt_bpk()
	 {		 
		  
	    $strid_master =$this->input->post('msg'); 	 
		$str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete	
		$str_flag_adjustment = "0";// jika "1" detail merupakan adjustmen		
		  //select field buat di cetak ------------------------------------	
			  $query = $this->db->select("desc,spec,qty,harga,total,vendor,currency,gran_total,gran_total_adjust,user_submission,head_user,note_bpk,date_send_aproval,date_aproval,date_print_bpk,flagadjust")
								->where('id_master',$strid_master)
								->where('status',$str_status_terdelete)
								->where('flagadjust',$str_flag_adjustment)
								->get('qv_master_det_pp');
			  if ($query->num_rows() >= 1)
			  {		
			     //Update flag pada table master_PP bpk yg sedang di cetak ------------------------------------
				  $str_give_flag_bpk= "1";		
				  $data_flag =array("flag_print_bpk" => $str_give_flag_bpk, //give_flag_printbpk																									
									"date_print_bpk" => date('Y-m-d')				
									);	
				  $this->db->where('id_master',$strid_master);
				  $this->db->update('tbl_master_pp',$data_flag); 
				  //--------------------------------------------------------------------------------------
			  	 foreach ($query->result() as $row)
				 {
			  		$data = array('sesidmaster'      => $strid_master,							
								  'ses_vendor'       => $row->vendor,								  	
								  'ses_user_sub'     => $row->user_submission,	
								  'ses_head'         => $row->head_user,
								  'ses_notebpk'      => $row->note_bpk,									  
								  'ses_datesend'     => $row->date_send_aproval,	
								  'ses_dateapproval' => $row->date_aproval,
								  'ses_datecair'     => $row->date_print_bpk,	
								  'ses_curr'	     => $row->currency,								  									
								  'ses_grand'        => $row->gran_total,
								  'ses_grand_adjust' => $row->gran_total_adjust
								  ); //buat array di session		
					$this->session->set_userdata($data); //simpan di session
				 }													 										  
					return $query->result();																 													
			  }else{				  
					 return false;					  
			  }			  
	 } */										  		
				    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}