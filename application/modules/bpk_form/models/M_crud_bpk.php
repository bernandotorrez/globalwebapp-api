<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_crud_bpk extends CI_Model {	
				
    public $db_tabel_qv       = 'qv_head_pp_complite';
	public $db_table_pp	      = 'tbl_master_pp';  
	public $db_table_pp_det   = 'tbl_detail_pp';          
	
	
	/* function getppnumber(){	//old counter 
	 	 
		$strshort = $this->session->userdata('short');//short company
        $strdept = substr(($this->session->userdata('dept')),0,3);
		$strbranchshort = $this->session->userdata('branch_short');
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");	
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
		 
 	    $q = $this->db->query("select MAX(RIGHT(id_master,4)) as code_max from tbl_master_pp where left(id_master,2)='".substr($strshort,0,2)."'");
				
								
		$code = "";					
				if($q->num_rows()>0){
					foreach($q->result() as $cd){
						$tmp = ((int)$cd->code_max)+1;
					    $code = sprintf("%04s", $tmp);;
					}
				}else{
					$code = "0001";
				}
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	    return substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$code ;			 
    } */
	
	function getppnumber(){	// view counter number when add new
	 	 
		$strshort = $this->session->userdata('short');//short company
		$stridcom = $this->session->userdata('id_company');//id company
        $strdept = substr(($this->session->userdata('dept')),0,3);
		$strbranchshort = $this->session->userdata('branch_short');
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");			    
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
		$id_dept = $this->session->userdata('id_dept');
				    
		 // ada perubahan disini
		 //$qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_ppno where id_company='".$stridcom."'");
		 
		 $qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_ppno where id_dept='".$stridcom."'");
									  	   
	    $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); 	//check tanggal
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
				if($qcounter->num_rows()== 1):
					foreach($qcounter->result() as $cd):
						$tmp = ((int)$cd->code_max);
						$code = sprintf("%04s", $tmp);;
					endforeach;
					
					 if ($code== '0000') :						
						 $code = "0001";					 
					 endif;
					 
				 else:					  
					$code = "0001";								
				 endif;		
		 else:		
		       $code = "0001";					 
		 endif;
		 
	 //update tanggal ketable date--------------
	 $data = array('date_add'=>$strdates );																			
	 $this->db->update('tbl_date_compare',$data); 		 
	 //end--------------------
		 
	 //update tanggal ketable counter--------------
	  $data = array('counter'=> $code );	

	  // ada perubahan
	  //$this->db->where('id_company',$stridcom);
	  $this->db->where('id_dept',$stridcom);															  									
	  $this->db->update('tbl_counter_ppno',$data); 		 
	 //end--------------------
		 
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	  return substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$strdates."/".$code ;			        //end------------------------------------------			 
    }
	
	function counterppnumber(){	//counter number when save data
	 	 
		$strshort = $this->session->userdata('short');//short company
		$stridcom = $this->session->userdata('id_company');//id company
        $strdept = substr(($this->session->userdata('dept')),0,3);
		$strbranchshort = $this->session->userdata('branch_short');
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");			    
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
		$id_dept = $this->session->userdata('id_dept');
				    
		 // ada perubahan
 	    //$qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_ppno where id_company='".$stridcom."'");
		 $qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_ppno where id_dept='".$stridcom."'");					  	   
	    $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); //check tanggal
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
					if($qcounter->num_rows()>0):
						foreach($qcounter->result() as $cd):
							$tmp = ((int)$cd->code_max)+1;
						    $code = sprintf("%04s", $tmp);;
						endforeach;
						 if ($code== '0000') :						
						     $code = "0001";					 
					 	 endif;
					 else:					
						 $code = "0001";					 
					 endif;		
		 else:	
		       $code = "0001";					 
		 endif;
		 
		 //update tanggal ketable date--------------
			 $data = array('date_add'=>$strdates );																			
			 $this->db->update('tbl_date_compare',$data); 		 
		 //end--------------------
		 
		 //update tanggal ketable counter--------------
			 $data = array('counter'=> $code );	

			 // ada perubahab
			 //$this->db->where('id_company',$stridcom);	
			 $this->db->where('id_dept',$stridcom);														  									
			 $this->db->update('tbl_counter_ppno',$data); 		 
		 //end--------------------
		 
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	    //end------------------------------------------					 
	      return true;
    }
	
	function get_vendor()
	{
		 $id_company = $this->session->userdata('id_company');
	     $id_dept = $this->session->userdata('id_dept');
		 $status_del = "1"; //flag status delete pada table vendor
		 		 		 
	     $this->load->database();		
		 $this->db->select('id_vendor,vendor');		
		 $this->db->where('id_company',$id_company);
		 $this->db->where('id_dept',$id_dept);
		 $this->db->where('status',$status_del);
		 $result = $this->db->get('tbl_vendor');			     		 
		 return $result->result();	 	
	}
	
	function get_bank()
	{		
	     $this->load->database();		
		 $this->db->select('name_bank');				
		 $result = $this->db->get('tbl_bank');			     		 
		 return $result->result();	 	
	}	
	
	function get_currency()
	{		
	     $this->load->database();		
		 $this->db->select('id_curr,currency');				
		 $result = $this->db->get('tbl_curr');			     		 
		 return $result->result();	 	
	}	
	 
		
	function insert_master_and_detail()	
	{ 
	     //-------------------Checck Duplicate NOPP / Idmaster		 
		 $ponumber = $this->input->post('txtnopp'); 		 		 
		 $query2=   $this->db->select('*')
						 ->where('id_master',$ponumber)	 //Checck Duplicate NOpo / Idmaster					
						 ->get('tbl_master_pp');		
		$id_user = $this->session->userdata('id');
		 if ($query2->num_rows() == 1) // Jika ketemu buat NOPP baru.
		 {					 			  	
			$strshort = $this->session->userdata('short');//short company
			$stridcom = $this->session->userdata('id_company');//id company
			$strdept = substr(($this->session->userdata('dept')),0,3);
			$strbranchshort = $this->session->userdata('branch_short');
			$stryear = substr(date("Y"),-3,3);
			$strmonth = date("m");			    
			$strdates = date("d");	//date (tanggal buat comparasi pada table)
			$id_dept = $this->session->userdata('id_dept');

			// tambahan
			
				    
			// ada perubahan
			 //$qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_ppno where id_company='".$stridcom."'");
			 $qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_ppno where id_dept='".$stridcom."'");
									  	   
	        $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); //check tanggal
	   
			$code = ""; //empty code
			 if ($qdate->num_rows()== 1) :			
						if($qcounter->num_rows()>0):
							foreach($qcounter->result() as $cd):
								$tmp = ((int)$cd->code_max)+1;
								$code = sprintf("%04s", $tmp);;
							endforeach;
							 if ($code== '0000') :						
								 $code = "0001";					 
							 endif;
						 else:					
							 $code = "0001";					 
						 endif;		
			 else:	
				   $code = "0001";					 
			 endif;
		 
		    //update tanggal ketable date--------------
			  $data = array('date_add'=>$strdates );																			
			  $this->db->update('tbl_date_compare',$data); 		 
		   //end--------------------
		 
		     //update tanggal ketable counter--------------
			   $data = array('counter'=> $code );	

			   // ada perubahan
			  //$this->db->where('id_company',$stridcom);	
			  $this->db->where('id_dept',$stridcom);														  									
			  $this->db->update('tbl_counter_ppno',$data); 
			  //buat NOPP Baru jika duplicate						  
		    return  substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$code;				
		 }else{			
		      //Ambil NOPP yang ada di inputbox posting	 jika tidak duplicate			 
			  $txtmasterid = $this->input->post('txtnopp'); 	  	   	  
	     }
		      //----mengambil data dari inputbox/textbox lalu di simpan didalam variable
			  $ses_idcom = $this->session->userdata('id_company');
			  $ses_dept = $this->session->userdata('id_dept');
			  $ses_branch = $this->session->userdata('branch_id');	  
			  $strhead_user= $this->session->userdata('head_user');
			  $cbobank = $this->input->post('cbobank');
			  $cbovendor = $this->input->post('cbovendor'); 
			  $txtsub	= $this->input->post('txtsub');	
			  $txt_desc_detail = $this->input->post('txtdesc[0]'); //*from description detail	  		  		  	  		   	  
			  $txtdate = date("Y-m-d");
			  
			  $txtemail =  $this->input->post('txtmail'); 	 	  
			  $txtphone = '-'; 			
			  $idcurr = $this->input->post('cbocur');
			  
			  //---------------------unformat grandtotal---------
			  $txt_grand_total =$this->input->post('result'); 
			  if ( strstr( $txt_grand_total, ',' ) ) $txt_grand_total = str_replace( ',','', $txt_grand_total );  
			  //-------------------------------------
			  $strgiro = $this->input->post('txtgirono');
			  $txtterm = '1'; 				
			  $straprove_head = '1';
			  $straprove_fc = '1';
			  $straprove_bod = '1';	
			  $str_flag_fpb = '1';	
			  $str_flag_print_fpb = '1';	
			  $str_flag_print_bpk = '0';	
			  $str_flag_bpk='1';	
			  $status_aktif = '1';
			  $flag_statpurchase = "1";
			  			 			  	  	  	  	  	  	  			  			  			
			  	
			  $strremrks = $this->input->post('txtremarks');
								
									 
			//---------------------------------------------- insert tbl_master_pp		      
			
			$data=array('id_master'=>$txtmasterid,
						'id_company'=>$ses_idcom, 
						'id_dept'=>$ses_dept,
						'branch_id'=>$ses_branch,
						'id_vendor'=>$cbovendor, 
						'user_submission'=>$txtsub,												
						'head_user'=>$strhead_user,
						'header_desc'=>$txt_desc_detail, //*from description detail
						'date_pp'=>$txtdate,
						'email_pp'=>$txtemail,
						'phone_pp'=>$txtphone,	
						'giro_no' =>$strgiro,	
						'bank' => $cbobank,																											
						'gran_total'=>$txt_grand_total,	
						'term_top'=>$txtterm,
						'id_curr' =>$idcurr,								
						'aprove_head'=>$straprove_head,
						'aprove_fc'=>$straprove_fc,									
						'aprove_bod'=>$straprove_bod,	
						'date_aproval'=> date('Y-m-d'),										
						'status'=>$status_aktif,
						'status_send_aprove'=> date('Y-m-d'),
						'remarks'=> $strremrks,
						'note_bpk'=> $strremrks,
						'flag_fpb'=> $str_flag_fpb,
						'flag_print_fpb'=> $str_flag_print_fpb,	
						'flag_print_bpk'=> $str_flag_print_bpk,
						'flag_bpk'=> $str_flag_bpk,		
						'flag_purchase' => 	$flag_statpurchase ,					
						'date_send_aproval' => date('Y-m-d'),
						'counter_reprint' => "0",
						'id_user' => $id_user								
						);					
										
			$this->db->insert('tbl_master_pp',$data);	
								

			//------------------detail------------------------------------------------													 				
			$desc = $this->input->post('txtdesc');										
			$result = array();					
				foreach($desc AS $key => $val)
				{					 											  
					 $result[1] = array(			
					  "id_master"   => $_POST['txtidmasdet'][$key],			  
					  "desc"  		=> $_POST['txtdesc'][$key],						  
					  "spec"  		=> $_POST['txtspecs'][$key],
					  "qty"  		=> str_replace(',','',$_POST["txtqty"][$key]),
					  "harga"  		=> str_replace(',','',$_POST["txtprice"][$key]),
					  "total"  		=> str_replace(',','',$_POST["txtamount"][$key]),
					  "flagadjust"  => '0',
					  "keterangan"  => 'Normal',
					  "status"		=> $_POST['flagstat'][$key] 					  			
					 );
					 
				 $test= $this->db->insert_batch('tbl_detail_pp', $result);
				} 					
				return true;			     					
		 
	}
	
	
	function edit_bpk()	
	{
		//master---------------------------------------post		  
	 	  $txtmasterid = $this->input->post('txtnopp'); 	
		  $txtsub	= $this->input->post('txtsub');		
		  $cbovendor = $this->input->post('cbovendor'); 
		  $cbobank = $this->input->post('cbobank');	
		  $cbocur = $this->input->post('cbocur'); 	
		  $txtgiro = $this->input->post('txtgirono');	
		  $txtterm = $this->input->post('txtterm');	 
		  $txtremarks = $this->input->post('txtremarks'); 			 		 
		  $txtdesc_detail = $this->input->post('txtdesc[0]');				  
		  
		  //---remove row detail pp from fom_edit pp array format ------------		  
		   $get_iddet_remove_row = $this->input->post('txtiddet_buff') ;
		
	       if(isset($get_iddet_remove_row) && trim($get_iddet_remove_row!='')){					 			   
			   $stsrconvert_iddet = explode(",",$get_iddet_remove_row) ;				   
			   //print_r($stsrconvert_iddet) ;
			   //echo count($stsrconvert_iddet);			   
			   $flag_remove = "0";		   			   
			   for ($intiddet=0; $intiddet < count($stsrconvert_iddet) ; $intiddet++)
			   {				   									   				   	   
					//table detail 								
					$data = array('status'=>$flag_remove);	
										
					$this->db->where('id_detail', $stsrconvert_iddet[$intiddet]);
					$this->db->update('tbl_detail_pp',$data);						   	 				  
			   }  			
		   }		  
		  //end---------------------------------------------------------------------------
		  
		  
		  
		  //---------------------unformat grandtotal---------
			 $txt_grand_total =$this->input->post('result'); 
			  if ( strstr( $txt_grand_total, ',' ) ) $txt_grand_total = str_replace( ',','', $txt_grand_total );  
		  //end------------------------------------- 
		  
		   if($this->input->post('btnsave_edit')){ //Check apakah dari button save atau dari button send
			     $strsend_aproval = '0'; //flag 0 hanya disampan saja
				 $date_send ="";
		   }else{
			  	 $strsend_aproval = '1'; //flag 1 disimpan sekaligus di kirim.
				 $date_send = date("Y-m-d");
		   }		 		 
		 
		
		  $txtmasterid = $this->input->post('txtnopp'); 			 
		  if (isset($txtmasterid) && trim($txtmasterid!=''))
		  {
			    $query = $this->db->select("id_master") //check id booking pada table_parent 
									->where('id_master',$txtmasterid)
									->get('tbl_master_pp');
				  if ($query->num_rows() == 1)
				  {				
				    //master------------------------------
				  	$data = array( 'user_submission'=>$txtsub,
								   'header_desc'=>$txtdesc_detail, 								  
								   'id_vendor'=>$cbovendor,
								   'id_curr'=>$cbocur,
								   'giro_no'=>$txtgiro,
								   'bank' => $cbobank,
								   'term_top'=>$txtterm,
								   'remarks'=>$txtremarks,
								   'note_bpk'=>$txtremarks,	
								   'status_send_aprove'=>$strsend_aproval,								  
								   'gran_total'=>$txt_grand_total,
								   'date_send_aproval' => $date_send	
									);					
					$this->db->where('id_master',$txtmasterid);
					$this->db->update('tbl_master_pp',$data); 
					
					
					//nyimpen session di array untuk session
					$buff=array('ses_noppnew'=> $txtmasterid,			 			 
								 'ses_usersubnew'=> $txtsub,
								 'ses_desc_detail'=> $txtdesc_detail,						 						
								 'ses_grandtotal'=>$txt_grand_total,
								 'ses_term'=>$txtterm,
								 'ses_strsend_aproval'=>$strsend_aproval ,
								 'ses_remarks'=>$txtremarks) ;					   
					$this->session->set_userdata($buff); //simpan di session untuk di tampilkan kedalam email
					//---------------------------------------------------------------------------	
					
					
					//detail---------------------------					
					  $id_det = $this->input->post('txtiddet');											
					  if ($id_det != "") {
							foreach($id_det AS $key => $val)
							{
								if ($_POST['txtiddet'][$key]!=""){ //jika hanya edit row detail 
								    // echo "masuk edit" ;				 											
									 $result[1] = array(								   
									  "id_detail"   => $_POST['txtiddet'][$key],										
									  "desc"  		=> $_POST['txtdesc'][$key],						  
									  "spec"  		=> $_POST['txtspecs'][$key],
									  "qty"  		=> str_replace(',','',$_POST["txtqty"][$key]),
									  "harga"  		=> str_replace(',','',$_POST["txtprice"][$key]),
									  "total"  		=> str_replace(',','',$_POST["txtamount"][$key])							
									  );												
									  $this->db->update_batch('tbl_detail_pp',$result,'id_detail');
								}else{  //jika edit row detail dan juga addnew rowdetail	
								    //  echo "masuk save" ;				 												
								      $result[1] = array(			
									  "id_master"   => $_POST['txtidmasdet'][$key],			  
									  "desc"  		=> $_POST['txtdesc'][$key],						  
									  "spec"  		=> $_POST['txtspecs'][$key],
									  "qty"  		=> str_replace(',','',$_POST["txtqty"][$key]),
									  "harga"  		=> str_replace(',','',$_POST["txtprice"][$key]),
									  "total"  		=> str_replace(',','',$_POST["txtamount"][$key]),
									  "flagadjust"	=> "0",		
									  "keterangan"	=> "Normal",									  
									  "status"		=> "1" 					  			
									 );									 
								    $test= $this->db->insert_batch('tbl_detail_pp', $result);
								}
									
							} 
					     }
				  	  return true;
				  }
				  
				  
		  }else{
			  return false;
		  }
		  
	}
	
	
	function getedit_master_view_bpk()
	{
	  $get_edit_nopp =$this->input->post('msg');	  		 				    							   				      $flag_status_master = "1" ;
	 
	  if (isset($get_edit_nopp) && trim($get_edit_nopp!=''))
	   {		
	           for ($i=0; $i < count($get_edit_nopp) ; $i++) { 		
					 $this->db->select("id_master,company,name_branch,id_vendor,vendor,bank,id_curr,currency,giro_no,note_bpk,gran_total")	;
					 $this->db->where('id_master',$get_edit_nopp[$i]);
					 $this->db->where('status',$flag_status_master);											  	
					 $result_master = $this->db->get('qv_head_pp_complite');							    				
			   }						
		      return $result_master->result() ;							  		  
		}else{
			  return false;
	    }   
	}
	
	function get_edit_detail_bpk()
	{		
		 
      $get_edit_nopo =$this->input->post('msg');	  		 				    							   				      $flag_status_det = "1" ;
	    	   					  				   			  			   	 		  
	  if (isset($get_edit_nopo) && trim($get_edit_nopo!=''))
	  {		
		  for ($i=0; $i < count($get_edit_nopo) ; $i++) { 	
			 $this->db->select("id_detail,id_master,desc,spec,qty,harga,total")	;
			 $this->db->where('id_master', $get_edit_nopo[$i]);	
			 $this->db->where('status', $flag_status_det);										  	
			 $result = $this->db->get('tbl_detail_pp');							    				
		   }						
		  return $result->result() ;							  		  
	  }else{
		  return false;
	  }
		
	}
	
	
					
	
	
	function delete_with_edit_flag() {				
		  $delete = $this->input->post('msg');		  				  		 
		  $flag_del = '0';
		  
		  if(isset($delete) && trim($delete!=''))
		  {	   
			  // --------delete table master dan detail pp menggunakan flag status
			    
				for ($i=0; $i < count($delete) ; $i++) { 				    
				    //array variable flag del
					
				    $data = array('status'=>$flag_del);	
					
					//master table 													
					$this->db->where('id_master',$delete[$i]);
					$this->db->update('tbl_master_pp',$data); 						   				   	   
				   
				    //master detail 													
				    $this->db->where('id_master', $delete[$i]);
				    $this->db->update('tbl_detail_pp',$data);						   				   	   					  				  
			    }
			   					  	   
			   return true;
		  }else{
			   return false;	  			  		  
		  }
	}
	
	
	function delete_checked_pp() {				
		  $delete = $this->input->post('msg');		  				  		 
		  
		  if(isset($delete) && trim($delete!=''))
		  {	   
			 // --------delete table master dan detail pp langsung ke table
			    
				for ($i=0; $i < count($delete) ; $i++) { 				    
				   $this->db->where('id_master', $delete[$i]);
				   $this->db->delete('tbl_master_pp');						   				   	   
				   
				   $this->db->where('id_master', $delete[$i]);
				   $this->db->delete('tbl_detail_pp');						   				   	   			  				   
			    }
			   					  	   
			   return true;
		  }else{
			   return false;	  			  		  
		  }
	}
	
	public function get_search_date()
	{
	    $strdatestart =$this->input->post('txtdatestart');	
	    $strdatend =$this->input->post('txtdateend');	
	    $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	    $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal	    		
	    $ses_id_company = $this->session->userdata('id_company');		
		$str_flag_bpkform ="1" ;	 
		$str_flag_del ="1" ;	 
		 
	    if($strdatestart !="" and $strdatend !="" ){	
		   $this->load->database();		
	       $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,date_pp,term_top,user_submission,flag_fpb,status,status_send_aprove,short,currency,gran_total,attach_quo');
		   $this->db->where('id_company',$ses_id_company);
		   $this->db->where('flag_bpk',$str_flag_bpkform);
		   $this->db->where('status',$str_flag_del);
		   $this->db->where('date_pp BETWEEN "'.$datestart.'" and "'.$dateend.'"');
		   $this->db->order_by('date_pp','desc');
	 	   $result = $this->db->get('qv_head_pp_complite');		
	       return $result;		 			  		 	   	 
	    }else{
		  redirect('bpk_form/C_create_bpk');
		}
		  
	}			
	
	
	function search_data() 	// fungsi cari data pp model
	{
	   $cari = $this->input->post('txtcari');
	   $kategory =  $this->input->post('cbostatus');	  
	   
	   $ses_company = $this->session->userdata('id_company');
	   $ses_id_dept = $this->session->userdata('id_dept');
	   $ses_branch_id = $this->session->userdata('branch_id');
	   $str_flag_bpkform ="1" ;	   
	   $str_flag_del ="1" ;
	   
	   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
	   {
		   redirect('bpk_form/C_create_bpk');  //direct ke url add_menu_booking/add_menu_booking
	   }else{	
		   $this->load->database();		   		 
		   $this->db->where('id_company',$ses_company);
		   //$this->db->where('id_dept',$ses_id_dept);
		   $this->db->where('branch_id',$ses_branch_id);
		   $this->db->where('flag_bpk',$str_flag_bpkform);
		   $this->db->where('status',$str_flag_del);
		   $this->db->like($kategory,$cari);
		   $result = $this->db->get('qv_head_pp_complite');		 
		   return $result->result(); 
	   }
	   	   	   	   
	} 
	 
  public function check_flag_bpk()// validasi untuk bpk jika nilai flag = 1 sudah tidak bisa di cetak lagi 
	{
		 $strid_master =$this->input->post('txtppno'); 		
		 $str_give_flag_bpk= "1";	  						  					    						   	 			   	   					  				   			  			   			
						
				  $query = $this->db->select("id_master") 
									->where('id_master',$strid_master)
									->where('flag_print_bpk',$str_give_flag_bpk)
									->get('qv_master_det_pp');
				  if ($query->num_rows() >= 1)
				  {					  
					return true;				
				  }
				  			
	}			 
 public function select_query_rpt_bpk()
	 {		 
		
		$strid_master =$this->input->post('msg'); 	 
	    for ($i=0; $i < count($strid_master) ; $i++) { 	
		     $str_hasil_idamaster = $strid_master[$i];			 
		}
		
		$update_date_bpk = array('date_print_bpk' => date('Y-m-d'));		
		$this->db->where('id_master',$str_hasil_idamaster);
		$this->db->update('tbl_master_pp',$update_date_bpk);
		//--------------------------------------------
		
		$str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete	
		$str_flag_adjustment = "0";// jika "1" detail merupakan adjustmen		
		  //select field buat di cetak ------------------------------------	
			  $query = $this->db->select("desc,spec,qty,harga,total,vendor,currency,gran_total,gran_total_adjust,user_submission,head_user,note_bpk,giro_no,date_send_aproval,date_aproval,date_print_bpk,bank,flagadjust")
								->where('id_master',$str_hasil_idamaster)
								->where('status',$str_status_terdelete)
								->where('flagadjust',$str_flag_adjustment)
								->get('qv_master_det_pp');
			  if ($query->num_rows() >= 1)
			  {		
			      //Update flag pada table master_PP bpk yg sedang di cetak ------------------------------------
				  $str_give_flag_bpk= "1";		
				  $data_flag =array("flag_print_bpk" => $str_give_flag_bpk, //give_flag_printbpk																									
									"date_print_bpk" => date('Y-m-d')				
									);	
				  $this->db->where('id_master',$str_hasil_idamaster);
				  $this->db->update('tbl_master_pp',$data_flag); 
				  //--------------------------------------------------------------------------------------
			  	 foreach ($query->result() as $row)
				 {
			  		$data = array('sesidmaster'      => $str_hasil_idamaster,							
								  'ses_vendor'       => $row->vendor,								  	
								  'ses_user_sub'     => $row->user_submission,	
								  'ses_head'         => $row->head_user,
								  'ses_notebpk'      => $row->note_bpk,									  
								  'ses_datesend'     => $row->date_send_aproval,	
								  'ses_dateapproval' => $row->date_aproval,
								  'ses_datecair'     => $row->date_print_bpk,	
								  'ses_curr'	     => $row->currency,
								  'ses_giro'		 =>	$row->giro_no,
								  'ses_bank'		 =>	$row->bank,							  									
								  'ses_grand'        => $row->gran_total,
								  'ses_grand_adjust' => $row->gran_total_adjust
								  ); //buat array di session		
					$this->session->set_userdata($data); //simpan di session
				 }													 										  
					 return $query->result();						 													
			  }else{				  
					 return false;
			  }
	 }							    					 	 	 	    

  
//------------------------------------------------------------------------------------------------------------------		
		   
				 
}