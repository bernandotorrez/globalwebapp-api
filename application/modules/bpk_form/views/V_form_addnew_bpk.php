<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js');?>" ></script>
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>

<script type="text/javascript">
//--------------function number only
 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	

//----------------set focus----
 $(function() {
  $("#txtsub").focus();    
});	

</script>


<script script type="text/javascript">

//calculate and add field--------------
$(function(){		
	$('#add').click(function(){
	  addnewrow();	
	 }); 	
	 
	
	 $('body').delegate('.remove','click',function(){				 				 
	 	 $(this).parent().parent().remove();
		   
		   //Untuk menghitung pengurangan Grand total di kurang total amount. ketika di remove
		   var tr =  $(this).parent().parent();  
		   var amtprice = tr.find('.amount').val();
		   var gt 	 = 	$('.total').val() ;
		   var hasil =  numeral().unformat(gt) - numeral().unformat(amtprice);
		   		   
		   $('.total').val(numeral(hasil).format('0,0.00')) ;
		   			  		  			 		 
	 });
	 	
	 
	  $('body').delegate('.quantity,.price','change',function(){  
		  var tr =  $(this).parent().parent();
		   var qty=numeral().unformat(tr.find('.quantity').val());
		   var price=numeral().unformat(tr.find('.price').val());
		   var amt=numeral().unformat(tr.find('.amount').val());
		   var amt=qty*price;             
		   tr.find('.amount').val(amt);
		   total(); 
 	 });
	 
	$('body').delegate('.quantity,.price','blur',function(){  
		   var tr =  $(this).parent().parent();
		   var qty=tr.find('.quantity').val();
		   var price=tr.find('.price').val();
		   var amt=tr.find('.amount').val();
		   
		   tr.find('.quantity').val(numeral(qty).format('0,0')) ;
		   tr.find('.price').val(numeral(price).format('0,0.00')) ;
		   tr.find('.amount').val(numeral(amt).format('0,0.00')) ;
		   total();
 	 });  	 	
	 	  	
});


function total()
{
	var t=0;
	var jmlbaris=parseInt($('.remove').length);
	for(var i=0;i<jmlbaris;++i){
	   t+=parseFloat(numeral().unformat($('.amount').eq(i).val()));
	}
	$('.total').val(numeral(t).format('0,0.00')) ;			
}

function addnewrow()
{

var n = ($('.detail tr').length-0)+1;	
var tr = '<tr>'+
'<td class="no" align="center">'+ n +'</td>'+                                            
'<td><input type="text" name="flagstat[]"  value="1"  hidden="true"  /> <input type="text" name="txtidmasdet[]"  value="<?php echo $ponumber ?>"  hidden="true"  /><input type="text" maxlength="70"  name="txtdesc[]"  placeholder="Description" size="24" class="form-control txtdesc" /></td>'+'<td><input type="text" maxlength="70"  name="txtspecs[]"  placeholder="Specs" size="30" class="form-control txtspecs" /></td>'+'<td><input maxlength="6" id="txtqty" style="text-align: center;" name="txtqty[]" type="text" placeholder="0" size="7" onkeypress="return isNumberKey(event)" class="form-control quantity" /></td>'+                                                		                                  
'<td><input maxlength="14" style="text-align: right;" name="txtprice[]" type="text" placeholder="0.00" size="30" onkeypress="return isNumberKey(event)" class="form-control price"/> </td>'+	
'<td><input  maxlength="70" style="text-align: right;"  name="txtamount[]"  placeholder="0.00"  size="35"  class="form-control amount" readonly="readonly"/></td>'+'<td>  <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button>  </td>'+
'</tr>';
 
 $('.detail').append(tr);
}

</script>

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_fail') ;?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_insert_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_insert_fail');?>  
        </div>	
<?php } ?>

          <div class="table-header btn-info">
            Create BPK
          </div>
          <?php  echo form_open_multipart('bpk_form/c_crud_bpk/multiple_submit',array('class'=>'form-multi'));  ?> 
          <!-- Responsive Form for Mobile and Web Start -->
          <div class="container">

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">PR Number</label>

                  <input type="text" id="txtnopp" name="txtnopp" class="form-control" readonly="readonly" value="<?php echo $ponumber ?>"  />
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">BPK Create By *</label>

                  <input type="text" id="txtsub" name="txtsub" class="form-control" value="<?php echo $this->session->userdata('name'); ?>"  readonly="readonly" />
                </div>
              </div>

            </div>
            <!-- Row End -->

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">Date BPK Created</label>

                  <input type="text" id="txtdate" name="txtdate" class="form-control" readonly value="<?php echo date("d-m-Y") ?>" />
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">Pay To</label>

                  <select id="cbovendor" name="cbovendor" class="form-control">
                        <?php foreach($tampil_vendor as $row){ ?>
                       		<option value="<?php echo $row->id_vendor; ?>"><?php echo $row->vendor ;?></option>
                         <?php } ?>                          
                       </select>
                </div>
              </div>

            </div>
            <!-- Row End -->

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">Company</label>

                  <input type="text" id="txtcompany" name="txtcompany" class="form-control" value="<?php echo $this->session->userdata('company');  ?>"  readonly />
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">Bank</label>

                  <select name="cbobank" class="form-control">
                 	<option> - </option>
                    <?php
                    foreach ($tampil_bank as $row_bank) :						
                      echo '<option>'.$row_bank->name_bank.'</option>';
                    endforeach;
                  ?>
                 </select>
                </div>
              </div>

            </div>
            <!-- Row End -->

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">Branch</label>

                  <input type="text" id="txtbranch" name="txtbranch" class="form-control" value="<?php echo $this->session->userdata('name_branch'); ?>" readonly />
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">Cheque / Giro No</label>

                  <input name="txtgirono" type="text" class="form-control" id="txtgirono" placeholder="Cheque / Giro Number" onkeypress="return isNumberKey(event)"  maxlength="40"  />                                                
                </div>
              </div>

            </div>
            <!-- Row End -->

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">Department </label>

                  <input type="text" id="txtdept" name="txtdept" class="form-control" value="<?php echo $this->session->userdata('dept'); ?>" readonly />
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">Currency</label>

                  <select id="cbocur" name="cbocur"  class="form-control">
                  <?php foreach($tampil_curr as $row){ ?>               
                      <option value="<?php echo $row->id_curr ;?>"><?php echo $row->currency ;?></option>
                    <?php } ?>  
                  </select>
                </div>
              </div>

            </div>
            <!-- Row End -->

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">E-mail </label>

                  <input type="text" id="txtmail" name="txtmail" class="form-control" readonly="readonly" value="<?php echo $this->session->userdata('email'); ?>">
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">Note BPK *</label>

                  <textarea name="txtremarks" class="form-control" id="txtremarks"></textarea>
                </div>
              </div>

            </div>
            <!-- Row End -->

            <div class="table-responsive">
                <div class="panel-heading" id="label_head_panel">

                  <span style="color:#A6090D; font-weight:bold; ">
                    Grand Total :
                  </span>
                  <input maxlength="70" id="result" name="result" class=" total" readonly="readonly"
                    style="color:#A6090D" />
                </div>

            <table class="table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Description</th>
                  <th>Specs</th>
                  <th>Qty</th>
                  <th>Price</th>
                  <th>Total Price</th>

                  <th align="center"><strong>
                      <input type="button" id="add" Class="btn-sm radius-6 btn-info" value="+" />
                  </th>
                </tr>
              </thead>

              <tbody class="detail">
              <tr>
                <td class="no" align="center">1</td>
                <td>
                  <input type="text" name="txtidmasdet[]" maxlength="25" hidden="true"
                    value="<?php echo $ponumber ?>" />
                  <input type="text" name="flagstat[]" value="1" hidden="true" />
                  <input type="text" maxlength="70" id="txtdesc" name="txtdesc[]" placeholder="Description" size="24"
                    class="form-control txtdesc" />
                </td>
                <td>
                  <input type="text" maxlength="70" name="txtspecs[]" placeholder="Specs" size="30"
                    class="form-control txtspecs" />
                </td>
                <td style="text-align: center;"><input maxlength="6" style="text-align: center;" name="txtqty[]" type="text" placeholder="0" size="12"
                    onkeypress="return isNumberKey(event)" class="form-control quantity" />
                </td>
                <td style="text-align: right;">
                  <input maxlength="14" style="text-align: right;" name="txtprice[]" type="text" placeholder="0.00" size="20"
                    onkeypress="return isNumberKey(event)" class="form-control price" />
                </td>

                <td style="text-align: right;">
                  <input maxlength="70" style="text-align: right;" name="txtamount[]" placeholder="0.00" size="30" class="form-control amount"
                    readonly="readonly" />
                </td>
                <td>
                <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button>                   
                        
                </td>

              </tr>


              </tbody>

            </table>

            <table>
              <tr>
                <td>
                  <button class="btn btn-app btn-info btn-xs radius-4 btnsave" type="submit" id="btnsave"
                    name="btnsave" value="save">
                    <i class="ace-icon fa fa-book sm-160"></i>
                    Submit
                  </button>


                </td>

                <td hidden>
                  <input id="btnsend" name="btnsend" type="submit" value="Save & Send Approval"
                    class="btn btn-default btn-clean" />
                </td>

                <td>
                  <a href="<?php echo base_url('bpk_form/c_create_bpk');?>" style="text-decoration:none;"
                    class="btn btn-app btn-success btn-xs radius-4 btnback"> <i
                      class="ace-icon fa fa-exchange bigger-160"></i>Back</a>
                </td>
              </tr>
            </table>

            </div>

            <!-- Responsive Form for Mobile and Web End -->
                
 		
		<?php form_close(); ?> 
 
 
 <?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>    
 