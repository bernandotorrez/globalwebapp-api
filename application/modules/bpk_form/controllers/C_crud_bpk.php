<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_crud_bpk extends MY_Controller
{
  public function __construct()
        {
            parent::__construct();		
			$this->load->helper(array('form', 'url'));    	   
			$this->load->model('M_crud_bpk','C_crud_bpk',TRUE);	
			
			// ada perubahan
		    $this->load->library('fpdf');  
            $this->load->library('form_validation');
			$this->load->helper('terbilang');
			$this->load->helper('form');
			$this->load->helper('url');			
		    $this->load->database();					
        }		  	
		
  public function index()
	{	
      $data['tampil_vendor']=$this->C_crud_bpk->get_vendor();		  	  
	  $data['tampil_bank']=$this->C_crud_bpk->get_bank();
	  $data['tampil_curr']=$this->C_crud_bpk->get_currency();		  	  
	  $data['ponumber']=$this->C_crud_bpk->getppnumber();
	  $data['show_view']='V_form_addnew_bpk';
	  $this->load->view('dashboard/Template',$data);		
	}
		
	
	
	public function do_insert_bpk()	
	{		
	    $this->form_validation->set_rules('txtsub','txtsub','required');		
		$this->form_validation->set_rules('txtremarks','txtremarks','required');
		$this->form_validation->set_rules('result','result','required');
		
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan_fail','BPK Create By, Remarks, Or Grand Total required, Insert can not be prosses!!');
		   redirect('bpk_form/C_crud_bpk'); 			   
		}else{
			if ($this->C_crud_bpk->insert_master_and_detail())
		    {	 			    
			      $this->C_crud_bpk->counterppnumber();  //add and manggil counter numberpp	 				  
				  $this->session->set_flashdata('pesan_succces','Insert BPK Successfull..!!');				 							
				  $url=  base_url('bpk_form/C_crud_bpk');		         
				  echo "<script type='text/javascript'> window.location='" . $url . "'; </script>"; 
			}else{
										 							           		 							
				  echo $this->session->set_flashdata('pesan_insert_fail','fail! ');
				  $url=  base_url('bpk_form/C_crud_bpk');		         
				  echo "<script type='text/javascript'> window.location='" . $url . "';</script>";  				
		    }
		}
	}			 

public function show_form_edit()
 {
	$tampil_edit_master_bpk = $this->C_crud_bpk->getedit_master_view_bpk();
	$tampil_edit_det_bpk = $this->C_crud_bpk->get_edit_detail_bpk();
	
	$tampil_vendor =$this->C_crud_bpk->get_vendor();	
	$tampil_cur = $this->C_crud_bpk->get_currency();	
	$tampil_bank_edit = $this->C_crud_bpk->get_bank(); 				
	
	foreach ($tampil_edit_det_bpk as $row)
	{
		 $data_edit['ppnumber'] =$row->id_master; 
	}
		  	   		
  	if ($tampil_edit_master_bpk == null and $tampil_edit_det_bpk ==null)
	{   	   
	    $data_edit['check_row'] = "0"; // jika tidak ketemu	 		  
	    $data_edit['tampil_edit_bpk'] =$tampil_edit_master_bpk;
	    $data_edit['pesan'] = 'Data Type Not Found'; 		     	  
	    $data_edit['show_view']='V_form_edit_bpk';
	    $data_edit->load->view('dashboard/Template',$data_edit);		
     }else{		 	 
	 	$data_edit['check_row'] = "1"; // jika  ketemu				  
	    $data_edit['ino'] = ""; // variable kosong buat nocounter perulangan detail		
		$data_edit['tampil_edit_master_bpk'] = $tampil_edit_master_bpk;
		$data_edit['tampil_det_bpk']  = $tampil_edit_det_bpk;	
		$data_edit['tampil_vendor'] = $tampil_vendor ; 	 
		$data_edit['tampil_bank']  = $tampil_bank_edit ;
	    $data_edit['tampil_curr'] = $tampil_cur;
		$data_edit['show_view'] ='V_form_edit_bpk';
		$this->load->view('dashboard/Template',$data_edit);
     }
	   
 }
	
public function do_edit_bpk() //lakukan submit edit data	
{	
       // $this->form_validation->set_rules('txtsub','txtsub','required');		
		$this->form_validation->set_rules('txtremarks','txtremarks','required');
		$this->form_validation->set_rules('result','result','required');
		
		//$this->C_crud_pp->edit_pp();
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan_fail','Failed update can not be prosses!!');
		   redirect('bpk_form/C_create_bpk'); 			   
		}else{			
		   if ($this->C_crud_bpk->edit_bpk())
		   {										    		  				  		       				  								
				 $this->session->set_flashdata('pesan_succes','update BPK  Successfull..!!');										
				 $url=  base_url('bpk_form/C_create_bpk');		         
				 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
		   }else{			
				 $this->session->set_flashdata('pesan_succces','Update BPK Failed!!!!!! ....'); 					
				 $url=  base_url('bpk_form/C_create_bpk');		         
				 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
		   }		
		} 
}

	 
public function do_delete_bpk()	 //lakukan submit delete data
{		
	 if ($this->C_crud_bpk->delete_with_edit_flag())
	 { 	 	
		 $this->session->set_flashdata('pesan','Delete Successfully....');		
		 redirect('bpk_form/C_create_bpk');   
	  }else{
		  $this->session->set_flashdata('pesan','No Data Selected To Remove!!');		  
		  redirect('bpk_form/C_create_bpk');
	 }
}			

public function do_search_data()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->C_crud_bpk->search_data(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('company') ;					
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				   		
		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['bpk_view'] =$tampung_cari;
		   $data['pesan'] = 'Data bpk Not a found'; 		   		   
		   $data['show_view'] = 'V_table_bpk';		
		   $this->load->view('dashboard/Template',$data);					
		}else{			
		    $dept  = $this->session->userdata('dept') ;	
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('company') ;					
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['bpk_view'] =$tampung_cari;			   			
			$data['show_view'] = 'V_table_bpk';		
		    $this->load->view('dashboard/Template',$data);		
		}	   
    }
	
 public function do_print_bpk()
  	{		
		if($this->C_crud_bpk->select_query_rpt_bpk())
		 {										
			 $res['currency'] = $this->C_crud_bpk->select_query_rpt_bpk();				  
			 $res['data'] = $this->C_crud_bpk->select_query_rpt_bpk();					 					  
			 //end--------------------------------------------------------------------------------------
		     $this->load->view('V_rptpdf_bpk',$res);		  
		  }else{     		
			 $this->session->set_flashdata('pesan_rpt','Print BPK Failed!!!!!! ....'); 					
			 $url=  base_url('bpk_form/C_create_bpk');		         
			 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
		 } 
		
	}


public function do_search_date()  	
{
	  $tampil_table_bpk= $this->C_crud_bpk->get_search_date()->result();	
	  $total_rows =$this->C_crud_bpk->get_search_date()->num_rows();
	  
		  if ($tampil_table_bpk)
		  {
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('company') ;		
			$data['bpk_view'] =	 $tampil_table_bpk ;		
			$data['intno'] = ""; //variable buat looping no table.	
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;														
			$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
			$data['show_view'] = 'V_table_bpk';		
			$this->load->view('dashboard/Template',$data);				
		  }else{	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('company') ;	
			$data['intno'] = ""; //variable buat looping no table.					
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;	
			$data['pesan'] = 'Data BPK table is empty';		
			$data['show_view'] = 'V_table_bpk';	
			$this->load->view('dashboard/Template',$data);				
		  } 
}


	
public function multiple_submit() //fungsi submit dengan banyak button dalam satu form
{  
	
	if ($this->input->post('btnadd')){	
		redirect('bpk_form/C_crud_bpk');
	}else{
		if ($this->input->post('btnsave')){	
			$this->do_insert_bpk();
		}else{
		  if ($this->input->post('btncaridate')){
			$this->do_search_date();	
		  }else{
				if ($this->input->post('btncari')){	
					$this->do_search_data();
				}else{   		
					if ($this->input->post('btndel')){
						$this->do_delete_bpk();
					}else{
						if ($this->input->post('btnedit')){
							$this->show_form_edit();
						}else{												
							if ($this->input->post('btnsave_edit')){
							   $this->do_edit_bpk();								
							}else{					
								 if ($this->input->post('btnprintbpk')){	
									$this->do_print_bpk();			
								 }else{
									redirect('bpk_form/C_create_bpk');  
								 }
							}
							
						 }
					  }
				   }
		      }
		   }
	  }

}



}

