<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_bank extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_bank');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_bank->get_value('id',$this->session->userdata('id'),'tbl_user');
				$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}		
	 	}
		
	public function index()
		{
		
		$data['bank']=$this->M_bank->get_all_bank();
		$data['total']=$this->M_bank->get_count_id();
		$data['show_view'] = 'bank_form/V_table_bank';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		
		}
		
	public function ajax_list()
	{
		$list = $this->M_bank->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_bank) {
			$chk_idmaster ='<div align="center"><input id="checkbank" name="checkbank" type="checkbox" value='.$tbl_bank->id_bank.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_bank->id_bank;
			$row[] = $tbl_bank->name_bank;
			$row[] = $tbl_bank->date_create;	
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_bank->count_all(),
						"recordsFiltered" => $this->M_bank->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	//function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		//$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		//$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		//$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		//$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		/*$this->db->select('id_bank');
		$this->db->from('tbl_bank');
		$this->db->where('status','1');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		//$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		//$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		//$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		//$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		/*if($search!=""){
		$this->db->like("id_bank",$search);
		$this->db->where('status','1');
		$this->db->or_like("name_bank",$search);
		$this->db->where('status','1');
		}


		/*Lanjutkan pencarian ke database*/
		/*$this->db->limit($length,$start);
		$this->db->where('status','1');
		/*Urutkan dari alphabet paling terkahir
		$this->db->order_by('id_bank','ASC');
		$query=$this->db->get('tbl_bank');*/


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		/*if($search!=""){
		$this->db->like("id_bank",$search);
		$jum=$this->db->get('tbl_bank');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $tbl_bank) {
		$chk_idmaster ='<div align="center"><input id="checkbank" name="checkbank" type="checkbox" value='.$tbl_bank["id_bank"].' class="editRow ace" req_id_del='.$tbl_bank["id_bank"].' />
        <span class="lbl"></span> ';

			$output['data'][]=array($tbl_bank['id_bank'],$tbl_bank['name_bank'],
									$chk_idmaster
									);

		}

		echo json_encode($output);


	}*/

	public function add_bank()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_add = date('Y-m-d H:i:s');

		if(empty($_POST["name_bank"])){
			die ("Field Nama Bank must be filled in!! ");
		}
		else{
			$data = array(
					'id_bank' => $this->input->post('id_bank'),
					'name_bank' => $this->input->post('name_bank'),
					'date_create' => $date_add,
					'remarks' => $remarks_add
				);
			$insert = $this->M_bank->add_bank($data);
			//return $this->db->insert_id();
		  }
		  echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_bank->get_by_id($id);
			echo json_encode($data);
		}

	public function update_bank()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_update = date('Y-m-d H:i:s');

		if(empty($_POST["name_bank"])){
			die ("Field Nama Bank must be filled in!! ");
		}
		else{
		$data = array(
				'name_bank' => $this->input->post('name_bank'),
				'date_update' => $date_update,
					'remarks' => $remarks_update
			);
		$this->M_bank->update_bank(array('id_bank' => $this->input->post('id_bank')), $data);
		  }
		  echo 'Insert';
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_delete = 'Action : Delete | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_delete = date('Y-m-d H:i:s');

		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array (
						'status' => '0',
						'date_update' => $date_delete,
						'remarks' => $remarks_delete
				);
				$this->db->where('id_bank', $id);
				$this->db->update('tbl_bank', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}



}
