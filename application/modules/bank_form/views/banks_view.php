<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Pengguna
          </h1>
          <ol class="breadcrumb">
            <li class="active">Data Pengguna</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="box" style="min-height:450px;">
                <div class="box-header">
                </div>
                <div class="table-responsive">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th width="6%"><input type="checkbox" id="bulkDelete"></th>
                        <th width="20%">Id Bank</th>
                        <th width="30%">Name Bank</th>
                      </tr>
                    </thead>
                    
                  </table>
                </div>
              </div>
        </section><!-- /.content -->
      </div>
	  <script>
	 $(document).ready(function(){
       var dataTable = $('#example1').DataTable({
            "processing": true,
            "serverSide": true,
			"columnDefs": [{
              "targets": 0,
              "orderable": false,
              "searchable": false
                
            }],
            "ajax":{
                url :"<?=@base_url('bank_form/Banks/ajaxdatapengguna')?>",
                type: "post",
                
            },
        });
		
	});
    </script>
	 