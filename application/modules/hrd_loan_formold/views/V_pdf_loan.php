<?php	
	require('mc_table.php');
	$pdf=new PDF_MC_Table('P','cm',"A4");
	$pdf->Open();
	$pdf->AddPage();
	$id=$_GET['id'];
	$pdf->AliasNbPages();
	$pdf->SetMargins(1,1,1);
	$pdf->SetFont('times','B',10);
	
	$pdf->Image('asset/images/mazda.jpg',1,1,2);
	$pdf->Ln(2);  
	$pdf->SetX(1);             
	$pdf->MultiCell(19,0.4,'PT. Eurokars Motor Indonesia',0,'L'); 
	$pdf->SetFont('times','',9);	
	$pdf->SetX(1);             
	$pdf->MultiCell(19,0.4,'Porsche Center Jakarta',0,'L');                
	$pdf->SetX(1);             
	$pdf->MultiCell(19,0.4,'JL.Sultan Iskandar Muda No.51, Arteri Pondok Indah',0,'L');                         
	$pdf->SetX(1);             
	$pdf->MultiCell(19,0.4,'Jakarta Selatan',0,'L');
	$pdf->SetX(1);             
	$pdf->MultiCell(19,0.4,'Telp. 021-27932838',0,'L');
	$pdf->SetX(1);             
	$pdf->MultiCell(19,0.4,'Fax. 021-27932991',0,'L');	
	$pdf->Ln(0.5);
	$pdf->SetFont('times','B',12);
	$pdf->SetX(1);             
	$pdf->setFont('times','B'.'U',12);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"LOAN FORM",0,0,'C',1);  		
	include 'koneksi.php';
	$loan = mysqli_query($connect, "select * from qv_hrd_loan where id_loan = $id and status='1'") or die(mysql_error()) ;
	while ($row = mysqli_fetch_array($loan)){
	$bulan = substr($row['date_loan'],5,2);
	$tahun = substr($row['date_loan'],0,4);
	if ($bulan == 01){
		$bulan = 'I';
	}else if($bulan == 02){
		$bulan = 'II';
	}else if($bulan == 03){
		$bulan = 'III';
	}else if($bulan == 04){
		$bulan = 'IV';
	}else if($bulan == 05){
		$bulan = 'V';
	}else if($bulan == 06){
		$bulan = 'VI';
	}else if($bulan == 07){
		$bulan = 'VII';
	}else if($bulan == 08){
		$bulan = 'VIII';
	}else if($bulan == 09){
		$bulan = 'IX';
	}else if($bulan == 10){
		$bulan = 'X';
	}else if($bulan == 11){
		$bulan = 'XI';
	}else {
		$bulan = 'XII';
	}
	$dept = $this->session->userdata('dept');
	$pdf->Ln(0.5);
	$pdf->setFont('times','B',10);
	$pdf->setFillColor(255,255,255);                               
	$pdf->Cell(0,0,"Nomor Loan : ".$row['no_loan'],0,0,'C',1);	
	$pdf->Ln(0.7);
	$pdf->setFont('times','B',9);
	$pdf->Cell(5,0.6,"Printed date : " . date('d/m/Y'),0,0,'C');   
	$pdf->Ln(0.7);
	$pdf->setFont('times','B',9);
	$pdf->Cell(3,0.6,'Name Staff',0,0,'C');
	$pdf->SetFont('times','',9);
	$pdf->Cell(6,0.6,$row['name_staff'],1,0);
	$pdf->setFont('times','B',9);
	$pdf->Cell(3,0.6,'No. Inventory',0,0,'C');
	$pdf->SetFont('times','',9);
	$pdf->Cell(6,0.6,$row['no_inventory'],1,0);
	$pdf->Ln();
	$pdf->Cell(3,0.3,'',0,0);
	$pdf->Ln();
	$pdf->setFont('times','B',9);
	$pdf->Cell(3,0.6,'NIK',0,0,'C');
	$pdf->SetFont('times','',9);
	$pdf->Cell(6,0.6,$row['nik'],1,0);
	$pdf->setFont('times','B',9);
	$pdf->Cell(3,0.6,'Date Loaned',0,0,'C');
	$pdf->SetFont('times','',9);
	$pdf->Cell(6,0.6,$row['date_loan'],1,0);
	$pdf->Ln();
	$pdf->Cell(3,0.3,'',0,0);
	$pdf->Ln();
	$pdf->setFont('times','B',9);
	$pdf->Cell(3,0.6,'Description',0,0,'C');
	$pdf->SetFont('times','',9);
	$pdf->Cell(9,0.6,$row['description'],1,0);
	$pdf->Ln();
	$pdf->Cell(3,0.3,'',0,0);
	$pdf->Ln();
	$pdf->setFont('times','B',9);
	$pdf->Cell(3,0.6,'Additional',0,0,'C');
	$pdf->SetFont('times','',9);
	$pdf->Cell(9,0.6,$row['additional'],1,0);
	$pdf->Ln();
	$pdf->Cell(3,0.3,'',0,0);
	$pdf->Ln();
	$pdf->setFont('times','B',9);
	$pdf->Cell(3,0.6,'Purpose',0,0,'C');
	$pdf->SetFont('times','',9);
	$pdf->Cell(9,0.6,$row['purpose'],1,0);
	
	
	$pdf->Ln(1.2);				
	$pdf->setFont('times','B',8);
	$pdf->setFillColor(255,255,255);    	
	$pdf->Cell(9,0.5,"",0,0,'C',1);  	// buat header jabatan yg menandatangani		    				
	$pdf->Cell(3,0.5,"Account / Manag.Tax",1,0,'C',1);
	$pdf->Cell(3,0.5,"Manag.Dept / Head",1,0,'C',1);    
	$pdf->Cell(3,0.5,"Applicant",1,1,'C',1);    								                              			   				
	
	$pdf->Cell(9,1.5,"",0,0,'C',1); //buat space tanda tangan 			    					
	$pdf->Cell(3,1.5,"",1,0,'C',1);
	$pdf->Cell(3,1.5,"",1,0,'C',1);    
	$pdf->Cell(3,1.5,"",1,1,'C',1);     								                              			
	
	$pdf->setFont('times','I'.'U',8);
	$pdf->setFillColor(255,255,255); 
	$pdf->Cell(9,0.5,"",0,0,'C',1);  // buat nama yg menandatangani			    	
	$pdf->Cell(3,0.5,"Account / Manag.Tax",1,0,'C',1);
	$pdf->Cell(3,0.5,$this->session->userdata('sign_head'),1,0,'C',1);	
	$pdf->Cell(3,0.5,$this->session->userdata('name'),1,1,'C',1);  
    
	$pdf->SetLineWidth(0.04);
	$pdf->Line(1,15.5,20,15.5);
	$pdf->SetLineWidth(0);
    $pdf->Ln(0.9);
	$pdf->setFont('times','B'.'I',8);
	$pdf->Cell(3,0.5,'Return On:',0,0,'R');
	$pdf->Cell(5,0.5,"",1,0);
	$pdf->Cell(2,0.5,'Checked By:',0,0,'R');
	$pdf->Cell(5,0.5,"",1,0);
	
	$pdf->Ln(0.8);
	$pdf->Cell(3,0.5,'Comments:',0,0,'R');
	$pdf->Cell(12,1.2,"",1,0);
	
	$pdf->Ln(1.4);
	$pdf->setFont('times','B'.'I',8);
	$pdf->Cell(3,0.5,'Admin Officer:',0,0,'R');
	$pdf->Cell(5,0.5,"",1,0);
	$pdf->Cell(2,0.5,'Date:',0,0,'R');
	$pdf->Cell(5,0.5,"",1,0);

	}
 	

	$pdf->Output();
	
?>