<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mazda | Loan Form</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker3.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-timepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/daterangepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-colorpicker.min.css') ?>" />
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">


        <!-- Main content -->
        <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <img src="../../assets/images/logo/mazda.png" alt="Mazda"> PT. Eurokars Motor Indonesia
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Address
              <address>
                <strong>Porsche Centre Jakarta</strong><br>
                Jl. Sultan Iskandar Muda, No. 15<br>
                Arteri Pondok Indah, Jakarta Selatan<br>
                Phone	: (021) 2793 2838<br>
                Fax		: (021) 2793 2838
              </address>
            </div><!-- /.col -->
          </div><!-- /.row -->
		<div class="row" id="modal_form">
		<h3 class="box-title" style="font-weight:bold;"><center>LOAN FORM</center></h3></br>
		 <form class="form-horizontal" id="form">
		<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="name" class="col-sm-3 control-label" style="padding-right:10px">Name of Staff</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="name" id="name" placeholder="Sapto Adiwigati" value="<?php echo $this->session->userdata('name') ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nik" class="col-sm-3 control-label" style="padding-right:10px">NIK</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="nik" id="nik" placeholder="1018">
                  </div>
                </div>
              </div>
		</div>
		</div>
		<div class="col-xs-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="noinventory" class="col-sm-3 control-label" style="padding-right:10px">No. Inventory</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="noinventory" id="noinventory" placeholder="EMI-IT 0000001">
                  </div>
                </div>
                <div class="form-group">
                  <label for="date" class="col-sm-3 control-label" style="padding-right:10px">Date Loaned</label>

                 <div class="input-group date col-sm-8">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="datepicker" id="datepicker">
                </div>
                </div>
              </div>
          </div>
		</div>
		<div class="col-xs-9">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label" style="padding-right:10px">Description</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" rows="3" name="description" id="description" placeholder="Lenovo ThinkPad"></textarea>
                  </div>
                </div>
				<div class="form-group">
                  <label for="additional" class="col-sm-2 control-label" style="padding-right:10px">Additional</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" name="additional" id="additional" placeholder="Mouse, Bag, ..."></textarea>
                  </div>
                </div>
				<div class="form-group">
                  <label for="purpose" class="col-sm-2 control-label" style="padding-right:10px">Purpose</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" rows="3" name="purpose" id="purpose" placeholder="Daily Activites"></textarea>
                  </div>
                </div>					
              </div>
		</div>
		</div>
		</form>
		</div>

          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
              <button class="btn btn-success pull-right" onclick="add_loan()"><i class="fa fa-save"></i> Submit</button>
            </div>
          </div>
        </section><!-- /.content -->
        <div class="clearfix"></div>
      </div><!-- /.content-wrapper -->
      
	 <script type="text/javascript">
            $(function () {
                $('#datepicker').datetimepicker();
            });
     </script>
    <!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
  </body>
  
  
  <script type="text/javascript">	

	var save_method; //for save method string
    var table;
		
    function add_loan()
    {
     var url;
	 url = "<?php echo site_url('hrd_loan_form/C_loan/add_loan')?>";
	 urlbef = "<?php echo site_url('hrd_loan_form/C_loan')?>";
	 var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR) 
            {
				if(data=='Insert'){
				window.location.href = urlbef;
			   }else {
			   alert (data);
			   }
			   
            },
        });
    }
  </script>
</html>
