<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<style>
 .outer {
    width: 210px;
    color: black;
    border: 2px solid Gainsboro;
    padding: 5px;
 }
</style> 


									<!--<div>
									<a href="<?php echo site_url('hrd_loan_form/c_loan/view_add_loan'); ?>"><button class="btn bg-purple btn-flat"><i class="fa fa-plus-circle"></i> Add Data</button></a>
									<button onclick="delete_loan()" id="deleteTriger" name="deleteTriger" class="btn bg-maroon btn-flat" disabled><i class="fa fa-trash"></i> Delete Data</button>
									<button onclick="edit_loan()" id="editTriger"  name="editTriger"class="btn bg-maroon btn-flat" disabled><i class="fa fa-edit"></i> Edit Data</button>
									<button onclick="send_loan()" id="sendTriger"  name="sendTriger"class="btn bg-maroon btn-flat" disabled><i class="fa fa-edit"></i> Send</button>
									</div> 
                                    <br/> -->
									<div class="row">
									<div class="col-xs-12" style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">                               
	                                <table  class="table table-striped table-bordered table-hover" id="myTable" >
	                                    <thead class="text-warning">
										<th width="5%" style="text-align:center">
												<label class="pos-rel">
                                                    <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
                                                    <span class="lbl"></span>
                                                </label>
                                            </th> 
											<th>No</th>
	                                        <th>Id</th>
											<th>Name</th>
											<th>NIK</th>
											<th>No. Inventory</th>
											<th>Date Loaned</th>
											<th>Description</th>
											<th>Additional</th>
											<th>Purpose</th>
											<th>Head</th>
											<th>F.C.</th>
											<th>Detail</th>
											<th>Input</th>
											<th>Print</th>
	                                    </thead>
                                     </table> 
									 
      </div>                               
</div>      
  <form class="form-horizontal" id="form" >
    <div class="modal fade" id="ViewDetailModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myRejectedLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myRejectedLabel">Detail Rejected</h4>
                    </div>
                    
                    <div class="modal-bodynew">                      
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
		
	<div class="modal fade" id="ViewEnter" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myRejectedLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myRejectedLabel">Enter Data</h4>
                    </div>
                    
                    <div class="modal-bodynewnew form" style="padding-top:5px">   
					<div class="col-sm-8">
					</br>
					 <div class="box box-info">
						  <div class="box-body">
					         <div class="contoh"></div>
							<input type="hidden" name="contoh" id="contoh">
						   <div class="form-group">
							  <label for="return" class="col-sm-3 control-label" style="padding-right:10px">Returned On</label>

							 <div class="input-group date col-sm-8">
							  <div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							  </div>
							  <input type="text" class="form-control pull-right" name="datepicker_return" id="datepicker_return">
							</div>
							</div>
							<div class="form-group">
							  <label class="control-label col-sm-3" style="padding-right:10px">Checked By</label>
							  <div class="col-md-8">
							   <input type="text" class="form-control" name="name" id="name"  value="<?php echo $this->session->userdata('name') ?>" disabled>
							  </div>
							</div>
							<div class="form-group">
							  <label for="comment" class="col-sm-3 control-label" style="padding-right:10px">Comment</label>
							  <div class="col-sm-8">
								<input type="text" class="form-control" name="comment" id="comment" placeholder="Jaga Barang Pinjaman Dengan Baik">
							  </div>
							</div>
						  </div>
					</div>
				</div>
				<div class="modal-footer">
						<button type="button" id="btnSaveNew" name="btnSaveNew" onclick="savenew()" class="btn btn-primary col-sm-1">Save</button>
						<button type="button" class="btn btn-danger col-sm-1" data-dismiss="modal">Cancel</button>
						</div>  
				
                    </div> 
                    
                                     
                </div>
            </div>
        </div>
		
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-80p">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">×</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Loan Form
                </h4>
            </div>
	  <div class="modal-body form" style="padding-top:10px">       
	
		<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
			  <div class="form-group">
                  <label for="id" class="col-sm-3 control-label" style="padding-right:10px">Id Loan</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="id" id="id" placeholder="1">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-3 control-label" style="padding-right:10px">Name of Staff</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="name" id="name" placeholder="Sapto Adiwigati">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nik" class="col-sm-3 control-label" style="padding-right:10px">NIK</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="nik" id="nik" placeholder="1018">
                  </div>
                </div>
              </div>
		</div>
		</div>
		<div class="col-xs-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="noinventory" class="col-sm-3 control-label" style="padding-right:10px">No. Inventory</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="noinventory" id="noinventory" placeholder="EMI-IT 0000001">
                  </div>
                </div>
                <div class="form-group">
                  <label for="date" class="col-sm-3 control-label" style="padding-right:10px">Date Loaned</label>

                 <div class="input-group date col-sm-8">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="datepicker" id="datepicker">
                </div>
                </div>
              </div>
          </div>
		</div>
		<div class="col-xs-9">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="description" class="col-sm-2 control-label" style="padding-right:10px">Description</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" rows="3" name="description" id="description" placeholder="Lenovo ThinkPad"></textarea>
                  </div>
                </div>
				<div class="form-group">
                  <label for="additional" class="col-sm-2 control-label" style="padding-right:10px">Additional</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" name="additional" id="additional" placeholder="Mouse, Bag, ..."></textarea>
                  </div>
                </div>
				<div class="form-group">
                  <label for="purpose" class="col-sm-2 control-label" style="padding-right:10px">Purpose</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" rows="3" name="purpose" id="purpose" placeholder="Daily Activites"></textarea>
                  </div>
                </div>					
              </div>
		</div>
		</div>
		</form>
		</div>
			<div class="modal-header">
            <button type="button" id="btnSave" name="btnSave" onclick="save()" class="btn btn-primary col-sm-3">Save</button>
            <button type="button" class="btn btn-danger col-sm-3" data-dismiss="modal">Cancel</button>
			</div>
		
		</div> <!-- End modal body div -->
      </div> <!-- End modal content div -->
</div>

 <script type="text/javascript">
            $(function () {
                $('#datepicker_return').datetimepicker();
            });
 </script>
	 
<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.detail',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("it_loan_form/c_it_loan/get_info_reject"); ?>';
		
		$("#ViewDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-bodynew').html(msg);
			}
						
		});				
		
   });	
});


</script>

<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.enter',function(e){
		var req_idnew = $(this).attr('req_idnew'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("it_loan_form/c_it_loan/update_returnnew"); ?>';
		
		$("#ViewEnter").modal('show'); 		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {idmasternew:req_idnew},
			success: function (msg) {
				$('#contoh').val(msg);
			}
						
		});				
		
   });	
});


</script>

<script>
$('.selectall').click(function() {
    if ($(this).is(':checked')) {
        $('div input').attr('checked', true);
    } else {
        $('div input').attr('checked', false);
    }
});
</script>
<script>


//$('#myTable').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
var tableUsers = $('#myTable').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		"dom": 'lfBrtip',
		"buttons": [
            {
		   "extend":    'copy',
		   "text":      '<i class="fa fa-files-o"></i> Copy',
		   "titleAttr": 'Copy'
		   },
		   {
		   "extend":    'print',
		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
		   "titleAttr": 'Print',
		   "orientation": 'landscape',
           "pageSize": 'LEGAL'
		   },
		   {
		   "extend":    'excel',
		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
		   "titleAttr": 'Excel'
		   },
		   {
		   "extend":    'pdf',
		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
		   "titleAttr": 'PDF',
		   "orientation": 'landscape',
           "pageSize": 'LEGAL'
		   }
        ],
		"autoWidth" : false,
		"scrollY" : '250',
		"scrollX" : true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('it_loan_form/C_it_loan/ajax_list') ?>",
            "type": "POST"
        },
		'columnDefs': [
		{
        'targets': [0],
		'orderable': false,
		}
   ],

      'order': [[2, 'desc']],
        });
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------
//aktif edit--------------------------------------------------------

var counterCheckedsend = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckedsend++ : counterCheckedsend--;
    counterCheckedsend == 1 ? $('#sendTriger').prop("disabled", false): $('#sendTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------

//aktif print--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#printTriger').prop("disabled", false): $('#printTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>


<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>

<script>
        	$(document).ready(function(){
	            $("#id_company").change(function (){
	                var url = "<?php echo site_url('it_loan_form/C_it_loan/pos_branch');?>/"+$(this).val();
	                $('#branch_id').load(url);
	                return false;
	            })
	        });
    	</script>

	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
	
	function edit_loan(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('it_loan_form/C_it_loan/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {
			$('[name="id"]').val(data.id_loan);
			$('[name="name"]').val(data.name_staff);
            $('[name="nik"]').val(data.nik);
			$('[name="noinventory"]').val(data.no_inventory);
			$('[name="datepicker"]').val(data.date_loan);
			$('[name="description"]').val(data.description);
			$('[name="additional"]').val(data.additional);
			$('[name="purpose"]').val(data.purpose);
			$('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Loan'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
	
	function send_loan()
    {
     if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Send Approval???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('it_loan_form/C_it_loan/sendloan')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
	}
			

     function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('it_loan_form/C_it_loan/add_loan')?>";
      }
      else
      {
		   url = "<?php echo site_url('it_loan_form/C_it_loan/update_loan')?>";
      }
		  var formData = new FormData(document.getElementById('form')); 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }
	
	 function savenew()
    {
	var url;
   
		   url = "<?php echo site_url('it_loan_form/C_it_loan/update_return')?>";
		  var idloan = $('#contoh').val();
		  var formData = new FormData(document.getElementById('form')); 
          $.ajax({
            url : url,
            type: "POST",
            data: formData,	idloan,								
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }

     function delete_loan()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('it_loan_form/C_it_loan/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
			
		

      
    }
	
  </script>

  

</html>
