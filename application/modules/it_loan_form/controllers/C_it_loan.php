<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_it_loan extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_it_loan');
			$this->load->library('pagination'); //call pagination library
			$this->load->helper('form');
			$this->load->helper('terbilang');			
			$this->load->library('fpdf');   		
			$this->load->database();
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_it_loan->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}
	 	}
		
	public function index()
		{

		$data['loan']=$this->M_it_loan->get_all_loan();
		$data['total']=$this->M_it_loan->get_count_id();
		$data['group']=$this->M_it_loan->get_all_group();
		$data['person']=$this->M_it_loan->get_all_person();
		$data['department']=$this->M_it_loan->get_all_dept();
		$data['purchase']=$this->M_it_loan->get_all_purchase();
		$data['company']=$this->M_it_loan->get_all_company();
		$data['company2']=$this->M_it_loan->get_all_company();
		$data['show_view'] = 'it_loan_form/V_loan';
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function ajax_list()
	{
		$list = $this->M_it_loan->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_hrd_loan) {
			
			If ($tbl_hrd_loan->approve_head == "1" && $tbl_hrd_loan->approve_fc == "1" ){
		   $print_pdf ='<div align="center"><a href="C_it_loan/view_loan_pdf?id='.$tbl_hrd_loan->id_loan.'" value='.$tbl_hrd_loan->id_loan.' name="print_pdf" type="button"  class="editRow ace">Print</a></div> ';
		   }else {
			   $print_pdf = '<div style=" color:#EB293D">'."Waiting".'</div>' ; 
		   }
			 
			If ($tbl_hrd_loan->approve_head == "1")
			 {
				$head_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
			 }else{
				  If ($tbl_hrd_loan->status_send_aprove == "-1")
				  {
					 $head_approval = '<label style="color:red">'."Reject".'</label>';
				  }else{
					 $head_approval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
				  }
			 }
			
			   If ($tbl_hrd_loan->approve_fc == "1")
			  {
					$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 }else{
					  If ($tbl_hrd_loan->status_send_aprove == "-1")
					  {
						$fc_aproval = '<label style="color:red">'."Reject".'</label>';
					  }else{
						$fc_aproval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
					  }
			   }
			   
			$chk_idmaster ='<div align="center"><input id="checkuser" name="checkuser" type="checkbox" value='.$tbl_hrd_loan->id_loan.' class="editRow ace" />
           <span class="lbl"></span> ';
		 
		   $btn_view_detail = '<a href="#" class="btn btn-primary detail" id="detail" style="text-decoration:none" req_id='.$tbl_hrd_loan->id_loan.'> <span class="glyphicon glyphicon-list" aria-hidden="true"></span>Info</a>';
		   
		   If ($tbl_hrd_loan->approve_fc == "1" && $tbl_hrd_loan->flag_input == "0")
			{
		   $btn_input = '<a href="#" class="btn btn-primary enter" id="enter" style="text-decoration:none" req_idnew='.$tbl_hrd_loan->id_loan.'> <span class="glyphicon glyphicon-list" aria-hidden="true"></span>Input</a>';
		    }else {
				$btn_input = '<a href="#" class="btn btn-primary out" id="out" style="text-decoration:none" req_idnew='.$tbl_hrd_loan->id_loan.' disabled="disabled"> <span class="glyphicon glyphicon-list" aria-hidden="true"></span>Input</a>';
		    }
				
		 
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_hrd_loan->id_loan;
			$row[] = $tbl_hrd_loan->name_staff;
			$row[] = $tbl_hrd_loan->nik;
			$row[] = $tbl_hrd_loan->no_inventory;
			$row[] = $tbl_hrd_loan->date_loan;
			$row[] = $tbl_hrd_loan->description;
			$row[] = $tbl_hrd_loan->additional;
			$row[] = $tbl_hrd_loan->purpose;
			$row[] = $head_approval;
			$row[] = $fc_aproval;
			$row[] = $btn_view_detail;
			$row[] = $btn_input;
			$row[] = $print_pdf;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_it_loan->count_all(),
						"recordsFiltered" => $this->M_it_loan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	public function reset_pass()
	{
		$data = array(
					'id' => $this->input->post('id'),
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('username')),
					);
		$this->M_it_loan->update_loan(array('id' => $this->input->post('id')), $data);
		echo 'Insert';
	}
	
	public function get_info_reject() {    	       			
	     $id_master = $this->input->post('idmaster',TRUE);     		 
	     $query = $this->db->query("select reason_reject from tbl_hrd_loan where id_loan ='".$id_master."'");  	
		 		  
		 if($query->num_rows() >0){	  		     					
			 foreach($query->result() as $row);
			 {
			   echo $row->reason_reject ;	 
			 }	
		 } 							 
    }  
	
	public function update_returnnew() {    	       			
	     $id_masternew = $this->input->post('idmasternew',TRUE); 
		 print_r($id_masternew);   		 
    }  

	
	public function add_loan()
		{
		$sesiuser =  $this->M_it_loan->get_value('id',$this->session->userdata('id'),'tbl_user');
		$strid_user =$this->session->userdata('id');
		$strdept_user =$this->session->userdata('id_dept');
		$stremail_user =$this->session->userdata('email');
		$stremail_head =$this->session->userdata('email_head');
		$stremail_cc =$this->session->userdata('email_cc');
		$strcompany =$this->session->userdata('id_company');
		$strbranch =$this->session->userdata('branch_id');
		$strdiv =$this->session->userdata('id_divisi');
		$tanggal = substr($this->input->post('datepicker'),8,2);
		$bulan = substr($this->input->post('datepicker'),5,2);
		$tahun = substr($this->input->post('datepicker'),1,3);
		if(empty($_POST["name"])){
			die ("Field Name must be filled in!! ");
		}
		elseif(empty($_POST["nik"])){
			die ("Field NIK must be filled in!! ");
		}
		elseif(empty($_POST["noinventory"])){
			die ("Field No. Inventory must be filled in!! ");
		}
		elseif(empty($_POST["datepicker"])){
			die ("Field Date Loaned must be filled in!! ");
		}
		elseif(empty($_POST["description"])){
			die ("Field Description must be filled in!! ");
		}
		elseif(empty($_POST["purpose"])){
			die ("Field Purpose must be filled in!! ");
		}
		else{
				if ($bulan == 01){
				$bulan = 'I';
			}else if($bulan == 02){
				$bulan = 'II';
			}else if($bulan == 03){
				$bulan = 'III';
			}else if($bulan == 04){
				$bulan = 'IV';
			}else if($bulan == 05){
				$bulan = 'V';
			}else if($bulan == 06){
				$bulan = 'VI';
			}else if($bulan == 07){
				$bulan = 'VII';
			}else if($bulan == 08){
				$bulan = 'VIII';
			}else if($bulan == 09){
				$bulan = 'IX';
			}else if($bulan == 10){
				$bulan = 'X';
			}else if($bulan == 11){
				$bulan = 'XI';
			}else {
				$bulan = 'XII';
			} 
			 $num= $this->M_it_loan->get_jumlah();
			 $sum= $num + 1;
			 $data = array(
					'no_loan' => 'LN/EMI/'.$strdept_user.'/'.$tanggal.'/'.$bulan.'/'.$tahun.'/'.$sum,
					'name_staff' => $this->input->post('name'),
					'dept' => $strdept_user,
					'nik' => $this->input->post('nik'),
					'no_inventory' => $this->input->post('noinventory'),
					'date_loan' => $this->input->post('datepicker'),
					'description' => $this->input->post('description'),
					'additional' => $this->input->post('additional'),
					'purpose' => $this->input->post('purpose'),
					'id_user' => $strid_user,
					'email' => $stremail_user,
					'email_head' => $stremail_head,
					'email_cc' => $stremail_cc,
					'id_company' => $strcompany,
					'branch_id' => $strbranch,
					'id_divisi' => $sesiuser[0]->id_divisi,
					'status' => '1',
				);
			$insert = $this->M_it_loan->add_loan($data);
			}
			echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_it_loan->get_by_id($id);
			echo json_encode($data);
		}
		
		
	function pos_branch($id_company)
	{
    	$query = $this->db->get_where('tbl_branches',array('id_company'=>$id_company));
    	$data = "<option value=''>- Select Branch -</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->branch_id."'>".$value->name_branch."</option>";
    	}
    	echo $data;
	}

	public function update_loan()
		{
		 if(empty($_POST["name"])){
			die ("Field Name must be filled in!! ");
		}
		elseif(empty($_POST["nik"])){
			die ("Field NIK must be filled in!! ");
		}
		elseif(empty($_POST["noinventory"])){
			die ("Field No. Inventory must be filled in!! ");
		}
		elseif(empty($_POST["datepicker"])){
			die ("Field Date Loaned must be filled in!! ");
		}
		elseif(empty($_POST["description"])){
			die ("Field Description must be filled in!! ");
		}
		elseif(empty($_POST["purpose"])){
			die ("Field Purpose must be filled in!! ");
		}
		else{
			 $data = array(
					'name_staff' => $this->input->post('name'),
					'nik' => $this->input->post('nik'),
					'no_inventory' => $this->input->post('noinventory'),
					'date_loan' => $this->input->post('datepicker'),
					'description' => $this->input->post('description'),
					'additional' => $this->input->post('additional'),
					'purpose' => $this->input->post('purpose'),
					'status' => '1',
				);
			$this->M_it_loan->update_loan(array('id_loan' => $this->input->post('id')), $data);
		  }
		   echo 'Insert';
		}
		
	public function update_return()
	 
		{
		if(empty($_POST["datepicker_return"])){
			die ("Field Returned On must be filled in!! ");
		}
		elseif(empty($_POST["comment"])){
			die ("Field Comment must be filled in!! ");
		}
		else{
			 $data = array(
					'returned' => $this->input->post('datepicker_return'),
					'checked' => $this->session->userdata('name'),
					'comment' => $this->input->post('comment'),
					'flag_input' => 1,
				);
			$this->M_it_loan->update_return(array('id_loan' => $this->input->post('contoh')), $data);
		  }
		   echo 'Insert';
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);
				$this->db->where('id_loan', $id);
				$this->db->update('tbl_hrd_loan', $data);
			}
		}
	}
	
	public function sendloan (){
		$data_ids = $_REQUEST['ID'];
		$date = date ('Y-m-d');
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'date_aproval' => $date,
						'status_send_aprove' => '1',
						'reason_reject' => '',
				);
				$this->db->where('id_loan', $id);
				$this->db->update('tbl_hrd_loan', $data);
			}
		}
	}
	
	 public function view_add_loan()
	{

	  $data['show_view']='it_loan_form/V_add_loan';
	  $this->load->view('dashboard/Template',$data);		  	 	     		   		      						   	   	
	}
	
	
	 public function view_loan_pdf($id)
	{
	 $this->load->view('it_loan_form/V_pdf_loan');	  
	}
	
	public function view_loan_rejected()
	{

	  $data['show_view']='it_loan_form/V_loan_rejected';
	 $this->load->view('dashboard/Template',$data);	  
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }



}
