<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_history_finance extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_history_finance','',TRUE);	
		//$this->load->library('fpdf');   		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('terbilang');
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_history_finance->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}		
	}		  	
		
  public function index()
  {					    																
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_pp= $this->M_history_finance->tampil_table()->result();	
	  $total_rows =$this->M_history_finance->tampil_table()->num_rows();
	  									
	  if ($tampil_table_pp)
		{		
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;	
		$data['intno'] = ""; //variable buat looping no table.					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		$data['ceck_row'] = $total_rows;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'finbpkall_form/V_history_finance';		
		$this->load->view('dashboard/Template',$data);				
	  }else{	
	    $dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;	
		$data['intno'] = ""; //variable buat looping no table.					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data PP table is empty';		
		$data['show_view'] = 'finbpkall_form/V_history_finance';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }
      						 			
	
	public function do_search_date()  	
	{
	  $tampil_table_pp= $this->M_history_finance->get_search_date()->result();	
	  $total_rows =$this->M_history_finance->get_search_date()->num_rows();
	  
		  if ($tampil_table_pp)
		  {
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('company') ;		
			$data['pp_view'] =	 $tampil_table_pp ;		
			$data['intno'] = ""; //variable buat looping no table.	
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;														
			$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
			$data['show_view'] = 'finbpkall_form/V_history_finance';		
			$this->load->view('dashboard/Template',$data);				
		  }else{	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('company') ;	
			$data['intno'] = ""; //variable buat looping no table.					
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;	
			$data['pesan'] = 'Data PP table is empty';		
			$data['show_view'] = 'finbpkall_form/V_history_finance';
			$this->load->view('dashboard/Template',$data);				
		  } 
	}
	
	public function do_search()  	
	{
	   $tampil_table_pp= $this->M_history_finance->get_search()->result();	
	   $total_rows =$this->M_history_finance->get_search()->num_rows();
	    if ($tampil_table_pp)
		  {
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;		
			$data['pp_view'] =	 $tampil_table_pp ;		
			$data['intno'] = ""; //variable buat looping no table.	
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;														
			$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
			$data['show_view'] = 'finbpkall_form/V_history_finance';		
			$this->load->view('dashboard/Template',$data);				
		  }else{	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;	
			$data['intno'] = ""; //variable buat looping no table.					
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;	
			$data['pesan'] = 'Data PP table is empty';		
			$data['show_view'] = 'finbpkall_form/V_history_finance';
			$this->load->view('dashboard/Template',$data);				
		  } 
	  
	}
	
	//------------------------------------pashing result to modal popup-------------------
	public function get_nopp_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //       
	     $query_master = $this->db->query("select id_master,remarks,gran_total,ppn,pph,gran_totalppn,gran_total_adjust,currency from qv_head_pp_complite where id_master ='".$nopp."'");  
         $query_detail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   
		   
		      $data['str_pp_master']=$query_master->result();
			  $data['str_pp_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_nopp'] = $nopp ;
			  $tampil_detail = $this->load->view('finbpkall_form/V_conten_detail',$data,true);
			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end phasing--------------------
	
	 public function do_print_bpk()
  	{		
		if($this->M_history_finance->select_data_rpt_bpk())
		 {	
															
			 $res['currency'] = $this->M_history_finance->select_query_rpt_bpk();				  
			 $res['data'] = $this->M_history_finance->select_query_rpt_bpk();	
			 
			 //array[data_detail_adjust] buat nampilin detail adjustment finance yang di input di form.			
			 $res['data_detail_adjust']=$this->M_history_finance->select_query_rpt_detail_adjustment();			
			 //end--------------------------------------------------------------------------------------
			 $this->load->view('finbpkall_form/V_rptpdf_bpk',$res);				 				  
		  }else{     		
			 $this->session->set_flashdata('pesan_rpt','Print BPK Failed!!!!!! ....'); 					
			 redirect('finbpkall_form/c_history_finance');	   
		  } 
		  
	}
	
	public function do_clear_adjust()
	{
		if($this->M_history_finance->clear_adjustment()) {
			
		    $this->session->set_flashdata('pesan_succes',"Clear Adjustmen Succesfully");
			 redirect('finbpkall_form/c_history_finance');	  
		}else{
	         $this->session->set_flashdata('pesan',"Clear Adjustmen Failed"); 	
			  redirect('finbpkall_form/c_history_finance');	  
		}
	}
	
	
	public function multiple_submit()
	{
		if ($this->input->post('btncaridate'))
		{
			$this->do_search_date();
		}else{
			if ($this->input->post('btncari'))
			{
			    $this->do_search();	
			}else{
				if ($this->input->post('btnprintbpk'))
			    {
			        $this->do_print_bpk();										
				}else{
					if ($this->input->post('btnclearadjust'))
			    	{
			        	$this->do_clear_adjust();	
					}else{
				    	redirect('finbpkall_form/c_history_finance');
					}
				}
			}
		}
	}
	
	function cath_data_fordatatables(){
						
		
		$str_submission = $this->session->userdata("name");
		$str_status_approved ="1";
			
		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];
		
		$search_date_approved = date('Y-m-d', strtotime($search)); //date
		$search_date_pp = date('Y-m-d', strtotime($search)); //date


		/*Menghitung total desa didalam database*/
		$this->db->select('id_master');
		$this->db->from('qv_head_pp_complite');					
		$this->db->where('flag_fpb','1');
		$this->db->where('status','1');
		$total = $this->db->count_all_results();
				
		//$total=$this->db->count_all_results("qv_vendor");
		//$total=$this->db->count_all_results("qv_head_pp_complite");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		
		
		if($search!=""){						
		    $this->db->where('flag_fpb','1');				   
			$this->db->where("status","1");			
			$this->db->like("id_master",$search);	
			$this->db->or_like("user_submission",$search);								
			$this->db->where('flag_fpb','1');
			$this->db->where("status","1");	
			$this->db->or_like("vendor",$search);								
			$this->db->where('flag_fpb','1');
			$this->db->where("status","1");	
			$this->db->or_like("date_aprove_bod",$search_date_approved);								
			$this->db->where('flag_fpb','1');
			$this->db->where("status","1");	
			$this->db->or_like("date_pp",$search_date_pp);								
			$this->db->where('flag_fpb','1');
			$this->db->where("status","1");	
			
		}

		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);	
		$this->db->where('flag_fpb','1');																						
		$this->db->where("status","1");		
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('date_pp','DESC');
		$query=$this->db->get('qv_head_pp_complite');


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		
		if($search!=""){
			$this->db->where('flag_fpb','1');
		    $this->db->where("status","1");			
			$this->db->like("id_master",$search);	
			$this->db->or_like("user_submission",$search);								
			$this->db->where('flag_fpb','1');
			$this->db->where("status","1");	
			$this->db->or_like("vendor",$search);								
			$this->db->where('flag_fpb','1');
			$this->db->where("status","1");	
			$this->db->or_like("date_aprove_bod",$search_date_approved);								
			$this->db->where('flag_fpb','1');
			$this->db->where("status","1");	
			$this->db->or_like("date_pp",$search_date_pp);								
			$this->db->where('flag_fpb','1');
			$this->db->where("status","1");							
		    $jum=$this->db->get('qv_head_pp_complite');
		    $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $row_tbl) {
			
			 if ($row_tbl['status_send_aprove'] == "-1")	
			 {
				   $info_aproval = '<label style="color:red">'."Reject".'</label>';
			 }else{				
				   $info_aproval = "New Request";				
			 }
			 
			 If ($row_tbl['attach_quo'] != "")
			  {  
			   $attach_quo = '<a href='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-primary">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".'View'.'</a>' ;
			 }else{
				$attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
			 }			 			
		   
		   
		   $btn_view_detail = '<a href="#" class="btn btn-primary detail" id="detail" style="text-decoration:none" req_detail='.$row_tbl["id_master"].'><span class="glyphicon glyphicon-list" aria-hidden="true"></span>Detail</a>';
						
			
			
			if ($row_tbl['date_aprove_bod'] != "") {
			   $strdateapproved = date('d-m-Y', strtotime($row_tbl['date_aprove_bod'])) ; 
		    }else{
				$strdateapproved = "-";
			}  
			
			
			if($row_tbl['gran_total_adjust'] != "" ) {
			  $strchk =  '<div align="center"><input  name="msg[]" type="checkbox" value= '.$row_tbl['id_master'].'></div>';
			}else{	  
				if ($row_tbl['flag_bpk'] == 1){             
				   $strchk = '<div style=" font-family:Arial;color:#FFF;" align="center">BPK Input</div>';               
				}else{
				   $strchk = '<div style=" font-family:Arial;color:#F00;" align="center">Nothing Adjustment</div>';
				}
			}
			
			  if ($row_tbl['flag_bpk'] == 1) {
                 $strbpk ='<div style=" font-family:Arial;color:#FFF;" align="center">From Create BPK</div>' ;
		      }else{
                 $strbpk = '<a href="#" class="btn btn-primary btn-setting bpkprint" id="reject" style="text-decoration:none" req_id="'.$row_tbl['id_master'].'" req_gtotal="'.$row_tbl['gran_total'].'" req_ppn="'.$row_tbl['ppn'].'"  req_pph="'.$row_tbl['pph'].'" req_gtotalppn="'.$row_tbl['gran_totalppn'].'" req_gadjust="'.$row_tbl['gran_total_adjust'].'" req_note="'.$row_tbl['note_bpk'].'"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>BPK Print</a>' ;
			  }

				 
			$output['data'][]=array( 
			 						$strchk,	
									$strbpk	,	
									$btn_view_detail,
								    $attach_quo,			                       																	
									$row_tbl['id_master'],
									$row_tbl['short'],
									$row_tbl['dept'],
									$row_tbl['vendor'],		
									$row_tbl['header_desc'],
									substr($row_tbl['remarks'],0,25),
									$row_tbl['user_submission'],									
									date('d-m-Y', strtotime($row_tbl['date_pp'])),									
									date('d-m-Y', strtotime($row_tbl['date_aprove_bod'])),																									
									$row_tbl['time_approved'],
									$row_tbl['currency'],																															
									number_format($row_tbl['gran_total'],2,'.',','),
									number_format($row_tbl['ppn'],2,'.',','),
									number_format($row_tbl['pph'],2,'.',','),
									number_format($row_tbl['gran_totalppn'],2,'.',','),		
									$row_tbl['term_top'],	
									//$row_tbl['nomor_coa']																																																																																																																																																																																																																						
							  );			
		}
		echo json_encode($output);
	}	


	function cath_data_fordatatables_rieceved(){
	
		//  $str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
		  $str_idcompany = $this->session->userdata('id_company'); 
		  $str_iddept = $this->session->userdata('id_dept'); 
		  $access = $this->session->userdata('apps_accsess'); 
		  //$access = 0;
		  $str_status_send = "1";
			$str_status_approved = "1";
		
		 $draw=$_REQUEST['draw'];
		 $length=$_REQUEST['length'];
		 $start=$_REQUEST['start'];
	
		 /*Keyword yang diketikan oleh user pada field pencarian*/
		 $search=$_REQUEST['search']["value"];
	
		 //order short column
		 $column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top");
		 
		 /*Menghitung total qv didalam database*/
		  $this->db->select('id_master');
		  $this->db->from('qv_head_pp_complite');
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
			$this->db->where("status","1");
			$this->db->where("flag_po","1");
		  $this->db->like("id_master",$search);
		  $this->db->or_like("user_submission",$search);
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");				
			$this->db->where("status","1");
			$this->db->where("flag_po","1");
		  $this->db->or_like("header_desc",$search);	
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
			$this->db->where("status","1");
			$this->db->where("flag_po","1");
		  $this->db->or_like("short",$search);	
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
			$this->db->where("status","1");
			$this->db->where("flag_po","1");
		  $this->db->or_like("vendor",$search);	
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
			$this->db->where("status","1");
			$this->db->where("flag_po","1");
		  $total = $this->db->count_all_results();
	
		 /*Mempersiapkan array tempat kita akan menampung semua data
		 yang nantinya akan server kirimkan ke client*/
		 $output=array();
	
		 /*Token yang dikrimkan client, akan dikirim balik ke client*/
		 $output['draw']=$draw;
	
		 /*
		 $output['recordsTotal'] adalah total data sebelum difilter
		 $output['recordsFiltered'] adalah total data ketika difilter
		 Biasanya kedua duanya bernilai sama, maka kita assignment 
		 keduaduanya dengan nilai dari $total
		 */
		 $output['recordsTotal']=$output['recordsFiltered']=$total;
	
		 /*disini nantinya akan memuat data yang akan kita tampilkan 
		 pada table client*/
		 $output['data']=array();
	
	
		 /*Jika $search mengandung nilai, berarti user sedang telah 
		 memasukan keyword didalam filed pencarian*/
		 if($search!=""){
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
					$this->db->where("status","1");
					$this->db->where("flag_po","1");
				  $this->db->like("id_master",$search);
				  $this->db->or_like("user_submission",$search);
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);		
				  $this->db->where("aprove_bod","1");		
					$this->db->where("status","1");
					$this->db->where("flag_po","1");
				  $this->db->or_like("header_desc",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
					$this->db->where("status","1");
					$this->db->where("flag_po","1");
				  $this->db->or_like("short",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
					$this->db->where("status","1");
					$this->db->where("flag_po","1");
				  $this->db->or_like("vendor",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
					$this->db->where("status","1");
					$this->db->where("flag_po","1");
		 }
	
			  /*Lanjutkan pencarian ke database*/
			  $this->db->limit($length,$start);		
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  //$this->db->where("id_dept",$str_iddept);			 
			  $this->db->where("aprove_bod","1");
			  /*Urutkan dari alphabet paling terkahir*/
					$this->db->where("status","1");
					$this->db->where("flag_po","1");
			  // $this->db->order_by('date_pp','DESC');
			 
			  //order column
			  if (isset($_REQUEST["order"])){
				   $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
			  }else{
				   $this->db->order_by('aprove_head','desc');
				   $this->db->order_by('approve_purchasing','desc');
				   $this->db->order_by('aprove_fc','desc');
				   $this->db->order_by('aprove_bod','desc');
			   }
		   
			   $this->db->order_by('aprove_head','asc');
			   $this->db->order_by('approve_purchasing','asc');
			   $this->db->order_by('aprove_fc','asc');
			   $this->db->order_by('aprove_bod','asc');	
			   $this->db->order_by('date_send_aproval','DESC');  
			   $query=$this->db->get('qv_head_pp_complite');
	
	
		 /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		 dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		 yang mengandung keyword tertentu
		 */
		 
		 if($search!=""){
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
					$this->db->where("status","1");
					$this->db->where("flag_po","1");
				  $this->db->like("id_master",$search);
				  $this->db->or_like("user_submission",$search);
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);		
				  $this->db->where("aprove_bod","1");		
					$this->db->where("status","1");
					$this->db->where("flag_po","1");
				  $this->db->or_like("header_desc",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
					$this->db->where("status","1");
					$this->db->where("flag_po","1");
				  $this->db->or_like("short",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("aprove_bod","1");
					$this->db->where("status","1");
					$this->db->where("flag_po","1");
				  $this->db->or_like("vendor",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
					$this->db->where("status","1");
					$this->db->where("flag_po","1");
				 
				  $jum=$this->db->get('qv_head_pp_complite');
				  $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		 }
	
	
	
	
		 
		 foreach ($query->result_array() as $row_tbl) {
	
			$flag_received = $row_tbl['flag_recived'];
			  
			  If ($row_tbl['attach_quo'] != "")
			   {  
				$attach_quo = '<div  align="center"><a href='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
			   }else{
				 $attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
			   }
		 
				$btn_input_grn = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 input_grn" id="input_grn" style="text-decoration:none" id_pp='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i></a></center>';//button detail
			
			
			
			 If ($row_tbl['aprove_head'] == "1")
			  {
				 $head_approval = "<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";
				 
			  }
			  
			   If ($row_tbl['approve_purchasing'] == "1")
			   {
					 //$purchase_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
					 $purchase_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
				 
			  }
				
				If ($row_tbl['aprove_fc'] == "1")
			   {
					 //$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
					 $fc_aproval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
				  }else{
					   If ($row_tbl['status_send_aprove'] == "-1")
					   {
						 $fc_aproval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
					   }else{
						 $fc_aproval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
					   }
				}
				
				 
				
				   If ($row_tbl['aprove_bod'] == "1")
				  {
						  $bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
					 
				   }
				  
			 
					 $strbodaprove  = $row_tbl["aprove_bod"] ;
					 $strpurchaseaprove = $row_tbl["approve_purchasing"] ;
					 $strfcaprove   = $row_tbl["aprove_fc"] ;
					 $strheadaprove = $row_tbl["aprove_head"] ;	
	
			
	
			 $chk_idmaster ='<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value='.$row_tbl["id_master"].' class="getPO ace" req_id_del='.$row_tbl["id_master"].' />
			<span class="lbl"></span> ';				
				
	
	//Pembedaan Warna pada type purchase.
			if ($row_tbl['flag_purchase'] == "1") {
				$type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
			}else{
				  if ($row_tbl['flag_purchase'] == "2") {
						$type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
					}else{
						  if ($row_tbl['flag_purchase'] == "3") {
							$type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
						}else{
							  if ($row_tbl['flag_purchase'] == "4") {
									$type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
								}else{
									 $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
							  };
					  };
				  };   
			  };
	//end------------------------------			 
	
	
	
	//label colum 1 idepp
	$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP
	//end label
	
	//header desc
	$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";
	//end header desc.
	
	
	//button detail ---
	$btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail"  id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon 	glyphicon-list  " aria-hidden="true"></i></a></center>';//button detail
	//--------
	
	
	
		//button input grn
		if($flag_received == '0' || $flag_received == null) {
			$str_grn_input = '<button class="btn btn-app btn-danger btn-xs radius-8" type="submit" id="btngrn" name="btngrn" value ='.$row_tbl['id_master'].' ><i class="ace-iconfa glyphicon 	glyphicon-edit bigger-160"></i> </button>';
			$company = $row_tbl['short'];
			$submission =  $row_tbl['user_submission'];
			$dept = $row_tbl['dept'];
			$vendor = $row_tbl['vendor'];
			$date_send = date('d-m-Y', strtotime($row_tbl['date_pp']));
			$currency = $row_tbl['currency'];
			$total = number_format($row_tbl['gran_total'],2,'.',',');
			$total_ppn = number_format($row_tbl['gran_totalppn'],2,'.',',');
			$term = $row_tbl['term_top'];
			$closing_po = '<button class="btn btn-app btn-success btn-xs radius-4 btnpo btn-clean closingpo" type="button" id="closingpo" name="closingpo" req_id ='.$row_tbl["id_master"].' OnClick="closingPO(this)" ><i class="fa fa-check-circle-o bigger-160"></i> </button>';
	
		} else {
			$str_grn_input = '<button class="btn btn-app btn-danger btn-xs radius-8" type="submit" id="btngrn" name="btngrn" value ='.$row_tbl['id_master'].' disabled><i class="ace-iconfa glyphicon 	glyphicon-edit bigger-160"></i> </button>';
			$company = '<span class="badge badge-success">'.$row_tbl['short'].'</span>';
			$submission =  '<span class="badge badge-success">'.$row_tbl['user_submission'].'</span>';
			$dept = '<span class="badge badge-success">'.$row_tbl['dept'].'</span>';
			$vendor = '<span class="badge badge-success">'.$row_tbl['vendor'].'</span>';
			$date_send = '<span class="badge badge-success">'.date('d-m-Y', strtotime($row_tbl['date_pp'])).'</span>';
			$currency = '<span class="badge badge-success">'.$row_tbl['currency'].'</span>';
			$total = '<span class="badge badge-success">'.number_format($row_tbl['gran_total'],2,'.',',').'</span>';
			$total_ppn = '<span class="badge badge-success">'.number_format($row_tbl['gran_totalppn'],2,'.',',').'</span>';
			$term = '<span class="badge badge-success">'.$row_tbl['term_top'].'</span>';
			$closing_po = '<button class="btn btn-app btn-success btn-xs radius-4 btnpo btn-clean closingpo" type="button" id="closingpo" name="closingpo" req_id ='.$row_tbl["id_master"].' OnClick="closingPO(this)" disabled><i class="fa fa-check-circle-o bigger-160"></i> </button>';
		}
	//-----------------------------------------------
	
	// Kalau Login sebagai Admin atau Finance
		if($access == 1 || $access == 2 ) {
			 $output['data'][]=array(
									// $chk_idmaster,
									 $idmas,
									 $btn_view_detail,
									 $attach_quo,															
									 $str_grn_input	,
									 $closing_po,
									 $company,
									 $submission,
									 $dept,
									 $header_desc ,//$row_tbl['header_desc'],
									 $vendor,																								
									 $date_send,	
									 $currency,
									 $type_purchase, //$row_tbl['type_purchase'],
									 $total,
									 $total_ppn,
									 $term,
									/* $head_approval.$datehead,
									 $purchase_approval.$datepur,
									 $fc_aproval.$datefc,
									 $bod_approval.$datebod, */	
																	 
							   );	
			} else {
				$output['data'][]=array(
					// $chk_idmaster,
					 $idmas,
					 $btn_view_detail,
					 $attach_quo,															
					 $str_grn_input	,
					 $company,
					 $submission,
					 $dept,
					 $header_desc ,//$row_tbl['header_desc'],
					 $vendor,																								
					 $date_send,	
					 $currency,
					 $type_purchase, //$row_tbl['type_purchase'],
					 $total,
					 $total_ppn,
					 $term,
					/* $head_approval.$datehead,
					 $purchase_approval.$datepur,
					 $fc_aproval.$datefc,
					 $bod_approval.$datebod, */	
													 
			   );	
			}		
		 }
		 echo json_encode($output);
	 }	
	 
	  function cath_data_fordatatables_bpk(){
		
		//  $str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
		  $str_idcompany = $this->session->userdata('id_company'); 
		  $str_iddept = $this->session->userdata('id_dept'); 
		  $str_status_send = "1";
		  $str_status_approved = "1";
		 /*Menagkap semua data yang dikirimkan oleh client*/
	
		 /*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		 server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		 sesuai dengan urutan yang sebenarnya */
		 $draw=$_REQUEST['draw'];
	
		 /*Jumlah baris yang akan ditampilkan pada setiap page*/
		 $length=$_REQUEST['length'];
	
		 /*Offset yang akan digunakan untuk memberitahu database
		 dari baris mana data yang harus ditampilkan untuk masing masing page
		 */
		 $start=$_REQUEST['start'];
	
		 /*Keyword yang diketikan oleh user pada field pencarian*/
		 $search=$_REQUEST['search']["value"];
	
		 //order short column
		 $column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top");
		 
		 /*Menghitung total qv didalam database*/
		  $this->db->select('id_master');
		  $this->db->from('qv_head_pp_complite');
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
		  $this->db->where("status","1");
		  $this->db->like("id_master",$search);
		  $this->db->or_like("user_submission",$search);
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");				
		  $this->db->where("status","1");
		  $this->db->or_like("header_desc",$search);	
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
		  $this->db->where("status","1");
		  $this->db->or_like("short",$search);	
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
		  $this->db->where("status","1");
		  $this->db->or_like("vendor",$search);	
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
		  $this->db->where("status","1");
		  $total = $this->db->count_all_results();
	
		 /*Mempersiapkan array tempat kita akan menampung semua data
		 yang nantinya akan server kirimkan ke client*/
		 $output=array();
	
		 /*Token yang dikrimkan client, akan dikirim balik ke client*/
		 $output['draw']=$draw;
	
		 /*
		 $output['recordsTotal'] adalah total data sebelum difilter
		 $output['recordsFiltered'] adalah total data ketika difilter
		 Biasanya kedua duanya bernilai sama, maka kita assignment 
		 keduaduanya dengan nilai dari $total
		 */
		 $output['recordsTotal']=$output['recordsFiltered']=$total;
	
		 /*disini nantinya akan memuat data yang akan kita tampilkan 
		 pada table client*/
		 $output['data']=array();
	
	
		 /*Jika $search mengandung nilai, berarti user sedang telah 
		 memasukan keyword didalam filed pencarian*/
		 if($search!=""){
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("status","1");
				  $this->db->like("id_master",$search);
				  $this->db->or_like("user_submission",$search);
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);		
				  $this->db->where("aprove_bod","1");		
				  $this->db->where("status","1");
				  $this->db->or_like("header_desc",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("status","1");
				  $this->db->or_like("short",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("status","1");
				  $this->db->or_like("vendor",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("status","1");
		 }
	
			  /*Lanjutkan pencarian ke database*/
			  $this->db->limit($length,$start);		
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  //$this->db->where("id_dept",$str_iddept);			 
			  $this->db->where("aprove_bod","1");
			  /*Urutkan dari alphabet paling terkahir*/
			  $this->db->where("status","1");
			  // $this->db->order_by('date_pp','DESC');
			 
			  //order column
			  if (isset($_REQUEST["order"])){
				   $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
			  }else{
				   $this->db->order_by('aprove_head','desc');
				   $this->db->order_by('approve_purchasing','desc');
				   $this->db->order_by('aprove_fc','desc');
				   $this->db->order_by('aprove_bod','desc');
			   }
		   
			   $this->db->order_by('aprove_head','asc');
			   $this->db->order_by('approve_purchasing','asc');
			   $this->db->order_by('aprove_fc','asc');
			   $this->db->order_by('aprove_bod','asc');	
			   $this->db->order_by('date_send_aproval','DESC');  
			   $query=$this->db->get('qv_head_pp_complite');
	
	
		 /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		 dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		 yang mengandung keyword tertentu
		 */
		 
		 if($search!=""){
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("status","1");
				  $this->db->like("id_master",$search);
				  $this->db->or_like("user_submission",$search);
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);		
				  $this->db->where("aprove_bod","1");		
				  $this->db->where("status","1");
				  $this->db->or_like("header_desc",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("status","1");
				  $this->db->or_like("short",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("aprove_bod","1");
				  $this->db->where("status","1");
				  $this->db->or_like("vendor",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("status","1");
				 
				  $jum=$this->db->get('qv_head_pp_complite');
				  $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		 }
	
	
	
	
		 
		 foreach ($query->result_array() as $row_tbl) {
			  
			  If ($row_tbl['attach_quo'] != "")
			   {  
				$attach_quo = '<div  align="center"><a href='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
			   }else{
				 $attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
			   }
		 
			
			
			  $btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i></a></center>';//button detail
			
			
				
				 
				
				   If ($row_tbl['aprove_bod'] == "1")
				  {
						  $bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
					 
				   }
				  
			 
	$strbodaprove  = $row_tbl["aprove_bod"] ;
	$strpurchaseaprove = $row_tbl["approve_purchasing"] ;
	$strfcaprove   = $row_tbl["aprove_fc"] ;
	$strheadaprove = $row_tbl["aprove_head"] ;			
				
	
	//Pembedaan Warna pada type purchase.
			if ($row_tbl['flag_purchase'] == "1") {
				$type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
			}else{
				  if ($row_tbl['flag_purchase'] == "2") {
						$type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
					}else{
						  if ($row_tbl['flag_purchase'] == "3") {
							$type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
						}else{
							  if ($row_tbl['flag_purchase'] == "4") {
									$type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
								}else{
									 $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
							  };
					  };
				  };   
			  };
	//end------------------------------			 
	
	
	//label colum 1 idepp
	$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP
	
	//end label
	$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";
	
	//tombol fpb......
	$strprintfbp_ok = '<button class="btn btn-app btn-success btn-xs radius-4 btnpo btn-clean fpbbutton" type="button" id="fpbbutton" name="fpbbutton" req_id ='.$row_tbl["id_master"].' onclick="window.location.href="" ><i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i> </button>';
	//end---------------------------
		  
			 $output['data'][]=array(
									 $idmas,
									// $btn_view_detail,
									// $attach_quo,															
									 $strprintfbp_ok,
									 $row_tbl['short'],
									 $row_tbl['user_submission'],
									 $row_tbl['dept'],
									 $header_desc ,//$row_tbl['header_desc'],
									 $row_tbl['vendor'],																								
									 date('d-m-Y', strtotime($row_tbl['date_pp'])),
									 $row_tbl['term_top']." "."Days",									 
									 $type_purchase, //$row_tbl['type_purchase'],
									 $row_tbl['currency'],
									 number_format($row_tbl['gran_total'],2,'.',','),
									 number_format($row_tbl['gran_totalppn'],2,'.',','),
											 
							   );			
		 }
		 echo json_encode($output);
	 }	

	 public function get_recieved_modal(){
		$id_master = $this->input->post('id_master');
		$getdatadetailgrn = $this->M_history_finance->getListHistoryParsial($id_master);
		$no_po = $this->M_history_finance->getNoPO($id_master)[0];
		$access = $this->session->userdata('apps_accsess');
		$no="1";
		$no_id="1";	
		$no_id_print="1"; 	
		echo "<div style='overflow-x:auto;'>" ;
		echo "<table class='table table-striped table-bordered table-hover'>";
		echo  "<tr style='font-weight:bold; font-size:11px'>";
		echo  "<td>No</td>";
		echo  "<td>PR No</td>";
		echo  "<td>PO No</td>";
		echo  "<td>GRN No</td>";
		echo  "<td>Date GRN</td>";
		echo  "<td >Grand Total</td>";
		echo  "<td >Detail</td>";
		echo  "<td >Print</td>";

		if($access == 1 || $access == 2) {
			echo  "<td >Give Right</td>";
		}
		
		echo  "</tr> "; 	 	 	 				
		 foreach ($getdatadetailgrn as $row_det_grn) {
			 $no_pp = base64_encode($row_det_grn->id_master);
			 $number_grn = base64_encode($row_det_grn->number_grn);
			
			 $url = base_url('finbpkall_form/c_history_finance/print_bpk?pp='.$no_pp.'&grn='.$number_grn.'#toolbar=0');
			 $url_pdf = "printJS({printable:'$url',type:'pdf',showModal:true});disableButtonPrint(this)";
			if($row_det_grn->flag_print_bpk == '0') {
				// $button_print = '<button type="button" OnClick="PrintPDF(this)" req_id_print=print'.$no_id++.' req_id="'.$url.'" class="btn btn-info">
				// <span class="fa fa-print"></span> Print BPK
				// </a>';
				
				$button_print = '<button type="button" OnClick='.$url_pdf.' req_id="'.$url.'" class="btn btn-info">
				<span class="fa fa-print"></span> Print BPK
				</a>';

				// $button_print = '<a target="_blank" OnClick=\'return confirm("Print this Data?");\' href="'.$url.'" class="btn btn-info">
				// <span class="fa fa-print"></span> Print BPK
				// </a>';
			} else {
				$button_print = '<button type="button" class="btn btn-info" disabled>
				<span class="fa fa-print"></span> Print BPK
				</button>';
			}

			 echo  "<tr style='font-size:12px'> ";
			 echo '<td>'.$no++.'</td>';	
			 echo '<td>'.$no_po->id_master.'</td>';
			 echo '<td>'.$no_po->po_no_created.'</td>';
			 echo '<td>'.$row_det_grn->number_grn.'</td>';		
			 echo '<td>'.date('d-m-Y H:i:s', strtotime($row_det_grn->date_grn)).'</td>';
			 echo '<td>'.number_format($row_det_grn->gran_total,2,'.',',').'</td>';
			 echo '<td>
			 		<button type="button" class="btn btn-success" onClick="tampilDetailGRN(this)" req_id='.$row_det_grn->number_grn.'>
					<span class="fa fa-search"></span> Detail
					</button>

					
					</a>
				  </td>';
			 echo '<td>
			 		'.$button_print.'
				  </td>';

				if($access == 1 || $access == 2) {
					echo '<td><button type="button" class="btn btn-success" onClick="giveRightFPB(this)" req_id='.$row_det_grn->number_grn.'>Give Right</button></td>';
				}


			// echo '<td>
			// <iframe id="print'.$no_id_print++.'" src='.$url.'> 
			// </iframe>
			// </td>';
			 echo "</tr>"; 							
			 
		 }

		 

		 echo '</div>';

	}
	
	public function giveRightFPB() {
		$number_grn = $this->input->get('number_grn');

		$data = array('flag_print_bpk' => '0'
					);

		$update = $this->M_history_finance->giveRightFPB($number_grn, $data);
		
		if($update) {
			echo json_encode('Success');
		} else {
			echo json_encode('Failed');
		}
	}

	public function print_bpk() {
		$number_grn = base64_decode($this->input->get('grn'));
		$no_pp = base64_decode($this->input->get('pp'));

		$data = array(	'flag_bpk' => '1',
						'flag_print_bpk' => '1',
						'date_print_bpk' => date('Y-m-d H:i:s')
					);

		// Print PDF with TCPDF

		$master_pp = $this->M_history_finance->getDataMasterPPForPrint($no_pp)[0];
		$master_grn = $this->M_history_finance->getMasterGRN($number_grn)[0];
		$detail_grn = $this->M_history_finance->getDetailGRN($number_grn);

		// Cek apakah sudah di print atau belum

		if($master_grn->flag_print_bpk > 0) {
			echo '<a style="font-size: 24px" href="'.base_url('finbpkall_form/c_history_finance').'"> This Data has been Printed!, please click to back </a>';
			die;
		}

		 $update = $this->M_history_finance->giveRightFPB($number_grn, $data);
		
		$this->load->library('Pdf');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator('IT @Eurokars Group Indonesia');
        $pdf->SetAuthor('IT @Eurokars Group Indonesia');
        $pdf->SetTitle("Bukti Pengeluaran Kas - ".$number_grn);
        $pdf->SetSubject("Bukti Pengeluaran Kas - ".$number_grn);

         // set header and footer fonts
         $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
         $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
 
         // set default monospaced font
         $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 
         // set margins
         $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
         $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
         $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
 
         // set auto page breaks
         $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
         $pdf->setPrintHeader(false);
         //$pdf->setPrintFooter(false);
 
         // set image scale factor
 
 
         // set some language-dependent strings (optional)
         if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
             require_once(dirname(__FILE__).'/lang/eng.php');
             $pdf->setLanguageArray($l);
         }
                 $pdf->SetFont('helvetica', '', 9);

                 $pdf->SetPrintHeader(false);
                 $pdf->SetPrintFooter(false);
 
         // add a page
         $pdf->AddPage();

         $html = '
        
         <!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<style type="text/css">
		.center {
			text-align: center;
		}

		.judul {
			text-align: center;
			font-weight: bold;
			font-size: 10px;
		}

        .garis_bawah {
            border-bottom: 1px solid black;
        }

        .disetujui { 
            text-decoration: line-through;
        }

	</style>

</head>

<body>


	<br>

	<table width="100%" border="0">
		<tr>
			<td> <img src="'.base_url('asset/images/euro.jpg').'" height="20"/> </td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>PT. '.$master_pp->company.'</td>
			<td align="right">No : '.$number_grn.'</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Department : '.$master_pp->dept.'</td> 
			<td align="right">Submission Date : '.date('d-m-Y', strtotime($master_pp->date_send_pr)).'</td>
		</tr>
	</table>

	<br>
	<br>

	<table width="100%" border="0" class="center">
		<tr>
			<td width="5%"></td>
			<td width="90%" class="judul">

			<u>BUKTI PENGELUARAN KAS</u>
			<br>
			<u>( BANK PAYMENT FORM )</u>
				

			</td>

			<td width="5%"></td>
		</tr>
	</table>

	<br>
	<br>

	<table width="100%" border="0">
		<tr>
		
			<td>Payee : '.$master_pp->vendor.'</td>
			<td align="right">Approval Date : '.date('d-m-Y', strtotime($master_pp->date_aproval)).'</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Amount : '.$master_pp->currency.' '.number_format($master_grn->gran_total,2,'.',',').'</td>
			<td align="right">Issued Date : '.date('d-m-Y').'</td>
		</tr>
	</table>

	<br>
	<br>

	<table width="100%" border="1">
		<tr align="center">
		
			<th width="5%">No</th>
			<th width="18%">Description</th>
			<th width="18%">Specs</th>
			<th width="5%">Qty</th>
			<th width="18%">Price</th>
			<th width="18%">Total</th>
			<th width="18%">Tax</th>
		</tr>

		
		';

		// Looping ea

		$no = 1;
		$tax_ppn_pph = $master_grn->total_ppn - $master_grn->total_pph;
		foreach($detail_grn as $det) { 
		//$total_plus_tax = $det->amount_in + ($det->tax_ppn - $det->tax_pph);
		$total_plus_tax = $det->tax_ppn - $det->tax_pph;
		$html .= '
		<tr >
			<th align="center"> '.$no++.'</th>
			<th> '.$det->description.' </th>
			<th> '.$det->spec.' </th>
			<th align="center"> '.$det->qty_in.' </th>
			<th align="right"> '.number_format($det->price,2,'.',',').' &nbsp; </th>
			<th align="right"> '.number_format($det->amount_in,2,'.',',').' &nbsp; </th>
			<th align="right"> '.number_format($total_plus_tax,2,'.',',').' &nbsp; </th>
		</tr>
		';

		}

		$html .= '

		<tr>
			<td colspan="6" align="right"><b>Grand Total : '.$master_pp->currency.' &nbsp; </b></td>
			<td align="right"> <b>'.number_format($master_grn->total_amount,2,'.',',').' &nbsp; </b> </td>
		</tr>

		<tr>
			<td colspan="6" align="right"><b>PPN Amount :  &nbsp; </b></td>
			<td align="right"> <b>'.number_format($master_grn->total_ppn,2,'.',',').' &nbsp; </b> </td>
		</tr>

		<tr>
			<td colspan="6" align="right"><b>PPH Amount :  &nbsp; </b></td>
			<td align="right"> <b>'.number_format($master_grn->total_pph,2,'.',',').' &nbsp; </b> </td>
		</tr>

		<tr>
			<td colspan="6" align="right"><b>Total + Tax :  &nbsp; </b></td>
			<td align="right"> <b>'.number_format($master_grn->gran_total,2,'.',',').' &nbsp; </b> </td>
		</tr>

		<tr>
			<td colspan="7"> <b> amount in words : '.terbilang($master_grn->gran_total).' rupiah &nbsp; </b> </td>
		</tr>

	</table>

	<br>
    <br>

	<table width="100%" border="0">
			<tr>
				<td> <b> Note : </b> </td>
			</tr>
	</table>

        
    <br>
    <br>
    
	<table width="100%" border="1" align="center">
		
		<tr>
			<th colspan="2"> <font style="font-weight: bold" size="8px"> TTD Giro / Cheque </font> </th>
			<th rowspan="2"><font style="font-weight: bold" size="8px">Management</font></th>
			<th rowspan="2"> <font style="font-weight: bold" size="8px"> Checker II </font> </th>
			<th rowspan="2"> <font style="font-weight: bold" size="8px"> Checker I </font> </th>
			<th rowspan="2"> <font style="font-weight: bold" size="8px"> Accounting  </font> </th>
			<th rowspan="2"> <font style="font-weight: bold" size="8px"> Finance  </font> </th>
			<th rowspan="2"> <font style="font-weight: bold" size="8px"> Recipient  </font>  </th>
		
		</tr>

		<tr>
			<td> I </td>
			<td> II </td>
		</tr>

		<tr>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
		</tr>

		<tr>
			<td> '.$this->session->userdata('sign_giro1').' </td>
			<td> '.$this->session->userdata('sign_giro2').' </td>
			<td> '.$this->session->userdata('sign_pimpinan').' </td>
			<td> '.$this->session->userdata('sign_pemeriksa2').' </td>
			<td> '.$this->session->userdata('sign_pemeriksa1').' </td>
			<td> '.$this->session->userdata('sign_accounting').' </td>
			<td> '.$this->session->userdata('sign_finance').' </td>
			<td> '.$this->session->userdata('sign_penerima').' </td>
		</tr>

	</table>	
    

</body>

</html>

                 
         ';

         // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        $file = 'Bukti Pengeluaran Kas - '.$number_grn.'.pdf';

        // reset pointer to the last page
        $pdf->lastPage();

        $pdf->Output($file, 'I');
	}

	public function get_history_grn()
    {		
	
		$number_grn = $this->input->get('number_grn');
     $master_grn = $this->M_history_finance->getMasterGRN($number_grn);
	 $detail_grn = $this->M_history_finance->getDetailGRN($number_grn);	 
	  
	 $no="1";	 	
	 echo  "<div style='overflow-x:auto'>";
	 echo  "<table class ='table table-striped table-bordered table-hover'>";
	 echo  "<thead>" ;
	 echo  "<tr style='font-weight:bold; font-size:11px'>";
	 echo  "<td width='2%'>No</td>";
	 echo  "<td width='22%'>Description</td>";
	 echo  "<td width='22%'>Specs</td>";
	 echo  "<td width='7%'>Qty</td>";
	 echo  "<td width='15%'>Prices</td>";
	 echo  "<td width='25%'>Total Prices</td>";
	 echo  "<td width='7%'>PPN</td>";
	 echo  "<td width='7%'>PPH</td>";
	 echo  "<td width='25%'>PPN Amount</td>";
	 echo  "<td width='25%'>PPH Amount</td>";
	 echo  "<td width='25%'>Total + Tax</td>";
	 echo  "</tr> "; 
	 echo  "</thead>";	 	 	 				
		foreach ($detail_grn as $row_jq) {
			$total_plus_tax = $row_jq->amount_in + ($row_jq->tax_ppn - $row_jq->tax_pph);
			

		    echo  "<tr style='font-size:12px'> ";
			echo '<td>'.$no++.'</td>';			
			echo '<td>'.$row_jq->description.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			echo '<td>'.$row_jq->qty_in.'</td>';
			echo '<td>'.number_format($row_jq->price,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->amount_in,2,'.',',').'</td>';
			echo '<td>'.$row_jq->type_ppn.'</td>';
			echo '<td>'.$row_jq->type_pph.'</td>';
			echo '<td>'.number_format($row_jq->tax_ppn,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->tax_pph,2,'.',',').'</td>';
			echo '<td>'.number_format($total_plus_tax,2,'.',',').'</td>';
		    echo "</tr>"; 							
			
		}
				
	 foreach ($master_grn as $row_jm) {	
		
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>Sub Total :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_amount,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>PPN Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_ppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 	
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>PPH Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_pph,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>Total + Tax :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_total,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 
	 }

	 echo "</table>";	
		 echo "</div>";
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}
	
}

