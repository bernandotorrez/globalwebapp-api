<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>


 <?php  echo form_open('finbpkall_form/c_history_finance/multiple_submit');  ?>     
 
 <table>
   <tr>
     <td>
     <!-- btn btn-app btn-danger btn-xs radius-4 btnclear btn-clean -->
      <button class="btn btn-app btn-danger btn-xs radius-4 btnclear btn-clean" type="submit" id="btnclearadjust" name="btnclearadjust"  value ="Clear Adjustment" disabled="disabled" />
        <i class="ace-icon fa fa-exchange bigger-160"></i>
          Clear Adj 
		  </button> 
     </td>	  
     <td>&nbsp;</td>	     
   </tr> 
 </table>   
 <br />
 
<div class="row">
 <div class="col-md-12 col-md-offset-0">   
	
	 <table id="myTable" class="table table-striped table-bordered table-hover"   >			
	  <thead align="center" > 		
      <th >Action</th>
      <th >Print</th>
      <th >View Detail</th>	
      <th >Quo</th>
	    <th >PP No</th>
	    <th >Company</th>
	    <th >Dept</th>					
	    <th >Vendor</th>	      		
	    <th >Description</th>	
      <th >Remarks</th>		
      <th >Subbmision</th>	
      <th >Date Subbmision</th>	 
	    <th >Date Approval</th>	  
      <th >Time Approval</th>	 
	    <th >Cur</th>
	    <th >Total</th>
      <th >PPN</th>
      <th >PPH</th>
      <th >Total+TAX</th>
	    <th >Term</th>      					  	  			 
	  </thead>	    						
  </table>	  								 		
        
 </div>  
</div> 
                
   </div>
  </div>
 		 


<!-- Modal -->
        <div class="modal fade" id="modaldetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Info & Remarks</h4>
                    </div>
                    
                    <div class="modal-body">
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->            
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal -->  	

<!-- Modal Buat Note BPK -->

  <div class="modal fade" id="myModal-bpkprint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Note And Adjustment BPK</h4>
                    </div>
                    
               <div class="modal-body-bpkprint">
                      <table width="90%" class="table-condensed" align="center">
                        <tbody > 
                          
                            <tr> 
                                <td> <label for="PP Number"  class="col-xs-10">PP Number</label></td> 
                             
                                  <td>
                                   <div class="col-xs-10">
                                    <input type="text" id="txtppno" name="txtppno" class="form-control btn-success" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                             <tr> 
                                <td> <label for="Grand Total"  class="col-xs-11">Grand Total</label></td> 
                             
                                  <td>
                                   <div class="col-xs-10" >
                                    <input type="text" id="txtgtotal" name="txtgtotal" class="form-control btn-danger gtotal_static" onkeypress="return isNumberKey(event)" readonly="readonly" >
                                   </div> 
                                  </td>                                  
                             </tr>     
                             <tr> 
                                <td> <label for="PPN"  class="col-xs-11">PPN</label></td> 
                             
                                  <td>
                                   <div class="col-xs-10" >
                                    <input type="text" id="txtppn" name="txtppn" class="form-control btn-danger gtotal_static" onkeypress="return isNumberKey(event)" readonly="readonly" >
                                   </div> 
                                  </td>                                  
                             </tr>     
                             <tr> 
                                <td> <label for="PPH"  class="col-xs-11">PPH</label></td> 
                             
                                  <td>
                                   <div class="col-xs-10" >
                                    <input type="text" id="txtpph" name="txtpph" class="form-control btn-danger gtotal_static" onkeypress="return isNumberKey(event)" readonly="readonly" >
                                   </div> 
                                  </td>                                  
                             </tr>     
                             <tr> 
                                <td> <label for="Grand Total"  class="col-xs-11">Grand Total+TAX</label></td> 
                             
                                  <td>
                                   <div class="col-xs-10" >
                                    <input type="text" id="txtgtotalppn" name="txtgtotalppn" class="form-control btn-danger gtotal_static" onkeypress="return isNumberKey(event)" readonly="readonly" >
                                   </div> 
                                  </td>                                  
                             </tr>     
                                                                               
                            
                                  <tr>                              
                     <td><label for="Grand Total"  class="col-xs-11">Grand Total Adjust</label></td>
                       <td>
                            <div class="col-xs-10">
                                    <input type="text" id="txtcathadjust" name="txtcathadjust" placeholder="0.00" class="form-control btn-warning cadjust" onkeypress="return isNumberKey(event)" readonly="readonly" >
                                  
                           </div> 
                           
                           <div class="col-xs-1" id="imgsucces">
                           	  <img src="<?php echo base_url("asset/images/success_ico.png") ?>">
                           </div>
                       </td>                   
                   </tr>                  
                       
                     <tr id="note_row"> 
                        <td ><label for="bpk note"  class="col-xs-9">Note</label></td> 
                        <td>
                          <div class="col-xs-12">
                            <textarea id="txtnote" name="txtnote"  class="form-control"></textarea>
                          </div>  
                        </td>    
                     </tr>  
            <tr id ="tdinfo">                        
              <td  style="color:#F00" ><label class="col-xs-9">Information :</label></td>
              <td  style="color:#F00" ><label class="col-xs-9">Grand Total Adjustment Already Exsist</label></td>
            </tr>            
                  </tbody>
                 </table>                                  
           
           <div class="panel-body">                               
                <table class="table-bordered"  align="center" id="table_detail_adjust">                                 
                     <tr align="center" class="btn-default" >
                            <td width="20%">Description</td>
                            <td width="27%">Value Adjust </td>
                     </tr>                            
                  <tbody class="detail">                         
                      <tr>
                        <td>                    
                        <input type="text" id="flagstat" name="flagstat[]"  value="1"  hidden="true"  />
                        <input type="text" maxlength="70" id="txtdesc1"  name="txtdesc[]"  placeholder="Description" size="24" class="form-control txtdesc" />
                        </td>                
                        <td>
                         <input maxlength="14" id="txtprice1"  name="txtprice[]" type="text" placeholder="0.00" size="20" onkeypress="return isNumberKey(event)" class="form-control priceadjst1"  /> 
                        </td>                     
                     </tr>
                      <tr>
                        <td>                        
                        <input type="text" id="flagstat" name="flagstat[]"  value="1"  hidden="true"  />
                        <input type="text" maxlength="70" id="txtdesc2"  name="txtdesc[]"  placeholder="Description" size="24" class="form-control txtdesc" />
                        </td>                
                        <td>
                         <input maxlength="14" id="txtprice2"  name="txtprice[]" type="text" placeholder="0.00" size="20" onkeypress="return isNumberKey(event)" class="form-control priceadjst2"  /> 
                         </td>                              
                        </tr>
                        
                  <tr>                              
                   <td style="color:#F00"><label for="Grand Total"  class="col-xs-11">Result Adjustment :</label></td>
                       <td>   
                         <input maxlength="14" id="txtgadjust" name="txtgadjust" placeholder="0.00" class="form-control  hasil" onkeypress="return isNumberKey(event)" readonly="readonly"  >

                       </td>                   
                   </tr>   
                          
                    </tbody> 
                 </table>
               </div>         
           </div>          
                  <div class="modal-footer">
                
                   <input id="btnprintbpk" type="submit" name="btnprintbpk" value="Print BPK" class="btn btn-danger btn_dobpkprint"/>   
                   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>                  
                    
                </div>
            </div>           
        </div>
    
<!-- Modal Buat Note BPK -->


<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 

<script>
$(function() {
  $("#txtcari").focus();
});		  
//set focus ----------------------------  
</script>

<script>
 //get data table remarks to modal popup.
$(function(){
    $(document).on('click','.detail',function(e){
		var req_detail = $(this).attr('req_detail'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("finbpkall_form/c_history_finance/get_nopp_modal"); ?>';
				
		$("#modaldetail").modal('show');    		
  		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {idmaster:req_detail},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});
	
</script>




<script>
$(function(){ // Show modal pop up send noted BPK print
  $(document).on('click','.bpkprint',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref php echo $row->id_master ke variable req_id  ;																	
		var req_gtotal = $(this).attr('req_gtotal');
		var req_ppn = $(this).attr('req_ppn')
		var req_pph= $(this).attr('req_pph');
		var req_gtotalppn = $(this).attr('req_gtotalppn'); 
		var req_gtotal_adjust = $(this).attr('req_gadjust'); 
		var req_note = $(this).attr('req_note');
	
		if (req_gtotal_adjust == "") { // jika grand total adjustment ada maka tampilkan information already exsist
		   $('#tdinfo').hide('fast');	
		   $('#imgsucces').hide('fast');		  
		   $('#note_row').show('fast');
		   $('#table_detail_adjust').show('fast');	
		   $('#btnprintbpk').show('fast');
		}else{
		   $('#tdinfo').show('fast');		
		   $('#imgsucces').show('fast');
		   $('#note_row').hide('fast');
		   $('#table_detail_adjust').hide('fast');
		   $('#btnprintbpk').hide('fast');			
		}
		
		$("#myModal-bpkprint").modal('show');
		$('#txtppno').val(req_id); //menampilkan isi dari  idmaster kedalam inputbox 			
		$('#txtgtotal').val(numeral(req_gtotal).format('0,0.00')); //menampilkan isi dari  grantotal kedalam inputbox 
		$('#txtppn').val(numeral(req_ppn).format('0,0.00')); //menampilkan isi dari  grantotal kedalam inputbox 
		$('#txtpph').val(numeral(req_pph).format('0,0.00')); //menampilkan isi dari  grantotal kedalam inputbox 
		$('#txtgtotalppn').val(numeral(req_gtotalppn).format('0,0.00')); //menampilkan isi dari  grantotal kedalam inputbox 							
	    $('#txtcathadjust').val(numeral(req_gtotal_adjust).format('0,0.00'));//  textbox txtcathadjust				
		$('#txtdesc1').val(''); //mengkosongkan description 1		
		$('#txtprice1').val(''); //mengkosongkan adjust 1		  				
		$('#txtdesc2').val(''); //mengkosongkan description 2					  				
		$('#txtprice2').val(''); //mengkosongkan adjust 2				  				
		$('#txtnote').val(req_note); //mengkosongkan note		  				
		$('#txtgadjust').val(''); //mengkosongkan total adjustmen				
	   });																					
});		
</script>



<script>
//setelah di print adjustment cecklist total adjust and hide text box adjust
$(document).on('click','.btn_dobpkprint',function(e){	
  var strgadjust = $('#txtgadjust').val();
  
  if (strgadjust !=''){
	  $('#tdinfo').show('fast');
	  $('#imgsucces').show('fast');
	  $('#note_row').hide('fast');
	  $('#table_detail_adjust').hide('fast');
	  $('#btnprintbpk').hide('fast');	
   }	
	     
});			  



//setelah di hide lalu di refresh page parent
$('#myModal-bpkprint').on('hidden.bs.modal', function () { //refresh page on close modal
   location.reload();
});


var counterCheckbpk = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckbpk++ : counterCheckbpk--;
    counterCheckbpk == 1 ? $('#btnclearadjust').prop("disabled", false): $('#btnclearadjust').prop("disabled", true);
});
</script>

<script>
//--------------------function disbaled enabled button with check book
$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btnclear').removeAttr('disabled');			
        }
        else{
            $('.btnclear').attr('disabled', 'disabled');								
        }
    });
});
</script>

<script>

$(function(){
    $('#myform').on('submit', function(e){ 
        e.preventDefault();		
		var url = '<?php echo base_url("finbpkall_form/c_history_finance/multiple_submit"); ?>';		
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), 
            success: function(data){
            }
        });
    });
}); 
</script>

<script>
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	
</script>

<script script type="text/javascript">

    $(document).on('change','#txtprice1','#txtprice2',function(e){   
		   var tr =  $(this).parent().parent();			  
		   var price=numeral().unformat($('#txtprice1').val());						 
		   var price2=numeral().unformat($('#txtprice2').val());		   
		   total(); 
    });
   
    $(document).on('blur','#txtprice1',function(e){   
		   var tr =  $(this).parent().parent();		  
		   
		   var price=$('#txtprice1').val();		  		   		  
		   var price2=$('#txtprice2').val();	
		   				   		  		  
		   if (price != ""){
			   $('#txtprice1').val(numeral(price).format('0,0.00')) ;			  		   		   	  		   			
		   }
		   total(); //masuk ke fungsi total		
    });  	 	
	
	  $(document).on('blur','#txtprice2',function(e){   
		   var tr =  $(this).parent().parent();		  
		   
		   var price=$('#txtprice1').val();		  		   		  
		   var price2=$('#txtprice2').val();	
		 		   		   		  
		   if (price2 != ""){		
			   $('#txtprice2').val(numeral(price2).format('0,0.00')) ;			   		   	  		   			
		   }
		   total(); //masuk ke fungsi total		
    });  	 	


function total()
{
	  
	var hasilprice  = numeral().unformat($('#txtprice1').val());		  		   		  
    var hasilprice2 = numeral().unformat($('#txtprice2').val());			
	var gtstatic = numeral().unformat($('#txtgtotalppn').val()); //textboxt grandtotal	
	var cathgadjust = numeral().unformat($('#txtcathadjust').val()); //textboxt grandtotal adjust		
	    
	//---------------------------------------------
		
		
	   if (hasilprice =="0" && hasilprice2 =="0"){//jika text boxt price satu dan dua kosong
		   $('.hasil').val('');	  
		   $('#txtprice1').val('');
	       $('#txtprice2').val('');	 
	   }else{
			if	(cathgadjust != "0") {				
				 var amount = hasilprice + hasilprice2 ;	
				 var result_t= cathgadjust + amount ;  									
			}else{ 			  
				 var amount = hasilprice + hasilprice2 ;	
				 var result_t=gtstatic + amount ;  				
			}
			$('.hasil').val(numeral(result_t).format('0,0.00')) ;
	   }
	 
							
}

</script>

<script type="text/javascript">

$("#myTable").DataTable({
	ordering: false,	
	autoWidth : false,
	processing: true,
	serverSide: true,
    "scrollY": 250,
    "scrollX": true,
	ajax: {
	  url: "<?php echo base_url('finbpkall_form/c_history_finance/cath_data_fordatatables') ?>",
	  type:'POST',
	}
});

</script>
