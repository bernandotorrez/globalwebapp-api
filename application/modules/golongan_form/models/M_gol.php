<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_gol extends CI_Model
{

	var $table = 'tbl_gol';
	var $column_order = array(null, null,'id_gol','code_gol','name_gol'); //set column field database for datatable orderable
	var $column_search = array('id_gol','code_gol','name_gol'); //set column field database for datatable searchable 
	var $order = array('id_gol' => 'desc'); // default order 
	
    
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	private function _get_datatables_query()
	{
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->where('status = 1');
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$this->db->where('status = 1');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		$this->db->where('status = 1');
		return $this->db->count_all_results();
	}


	public function get_all_gol()
		{
			$this->db->from('tbl_gol');
			$this->db->where('status = 1');
			$query=$this->db->get();
			return $query->result();
		}
	
	public function get_data($start, $length, $order, $dir) 
    {

        if($order !=null) {
            $this->db->order_by($order, $dir);
        }

        return $this->db
            ->limit($length,$start)
            ->get("tbl_gol");
    }
	
	public function get_by_id($id)
		{
			$this->db->from($this->table);
			$this->db->where('id_gol',$id);
			$query = $this->db->get();
			return $query->row();
		}
		
	public function get_count_id()
		{
			$this->db->select('max(id_gol) as totaldata');
			$this->db->from('tbl_gol');
			$query=$this->db->get();
			return $query->result();
		}

	public function add_gol($data)
		{
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}

	public function update_gol($where, $data)
		{
			$this->db->update($this->table, $data, $where);
			return $this->db->affected_rows();
		}

	public function delete_by_id($id)
		{
			$this->db->where('id_gol', $id);
			$this->db->delete($this->table);
		}
	
	public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}

}
