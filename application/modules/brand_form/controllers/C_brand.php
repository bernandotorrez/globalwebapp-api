<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_brand extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_brand');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_brand->get_value('id',$this->session->userdata('id'),'tbl_user');
				$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}	
	 	}
		
	public function index()
		{

		$data['brand']=$this->M_brand->get_all_brand();
		$data['total']=$this->M_brand->get_count_id();
		$data['show_view'] = 'brand_form/V_brand';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		}
	
	public function ajax_list()
	{
		$list = $this->M_brand->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_brand) {
			$chk_idmaster ='<div align="center"><input id="checkbrand" name="checkbrand" type="checkbox" value='.$tbl_brand->id_brand.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_brand->id_brand;
			$row[] = $tbl_brand->brand_name;
			$row[] = $tbl_brand->type_veh;
			$row[] = $tbl_brand->date_create;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_brand->count_all(),
						"recordsFiltered" => $this->M_brand->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_brand');
		$this->db->from('tbl_brand');
		$this->db->where('status','1');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_brand",$search);
		$this->db->where('status','1');
		$this->db->or_like("brand_name",$search);
		$this->db->where('status','1');
		$this->db->or_like("type_veh",$search);
		$this->db->where('status','1');
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		$this->db->where('status','1');
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_brand','ASC');
		$query=$this->db->get('tbl_brand');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("id_brand",$search);
		$jum=$this->db->get('tbl_brand');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $tbl_brand) {
		$chk_idmaster ='<div align="center"><input id="checkvendor" name="checkvendor" type="checkbox" value='.$tbl_brand["id_brand"].' class="editRow ace" req_id_del='.$tbl_brand["id_brand"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($tbl_brand['id_brand'],$tbl_brand['brand_name'],
									$tbl_brand['type_veh'],
									$chk_idmaster
									);

		}

		echo json_encode($output);


	}
	
	public function add_brand()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_add = date('Y-m-d H:i:s');

		if(empty($_POST["brand_name"])){
			die ("Field Name Brand must be filled in!! ");
		}
		elseif(empty($_POST["type_veh"])){
			die ("Field Type must be filled in!! ");
		}
		else{
			$data = array(
					'id_brand' => $this->input->post('id_brand'),
					'brand_name' => $this->input->post('brand_name'),
					'type_veh' => $this->input->post('type_veh'),
					'status' => '1',
					'date_create' => $date_add,
					'remarks' => $remarks_add
				);
			$insert = $this->M_brand->add_brand($data);
		  }
		  echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_brand->get_by_id($id);
			echo json_encode($data);
		}

	public function update_brand()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_update = date('Y-m-d H:i:s');

		if(empty($_POST["brand_name"])){
			die ("Field Name Brand must be filled in!! ");
		}
		elseif(empty($_POST["type_veh"])){
			die ("Field Type must be filled in!! ");
		}
		else{
		$data = array(
					'brand_name' => $this->input->post('brand_name'),
					'type_veh' => $this->input->post('type_veh'),
					'status' => '1',
					'date_update' => $date_update,
					'remarks' => $remarks_update
				);
		$this->M_brand->update_brand(array('id_brand' => $this->input->post('id_brand')), $data);
		  }
		   echo 'Insert';
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_delete = 'Action : Delete | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_delete = date('Y-m-d H:i:s');

		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
						'date_update' => $date_delete,
						'remarks' => $remarks_delete
				);
				$this->db->where('id_brand', $id);
				$this->db->update('tbl_brand', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}



}
