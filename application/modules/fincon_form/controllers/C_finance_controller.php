<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_finance_controller extends MY_Controller
{
  public function __construct()
	{
		parent::__construct();
	    $this->load->model('M_finance_controller','',TRUE);
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('terbilang');
		//$this->load->library('fpdf');
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_finance_controller->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}		
	}		  	
		
  public function index()
  {					    																
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_pp= $this->M_finance_controller->tampil_add_pp()->result();	
	  $total_rows =$this->M_finance_controller->tampil_add_pp()->num_rows();
	  									
	  if ($tampil_table_pp)
		{			
		
		
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;		
		$data['intno'] = ""; //variable buat looping no table.				
		$data['header'] ="Finance Controller"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		$data['ceck_row'] = $total_rows;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.
		$data['tampil_company']=$this->M_finance_controller->get_company()->result();																															
		$data['tampil_dept']=$this->M_finance_controller->get_dept()->result();																
		$data['show_view'] = 'fincon_form/V_finance_controller';		
		$this->load->view('dashboard/Template',$data);				
	  }else{	
	    $dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Finance Controller"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data PP table is empty';		
		$data['tampil_company']=$this->M_finance_controller->get_company()->result();																															
		$data['tampil_dept']=$this->M_finance_controller->get_dept()->result();		
		$data['show_view'] = 'fincon_form/V_finance_controller';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }
   
    public function do_search_data()  // fungsi cari data controler
    {				  			    	
		$tampung_cari = $this->M_finance_controller->search_data(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;	
		   $data['intno'] = ""; //variable buat looping no table.					
		   $data['header'] ="Finance Controller"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				   		
		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['pp_view'] =$tampung_cari;
		   $data['pesan'] = 'Data PP Not a found'; 		   		   
		   $data['show_view'] = 'fincon_form/V_finance_controller';		
		   $this->load->view('dashboard/Template',$data);					
		}else{			
		    $dept  = $this->session->userdata('dept') ;	
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;	
		    $data['intno'] = ""; //variable buat looping no table.				
		    $data['header'] ="Finance Controller"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		    $data['tampil_company']=$this->M_finance_controller->get_company()->result();																															
		    $data['tampil_dept']=$this->M_finance_controller->get_dept()->result();		
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['pp_view'] =$tampung_cari;			   			
			$data['show_view'] ='fincon_form/V_finance_controller';		
		    $this->load->view('dashboard/Template',$data);		
		}	   
    }	
	
	 
    public function do_search_date()  // fungsi cari data controler
    {				  			    	
		$tampung_cari = $this->M_finance_controller->search_date(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;	
		   $data['intno'] = ""; //variable buat looping no table.					
		   $data['header'] ="Finance Controller"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				   		
		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['pp_view'] =$tampung_cari;
		   $data['pesan'] = 'Data PP Not a found'; 		   		   
		   $data['show_view'] = 'fincon_form/V_finance_controller';		
		   $this->load->view('dashboard/Template',$data);					
		}else{			
		    $dept  = $this->session->userdata('dept') ;	
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;					
			$data['intno'] = ""; //variable buat looping no table.
		    $data['header'] ="Finance Controller"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		    $data['tampil_company']=$this->M_finance_controller->get_company()->result();																															
		    $data['tampil_dept']=$this->M_finance_controller->get_dept()->result();		
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['pp_view'] =$tampung_cari;			   			
			$data['show_view'] ='fincon_form/V_finance_controller';		
		    $this->load->view('dashboard/Template',$data);		
		}	   
    }		     		 
	 
	  public function do_give_right_print_fpb()
      {		
		if($this->M_finance_controller->give_right_fpb_print())
		 {								    
		    $strnopp = $this->session->userdata('ses_nopp');
			$this->session->set_flashdata('pesan_give_right','PP No.'.$strnopp." ".'has been allow for reprint FPB!!'); 	
			$this->show_table(); //manggil fungsi show_table		
		  }else{     		
			 $this->session->set_flashdata('pesan_rpt','Give right Update Failed!!!!!! ....'); 					
			 $url=  base_url('c_finance_controller/c_finance_controller');		         
			 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
		  } 		 				 
  	   }
	   
	    public function do_give_right_print_po()
	    {		
			if($this->M_finance_controller->give_right_po_print())
			 {								    
				$strnopp = $this->session->userdata('ses_nopp');
				$this->session->set_flashdata('pesan_give_right','PP No.'.$strnopp." ".'has been allow for re-print INV !!'); 	
				$this->show_table(); //manggil fungsi show_table		
			  }else{     		
				 $this->session->set_flashdata('pesan_rpt','Give right Update Failed!!!!!! ....'); 					
				 $url=  base_url('c_finance_controller/c_finance_controller');		         
				 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
			  } 		 				 
		 }	
		 
		 public function do_give_right_print_inv()
	    {		
			if($this->M_finance_controller->give_right_inv_print())
			 {								    
				$strnopp = $this->session->userdata('ses_nopp');
				$this->session->set_flashdata('pesan_give_right','PP No.'.$strnopp." ".'has been allow for re-print INV !!'); 	
				$this->show_table(); //manggil fungsi show_table		
			  }else{     		
				 $this->session->set_flashdata('pesan_rpt','Give right Update Failed!!!!!! ....'); 					
				 $url=  base_url('c_finance_controller/c_finance_controller');		         
				 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
			  } 		 				 
		 }
	
	
	//------------------------------------pashing result to modal popup-------------------
	public function get_nopp_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //       
	     $query_master = $this->db->query("select id_master,remarks,gran_total,ppn,pph,gran_totalppn,gran_total_adjust,currency from qv_head_pp_complite where id_master ='".$nopp."'");  
         $query_detail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   
		   
		      $data['str_pp_master']=$query_master->result();
			  $data['str_pp_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_nopp'] = $nopp ;
			  $tampil_detail = $this->load->view('v_finance_controller/v_conten_detail',$data,true);
			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end phasing--------------------  
	
	function toExcelAllheader() {	 
		$tampil_to_excel =  $this->M_finance_controller->ToExcelAll();									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
		$data['data_excel'] = $tampil_to_excel ; 		
		$this->load->view('fincon_form/Excel_view_header',$data);
    }
	
	function toExcelAll() {	 
		$tampil_to_excel =  $this->M_finance_controller->ToExcelAll();									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
		$data['data_excel'] = $tampil_to_excel ; 		
		$this->load->view('fincon_form/Excel_view',$data);
    }
	
	function toExcelAllheaderkwee() {	 
		$tampil_to_excel =  $this->M_finance_controller->ToExcelAllkwee();									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
		$data['data_excel'] = $tampil_to_excel ; 		
		$this->load->view('fincon_form/Excel_view_header_kwee',$data);
    }
	
	function toExcelAllkwee() {	 
		$tampil_to_excel =  $this->M_finance_controller->ToExcelAllkwee();									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
		$data['data_excel'] = $tampil_to_excel ; 		
		$this->load->view('fincon_form/Excel_view_kwee',$data);
    }
	
	
		 	 
    public function finance_controller_submit() //fungsi submit dengan banyak button dalam satu form
	{  	
		
		if ($this->input->post('btncari')){	
			$this->do_search_data();
		}else{	
		   if ($this->input->post('btncaridate')){	
			  $this->do_search_date();
		   }else{
				if ($this->input->post('btngiverightinv')){	
				   $this->do_give_right_print_inv();
				}else{
					if ($this->input->post('btngiverightpo')){	
				  		 $this->do_give_right_print_po();
				    }else{
						if ($this->input->post('btnreportexcel')){	
						   $this->toExcelAll();
						}else{	
							if ($this->input->post('btnreportexcel_header')){	
							   $this->toExcelAllheader();
							}else{	
								if ($this->input->post('btnreport_headerkwee')){	
									$this->toExcelAllheaderkwee();
								 }else{
									if ($this->input->post('btnreportkwee')){	
										 $this->toExcelAllkwee();
								   }else{	
									   redirect('fincon_form/c_finance_controller');  //direct ke url add_menu_booking/add_menu_booking
								   }
								 }
							}
						}
					}
				}
		   }
		}				
		
	}
	
	
public function get_idtrans_modal()
{		
     $tampungmaster = $this->M_finance_controller->m_mastergetid_pp();
	 $tampungdetail = $this->M_finance_controller->m_getid_pp();	 
	  
	 $no="1";	 	
	 echo "<div style='overflow-x:auto;'>";
	 echo "<table class='table table-striped table-bordered table-hover '>";
	 echo  "<tr style='font-weight:bold'>";
	 echo  "<td width='2%'>No</td>";
	 echo  "<td width='22%'>Item Type</td>";
	 echo  "<td width='22%'>Description</td>";
	 echo  "<td width='10%'>P.O Ref</td>";
	 echo  "<td width='10%'>C.O.A</td>";
	 echo  "<td width='10%'>No Activity</td>";
	 echo  "<td width='7%'>Qty</td>";
	 echo  "<td width='15%'>Prices</td>";
	 echo  "<td width='40%'>Total Prices</td>";
	 echo  "<td width='7%'>PPN</td>";
	 echo  "<td width='7%'>PPH</td>";
	 echo  "<td width='25%'>PPN Amount</td>";
	 echo  "<td width='25%'>PPH Amount</td>";
	 echo  "</tr> "; 	 	 	 				
		foreach ($tampungdetail as $row_jq) {
		    echo  "<tr> ";
			echo '<td align="center">'.$no++.'</td>';			
			echo '<td>'.$row_jq->desc.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			echo '<td>'.$row_jq->po_reff.'</td>';
			echo '<td>'.$row_jq->coa.'</td>';
			echo '<td>'.$row_jq->no_act.'</td>';
			echo '<td align="center" style="text-align: center;">'.$row_jq->qty.'</td>';
			echo '<td align="right" style="text-align: right;">'. number_format($row_jq->harga,2,'.',',').'</td>';
			echo '<td align="right" style="text-align: right;">'. number_format($row_jq->total,2,'.',',').'</td>';
			echo '<td>'.$row_jq->tax_type.'</td>';
			echo '<td>'.$row_jq->tax_typepph.'</td>';
			echo '<td align="right" style="text-align: right;">'.number_format($row_jq->tax_detail,2,'.',',').'</td>';
			echo '<td align="right" style="text-align: right;">'.number_format($row_jq->tax_detailpph,2,'.',',').'</td>';
		    echo "</tr>"; 							
			
		}
				
	 foreach ($tampungmaster as $row_jm) {	
	     echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='2'>Remarks :</td>";
		 echo  "<td width='40%' colspan='11' align='right' style='text-align: right;'>".$row_jm->remarks."</td>";	
		 echo  "</tr> "; 
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Total (".$row_jm->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>".number_format($row_jm->gran_total,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPN Amount (".$row_jm->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>".number_format($row_jm->ppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 		
		  echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPH Amount (".$row_jm->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>(".number_format($row_jm->pph,2,'.',',').")</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Grand Total (".$row_jm->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>".number_format($row_jm->gran_totalppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "</table>";	
		 echo  "</div'>";
	 }
}	

	
function cath_data_fordatatables(){
	
	$str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
	  $str_idcompany = $this->session->userdata('id_company'); 
	  $str_iddept = $this->session->userdata('id_dept'); 
	  $str_status_send = "1";
	  $str_status_approved = "1";
	 /*Menagkap semua data yang dikirimkan oleh client*/

	 /*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
	 server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
	 sesuai dengan urutan yang sebenarnya */
	 $draw=$_REQUEST['draw'];

	 /*Jumlah baris yang akan ditampilkan pada setiap page*/
	 $length=$_REQUEST['length'];

	 /*Offset yang akan digunakan untuk memberitahu database
	 dari baris mana data yang harus ditampilkan untuk masing masing page
	 */
	 $start=$_REQUEST['start'];

	 /*Keyword yang diketikan oleh user pada field pencarian*/
	 $search=$_REQUEST['search']["value"];

	 $start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];

	 //order short column
	 $column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top");
	 
	 /*Menghitung total desa didalam database*/
	 $this->db->select('id_master');
	 $this->db->from('qv_head_pp_complite');
	 if  ($str_flag_approval == "3" or $str_flag_approval == "0" ){	
			$this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
			  $this->db->like("id_master",$search);
			  $this->db->or_like("user_submission",$search);
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);				
			  $this->db->where("status","1");
			  $this->db->or_like("header_desc",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
			  $this->db->or_like("short",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
			  $this->db->or_like("vendor",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
			 
		 }else{
				 if  ($str_flag_approval == "4"){	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );								
					  $this->db->where("status","1");
					  $this->db->like("id_master",$search);
					  $this->db->or_like("user_submission",$search);
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("header_desc",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("short",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("vendor",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
					 
			  }else{
				  if  ($str_flag_approval == "2"){	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );								
					  $this->db->where("status","1");
					  $this->db->like("id_master",$search);
					  $this->db->or_like("user_submission",$search);
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("header_desc",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("short",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("vendor",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
					 
				  }else{		
						   if  ($str_flag_approval == "5"){	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );
							  $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));								
							  $this->db->where("status","1");
							  $this->db->like("id_master",$search);
							  $this->db->or_like("user_submission",$search);
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );	
							  $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
							  $this->db->where("status","1");
							  $this->db->or_like("header_desc",$search);	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );
							   $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
							  $this->db->where("status","1");
							  $this->db->or_like("short",$search);	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );
							  $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
							  $this->db->where("status","1");
							  $this->db->or_like("vendor",$search);	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );
							  $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
							  $this->db->where("status","1");
							 
						  }else{	
				  
									  if  ($str_flag_approval == "6"){	
										  $this->db->where("status_send_aprove",$str_status_send);
										  $this->db->where("approve_purchasing",$str_status_approved );	
										  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
										  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));							
										  $this->db->where("status","1");
										  $this->db->like("id_master",$search);
										  $this->db->or_like("user_submission",$search);
										  $this->db->where("status_send_aprove",$str_status_send);
										  $this->db->where("approve_purchasing",$str_status_approved );	
										  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
										  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
										  $this->db->where("status","1");
										  $this->db->or_like("header_desc",$search);	
										  $this->db->where("status_send_aprove",$str_status_send);
										  $this->db->where("approve_purchasing",$str_status_approved );	
										  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
										  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
										  $this->db->where("status","1");
										  $this->db->or_like("short",$search);	
										  $this->db->where("status_send_aprove",$str_status_send);
										  $this->db->where("approve_purchasing",$str_status_approved );	
										  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
										  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
										  $this->db->where("status","1");
										  $this->db->or_like("vendor",$search);	
										  $this->db->where("status_send_aprove",$str_status_send);
										  $this->db->where("approve_purchasing",$str_status_approved );	
										  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
										  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
										  $this->db->where("status","1");
										 
									  }else{		
											  if  ($str_flag_approval == "7"){	
												  $this->db->where("status_send_aprove",$str_status_send);
												  $this->db->where("approve_purchasing",$str_status_approved );	
												  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));							
												  $this->db->where("status","1");
												  $this->db->like("id_master",$search);
												  $this->db->or_like("user_submission",$search);
												  $this->db->where("status_send_aprove",$str_status_send);
												  $this->db->where("approve_purchasing",$str_status_approved );	
												  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
												  $this->db->where("status","1");
												  $this->db->or_like("header_desc",$search);	
												  $this->db->where("status_send_aprove",$str_status_send);
												  $this->db->where("approve_purchasing",$str_status_approved );
												  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
												  $this->db->where("status","1");
												  $this->db->or_like("short",$search);	
												  $this->db->where("status_send_aprove",$str_status_send);
												  $this->db->where("approve_purchasing",$str_status_approved );
												  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
												  $this->db->where("status","1");
												  $this->db->or_like("vendor",$search);	
												  $this->db->where("status_send_aprove",$str_status_send);
												  $this->db->where("approve_purchasing",$str_status_approved );
												  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
												  $this->db->where("status","1");
												 
											  }else{	
													  if  ($str_flag_approval == "8"){	
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("approve_purchasing",$str_status_approved );								
														  $this->db->where_in('id_company', array("3","6","11"));	 //ESU EPI
														  $this->db->where("status","1");
														  $this->db->like("id_master",$search);
														  $this->db->or_like("user_submission",$search);
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("approve_purchasing",$str_status_approved );		
														  $this->db->where_in('id_company', array("3","6","11"));	 //ESU EPI
														  $this->db->where("status","1");
														  $this->db->or_like("header_desc",$search);	
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("approve_purchasing",$str_status_approved );		
														  $this->db->where_in('id_company', array("3","6","11"));	 //ESU EPI
														  $this->db->where("status","1");
														  $this->db->or_like("short",$search);	
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("approve_purchasing",$str_status_approved );		
														  $this->db->where_in('id_company', array("3","6","11"));	 //ESU EPI
														  $this->db->where("status","1");
														  $this->db->or_like("vendor",$search);	
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("approve_purchasing",$str_status_approved );		
														  $this->db->where("status","1");
														 
													  }else{		
																 
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("aprove_fc",$str_status_approved );								
														  $this->db->where("status","1");
														  $this->db->like("id_master",$search);
														  $this->db->or_like("user_submission",$search);
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("aprove_fc",$str_status_approved );		
														  $this->db->where("status","1");
														  $this->db->or_like("header_desc",$search);	
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("aprove_fc",$str_status_approved );		
														  $this->db->where("status","1");
														  $this->db->or_like("short",$search);	
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("aprove_fc",$str_status_approved );		
														  $this->db->where("status","1");
														  $this->db->or_like("vendor",$search);	
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("aprove_fc",$str_status_approved );		
														  $this->db->where("status","1");
																										  
													 }
											  }
									  }
								 }
					  }
			  }
		 }
	 $total = $this->db->count_all_results();
			 
	 //$total=$this->db->count_all_results("qv_vendor");
	 //$total=$this->db->count_all_results("qv_head_pp_complite");

	 /*Mempersiapkan array tempat kita akan menampung semua data
	 yang nantinya akan server kirimkan ke client*/
	 $output=array();

	 /*Token yang dikrimkan client, akan dikirim balik ke client*/
	 $output['draw']=$draw;

	 /*
	 $output['recordsTotal'] adalah total data sebelum difilter
	 $output['recordsFiltered'] adalah total data ketika difilter
	 Biasanya kedua duanya bernilai sama, maka kita assignment 
	 keduaduanya dengan nilai dari $total
	 */
	 $output['recordsTotal']=$output['recordsFiltered']=$total;

	 /*disini nantinya akan memuat data yang akan kita tampilkan 
	 pada table client*/
	 $output['data']=array();


	 /*Jika $search mengandung nilai, berarti user sedang telah 
	 memasukan keyword didalam filed pencarian*/
	 if($search!=""){
		 if  ($str_flag_approval == "3" or $str_flag_approval == "0" ){	
			$this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
			  $this->db->like("id_master",$search);
			  $this->db->or_like("user_submission",$search);
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);				
			  $this->db->where("status","1");
			  $this->db->or_like("header_desc",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
			  $this->db->or_like("short",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
			  $this->db->or_like("vendor",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
		 }else{
				 if  ($str_flag_approval == "4"){	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );								
					  $this->db->where("status","1");
					  $this->db->like("id_master",$search);
					  $this->db->or_like("user_submission",$search);
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("header_desc",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("short",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("vendor",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
			  }else{
				  if  ($str_flag_approval == "2"){	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );								
					  $this->db->where("status","1");
					  $this->db->like("id_master",$search);
					  $this->db->or_like("user_submission",$search);
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("header_desc",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("short",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("vendor",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
				  }else{	
						 if  ($str_flag_approval == "5"){	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );		
							  $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));						
							  $this->db->where("status","1");
							  $this->db->like("id_master",$search);
							  $this->db->or_like("user_submission",$search);
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );	
							  $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
							  $this->db->where("status","1");
							  $this->db->or_like("header_desc",$search);	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );	
							   $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
							  $this->db->where("status","1");
							  $this->db->or_like("short",$search);	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );	
							   $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
							  $this->db->where("status","1");
							  $this->db->or_like("vendor",$search);	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );		
							  $this->db->where("status","1");
							 
						  }else{	
				  
									  if  ($str_flag_approval == "6"){	
										  $this->db->where("status_send_aprove",$str_status_send);
										  $this->db->where("approve_purchasing",$str_status_approved );	
										   $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
										  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));								
										  $this->db->where("status","1");
										  $this->db->like("id_master",$search);
										  $this->db->or_like("user_submission",$search);
										  $this->db->where("status_send_aprove",$str_status_send);
										  $this->db->where("approve_purchasing",$str_status_approved );	
										  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
										  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
										  $this->db->where("status","1");
										  $this->db->or_like("header_desc",$search);	
										  $this->db->where("status_send_aprove",$str_status_send);
										  $this->db->where("approve_purchasing",$str_status_approved );	
										   $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
										  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
										  $this->db->where("status","1");
										  $this->db->or_like("short",$search);	
										  $this->db->where("status_send_aprove",$str_status_send);
										  $this->db->where("approve_purchasing",$str_status_approved );	
										  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
										  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
										  $this->db->where("status","1");
										  $this->db->or_like("vendor",$search);	
										  $this->db->where("status_send_aprove",$str_status_send);
										  $this->db->where("approve_purchasing",$str_status_approved );
										  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
										  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));			
										  $this->db->where("status","1");
										 
									  }else{		
											  if  ($str_flag_approval == "7"){	
												  $this->db->where("status_send_aprove",$str_status_send);
												  $this->db->where("approve_purchasing",$str_status_approved );	
												  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));							
												  $this->db->where("status","1");
												  $this->db->like("id_master",$search);
												  $this->db->or_like("user_submission",$search);
												  $this->db->where("status_send_aprove",$str_status_send);
												  $this->db->where("approve_purchasing",$str_status_approved );	
												  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
												  $this->db->where("status","1");
												  $this->db->or_like("header_desc",$search);	
												  $this->db->where("status_send_aprove",$str_status_send);
												  $this->db->where("approve_purchasing",$str_status_approved );
												  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
												  $this->db->where("status","1");
												  $this->db->or_like("short",$search);	
												  $this->db->where("status_send_aprove",$str_status_send);
												  $this->db->where("approve_purchasing",$str_status_approved );
												  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
												  $this->db->where("status","1");
												  $this->db->or_like("vendor",$search);	
												  $this->db->where("status_send_aprove",$str_status_send);
												  $this->db->where("approve_purchasing",$str_status_approved );	
												  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
												  $this->db->where("status","1");
												 
											  }else{		
												   if  ($str_flag_approval == "8"){	
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("approve_purchasing",$str_status_approved );
													  $this->db->where_in('id_company', array("3","6","11"));								
													  $this->db->where("status","1");
													  $this->db->like("id_master",$search);
													  $this->db->or_like("user_submission",$search);
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("approve_purchasing",$str_status_approved );
													  $this->db->where_in('id_company', array("3","6","11"));		
													  $this->db->where("status","1");
													  $this->db->or_like("header_desc",$search);	
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("approve_purchasing",$str_status_approved );
													  $this->db->where_in('id_company', array("3","6","11"));		
													  $this->db->where("status","1");
													  $this->db->or_like("short",$search);	
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("approve_purchasing",$str_status_approved );
													  $this->db->where_in('id_company', array("3","6","11"));		
													  $this->db->where("status","1");
													  $this->db->or_like("vendor",$search);	
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("approve_purchasing",$str_status_approved );
													  $this->db->where_in('id_company', array("3","6","11"));		
													  $this->db->where("status","1");
													 
												  }else{	
																 
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("aprove_fc",$str_status_approved );								
														  $this->db->where("status","1");
														  $this->db->like("id_master",$search);
														  $this->db->or_like("user_submission",$search);
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("aprove_fc",$str_status_approved );		
														  $this->db->where("status","1");
														  $this->db->or_like("header_desc",$search);	
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("aprove_fc",$str_status_approved );		
														  $this->db->where("status","1");
														  $this->db->or_like("short",$search);	
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("aprove_fc",$str_status_approved );		
														  $this->db->where("status","1");
														  $this->db->or_like("vendor",$search);	
														  $this->db->where("status_send_aprove",$str_status_send);
														  $this->db->where("aprove_fc",$str_status_approved );		
														  $this->db->where("status","1");
																										  
												  }
											  }
									   }
								 }
						}
			  }
		 }
	 }

	 if($_POST["is_date_search"] == "yes") { // hitung total record filter range date
		$this->db->where('date_send_aproval >=', date('Y-m-d',strtotime($start_date)));
		$this->db->where('date_send_aproval <=',date('Y-m-d', strtotime($end_date)));
	  
  	}

	 /*Lanjutkan pencarian ke database*/
	 $this->db->limit($length,$start);
	 
	  if  ($str_flag_approval == "3" or $str_flag_approval == "0"){						
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  $this->db->where("id_dept",$str_iddept);			 
	  }else{
		   if  ($str_flag_approval == "4"){
				$this->db->where("status_send_aprove",$str_status_send);	
				$this->db->where("aprove_head",$str_status_approved );	
				$this->db->where("approve_purchasing","0");	  
		   }else{
			   if ($str_flag_approval == "2"){
					$this->db->where("status_send_aprove",$str_status_send);						  
					$this->db->where("approve_purchasing",$str_status_approved);	
					$this->db->where("aprove_fc","0");	
			   }else{		
					   if ($str_flag_approval == "5"){
						 $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
						 $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));
						$this->db->where("status_send_aprove",$str_status_send);						  
						$this->db->where("approve_purchasing",$str_status_approved);	
						$this->db->where("aprove_fc","0");
					}else{					
						 if ($str_flag_approval == "6"){
							 $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
							 $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
							$this->db->where("status_send_aprove",$str_status_send);						  
							$this->db->where("approve_purchasing",$str_status_approved);	
							$this->db->where("aprove_fc","0");
						}else{
							if ($str_flag_approval == "7"){
								$this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));
								$this->db->where("status_send_aprove",$str_status_send);						  
								$this->db->where("approve_purchasing",$str_status_approved);	
								$this->db->where("aprove_fc","0");
							}else{
								if ($str_flag_approval == "8"){
									$this->db->where_in('id_company', array("3","6","11"));
									 $this->db->where("status_send_aprove",$str_status_send);						  
									  $this->db->where("approve_purchasing",$str_status_approved);	
									 $this->db->where("aprove_fc",$str_status_approved);
								   }else{									    
										$this->db->where("status_send_aprove",$str_status_send);						
										$this->db->where("aprove_fc",$str_status_approved );
									 $this->db->where("aprove_bod","0");
								}
							}
						}
					 }
				  }
		   }
	  }
 
	  /*Urutkan dari alphabet paling terkahir*/
	  $this->db->where("status","1");
	 // $this->db->order_by('date_pp','DESC');
	  //order column
	 
	  if (isset($_REQUEST["order"])){
		   $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
	  }else{
		   $this->db->order_by('aprove_head','desc');
		   $this->db->order_by('approve_purchasing','desc');
		   $this->db->order_by('aprove_fc','desc');
		   $this->db->order_by('aprove_bod','desc');
	   }
	   
	   $this->db->order_by('aprove_head','asc');
	   $this->db->order_by('approve_purchasing','asc');
	   $this->db->order_by('aprove_fc','asc');
	   $this->db->order_by('aprove_bod','asc');	
	   $this->db->order_by('date_send_aproval','DESC');  
	   $query=$this->db->get('qv_head_pp_complite');


	 /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
	 dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
	 yang mengandung keyword tertentu
	 */
	 
	 if($search!=""){
		  if  ($str_flag_approval == "3" or $str_flag_approval == "0" ){	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
			  $this->db->like("id_master",$search);
			  $this->db->or_like("user_submission",$search);
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);				
			  $this->db->where("status","1");
			  $this->db->or_like("header_desc",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
			  $this->db->or_like("short",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
			  $this->db->or_like("vendor",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("status","1");
			 
		 }else{
				 if  ($str_flag_approval == "4"){	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );								
					  $this->db->where("status","1");
					  $this->db->like("id_master",$search);
					  $this->db->or_like("user_submission",$search);
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("header_desc",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("short",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("vendor",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("aprove_head",$str_status_approved );		
					  $this->db->where("status","1");
					 
			  }else{
				  if  ($str_flag_approval == "2"){	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );								
					  $this->db->where("status","1");
					  $this->db->like("id_master",$search);
					  $this->db->or_like("user_submission",$search);
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
					   $this->db->or_like("header_desc",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("short",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
					  $this->db->or_like("vendor",$search);	
					  $this->db->where("status_send_aprove",$str_status_send);
					  $this->db->where("approve_purchasing",$str_status_approved );		
					  $this->db->where("status","1");
					 
				  }else{		
						   if  ($str_flag_approval == "5"){	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );	
							   $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));										
							  $this->db->where("status","1");
							  $this->db->like("id_master",$search);
							  $this->db->or_like("user_submission",$search);
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );	
							  $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));				
							  $this->db->where("status","1");
							  $this->db->or_like("header_desc",$search);	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );	
							  $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));				
							  $this->db->where("status","1");
							  $this->db->or_like("short",$search);	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );	
							   $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));				
							  $this->db->where("status","1");
							  $this->db->or_like("vendor",$search);	
							  $this->db->where("status_send_aprove",$str_status_send);
							  $this->db->where("approve_purchasing",$str_status_approved );	
							  $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
							  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));				
							  $this->db->where("status","1");
						  }else{							
								   if  ($str_flag_approval == "6"){	
									  $this->db->where("status_send_aprove",$str_status_send);
									  $this->db->where("approve_purchasing",$str_status_approved );	
									  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
									  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));									
									  $this->db->where("status","1");
									  $this->db->like("id_master",$search);
									  $this->db->or_like("user_submission",$search);
									  $this->db->where("status_send_aprove",$str_status_send);
									  $this->db->where("approve_purchasing",$str_status_approved );	
									  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
									  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));			
									  $this->db->where("status","1");
									  $this->db->or_like("header_desc",$search);	
									  $this->db->where("status_send_aprove",$str_status_send);
									  $this->db->where("approve_purchasing",$str_status_approved );	
									  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
									  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));			
									  $this->db->where("status","1");
									  $this->db->or_like("short",$search);	
									  $this->db->where("status_send_aprove",$str_status_send);
									  $this->db->where("approve_purchasing",$str_status_approved );	
									  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
									  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));			
									  $this->db->where("status","1");
									  $this->db->or_like("vendor",$search);	
									  $this->db->where("status_send_aprove",$str_status_send);
									  $this->db->where("approve_purchasing",$str_status_approved );	
									  $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
									  $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));			
									  $this->db->where("status","1");
								  }else{		
										   if  ($str_flag_approval == "7"){	
											  $this->db->where("status_send_aprove",$str_status_send);
											  $this->db->where("approve_purchasing",$str_status_approved );	
											  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));										
											  $this->db->where("status","1");
											  $this->db->like("id_master",$search);
											  $this->db->or_like("user_submission",$search);
											  $this->db->where("status_send_aprove",$str_status_send);
											  $this->db->where("approve_purchasing",$str_status_approved );		
											  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));			
											  $this->db->where("status","1");
											  $this->db->or_like("header_desc",$search);	
											  $this->db->where("status_send_aprove",$str_status_send);
											  $this->db->where("approve_purchasing",$str_status_approved );		
											  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));			
											  $this->db->where("status","1");
											  $this->db->or_like("short",$search);	
											  $this->db->where("status_send_aprove",$str_status_send);
											  $this->db->where("approve_purchasing",$str_status_approved );		
											  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));			
											  $this->db->where("status","1");
											  $this->db->or_like("vendor",$search);	
											  $this->db->where("status_send_aprove",$str_status_send);
											  $this->db->where("approve_purchasing",$str_status_approved );		
											  $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));			
											  $this->db->where("status","1");
										  }else{	
												   if  ($str_flag_approval == "8"){	
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("approve_purchasing",$str_status_approved );	
													  $this->db->where_in('id_company', array("3","6","11"));							
													  $this->db->where("status","1");
													  $this->db->like("id_master",$search);
													  $this->db->or_like("user_submission",$search);
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("approve_purchasing",$str_status_approved );		
													  $this->db->where_in('id_company', array("3","6","11"));
													  $this->db->where("status","1");
													  $this->db->or_like("header_desc",$search);	
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("approve_purchasing",$str_status_approved );		
													  $this->db->where_in('id_company', array("3","6","11"));
													  $this->db->where("status","1");
													  $this->db->or_like("short",$search);	
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("approve_purchasing",$str_status_approved );		
													  $this->db->where_in('id_company', array("3","6","11"));
													  $this->db->where("status","1");
													  $this->db->or_like("vendor",$search);	
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("approve_purchasing",$str_status_approved );		
													  $this->db->where_in('id_company', array("3","6","11"));
													  $this->db->where("status","1");
												  }else{		
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("aprove_fc",$str_status_approved );								
													  $this->db->where("status","1");
													  $this->db->like("id_master",$search);
													  $this->db->or_like("user_submission",$search);
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("aprove_fc",$str_status_approved );		
													  $this->db->where("status","1");
													  $this->db->or_like("header_desc",$search);	
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("aprove_fc",$str_status_approved );		
													  $this->db->where("status","1");
													  $this->db->or_like("short",$search);	
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("aprove_fc",$str_status_approved );		
													  $this->db->where("status","1");
													  $this->db->or_like("vendor",$search);	
													  $this->db->where("status_send_aprove",$str_status_send);
													  $this->db->where("aprove_fc",$str_status_approved );		
													  $this->db->where("status","1");
																									  
												 }
										  }
								  }
						  }
				  }
			  }
		 }

		 if($_POST["is_date_search"] == "yes") { // hitung total record filter range date
			$this->db->where('date_send_aproval >=', date('Y-m-d',strtotime($start_date)));
			$this->db->where('date_send_aproval <=',date('Y-m-d', strtotime($end_date)));
		  
		  }
		 
		 $jum=$this->db->get('qv_head_pp_complite');
		 $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
	 }




	 
	 foreach ($query->result_array() as $row_tbl) {
		 
		  if ($row_tbl['status_send_aprove'] == "-1")	
		  {
				$info_aproval = "Reject";
		  }else{				
				$info_aproval = "New Crated";				
		  }
		  
		  If ($row_tbl['attach_quo'] != "")
		   {  
			$attach_quo = '<div  align="center"><a onClick="PDFPopup(this)" req_id='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
		  }else{
			 $attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
		  }
	 
		
		
		  $btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i></a></center>';//button detail
		
		 If ($row_tbl['aprove_head'] == "1")
		  {
			 $head_approval = "<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";
			 
		  }else{
			   If ($row_tbl['status_send_aprove'] == "-1")
			   {
				  $head_approval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
			   }else{
				  $head_approval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
			   }
		  }
		  
		   If ($row_tbl['approve_purchasing'] == "1")
		   {
				 //$purchase_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 $purchase_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
			  }else{
				   If ($row_tbl['status_send_aprove'] == "-1")
				   {
					 $purchase_approval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
				   }else{
					 $purchase_approval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
				   }
			}
			
			If ($row_tbl['aprove_fc'] == "1")
		   {
				 //$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 $fc_aproval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
			  }else{
				   If ($row_tbl['status_send_aprove'] == "-1")
				   {
					 $fc_aproval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
				   }else{
					 $fc_aproval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
				   }
			}
			
			 
			
			   If ($row_tbl['aprove_bod'] == "1")
			  {
					 //$bod_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
					  $bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
				  }else{
					   If ($row_tbl['status_send_aprove'] == "-1")
					   {
						 //$bod_approval =  '<img src="'.base_url("asset/images/reject.png").'">' ;
							 $bod_approval = "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
					   }else{
						 $bod_approval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
					   }
			   }
			  
		 
$strbodaprove  = $row_tbl["aprove_bod"] ;
$strpurchaseaprove = $row_tbl["approve_purchasing"] ;
$strfcaprove   = $row_tbl["aprove_fc"] ;
$strheadaprove = $row_tbl["aprove_head"] ;	


if ($this->session->userdata("aproval_flag")== "3" and $row_tbl["aprove_head"]=="1" ) {			
   $str_reject ='<div align="center" style="color:#2292d8"> Has been aproved head</div>' ;	
   $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";		
}else{
if ($this->session->userdata("aproval_flag")== "4" and $row_tbl["approve_purchasing"]=="1" ) {	
	$str_reject =  '<div align="center" style="color:#2292d8>Has been Aproved Purchasing</div>' ;
	 $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";				
}else{
  if ($this->session->userdata("aproval_flag")== "2" and $row_tbl["aprove_fc"]=="1" ) {	
	   $str_reject =  '<div align="center" style="color:#2292d8>Has been aproved fc</div>' ;	
		$chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";			
   }else{	
	   if ($this->session->userdata("aproval_flag")== "5" and $row_tbl["aprove_fc"]=="1" ) {	
		   $str_reject =  '<div align="center" style="color:#2292d8>Has been aproved fc</div>' ;		
			$chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";		
	  }else{	
		  if ($this->session->userdata("aproval_flag")== "6" and $row_tbl["aprove_fc"]=="1" ) {	
			  $str_reject =  '<div align="center" style="color:#2292d8>Has been aproved fc</div>' ;
			   $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";				
		  }else{	
			   if ($this->session->userdata("aproval_flag")== "7" and $row_tbl["aprove_fc"]=="1" ) {	
				   $str_reject =  '<div align="center" style="color:#2292d8>Has been aproved fc</div>' ;
					$chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";	
			  }else{	
				 if ($this->session->userdata("aproval_flag") == "1" and $row_tbl["aprove_bod"] ) {
					  $str_reject =  '<div align="center" style="color:#2292d8>Has been aproved all</div>' ;
					   $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";	
				 }else{ 	 
					   $str_reject =  '<div align="center"><a href="#" class="btn btn-app btn-danger btn-xs radius-8 reject " id="reject" style="text-decoration:none" req_id='.$row_tbl["id_master"].'><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></a></div>'; //button reject 	
					   
				  //checkbox aktiff belum teraproval.
				  $chk_idmaster ='<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value='.$row_tbl["id_master"].' class="ace editRow" req_id_del='.$row_tbl["id_master"].' />
		<span class="lbl"></span> ';				
				 }
			  }
		  }
	   }
	}
  }
}

//Pembedaan Warna pada type purchase.
		if ($row_tbl['flag_purchase'] == "1") {
			$type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
		}else{
			  if ($row_tbl['flag_purchase'] == "2") {
					$type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
				}else{
					  if ($row_tbl['flag_purchase'] == "3") {
						$type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
					}else{
						  if ($row_tbl['flag_purchase'] == "4") {
								$type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
							}else{
								 $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
						  };
				  };
			  };   
		  };
//end------------------------------			 

//date--------------
if ($row_tbl['date_aprove_head'] ==""){
$datehead = "" ;
}else{
$datehead  = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_head']))."</div>";
}

if ($row_tbl['date_approve_purchasing'] ==""){
$datepur = "" ;
}else{
$datepur = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_approve_purchasing']))."</div>";
}

if ($row_tbl['date_aprove_fc'] ==""){
 $datefc = "" ;
}else{
 $datefc= "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_fc']))."</div>";
}

if ($row_tbl['date_aprove_bod'] ==""){
$datebod = "" ;
}else{
$datebod = "<div style='color:#EB293D'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_bod']))."</div>";
}

//end date-----------------

if($row_tbl['term_top'] > 1) {
	$day = 'Days';
} else {
	$day = 'Day';
}

//label colum 1 idepp
$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP
//end label

$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";


	  
		 $output['data'][]=array(
								 $chk_idmaster,
								 //$row_tbl['id_master'],
								 $idmas,
								 $btn_view_detail,
								 $attach_quo,															
								 $str_reject	,
								 $row_tbl['short'],
								 $row_tbl['user_submission'],
								 $row_tbl['dept'],
								 $header_desc ,//$row_tbl['header_desc'],
								 $row_tbl['vendor'],																								
								 date('d-m-Y', strtotime($row_tbl['date_pp'])),	
								 $row_tbl['currency'],
								 $type_purchase, //$row_tbl['type_purchase'],
								 //$row_tbl['nomor_coa'],
								 number_format($row_tbl['gran_total'],2,'.',','),
								 //number_format($row_tbl['ppn'],2,'.',','),
								 number_format($row_tbl['gran_totalppn'],2,'.',','),
								 $row_tbl['term_top'].' '.$day,
								 $head_approval.$datehead,
								 $purchase_approval.$datepur,
								 $fc_aproval.$datefc,
								 $bod_approval.$datebod,	
																 
						   );			
	 }
	 echo json_encode($output);
 }	
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}

	public function getListGRN() {
		$id_master = $this->input->post('id_master');
		$getdatadetailgrn = $this->M_finance_controller->m_getgrnpo($id_master);
		$no_po = $this->M_finance_controller->getNoPO($id_master)[0];
		$access = $this->session->userdata('apps_accsess');
		$no="1";	 	
		echo "<div style='overflow-x:auto;'>" ;
		echo "<table class='table table-striped table-bordered table-hover'>";
		echo  "<tr style='font-weight:bold; font-size:11px'>";
		echo  "<td>No</td>";
		echo  "<td >PR No</td>";
		echo  "<td >PO No</td>";
		echo  "<td >GRN No</td>";
		echo  "<td>Date GRN</td>";
		echo  "<td >Grand Total</td>";
		//echo  "<td >Print</td>";

		// if($access == 1 || $access == 2) {
		// 	echo  "<td >Give Right</td>";
		// }

		echo  "<td >Give Right</td>";
		
		echo  "</tr> "; 	 	 	 				
		 foreach ($getdatadetailgrn as $row_det_grn) {
			 $no_pp = base64_encode($row_det_grn->id_master);
			 $number_grn = base64_encode($row_det_grn->number_grn);
			//$url = base_url('fpbpo_form/C_print_fpb_po/print_fpb?pp='.$no_pp.'&grn='.$number_grn);
			// if($row_det_grn->flag_print_fpb == '0') {
			// 	$button_print = '<a target="_blank" OnClick=\'return confirm("Print this Data?");\' href="'.$url.'" class="btn btn-info">
			// 	<span class="fa fa-print"></span> Print
			// 	</a>';
			// } else {
			// 	$button_print = '<button type="button" class="btn btn-info" disabled>
			// 	<span class="fa fa-print"></span> Print
			// 	</button>';
			// }

			 echo  "<tr style='font-size:12px'> ";
			 echo '<td>'.$no++.'</td>';	
			 echo '<td>'.$no_po->id_master.'</td>';
			 echo '<td>'.$no_po->po_no_created.'</td>';
			 echo '<td>'.$row_det_grn->number_grn.'</td>';
			 echo '<td>'.number_format($row_det_grn->gran_total,2,'.',',').'</td>';
			 echo '<td>
			 		<button type="button" class="btn btn-success" onClick="tampilDetailGRN(this)" req_id='.$row_det_grn->number_grn.'>
					<span class="fa fa-search"></span> Detail
					</button>

					
					</a>
				  </td>';

				// if($access == 1 || $access == 2) {
				// 	echo '<td><button type="button" class="btn btn-success" onClick="giveRightFPB(this)" req_id='.$row_det_grn->number_grn.'>Give Right</button></td>';
				// }

				echo '<td><button type="button" class="btn btn-success" onClick="giveRightFPB(this)" req_id='.$row_det_grn->number_grn.'>Give Right</button></td>';
				
			
			 echo "</tr>"; 							
			 
		 }

		 

		 echo '</div>';
	}

	public function getDetailGRN() {
		$number_grn = $this->input->get('number_grn');
     $master_grn = $this->M_finance_controller->getMasterGRN($number_grn);
	 $detail_grn = $this->M_finance_controller->getDetailGRN($number_grn);	 
	  
	 $no="1";	 	
	 echo  "<div style='overflow-x:auto'>";
	 echo  "<table class ='table table-striped table-bordered table-hover'>";
	 echo  "<thead>" ;
	 echo  "<tr style='font-weight:bold; font-size:11px'>";
	 echo  "<td width='2%'>No</td>";
	 echo  "<td width='22%'>Item Type</td>";
	 echo  "<td width='22%'>Description</td>";
	 echo  "<td width='7%'>Qty</td>";
	 echo  "<td width='15%'>Prices</td>";
	 echo  "<td width='25%'>Total Prices</td>";
	 echo  "<td width='7%'>PPN</td>";
	 echo  "<td width='7%'>PPH</td>";
	 echo  "<td width='25%'>PPN Amount</td>";
	 echo  "<td width='25%'>PPH Amount</td>";
	 echo  "<td width='25%'>Total + Tax</td>";
	 echo  "</tr> "; 
	 echo  "</thead>";	 	 	 				
		foreach ($detail_grn as $row_jq) {
			$total_plus_tax = $row_jq->amount_in + ($row_jq->tax_ppn - $row_jq->tax_pph);
			

		    echo  "<tr style='font-size:12px'> ";
			echo '<td>'.$no++.'</td>';			
			echo '<td>'.$row_jq->description.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			echo '<td>'.$row_jq->qty_in.'</td>';
			echo '<td>'.number_format($row_jq->price,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->amount_in,2,'.',',').'</td>';
			echo '<td>'.$row_jq->type_ppn.'</td>';
			echo '<td>'.$row_jq->type_pph.'</td>';
			echo '<td>'.number_format($row_jq->tax_ppn,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->tax_pph,2,'.',',').'</td>';
			echo '<td>'.number_format($total_plus_tax,2,'.',',').'</td>';
		    echo "</tr>"; 							
			
		}
				
	 foreach ($master_grn as $row_jm) {	
		
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>Sub Total :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_amount,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>PPN Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_ppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 	
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>PPH Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_pph,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>Total + Tax :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_total,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 
	 }

	 echo "</table>";	
		 echo "</div>";
	}

	public function giveRightFPB() {
		$number_grn = $this->input->get('number_grn');

		$data = array('flag_print_fpb' => '0'
					);

		$update = $this->M_finance_controller->giveRightFPB($number_grn, $data);
		
		if($update) {
			echo json_encode('Success');
		} else {
			echo json_encode('Failed');
		}
	}

	public function getListBPK() {
		$id_master = $this->input->post('id_master');
		$getdatadetailgrn = $this->M_finance_controller->m_getgrnpo($id_master);
		$no_po = $this->M_finance_controller->getNoPO($id_master)[0];
		$access = $this->session->userdata('apps_accsess');
		$no="1";	 	
		echo "<div style='overflow-x:auto;'>" ;
		echo "<table class='table table-striped table-bordered table-hover'>";
		echo  "<tr style='font-weight:bold; font-size:11px'>";
		echo  "<td>No</td>";
		echo  "<td >PR No</td>";
		echo  "<td >PO No</td>";
		echo  "<td >GRN No</td>";
		echo  "<td>Date GRN</td>";
		echo  "<td >Grand Total</td>";
		echo  "<td >Detail</td>";
		//echo  "<td >Print</td>";

		// if($access == 1 || $access == 2) {
		// 	echo  "<td >Give Right</td>";
		// }

		echo  "<td >Give Right</td>";
		
		echo  "</tr> "; 	 	 	 				
		 foreach ($getdatadetailgrn as $row_det_grn) {
			 $no_pp = base64_encode($row_det_grn->id_master);
			 $number_grn = base64_encode($row_det_grn->number_grn);
			//$url = base_url('fpbpo_form/C_print_fpb_po/print_fpb?pp='.$no_pp.'&grn='.$number_grn);
			// if($row_det_grn->flag_print_fpb == '0') {
			// 	$button_print = '<a target="_blank" OnClick=\'return confirm("Print this Data?");\' href="'.$url.'" class="btn btn-info">
			// 	<span class="fa fa-print"></span> Print
			// 	</a>';
			// } else {
			// 	$button_print = '<button type="button" class="btn btn-info" disabled>
			// 	<span class="fa fa-print"></span> Print
			// 	</button>';
			// }

			 echo  "<tr style='font-size:12px'> ";
			 echo '<td>'.$no++.'</td>';	
			 echo '<td>'.$no_po->id_master.'</td>';
			 echo '<td>'.$no_po->po_no_created.'</td>';
			 echo '<td>'.$row_det_grn->number_grn.'</td>';		
			 echo '<td>'.date('d-m-Y H:i:s', strtotime($row_det_grn->date_grn)).'</td>';
			 echo '<td>'.number_format($row_det_grn->gran_total,2,'.',',').'</td>';
			 echo '<td>
			 		<button type="button" class="btn btn-success" onClick="tampilDetailBPK(this)" req_id='.$row_det_grn->number_grn.'>
					<span class="fa fa-search"></span> Detail
					</button>

					
					</a>
				  </td>';

				// if($access == 1 || $access == 2) {
				// 	echo '<td><button type="button" class="btn btn-success" onClick="giveRightFPB(this)" req_id='.$row_det_grn->number_grn.'>Give Right</button></td>';
				// }

				echo '<td><button type="button" class="btn btn-success" onClick="giveRightBPK(this)" req_id='.$row_det_grn->number_grn.'>Give Right</button></td>';
				
			
			 echo "</tr>"; 							
			 
		 }

		 

		 echo '</div>';
	}
	
	public function giveRightBPK() {
		$number_grn = $this->input->get('number_grn');

		$data = array('flag_print_bpk' => '0'
					);

		$update = $this->M_finance_controller->giveRightFPB($number_grn, $data);
		
		if($update) {
			echo json_encode('Success');
		} else {
			echo json_encode('Failed');
		}
	}

	public function getDetailBPK() {
		$number_grn = $this->input->get('number_grn');
     $master_grn = $this->M_finance_controller->getMasterGRN($number_grn);
	 $detail_grn = $this->M_finance_controller->getDetailGRN($number_grn);	 
	  
	 $no="1";	 	
	 echo  "<div style='overflow-x:auto'>";
	 echo  "<table class ='table table-striped table-bordered table-hover'>";
	 echo  "<thead>" ;
	 echo  "<tr style='font-weight:bold; font-size:11px'>";
	 echo  "<td width='2%'>No</td>";
	 echo  "<td width='22%'>Item Type</td>";
	 echo  "<td width='22%'>Description</td>";
	 echo  "<td width='7%'>Qty</td>";
	 echo  "<td width='15%'>Prices</td>";
	 echo  "<td width='25%'>Total Prices</td>";
	 echo  "<td width='7%'>PPN</td>";
	 echo  "<td width='7%'>PPH</td>";
	 echo  "<td width='25%'>PPN Amount</td>";
	 echo  "<td width='25%'>PPH Amount</td>";
	 echo  "<td width='25%'>Total + Tax</td>";
	 echo  "</tr> "; 
	 echo  "</thead>";	 	 	 				
		foreach ($detail_grn as $row_jq) {
			$total_plus_tax = $row_jq->amount_in + ($row_jq->tax_ppn - $row_jq->tax_pph);
			

		    echo  "<tr style='font-size:12px'> ";
			echo '<td>'.$no++.'</td>';			
			echo '<td>'.$row_jq->description.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			echo '<td>'.$row_jq->qty_in.'</td>';
			echo '<td>'.number_format($row_jq->price,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->amount_in,2,'.',',').'</td>';
			echo '<td>'.$row_jq->type_ppn.'</td>';
			echo '<td>'.$row_jq->type_pph.'</td>';
			echo '<td>'.number_format($row_jq->tax_ppn,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->tax_pph,2,'.',',').'</td>';
			echo '<td>'.number_format($total_plus_tax,2,'.',',').'</td>';
		    echo "</tr>"; 							
			
		}
				
	 foreach ($master_grn as $row_jm) {	
		
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>Sub Total :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_amount,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>PPN Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_ppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 	
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>PPH Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_pph,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>Total + Tax :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_total,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 
	 }

	 echo "</table>";	
		 echo "</div>";
	}
}

