

<style>
.modal-dialog{
  width:1024px
}
</style>
<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);
$this->db->update('tbl_user',$data1);
}
?>

<?php
	If ( $this->session->flashdata('pesan_give_right') != ""){ ?>
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>
            <?php  echo $this->session->flashdata('pesan_give_right');	?>
        </div>
<?php } ?>

<?php
	If ( $this->session->flashdata('pesan') != ""){ ?>
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>
            <?php  echo $this->session->flashdata('pesan');	?>
        </div>
<?php } ?>

<?php
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>
                <?php  echo "Send Approval Successfully"	?>
		  </div>
<?php } ?>

<?php
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>
			<?php  echo "Send Approval failed"	?>
        </div>
<?php } ?>

<?php  
 echo form_open('fincon_form/c_finance_controller/finance_controller_submit');  
?>

<table>
     <tr>
      <td>
        
      <button class="btn btn-app btn-danger btn-xs radius-4 btnpo btn-clean btn-grn" type="button" 
        id="btngiverightfpb" name="btngiverightfpb"  value = "btngiverightfpb" disabled="disabled" onClick="getListGRN()">
            <i class="ace-icon fa fa-exchange bigger-180"></i>
            Re-P FPB 
		</button> 
        
      </td>

      <td colspan="3">
        
      <button class="btn btn-app btn-info btn-xs radius-4 btnpo btn-clean btn-bpk" type="button" 
        id="btngiverightbpk" name="btngiverightbpk"  value = "btngiverightbpk" disabled="disabled" onClick="getListBPK()">
            <i class="ace-icon fa fa-exchange bigger-180"></i>
            Re-P BPK 
		</button> 
        
      </td>

       <td colspan="3">
       		<button class="btn btn-app btn-success btn-xs radius-4 btnpo btn-clean" type="submit" id="btngiverightpo" name="btngiverightpo"  value ="btngiverightpo"  disabled="disabled" >
            <i class="ace-icon fa fa-exchange bigger-180"></i>
            Re-P P.O 
		</button> 
       </td> 
       <td colspan="3">   
         
       <button class="btn btn-app btn-warning btn-xs radius-4 btnpo btn-clean" type="submit" id="btngiverightinv" name="btngiverightinv"  value = "btngiverightfpb"  disabled="disabled" >
            <i class="ace-icon fa fa-exchange bigger-180"></i>
            Re-P INV 
		</button> 
         
         <button class="btn btn-app btn-primary btn-xs radius-4 report btn-clean" type="button"  id="report" name="report"  value ="report" >
            <i class="ace-icon fa fa-check-square-o bigger-180"></i>
            Report
		</button> 
         	           
         </td>         
      </tr> 
</table>
</br>

<div class="table-header btn-info"> <?php echo " ".$header ;?>   </div> 

<div class="space-20"></div>
<div class="form-group col-xs-6">
	<div class="col-xs-10">
		<div class="form-inline ">
			<div class="form-group">Date start</div>

			<div class="form-group">
				<div class="input-group date">
					<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</div>

					<input type="text" class="form-control input-daterange" id="start_date" name="start_date"
						data-date-format="dd-mm-yyyy" autocomplete="off">
				</div> <!-- /.input-group datep -->
			</div>

			<div class="form-group">
				<label for="From" class="col-xs-1 control-label">To</label>
			</div>

			<div class="form-group">
				<div class="input-group date">
					<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</div>
					<input type="text" class="form-control input-daterange" id="end_date" name="end_date"
						data-date-format="dd-mm-yyyy" autocomplete="off">
				</div>
			</div>

		</div>
	</div>
</div>

  <table id="myTable"  cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped ">
    <thead class="text-warning">
        <th>Action</th>
	    <th> PP No </th>	
        <th>Detail</th> 	
        <th>Quote</th> 	
		<th>Reject</th> 
		<th>Company</th>	
        <th>Requester</th> 
        <th>Dept</th>
        <th>Item Type</th> 								
		<th>Vendor</th>			
		<th>Date Sent</th> 
		<th>Curr</th> 
        <th>Type</th> 
         <!-- <th >C.O.A</th> --> 
		<th>Total </th> 
        <!-- <th >PPN </th> --> 
        <th>Total+PPN </th> 
        <th>T.O.P</th> 
	    <th>Dept Head</th> 
	    <th>Purchasing</th> 
        <th>F.C</th> 
	    <th>B.O.D</th> 						
    </thead>
  </table>

 <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Info & Remarks</h4>
                    </div>

                    <div class="modal-body">
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->
                    </div>

                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
<!-- Modal -->

<!-- Modal For Report -->

        <div class="modal fade" id="rptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog  ">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Report convert to excel</h4>
                    </div>

                    <div class="modal-body-report">
                       <table class="table-condensed" align="center"  width="60%" >
                       <tr>
                          <td style="color:#900"><label for="Date start">Date Start  </label></td>
                          <td>
                           <div class="control-group">
                           <div class="col-xs-11">
                          	<input id="txtrptdtstart" name="txtrptdtstart" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
                            </div>
                           </div>
                          </td>
                       </tr>
                        <tr>
                          <td style="color:#900"><label for="Date End"  >Date End   </label></td>
                          <td>
                          <div class="control-group">
                           <div class="col-xs-11">
                          	<input id="txtrptdtend" name="txtrptdtend" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
                            </div>
                           </div>
                          </td>
                       </tr>
                       <tr>
                       	  <td><label for="Company"   >Company  : </label></td>
                          <td>
                         <div class="control-group">
                           <div class="col-xs-11">
                            <select id="cbocomp" name="cbocomp" class="form-control" >
                             <!-- <option value="all">- ALL -</option> --->
                               <?php foreach($tampil_company as $rowcom){ ?>
                             	 <option value="<?php echo $rowcom->id_company; ?>"><?php echo $rowcom->company ;?></option>
                               <?php }?>
                            </select>
                           </div>
                         </div>
                          </td>
                       </tr>
                        <tr>
                       	  <td><label for="Departement"   >Dept </label></td>
                          <td>
                         <div class="control-group">
                         <div class="col-xs-11">
                           <select id="cbodept" name="cbodept"  class="form-control" >
                           <?php  if($this->session->userdata('aproval_flag') =="3"){ ?>
                            <option value="<?php echo $this->session->userdata('id_dept'); ?>"><?php echo $this->session->userdata('dept') ;?></option>
                           <?php }else{ ?>
                              <?php  if($this->session->userdata('aproval_flag') =="4"){ ?>		
                                     <?php foreach($tampil_dept as $rowdept){ ?>
                             	     <option value="<?php echo $rowdept->id_dept; ?>"><?php echo $rowdept->dept ;?></option>  
                                     <?php }?> 	
                              <?php }else{ ?> 
                               	      <option value="all">- ALL -</option>
                                      <?php foreach($tampil_dept as $rowdept){ ?>
                             	      <option value="<?php echo $rowdept->id_dept; ?>"><?php echo $rowdept->dept ;?></option> 
                                      <?php }?> 	
                              <?php }?>
                           <?php }?>
                            </select>
                            </div>
                           </div>
                          </td>
                       </tr>
                       <tr>
                       <td><label for="Status BPK">Status BPK </label></td>
                         <td>
                         <div class="control-group">
                           <div class="col-xs-11">
                            <select id="cbobpk" name="cbobpk"  class="form-control" >
                              <option value="all">- ALL -</option>
                              <option value="1">BPK ONLY</option>
                            </select>
                            </div>
                           </div>
                          </td>
                       </tr>
                       </table>
                    </div>

                     <div class="modal-footer">
                     <button class="btn btn-white btn-info btn-bold" type="submit" id="btnreportexcel_header" name="btnreportexcel_header"  value ="btnreportexcel_header" >
					 <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Excel Total
					 </button>
                     
                        <button class="btn btn-white btn-danger btn-bold" type="submit" id="btnreportexcel" name="btnreportexcel"  value ="btnreportexcel" >
					 <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Excel Detail
					 </button>
                     
                     <button class="btn btn-white btn-warning btn-bold" type="submit" id="btnreport_headerkwee" name="btnreport_headerkwee"  value ="btnreport_headerkwee" >
					 <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total >= 200jt
					 </button>
                     
                     <button class="btn btn-white btn-success btn-bold" type="submit" id="btnreportkwee" name="btnreportkwee"  value ="btnreportkwee" >
					 <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total >= 200jt
					 </button>
                     <button type="button" class="btn btn-white btn-danger  btn-bold" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

<!-- Modal -->

<!-- Modal FPB List Print -->
<div class="modal fade" id="FpbPrintModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">List Recieve</h4>
                                        </div>
                    
                                        <div class="modal-body">
                    
                                        </div>
                    
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- Modal -->

                     <!-- Modal FPB List Print -->
                     <div class="modal fade" id="historyGRN" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="titleHistoryGRN">History GRN</h4>
                                        </div>
                    
                                        <div class="modal-body-grn">
                    
                                        </div>
                    
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- Modal -->


    <!-- Modal BPK List -->
<div class="modal fade" id="listBPKModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">List BPK</h4>
                                        </div>
                    
                                        <div class="modal-body">
                    
                                        </div>
                    
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- Modal -->

                    
                    <!-- Modal BPK List Print -->
                    <div class="modal fade" id="historyBPK" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="titleHistoryBPK">History BPK</h4>
                                        </div>
                    
                                        <div class="modal-body-bpk">
                    
                                        </div>
                    
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- Modal -->

<!-- Modal -->
        <div class="modal fade" id="ViewDetailModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Purchase</h4>
                    </div>

                    <div class="modal-body">

                    </div>

                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
<!-- Modal -->

<!-- Content Popup -->
<div id="dialog" style="display: none;">
    <div>
        <iframe id="frame" width="750px" height="550px"></iframe>
    </div>
</div>
<!-- Content Popup -->

<!-- Fungsi datepickier yang digunakan -->

<script type="text/javascript">

function PDFPopup(e) {
    var url = $(e).attr('req_id');
    $("#dialog").dialog({
        width: 'auto',
        height: 'auto',
        resize: 'auto',
        autoResize: true
    });
    $("#frame").attr("src", url + "#toolbar=0");

};

 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script>

<script>

var counterCheckggivefpb = 0;
	$('body').on('change', 'input[type="checkbox"]', function() {
		this.checked ? counterCheckggivefpb++ : counterCheckggivefpb--;
		counterCheckggivefpb == 1 ? $('#btngiverightfpb').prop("disabled", false): $('#btngiverightfpb').prop("disabled", true);
	})
	
	var counterCheckggivepo = 0;
	$('body').on('change', 'input[type="checkbox"]', function() {
		this.checked ? counterCheckggivepo++ : counterCheckggivepo--;
		counterCheckggivepo == 1 ? $('#btngiverightpo').prop("disabled", false): $('#btngiverightpo').prop("disabled", true);
	})

  var counterCheckggiveinv = 0;
	$('body').on('change', 'input[type="checkbox"]', function() {
		this.checked ? counterCheckggiveinv++ : counterCheckggiveinv--;
		counterCheckggiveinv == 1 ? $('#btngiverightinv').prop("disabled", false): $('#btngiverightinv').prop("disabled", true);
	})

  var counterCheckggivebpk = 0;
	$('body').on('change', 'input[type="checkbox"]', function() {
		this.checked ? counterCheckggivebpk++ : counterCheckggivebpk--;
		counterCheckggivebpk == 1 ? $('#btngiverightbpk').prop("disabled", false): $('#btngiverightbpk').prop("disabled", true);
	})

//-------------------------------------------------------------------------


//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});
//set focus ----------------------------
</script>

<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.print',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;
		var url = '<?php echo site_url("fincon_form/c_finance_controller/get_nopp_modal"); ?>';


		$("#rptModal").modal('hide');
		$("#myModal").modal('show');

		$.ajax({
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}

		});

   });
});
</script>

<script>
//post dari modal popup
$(function(){
    $('#myform').on('submit', function(e){ //id #myform di buat untuk diferentsiasi dengan form open code igniter
        e.preventDefault();
		var url = '<?php echo base_url("fincon_form/c_finance_controller/finance_controller_submit"); ?>';	 // url
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), // myform di buat untuk diferentsiasi dengan form open code igniter
            success: function(data){
            }
        });
    });
});
</script>

<script>
 //modala\ popup report.
$(function(){
  $(document).on('click','.report',function(e){
      $("#myModal").modal('hide');
	  $("#rptModal").modal('show');
	  $("#txtrptdtstart").val('');
	  $("#txtrptdtend").val('');

	  document.getElementById('cbocomp').selectedIndex= '0';
	  document.getElementById('cbodept').selectedIndex = '0';
	  document.getElementById('cbobpk').selectedIndex = '0';
   });
});
</script>

<?php
  // if ( $this->session->userdata('give_righ_flag') != '4' || $this->session->userdata('give_righ_flag') == '3' ): // jika status session user nya 7
  if ( $this->session->userdata('give_righ_flag') != '1'  ):
?>
   <script>
	$(document).ready(function() {

    // Note : Nanti uncomment lagi ya

		//$('#btngiverightfpb').hide();
		//$('#btngiverightpo').hide();
	});
	</script>
    
    
<?php endif ?>




<script type="text/javascript">

// $("#myTable").DataTable({
// 	// "aaSorting": [[5, "asc"]],
// 	//ordering: true,
//         //"order": [],
// 	autoWidth : false,
// 	processing: true,
// 	serverSide: true,
//     "scrollY": 250,
//     "scrollX": true,
// 	ajax: {
// 	  url: "<?php echo base_url('fincon_form/c_finance_controller/cath_data_fordatatables') ?>",
// 	  type:'POST',
// 	},
	
// 	//disabled column sort(first column)
// 		"columnDefs": [ {
// 			"targets": 0,
// 			"orderable": false
// 		} ],
// 		//----------------------------------

	
// 	//button custome datatables
// 	// dom: 'Bfrtip',
//     //    buttons: [
//     //        {
//     //            text: 'Short >= 200 JT',
//     //            action: function ( e, dt, node, config ) {
//     //                alert( 'Button activated' );
//     //            }
//     //        }
//     //    ]
	
// 	 //------------------------------------------------
// });

</script>

<!-- Datatable -->
<script>
$(document).ready(function(){ 
		$('.input-daterange').datepicker({
			language:  'id',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
		});
		
	    fetch_data('no'); //
	
		function fetch_data(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			//dom: 'Bfrtip',
            "lengthChange": false,
			// buttons: [
			// 	'copy', 'csv', 'excel', 'pdf', 'print'
			// ],
				"ajax":{
				    url: "<?php echo base_url('fincon_form/c_finance_controller/cath_data_fordatatables') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
				 
				 'columnDefs': [
							{
							'targets': [0],
							'orderable': false,
							}
					],

						'order': [[2, 'desc']],
			 });
		  }
		  	  
		  
//proses change date-----------------------------------------------
        $('#start_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
		$('#end_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);		
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	
 
 
 
 //onblur date-------------------------------------------------------------------------
 
 		// $('#start_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
		
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		  fetch_data('no', start_date, end_date);
		// 	} 
			
		// });
 
 
 	
		// $('#end_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
			
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		 fetch_data('no', start_date, end_date);
		// 	} 
		// });
 //end onblur-------------------------------------	

  }); //end document on ready	
		
</script>

<script>
//get data from control to modal popup.
$(function(){
  $(document).on('click','.detail',function(e){
		var req_id = $(this).attr('req_id');
		var url = '<?php echo site_url("fincon_form/c_finance_controller/get_idtrans_modal"); ?>';

		$("#ViewDetailModal").modal('show');

		$.ajax({
			type: 'POST',
			url: url,
			data: {id_pp:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}

		});

   });
});

function getListGRN() {

if ($('.editRow:checked').length >= 1) {
    var ids = [];
    $('.editRow').each(function () {
        if ($(this).is(':checked')) {
            ids.push($(this).val());
        }
    });

    var url = '<?php echo site_url("fincon_form/c_finance_controller/getListGRN"); ?>';
    var req_id = ids.toString();
    $("#FpbPrintModal").modal('show');

		$.ajax({
			type: 'POST',
			url: url,
			data: {id_master:req_id},
			success: function (msg) {
        //console.log(msg)
				$('.modal-body').html(msg);
			}

		});
  }

}

// Tampil Detail GRN

function tampilDetailGRN(e) {
                        var value = $(e).attr('req_id');
                        var url = "<?=base_url('fincon_form/C_finance_controller/getDetailGRN');?>";
                        //var acc = confirm("See Detail?");
                        
                        
                        //if (acc == true) {
                            //$("#FpbPrintModal").modal('hide');
                            $("#historyGRN").modal('show');
                        
                            $.ajax({
                                url: url,
                                data: {
                                    number_grn: value
                                },
                               
                                type: "GET",
                                beforeSend: function () {
                                    //$(e).prop('disabled', true);
                                },
                                success: function (msg) {
                                    //$('#btnsendtab').prop('disabled', false);
                                    //$('#titleHistoryGRN').html('History GRN +')
                                    $('.modal-body-grn').html(msg);

                                }
                            });

                        //}

                        return false;
                    }

                    function tampilDetailBPK(e) {
                        var value = $(e).attr('req_id');
                        var url = "<?=base_url('fincon_form/C_finance_controller/getDetailBPK');?>";
                        //var acc = confirm("See Detail?");
                        
                        
                        //if (acc == true) {
                            //$("#FpbPrintModal").modal('hide');
                            $("#historyBPK").modal('show');
                        
                            $.ajax({
                                url: url,
                                data: {
                                    number_grn: value
                                },
                               
                                type: "GET",
                                beforeSend: function () {
                                    //$(e).prop('disabled', true);
                                },
                                success: function (msg) {
                                    //$('#btnsendtab').prop('disabled', false);
                                    //$('#titleHistoryGRN').html('History GRN +')
                                    $('.modal-body-bpk').html(msg);

                                }
                            });

                        //}

                        return false;
                    }

                    function giveRightFPB(e) {
                        var value = $(e).attr('req_id');
                        var url = "<?=base_url('fincon_form/C_finance_controller/giveRightFPB');?>";
                        var acc = confirm("Give Right this FPB?");
                        //var tr =  $(e).parent().parent();
                        
                        if (acc == true) {
                        
                            $.ajax({
                                url: url,
                                data: {
                                    number_grn: value
                                },
                                dataType: "json",
                                type: "GET",
                                beforeSend: function () {
                                    $(e).prop('disabled', true);
                                },
                                success: function (response) {
                                    //$('#btnsendtab').prop('disabled', false);
                                
                                    if(response=='Success') {
                                        alert('Successfully Re-Print this FPB !');
                                        //tr.find('.btn-info').prop('disabled', false);
                                        location.reload();
                                    } else {
                                        alert('Failed Re-Print this FPB !');
                                    }

                                    $(e).prop('disabled', false);

                                }
                            });

                        }

                        return false;
                    }

                    function giveRightBPK(e) {
                        var value = $(e).attr('req_id');
                        var url = "<?=base_url('fincon_form/C_finance_controller/giveRightBPK');?>";
                        var acc = confirm("Give Right this BPK?");
                        //var tr =  $(e).parent().parent();
                        
                        if (acc == true) {
                        
                            $.ajax({
                                url: url,
                                data: {
                                    number_grn: value
                                },
                                dataType: "json",
                                type: "GET",
                                beforeSend: function () {
                                    $(e).prop('disabled', true);
                                },
                                success: function (response) {
                                    //$('#btnsendtab').prop('disabled', false);
                                
                                    if(response=='Success') {
                                        alert('Successfully Re-Print this BPK !');
                                        //tr.find('.btn-info').prop('disabled', false);
                                        location.reload();
                                    } else {
                                        alert('Failed Re-Print this BPK !');
                                    }

                                    $(e).prop('disabled', false);

                                }
                            });

                        }

                        return false;
                    }

function getListBPK() {

if ($('.editRow:checked').length >= 1) {
    var ids = [];
    $('.editRow').each(function () {
        if ($(this).is(':checked')) {
            ids.push($(this).val());
        }
    });

    var url = '<?php echo site_url("fincon_form/c_finance_controller/getListBPK"); ?>';
    var req_id = ids.toString();
    $("#listBPKModal").modal('show');

		$.ajax({
			type: 'POST',
			url: url,
			data: {id_master:req_id},
			success: function (msg) {
        //console.log(msg)
				$('.modal-body').html(msg);
			}

		});
  }

}

</script>
