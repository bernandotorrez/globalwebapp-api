<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class M_signature extends CI_Model 
{
 	
   public $db_table = 'tbl_user';   
   
    function get_pass()
   {     
   		$strid_user =$this->session->userdata('id');	
	    $head = $this->input->post('txthead');  		
		$fc = $this->input->post('txtfc');  		
		$bod = $this->input->post('txtbod');  		
		
		$data=array('sign_head'=>$head,
					'sign_fc'=>$fc,
					'sign_bod'=>$bod,
					 );	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update password user
		
		 $this->session->unset_userdata('sign_head');
		 $this->session->unset_userdata('sign_fc')	;				 
		 $this->session->unset_userdata('sign_bod');
		
		$this->session->set_userdata($data);		
					
		return true;
   }
   
   public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}
  
}