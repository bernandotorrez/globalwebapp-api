<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_signature extends MY_Controller
{
 	
  public function __construct()
    {
		parent::__construct();
        $this->load->helper('form');       
		$this->load->model('M_signature','change_pass',TRUE);
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->change_pass->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect(); 
		}
	}
	
	public function index()
    {		
		   $data['show_view'] = 'signaturefpb_form/V_signature';	
		   $this->load->view('dashboard/Template',$data);	  		 			
	}	
		
	
	public function do_change_sign()	
	{		    
	    $this->change_pass->get_pass();													    
		echo '<script>alert("Signature Update Successfull!");</script>'; 					 		
		
		$url=  base_url('signaturefpb_form/c_signature');		         
		echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";                            				
	}

	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }
	
}