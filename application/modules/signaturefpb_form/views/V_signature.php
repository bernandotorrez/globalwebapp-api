<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>        
<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js');?>" ></script>		
<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.validate.js');?>" ></script>
<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.form.js');?>" ></script>
        
 		
<script type="text/javascript">
        $('document').ready(function(){
			$('#form').validate({
                    rules:{
                        "txthead":{
                            required:true,
                            maxlength:25
                        },
						 "txtfc":{
                            required:true,
                            maxlength:25
                        },
						  "txtbod":{
                            required:true,
                            maxlength:25
                        }},


                    messages:{
                        "txthead":{
                            required:"Head Can't Be Empty"
                        },
						 "txtfc":{
                            required:"fc Can't Be Empty"
                        },
						"txtbod":{
                            required:"bod Can't Be Empty"
                        }},


                    submitHandler: function(form){
                      $(form).ajaxSubmit({        
					
					target: '#tampilanform',   					
                                                  					
                }); 
			
              }
                
            })
						
        });
			
</script>        		

<script>
	//set focus ----------------------------
$(function() {
  $("#txthead").focus();
});		  
//set focus ----------------------------  
</script>     
<style type="text/css">   		
	/*membuat lable yg dihasilkan jquery menjadi warna merah ketika textbox kosong. */
	label
	{
		width:20%;
		display: block;
		margin-right:2%;
		text-align:left;
		line-height:22px;
	}	
    span.error
	{
		font: 11px Verdana, Geneva, sans-serif;
		color:red;
		margin-left:8px;
		line-height:22px;
	}
	
	/*------------------------- */
</style>

<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">    
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel">Change Your Signature Here</div>
   <div class="panel-body">               
    <div id="tampilanform"></div>                   
      
     <form name="form" id="form"   action="<?php echo base_url('c_signature/c_signature/do_change_sign');?>" method="post">
     	 
     <div id="rounded_main_menu" class="col-lg-7 col-lg-offset-0" > 
        <h3>Signature</h3>                             
        <table class="table">                                                       
         <tr> 
            <td>  
                 <div class="form-group">
                   <label for="Head"  class="col-xs-3">Head</label>
                    <div class="col-xs-6">
                 <input type="text" id="txthead" name="txthead" class="form-control" value="<?php echo $this->session->userdata("sign_head"); ?>"  />
                    </div>
                </div>
               </td>
            <td>                    
            </tr>
             <tr> 
            <td>  
                 <div class="form-group">
                   <label for="Head"  class="col-xs-3">Finance Controller</label>
                    <div class="col-xs-6">
                 <input type="text" id="txtfc" name="txtfc" class="form-control" value="<?php echo $this->session->userdata("sign_fc"); ?>"  />
                    </div>
                </div>
               </td>
            <td>                    
            </tr>
             <tr> 
            <td>  
                 <div class="form-group">
                   <label for="Head"  class="col-xs-3">B.O.D</label>
                    <div class="col-xs-6">
                 <input type="text" id="txtbod" name="txtbod" class="form-control" value="<?php echo $this->session->userdata("sign_bod"); ?>"  />
                    </div>
                </div>
               </td>                               
            </tr>
            <tr>
            	<td>
                <div><input type="submit" name="btnchange" value="change" class="btn btn-danger" ></div>     
                </td>
            </tr>
         </table>      
    </div>
    
    </form>   
    
   </div>
 </div>
</div>  
    
 


