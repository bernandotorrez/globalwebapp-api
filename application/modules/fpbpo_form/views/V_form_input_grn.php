

<script type="text/javascript">
//--------------function number only
 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	


</script>
<style>
	.btn-disabled {
		background-color: #ddd!important;
		border-color: #ddd;
	}
</style>


<script script type="text/javascript">

//calculate and add field--------------
$(function(){		
	$('#add').click(function(){
	  addnewrow();	
	 }); 	
	 
	
	 $('body').delegate('.quantity,.price,.quantityin','change',function(){  
		var tr =  $(this).parent().parent();
		var qty=numeral().unformat(tr.find('.quantity').val());
		var price=numeral().unformat(tr.find('.price').val());
		var qtyreceived=numeral().unformat(tr.find('.quantityreceived').val());
		var qtyreceivedbefore=numeral().unformat(tr.find('.quantityreceivedbefore').val());
		var qtyin=numeral().unformat(tr.find('.quantityin').val());
		var qtyoutstanding=numeral().unformat(tr.find('.quantityoutstanding').val());
		var qtyoutstandingbefore=numeral().unformat(tr.find('.quantityoutstandingbefore').val());
		var amt=numeral().unformat(tr.find('.amount').val());
		var amtin=numeral().unformat(tr.find('.amountin').val());
		var amtrec=numeral().unformat(tr.find('.amountreceived').val());
		var stattaxppn =  tr.find(".txtstat").val();
		var stattaxpph =  tr.find(".txtstatpph").val();

		var remaining_price = numeral().unformat(tr.find('.remainingprice').val());
		var remaining_price_hide = numeral().unformat(tr.find('.remainingpricehide').val());

		var tax_ppn = numeral().unformat(tr.find('.tax_ppn').val());
		var tax_pph = numeral().unformat(tr.find('.tax_pph').val());
			

		if(qtyin < 0){
			tr.find('.quantityin').val(0)
			qtyin = 0
			$('#msg_qty').html('<div class="alert alert-danger">Please fill Qty Above 0 !</div>')
		} else if(qtyin > qty){
			// Modified by Bernand
			tr.find('.quantityin').val(0)
			qtyin = 0
			$('#msg_qty').html('<div class="alert alert-danger">You input Qty in more than the required Qty</div>')
		}  else {
			$('#msg_qty').html('');
		
			var amtin = qtyin*price;
			var qtyrec = qtyreceivedbefore + qtyin;
			var qtyout = qty - qtyrec;
			var amtrec = qtyrec*price;
			var remaining_price = remaining_price_hide - amtin;
			//tr.find('.amount').val(amt);
			tr.find('.amountin').val(numeral(amtin).format('0,0.00'));
			tr.find('.amountreceived').val(numeral(amtrec).format('0,0.00'));
			tr.find('.quantityreceived').val(qtyrec);
			tr.find('.quantityoutstanding').val(qtyout);
			tr.find('.remainingprice').val(numeral(remaining_price).format('0,0.00'));

			var new_tax_ppn = tax_ppn / qty;
			var new_tax_pph = tax_pph / qty;
			var total_ppn_pph = amtin + (new_tax_ppn - new_tax_pph);
			tr.find('.amountreceived').val(numeral(total_ppn_pph).format('0,0.00'));
		}
		
		if (stattaxppn == "NONE") { //ppn 10%			   		       		   	
			var ttlppn= amt * (0/100);
			var ttlppnin= amtin * (0/100);
			tr.find('.taxcharge').val(numeral(ttlppn).format('0,0.00'));
			tr.find('.taxchargein').val(numeral(ttlppnin).format('0,0.00'));								
		} else if (stattaxppn == "PPN1%") { //ppn 10%			   		       		   	
			var ttlppn= amt * (1/100);
			var ttlppnin= amtin * (1/100);
			tr.find('.taxcharge').val(numeral(ttlppn).format('0,0.00'));
			tr.find('.taxchargein').val(numeral(ttlppnin).format('0,0.00'));								
		} else if (stattaxppn == "PPN10%") { //ppn 10%			   		       		   	
			var ttlppn= amt * (10/100);
			var ttlppnin= amtin * (10/100);
			tr.find('.taxcharge').val(numeral(ttlppn).format('0,0.00'));
			tr.find('.taxchargein').val(numeral(ttlppnin).format('0,0.00'));								
		}	   

		if (stattaxpph =="PPH2%") { //pph 2%			   
			var ttlpph = amt * (2/100);
			var ttlpphin = amtin * (2/100);	   
			tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
			tr.find('.taxchargepphin').val(numeral(ttlpphin).format('0,0.00'));								
		}else if (stattaxpph =="PPH4%") { //pph 2%			   
			var ttlpph = amt * (4/100);
			var ttlpphin = amtin * (4/100);
			tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
			tr.find('.taxchargepphin').val(numeral(ttlpphin).format('0,0.00'));
		}else if (stattaxpph =="PPH15%") { //pph 2%			   
			var ttlpph = amt * (15/100);
			var ttlpphin = amtin * (15/100);
			tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
			tr.find('.taxchargepphin').val(numeral(ttlpphin).format('0,0.00'));
		}else{	
			tr.find('.taxchargepph').val('0,0.00');
			tr.find('.taxchargepphin').val('0,0.00');
		
		}
		 
	total(); 
});

	  $('body').delegate('.quantity_edit,.price_edit,.quantityin_edit','change',function(){  
		var tr =  $(this).parent().parent();
		var qty_edit=numeral().unformat(tr.find('.quantity_edit').val());
		var price_edit=numeral().unformat(tr.find('.price_edit').val());
		var qtyreceived_edit=numeral().unformat(tr.find('.quantityreceived_edit').val());
		var qtyreceivedbefore_edit=numeral().unformat(tr.find('.quantityreceivedbefore_edit').val());
		var qtyin_edit=numeral().unformat(tr.find('.quantityin_edit').val());
		var qtyoutstanding_edit=numeral().unformat(tr.find('.quantityoutstanding_edit').val());
		var qtyoutstandingbefore_edit=numeral().unformat(tr.find('.quantityoutstandingbefore_edit').val());
		var amountreceivedbefore_edit=numeral().unformat(tr.find('.amountreceivedbefore_edit').val());
	    var amt_edit=numeral().unformat(tr.find('.amount_edit').val());
	    var amtin_edit=numeral().unformat(tr.find('.amountin_edit').val());
	    var amtrec_edit=numeral().unformat(tr.find('.amountreceived_edit').val());
		//var amountreceivedbefore_edit =tr.find(".amountreceived_edit").val(0);
		var stattaxppn_edit =tr.find(".txtstat_edit").val();
		var stattaxpph_edit =tr.find(".txtstatpph_edit").val();

		var remaining_price = numeral().unformat(tr.find('.remainingprice').val());
		var remaining_price_hide = numeral().unformat(tr.find('.remainingpricehide').val());

		var tax_ppn_edit = numeral().unformat(tr.find('.tax_ppn_edit').val());
		var tax_pph_edit = numeral().unformat(tr.find('.tax_pph_edit').val());

		//alert(qtyreceived_edit);
		if(qtyin_edit < 0){
			tr.find('.quantityoutstanding_edit').val(qtyoutstandingbefore_edit);
			tr.find('.quantityreceived_edit').val(qtyreceivedbefore_edit);
			tr.find('.amountreceived_edit').val(0);
			tr.find('.quantityin_edit').val(0);
			tr.find('.quantityin_edit').val(0);
			tr.find('.amountin_edit').val(formatRupiah(amountreceivedbefore_edit));
			tr.find('.taxcharge_edit').val(0);

			tr.find('.quantityin_edit').val(0)
			qtyin_edit = 0
			$('#msg_qty').html('<div class="alert alert-danger">Please fill Qty Above 0 !</div>')
		} else if(qtyin_edit > qtyoutstandingbefore_edit){
			//alert("Quantity yang dimasukkan salah");
			tr.find('.quantityoutstanding_edit').val(qtyoutstandingbefore_edit);
			tr.find('.quantityreceived_edit').val(qtyreceivedbefore_edit);
			tr.find('.amountreceived_edit').val(0);
			tr.find('.quantityin_edit').val(0);
			tr.find('.quantityin_edit').val(0);
			tr.find('.amountin_edit').val(formatRupiah(amountreceivedbefore_edit));
			tr.find('.taxcharge_edit').val(0);

			tr.find('.quantityin_edit').val(0)
			qtyin_edit = 0
			$('#msg_qty').html('<div class="alert alert-danger">You input Qty in more than the required Qty</div>')
		}
		else{
			var qtyrec_edit = qtyreceivedbefore_edit + qtyin_edit;
			var amtin_edit  = qtyin_edit*price_edit;
			var qtyout_edit = qty_edit - qtyrec_edit;
			var amtrec_edit = qtyin_edit*price_edit; 
			var remaining_price = remaining_price_hide - amtin_edit;
			//tr.find('.amount_edit').val(formatRupiah(amt_edit));
			tr.find('.amountin_edit').val(numeral(amtin_edit).format('0,0.00'));
			tr.find('.amountreceived_edit').val(formatRupiah(amtrec_edit));
			tr.find('.quantityreceived_edit').val(qtyrec_edit);
			tr.find('.quantityoutstanding_edit').val(qtyout_edit);
			tr.find('.remainingprice').val(numeral(remaining_price).format('0,0.00'));  
			$('#msg_qty').html('')

			var ttlppn_edit_for_ppn;
			
		   			
		if (stattaxppn_edit == "NONE") { //ppn 10%			   		       		   	
			var ttlppn_edit = amt_edit * (0/100);
			var ttlppnin_edit = amtin_edit * (0/100);	
			ttlppn_edit_for_ppn = ttlppnin_edit;
			tr.find('.taxcharge_edit').val(numeral(ttlppn_edit).format('0,0.00'));
			tr.find('.taxchargein_edit').val(numeral(ttlppnin_edit).format('0,0.00'));								
		} else if (stattaxppn_edit == "PPN1%") { //ppn 10%			   		       		   	
			var ttlppn_edit = amt_edit * (1/100);
			var ttlppnin_edit = amtin_edit * (1/100);	
			ttlppn_edit_for_ppn = ttlppnin_edit;
			tr.find('.taxcharge_edit').val(numeral(ttlppn_edit).format('0,0.00'));
			tr.find('.taxchargein_edit').val(numeral(ttlppnin_edit).format('0,0.00'));								
		} else if (stattaxppn_edit == "PPN10%") { //ppn 10%			   		       		   	
			var ttlppn_edit = amt_edit * (10/100);
			var ttlppnin_edit = amtin_edit * (10/100);	
			ttlppn_edit_for_ppn = ttlppnin_edit;
			tr.find('.taxcharge_edit').val(numeral(ttlppn_edit).format('0,0.00'));
			tr.find('.taxchargein_edit').val(numeral(ttlppnin_edit).format('0,0.00'));								
		}

		var ttlpph_edit_for_pph;

		if (stattaxpph_edit =="PPH2%") { //pph 2%			   
		    var ttlpph_edit = amt_edit * (2/100);
			var ttlpphin_edit = amtin_edit * (2/100);
			ttlpph_edit_for_pph = ttlpphin_edit;  
			tr.find('.taxchargepph_edit').val(numeral(ttlpph_edit).format('0,0.00'));
			tr.find('.taxchargepphin_edit').val(numeral(ttlpphin_edit).format('0,0.00'));								
		}else if (stattaxpph_edit =="PPH4%") { //pph 2%			   
			var ttlpph_edit = amt_edit * (4/100);
			var ttlpphin_edit = amtin_edit * (4/100);
			ttlpph_edit_for_pph = ttlpphin_edit;
			tr.find('.taxchargepph_edit').val(numeral(ttlpph_edit).format('0,0.00'));
			tr.find('.taxchargepphin_edit').val(numeral(ttlpphin_edit).format('0,0.00'));
		}else if (stattaxpph_edit =="PPH15%") { //pph 2%			   
		    var ttlpph_edit = amt_edit * (15/100);
			var ttlpphin_edit = amtin_edit * (15/100);
			ttlpph_edit_for_pph = ttlpphin_edit;
			tr.find('.taxchargepph_edit').val(numeral(ttlpph_edit).format('0,0.00'));
			tr.find('.taxchargepphin_edit').val(numeral(ttlpphin_edit).format('0,0.00'));
		}else{	
			tr.find('.taxchargepph_edit').val('0,0.00');
			tr.find('.taxchargepphin_edit').val('0,0.00');
		}

		var new_tax_ppn = tax_ppn_edit / qtyreceivedbefore_edit;

			if(isNaN(new_tax_ppn) || new_tax_ppn == Number.POSITIVE_INFINITY || new_tax_ppn == Number.NEGATIVE_INFINITY) {
				new_tax_ppn = ttlppn_edit_for_ppn;
			}

			var new_tax_pph = tax_pph_edit / qtyreceivedbefore_edit;

			if(isNaN(new_tax_pph) || new_tax_pph == Number.POSITIVE_INFINITY || new_tax_pph == Number.NEGATIVE_INFINITY) {
				new_tax_pph = 0;
			}
			var total_ppn_pph = amtin_edit + (new_tax_ppn - new_tax_pph);
			tr.find('.amountreceived_edit').val(numeral(total_ppn_pph).format('0,0.00'));
		} 
		
		 
	totalEdit(); 
});
	 	 

	$('body').delegate('.quantity,.price','blur',function(){  
		var tr =  $(this).parent().parent();
		var qty=tr.find('.quantity').val();
		var price=tr.find('.price').val();
		var qtyreceived=numeral().unformat(tr.find('.quantityreceived').val());
		var qtyreceivedbefore=numeral().unformat(tr.find('.quantityreceivedbefore').val());
		var qtyin=numeral().unformat(tr.find('.quantityin').val());
		var qtyoutstanding=numeral().unformat(tr.find('.quantityoutstanding').val());
		var qtyoutstandingbefore=numeral().unformat(tr.find('.quantityoutstandingbefore').val());
		var amt=numeral().unformat(tr.find('.amount').val());
		var amtin=numeral().unformat(tr.find('.amountin').val());
		var stattaxppn =  tr.find(".txtstat").val();
		var stattaxpph =  tr.find(".txtstatpph").val();
		var amtin = qtyin*price;
		var qtyrec = qtyreceivedbefore + qtyin;
		var qtyout = qty - qtyrec;	
		   
		   tr.find('.quantity').val(numeral(qty).format('0,0')) ;
		   tr.find('.price').val(numeral(price).format('0,0.00')) ;
		   tr.find('.amount').val(numeral(amt).format('0,0.00')) ;
		   total();
 	 });  	
	 
	  $('body').delegate('.txtstat','change',function(){  		
		  var tr =  $(this).parent().parent();
		   var amt = numeral().unformat(tr.find('.amount').val());
		   var stattaxppn =  tr.find(".txtstat").val()	 ;
			
		   if (stattaxppn == "PPN") { //ppn 10%			   		       		   	
		       var ttlppn= amt * (10/100)
			   tr.find('.taxcharge').val(numeral(ttlppn).format('0,0.00'));								
		   }		   		  
		   
		   if (stattaxppn == "NONE") { //none			   
				tr.find('.taxcharge').val('0,0.00');
		   }	
		 
		    //function total----------------
		      total() ;
		    //end---------------------------		   		  	   	   		   
 	 }); 	
	 
	  $('body').delegate('.txtstatpph','click',function(){  		
		   var tr =  $(this).parent().parent();
		   var amt = numeral().unformat(tr.find('.amount').val())	;	
			 var amtin = numeral().unformat(tr.find('.amountin').val())	;			 
		   var stattaxpph =  tr.find(".txtstatpph").val();
					 
		   if (stattaxpph =="PPH2%") { //pph 2%			   
		       var ttlpph = amt * (2/100);
					 var ttlpphin = amtin * (2/100);
				 tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));	   
			   tr.find('.taxchargepphin').val(numeral(ttlpphin).format('0,0.00'));								
		   }else{
			    if (stattaxpph =="PPH4%") { //pph 2%			   
		      	    var ttlpph = amt * (4/100);
								var ttlpphin = amtin * (4/100);
								tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
					 			tr.find('.taxchargepphin').val(numeral(ttlpphin).format('0,0.00'));
				}else{	
					if (stattaxpph =="PPH15%") { //pph 2%			   
		      	    	var ttlpph = amt * (15/100);
									var ttlpphin = amtin * (15/100);
									tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
						 			tr.find('.taxchargepphin').val(numeral(ttlpphin).format('0,0.00'));
					}else{	
			   			 tr.find('.taxchargepph').val('0,0.00');
						 	 tr.find('.taxchargepphin').val('0,0.00');
					}
				}
		   }
		 
		    //function total----------------
		      total() ;
		    //end---------------------------	   		  	   	   		   
 	 }); 	
	 	
	 	  	
});


function total()
{
	var t=0;
	var tppn=0;
	var tpph=0;
	var tin=0;
	var tppnin=0;
	var tpphin=0;	
	var full_receive=0;
	var jmlbaris=parseInt($('.remove').length);
	
	for(var i=0;i<jmlbaris;++i){
		if(parseFloat(numeral().unformat($('.quantityin').eq(i).val())) > 0) {
			t+=parseFloat(numeral().unformat($('.amount').eq(i).val()));
			tin+=parseFloat(numeral().unformat($('.amountin').eq(i).val()));
		}
	//    t+=parseFloat(numeral().unformat($('.amount').eq(i).val()));
	// 	 tin+=parseFloat(numeral().unformat($('.amountin').eq(i).val()));

	full_receive+=parseFloat(numeral().unformat($('.quantityoutstanding').eq(i).val()));
	}	

	
	
	for(var i=0;i<jmlbaris;++i){	
	
	      tppn+=parseFloat(numeral().unformat($('.taxcharge').eq(i).val()));			  
	  	  tpph+=parseFloat(numeral().unformat($('.taxchargepph').eq(i).val()));
				tppnin+=parseFloat(numeral().unformat($('.taxchargein').eq(i).val()));			  
	  	  tpphin+=parseFloat(numeral().unformat($('.taxchargepphin').eq(i).val()));	   		 		  
	}		
	
			
	var hasilgranppn = (t+tppn) - tpph;
	var hasilgranppnin = (tin+tppnin) - tpphin;
	$('.total').val(numeral(tin).format('0,0.00')) ;	
	$('.ppncalc').val(numeral(tppnin).format('0,0.00'));
	$('.pphcalc').val(numeral(tpphin).format('0,0.00'))	;
	$('.grandppn').val(numeral(hasilgranppnin).format('0,0.00'));
	$('#full_receive').val(full_receive);
	}

	function totalEdit()
{
	var t=0;
	var tppn=0;
	var tpph=0;
	var tin=0;
	var tppnin=0;
	var tpphin=0;	
	var full_receive=0;
	var jmlbaris=parseInt($('.remove').length);
	
	for(var i=0;i<jmlbaris;++i){
		if(parseFloat(numeral().unformat($('.quantityin_edit').eq(i).val())) > 0) {
			t+=parseFloat(numeral().unformat($('.amount_edit').eq(i).val()));
			tin+=parseFloat(numeral().unformat($('.amountin_edit').eq(i).val()));
		}

		// t+=parseFloat(numeral().unformat($('.amount_edit').eq(i).val()));
		// 	tin+=parseFloat(numeral().unformat($('.amountin_edit').eq(i).val()));
		full_receive+=parseFloat(numeral().unformat($('.quantityoutstanding_edit').eq(i).val()));
	}	
	
	for(var i=0;i<jmlbaris;++i){	
	
	      tppn+=parseFloat(numeral().unformat($('.taxcharge_edit').eq(i).val()));			  
	  	  tpph+=parseFloat(numeral().unformat($('.taxchargepph_edit').eq(i).val()));
				tppnin+=parseFloat(numeral().unformat($('.taxchargein_edit').eq(i).val()));			  
	  	  tpphin+=parseFloat(numeral().unformat($('.taxchargepphin_edit').eq(i).val()));	   		 		  
	}		
	
			
	var hasilgranppn = (t+tppn) - tpph;
	var hasilgranppnin = (tin+tppnin) - tpphin;
	$('.total').val(numeral(tin).format('0,0.00')) ;	
	$('.ppncalc').val(numeral(tppnin).format('0,0.00'));
	$('.pphcalc').val(numeral(tpphin).format('0,0.00'))	;
	$('.grandppn').val(numeral(hasilgranppnin).format('0,0.00'));
	$('#full_receive').val(full_receive);
	}

function addnewrow()
{

var strnopp =$('#txtnopp').val()

var n = ($('.detail tr').length-0)+1;	
var tr = '<tr>'+
'<td class="no" align="center">'+ n +'</td>'+                                            
'<td><input type="text" id="txtidmasdetid" name="txtidmasdet[]"  value="'+strnopp+'"  hidden="hide" /><input type="text" name="txtiddet[]" class="txtid" value="" hidden="hide"  /> <input type="text" name="flagstat[]"  value="1" hidden="hide" /><input type="text" maxlength="70"  name="txtdesc[]"  placeholder="Item Type" size="24" class="form-control txtdesc" /></td>'+'<td><input type="text" maxlength="70"  name="txtspecs[]"  placeholder="Description" size="30" class="form-control txtspecs" /></td>'+'<td><input maxlength="6" id="txtqty"  name="txtqty[]" type="text" placeholder="0" size="7" onkeypress="return isNumberKey(event)" class="form-control quantity" /></td>'+                                                		                                  
'<td><input maxlength="14"  name="txtprice[]" type="text" placeholder="0.00" size="30" onkeypress="return isNumberKey(event)" class="form-control price"/> </td>'+	
'<td><input  maxlength="70"   name="txtamount[]"  placeholder="0.00"  size="35"  class="form-control amount" readonly="readonly"/></td>'+'<td width="7%"><select  name="cbotax[]" class="form-control txtstat"><option value="NONE">NONE</option><option value="PPN">PPN</option></select></td>'+' <td width="7%"><select  name="cbotaxpph[]" class="form-control txtstatpph"><option value="NONE">NONE</option><option value="PPH2%">PPH2%</option><option value="PPH4%">PPH4% </option><option value="PPH15%">PPH15% </option>  </select></td>'+'<td><input  maxlength="70" name="txttaxcharge[]" placeholder="0.00"  size="30"  class="form-control taxcharge" readonly="readonly" /></td>'+'<td><input  maxlength="70" name="txttaxchargepph[]"  placeholder="0.00"  size="30"  class="form-control taxchargepph" readonly="readonly" /></td>'+'<td> <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button></td>'+
'</tr>';
 
 $('.detail').append(tr);
}


</script>

<script>
$(function(){		
	$('.ppnnot').change(function(){		 		
		 if($(this).is(':checked')){
				  $('.ppncalc').val("0.00");	
				  $('.grandppn').val($('.total').val());						 		 
		 }else{					
			total()
		 }		 		
	});
});
</script>

<script type="text/javascript">
//upload menggunakan userfile type file
// $(document).ready(function(){	
// 	 $('#fileSelect').bind('change',function(event){     	 
// 	  event.preventDefault(); 	  
// 	  var formData = new FormData(document.getElementById('form-upload'));		 										
// 		   $.ajax({					          					
// 					url:"<?php echo base_url('fpbpo_form/C_print_fpb_po/do_upload_excel');  ?>", 										
// 					type: 'POST',						
// 					//enctype: "multipart/form-data", 					
// 					data: formData,										
// 					processData: false,															
// 		    		//async: false,
// 					processData: false,
// 					contentType: false,		
// 					cache : false,	
// 					beforeSend:function()
// 					 {
// 						$("#prog").show();
// 						$("#prog").attr('value','0');
						   
// 				     },
// 					   uploadProgress:function(event,position,total,percentCompelete)
// 					   {
// 						  $("#prog").attr('value',percentCompelete); 
// 						  $("#percent").html(percentCompelete+'%');
// 					   },		            
									
// 				    success:function (data, textStatus, jqXHR) {			 											
// 					  alert(data);
// 					  $('#hexcel').show();
// 					  $('#hdel').show();																	
// 				    }
// 		  });	 
//     });
// });

</script>


<script>
function load_fromexcel_data_part() //fungsi load
{   
  $.ajax({
   url:"<?php 
          //$company_cut =substr($this->session->userdata('short'),0,-1);
		  $company_cut =substr($this->session->userdata('short'),0,-1);
		  $id_dept =$this->session->userdata('id_dept');
          echo base_url('asset/uploads/'.$company_cut."/".$id_dept.'/data_part.csv'); 
		?>",
   dataType:"text",
   success:function(data)
   {		
	//---------------------------------------------------     
    var part_data = data.split(/\r?\n|\r/);	  
	//---------------------------------------------------
	var strnopp =$('#txtnopp').val()
    for(var count = 0;count< part_data.length - 1; count++)
    {
	     	 
     var cell_data = part_data[count].split(",");	 	
		 
	 var tr = '<tr id="rowdetail">'+
'<td class="no" align="center">'+ cell_data[0] +'</td>'+'<td><input type="text" id="txtidmasdetid" name="txtidmasdet[]"  value="'+strnopp+'" hidden="hide" /><input type="text" name="txtiddet[]" class="txtid" value="" hidden="hide" /> <input type="text" name="flagstat[]"  value="1" hidden="hide" /><input type="text" maxlength="70"  name="txtdesc[]" value='+ cell_data[1] +' placeholder="Item Type" size="24" class="form-control txtdesc" /></td>'+'<td><input type="text" maxlength="70"  name="txtspecs[]" value='+ cell_data[2] +'  placeholder="Description" size="30" class="form-control txtspecs" /></td>'+'<td><input maxlength="6" id="txtqty"  name="txtqty[]" value='+ cell_data[6] +' type="text" placeholder="0" size="7" onkeypress="return isNumberKey(event)" class="form-control quantity"  /></td>'+                                                		                                  
'<td><input maxlength="14" style="text-align: right;" name="txtprice[]" value='+ numeral(cell_data[7]).format('0,0.00')  +' type="text" placeholder="0.00" size="30" onkeypress="return isNumberKey(event)" class="form-control price"/> </td>'+	
'<td><input  maxlength="70"  style="text-align: right;"  name="txtamount[]" value='+ numeral(cell_data[8]).format('0,0.00') +' placeholder="0.00"  size="35"  class="form-control amount" readonly="readonly"/></td>'+' <td width="7%"><select  name="cbotax[]" class="form-control txtstat"><option value='+ cell_data[9] +'>'+ cell_data[9] +'</option><option value="NONE">NONE</option><option value="PPN">PPN</option></select></td> '+'<td width="10%"><select  name="cbotaxpph[]" class="form-control txtstatpph" ><option value='+ cell_data[10] +'>'+ cell_data[10] +'</option><option value="NONE">NONE</option><option value="PPH2%">PPH 23 2%</option><option value="PPH4%">PPH 23 4%</option><option value="PPH15%">PPH 23 15%</option></select></td> '+'<td><input maxlength="70" name="txttaxcharge[]"  placeholder="0.00"  size="30"  class="form-control taxcharge" readonly="readonly" /></td>'+'<td><input  maxlength="70" name="txttaxchargepph[]"  placeholder="0.00"  size="30"  class="form-control taxchargepph" readonly="readonly" /></td>'+'<td>  <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button></td>'+
'</tr>';    
	$('.detail').append(tr);   	 	
	
	//------call total detail....	
	       var tr =  $(this).parent().parent();	
		  // numeral().unformat($('.taxcharge').eq(i).val())	  
		   var amt =  numeral().unformat(cell_data[8]);		   
		   var stattaxppn =  tr.find(".txtstat").val();
		   var stattaxpph =  tr.find(".txtstatpph").val();
				  
		   if ( cell_data[9] == "PPN") { //ppn 10%			   		       		   	
		       var ttlppn= amt * (10/100)	;		  			  
			   $('.taxcharge').eq(count).val(numeral(ttlppn).format('0,0.00'))	;							
		   }		   		  		  
		   
		    if ( cell_data[9] == "NONE") { //none			   				
				$('.taxcharge').eq(count).val('0,0.00')	;
		    }		
		   
		  
					 
		   if (cell_data[10] =="PPH2%") { //pph 2%			   
		       var ttlpph = amt * (2/100);	   
			   $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));								
		   }else{
			    if (cell_data[10] =="PPH4%") { //pph 2%			   
		      	    var ttlpph = amt * (4/100);
					 $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));
				}else{	
					if (cell_data[10] =="PPH15%") { //pph 2%			   
		      	    	var ttlpph = amt * (15/100);
						 $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));
					}else{	
			   			 $('.taxchargepph').eq(count).val('0,0.00');
						 $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));
					}
				}
		   }	 			
	      
		   total(); //call total
	  //--------------------------	
	
    }//endfor			
   }   
  });
};

</script>

<script type="text/javascript">
//fungsi check file excel jika exist.
$(document).ready(function() {
	$('#btnsavebtn').click(function(e){ 
	  var url = "<?php echo base_url('fpbpo_form/C_print_fpb_po/add_grn_new'); ?>";		 
	  var formData = new FormData(document.getElementById('form-upload'));	
	  var m =  $('#ViewloadinglModal');	  

	  var check_qty = 0;
	  var check_input_grn = 0;
	  var do_number = $('#do_number').val();
	  var attach_do = $('#txtupload').val();

	  $('.input_validation').each(function() {
		var val = $(this).val();

		if(val == "") {
			//$('#input_message').html('<div class="alert alert-danger">Please fill this Field !</div>')
			//$('.input_validation').css('border', '1px solid red');
			check_input_grn += 1;
			return false;
		} else {
			//$('#input_message').html('')
			//$('.input_validation').css('border', '1px solid #d5d5d5');
			check_input_grn += 0;
			return false;
		}


	})

	  $('.qty_validation').each(function() {
		var val = $(this).val();

		if(val == "") {
			$('#msg_qty').html('<div class="alert alert-danger">Please fill Qty In !</div>')
			$('.qty_validation').css('border', '1px solid red');
			return false;
		}

		check_qty += parseInt(val);
	  })	

	  if(do_number == '') {
			$('.input_message').html('<div class="alert alert-danger">Please fill this field ! </div>')
		  $('.input_validation').css('border', '1px solid red');
		  $('#do_number').focus()
		return false;
	  } else if(attach_do == '') {
			$('.input_message').html('<div class="alert alert-danger">Please fill this field ! </div>')
		  $('.input_validation').css('border', '1px solid red');
		  $('#txtupload').focus()
		return false;
	  }	else if(check_qty == 0) {
		  $('#msg_qty').html('<div class="alert alert-danger">Please fill Qty In above 0 !</div>')
		  $('.qty_validation').css('border', '1px solid red');
		return false;
	  } else {
		$('#msg_qty').html('')
		$('.input_message').html('')
		$('.qty_validation').css('border', '1px solid #d5d5d5');
		//after validation--------------------------------------		

			ajax()
			return false;
		


	  }
	  
	  function ajax() {
		$.ajax({

			url: url, // point to server-side PHP script 
			//dataType: 'json',  // what to expect back from the PHP script, if anything
			cache: false,
			contentType: false,
			processData: false,
			data: formData,                         
			type: 'post',

			beforeSend: function () {
				//m.modal('show')
				$('#btnsavebtn').prop('disabled', true);
			},
			success: function (response) {
				//m.modal('hide');
				console.log(response);
				var json = JSON.parse(response);
						var status_upload = json.status_upload;
						var message_upload = json.message_upload;
						var status_update_flag_received = json.status_update_flag_received;
						var status_insert_master_grn = json.status_insert_master_grn;
						var status_insert_detail_grn = json.status_insert_detail_grn;

						if(status_upload == 'gagal') {
							alert("Update & Upload Fail, Message : " + message_upload.error);
						} else {
							if (status_update_flag_received == true && status_insert_master_grn == true && status_insert_detail_grn == true) {
							alert("Update Successfully");
							var url_redirect = "<?=base_url('fpbpo_form/c_print_fpb_po');?>";
							window.location.href = url_redirect;
							} else if (status_update_flag_received != true || status_insert_master_grn != true || status_insert_detail_grn || true) {
								alert("One or More Update Failed");
							} 
						}
						
						

				$('#btnsavebtn').prop('disabled', false);
			}

			});
	  }
			  
 	 });	
})
 

</script>

<script>

$(document).ready(function () {
	$(function () {
		$("#cbovendor_search").autocomplete({
			minLength:0,
			delay:0,
			source: function(request, response) {	
			var str_url = '<?php echo site_url('fpbpo_form/C_print_fpb_po/suggest_vendor'); ?>';					
			var str_vendor =$("#cbovendor_search").val();																																		
				$.ajax({ 
					url: str_url ,												
					data: {vendor:str_vendor},																																		
					dataType: "json",
					type: "POST",										  					
					success: function(data){						
						response(data);	
						$(".ui-autocomplete").css("z-index", "2147483647");																																																						
					}						
				});
			},	
				select:function(event, ui){						   		  
				   $('#cbovendor').val(ui.item.idvendor);					  
				}							                  				                     
		});		
			
	});

});	  



 </script> 
 
 
 <script>

/*$('body').delegate('.cbocoa_search_data','focusin',function() {	   
	        var tr =  $(this).parent().parent();	
		    tr.find(".cbocoa_search_data").autocomplete({			 
			
			
			minLength:0,
			delay:0,			
			source: function(request, response) {				
			var str_url = '<?php //echo site_url('fpbpo_form/C_print_fpb_po/suggest_coa'); ?>';					
			var str_coa = tr.find('.cbocoa_search_data').val()					
				$.ajax({ 
					url: str_url ,												
					data: {coa:str_coa},																																		
					dataType: "json",
					type: "POST",										  					
					success: function(data){						
						response(data);	
						//alert(str_coa);
						$(".ui-autocomplete").css("z-index", "2147483647");																																																						
					}						
				});
			},	
				select:function(event, ui){					   				  				 
				   tr.find('.cbocoa').val(ui.item.idcoa);					  
				}						                  				                     
		});		
			
}); */


$('body').delegate('.cbocoa_search','blur',function(){  	
  var coa = $('#cbocoa').val() ;
  var coasearch = $('#cbocoa_search').val() ;
  cboact_search_data
	if(coa == "" ) {
	  $('#cbocoa_search').val("") ;
	  $('#cbocoa_search').focus();
	}
	
	if (coasearch==""){
		$('#cbocoa').val("");
		$('#cbocoa_search').focus();
	}
	  
		   		
}); 	

 </script> 
 
 <script>
  $('body').delegate('.cboact_search_data','focusin','change',function() { //using focusin
	        var tr =  $(this).parent().parent();
			var str_coa = tr.find('.cbocoa_search_data').val();
			
		    tr.find(".cboact_search_data").autocomplete({
			
			minLength:0,
			delay:0,
			source: function(request, response) {
			var str_url = '<?php echo site_url('fpbpo_form/C_print_fpb_po/suggest_act'); ?>';
			var str_act = tr.find('.cboact_search_data').val();
			//var totalrow=parseInt($('.remove').length);
		        /* for(var i=0;i<totalrow;++i){
		            var str_coa =$('.cbocoa_search_data').eq(i).val();
						if (str_coa=="") {
							$('.cboact_search_data').eq(i).val("")
							alert("coa must be required");
							$('.cbocoa_search_data').eq(i).focus()
							return;	
						}
				   }*/	
			
				$.ajax({
					url: str_url ,
					data: {act:str_act},
					dataType: "json",
					type: "POST",
					success: function(data){
						response(data);
						$(".ui-autocomplete").css("z-index", "2147483647");
						
					}
				});
			},
				select:function(event, ui){
					var thisRow = $(this).parents("tr");
					thisRow.find('.cbocoa_search_data').val(ui.item.nomorcoa);
				}
		});

    }); //bodydelagite



/*$('body').delegate('.cbocoa_search_data','blur',function(){
   var tr =  $(this).parent().parent();
   var coa = tr.find('.cbocoa').val() ;
   var coasearch = tr.find('cbocoa_search_data').val() ;

	if(coa=="") {
	  tr.find('.cbocoa_search_data').val("") ;
	  tr.find('.cbocoa_search_data').focus();
	}

	if (coasearch==""){
		tr.find('.cbocoa').val("");
		tr.find('.cbocoa_search_data').focus();
	}
}); */
 /*$(function() {
    $(".chosenselect").chosen();
  }); */
 </script>

 <script>
	/* Fungsi formatRupiah */
function formatRupiah(angka) {
	//var angka = e.value;
	var angka = angka.toString()
	var number_string = angka.replace(/[^\d]/g, ''),
	
		split = number_string.split(','),
		sisa = split[0].length % 3,
		rupiah = split[0].substr(0, sisa),
		ribuan = split[0].substr(sisa).match(/\d{3}/gi);

	// tambahkan titik jika yang di input sudah menjadi angka ribuan
	if (ribuan) {
		separator = sisa ? ',' : '';
		rupiah += separator + ribuan.join(',');
	}

	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	return rupiah;
	//e.value = rupiah;
}
 </script>

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_fail') ;?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_insert_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_insert_fail');?>  
        </div>	
<?php } ?>


 
<form class="form-multi" id="form-upload" enctype="multipart/form-data" accept-charset="utf-8">
  
    <div class="table-header btn-info">
                <?php echo " ".$header ;?> 
</div>


	<!-- Untuk cek apakah sudah receive semua, kalo receive semua itu sama dengan 0 -->
	<input type="hidden" id="full_receive" name="full_receive" value="1"/>
          
<?php	 
					if(!empty($tampil_pp)){
						foreach($tampil_pp as $row) {
						$sqlcount  = "select * from tbl_master_grn where id_master='".$row->id_master."' ORDER BY number_grn DESC LIMIT 1";
						$qrycount  = $this->db->query($sqlcount);
						$adacount = $qrycount->result();
						if($adacount){
							$number = $adacount[0]->number_pay + 1;
							$number_pay = $adacount[0]->number_pay + 1;
						}else{
							$number = 1;
							$number_pay = 1;
						}

						$filter_idmaster = str_replace("/", "", $row->id_master);
						$date = date('dmy');
						
						//$numbergrn = "RO"."/".$row->id_master."/".$number;
						$numbergrn = "RO".$filter_idmaster."/".$date.$number;
						?> 

	<div class="space-10"></div>
	<!-- Responsive Form for Mobile and Web Start -->
	<div class="container">

	<input type="hidden" id="number_pay" name="number_pay" class="form-control" readonly="readonly" ="true" value="<?php echo $number_pay ?>" on />

		<!-- Row Start -->
		<div class="row">
			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label">PR Number</label>

					<input type="text" id="txtnopp" name="txtnopp" class="form-control" readonly="readonly" ="true" value="<?php echo $row->id_master ?>" on />
				</div>
			</div>

			<div class="col-sm-1">


			</div>

			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label">Number GRN *</label>

					<input type="text" id="txtnogrn" name="txtnogrn" class="form-control" readonly="readonly" ="true" value="<?php echo $numbergrn ?>" on />
				</div>
			</div>

		</div>
		<!-- Row End -->

		<!-- Row Start -->
		<div class="row">
			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label">Date Submission</label>

					<input type="text" id="txtdate" name="txtdate" class="form-control" readonly value="<?php echo date("d-m-Y") ?>" />
				</div>
			</div>

			<div class="col-sm-1">


			</div>

			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label">Requester</label>

					<input type="text" id="txtsub" name="txtsub" class="form-control" value="<?php echo $this->session->userdata('name'); ?>" readonly="readonly" />
				</div>
			</div>

		</div>
		<!-- Row End -->

		<!-- Row Start -->
		<div class="row">
			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label">Currency</label>

					<input type="text" id="txtcurr" name="txtcurr" class="form-control" value="<?php echo $row->currency ;?>" readonly />
				</div>
			</div>

			<div class="col-sm-1">


			</div>

			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label">Vendor Name</label>

					<input type="text" id="txtvendor" name="txtvendor" class="form-control" value="<?php echo $row->vendor ?>" readonly />
				</div>
			</div>

		</div>
		<!-- Row End -->

		<!-- Row Start -->
		<div class="row">
			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label">Remarks</label>

					<input type="text" id="txtremarks" name="txtremarks" class="form-control" value="<?php echo $row->remarks; ?>" readonly />
				</div>
			</div>

			<div class="col-sm-1">


			</div>

			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label">Department</label>

					<input type="text" id="txtremarks" name="txtremarks" class="form-control" value="<?php echo $row->dept; ?>" readonly />
				</div>
			</div>

		</div>
		<!-- Row End -->

		<!-- Row Start -->
		<div class="row">
			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label">DO Number</label>

					<input type="text" id="do_number" name="do_number" class="form-control input_validation" required/>
					
					<br>
					<font class="text-danger input_message" style="font-size:15px" size="15px"></font>
				</div>
			</div>

			<div class="col-sm-1">


			</div>

			<div class="col-sm-5">
				<div class="form-group">

					<label class="control-label" style="color:#900">Upload DO (PDF, Max 2 MB)  </label>

					<input type="file" onChange="checkSize(this)" id="txtupload" name="userfile" class="btn btn-info input_validation" 
					accept="application/pdf"  />

					<br>
					<font class="text-danger" style="font-size:15px" size="15px" id="alert-msg"></font>
					<font class="text-danger input_message" style="font-size:15px" size="15px"></font>
				</div>
			</div>

		</div>
		<!-- Row End -->

	</div>

	
	<!-- Responsive Form for Mobile and Web Start -->
	<?php } } ?> 	

   
<div class="table-header btn-info">
       GRN detail
</div>


<br/>
<div id="msg_qty"></div>
<input  type="text" name="txtiddet_buff"  readonly="readonly" class ="buffarray_iddet" width="70"  hidden="hide"  />
<br/>

<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>No</th>
				<th>Item Type</th>
				<th>Description</th>
				<th>Qty</th>
				<th>Unit Price</th>
				<th>Total Price <br> (Ordered)</th>
				<th>Qty Receipt</th>
				<th hidden>Amount Received</th>
				<th>Qty Outstd</th>
				<th>Qty Input</th>
				<th>Total Price <br> (Inputed)</th>
				<th>Outstd Value</th>
				<th>PPN</th>
				<th hidden>PPH</th>
				<th align="center"><strong>
						<input type="button" disabled id="add" Class="btn-sm radius-6 btn-disabled" value="+" />
				</th>
			</tr>
		</thead>

		<tbody class="detail">
			<?php if(empty($tampil_ada_grn)){
		  	  foreach($tampil_det_pp as $row2){ 
			  $ino++ ; // buat  perulangan tampilan detail 
			  ?>
			<tr>
				<td class="no" align="center"><?php echo $ino ;?></td>
				<td>
					<input type="text" name="flagstat[]" value="1" hidden="hide" />
					<input type="text" id="txtidmasdetid" name="txtidmasdet[]" maxlength="25"
						value="<?php echo $row2->id_master ?>" hidden="hide" />
					<input type="text" name="txtiddet[]" value="<?php echo $row2->id_detail ?>" class="txtid"
						hidden="hide" />
					<input type="text" maxlength="70" id="txtdesc" name="txtdesc[]" placeholder="Item Type" size="24"
						class="form-control txtdesc" value="<?php echo $row2->desc ?>" readonly />
				</td>
				<td><input type="text" maxlength="70" name="txtspecs[]" placeholder="Description" size="30"
						class="form-control txtspecs" value="<?php echo $row2->spec ?>" readonly /></td>
				<!-- <td hidden="hide">
            <input type="text" maxlength="70"  name="txtporeff[]"  placeholder="Invoice No" size="30" class="form-control txtporeff" value="<?php //echo $row2->po_reff ?>"  />
            </td> -->
				<!--  <td hidden="hide">
            <div class="form-group">           
            <div class="col-xs-11">
            <input data-rel="tooltip" type="text" name="cboact_search[]" placeholder="Actifity No" title="<script>Hello Put like : Actifity number </script>" data-placement="bottom" class="col-md-12 cboact_search_data" size="30" maxlength="40" value="<?php //echo $row2->no_act ?>"  />
            </div>
            </div>       
            </td>-->
				<!--<td>
            <div class="form-group">           
            <div class="col-xs-11">
            <input data-rel="tooltip" type="text" name="cbocoa_search[]" placeholder="C.O.A Number /C.O.A Desc" title="Hello Put like : Coa Number /C.O.A Desc " data-placement="bottom" class="col-md-12 cbocoa_search_data" size="40" maxlength="40" value="<?php //echo $row2->coa ?>"  />
            </div>
            </div>   
            </td>  -->
				<td><input maxlength="6" style="text-align: center;" name="txtqty[]" type="text" placeholder="0" size="8"
						onkeypress="return isNumberKey(event)" class="form-control quantity" id="txtqty"
						value="<?php echo number_format($row2->qty) ?>" readonly /></td>
				<td><input maxlength="14" style="text-align: right;" name="txtprice[]" type="text" placeholder="0.00" size="30"
						onkeypress="return isNumberKey(event)" class="form-control price" id="txtprice"
						value="<?php echo number_format($row2->harga,2) ?>" readonly /></td>
				<td><input maxlength="70" style="text-align: right;" style="text-align: right;" name="txtamount[]" placeholder="0.00" size="30" class="form-control amount"
						readonly="readonly" value="<?php echo number_format($row2->total,2) ?>" readonly /></td>
				<input maxlength="70" type="hidden" name="cbotax[]" class="form-control txtstat"
					value="<?=trim($row2->tax_type);?>" />
				<input maxlength="70" type="hidden" name="cbotaxpph[]" class="form-control txtstatpph"
					value="<?=trim($row2->tax_typepph);?>" />
				<input maxlength="70" type="hidden" name="txttaxcharge[]" class="form-control taxcharge"
					value="<?php echo number_format($row2->tax_detail,2) ?>" />
				<input maxlength="70" type="hidden" name="txttaxchargepph[]" class="form-control taxchargepph"
					value="<?php echo number_format($row2->tax_detailpph,2) ?>" />

				<input name="txtqtyreceivedbefore[]" type="hidden" class="form-control quantityreceivedbefore"
					id="txtqtyreceivedbefore" value="0" />
				<input name="txtqtyoutstandingbefore[]" type="hidden" class="form-control quantityoutstandingbefore"
					id="txtqtyoutstandingbefore" value="<?php echo number_format($row2->qty) ?>" />
				<input name="txtamountreceivedbefore[]" type="hidden" class="form-control amountreceivedbefore"
					id="txtamountreceivedbefore" value="0" />
				<td><input maxlength="6" style="text-align: center;" style="text-align: center;" name="txtqtyreceived[]" type="text" placeholder="0" size="8"
						onkeypress="return isNumberKey(event)" class="form-control quantityreceived" id="txtqtyreceived"
						value="0" readonly /></td>
				<td hidden><input maxlength="70" style="text-align: right;"  name="txtamountreceived[]" placeholder="0.00" size="30"
						class="form-control amountreceived" readonly="readonly" value="0" readonly /></td>
				<td><input maxlength="6" style="text-align: center;" name="txtqtyoutstanding[]" type="text" placeholder="0" size="8"
						onkeypress="return isNumberKey(event)" class="form-control quantityoutstanding" id="txtqtyoutstanding"
						value="<?php echo number_format($row2->qty) ?>" readonly /></td>
				<td><input maxlength="6" style="text-align: center;" name="txtqtyin[]" type="text" placeholder="0" size="8"
						onkeypress="return isNumberKey(event)" class="form-control quantityin qty_validation" id="txtqtyin"
						value="0" /></td>
				

				<td>
					<input maxlength="70" style="text-align: right;" name="txtamountin[]" placeholder="0.00" size="30"
						class="form-control amountin" readonly="readonly" value="0" readonly />
				</td>
				<td>
					<input maxlength="70" style="text-align: center;" name="txtremainingprice[]" type="text" placeholder="0" size="30"
						onkeypress="return isNumberKey(event)" class="form-control remainingprice" id="txtremainingprice"
						value="<?php echo number_format($row2->total) ?>" readonly />
					<input type="hidden" value="<?php echo number_format($row2->total) ?>" name="txtremainingpricehide[]"
						class="form-control remainingpricehide">
				</td>
				<td><input maxlength="70" style="text-align: right;" name="txttaxchargein[]" placeholder="0.00" size="30"
						class="form-control taxchargein" readonly="readonly" value="0" /></td>
				<td hidden><input maxlength="70" style="text-align: right;" name="txttaxchargepphin[]" placeholder="0.00" size="30"
						class="form-control taxchargepphin" readonly="readonly" value="0" /></td>
				<td><button class="btn-app btn-disabled btn-sm radius-6 remove" disabled><i
							class="glyphicon glyphicon-remove"></i></button></td>

							<td>
				<input maxlength="70" type="hidden" name="tax_ppn[]" class="form-control tax_ppn"
					value="<?php echo number_format($row2->tax_detail,2) ?>" />

					<input maxlength="70" type="hidden" name="tax_pph[]" class="form-control tax_pph"
					value="<?php echo number_format($row2->tax_detailpph,2) ?>" />
				</td>
			</tr>
			<?php }} else { 
			  foreach($tampil_ada_det_grn as $row3){ 
			  $ino++ ; // buat  perulangan tampilan detail 
			  $remaining_price = $row3->qty_outstanding * $row3->price;
			  $tax_ppn = $row3->tax_ppn;
			  $tax_pph = $row3->tax_pph;
			  $price = $row3->price;

			  if($tax_ppn == '' || $tax_ppn == '0.00' || $tax_ppn == '0') {
				  $type_ppn = $row3->type_ppn;

				  if($type_ppn == 'PPN10%') {
					$ppn_edit = $price * (10 / 100);
				  } else if($type_ppn == 'PPN1%') {
					$ppn_edit = $price * (1 / 100);
				  } else if($type_ppn == 'NONE' || $type_ppn == '') {
					$ppn_edit = $price * (0 / 100);
				  }
			  } else {
				$ppn_edit = $tax_ppn;
			  }
			  ?>
			<tr>
				<td class="no" align="center"><?php echo $ino ;?></td>
				<td>
					<input type="text" name="flagstat[]" value="1" hidden="hide" />
					<input type="text" id="txtidmasdetid" name="txtidmasdet[]" maxlength="25"
						value="<?php echo $row3->id_master ?>" hidden="hide" />
					<input type="text" name="txtiddet[]" value="<?php echo $row3->id_detail ?>" class="txtid"
						hidden="hide" />
					<input type="text" maxlength="70" id="txtdesc" name="txtdesc[]" placeholder="Item Type" size="24"
						class="form-control txtdesc" value="<?php echo $row3->description ?>" readonly />
				</td>
				<td><input type="text" maxlength="70" name="txtspecs[]" placeholder="Description" size="30"
						class="form-control txtspecs" value="<?php echo $row3->spec ?>" readonly /></td>
				<!-- <td hidden="hide">
            <input type="text" maxlength="70"  name="txtporeff[]"  placeholder="Invoice No" size="30" class="form-control txtporeff" value="<?php //echo $row2->po_reff ?>"  />
            </td> -->
				<!--  <td hidden="hide">
            <div class="form-group">           
            <div class="col-xs-11">
            <input data-rel="tooltip" type="text" name="cboact_search[]" placeholder="Actifity No" title="<script>Hello Put like : Actifity number </script>" data-placement="bottom" class="col-md-12 cboact_search_data" size="30" maxlength="40" value="<?php //echo $row2->no_act ?>"  />
            </div>
            </div>       
            </td>-->
				<!--<td>
            <div class="form-group">           
            <div class="col-xs-11">
            <input data-rel="tooltip" type="text" name="cbocoa_search[]" placeholder="C.O.A Number /C.O.A Desc" title="Hello Put like : Coa Number /C.O.A Desc " data-placement="bottom" class="col-md-12 cbocoa_search_data" size="40" maxlength="40" value="<?php //echo $row2->coa ?>"  />
            </div>
            </div>   
            </td>  -->
				<td><input maxlength="6" style="text-align: center;" name="txtqty[]" type="text" placeholder="0" size="8"
						onkeypress="return isNumberKey(event)" class="form-control quantity_edit" id="txtqty"
						value="<?php echo number_format($row3->qty_begin) ?>" readonly /></td>
				<td><input maxlength="14" style="text-align: right;" name="txtprice[]" type="text" placeholder="0.00" size="30"
						onkeypress="return isNumberKey(event)" class="form-control price_edit" id="txtprice"
						value="<?php echo number_format($row3->price,2) ?>" readonly /></td>
				<td><input maxlength="70" style="text-align: right;" name="txtamount[]" placeholder="0.00" size="30"
						class="form-control amount_edit" readonly="readonly"
						value="<?php echo number_format($row3->amount,2) ?>" readonly /></td>
				<input maxlength="70" type="hidden" name="cbotax[]" class="form-control txtstat_edit"
					value="<?=trim($row3->type_ppn);?>" />
				<input maxlength="70" type="hidden" name="cbotaxpph[]" class="form-control txtstatpph_edit"
					value="<?=trim($row3->type_pph);?>" />
				<input maxlength="70" type="hidden" name="txttaxcharge[]" class="form-control taxcharge_edit"
					value="<?php echo number_format($ppn_edit,2) ?>" />
				<input maxlength="70" type="hidden" name="txttaxchargepph[]" class="form-control taxchargepph_edit"
					value="<?php echo number_format($row3->tax_pph,2) ?>" />

				<input name="txtqtyreceivedbefore[]" type="hidden" class="form-control quantityreceivedbefore_edit"
					id="txtqtyreceivedbefore" value="<?php echo number_format($row3->qty_received)?>" />
				<input name="txtqtyoutstandingbefore[]" type="hidden"
					class="form-control quantityoutstandingbefore_edit" id="txtqtyoutstandingbefore"
					value="<?php echo number_format($row3->qty_outstanding)?>" />
				<input name="txtamountreceivedbefore[]" type="hidden" class="form-control amountreceivedbefore_edit"
					id="txtamountreceivedbefore" value="<?php echo number_format($row3->amount_received)?>" />
				<td><input maxlength="6" style="text-align: center;" name="txtqtyreceived[]" type="text" placeholder="0" size="8"
						onkeypress="return isNumberKey(event)" class="form-control quantityreceived_edit"
						id="txtqtyreceived" value="<?php echo number_format($row3->qty_received)?>" readonly /></td>
				<td hidden><input maxlength="70" style="text-align: right;" name="txtamountreceived[]" placeholder="0.00" size="30"
						class="form-control amountreceived_edit" readonly="readonly" value="0" readonly /></td>
				<td><input maxlength="6" style="text-align: center;" name="txtqtyoutstanding[]" type="text" placeholder="0" size="8"
						onkeypress="return isNumberKey(event)" class="form-control quantityoutstanding_edit" id="txtqtyoutstanding"
						value="<?php echo number_format($row3->qty_outstanding)?>" readonly />
				</td>
				<td>
					<input maxlength="6" style="text-align: center;" name="txtqtyin[]" type="text" placeholder="0" size="8"
						onkeypress="return isNumberKey(event)" class="form-control quantityin_edit qty_validation" id="txtqtyin"
						value="0" <?php if($row3->qty_outstanding == '0') { echo 'readonly'; } ?> />
				</td>
				
	
				<td>
			
					<input maxlength="70" style="text-align: right;" name="txtamountin[]" placeholder="0.00" size="30"
						class="form-control amountin_edit" readonly="readonly"
						value="0" readonly />
	
				</td>

				<td>
						<input maxlength="70" style="text-align: center;" name="txtremainingprice[]" type="text" placeholder="0" size="30"
						onkeypress="return isNumberKey(event)" class="form-control remainingprice"
						id="txtremainingprice" value="<?php echo number_format($remaining_price, 2, '.', ',') ?>" readonly />
						<input type="hidden" value="<?php echo number_format($remaining_price, 2, '.', ',') ?>" name="txtremainingpricehide[]" class="form-control remainingpricehide">	
				</td>

				<td><input maxlength="70" style="text-align: right;" name="txttaxchargein[]" placeholder="0.00" size="30"
						class="form-control taxchargein_edit" readonly="readonly" value="0" /></td>
				<td hidden><input maxlength="70" style="text-align: right;" name="txttaxchargepphin[]" placeholder="0.00" size="30"
						class="form-control taxchargepphin_edit" readonly="readonly" value="0" /></td>
				<td><button class="btn-app btn-disabled btn-sm radius-6 remove" disabled><i
							class="glyphicon glyphicon-remove"></i></button></td>

				<td>
				<input maxlength="70" type="hidden" name="tax_ppn[]" class="form-control tax_ppn_edit"
					value="<?php echo number_format($ppn_edit,2) ?>" />

					<input maxlength="70" type="hidden" name="tax_pph[]" class="form-control tax_pph_edit"
					value="<?php echo number_format($row3->tax_pph,2) ?>" />
				</td>

			</tr>
			<?php }} ?>

		</tbody>


	</table>
</div>


<div class="">
	<table class="btn-warning" hidden="hide">
		<tr>
			<td width="72%"><label>No Total Tax :</label></td>
			<td align="center" style="padding:10px">
				<input type="checkbox" id="chkppn" name="chkppn" class="ppnnot ace ace-checkbox-1 col-xs-3" />
				<span class="lbl"></span>
			</td>
		</tr>
	</table>

	<table class="btn-danger" style=" color:#FFF;">


		<tr>
			<td colspan="0" width="25%">
				<label> Sub Total : </label>
			</td>
			<td width="40%">
				<input maxlength="70" style="text-align: right;" id="result" name="result" class="form-control total" readonly="readonly"
					style="color:#900" value="0.00" />
			</td>

		</tr>
		<tr>
			<td colspan="0" width="25%">
				<label> Total Tax PPN: </label>
			</td>
			<td width="40%">
				<input maxlength="70" style="text-align: right;" id="txtppn" name="txtppn" class="form-control ppncalc" readonly="readonly"
					style="color:#900" value="0.00" />
			</td>

		</tr>

		<tr>
			<td colspan="0" width="25%">
				<label> Total Tax PPH: </label>
			</td>
			<td width="40%">
				<input maxlength="70" style="text-align: right;" id="txtpph" name="txtpph" class="form-control pphcalc" readonly="readonly"
					style="color:#900" value="0.00" />
			</td>

		</tr>

		<tr>
			<td colspan="0" width="25%">
				<label> Grand Total + TAX </label>
			</td>
			<td width="40%">
				<input maxlength="70" style="text-align: right;" id="gpppn" name="gpppn" class="form-control grandppn" readonly="readonly"
					style="color:#900" value="0.00" />
			</td>
		</tr>
	</table>
</div>


     
    <!-- <table class="btn-danger" width="40%" style=" color:#FFF;"  >
    	<tr>              
            <td colspan="0" width=""><label> Sub Total : </label></td>
            <td><input  maxlength="70" id="result" name="result" class="form-control total" readonly="readonly" style="color:#900" value="0"/></td>
        </tr>
        <tr>
            <td colspan="0" width=""><label> Total Tax PPN: </label></td>
            <td><input  maxlength="70" id="txtppn" name="txtppn" class="form-control ppncalc" readonly="readonly" style="color:#900" value="0" /></td>
        </tr>
        <tr>
            <td colspan="0" width=""><label> Total Tax PPH: </label></td>
            <td><input  maxlength="70" id="txtpph" name="txtpph" class="form-control pphcalc" readonly="readonly" style="color:#900" value="0" /></td>
        </tr>
        <tr>
            <td colspan="0" width="31%"><label> Grand Total + TAX  </label></td>
            <td><input  maxlength="70" id="gpppn" name="gpppn" class="form-control grandppn" readonly="readonly" style="color:#900" value="0" /></td>
        </tr>
    </table>    -->
<br />
	<table>
        <tr>
            <td><button class="btn btn-app btn-info btn-xs radius-4 cbtnsavebtn" type="button" id="btnsavebtn" name="btnsavebtn"  value ="btnsavebtn"  >
            <i class="ace-icon fa fa-book sm-160"></i>Submit</button>    
            </td>                 
            <td>&nbsp;</td>
            <td><a href="<?php echo base_url('fpbpo_form/C_print_fpb_po');?>" style="text-decoration:none;" class="btn btn-app btn-success btn-xs radius-4 btnback"> <i class="ace-icon fa fa-exchange bigger-160"></i>Back</a>                          </td>
        </tr>
    </table>                                                	
	<p>
  </form>
	<?php if ($this->session->flashdata('pesan_succces') !="") {	 
	echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	 }?>                    
  	<!-- Modal -->
</p>
        <div class="modal fade" id="ViewloadinglModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">                                        
                    <div class="modal-body" align="center">
                            <img src="<?php echo base_url('assets/img/loading_spinner.gif') ; ?>" />
                    </div>  
                </div>
            </div>
        </div>
<!-- Modal -->

<!-- combobox autocomplete -->
<script type="text/javascript">
      $(".myselect").select2();

	  function checkSize(e) {
		  	var size = e.files[0].size;
		  	var ext = e.files[0].type;

			if (ext != 'application/pdf') {
				$('#alert-msg').html('Your chosen file is not PDF, please choose PDF !');
				$('#btnsavebtn').prop('disabled', true);
				return false;
			} else {
				if (size > 2097152) {
					$('#alert-msg').html('File size is more than 2MB ! <br> Please change another file under 2MB');
					$('#btnsavebtn').prop('disabled', true);
					return false;
				} else {
					$('#alert-msg').html('');
					$('#btnsavebtn').prop('disabled', false);
					return true;
				}
			}			
	  }
</script>   
<!-- combobox autocomplete end a--> 