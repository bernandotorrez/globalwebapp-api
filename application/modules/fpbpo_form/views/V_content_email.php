
<h2>
	<?php
	    $strcom = $this->session->userdata('company');
		$strbranch = $this->session->userdata('name_branch');
        $strdept = $this->session->userdata('dept');
        $submission = $data_request->user_submission;
		 
	    echo  'Company : '.$strcom. " | " ." Location : " .$strbranch. " | " ."Dept : ".$strdept." | Submission : ".$submission;
	 ?>
</h2>

<a href="<?=base_url('');?>">Link Address</a>

<h2>Detail E-Purchase</h2>
<table width="70%" border="1">  
 
    <tr >
      <td >No PP :</td>
      <td><?=$data_request->id_master;?></td>
    </tr>
    <tr >
      <td>Requester Name :</td>
      <td><?=$data_request->user_submission;?></td>
    </tr>
     <tr >
      <td >Item Type :</td>
      <td><?=$data_request->header_desc;?></td>
    </tr>
    <tr >
        <td>Vendor :</td>
        <td><?=$data_request->vendor;?></td>
    </tr>    
    <tr>   
        <td>Date PP :</td>
        <td><?=$data_request->date_pp;?></td>
    </tr>
    <tr>
       <td>Currency :</td>
        <td><?=$data_request->currency;?></td>
    <tr>  
       <td>Purchase Type :</td>
        <td><?=$data_request->type_purchase;?></td>
    </tr>
    <tr>  
       <td>Total :</td> 
        <td><?=number_format($data_request->gran_total,2,'.',',');?></td>
    </tr>
     <tr>
       <td>Total + PPN :</td>
        <td><?=number_format($data_request->gran_totalppn,2,'.',',');?></td>  
    </tr>
    <tr>
       <td>Status PO :</td>
        <td><b><?=$status_po;?></b></td>  
    </tr>
  
</table>