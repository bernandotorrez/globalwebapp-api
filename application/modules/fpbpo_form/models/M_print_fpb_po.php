<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_print_fpb_po extends CI_Model {

    public $db_tabel = 'qv_head_pp_complite';

 
	//-------------------function untuk cari dan narik table report fpb
	function select_data_rpt_fpb()
	{
	  $str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete		
	  $strflag_adjust = "0";		
	  $strid_master =$this->input->post('msg'); 	 	  						  					    						   	 			   	   					  				   			  			   			
	 	for ($i=0; $i < count($strid_master) ; $i++) { 				
			  $query = $this->db->select("desc,spec,qty,harga,total,vendor,currency,gran_total,ppn,pph,gran_totalppn,counter_reprint")
								->where('id_master',$strid_master[$i])
								->where('flagadjust',$strflag_adjust)
								->where('status',$str_status_terdelete)
								->get('qv_master_det_pp');
			  if ($query->num_rows() >= 1)
			  {		
			  	 foreach ($query->result() as $row)
				 {
			  		$data = array('sesidmaster' => $strid_master[$i],							
								  'ses_vendor' =>  $row->vendor,
								  'ses_grand'   => $row->gran_total,
								  'counter_reprint' => $row->counter_reprint,								  
								  ); //buat array di session		
					$this->session->set_userdata($data); //simpan di session
				 }
											 
					 
					 return $query->result();										 
			  }else{
					 return false;
			  }
	   	}	
	   
	 }
	 
	//-------------------function untuk cari dan narik table lalucetak PO 
	//-------------------function untuk cari dan narik table lalucetak PO 
	function select_data_rpt_po() //model untuk print po
	{			
	  $strid_master =$this->input->post('btnprintpo'); 	 	  						  					    						   	  $str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete
	  $strflag_adjust = "0";
	  $strflag_beforeprintpo = "0" ; 
	   			   	   					  				   			  			   			
			 
					  $query = $this->db->select("desc,spec,qty,quo_reff,harga,total,vendor,alamat,notelp,fax,currency,gran_total,ppn,pph,gran_totalppn")
										->where('id_master',$strid_master)
										->where('status',$str_status_terdelete)
										->where('flagadjust',$strflag_adjust)
										->get('qv_master_det_pp');
										
					  if ($query->num_rows() >= 1)
					  {						
						 foreach ($query->result() as $row)
						 {
							$data = array('sesidmaster' => $strid_master,							
										  'ses_vendor' =>  $row->vendor,
										  'ses_alamat_ven' =>  $row->alamat,
										  'ses_notelp_ven' =>  $row->notelp,
										  'ses_fax_ven' =>  $row->fax,
										  'ses_grand'   => $row->gran_total,
										  'ses_ppn'   => $row->ppn,
										  'ses_gran_totalppn'   => $row->gran_totalppn,	
										  ); //buat array di session		
							$this->session->set_userdata($data); //simpan di session
						 }
							  //Update flag p.o yg sedang di cetak ------------------------------------	
							  $data_flag =array("flag_print_po" => "1", //give flag_print_po
												"flag_po" => "1",									
												);	
							  $this->db->where('id_master',$strid_master);
							  $this->db->update('tbl_master_pp',$data_flag);  				
							  //---------------------------------------------------------------------																	
							  return $query->result();										 
					  }else{
							  return false;
					  }
			  
	  
	 } 
	
	 function check_flag_po()// validasi untuk ceatk fpb jika nilai flag = 1 sudah tidak bisa di cetak lagi 
	{
		  $strid_master =$this->input->post('btnprintpo'); 		
		  $str_give_flag_po= "1";	  						  					    						   	 					  				   			  			   					
		  $query = $this->db->select("id_master") 
							->where('id_master',$strid_master)
							->where('flag_print_po',$str_give_flag_po)
							->get('tbl_master_pp');
		  if ($query->num_rows() > 0)
		  {					  
			return true;				
		  }	  
			
	}	 
	
	
    function check_flag_fpb()// validasi untuk ceatk fpb jika nilai flag = 1 sudah tidak bisa di cetak lagi 
	{
		 $strid_master =$this->input->post('msg'); 		
		 $str_give_flag_fpb= "1";	  						  					    						   	 			   	   					  				   			  			   			
			for ($i=0; $i < count($strid_master) ; $i++) { 				
				  $query = $this->db->select("id_master") 
									->where('id_master',$strid_master[$i])
									->where('flag_print_fpb',$str_give_flag_fpb)
									->get('qv_master_det_pp');
				  if ($query->num_rows() >= 1)
				  {					  
					return true;				
				  }
				  
			}
	}	

	function get_ada_grn()
	{	
		$get_edit_nopo =$this->input->post('btngrn'); 	 	  						  					    	        $str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete
	    $strflag_adjust = "0";
	    $strflag_po= "0" ; 	
  
		 if (isset($get_edit_nopo) && trim($get_edit_nopo!=''))
		 {		
		  	//for ($i=0; $i < count($get_edit_nopo) ; $i++) { 	
			   $this->db->select("*")	;
			   // $this->db->where('id_master', $get_edit_nopo[$i]);	
			   $this->db->where('id_master',$get_edit_nopo);									  	
			   $result = $this->db->get('tbl_master_grn');							    				
		   //}						
			  return $result->result() ;									  		  
		  }else{
			   return false;
		  } 
		
	}

	function get_ada_det_grn()
	{		
	 $get_edit_nopo =$this->input->post('btngrn'); 
			  
	  if (isset($get_edit_nopo) && trim($get_edit_nopo!=''))
	  {		
				
		$query = $this->db->select("*") 
						  ->where('id_master',$get_edit_nopo)
						  ->order_by('number_grn', 'DESC')
					      ->get('tbl_detail_grn');
			if ($query->num_rows() >= 1){
				//echo ("test");die();
				foreach ($query->result() as $row){
					$sqlqtygrn = "select * from tbl_detail_grn where number_grn='".$row->number_grn."' order by id_det_received desc";
					$qryqtygrn = $this->db->query($sqlqtygrn);
					$adaqtygrn = $qryqtygrn->result();
					return $adaqtygrn;
					}
			}								  		  
	   }else{
			return false;
	   } 
	}


	
	function get_input_grn()
	{		
	  $get_edit_nopo =$this->input->post('btngrn');
	  $flag_status_mas = "1" ;
			  
	  if (isset($get_edit_nopo) && trim($get_edit_nopo!=''))
	  {		
		  for ($i=0; $i < count($get_edit_nopo) ; $i++) { 	
			 $this->db->select("*")	;
			 $this->db->where('id_master', $get_edit_nopo);
			 $this->db->where('status',  $flag_status_mas);											  	
			 $result = $this->db->get('qv_head_pp_complite');							    				
		   }						
			 return $result->result() ;									  		  
	  }else{
		   return false;
	  }
		
	}
	
	function m_add_flag_po()
	{
		$get_edit_nopo =$this->input->post('msg');
		//alert($get_edit_nopo);die();
		$str_give_flag_po= "1";
		$str_give_flag_print_po= "1";  	
		$data_flag =array("flag_po" => $str_give_flag_po,
						  "flag_print_po" => $str_give_flag_print_po,							
						);	
		$this->db->where('id_master',$get_edit_nopo[$i]);
		$this->db->update('tbl_master_pp',$data_flag); 
	}

	function get_vendor()
	{
		 $id_company = $this->session->userdata('id_company');
	     $id_dept = $this->session->userdata('id_dept');
		 $status_del = "1"; //flag status delete pada table vendor
		 		 		 
	   $this->load->database();		
		 $this->db->select('id_vendor,vendor');		
		 $this->db->where('id_company',$id_company);
		 $this->db->where('id_dept',$id_dept);
		 $this->db->where('status',$status_del);
		 $result = $this->db->get('tbl_vendor');			     		 
		 return $result->result();	 	
	}
	
	function get_currency()
	{		
	   $this->load->database();		
		 $this->db->select('id_curr,currency');				
		 $result = $this->db->get('tbl_curr');			     		 
		 return $result->result();	 	
	}	

	function get_purchase_type()
	{	
	   	 $id_user = $this->session->userdata('id');
		 $query =$this->db->query("select status_pur,id from tbl_user where id ='".$id_user."'") ;
		 $hasil =$query->result();
		 
		 foreach($hasil as $row) {
			 $cath_status_pur = $row->status_pur;
		 }
		 
		  $saparate_typepur = explode(",",$cath_status_pur );			 					 		 		   	      
		  $this->db->select('*');
			 if (count($saparate_typepur)=="5")	{	
									 
				 $this->db->where('id_flagpur',$saparate_typepur[0]);				
				 $this->db->or_where('id_flagpur',$saparate_typepur[1]);	
				 $this->db->or_where('id_flagpur',$saparate_typepur[2]);			 
				 $this->db->or_where('id_flagpur',$saparate_typepur[3]);		  								
				 $this->db->or_where('id_flagpur',$saparate_typepur[4]);	
			}else{
				if (count($saparate_typepur)=="4")	{				
					 $this->db->select('id_flagpur','flag,type_purchase');	 
					 $this->db->where('id_flagpur',$saparate_typepur[0]);				
					 $this->db->or_where('id_flagpur',$saparate_typepur[1]);	
					 $this->db->or_where('id_flagpur',$saparate_typepur[2]);			 
					 $this->db->or_where('id_flagpur',$saparate_typepur[3]);		  											
				}else{
					if (count($saparate_typepur)=="3")	{	
				
						 $this->db->select('id_flagpur','flag,type_purchase');	 
						 $this->db->where('id_flagpur',$saparate_typepur[0]);				
						 $this->db->or_where('id_flagpur',$saparate_typepur[1]);	
						 $this->db->or_where('id_flagpur',$saparate_typepur[2]);			 					
					}else{	
						if (count($saparate_typepur)=="2")	{	
											
						   $this->db->select('id_flagpur','flag,type_purchase');	 
						   $this->db->where('id_flagpur',$saparate_typepur[0]);										                           $this->db->or_where('id_flagpur',$saparate_typepur[1]);								 				
						}else{								
							$this->db->select('id_flagpur','flag,type_purchase');
							$this->db->where('id_flagpur',$saparate_typepur[0]);											 			    	    }
					}
				}
			}				   			 									
		 $result = $this->db->get('tbl_type_purchase'); 		     		 
		 return $result->result();	  
	}	

	function get_input_detail_grn()
	{		
		 
	  $get_edit_nopo =$this->input->post('btngrn');
	 //echo ($get_edit_nopo); die();	  		 				    							   				      	      
	  $flag_status_det = "1" ;
	    	   					  				   			  			   	 		  
	  if (isset($get_edit_nopo) && trim($get_edit_nopo!=''))
	  {		
		$query = $this->db->select("*") 
						  ->where('id_master',$get_edit_nopo)
					      ->get('tbl_detail_grn');
			if ($query->num_rows() >= 1){
				//echo ("test");die();
				foreach ($query->result() as $row){
					$sqlqtygrn = "select * from tbl_detail_grn where id_detail='".$row->id_detail."' order by id_det_received desc limit 1";
					$qryqtygrn = $this->db->query($sqlqtygrn);
					$adaqtygrn = $qryqtygrn->result();
					return $adaqtygrn;
					}
			}
			else{
				$this->db->select("id_detail,id_master,desc,spec,qty,harga,total,po_reff,coa,no_act,tax_type,tax_typepph,tax_detail,tax_detailpph")	;
				$this->db->where('id_master', $get_edit_nopo);	
				$this->db->where('status', $flag_status_det);										  	
				$result = $this->db->get('tbl_detail_pp');							    								
				return $result->result() ;
			}							  		  
	  }else{
		  return false;
	  }
		
	}
	
	function give_right_fpb_print()
	{
					
	  $strid_master =$this->input->post('msg'); 	 	  						  					    						   	 			   	   					  				   			  			   			
	 	for ($i=0; $i < count($strid_master) ; $i++) { 				
			  $query = $this->db->select("id_master,desc,spec,qty,harga,total,vendor,currency,ppn,pph,gran_totalppn,gran_total")
								->where('id_master',$strid_master[$i])
								->get('qv_master_det_pp');
			  if ($query->num_rows() >= 1)
			  {				
			    
				  foreach ($query->result() as $row)
				  {
			  		$data_right = array('ses_nopp' => $strid_master[$i]); 																							  					$this->session->set_userdata($data_right); //simpan di session
				  }	  								  
			     							  
				 //Update flag fpb yg sedang di cetak ------------------------------------
				  $str_give_flag_fpb= "0"; //0 jika di berikan izin untuk print ulang		
				  $data_flag =array("flag_print_fpb" => $str_give_flag_fpb, //give flag_print_fpb										
									);	
				  $this->db->where('id_master',$strid_master[$i]);
				  $this->db->update('tbl_master_pp',$data_flag); 				
				 //---------------------------------------------------------------------								 
					 
					 return $query->result();										 
			  }else{
					 return false;
			  }
	   	}	
	   
	 }		
 
 function m_mastergetid_pp()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('id_master,gran_total,ppn,pph,gran_totalppn,currency');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('qv_head_pp_complite');		
		return $result->result();
		
	}	   	
	
	function m_master_pp()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('*');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_master_pp');		
		return $result->result();
		
	}	 

	function m_getid_pp()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('*');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_detail_pp');		
		return $result->result();
		
	}

	function m_getgrnpo()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('*');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','0');	//aktiff	
		$result = $this->db->get('tbl_master_grn');		
		return $result->result();
	}

	function getNoPO()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('id_master, po_no_created');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_master_pp');		
		return $result->result();
	}

	function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}

	function getDataMasterPP($id_master) {
		$this->db->select('*');
		$this->db->where('id_master',$id_master);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('qv_head_pp_complite');		
		return $result->result();
	}

	function add_grn()	
	{
		//master---------------------------------------post		  
		  $txtmasterid = $this->input->post('txtnopp');
		  $txtnumbergrn = $this->input->post('txtnogrn');  	
		  $txtsub	= $this->input->post('txtsub');		
		  $txtvendor = $this->input->post('txtvendor'); 	
		  $txtcurr = $this->input->post('txtcurr'); 
		  $txtremarks = $this->input->post('txtremarks');
		  $full_receive = $this->input->post('full_receive');
		  $number_pay = $this->input->post('number_pay');
		  $remaining_price = $this->input->post('txtremainingprice');
		  $do_number = $this->input->post('do_number');

		  // Upload pdf
		  $config['upload_path']          = './asset/upload_do';
		  $config['allowed_types']        = 'pdf';
		  $config['max_size']             = 5000;

		  // Set file name upload
		  $config['file_name'] = str_replace('/','-',$txtmasterid);
		  $upload_vin = $config['file_name'].'.pdf';

		  $this->load->library('upload', $config);
		  $this->upload->initialize($config);
		  
		  if ( ! $this->upload->do_upload('userfile')) {
            
            $message_upload = array('error' => $this->upload->display_errors());
            $status_upload = 'gagal';

            $callback = array('status_upload' => $status_upload,
                                'message_upload' => $message_upload,
                                'status_insert' => 'Insert Gagal'
                            );

			return json_encode($callback);
			die();
        } else {

			

		  /* kalo $full_receive = 0, itu artinya sudah di receive semua, 
		  saya ambil dari qty outstanding #Bernand */
		  if($full_receive == '0') {
				$full_receive = '1';
		  } else {
				$full_receive = '0';
		  }

		  $data_update_flag_receive = array('flag_recived' => $full_receive);

		  $this->db->where('id_master', $txtmasterid);
		  $this->db->update('tbl_master_pp',  $data_update_flag_receive);
  
		  //---------------------unformat grandtotal---------
			  $txt_grand_total =$this->input->post('result'); 
			  if ( strstr( $txt_grand_total, ',' ) ) $txt_grand_total = str_replace( ',','', $txt_grand_total );  
			  //-------------------------------------
			  
			   //---------------------unformat ppn---------
			  $txt_ppn =$this->input->post('txtppn'); 
			  if ( strstr( $txt_ppn, ',' ) ) $txt_ppn = str_replace( ',','', $txt_ppn );  
			  //-------------------------------------
			  
			    //---------------------unformat ppn---------
			  $txt_pph =$this->input->post('txtpph'); 
			  if ( strstr( $txt_pph, ',' ) ) $txt_pph = str_replace( ',','', $txt_pph );  
			  //-------------------------------------
			  
			   //---------------------unformat grandtotalppn---------
			  $txt_grand_totalppn =$this->input->post('gpppn'); 
			  if ( strstr( $txt_grand_totalppn, ',' ) ) $txt_grand_totalppn = str_replace( ',','', $txt_grand_totalppn );  
			  //-------------------------------------
		
		 		 
		  $date_send = date("Y-m-d H:i:s");
		  $txtmasterid = $this->input->post('txtnopp'); 			 
		  if (isset($txtmasterid) && trim($txtmasterid!=''))
		  {
			
			    $query = $this->db->select("id_master,number_pay") //check id booking pada table_parent 
									->where('id_master',$txtmasterid)
									->get('tbl_master_grn');
				  if ($query->num_rows() >= 1)
				  {	
					  //echo '431';die;
				    //master------------------------------
				  	// $data = array('total_amount'=>$txt_grand_total,
					// 				 'total_ppn'=>$txt_ppn,
					// 			   'total_pph'=>$txt_pph, 								  
					// 			   'gran_total'=>$txt_grand_totalppn	
					// 				);					
					// $this->db->where('id_master',$txtmasterid);
					// $this->db->update('tbl_master_grn',$data); 

					//detail---------------------------					
					$id_det = $this->input->post('txtiddet');

					if ($id_det != "" OR is_array($id_det)) {	
						$data = array('id_master'=>$txtmasterid,
									 'number_grn'=>$txtnumbergrn,
									 'date_grn'=>$date_send,
									 'user_submission'=>$txtsub,
									 'number_pay'=> 1,
									 'total_amount'=>$txt_grand_total,
									 'total_ppn'=>$txt_ppn,
								     'total_pph'=>$txt_pph, 								  
									 'gran_total'=>$txt_grand_totalppn,
									 'number_pay'=>	$number_pay,
									 'do_number' => $do_number,
									 'attach_do' => $upload_vin
									);					
						$this->db->insert('tbl_master_grn',$data);

						foreach($id_det AS $key => $val)
						{  			

							
								    
							$result[1] = array(	
								"id_master"     	=> $_POST['txtidmasdet'][$key],							   
								"id_detail"     	=> $_POST['txtiddet'][$key],										
								"description"   	=> $_POST['txtdesc'][$key],
								"spec"  		    => $_POST['txtspecs'][$key],		
								"qty_begin"  		=> str_replace(',','',$_POST["txtqty"][$key]),
								"price"  		    => str_replace(',','',$_POST["txtprice"][$key]),
								"amount"  		  	=> str_replace(',','',$_POST["txtamount"][$key]),
								"type_ppn"	    	=> $_POST['cbotax'][$key],	
								"type_pph"		  	=> $_POST['cbotaxpph'][$key],
								"tax_ppn"			=> str_replace(',','',$_POST["txttaxchargein"][$key]),
								"tax_pph"			=> str_replace(',','',$_POST["txttaxchargepphin"][$key]),					
								"qty_received"		=> str_replace(',','',$_POST["txtqtyreceived"][$key]),
								"qty_in" 			=> str_replace(',','',$_POST["txtqtyin"][$key]),
								"qty_outstanding"	=> str_replace(',','',$_POST["txtqtyoutstanding"][$key]),
								"amount_in"			=> str_replace(',','',$_POST["txtamountin"][$key]),
								"amount_received"	=> str_replace(',','',$_POST["txtamountreceived"][$key]),
								"number_grn"		=> $txtnumbergrn,
								"remaining_price"	=> str_replace(',','',$_POST["txtremainingprice"][$key]),
								
								);
								//echo($result[1]);die();												
								$this->db->insert_batch('tbl_detail_grn',$result,'id_detail');
	
						} 
					}			
					}else{
						//echo '441';die;
						$data = array('id_master'=>$txtmasterid,
									 'number_grn'=>$txtnumbergrn,
									 'date_grn'=>$date_send,
									 'user_submission'=>$txtsub,
									 'number_pay'=> 1,
									 'total_amount'=>$txt_grand_total,
									 'total_ppn'=>$txt_ppn,
								     'total_pph'=>$txt_pph, 								  
									 'gran_total'=>$txt_grand_totalppn,
									 'do_number' => $do_number,
									 'attach_do' => $upload_vin	
									);					
					$this->db->insert('tbl_master_grn',$data);

					//detail---------------------------					
					$id_det = $this->input->post('txtiddet');											
					if ($id_det != "") {	
						//echo '455';die;
						foreach($id_det AS $key => $val)
						{  							    
							if ($_POST['txtiddet'][$key]==""){ //jika hanya edit row detail 
								//echo 'parsial lebih 1';	die;
								 $result[1] = array(	
									"id_master"     	=> $_POST['txtidmasdet'][$key],							   
									"id_detail"     	=> $_POST['txtiddet'][$key],										
									"description"   	=> $_POST['txtdesc'][$key],
									"spec"  		    => $_POST['txtspecs'][$key],		
									"qty_begin"  		=> str_replace(',','',$_POST["txtqty"][$key]),
									"price"  		    => str_replace(',','',$_POST["txtprice"][$key]),
									"amount"  		  	=> str_replace(',','',$_POST["txtamount"][$key]),
									"type_ppn"	    	=> $_POST['cbotax'][$key],	
									"type_pph"		  	=> $_POST['cbotaxpph'][$key],
									"tax_ppn"			=> str_replace(',','',$_POST["txttaxchargein"][$key]),
									"tax_pph"			=> str_replace(',','',$_POST["txttaxchargepphin"][$key]),					
									"qty_received"		=> str_replace(',','',$_POST["txtqtyreceived"][$key]),
									"qty_in" 			=> str_replace(',','',$_POST["txtqtyin"][$key]),
									"qty_outstanding"	=> str_replace(',','',$_POST["txtqtyoutstanding"][$key]),
									"amount_in"			=> str_replace(',','',$_POST["txtamountin"][$key]),
									"amount_received"	=> str_replace(',','',$_POST["txtamountreceived"][$key]),
									"number_grn"		=> $txtnumbergrn,
									"remaining_price"	=> str_replace(',','',$_POST["txtremainingprice"][$key]),
									
									);
									//echo($result[1]);die();												
									$this->db->update_batch('tbl_detail_grn',$result,'id_detail');
							}else{  //jika edit row detail dan juga addnew rowdetail	
										//echo $_POST['cbotax'][$key];
										
										$result[1] = array(			
											"id_master"     	=> $_POST['txtidmasdet'][$key],							   
											"id_detail"     	=> $_POST['txtiddet'][$key],										
											"description"   	=> $_POST['txtdesc'][$key],
											"spec"  		    => $_POST['txtspecs'][$key],		
											"qty_begin"  		=> str_replace(',','',$_POST["txtqty"][$key]),
											"price"  		    => str_replace(',','',$_POST["txtprice"][$key]),
											"amount"  		  	=> str_replace(',','',$_POST["txtamount"][$key]),
											"type_ppn"	    	=> $_POST['cbotax'][$key],	
											"type_pph"		  	=> $_POST['cbotaxpph'][$key],
											"tax_ppn"			=> str_replace(',','',$_POST["txttaxchargein"][$key]),
											"tax_pph"			=> str_replace(',','',$_POST["txttaxchargepphin"][$key]),					
											"qty_received"		=> str_replace(',','',$_POST["txtqtyreceived"][$key]),
											"qty_in" 			=> str_replace(',','',$_POST["txtqtyin"][$key]),
											"qty_outstanding"	=> str_replace(',','',$_POST["txtqtyoutstanding"][$key]),
											"amount_in"			=> str_replace(',','',$_POST["txtamountin"][$key]),
											"amount_received"	=> str_replace(',','',$_POST["txtamountreceived"][$key]),
											"number_grn"		=> $txtnumbergrn,
											"remaining_price"	=> str_replace(',','',$_POST["txtremainingprice"][$key]),	
												  			
								 );									 
									$test= $this->db->insert_batch('tbl_detail_grn', $result);
							}
	
						} 
					}
				}
			} 

			$callback = array('status_upload' => 'success',
                                'message_upload' => 'success',
                                'status_insert' => 'Insert'
							);
							
			return json_encode($callback);
			die();
		}

		
				//return true;
	}
			
	public function giveRightPO($id_master, $data) {
		$this->db->where('id_master', $id_master);
        $query = $this->db->update('tbl_master_pp', $data);
        return $query;
	}

	public function closingPO($id_master, $data) {
		$this->db->where('id_master', $id_master);
        $query = $this->db->update('tbl_master_pp', $data);
        return $query;
	}

	public function getMasterGRN($number_grn) {
		$this->db->select('*');
		$this->db->where('number_grn',$number_grn);		
		//$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_master_grn');		
		return $result->result();
	}

	public function getCurrency($id) {
		$this->db->select('currency');
		$this->db->where('id_master',$id);		
		//$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('qv_head_pp_complite');		
		return $result->result();
	}

	public function getDetailGRN($number_grn) {
		$this->db->select('*');
		$this->db->where('number_grn',$number_grn);		
		//$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_detail_grn');		
		return $result->result();
	}

	public function getDetailPP($id_master) {
		$this->db->select('*');
		$this->db->where('id_master',$id_master);		
		//$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_detail_pp');		
		return $result->result();
	}

	public function giveRightFPB($number_grn, $data) {
		$this->db->where('number_grn', $number_grn);
        $query = $this->db->update('tbl_master_grn', $data);
        return $query;
	}

	public function updateFlagPO($id_master, $data) {
		$this->db->where('id_master', $id_master);
        $query = $this->db->update('tbl_master_pp', $data);
        return $query;
	}


	public function getDataMasterPPForPrint($id_master) {
		$this->db->select('*');
		$this->db->where('id_master',$id_master);		
		//$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('qv_head_pp_complite');		
		return $result->result();
	}

	public function getQuotation($id_master) {
		$this->db->select('attach_quo, attach_quo_purchase, attach_quo_purchase2, attach_quo_purchase3');
		$this->db->where('status', '1');
		$this->db->where('id_master', $id_master);
		$result = $this->db->get('qv_head_pp_complite');		
		return $result->result();
	}

	public function add_grn_new($data) {


		// Update flag recived
		$this->db->where('id_master', $data['txtnopp']);
		$upload_flag_reeived = $this->db->update('tbl_master_pp',  $data['data_update_flag_receive']);

		// Insert ke tbl_master_grn
		$data_insert_master_grn = array('id_master'		=>	$data['txtnopp'],
									 'number_grn'		=>	$data['txtnogrn'],
									 'date_grn'			=>	$data['date_send'],
									 'user_submission'	=>	$data['txtsub'],
									 'number_pay'		=> 	$data['number_pay'],
									 'total_amount'		=>	$data['txt_grand_total'],
									 'total_ppn'		=>	$data['txt_ppn'],
								     'total_pph'		=>	$data['txt_pph'], 								  
									 'gran_total'		=>	$data['txt_grand_totalppn'],
									 'number_pay'		=>	$data['number_pay'],
									 'do_number' 		=> 	$data['do_number'],
									 'attach_do' 		=> 	$data['attach_do']
									);					
		$insert_master_grn = $this->db->insert('tbl_master_grn',$data_insert_master_grn);

		// Insert ke tbl_detail_grn
		foreach($data['txtiddet'] AS $key => $val)
						{  			

							
								    
							$data_insert_detail_grn = array(	
								"id_master"     	=> $data['txtnopp'],							   
								"id_detail"     	=> $data['txtiddet'][$key],										
								"description"   	=> $data['txtdesc'][$key],
								"spec"  		    => $data['txtspecs'][$key],		
								"qty_begin"  		=> str_replace(',','',$data["txtqty"][$key]),
								"price"  		    => str_replace(',','',$data["txtprice"][$key]),
								"amount"  		  	=> str_replace(',','',$data["txtamount"][$key]),
								"type_ppn"	    	=> $data['cbotax'][$key],	
								"type_pph"		  	=> $data['cbotaxpph'][$key],
								"tax_ppn"			=> str_replace(',','',$data["txttaxchargein"][$key]),
								"tax_pph"			=> str_replace(',','',$data["txttaxchargepphin"][$key]),					
								"qty_received"		=> str_replace(',','',$data["txtqtyreceived"][$key]),
								"qty_in" 			=> str_replace(',','',$data["txtqtyin"][$key]),
								"qty_outstanding"	=> str_replace(',','',$data["txtqtyoutstanding"][$key]),
								"amount_in"			=> str_replace(',','',$data["txtamountin"][$key]),
								"amount_received"	=> str_replace(',','',$data["txtamountreceived"][$key]),
								"number_grn"		=> $data['txtnogrn'],
								"remaining_price"	=> str_replace(',','',$data["txtremainingprice"][$key]),
								
								);
								//echo($result[1]);die();												
								//return $this->db->insert_batch('tbl_detail_grn',$result,'id_detail');
	
								$insert_detail_grn = $this->db->insert('tbl_detail_grn',$data_insert_detail_grn);
						}

		$callback = array('status_upload' => 'success',
						'message_upload' => 'success',
						'status_update_flag_received' => $upload_flag_reeived,
						'status_insert_master_grn' => $insert_master_grn,
						'status_insert_detail_grn' => $insert_detail_grn
		);
					
		return $callback;
	}
				 
}