<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_print_fpb_po extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_print_fpb_po','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('terbilang');			
		//$this->load->library('fpdf');   		
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_print_fpb_po->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
			$this->disconnect();
		}					
	}		  	
		
  public function index()
  {					    																
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	 
	$dept  = $this->session->userdata('dept') ;	
	$branch  = $this->session->userdata('name_branch') ; 
	$company = $this->session->userdata('short') ;		
	$data['intno'] = ""; //variable buat looping no table.				
	$data['header'] ="P.O & Received , FPB Print"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;													
	$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
	$data['show_view'] = 'fpbpo_form/V_print_fpb_po';$this->load->view('dashboard/Template',$data);
  }
	 
	public function add_grn() //lakukan submit data GRN
{
		   if ($this->M_print_fpb_po->add_grn())
		   {
				 //redirect('fpbpo_form/c_print_fpb_po');
				 echo $this->M_print_fpb_po->add_grn();
				 die();
		   }else{
				 $this->session->set_flashdata('pesan_succces','Data  Update Failed!!!!!! ....');
		   }

}

    
     public function do_print_fpb()
  	{
		if ($this->M_print_fpb_po->check_flag_fpb())
		{   
		   $this->session->set_flashdata('pesan',"Data has been printed, Please contact your finance to give access for reprint FPB!!"); 		          
		   redirect('fpbpo_form/c_print_fpb_po');	   
		}else{
			if($this->M_print_fpb_po->select_data_rpt_fpb())
			 {
				 //------give flag already print FPB--------------------
				 $data_flag =array("flag_fpb" => '1', //give_flag_printfpb
									"flag_print_fpb" => '1', //give_flag_printfpb										
									"date_print_fpb" => date('Y-m-d')				
									);	 
				 $this->db->where('id_master',$this->session->userdata("sesidmaster"));
				 $this->db->update('tbl_master_pp',$data_flag);
				 //end---------------------------------------------------------------
				 
				 $hasil_fpb = $this->M_print_fpb_po->select_data_rpt_fpb();						
				 $res['currency'] = $hasil_fpb;				  
				 $res['data'] = $hasil_fpb;				  
				 $this->load->view('fpbpo_form/V_rptpdf_fpb',$res);
			  }else{     		
				 $this->session->set_flashdata('pesan_rpt','Data Permintaan Pembayaran Update Failed!!!!!! ....'); 											         
				 redirect('fpbpo_form/c_print_fpb_po');	
			 } 
		}
	}
	
	 public function do_print_po()	 
  	 {	
			if($this->M_print_fpb_po->check_flag_po()){
				 $this->session->set_flashdata('pesan_rpt','P.O Already Print !!!!!! ....'); 					
				 redirect('fpbpo_form/c_print_fpb_po');	
			}else{
				  if($this->M_print_fpb_po->select_data_rpt_po())
				  {  	
					 $res['data'] = $this->M_print_fpb_po->select_data_rpt_po();  
					 $this->load->view('fpbpo_form/V_rptpdf_po',$res);
				   }else{     	
					 $this->session->set_flashdata('pesan_rpt','Print P.O failed there find some errors !!!!!! ....'); 					
					 redirect('fpbpo_form/c_print_fpb_po');	
				  }
			 }
	 }
	 
	 
	  public function do_give_right_print_fpb()
      {		
		if($this->M_print_fpb_po->give_right_fpb_print())
		 {								    
		    $strnopp = $this->session->userdata('ses_nopp');
			$this->session->set_flashdata('pesan_give_right','PP No.'.$strnopp." ".'has been allow for reprint FPB!!'); 	
			$this->show_table(); //manggil fungsi show_table		
		  }else{     		
			 $this->session->set_flashdata('pesan_rpt','Give right Update Failed!!!!!! ....'); 					
			redirect('fpbpo_form/c_print_fpb_po');	
		  } 		 				 
  	  }	  
		 	 
    public function print_fpb_submit() //fungsi submit dengan banyak button dalam satu form
	{  	
		if ($this->input->post('btnprintfpb')){	
			$this->do_print_fpb();
		}else{
			if ($this->input->post('btnprintpo')){
				 $this->do_print_po();
			}else{
				        if ($this->input->post('btncari')){	
						    $this->do_search_data();
						}else{
							    if ($this->input->post('btngrn')){	
									$this->do_input_grn();
								}else{	
									    if ($this->input->post('btncaridate')){	
										    $this->do_search_date();
										}else{
											  if ($this->input->post('btngiveright')){	
												  $this->do_give_right_print_fpb();
											  }else{
													if ($this->input->post('btnsavebtn')){	
															 //$this->add_grn();
													}else{			
														redirect('fpbpo_form/c_print_fpb_po');	
														
													 }
										        }
									       }					
									}
							 } 
				    }
				}
		}
	
	public function get_data_grn()
	{
		$getdatamastergrn = $this->M_print_fpb_po->m_master_pp();
		$getdatadetailgrn = $this->M_print_fpb_po->m_getid_pp();

		//$data= $this->M_menu_user_grup->get_by_id($id);
		//echo json_encode($data);
		echo "<div>" ;
	  	echo "<tr style='font-weight:bold; font-size:11px'> ";
		echo "<td><label>PP No</label></td>";
		echo "<td><input class='form-control' id='id_master' name='id_master' type='text' value='".$getdatamastergrn[0]->id_master."'/></td>";
		echo "</tr>";
		echo "<tr style='font-weight:bold; font-size:11px'> ";
		echo "<td><label>User Submission</label></td>";
		echo "<td><input class='form-control' id='user_submission' name='user_submission' type='text' value='".$getdatamastergrn[0]->user_submission."'/></td>";
		echo "</tr>";
		echo "<tr style='font-weight:bold; font-size:11px'> ";
		echo "<td><label>Header Description</label></td>";
		echo "<td><input class='form-control' id='header_desc' name='header_desc' type='text' value='".$getdatamastergrn[0]->header_desc."'/></td>";
		echo "</tr>";
		echo "<tr style='font-size:12px'> ";
		echo "<td><label>Grand Total</label></td>";
		echo "<td><input class='form-control' id='gran_total' name='gran_total' type='text' value='".$getdatamastergrn[0]->gran_total."'/></td>";
		echo "</tr>";
		echo "</div>" ;

		$no="1";	 	
		echo "<div style='overflow-x:auto;'>" ;
		echo "<table class='table table-striped table-bordered table-hover'>";
		echo  "<tr style='font-weight:bold; font-size:11px'>";
		echo  "<td width='2%'>No</td>";
		echo  "<td width='2%'>Id Detail</td>";
		echo  "<td width='22%'>Item Type</td>";
		echo  "<td width='22%'>Description</td>";
		echo  "<td width='7%'>Qty</td>";
		echo  "<td width='15%'>Prices</td>";
		echo  "<td width='40%'>Total Prices</td>";
		echo  "<td width='40%'>Qty In</td>";
		echo  "</tr> "; 	 	 	 				
		 foreach ($getdatadetailgrn as $row_det_grn) {
			 echo  "<tr style='font-size:12px'> ";
			 echo '<td>'.$no++.'</td>';	
			 echo '<td>'.$row_det_grn->id_detail.'</td>';		
			 echo '<td>'.$row_det_grn->desc.'</td>';
			 echo '<td>'.$row_det_grn->spec.'</td>';
			 echo '<td>'.$row_det_grn->qty.'</td>';
			 echo '<td>'.number_format($row_det_grn->harga,2,'.',',').'</td>';
			 echo '<td>'.number_format($row_det_grn->total,2,'.',',').'</td>';
			 echo '<td><input class="form-control" id="qty_in" name="qty_in" type="text"/></td>';
			 echo "</tr>"; 							
			 
		 }
		
	}


	public function do_input_grn()
	 {
		$tampil_input_grn= $this->M_print_fpb_po->get_input_grn();
		$tampil_input_det_grn= $this->M_print_fpb_po->get_input_detail_grn();
		$tampil_ada_grn = $this->M_print_fpb_po->get_ada_grn();
		$tampil_ada_det_grn = $this->M_print_fpb_po->get_ada_det_grn();
	
		$tampil_vendor =$this->M_print_fpb_po->get_vendor();
		$tampil_cur = $this->M_print_fpb_po->get_currency();
	
	  	$dept  = $this->session->userdata('dept') ;
		$branch  = $this->session->userdata('name_branch') ;
		$company = $this->session->userdata('company') ;

		if ($tampil_input_grn == null and $tampil_input_det_grn ==null)
		{
			//echo("test");die();
			$data['ceck_row'] = "0"; // jika tidak ketemu
		    $data['tampil_pp'] =$tampil_input_grn;
			//$data['tampil_coa']=$this->M_create_pp->get_coa();
			$data['tampil_type_purchase']=$this->M_print_fpb_po->get_purchase_type();
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		    $data['pesan'] = 'Data Type Not Found';
		    $data['show_view']='fpbpo_form/V_form_input_grn';
		    $this->load->view('dashboard/Template',$data);
		 }else{
			$data['ceck_row'] = "1"; // jika  ketemu
		    $data['ino'] = ""; // variable kosong buat nocounter perulangan detail
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
			$data['tampil_pp'] =$tampil_input_grn;
			//$data['tampil_coa']=$this->M_create_pp->get_coa();
			$data['tampil_type_purchase']=$this->M_print_fpb_po->get_purchase_type();
			$data['tampil_det_pp']=$tampil_input_det_grn;
			$data['tampil_ada_grn']=$tampil_ada_grn;
			$data['tampil_ada_det_grn']=$tampil_ada_det_grn;
			$data['tampil_vendor']=	$tampil_vendor ;
		    $data['tampil_curr']= $tampil_cur;
			$data['show_view']='fpbpo_form/V_form_input_grn';
			$this->load->view('dashboard/Template',$data);
		 }

 }

	 public function add_flag_po()
	 {
		 //echo("test");
		 $addflagpo = $this->M_print_fpb_po->m_add_flag_po();
	 }
	 
	public function get_idtrans_modal()
    {		
     $tampungmaster = $this->M_print_fpb_po->m_mastergetid_pp();
	 $tampungdetail = $this->M_print_fpb_po->m_getid_pp();	 

	 $nopp= $this->input->post('id_pp');
	  
	 $no="1";	 	
	 echo  "<div style='overflow-x:auto'>";
	 echo  "<table class ='table table-striped table-bordered table-hover'>";
	 echo  "<thead>" ;
	 echo  "<tr style='font-weight:bold; font-size:11px'>";
	 echo  "<td width='2%'>No</td>";
	 echo  "<td width='22%'>Item Type</td>";
	 echo  "<td width='22%'>Description</td>";
	 echo  "<td width='10%'>P.O Ref</td>";
	 echo  "<td width='10%'>C.O.A</td>";
	 echo  "<td width='10%'>No Activity</td>";
	 echo  "<td width='7%'>Qty</td>";
	 echo  "<td width='15%'>Prices</td>";
	 echo  "<td width='40%'>Total Prices</td>";
	 echo  "<td width='7%'>PPN</td>";
	 echo  "<td width='7%'>PPH</td>";
	 echo  "<td width='25%'>PPN Amount</td>";
	 echo  "<td width='25%'>PPH Amount</td>";
	 echo  "</tr> "; 
	 echo  "</thead>";	 	 	 				
		foreach ($tampungdetail as $row_jq) {
		    echo  "<tr style='font-size:12px'> ";
			echo '<td>'.$no++.'</td>';			
			echo '<td>'.$row_jq->desc.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			echo '<td>'.$row_jq->po_reff.'</td>';
			echo '<td>'.$row_jq->coa.'</td>';
			echo '<td>'.$row_jq->no_act.'</td>';
			echo '<td align="center" style="text-align: center;">'.$row_jq->qty.'</td>';
			echo '<td align="right" style="text-align: right;">'.number_format($row_jq->harga,2,'.',',').'</td>';
			echo '<td align="right" style="text-align: right;">'.number_format($row_jq->total,2,'.',',').'</td>';
			echo '<td>'.$row_jq->tax_type.'</td>';
			echo '<td>'.$row_jq->tax_typepph.'</td>';
			echo '<td align="right" style="text-align: right;">'.number_format($row_jq->tax_detail,2,'.',',').'</td>';
			echo '<td align="right" style="text-align: right;">'.number_format($row_jq->tax_detailpph,2,'.',',').'</td>';
		    echo "</tr>"; 							
			
		}
				
	 foreach ($tampungmaster as $row_jm) {	
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Total (".$row_jm->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>".number_format($row_jm->gran_total,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPN Amount (".$row_jm->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>".number_format($row_jm->ppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 	
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPH Amount (".$row_jm->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>(".number_format($row_jm->pph,2,'.',',').")</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Grand Total (".$row_jm->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>".number_format($row_jm->gran_totalppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo "</table>";	
		 echo "</div>";
	 }

	 $data_attach = $this->M_print_fpb_po->getQuotation($nopp);
	 $data_attach = $data_attach[0];

	 if($data_attach->attach_quo != '') {
		$attach_quo = '<a id="attach_quo" onClick="PDFPopup(this)" req_id='.base_url($data_attach->attach_quo).' href="#" class="btn btn-app btn-primary btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a>';
	 } else {
		$attach_quo = '<font color="red">'."No Quotation".'</font>';
	 }

	 if($data_attach->attach_quo_purchase != '') {
		$attach_quo_purchase = '<a id="attach_quo_purchase" onClick="PDFPopup(this)" req_id='.base_url($data_attach->attach_quo_purchase).' href="#" class="btn btn-app btn-primary btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".' 1 '.'</a>';
	 } else {
		$attach_quo_purchase = '<font color="red">'."No Quotation Purchase".'</font>';
	 }

	 if($data_attach->attach_quo_purchase2 != '') {
		$attach_quo_purchase2 = '<a id="attach_quo_purchase2" onClick="PDFPopup(this)" req_id='.base_url($data_attach->attach_quo_purchase2).' href="#" class="btn btn-app btn-primary btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".' 2 '.'</a>';
	 } else {
		$attach_quo_purchase2 = '<font color="red">'."No Quotation Purchase 2".'</font>';
	 }

	 if($data_attach->attach_quo_purchase3 != '') {
		$attach_quo_purchase3 = '<a id="attach_quo_purchase3" onClick="PDFPopup(this)" req_id='.base_url($data_attach->attach_quo_purchase3).' href="#" class="btn btn-app btn-primary btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".' 3 '.'</a>';
	 } else {
		$attach_quo_purchase3 = '<font color="red">'."No Quotation Purchase 3".'</font>';
	 }

	 echo '<div class="modal-footer">
	 <div class="col-md-4 text-left">
	 Attached Quotation Requester: '.$attach_quo.'
	                        
	 </div>

	 <div class="col-md-6 text-left">
	 Attached Quotation Purchase: '.$attach_quo_purchase.'  '.$attach_quo_purchase2.'  '.$attach_quo_purchase3.'
	                      
	 </div>

	 <div class="col-md-2">
	 <button type="button" class="btn btn-app btn-primary btn-xs radius-8 btnclose" data-dismiss="modal">Close</button>                       
					
	 </div>

 </div>';
}	

function cath_data_fordatatables_po_backup(){
	//$str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
$str_idcompany = $this->session->userdata('id_company'); 
$str_iddept = $this->session->userdata('id_dept');
$access = $this->session->userdata('apps_accsess'); 
//$access = 0;
$str_status_send = "1";
$str_status_approved = "1";
/*Menagkap semua data yang dikirimkan oleh client*/


$draw=$_REQUEST['draw'];
$length=$_REQUEST['length'];
$start=$_REQUEST['start'];
$search=$_REQUEST['search']["value"];

//order short column
$column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top","flag_po");

/*Menghitung total qv didalam database*/
$this->db->select('id_master');
$this->db->from('qv_head_pp_complite');
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("id_company",$str_idcompany);
$this->db->where("id_dept",$str_iddept);
$this->db->where("aprove_bod","1");
$this->db->where("status","1");
$this->db->where("flag_po","0");
$this->db->where("flag_inv_receipt","0");

// query khusus Inv Receipt
$this->db->or_where("status_send_aprove",$str_status_send);
$this->db->where("id_company",'5');
$this->db->where("id_dept",'8');
$this->db->where("aprove_bod","1");
$this->db->where("status","1");
$this->db->where("flag_po","0");
$this->db->where("flag_inv_receipt","1");
// query khusus Inv Receipt

$this->db->like("id_master",$search);
$this->db->or_like("user_submission",$search);
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("id_company",$str_idcompany);
$this->db->where("id_dept",$str_iddept);
$this->db->where("aprove_bod","1");				
$this->db->where("status","1");
$this->db->where("flag_po","0");
$this->db->where("flag_inv_receipt","0");

// query khusus Inv Receipt
$this->db->or_where("status_send_aprove",$str_status_send);
$this->db->where("id_company",'5');
$this->db->where("id_dept",'8');
$this->db->where("aprove_bod","1");
$this->db->where("status","1");
$this->db->where("flag_po","0");
$this->db->where("flag_inv_receipt","1");
// query khusus Inv Receipt

$this->db->or_like("header_desc",$search);	
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("id_company",$str_idcompany);
$this->db->where("id_dept",$str_iddept);
$this->db->where("aprove_bod","1");
$this->db->where("status","1");
$this->db->where("flag_po","0");
$this->db->where("flag_inv_receipt","0");

// query khusus Inv Receipt
$this->db->or_where("status_send_aprove",$str_status_send);
$this->db->where("id_company",'5');
$this->db->where("id_dept",'8');
$this->db->where("aprove_bod","1");
$this->db->where("status","1");
$this->db->where("flag_po","0");
$this->db->where("flag_inv_receipt","1");
// query khusus Inv Receipt

$this->db->or_like("short",$search);	
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("id_company",$str_idcompany);
$this->db->where("id_dept",$str_iddept);
$this->db->where("aprove_bod","1");
$this->db->where("status","1");
$this->db->where("flag_po","0");
$this->db->where("flag_inv_receipt","0");

// query khusus Inv Receipt
$this->db->or_where("status_send_aprove",$str_status_send);
$this->db->where("id_company",'5');
$this->db->where("id_dept",'8');
$this->db->where("aprove_bod","1");
$this->db->where("status","1");
$this->db->where("flag_po","0");
$this->db->where("flag_inv_receipt","1");
// query khusus Inv Receipt

$this->db->or_like("vendor",$search);	
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("id_company",$str_idcompany);
$this->db->where("id_dept",$str_iddept);
$this->db->where("aprove_bod","1");
$this->db->where("status","1");
$this->db->where("flag_po","0");
$this->db->where("flag_inv_receipt","0");

// query khusus Inv Receipt
$this->db->or_where("status_send_aprove",$str_status_send);
$this->db->where("id_company",'5');
$this->db->where("id_dept",'8');
$this->db->where("aprove_bod","1");
$this->db->where("status","1");
$this->db->where("flag_po","0");
$this->db->where("flag_inv_receipt","1");
// query khusus Inv Receipt

$total = $this->db->count_all_results();

$output=array();
$output['draw']=$draw;
$output['recordsTotal']=$output['recordsFiltered']=$total;
$output['data']=array();

/*Jika $search mengandung nilai, berarti user sedang telah 
memasukan keyword didalam filed pencarian*/
if($search!=""){
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","0");
		$this->db->where("flag_status_inv_receipt","0");

		// query khusus Inv Receipt
		$this->db->or_where("status_send_aprove",$str_status_send);
		$this->db->where("flag_status_inv_receipt","1");
		$this->db->where("id_company",'5');
		$this->db->where("id_dept",'8');
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","1");
		// query khusus Inv Receipt

		$this->db->like("id_master",$search);
		$this->db->or_like("user_submission",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);		
		$this->db->where("aprove_bod","1");		
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","0");
		$this->db->where("flag_status_inv_receipt","0");

		// query khusus Inv Receipt
		$this->db->or_where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",'5');
		$this->db->where("id_dept",'8');
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","1");
		$this->db->where("flag_status_inv_receipt","1");
		// query khusus Inv Receipt

		$this->db->or_like("header_desc",$search);	
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","0");
		$this->db->where("flag_status_inv_receipt","0");

		// query khusus Inv Receipt
		$this->db->or_where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",'5');
		$this->db->where("id_dept",'8');
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","1");
		$this->db->where("flag_status_inv_receipt","1");
		// query khusus Inv Receipt

		$this->db->or_like("short",$search);	
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","0");
		$this->db->where("flag_status_inv_receipt","0");

		// query khusus Inv Receipt
		$this->db->or_where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",'5');
		$this->db->where("id_dept",'8');
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","1");
		$this->db->where("flag_status_inv_receipt","1");
		// query khusus Inv Receipt

		$this->db->or_like("vendor",$search);	
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","0");
		$this->db->where("flag_status_inv_receipt","0");

		// query khusus Inv Receipt
		$this->db->or_where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",'5');
		$this->db->where("id_dept",'8');
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","1");
		$this->db->where("flag_status_inv_receipt","1");
		// query khusus Inv Receipt
}

	/*Lanjutkan pencarian ke database*/
	$this->db->limit($length,$start);		
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("id_company",$str_idcompany);
	$this->db->where("id_dept",$str_iddept);			 
	$this->db->where("aprove_bod","1");
	/*Urutkan dari alphabet paling terkahir*/
		  $this->db->where("status","1");
		  $this->db->where("flag_po","0");
		  $this->db->where("flag_inv_receipt","0");
		  $this->db->where("flag_status_inv_receipt","0");

		  // query khusus Inv Receipt
		  $this->db->or_where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",'5');
		  $this->db->where("id_dept",'8');
		  $this->db->where("aprove_bod","1");
		  $this->db->where("status","1");
		  $this->db->where("flag_po","0");
		  $this->db->where("flag_inv_receipt","1");
		  $this->db->where("flag_status_inv_receipt","1");
		// query khusus Inv Receipt
	 $this->db->order_by('date_pp','DESC');
   
	//order column
	if (isset($_REQUEST["order"])){
		 $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
	}else{
		 $this->db->order_by('aprove_head','desc');
		 $this->db->order_by('approve_purchasing','desc');
		 $this->db->order_by('aprove_fc','desc');
		 $this->db->order_by('aprove_bod','desc');
	 }
 
	 $this->db->order_by('aprove_head','asc');
	 $this->db->order_by('approve_purchasing','asc');
	 $this->db->order_by('aprove_fc','asc');
	 $this->db->order_by('aprove_bod','asc');	
	 $this->db->order_by('date_send_aproval','DESC');  
	 $query=$this->db->get('qv_head_pp_complite');


/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
yang mengandung keyword tertentu
*/

if($search!=""){
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","0");
		$this->db->where("flag_status_inv_receipt","0");

		// query khusus Inv Receipt
		$this->db->or_where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",'5');
		$this->db->where("id_dept",'8');
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","1");
		$this->db->where("flag_status_inv_receipt","1");
		// query khusus Inv Receipt

		$this->db->like("id_master",$search);
		$this->db->or_like("user_submission",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);		
		$this->db->where("aprove_bod","1");		
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","0");
		$this->db->where("flag_status_inv_receipt","0");

		// query khusus Inv Receipt
		$this->db->or_where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",'5');
		$this->db->where("id_dept",'8');
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","1");
		$this->db->where("flag_status_inv_receipt","1");
		// query khusus Inv Receipt

		$this->db->or_like("header_desc",$search);	
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","0");
		$this->db->where("flag_status_inv_receipt","0");

		// query khusus Inv Receipt
		$this->db->or_where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",'5');
		$this->db->where("id_dept",'8');
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","1");
		$this->db->where("flag_status_inv_receipt","1");
		// query khusus Inv Receipt

		$this->db->or_like("short",$search);	
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","0");
		$this->db->where("flag_status_inv_receipt","0");

		// query khusus Inv Receipt
		$this->db->or_where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",'5');
		$this->db->where("id_dept",'8');
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","1");
		$this->db->where("flag_status_inv_receipt","1");
		// query khusus Inv Receipt

		$this->db->or_like("vendor",$search);	
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","0");
		$this->db->where("flag_status_inv_receipt","0");

		// query khusus Inv Receipt
		$this->db->or_where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",'5');
		$this->db->where("id_dept",'8');
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","0");
		$this->db->where("flag_inv_receipt","1");
		$this->db->where("flag_status_inv_receipt","1");
		// query khusus Inv Receipt
	   
		$jum=$this->db->get('qv_head_pp_complite');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
}





foreach ($query->result_array() as $row_tbl) {
	
	If ($row_tbl['attach_quo'] != "")
	 {  
	  $attach_quo = '<div  align="center"><a onClick="PDFPopup(this)" req_id='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
	 }else{
	   $attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
	 }

  
  
	   $btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-list" aria-hidden="true"></i></a></center>';//button detail
  
   If ($row_tbl['aprove_head'] == "1")
	{
	   $head_approval = "<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";
	   
	}
	
	 If ($row_tbl['approve_purchasing'] == "1")
	 {
		   //$purchase_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
		   $purchase_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
	   
	}
	  
	  If ($row_tbl['aprove_fc'] == "1")
	 {
		   //$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
		   $fc_aproval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
		}else{
			 If ($row_tbl['status_send_aprove'] == "-1")
			 {
			   $fc_aproval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
			 }else{
			   $fc_aproval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
			 }
	  }
	  
	   
	  
		 If ($row_tbl['aprove_bod'] == "1")
		{
			$bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
		   
		 }
  
$strbodaprove  = $row_tbl["aprove_bod"] ;
$strpurchaseaprove = $row_tbl["approve_purchasing"] ;
$strfcaprove   = $row_tbl["aprove_fc"] ;
$strheadaprove = $row_tbl["aprove_head"] ;	

				   
	  

//Pembedaan Warna pada type purchase.
  if ($row_tbl['flag_purchase'] == "1") {
	  $type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
  }else{
		if ($row_tbl['flag_purchase'] == "2") {
			  $type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
		  }else{
				if ($row_tbl['flag_purchase'] == "3") {
				  $type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
			  }else{
					if ($row_tbl['flag_purchase'] == "4") {
						  $type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
					  }else{
						   $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
					};
			};
		};   
	};
//end------------------------------			 

//date--------------
  if ($row_tbl['date_aprove_head'] ==""){
	  $datehead = "" ;
  }else{
	  $datehead  = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_head']))."</div>";
  }
  
  if ($row_tbl['date_approve_purchasing'] ==""){
	  $datepur = "" ;
  }else{
	  $datepur = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_approve_purchasing']))."</div>";
  }

  if ($row_tbl['date_aprove_fc'] ==""){
	   $datefc = "" ;
  }else{
	   $datefc= "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_fc']))."</div>";
  }

  if ($row_tbl['date_aprove_bod'] ==""){
	  $datebod = "" ;
  }else{
	  $datebod = "<div style='color:#EB293D'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_bod']))."</div>";
  }

//end date-----------------

//label colum 1 idepp
$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP
//end label
//-----------------------------------------------------------------------------------------------
$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";
//-----------------------------------------------------------------------------------------------

//button print p.o--
$strprintpo_ok = '<button class="btn btn-app btn-grey btn-xs radius-4 btnpo btn-clean printpobutton" type="submit" id="btnprintpo" name="btnprintpo" value ='.$row_tbl["id_master"].' onclick="window.location.href="" ><i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i> </button>';
//end---------------------------

// Button Give Right (Re-Print)
$give_right = '<button class="btn btn-app btn-info btn-xs radius-4 btnpo btn-clean giverightpo" type="button" id="giverightpo" name="giverightpo" req_id ='.$row_tbl["id_master"].' OnClick="giveRightPO(this)" ><i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i> </button>';
// ---------------

  // Kalau Login sebagai Admin / Finance
if($access == 1 || $access == 2 ) {
   $output['data'][]=array(
						   $idmas,
						   $btn_view_detail,
						   $attach_quo,															
						   $strprintpo_ok,
						   $give_right,
						   $row_tbl['short'],
						   $row_tbl['user_submission'],
						   $row_tbl['dept'],
						   $header_desc ,//$row_tbl['header_desc'],
						   $row_tbl['vendor'],																								
						   date('d-m-Y', strtotime($row_tbl['date_pp'])),	
						   $type_purchase, //$row_tbl['type_purchase'],
						   $row_tbl['currency'],
						   number_format($row_tbl['gran_total'],2,'.',','),
						   number_format($row_tbl['gran_totalppn'],2,'.',','),
						   $row_tbl['term_top']." "."Days",
						   $head_approval.$datehead,
						   $purchase_approval.$datepur,
						   $fc_aproval.$datefc,
						   $bod_approval.$datebod	
														   
					 );		
  } else {
	  $output['data'][]=array(
		  $idmas,
		  $btn_view_detail,
		  $attach_quo,															
		  $strprintpo_ok,
		  $row_tbl['short'],
		  $row_tbl['user_submission'],
		  $row_tbl['dept'],
		  $header_desc ,//$row_tbl['header_desc'],
		  $row_tbl['vendor'],																								
		  date('d-m-Y', strtotime($row_tbl['date_pp'])),	
		  $type_purchase, //$row_tbl['type_purchase'],
		  $row_tbl['currency'],
		  number_format($row_tbl['gran_total'],2,'.',','),
		  number_format($row_tbl['gran_totalppn'],2,'.',','),
		  $row_tbl['term_top']." "."Days",
		  $head_approval.$datehead,
		  $purchase_approval.$datepur,
		  $fc_aproval.$datefc,
		  $bod_approval.$datebod
	  );	
  }
					 
}
echo json_encode($output);
}	
	
function cath_data_fordatatables_po(){
	//$str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
$str_idcompany = $this->session->userdata('id_company'); 
$str_iddept = $this->session->userdata('id_dept');
$access = $this->session->userdata('apps_accsess'); 
//$access = 0;
$str_status_send = "1";
$str_status_approved = "1";
/*Menagkap semua data yang dikirimkan oleh client*/


$draw=$_REQUEST['draw'];
$length=$_REQUEST['length'];
$start=$_REQUEST['start'];
$search=$_REQUEST['search']["value"];

//order short column
$column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top","flag_po");

/*Menghitung total qv didalam database*/
$this->db->select('id_master');
$this->db->from('qv_head_pp_complite');
$this->db->where("aprove_bod","1");
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("flag_po","0");
$this->db->where("flag_status_inv_receipt","0");
$this->db->where("status","1");
$this->db->where("flag_dept_inv_receipt","0");
$this->db->where("flag_com_inv_receipt",'0');

// query khusus Inv Receipt
$this->db->or_where("aprove_bod","1");
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("flag_po","0");
$this->db->where("flag_status_inv_receipt","1");
$this->db->where("flag_inv_receipt","1");
$this->db->where("status","1");
$this->db->where("flag_dept_inv_receipt","1");
$this->db->where("flag_com_inv_receipt",'1');
// query khusus Inv Receipt

$this->db->like("id_master",$search);
$this->db->or_like("user_submission",$search);
$this->db->where("aprove_bod","1");
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("flag_po","0");
$this->db->where("flag_status_inv_receipt","0");
$this->db->where("status","1");
$this->db->where("flag_dept_inv_receipt","0");
$this->db->where("flag_com_inv_receipt",'0');

// query khusus Inv Receipt
$this->db->or_where("aprove_bod","1");
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("flag_po","0");
$this->db->where("flag_status_inv_receipt","1");
$this->db->where("flag_inv_receipt","1");
$this->db->where("status","1");
$this->db->where("flag_dept_inv_receipt","1");
$this->db->where("flag_com_inv_receipt",'1');
// query khusus Inv Receipt

$this->db->or_like("header_desc",$search);	
$this->db->where("aprove_bod","1");
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("flag_po","0");
$this->db->where("flag_status_inv_receipt","0");
$this->db->where("status","1");
$this->db->where("flag_dept_inv_receipt","0");
$this->db->where("flag_com_inv_receipt",'0');

// query khusus Inv Receipt
$this->db->or_where("aprove_bod","1");
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("flag_po","0");
$this->db->where("flag_status_inv_receipt","1");
$this->db->where("flag_inv_receipt","1");
$this->db->where("status","1");
$this->db->where("flag_dept_inv_receipt","1");
$this->db->where("flag_com_inv_receipt",'1');
// query khusus Inv Receipt

$this->db->or_like("short",$search);	
$this->db->where("aprove_bod","1");
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("flag_po","0");
$this->db->where("flag_status_inv_receipt","0");
$this->db->where("status","1");
$this->db->where("flag_dept_inv_receipt","0");
$this->db->where("flag_com_inv_receipt",'0');

// query khusus Inv Receipt
$this->db->or_where("aprove_bod","1");
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("flag_po","0");
$this->db->where("flag_status_inv_receipt","1");
$this->db->where("flag_inv_receipt","1");
$this->db->where("status","1");
$this->db->where("flag_dept_inv_receipt","1");
$this->db->where("flag_com_inv_receipt",'1');
// query khusus Inv Receipt

$this->db->or_like("vendor",$search);	
$this->db->where("aprove_bod","1");
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("flag_po","0");
$this->db->where("flag_status_inv_receipt","0");
$this->db->where("status","1");
$this->db->where("flag_dept_inv_receipt","0");
$this->db->where("flag_com_inv_receipt",'0');

// query khusus Inv Receipt
$this->db->or_where("aprove_bod","1");
$this->db->where("status_send_aprove",$str_status_send);
$this->db->where("flag_po","0");
$this->db->where("flag_status_inv_receipt","1");
$this->db->where("flag_inv_receipt","1");
$this->db->where("status","1");
$this->db->where("flag_dept_inv_receipt","1");
$this->db->where("flag_com_inv_receipt",'1');
// query khusus Inv Receipt

$total = $this->db->count_all_results();

$output=array();
$output['draw']=$draw;
$output['recordsTotal']=$output['recordsFiltered']=$total;
$output['data']=array();

/*Jika $search mengandung nilai, berarti user sedang telah 
memasukan keyword didalam filed pencarian*/
if($search!=""){

	// cari id_master
	$this->db->like("id_master",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari po no
	$this->db->or_like("po_no_created",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari short
	$this->db->or_like("short",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari user_submission
	$this->db->or_like("user_submission",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari vendor
	$this->db->or_like("vendor",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari header_desc
	$this->db->or_like("header_desc",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari dept
	$this->db->or_like("dept",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari currency
	$this->db->or_like("currency",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));
	
	// cari type_purchase
	$this->db->or_like("type_purchase",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));
	
// 		$this->db->where("aprove_bod","1");
// 		$this->db->where("status_send_aprove",$str_status_send);
// 		$this->db->where("flag_po","0");
// 		$this->db->where("flag_status_inv_receipt","0");
// 		$this->db->where("status","1");
// 		$this->db->where("flag_dept_inv_receipt","0");
// 		$this->db->where("flag_com_inv_receipt",'0');

// 		// query khusus Inv Receipt
// 		$this->db->or_where("aprove_bod","1");
// 		$this->db->where("status_send_aprove",$str_status_send);
// 		$this->db->where("flag_po","0");
// 		$this->db->where("flag_status_inv_receipt","1");
// 		$this->db->where("flag_inv_receipt","1");
// 		$this->db->where("status","1");
// 		$this->db->where("flag_dept_inv_receipt","1");
// 		$this->db->where("flag_com_inv_receipt",'1');
// 		// query khusus Inv Receipt

// 		$this->db->like("id_master",$search);
// 		$this->db->or_like("user_submission",$search);
// 		$this->db->where("aprove_bod","1");
// 		$this->db->where("status_send_aprove",$str_status_send);
// 		$this->db->where("flag_po","0");
// 		$this->db->where("flag_status_inv_receipt","0");
// 		$this->db->where("status","1");
// 		$this->db->where("flag_dept_inv_receipt","0");
// 		$this->db->where("flag_com_inv_receipt",'0');

// 		// query khusus Inv Receipt
// 		$this->db->or_where("aprove_bod","1");
// 		$this->db->where("status_send_aprove",$str_status_send);
// 		$this->db->where("flag_po","0");
// 		$this->db->where("flag_status_inv_receipt","1");
// 		$this->db->where("flag_inv_receipt","1");
// 		$this->db->where("status","1");
// 		$this->db->where("flag_dept_inv_receipt","1");
// 		$this->db->where("flag_com_inv_receipt",'1');
// 		// query khusus Inv Receipt

// 		$this->db->or_like("header_desc",$search);	
// 		$this->db->where("aprove_bod","1");
// 		$this->db->where("status_send_aprove",$str_status_send);
// 		$this->db->where("flag_po","0");
// 		$this->db->where("flag_status_inv_receipt","0");
// 		$this->db->where("status","1");
// 		$this->db->where("flag_dept_inv_receipt","0");
// 		$this->db->where("flag_com_inv_receipt",'0');

// 		// query khusus Inv Receipt
// 		$this->db->or_where("aprove_bod","1");
// 		$this->db->where("status_send_aprove",$str_status_send);
// 		$this->db->where("flag_po","0");
// 		$this->db->where("flag_status_inv_receipt","1");
// 		$this->db->where("flag_inv_receipt","1");
// 		$this->db->where("status","1");
// 		$this->db->where("flag_dept_inv_receipt","1");
// 		$this->db->where("flag_com_inv_receipt",'1');
// 		// query khusus Inv Receipt

// 		$this->db->or_like("short",$search);	
// 		$this->db->where("status_send_aprove",$str_status_send);
// 		$this->db->where("aprove_bod","1");
// 		$this->db->where("status_send_aprove",$str_status_send);
// 		$this->db->where("flag_po","0");
// 		$this->db->where("flag_status_inv_receipt","0");
// 		$this->db->where("status","1");
// 		$this->db->where("flag_dept_inv_receipt","0");
// 		$this->db->where("flag_com_inv_receipt",'0');

// 		// query khusus Inv Receipt
// 		$this->db->or_where("aprove_bod","1");
// 		$this->db->where("status_send_aprove",$str_status_send);
// 		$this->db->where("flag_po","0");
// 		$this->db->where("flag_status_inv_receipt","1");
// 		$this->db->where("flag_inv_receipt","1");
// 		$this->db->where("status","1");
// 		$this->db->where("flag_dept_inv_receipt","1");
// 		$this->db->where("flag_com_inv_receipt",'1');
// 		// query khusus Inv Receipt

// 		$this->db->or_like("vendor",$search);	
// 		$this->db->where("aprove_bod","1");
// 		$this->db->where("status_send_aprove",$str_status_send);
// 		$this->db->where("flag_po","0");
// 		$this->db->where("flag_status_inv_receipt","0");
// 		$this->db->where("status","1");
// 		$this->db->where("flag_dept_inv_receipt","0");
// 		$this->db->where("flag_com_inv_receipt",'0');

// 		// query khusus Inv Receipt
// 		$this->db->or_where("aprove_bod","1");
// 		$this->db->where("status_send_aprove",$str_status_send);
// 		$this->db->where("flag_po","0");
// 		$this->db->where("flag_status_inv_receipt","1");
// 		$this->db->where("flag_inv_receipt","1");
// 		$this->db->where("status","1");
// 		$this->db->where("flag_dept_inv_receipt","1");
// 		$this->db->where("flag_com_inv_receipt",'1');
// 		// query khusus Inv Receipt
 }

		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);	
		$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

		// $this->db->where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","0");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","0");
		// $this->db->where("flag_com_inv_receipt",'0');

		// // query khusus Inv Receipt
		// $this->db->or_where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","1");
		// $this->db->where("flag_inv_receipt","1");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","1");
		// $this->db->where("flag_com_inv_receipt",'1');
		// query khusus Inv Receipt
	$this->db->order_by('date_pp','DESC');
   
	//order column
	if (isset($_REQUEST["order"])){
		 $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
	}else{
		 $this->db->order_by('aprove_head','desc');
		 $this->db->order_by('approve_purchasing','desc');
		 $this->db->order_by('aprove_fc','desc');
		 $this->db->order_by('aprove_bod','desc');
	 }
 
	 $this->db->order_by('aprove_head','asc');
	 $this->db->order_by('approve_purchasing','asc');
	 $this->db->order_by('aprove_fc','asc');
	 $this->db->order_by('aprove_bod','asc');	
	 $this->db->order_by('date_send_aproval','DESC');  
	 $query=$this->db->get('qv_head_pp_complite');


/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
yang mengandung keyword tertentu
*/

if($search!=""){
	// cari id_master
	$this->db->like("id_master",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari po no
	$this->db->or_like("po_no_created",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari short
	$this->db->or_like("short",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari user_submission
	$this->db->or_like("user_submission",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari vendor
	$this->db->or_like("vendor",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari header_desc
	$this->db->or_like("header_desc",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari dept
	$this->db->or_like("dept",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));

	// cari currency
	$this->db->or_like("currency",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));
	
	// cari type_purchase
	$this->db->or_like("type_purchase",$search);
	$this->db->where("aprove_bod","1");
	$this->db->where("status_send_aprove",$str_status_send);
	$this->db->where("flag_po","0");
	$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	$this->db->where("status","1");
	$this->db->where_in("flag_dept_inv_receipt",array('0', '1'));
	$this->db->where_in("flag_com_inv_receipt",array('0', '1'));
		// $this->db->where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","0");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","0");
		// $this->db->where("flag_com_inv_receipt",'0');

		// // query khusus Inv Receipt
		// $this->db->or_where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","1");
		// $this->db->where("flag_inv_receipt","1");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","1");
		// $this->db->where("flag_com_inv_receipt",'1');
		// // query khusus Inv Receipt

		// $this->db->like("id_master",$search);
		// $this->db->or_like("user_submission",$search);
		// $this->db->where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","0");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","0");
		// $this->db->where("flag_com_inv_receipt",'0');

		// // query khusus Inv Receipt
		// $this->db->or_where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","1");
		// $this->db->where("flag_inv_receipt","1");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","1");
		// $this->db->where("flag_com_inv_receipt",'1');
		// // query khusus Inv Receipt

		// $this->db->or_like("header_desc",$search);	
		// $this->db->where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","0");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","0");
		// $this->db->where("flag_com_inv_receipt",'0');

		// // query khusus Inv Receipt
		// $this->db->or_where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","1");
		// $this->db->where("flag_inv_receipt","1");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","1");
		// $this->db->where("flag_com_inv_receipt",'1');
		// // query khusus Inv Receipt

		// $this->db->or_like("short",$search);	
		// $this->db->where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","0");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","0");
		// $this->db->where("flag_com_inv_receipt",'0');

		// // query khusus Inv Receipt
		// $this->db->or_where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","1");
		// $this->db->where("flag_inv_receipt","1");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","1");
		// $this->db->where("flag_com_inv_receipt",'1');
		// // query khusus Inv Receipt

		// $this->db->or_like("vendor",$search);	
		// $this->db->where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","0");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","0");
		// $this->db->where("flag_com_inv_receipt",'0');

		// // query khusus Inv Receipt
		// $this->db->or_where("aprove_bod","1");
		// $this->db->where("status_send_aprove",$str_status_send);
		// $this->db->where("flag_po","0");
		// $this->db->where("flag_status_inv_receipt","1");
		// $this->db->where("flag_inv_receipt","1");
		// $this->db->where("status","1");
		// $this->db->where("flag_dept_inv_receipt","1");
		// $this->db->where("flag_com_inv_receipt",'1');
		// query khusus Inv Receipt
	   
		$jum=$this->db->get('qv_head_pp_complite');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
}





foreach ($query->result_array() as $row_tbl) {
	
	If ($row_tbl['attach_quo'] != "")
	 {  
	  $attach_quo = '<div  align="center"><a onClick="PDFPopup(this)" req_id='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
	 }else{
	   $attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
	 }

  
  
	   $btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-list" aria-hidden="true"></i></a></center>';//button detail
  
   If ($row_tbl['aprove_head'] == "1")
	{
	   $head_approval = "<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";
	   
	}
	
	 If ($row_tbl['approve_purchasing'] == "1")
	 {
		   //$purchase_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
		   $purchase_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
	   
	}
	  
	  If ($row_tbl['aprove_fc'] == "1")
	 {
		   //$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
		   $fc_aproval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
		}else{
			 If ($row_tbl['status_send_aprove'] == "-1")
			 {
			   $fc_aproval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
			 }else{
			   $fc_aproval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
			 }
	  }
	  
	   
	  
		 If ($row_tbl['aprove_bod'] == "1")
		{
			$bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
		   
		 }
  
$strbodaprove  = $row_tbl["aprove_bod"] ;
$strpurchaseaprove = $row_tbl["approve_purchasing"] ;
$strfcaprove   = $row_tbl["aprove_fc"] ;
$strheadaprove = $row_tbl["aprove_head"] ;	

				   
	  

//Pembedaan Warna pada type purchase.
  if ($row_tbl['flag_purchase'] == "1") {
	  $type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
  }else{
		if ($row_tbl['flag_purchase'] == "2") {
			  $type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
		  }else{
				if ($row_tbl['flag_purchase'] == "3") {
				  $type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
			  }else{
					if ($row_tbl['flag_purchase'] == "4") {
						  $type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
					  }else{
						   $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
					};
			};
		};   
	};
//end------------------------------			 

//date--------------
  if ($row_tbl['date_aprove_head'] ==""){
	  $datehead = "" ;
  }else{
	  $datehead  = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_head']))."</div>";
  }
  
  if ($row_tbl['date_approve_purchasing'] ==""){
	  $datepur = "" ;
  }else{
	  $datepur = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_approve_purchasing']))."</div>";
  }

  if ($row_tbl['date_aprove_fc'] ==""){
	   $datefc = "" ;
  }else{
	   $datefc= "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_fc']))."</div>";
  }

  if ($row_tbl['date_aprove_bod'] ==""){
	  $datebod = "" ;
  }else{
	  $datebod = "<div style='color:#EB293D'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_bod']))."</div>";
  }

//end date-----------------

//label colum 1 idepp
$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP
//end label
//-----------------------------------------------------------------------------------------------
$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";
//-----------------------------------------------------------------------------------------------

//button print p.o--
//$strprintpo_ok = '<button class="btn btn-app btn-grey btn-xs radius-4 btnpo btn-clean printpobutton" type="submit" id="btnprintpo" name="btnprintpo" value ='.$row_tbl["id_master"].' onclick="window.location.href="" ><i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i> </button>';
$no_pp = base64_encode($row_tbl["id_master"]);
$url = base_url('fpbpo_form/C_print_fpb_po/print_po?no_pp='.$no_pp.'#toolbar=0');
$url_pdf = "printJS({printable:'$url',type:'pdf',showModal:true});disableButtonPrint(this)";
$strprintpo_ok = '<button type="button" OnClick='.$url_pdf.' req_id="'.$url.'" class="btn btn-app btn-grey btn-xs radius-4 btnpo btn-clean printpobutton">
<i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i> 
				</button>';
//end---------------------------

// Button Give Right (Re-Print)
$give_right = '<button class="btn btn-app btn-info btn-xs radius-4 btnpo btn-clean giverightpo" type="button" id="giverightpo" name="giverightpo" req_id ='.$row_tbl["id_master"].' OnClick="giveRightPO(this)" ><i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i> </button>';
// ---------------

  // Kalau Login sebagai Admin / Finance
if($access == 1 || $access == 2 ) {
   $output['data'][]=array(
						   $idmas,
						   $btn_view_detail,
						   $attach_quo,															
						   $strprintpo_ok,
						   //$give_right,
						   $row_tbl['short'],
						   $row_tbl['user_submission'],
						   $row_tbl['dept'],
						   $header_desc ,//$row_tbl['header_desc'],
						   $row_tbl['vendor'],																								
						   date('d-m-Y', strtotime($row_tbl['date_pp'])),	
						   $type_purchase, //$row_tbl['type_purchase'],
						   $row_tbl['currency'],
						   number_format($row_tbl['gran_total'],2,'.',','),
						   number_format($row_tbl['gran_totalppn'],2,'.',','),
						   $row_tbl['term_top']." "."Days",
						   $head_approval.$datehead,
						   $purchase_approval.$datepur,
						   $fc_aproval.$datefc,
						   $bod_approval.$datebod	
														   
					 );		
  } else {
	  $output['data'][]=array(
		  $idmas,
		  $btn_view_detail,
		  $attach_quo,															
		  $strprintpo_ok,
		  $row_tbl['short'],
		  $row_tbl['user_submission'],
		  $row_tbl['dept'],
		  $header_desc ,//$row_tbl['header_desc'],
		  $row_tbl['vendor'],																								
		  date('d-m-Y', strtotime($row_tbl['date_pp'])),	
		  $type_purchase, //$row_tbl['type_purchase'],
		  $row_tbl['currency'],
		  number_format($row_tbl['gran_total'],2,'.',','),
		  number_format($row_tbl['gran_totalppn'],2,'.',','),
		  $row_tbl['term_top']." "."Days",
		  $head_approval.$datehead,
		  $purchase_approval.$datepur,
		  $fc_aproval.$datefc,
		  $bod_approval.$datebod
	  );	
  }
					 
}
echo json_encode($output);
}	

 
 function cath_data_fordatatables_rieceved(){
	
	//  $str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
	  $str_idcompany = $this->session->userdata('id_company'); 
	  $str_iddept = $this->session->userdata('id_dept'); 
	  $access = $this->session->userdata('apps_accsess'); 
	  //$access = 0;
	  $str_status_send = "1";
		$str_status_approved = "1";
	
	 $draw=$_REQUEST['draw'];
	 $length=$_REQUEST['length'];
	 $start=$_REQUEST['start'];

	 /*Keyword yang diketikan oleh user pada field pencarian*/
	 $search=$_REQUEST['search']["value"];

	 //order short column
	 $column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top");
	 
	 /*Menghitung total qv didalam database*/
	  $this->db->select('id_master');
	  $this->db->from('qv_head_pp_complite');
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where("flag_status_inv_receipt","0");

		// query khusus Inv Receipt
		$this->db->or_where("flag_status_inv_receipt","1");
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		// query khusus Inv Receipt

	  $this->db->like("id_master",$search);
	  $this->db->or_like("user_submission",$search);
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");				
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where("flag_status_inv_receipt","0");
		
		// query khusus Inv Receipt
		$this->db->or_where("flag_status_inv_receipt","1");
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		// query khusus Inv Receipt

	  $this->db->or_like("header_desc",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where("flag_status_inv_receipt","0");
		
		// query khusus Inv Receipt
		$this->db->or_where("flag_status_inv_receipt","1");
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		// query khusus Inv Receipt

	  $this->db->or_like("short",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where("flag_status_inv_receipt","0");
		
		// query khusus Inv Receipt
		$this->db->or_where("flag_status_inv_receipt","1");
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		// query khusus Inv Receipt

	  $this->db->or_like("vendor",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where("flag_status_inv_receipt","0");
		
		// query khusus Inv Receipt
		$this->db->or_where("flag_status_inv_receipt","1");
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		// query khusus Inv Receipt
	  $total = $this->db->count_all_results();

	 /*Mempersiapkan array tempat kita akan menampung semua data
	 yang nantinya akan server kirimkan ke client*/
	 $output=array();

	 /*Token yang dikrimkan client, akan dikirim balik ke client*/
	 $output['draw']=$draw;

	 /*
	 $output['recordsTotal'] adalah total data sebelum difilter
	 $output['recordsFiltered'] adalah total data ketika difilter
	 Biasanya kedua duanya bernilai sama, maka kita assignment 
	 keduaduanya dengan nilai dari $total
	 */
	 $output['recordsTotal']=$output['recordsFiltered']=$total;

	 /*disini nantinya akan memuat data yang akan kita tampilkan 
	 pada table client*/
	 $output['data']=array();


	 /*Jika $search mengandung nilai, berarti user sedang telah 
	 memasukan keyword didalam filed pencarian*/
	 if($search!=""){

		// cari id_master
		$this->db->like("id_master",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari po no
		$this->db->or_like("po_no_created",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari short
		$this->db->or_like("short",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari user_submission
		$this->db->or_like("user_submission",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari vendor
		$this->db->or_like("vendor",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari header_desc
		$this->db->or_like("header_desc",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari dept
		$this->db->or_like("dept",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari currency
		$this->db->or_like("currency",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
		
		// cari type_purchase
		$this->db->or_like("type_purchase",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
		
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->like("id_master",$search);
			//   $this->db->or_like("user_submission",$search);
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);		
			//   $this->db->where("aprove_bod","1");		
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->or_like("header_desc",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->or_like("short",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt
				
			//   $this->db->or_like("vendor",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
				// query khusus Inv Receipt
	 }

		  /*Lanjutkan pencarian ke database*/
		  $this->db->limit($length,$start);	
		  
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		$this->db->or_where("flag_inv_receipt","1");
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
		  
		//   $this->db->where("status_send_aprove",$str_status_send);
		//   $this->db->where("id_company",$str_idcompany);
		//   $this->db->where("id_dept",$str_iddept);			 
		//   $this->db->where("aprove_bod","1");
	    //   /*Urutkan dari alphabet paling terkahir*/
		// 		$this->db->where("status","1");
		// 		$this->db->where("flag_po","1");
		// 		$this->db->where("flag_status_inv_receipt","0");
		// 		$this->db->or_where("flag_inv_receipt","1");

		// 		// query khusus Inv Receipt
		// 		$this->db->or_where("flag_status_inv_receipt","1");
		// 		$this->db->where("status_send_aprove",$str_status_send);
		// 		$this->db->where("id_company",$str_idcompany);
		// 		$this->db->where("id_dept",$str_iddept);
		// 		$this->db->where("aprove_bod","1");
		// 		$this->db->where("status","1");
		// 		$this->db->where("flag_po","1");
				// query khusus Inv Receipt
	      $this->db->order_by('date_pp','DESC');
	     
		  //order column
		  if (isset($_REQUEST["order"])){
			   $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
		  }else{
			   $this->db->order_by('aprove_head','desc');
			   $this->db->order_by('approve_purchasing','desc');
			   $this->db->order_by('aprove_fc','desc');
			   $this->db->order_by('aprove_bod','desc');
		   }
	   
		   $this->db->order_by('aprove_head','asc');
		   $this->db->order_by('approve_purchasing','asc');
		   $this->db->order_by('aprove_fc','asc');
		   $this->db->order_by('aprove_bod','asc');	
		   $this->db->order_by('date_send_aproval','DESC');  
		   $query=$this->db->get('qv_head_pp_complite');


	 /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
	 dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
	 yang mengandung keyword tertentu
	 */
	 
	 if($search!=""){
		 // cari id_master
		$this->db->like("id_master",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari po no
		$this->db->or_like("po_no_created",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari short
		$this->db->or_like("short",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari user_submission
		$this->db->or_like("user_submission",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari vendor
		$this->db->or_like("vendor",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari header_desc
		$this->db->or_like("header_desc",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari dept
		$this->db->or_like("dept",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari currency
		$this->db->or_like("currency",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
		
		// cari type_purchase
		$this->db->or_like("type_purchase",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->like("id_master",$search);
			//   $this->db->or_like("user_submission",$search);
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);		
			//   $this->db->where("aprove_bod","1");		
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->or_like("header_desc",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->or_like("short",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			//   $this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->or_like("vendor",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
				// query khusus Inv Receipt
			 
		      $jum=$this->db->get('qv_head_pp_complite');
		      $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
	 }




	 
	 foreach ($query->result_array() as $row_tbl) {

		$flag_received = $row_tbl['flag_recived'];
		  
		  If ($row_tbl['attach_quo'] != "")
		   {  
			$attach_quo = '<div  align="center"><a onClick="PDFPopup(this)" req_id='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
		   }else{
			 $attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
		   }
	 
			$btn_input_grn = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 input_grn" id="input_grn" style="text-decoration:none" id_pp='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i></a></center>';//button detail
		
		
		
		 If ($row_tbl['aprove_head'] == "1")
		  {
			 $head_approval = "<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";
			 
		  }
		  
		   If ($row_tbl['approve_purchasing'] == "1")
		   {
				 //$purchase_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 $purchase_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
			 
		  }
			
			If ($row_tbl['aprove_fc'] == "1")
		   {
				 //$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 $fc_aproval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
			  }else{
				   If ($row_tbl['status_send_aprove'] == "-1")
				   {
					 $fc_aproval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
				   }else{
					 $fc_aproval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
				   }
			}
			
			 
			
			   If ($row_tbl['aprove_bod'] == "1")
			  {
					  $bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
				 
			   }
			  
		 
				 $strbodaprove  = $row_tbl["aprove_bod"] ;
				 $strpurchaseaprove = $row_tbl["approve_purchasing"] ;
				 $strfcaprove   = $row_tbl["aprove_fc"] ;
				 $strheadaprove = $row_tbl["aprove_head"] ;	

        

		 $chk_idmaster ='<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value='.$row_tbl["id_master"].' class="getPO ace" req_id_del='.$row_tbl["id_master"].' />
		<span class="lbl"></span> ';				
			

//Pembedaan Warna pada type purchase.
		if ($row_tbl['flag_purchase'] == "1") {
			$type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
		}else{
			  if ($row_tbl['flag_purchase'] == "2") {
					$type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
				}else{
					  if ($row_tbl['flag_purchase'] == "3") {
						$type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
					}else{
						  if ($row_tbl['flag_purchase'] == "4") {
								$type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
							}else{
								 $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
						  };
				  };
			  };   
		  };
//end------------------------------			 



//label colum 1 idepp
$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP
//end label

//header desc
$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";
//end header desc.


//button detail ---
$btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail"  id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon 	glyphicon-list  " aria-hidden="true"></i></a></center>';//button detail
//--------



	//button input grn
	if($flag_received == '0' || $flag_received == null) {
		$str_grn_input = '<button class="btn btn-app btn-danger btn-xs radius-8" type="submit" id="btngrn" name="btngrn" value ='.$row_tbl['id_master'].' ><i class="ace-iconfa glyphicon 	glyphicon-edit bigger-160"></i> </button>';
		$company = $row_tbl['short'];
		$submission =  $row_tbl['user_submission'];
		$dept = $row_tbl['dept'];
		$vendor = $row_tbl['vendor'];
		$date_send = date('d-m-Y', strtotime($row_tbl['date_pp']));
		$currency = $row_tbl['currency'];
		$total = number_format($row_tbl['gran_total'],2,'.',',');
		$total_ppn = number_format($row_tbl['gran_totalppn'],2,'.',',');
		$term = $row_tbl['term_top'].' Days';
		$closing_po = '<button class="btn btn-app btn-success btn-xs radius-4 btnpo btn-clean closingpo" type="button" id="closingpo" name="closingpo" req_id ='.$row_tbl["id_master"].' OnClick="closingPO(this)" ><i class="fa fa-check-circle-o bigger-160"></i> </button>';
		$header_desc = $header_desc;
		$po_no = $row_tbl['po_no_created'];
		$type_purchase =  $type_purchase;

	} else {
		$str_grn_input = '<button class="btn btn-app btn-danger btn-xs radius-8" type="submit" id="btngrn" name="btngrn" value ='.$row_tbl['id_master'].' disabled><i class="ace-iconfa glyphicon 	glyphicon-edit bigger-160"></i> </button>';
		$company = '<span class="badge badge-success">'.$row_tbl['short'].'</span>';
		$submission =  '<span class="badge badge-success">'.$row_tbl['user_submission'].'</span>';
		$dept = '<span class="badge badge-success">'.$row_tbl['dept'].'</span>';
		$vendor = '<span class="badge badge-success">'.$row_tbl['vendor'].'</span>';
		$date_send = '<span class="badge badge-success">'.date('d-m-Y', strtotime($row_tbl['date_pp'])).'</span>';
		$currency = '<span class="badge badge-success">'.$row_tbl['currency'].'</span>';
		$total = '<span class="badge badge-success">'.number_format($row_tbl['gran_total'],2,'.',',').'</span>';
		$total_ppn = '<span class="badge badge-success">'.number_format($row_tbl['gran_totalppn'],2,'.',',').'</span>';
		$term = '<span class="badge badge-success">'.$row_tbl['term_top'].' Days'.'</span>';
		$closing_po = '<button class="btn btn-app btn-success btn-xs radius-4 btnpo btn-clean closingpo" type="button" id="closingpo" name="closingpo" req_id ='.$row_tbl["id_master"].' OnClick="closingPO(this)" disabled><i class="fa fa-check-circle-o bigger-160"></i> </button>';
		$header_desc = '<span class="badge badge-success">'.$row_tbl['header_desc'].'</span>';
		$po_no = '<span class="badge badge-success">'.$row_tbl['po_no_created'].'</span>';

		$type_purchase = $row_tbl['type_purchase'];
		$type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>".$type_purchase."</span></div>";
				   
	}
//-----------------------------------------------

// Kalau Login sebagai Admin atau Finance
	if($access == 1 || $access == 2 ) {
		 $output['data'][]=array(
								// $chk_idmaster,
								 $idmas,
								 $btn_view_detail,
								 $attach_quo,															
								 $str_grn_input	,
								 $closing_po,
								 $po_no	,
								 $company,
								 $submission,
								 $dept,
								 $header_desc ,//$row_tbl['header_desc'],
								 $vendor,																								
								 $date_send,	
								 $currency,
								 $type_purchase, //$row_tbl['type_purchase'],
								 $total,
								 $total_ppn,
								 $term,
								/* $head_approval.$datehead,
								 $purchase_approval.$datepur,
								 $fc_aproval.$datefc,
								 $bod_approval.$datebod, */	
																 
						   );	
		} else {
			$output['data'][]=array(
				// $chk_idmaster,
				 $idmas,
				 $btn_view_detail,
				 $attach_quo,															
				 $str_grn_input	,
				 $po_no	,
				 $company,
				 $submission,
				 $dept,
				 $header_desc ,//$row_tbl['header_desc'],
				 $vendor,																								
				 $date_send,	
				 $currency,
				 $type_purchase, //$row_tbl['type_purchase'],
				 $total,
				 $total_ppn,
				 $term,
				/* $head_approval.$datehead,
				 $purchase_approval.$datepur,
				 $fc_aproval.$datefc,
				 $bod_approval.$datebod, */	
												 
		   );	
		}		
	 }
	 echo json_encode($output);
 }	
 
  function cath_data_fordatatables_fpb(){
	
	//  $str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
	  $str_idcompany = $this->session->userdata('id_company'); 
	  $str_iddept = $this->session->userdata('id_dept'); 
	  $str_status_send = "1";
	  $str_status_approved = "1";
	 /*Menagkap semua data yang dikirimkan oleh client*/

	 /*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
	 server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
	 sesuai dengan urutan yang sebenarnya */
	 $draw=$_REQUEST['draw'];

	 /*Jumlah baris yang akan ditampilkan pada setiap page*/
	 $length=$_REQUEST['length'];

	 /*Offset yang akan digunakan untuk memberitahu database
	 dari baris mana data yang harus ditampilkan untuk masing masing page
	 */
	 $start=$_REQUEST['start'];

	 /*Keyword yang diketikan oleh user pada field pencarian*/
	 $search=$_REQUEST['search']["value"];

	 //order short column
	 $column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top");
	 
	 /*Menghitung total qv didalam database*/
	  $this->db->select('id_master');
	  $this->db->from('qv_head_pp_complite');
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->where("flag_po","1");
	  $this->db->where("flag_status_inv_receipt","0");
	  
	  // query khusus Inv Receipt
	  $this->db->or_where("flag_status_inv_receipt","1");
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->where("flag_po","1");
	  // query khusus Inv Receipt

	  $this->db->like("id_master",$search);
	  $this->db->or_like("user_submission",$search);
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");				
	  $this->db->where("status","1");
	  $this->db->where("flag_po","1");
	  $this->db->where("flag_status_inv_receipt","0");
	  
	  // query khusus Inv Receipt
	  $this->db->or_where("flag_status_inv_receipt","1");
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->where("flag_po","1");
	  // query khusus Inv Receipt

	  $this->db->or_like("header_desc",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->where("flag_po","1");
	  $this->db->where("flag_status_inv_receipt","0");
	  
	  // query khusus Inv Receipt
	  $this->db->or_where("flag_status_inv_receipt","1");
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->where("flag_po","1");
	  // query khusus Inv Receipt

	  $this->db->or_like("short",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->where("flag_po","1");
	  $this->db->where("flag_status_inv_receipt","0");
	  
	  // query khusus Inv Receipt
	  $this->db->or_where("flag_status_inv_receipt","1");
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->where("flag_po","1");
	  // query khusus Inv Receipt

	  $this->db->or_like("vendor",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->where("flag_po","1");
	  $this->db->where("flag_status_inv_receipt","0");
	  
	  // query khusus Inv Receipt
	  $this->db->or_where("flag_status_inv_receipt","1");
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->where("flag_po","1");
	  // query khusus Inv Receipt

	  $total = $this->db->count_all_results();

	 /*Mempersiapkan array tempat kita akan menampung semua data
	 yang nantinya akan server kirimkan ke client*/
	 $output=array();

	 /*Token yang dikrimkan client, akan dikirim balik ke client*/
	 $output['draw']=$draw;

	 /*
	 $output['recordsTotal'] adalah total data sebelum difilter
	 $output['recordsFiltered'] adalah total data ketika difilter
	 Biasanya kedua duanya bernilai sama, maka kita assignment 
	 keduaduanya dengan nilai dari $total
	 */
	 $output['recordsTotal']=$output['recordsFiltered']=$total;

	 /*disini nantinya akan memuat data yang akan kita tampilkan 
	 pada table client*/
	 $output['data']=array();


	 /*Jika $search mengandung nilai, berarti user sedang telah 
	 memasukan keyword didalam filed pencarian*/
	 if($search!=""){
		 // cari id_master
		$this->db->like("id_master",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari po no
		$this->db->or_like("po_no_created",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari short
		$this->db->or_like("short",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari user_submission
		$this->db->or_like("user_submission",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari vendor
		$this->db->or_like("vendor",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari header_desc
		$this->db->or_like("header_desc",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari dept
		$this->db->or_like("dept",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari currency
		$this->db->or_like("currency",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	
		// cari type_purchase
		$this->db->or_like("type_purchase",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			//   $this->db->where("status","1");
			//   $this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->like("id_master",$search);
			//   $this->db->or_like("user_submission",$search);
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);		
			//   $this->db->where("aprove_bod","1");		
			//   $this->db->where("status","1");
			//   $this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->or_like("header_desc",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			//   $this->db->where("status","1");
			//   $this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->or_like("short",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			//   $this->db->where("status","1");
			//   $this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->or_like("vendor",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			//   $this->db->where("status","1");
			//   $this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
				// query khusus Inv Receipt
	 }

		  /*Lanjutkan pencarian ke database*/
		  $this->db->limit($length,$start);		
		  $this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		//   $this->db->where("status_send_aprove",$str_status_send);
		//   $this->db->where("id_company",$str_idcompany);
		//   $this->db->where("id_dept",$str_iddept);			 
		//   $this->db->where("aprove_bod","1");
	    //   /*Urutkan dari alphabet paling terkahir*/
		//   $this->db->where("status","1");
		//   $this->db->where("flag_po","1");
		// 		$this->db->where("flag_status_inv_receipt","0");
				
		// 		// query khusus Inv Receipt
		// 		$this->db->or_where("flag_status_inv_receipt","1");
		// 		$this->db->where("status_send_aprove",$str_status_send);
		// 		$this->db->where("id_company",$str_idcompany);
		// 		$this->db->where("id_dept",$str_iddept);
		// 		$this->db->where("aprove_bod","1");
		// 		$this->db->where("status","1");
		// 		$this->db->where("flag_po","1");
				// query khusus Inv Receipt
	      $this->db->order_by('date_pp','DESC');
	     
		  //order column
		  if (isset($_REQUEST["order"])){
			   $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
		  }else{
			   $this->db->order_by('aprove_head','desc');
			   $this->db->order_by('approve_purchasing','desc');
			   $this->db->order_by('aprove_fc','desc');
			   $this->db->order_by('aprove_bod','desc');
		   }
	   
		   $this->db->order_by('aprove_head','asc');
		   $this->db->order_by('approve_purchasing','asc');
		   $this->db->order_by('aprove_fc','asc');
		   $this->db->order_by('aprove_bod','asc');	
		   $this->db->order_by('date_send_aproval','DESC');  
		   $query=$this->db->get('qv_head_pp_complite');


	 /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
	 dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
	 yang mengandung keyword tertentu
	 */
	 
	 if($search!=""){
		$this->db->like("id_master",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari po no
		$this->db->or_like("po_no_created",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari short
		$this->db->or_like("short",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari user_submission
		$this->db->or_like("user_submission",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari vendor
		$this->db->or_like("vendor",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari header_desc
		$this->db->or_like("header_desc",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari dept
		$this->db->or_like("dept",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));

		// cari currency
		$this->db->or_like("currency",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
	
		// cari type_purchase
		$this->db->or_like("type_purchase",$search);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where("id_company",$str_idcompany);
		$this->db->where("id_dept",$str_iddept);
		$this->db->where("aprove_bod","1");
		$this->db->where("status","1");
		$this->db->where("flag_po","1");
		$this->db->where_in("flag_status_inv_receipt",array('0', '1'));
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			//   $this->db->where("status","1");
			//   $this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->like("id_master",$search);
			//   $this->db->or_like("user_submission",$search);
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);		
			//   $this->db->where("aprove_bod","1");		
			//   $this->db->where("status","1");
			//   $this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
			// 	$this->db->or_where("flag_inv_receipt","1");
			//   $this->db->or_like("header_desc",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			//   $this->db->where("status","1");
			//   $this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->or_like("short",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			//   $this->db->where("aprove_bod","1");
			//   $this->db->where("status","1");
			//   $this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
			// 	// query khusus Inv Receipt

			//   $this->db->or_like("vendor",$search);	
			//   $this->db->where("status_send_aprove",$str_status_send);
			//   $this->db->where("id_company",$str_idcompany);
			//   $this->db->where("id_dept",$str_iddept);
			//   $this->db->where("aprove_bod","1");
			//   $this->db->where("status","1");
			//   $this->db->where("flag_po","1");
			// 	$this->db->where("flag_status_inv_receipt","0");
				
			// 	// query khusus Inv Receipt
			// 	$this->db->or_where("flag_status_inv_receipt","1");
			// 	$this->db->where("status_send_aprove",$str_status_send);
			// 	$this->db->where("id_company",$str_idcompany);
			// 	$this->db->where("id_dept",$str_iddept);
			// 	$this->db->where("aprove_bod","1");
			// 	$this->db->where("status","1");
			// 	$this->db->where("flag_po","1");
				// query khusus Inv Receipt
			 
		      $jum=$this->db->get('qv_head_pp_complite');
		      $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
	 }




	 
	 foreach ($query->result_array() as $row_tbl) {
		  
		  If ($row_tbl['attach_quo'] != "")
		   {  
			$attach_quo = '<div  align="center"><a onClick="PDFPopup(this)" req_id='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
		   }else{
			 $attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
		   }
	 
		
		
		  $btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i></a></center>';//button detail
		
		
			
			 
			
			   If ($row_tbl['aprove_bod'] == "1")
			  {
					  $bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
				 
			   }
			  
		 
$strbodaprove  = $row_tbl["aprove_bod"] ;
$strpurchaseaprove = $row_tbl["approve_purchasing"] ;
$strfcaprove   = $row_tbl["aprove_fc"] ;
$strheadaprove = $row_tbl["aprove_head"] ;			
			

//Pembedaan Warna pada type purchase.
		if ($row_tbl['flag_purchase'] == "1") {
			$type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
		}else{
			  if ($row_tbl['flag_purchase'] == "2") {
					$type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
				}else{
					  if ($row_tbl['flag_purchase'] == "3") {
						$type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
					}else{
						  if ($row_tbl['flag_purchase'] == "4") {
								$type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
							}else{
								 $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
						  };
				  };
			  };   
		  };
//end------------------------------			 


//label colum 1 idepp
$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP

//end label
$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";

//tombol fpb......
$strprintfbp_ok = '<button class="btn btn-app btn-success btn-xs radius-4 btnpo btn-clean fpbbutton" type="button" id="fpbbutton" name="fpbbutton" req_id ='.$row_tbl["id_master"].' onclick="window.location.href="" ><i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i> </button>';
//end---------------------------
	  
		 $output['data'][]=array(
								 $idmas,
								// $btn_view_detail,
								// $attach_quo,															
								 $strprintfbp_ok,
								 $row_tbl['short'],
								 $row_tbl['user_submission'],
								 $row_tbl['dept'],
								 $header_desc ,//$row_tbl['header_desc'],
								 $row_tbl['vendor'],																								
								 date('d-m-Y', strtotime($row_tbl['date_pp'])),
								 $row_tbl['term_top']." "."Days",									 
								 $type_purchase, //$row_tbl['type_purchase'],
								 $row_tbl['currency'],
								 number_format($row_tbl['gran_total'],2,'.',','),
								 number_format($row_tbl['gran_totalppn'],2,'.',','),
										 
						   );			
	 }
	 echo json_encode($output);
 }	

	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
	  }
	  
	
	  public function get_recieved_modal(){
		$getdatadetailgrn = $this->M_print_fpb_po->m_getgrnpo();
		$no_po = $this->M_print_fpb_po->getNoPO()[0];
		$access = $this->session->userdata('apps_accsess');
		$no="1";	 	
		echo "<div style='overflow-x:auto;'>" ;
		echo "<table class='table table-striped table-bordered table-hover'>";
		echo  "<tr style='font-weight:bold; font-size:11px'>";
		echo  "<td>No</td>";
		echo  "<td>PR No</td>";
		echo  "<td>PO No</td>";
		echo  "<td >GRN No</td>";
		echo  "<td >DO No</td>";
		echo  "<td>Date GRN</td>";
		echo  "<td >Grand Total</td>";
		echo  "<td >Detail</td>";
		echo  "<td >Print</td>";

		if($access == 1 || $access == 2) {
			echo  "<td >Give Right</td>";
		}
		
		echo  "</tr> "; 	 	 	 				
		 foreach ($getdatadetailgrn as $row_det_grn) {
			 $no_pp = base64_encode($row_det_grn->id_master);
			 $number_grn = base64_encode($row_det_grn->number_grn);
			 $url = base_url('fpbpo_form/C_print_fpb_po/print_fpb?pp='.$no_pp.'&grn='.$number_grn.'#toolbar=0');
			 $url_pdf = "printJS({printable:'$url',type:'pdf',showModal:true});disableButtonPrint(this)";
			if($row_det_grn->flag_print_fpb == '0') {
				// $button_print = '<a target="_blank" OnClick=\'return confirm("Print this Data?");\' href="'.$url.'" class="btn btn-info">
				// <span class="fa fa-print"></span> Print
				// </a>';
				$button_print = '<button type="button" OnClick='.$url_pdf.' req_id="'.$url.'" class="btn btn-info">
				<span class="fa fa-print"></span> Print
				</button>';
			} else {
				$button_print = '<button type="button" class="btn btn-info" disabled>
				<span class="fa fa-print"></span> Print
				</button>';
			}
			
			 echo  "<tr style='font-size:12px'> ";
			 echo '<td>'.$no++.'</td>';	
			 echo '<td>'.$no_po->id_master.'</td>';
			 echo '<td>'.$no_po->po_no_created.'</td>';
			 echo '<td>'.$row_det_grn->number_grn.'</td>';
			 echo '<td>'.$row_det_grn->do_number.'</td>';		
			 echo '<td>'.date('d-m-Y H:i:s', strtotime($row_det_grn->date_grn)).'</td>';
			 echo '<td align="right" style="text-align: right;">'.number_format($row_det_grn->gran_total,2,'.',',').'</td>';
			 echo '<td>
			 		<button type="button" class="btn btn-success" onClick="tampilDetailGRN(this)" req_id='.$row_det_grn->number_grn.'>
					<span class="fa fa-search"></span> Detail
					</button>

					
					</a>
				  </td>';
			 echo '<td>
			 		'.$button_print.'
				  </td>';

				if($access == 1 || $access == 2) {
					echo '<td><button type="button" class="btn btn-success" onClick="giveRightFPB(this)" req_id='.$row_det_grn->number_grn.'>Give Right</button></td>';
				}

			
			 echo "</tr>"; 							
			 
		 }

		 

		 echo '</div>';

	}
  
	public function giveRightPO() {
		$id_master = $this->input->get('id_master');

		$data = array('flag_print_po' => '0', 
						'flag_po' => '0'
					);

		$update = $this->M_print_fpb_po->giveRightPO($id_master, $data);
		
		if($update) {
			echo json_encode('Success');
		} else {
			echo json_encode('Failed');
		}
	}

	public function closingPO() {
		$id_master = $this->input->get('id_master');

		$data = array('flag_recived' => '2'
					);

		$data_request = $this->M_print_fpb_po->getDataMasterPP($id_master)[0];
        
        $kirim_email = $this->kirim_email_closing_po($data_request);

            if($kirim_email == 'ok') {
                $update = $this->M_print_fpb_po->closingPO($id_master, $data);
                $callback = array(  'status_email' => 'ok',
                                    'status_close' => 'Closing PO',
                                    'error_email' => null
                                );
            } else {
                $callback = array(  'status_email' => 'fail',
                                    'status_close' => 'Closing PO Gagal',
                                    'error_email' => $kirim_email
                                );
            }

        echo json_encode($callback);	
	}

	public function kirim_email_closing_po($data_request){        
        //echo '<pre>';print_r($this->session->userdata());die;
        $this->load->library('email');  
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html';
        $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
        $config['smtp_pass']= "3mim@zd4!";
        $config['validation'] = TRUE;     

        $this->email->initialize($config);                                                                               

        $sender_name =  "epurchase_fpb";       ;                                              

        $this->email->from($sender_name);
		
		$email_pp = $data_request->email_pp;
		$email_cc_pp = $data_request->email_cc_pp;
		$email_fc_pp = $data_request->email_fc_pp;

        //end-----------------------------------------

        $to_email = $email_pp.','.$email_cc_pp.','.$email_fc_pp;

        $this->email->to($to_email);
        $this->email->subject('PO : '.$data_request->id_master.' has been Closed');
        $data['data_request'] = $data_request;
        $data['status_po'] = 'PO has been Closed';
        $email_body = $this->load->view('fpbpo_form/V_content_email',$data,true);
        //return $email_body;die;
        
       
        $this->email->message($email_body);
        
        if($this->email->send()){

            return 'ok';

        } else {
            return $this->email->print_debugger();
            //print_r($this->email->print_debugger());
        }


    }


	public function giveRightFPB() {
		$number_grn = $this->input->get('number_grn');

		$data = array('flag_print_fpb' => '0'
					);

		$update = $this->M_print_fpb_po->giveRightFPB($number_grn, $data);
		
		if($update) {
			echo json_encode('Success');
		} else {
			echo json_encode('Failed');
		}
	}

	public function print_po() {
		$no_pp = base64_decode($this->input->get('no_pp'));

		// Print PDF with TCPDF

		$master_pp = $this->M_print_fpb_po->getDataMasterPPForPrint($no_pp)[0];
		$detail_pp = $this->M_print_fpb_po->getDetailPP($no_pp);

		$data = array(	'flag_po' => '1',
						'flag_print_po' => '1',
						'date_print_po' => date('Y-m-d H:i:s'),
						'po_no_created' => 'PO/'.$no_pp
					);

		if($master_pp->flag_print_po > 0) {
			echo '<a style="font-size: 24px" href="'.base_url('fpbpo_form/c_print_fpb_po').'"> This Data has been Printed!, please click to back </a>';
			die;
		}

		$update = $this->M_print_fpb_po->updateFlagPO($no_pp, $data);

		if($master_pp->term_top > 1) {
			$top = $master_pp->term_top. ' Days';
		} else {
			$top = $master_pp->term_top. ' Day';
		}
		
		$this->load->library('Pdf');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator('IT @Eurokars Group Indonesia');
        $pdf->SetAuthor('IT @Eurokars Group Indonesia');
        $pdf->SetTitle("Purchase Order - PO/".$no_pp);
        $pdf->SetSubject("Purchase Order - PO/".$no_pp);

         // set header and footer fonts
         $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
         $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
 
         // set default monospaced font
         $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 
         // set margins
         $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
         $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
         $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
 
         // set auto page breaks
         $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
         $pdf->setPrintHeader(false);
         //$pdf->setPrintFooter(false);
 
         // set image scale factor
 
 
         // set some language-dependent strings (optional)
         if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
             require_once(dirname(__FILE__).'/lang/eng.php');
             $pdf->setLanguageArray($l);
         }
                 $pdf->SetFont('dejavusans', '', 9);

                 $pdf->SetPrintHeader(false);
                 $pdf->SetPrintFooter(false);
 
         // add a page
         $pdf->AddPage();

         $html = '
        
         <!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<style type="text/css">
		.center {
			text-align: center;
		}

		.judul {
			text-align: center;
			font-weight: bold;
			font-size: 10px;
		}

        .garis_bawah {
            border-bottom: 1px solid black;
        }

        .disetujui { 
            text-decoration: line-through;
		}
		
		.spasi {
			line-height: 1.5;
		}

	</style>

</head>

<body>


	<br>

	<table width="100%" border="0">
		<tr>
			<td> <img src="'.base_url('asset/images/euro.jpg').'" height="20"/> </td>
			<td>&nbsp;</td>
		</tr>

	</table>

	<br>
	<br>

	<table width="100%" border="0" class="center">
		<tr>
			<td width="5%"></td>
			<td width="90%" class="judul">

			<u>PURCHASE ORDER</u>
				

			</td>

			<td width="5%"></td>
		</tr>
	</table>

	<table width="100%" border="0" class="spasi">
		<tr>
			<td width="16%">&nbsp;</td>
			<td width="3%">&nbsp;</td>
			<td width="33%">&nbsp;</td>
			<td width="18%">&nbsp;</td>
			<td width="3%">&nbsp;</td>
			<td width="27%">&nbsp;</td>
		</tr>

		<tr>
			<td > Company </td>
			<td align="center"> : </td>
			<td> PT. '.$master_pp->company.' </td>
			<td align=""> No PO </td>
			<td align="center"> : </td>
			<td> PO/'.$no_pp.' </td>
		</tr>

		<tr>
			<td > Department </td>
			<td align="center"> : </td>
			<td> '.$master_pp->dept.' </td>
			<td align=""> Printed Date </td>
			<td align="center"> : </td>
			<td> '.date('d-m-Y').' </td>
		</tr>

		<tr>
			<td > Address </td>
			<td align="center"> : </td>
			<td> '.$master_pp->address.' </td>
			<td align=""> Term Of Payment </td>
			<td align="center"> : </td>
			<td> '.$top.' </td>
		</tr>
		
		<tr>
			<td > City </td>
			<td align="center"> : </td>
			<td> '.$master_pp->city.' </td>
			<td align="">  </td>
			<td align="center">  </td>
			<td>  </td>
		</tr>

		<tr>
			<td > Phone </td>
			<td align="center"> : </td>
			<td> '.$master_pp->phone.' </td>
			<td align="">  </td>
			<td align="center"> </td>
			<td> </td>
		</tr>

		<tr>
			<td > Fax </td>
			<td align="center"> : </td>
			<td> '.$master_pp->fax.' </td>
			<td align="right"> </td>
			<td align="center">  </td>
			<td>  </td>
		</tr>

	</table>

	<br>

	<table width="100%" border="0" class="spasi">
		<tr>
			<td width="16%">&nbsp;</td>
			<td width="3%">&nbsp;</td>
			<td width="81%">&nbsp;</td>
		</tr>

		<tr>
			<td > Vendor </td>
			<td align="center"> : </td>
			<td> PT. '.$master_pp->vendor.' </td>
		</tr>

		<tr>
			<td > Address </td>
			<td align="center"> : </td>
			<td> '.$master_pp->alamat.' </td>
		</tr>

		<tr>
			<td > Phone </td>
			<td align="center"> : </td>
			<td> '.$master_pp->notelp.' </td>
		</tr>
		
		<tr>
			<td > Fax </td>
			<td align="center"> : </td>
			<td> '.$master_pp->fax_vendor.' </td>
		</tr>

		<tr>
			<td > Contact Person </td>
			<td align="center"> : </td>
			<td> '.$master_pp->contact_person_vendor.' </td>
		</tr>

	</table>

	<br>
	<br>

	<table width="100%" border="1" class="spasi">
		<tr align="center">
		
			<th width="5%"><b>No</b></th>
			<th width="18%"><b>Item Type</b></th>
			<th width="18%"><b>Description</b></th>
			<th width="18%"><b>Ref</b></th>
			<th width="5%"><b>Qty</b></th>
			<th width="18%"><b>Price</b></th>
			<th width="18%"><b>Sub Total</b></th>
		</tr>

		
		';
		$no = 1;
		$tax_ppn_pph = $master_pp->ppn - $master_pp->pph;
		foreach($detail_pp as $det) { 
		//$total_plus_tax = $det->amount_in + ($det->tax_ppn - $det->tax_pph);
		$total_plus_tax = $det->tax_detail - $det->tax_detailpph;
		$html .= '
		<tr >
			<th align="center"> '.$no++.'</th>
			<th > '.$det->desc.' </th>
			<th > '.$det->spec.' </th>
			<th > '.$master_pp->quo_reff.' </th>
			<th align="center"> '.$det->qty.' </th>
			<th align="right"> '.number_format($det->harga,2,'.',',').' &nbsp; </th>
			<th align="right"> '.number_format($det->total,2,'.',',').' &nbsp; </th>
		</tr>
		';

		}

		$html .= '</table> ';

		$html .= '
		

		<table border="0">
			<tr class="spasi">
				<td></td>
				<td></td>
			</tr>
		</table>

		<table border="1">
			<tr class="spasi">
				<td colspan="5" align="right" width="82%"><b> Total ('.$master_pp->currency.') : &nbsp; </b></td>
				<td align="right" width="18%"> <b>'.number_format($master_pp->gran_total,2,'.',',').' &nbsp; </b> </td>
			</tr>

		
			<tr class="spasi">
				<td colspan="5" align="right" ><b>PPN ('.$master_pp->currency.') :  &nbsp; </b></td>
				<td align="right"> <b>'.number_format($master_pp->ppn,2,'.',',').' &nbsp; </b> </td>
			</tr>

		
			<tr class="spasi">
				<td colspan="5" align="right" ><b>Grand Total ('.$master_pp->currency.') :  &nbsp; </b></td>
				<td align="right" > <b>'.number_format($master_pp->gran_totalppn,2,'.',',').' &nbsp; </b> </td>
			</tr>

			<tr class="spasi">
		
				<td colspan="7"> <b> Amount in words : '.terbilang($master_pp->gran_totalppn).' '.$master_pp->mention.' &nbsp; </b> </td>
			</tr>
		</table>

        
    <br>
    <br>
    <br>

    
	<table width="100%" border="0" align="center" style="display:none">
		
		<tr>
			<th></th>
			<th></th>
			<th border="1"> <b> Purchasing </b></th>
			<th border="1"> <b> Requester</b></th>
			
		</tr>

		<tr>
		<td></td>
		<td></td>
		<td border="1">
		
			<br>
			<br>
			<br>
			
			Signed

			<br>
			<br>

		</td>

		<td border="1">
		
			<br>
			<br>
			<br>
			
			Signed

			<br>
			<br>
		
		</td>
		</tr>

		<tr>
			<td></td>
			<td></td>
			<td border="1"> Fastwin Alfonso Sujoto </td>
			<td border="1"> '.$master_pp->user_submission.' </td>
		</tr>
	</table>	
    

</body>

</html>

                 
         ';

         // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        $file = 'Purchase Order - PO/'.$no_pp.'.pdf';

        // reset pointer to the last page
        $pdf->lastPage();

        $pdf->Output($file, 'I');
	}

	public function print_fpb() {
		$number_grn = base64_decode($this->input->get('grn'));
		$no_pp = base64_decode($this->input->get('pp'));

		$data = array(	'flag_fpb' => '1',
						'flag_print_fpb' => '1',
						'date_print_fpb' => date('Y-m-d H:i:s')
					);

		// Print PDF with TCPDF

		$master_pp = $this->M_print_fpb_po->getDataMasterPPForPrint($no_pp)[0];
		$master_grn = $this->M_print_fpb_po->getMasterGRN($number_grn)[0];
		$detail_grn = $this->M_print_fpb_po->getDetailGRN($number_grn);

		// Cek apakah sudah di print atau belum

		if($master_grn->flag_print_fpb > 0) {
			echo '<a style="font-size: 24px" href="'.base_url('fpbpo_form/c_print_fpb_po').'"> This Data has been Printed!, please click to back </a>';
			die;
		}

		$update = $this->M_print_fpb_po->giveRightFPB($number_grn, $data);

		if($master_pp->term_top > 1) {
			$top = $master_pp->term_top. ' Days';
		} else {
			$top = $master_pp->term_top. ' Day';
		}
			
		$this->load->library('Pdf');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator('IT @Eurokars Group Indonesia');
        $pdf->SetAuthor('IT @Eurokars Group Indonesia');
        $pdf->SetTitle("Form Permintaan Biaya - ".$number_grn);
        $pdf->SetSubject("Form Permintaan Biaya - ".$number_grn);

         // set header and footer fonts
         $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
         $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
 
         // set default monospaced font
         $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 
         // set margins
         $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
         $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
         $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
 
         // set auto page breaks
         $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
         $pdf->setPrintHeader(false);
         //$pdf->setPrintFooter(false);
 
         // set image scale factor
 
 
         // set some language-dependent strings (optional)
         if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
             require_once(dirname(__FILE__).'/lang/eng.php');
             $pdf->setLanguageArray($l);
         }
                 $pdf->SetFont('dejavusans', '', 9);

                 $pdf->SetPrintHeader(false);
                 $pdf->SetPrintFooter(false);
 
         // add a page
         $pdf->AddPage();

         $html = '
        
         <!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<style type="text/css">
		.center {
			text-align: center;
		}

		.judul {
			text-align: center;
			font-weight: bold;
			font-size: 10px;
		}

        .garis_bawah {
            border-bottom: 1px solid black;
        }

        .disetujui { 
            text-decoration: line-through;
		}
		
		.spasi {
			line-height: 1.5;
		}

		

	</style>

</head>

<body>


	<br>

	
	<table width="100%" border="0">
		<tr>
			<td> <img src="'.base_url('asset/images/euro.jpg').'" height="20"/> </td>
			<td>&nbsp;</td>
		</tr>

	</table>

	<br>
	<br>

	
	<table width="100%" border="0" class="center">
		<tr>
			<td width="5%"></td>
			<td width="90%" class="judul">

			<u>FORM PERMINTAAN BIAYA</u>
			<br>
			<u>( PAYMENT REQUEST FORM )</u>
				

			</td>

			<td width="5%"></td>
		</tr>
	</table>

	<br>
	<br>

	<table width="100%" border="0" class="spasi">
		<tr>
			<td width="16%">&nbsp;</td>
			<td width="3%">&nbsp;</td>
			<td width="30%">&nbsp;</td>
			<td width="18%">&nbsp;</td>
			<td width="3%">&nbsp;</td>
			<td width="30%">&nbsp;</td>
		</tr>

		<tr>
			<td > Company </td>
			<td align="center"> : </td>
			<td> PT. '.$master_pp->company.' </td>
			<td align=""> No PO </td>
			<td align="center"> : </td>
			<td> '.$master_pp->po_no_created.' </td>
		</tr>

		<tr>
			<td > Department </td>
			<td align="center"> : </td>
			<td> '.$master_pp->dept.' </td>
			<td align=""> No GRN </td>
			<td align="center"> : </td>
			<td> '.$master_grn->number_grn.' </td>
		</tr>

		<tr>
			<td > Address </td>
			<td align="center"> : </td>
			<td> '.$master_pp->address.' </td>
			<td align=""> No DO </td>
			<td align="center"> : </td>
			<td> '.$master_grn->do_number.' </td>
		</tr>
		
		<tr>
			<td > City </td>
			<td align="center"> : </td>
			<td> '.$master_pp->city.' </td>
			<td align=""> Printed Date </td>
			<td align="center"> : </td>
			<td> '.date('d-m-Y').' </td>
		</tr>

		<tr>
			<td > Phone </td>
			<td align="center"> : </td>
			<td> '.$master_pp->phone.' </td>
			<td align=""> Term Of Payment </td>
			<td align="center"> : </td>
			<td> '.$top.' </td>
		</tr>

		<tr>
			<td > Fax </td>
			<td align="center"> : </td>
			<td> '.$master_pp->fax.' </td>
			<td align="right"> </td>
			<td align="center">  </td>
			<td>  </td>
		</tr>

	</table>

	<br>

	<table width="100%" border="0" class="spasi">
		<tr>
			<td width="16%">&nbsp;</td>
			<td width="3%">&nbsp;</td>
			<td width="81%">&nbsp;</td>
		</tr>

		<tr>
			<td > Vendor </td>
			<td align="center"> : </td>
			<td> PT. '.$master_pp->vendor.' </td>
		</tr>

		<tr>
			<td > Address </td>
			<td align="center"> : </td>
			<td> '.$master_pp->alamat.' </td>
		</tr>

		<tr>
			<td > Phone </td>
			<td align="center"> : </td>
			<td> '.$master_pp->notelp.' </td>
		</tr>
		
		<tr>
			<td > Fax </td>
			<td align="center"> : </td>
			<td> '.$master_pp->fax_vendor.' </td>
		</tr>

		<tr>
			<td > Contact Person </td>
			<td align="center"> : </td>
			<td> '.$master_pp->contact_person_vendor.' </td>
		</tr>

	</table>

	<br>
	<br>

	<table width="100%" border="1">
		<tr align="center">
		
			<th width="5%"><b>No</b></th>
			<th width="27%"><b>Item Type</b></th>
			<th width="22%"><b>Description</b></th>
			<th width="6%"><b>Qty</b></th>
			<th width="20%"><b> Unit Price</b></th>
			<th width="20%"><b>Sub Total</b></th>
		</tr>

		
		';
		$no = 1;
		$tax_ppn_pph = $master_grn->total_ppn - $master_grn->total_pph;
		foreach($detail_grn as $det) { 
		//$total_plus_tax = $det->amount_in + ($det->tax_ppn - $det->tax_pph);
		$total_plus_tax = $det->tax_ppn - $det->tax_pph;
		$html .= '
		<tr class="spasi">
			<th align="center"> '.$no++.'</th>
			<th > '.$det->description.' </th>
			<th > '.$det->spec.' </th>
			<th align="center"> '.$det->qty_in.' </th>
			<th align="right"> '.number_format($det->price,2,'.',',').' &nbsp; </th>
			<th align="right"> '.number_format($det->amount_in,2,'.',',').' &nbsp; </th>
		</tr>
		
		';

		}

		$html .= '</table> ';

		$html .= '
		

		<table border="0">
			<tr class="spasi">
				<td colspan="4"></td>
				<td style="border-left: 1px solid black; border-right: 1px solid black;"></td>
			</tr>
		</table>

		<table border="1">
			<tr class="spasi">
				<td colspan="5" align="right" width="80%"><b>  Total ('.$master_pp->currency.') :  &nbsp; </b></td>
				<td align="right" width="20%"> <b>'.number_format($master_grn->total_amount,2,'.',',').' &nbsp; </b> </td>
			</tr>

		
			<tr class="spasi">
				<td colspan="5" align="right" width="80%"><b>PPN ('.$master_pp->currency.') :  &nbsp; </b></td>
				<td align="right" width="20%"> <b>'.number_format($master_grn->total_ppn,2,'.',',').' &nbsp; </b> </td>
			</tr>

		
			<tr class="spasi">
				<td colspan="5" align="right" width="80%"><b>Grand Total ('.$master_pp->currency.') :  &nbsp; </b></td>
				<td align="right" width="20%"> <b>'.number_format($master_grn->gran_total,2,'.',',').' &nbsp; </b> </td>
			</tr>

			<tr class="spasi">
		
				<td colspan="7"> <b> Amount in words : '.terbilang($master_pp->gran_totalppn).' '.$master_pp->mention.' &nbsp; </b> </td>
			</tr>
		</table>

    <br>
    <br>

	<table width="100%" border="1">

	<tr align="center">
		<td colspan="2" width="30%"> <b> Approved By </b></td>
		<td width="35%"><b> H.O.D</b></td>
		<td width="35%"><b> Requester</b></td>
	</tr>

	<tr>
		<td width="20%" rowspan="2" style="border-right: none solid white"> 
		
		<br><br>
		&nbsp; BOD 
		<br>
		&nbsp; Finance Controller
		<br>
		&nbsp; Purchase
		<br>
		&nbsp; HOD
		<br>

		</td>

		<td align="center" width="10%" rowspan="2" style="border-left: none solid white">
		
			<br><br>
			&nbsp; &#9746;
			<br>
			&nbsp; &#9746;
			<br>
			&nbsp; &#9746;
			<br>
			&nbsp; &#9746;
			<br>
		
		</td>
		<td width="35%" align="center">
		
			<br>
			<br>
			<br>
			
			Signed

			<br>
			<br>

		</td>

		<td width="35%" align="center">
		
			<br>
			<br>
			<br>
			
			Signed

			<br>
			<br>
		
		</td>
	</tr>

	<tr>
			<td border="1" align="center"> Fastwin Alfonso Sujoto </td>
			<td border="1" align="center"> '.$master_pp->user_submission.' </td>
		</tr>

	</table>

	<br>
	<br>
	
	

</body>

</html>

                 
         ';

         // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        $file = 'Form Permintaan Biaya - '.$number_grn.'.pdf';

        // reset pointer to the last page
        $pdf->lastPage();

        $pdf->Output($file, 'I');
	}

	public function get_history_grn()
    {		
	
		$number_grn = $this->input->get('number_grn');
     $master_grn = $this->M_print_fpb_po->getMasterGRN($number_grn);
	 $detail_grn = $this->M_print_fpb_po->getDetailGRN($number_grn);
	 $currency = $this->M_print_fpb_po->getCurrency($master_grn[0]->id_master)[0];
	  
	 $no="1";	 	
	 echo  "<div style='overflow-x:auto'>";
	 echo  "<table class ='table table-striped table-bordered table-hover'>";
	 echo  "<thead>" ;
	 echo  "<tr style='font-weight:bold; font-size:11px'>";
	 echo  "<td width='2%'>No</td>";
	 echo  "<td width='22%'>Item Type</td>";
	 echo  "<td width='22%'>Description</td>";
	 echo  "<td width='7%'>Qty</td>";
	 echo  "<td width='15%'>Unit Price</td>";
	 echo  "<td width='25%'>Total Price</td>";
	 echo  "<td width='7%'>PPN</td>";
	 echo  "<td width='7%'>PPH</td>";
	 echo  "<td width='25%'>PPN Amount</td>";
	 echo  "<td width='25%'>PPH Amount</td>";
	 echo  "<td width='25%'>Total + Tax</td>";
	 echo  "</tr> "; 
	 echo  "</thead>";	 	 	 				
		foreach ($detail_grn as $row_jq) {
			$total_plus_tax = $row_jq->amount_in + ($row_jq->tax_ppn - $row_jq->tax_pph);
			

		    echo  "<tr style='font-size:12px'> ";
			echo '<td>'.$no++.'</td>';			
			echo '<td>'.$row_jq->description.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			echo '<td align="center" style="text-align: center;">'.$row_jq->qty_in.'</td>';
			echo '<td align="right" style="text-align: right;">'.number_format($row_jq->price,2,'.',',').'</td>';
			echo '<td align="right" style="text-align: right;">'.number_format($row_jq->amount_in,2,'.',',').'</td>';
			echo '<td>'.$row_jq->type_ppn.'</td>';
			echo '<td>'.$row_jq->type_pph.'</td>';
			echo '<td align="right" style="text-align: right;">'.number_format($row_jq->tax_ppn,2,'.',',').'</td>';
			echo '<td align="right" style="text-align: right;">'.number_format($row_jq->tax_pph,2,'.',',').'</td>';
			echo '<td align="right" style="text-align: right;">'.number_format($total_plus_tax,2,'.',',').'</td>';
		    echo "</tr>"; 							
			
		}
		
	 foreach ($master_grn as $row_jm) {	
		
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>Total (".$currency->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>".number_format($row_jm->total_amount,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>PPN Amount  (".$currency->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>".number_format($row_jm->total_ppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 	
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>PPH Amount  (".$currency->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>(".number_format($row_jm->total_pph,2,'.',',').")</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>Grand Total  (".$currency->currency.") : </td>";
		 echo  "<td width='40%' align='right' style='text-align: right;'>".number_format($row_jm->gran_total,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 
	 }

	 echo "</table>";	
		 echo "</div>";
	}

	public function add_grn_new() {
		//echo '<pre>';print_r($this->input->post());

		$data['txtnopp'] = $this->input->post('txtnopp');
		$data['txtnogrn'] = $this->input->post('txtnogrn');  	
		$data['txtsub']	= $this->input->post('txtsub');		
		$data['txtvendor'] = $this->input->post('txtvendor'); 	
		$data['txtcurr'] = $this->input->post('txtcurr'); 
		$data['txtremarks'] = $this->input->post('txtremarks');
		$data['full_receive'] = $this->input->post('full_receive');
		$data['number_pay'] = $this->input->post('number_pay');
		$data['txtremainingprice'] = $this->input->post('txtremainingprice');
		$data['do_number'] = $this->input->post('do_number');
		$data['date_send'] = date("Y-m-d H:i:s");

		$data['txtiddet'] = $this->input->post('txtiddet');
		$data['txtdesc'] = $this->input->post('txtdesc');
		$data['txtspecs'] = $this->input->post('txtspecs');
		$data['txtqty'] = $this->input->post('txtqty');
		$data['txtprice'] = $this->input->post('txtprice');
		$data['txtamount'] = $this->input->post('txtamount');
		$data['cbotax'] = $this->input->post('cbotax');
		$data['cbotaxpph'] = $this->input->post('cbotaxpph');
		$data['txttaxchargein'] = $this->input->post('txttaxchargein');
		$data['txttaxchargepphin'] = $this->input->post('txttaxchargepphin');
		$data['txtqtyreceived'] = $this->input->post('txtqtyreceived');
		$data['txtqtyin'] = $this->input->post('txtqtyin');
		$data['txtqtyoutstanding'] = $this->input->post('txtqtyoutstanding');
		$data['txtamountin'] = $this->input->post('txtamountin');
		$data['txtamountreceived'] = $this->input->post('txtamountreceived');
		$data['txtremainingprice'] = $this->input->post('txtremainingprice');

		/* kalo $full_receive = 0, itu artinya sudah di receive semua, 
		  saya ambil dari qty outstanding #Bernand */
		if($data['full_receive'] == '0') {
			$data['full_receive'] = '1';
	  	} else {
			$data['full_receive'] = '0';
		}

		$data['data_update_flag_receive'] = array('flag_recived' => $data['full_receive']);

		//---------------------unformat grandtotal---------
		$data['txt_grand_total'] = $this->input->post('result'); 
		if ( strstr( $data['txt_grand_total'], ',' ) ) $data['txt_grand_total'] = str_replace( ',','', $data['txt_grand_total'] );  
		//-------------------------------------
		
		//---------------------unformat ppn---------
		$data['txt_ppn'] = $this->input->post('txtppn'); 
		if ( strstr( $data['txt_ppn'], ',' ) ) $data['txt_ppn'] = str_replace( ',','', $data['txt_ppn'] );  
		//-------------------------------------
		
			//---------------------unformat ppn---------
		$data['txt_pph'] = $this->input->post('txtpph'); 
		if ( strstr( $data['txt_pph'], ',' ) ) $data['txt_pph'] = str_replace( ',','', $data['txt_pph'] );  
		//-------------------------------------
		
		//---------------------unformat grandtotalppn---------
		$data['txt_grand_totalppn'] = $this->input->post('gpppn'); 
		if ( strstr( $data['txt_grand_totalppn'], ',' ) ) $data['txt_grand_totalppn'] = str_replace( ',','', $data['txt_grand_totalppn'] ); 
		

		// Upload Attach DO
		$config['upload_path']          = './asset/upload_do';
		$config['allowed_types']        = 'pdf';
		$config['max_size']             = 2500;

		// Set file name upload
		$config['file_name'] = str_replace('/','-',$data['txtnogrn']);
		$data['attach_do'] = $config['file_name'].'.pdf';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('userfile')) {
            
            $message_upload = array('error' => $this->upload->display_errors());
            $status_upload = 'gagal';

            $callback = array('status_upload' => $status_upload,
                                'message_upload' => $message_upload,
								'status_update_flag_received' => false,
								'status_insert_master_grn' => false,
								'status_insert_detail_grn' => false
                            );

			echo json_encode($callback);
        } else {
			/* Insert ke tbl_master_grn */
			/* Update flag received to tbl_master_pp */
			$insert_and_update_flag_received = $this->M_print_fpb_po->add_grn_new($data);
			echo json_encode($insert_and_update_flag_received);
		}

		

	}
	
}

