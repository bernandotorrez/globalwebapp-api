<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_child extends CI_Model
{
	var $table = 'tbl_child_menu';
	var $tablenew = 'qv_child';
	var $column_order = array(null, null, 'id_child','parent_menu_desc','order_child','desc_child','anchor_child_url','date_create'); //set column field database for datatable orderable
	var $column_search = array('id_child','parent_menu_desc','order_child','desc_child','anchor_child_url','date_create'); //set column field database for datatable searchable 
	var $order = array('id_child' => 'desc'); // default order 
	
    
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	private function _get_datatables_query()
	{
		
		$this->db->from($this->tablenew);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}

		if($_POST['start_date'] != '' && $_POST['end_date'] != '') // here order processing
		{
			$start_date = $_POST['start_date'];
			$end_date = $_POST['end_date'];

			$start_date = date("Y-m-d", strtotime($start_date));
			$end_date = date("Y-m-d", strtotime($end_date));
			//$this->db->where('date_create BETWEEN "'.$start_date. '" and "'.$end_date.'"');

			$this->db->where('date_create >=', $start_date.' 00:00:00');
			$this->db->where('date_create <=', $end_date.' 23:59:59');
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->tablenew);
		return $this->db->count_all_results();
	}

	public function get_all_child()
		{
			$this->db->from('tbl_child_menu');
			$query=$this->db->get();
			return $query->result();

		}
	
	
	public function get_all_parent()
		{
			$this->db->from('tbl_parent_menu');
			$query=$this->db->get();
			return $query->result();

		}
	
	public function get_all_mix($data,$field)
		{
			$this->db->from('tbl_parent_menu');
			$this->db->where('id_parent', $data );
			$query=$this->db->get();
			$aaa = $query->result();
			if($aaa){
				return $aaa[0]->$field;
			}else{
				return $aaa;
			}

		}

	public function get_by_id($id)
		{
			$this->db->from($this->table);
			$this->db->where('id_child',$id);
			$query = $this->db->get();
			return $query->row();
		}
	
	public function get_by_aaa($id)
		{
			$this->db->from('tbl_parent_menu');
			$this->db->where('id_parent',$id);
			$query = $this->db->get();
			return $query->row();
		}
	
	public function get_by_bbb($id)
		{
			$this->db->from($this->table);
			$this->db->where('id_parent',$id);
			$query = $this->db->get();
			return $query->row();
		}
		
	public function add_child($data)
		{
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}

	public function update_child($where, $data)
		{
			$this->db->update($this->table, $data, $where);
			return $this->db->affected_rows();
		}

	public function delete_by_id($id)
		{
			$this->db->where('id_child', $id);
			$this->db->delete($this->table);
		}
		
	public function get_child($id)
		{
        $q = $this->db->query("select * from tbl_child_menu left join tbl_parent_menu on tbl_child_menu.id_parent=tbl_parent_menu.id_parent where tbl_child_menu.id_parent='$id'");
        return $q;
		}
		
	public function get_all_count($id)
		{
			$this->db->select('count(order_child) as totaldata');
			$this->db->where('id_parent', $id);
			$this->db->from('tbl_child_menu');
			$query=$this->db->get();
			return $query->result();
		}
	
	public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}


}
