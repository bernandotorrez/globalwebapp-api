<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_child extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_child');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_child->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}	
	 	}
		
	public function index()
		{

		$data['child']=$this->M_child->get_all_child();
		$data['parent']=$this->M_child->get_all_parent();
		$data['parent2']=$this->M_child->get_all_parent();
		$data['show_view'] = 'child_form/V_child';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		}

	public function ajax_list()
	{
		$list = $this->M_child->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_child_menu) {
			$chk_idmaster ='<div align="center"><input id="checkchild" name="checkchild" type="checkbox" value='.$tbl_child_menu->id_child.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_child_menu->id_child;
			$row[] = $tbl_child_menu->parent_menu_desc;
			$row[] = $tbl_child_menu->order_child;
			$row[] = $tbl_child_menu->desc_child;
			$row[] = $tbl_child_menu->anchor_child_url;
			$row[] = $tbl_child_menu->date_create;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_child->count_all(),
						"recordsFiltered" => $this->M_child->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_child');
		$this->db->from('tbl_child_menu');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_child",$search);
		$this->db->or_like("id_parent",$search);
		$this->db->or_like("order_child",$search);
		$this->db->or_like("desc_child",$search);
		$this->db->or_like("anchor_child_url",$search);
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_child','ASC');
		$query=$this->db->get('tbl_child_menu');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("id_child",$search);
		$jum=$this->db->get('tbl_child_menu');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $tbl_child_menu) {
		$chk_idmaster ='<div align="center"><input id="checkchild" name="checkchild" type="checkbox" value='.$tbl_child_menu["id_child"].' class="editRow ace" req_id_del='.$tbl_child_menu["id_child"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($tbl_child_menu['id_child'],$tbl_child_menu['id_parent'],
									$tbl_child_menu['order_child'],$tbl_child_menu['desc_child'],
									$tbl_child_menu['anchor_child_url'],$chk_idmaster
									);
		}
		echo json_encode($output);
}

public function crudchild(){
	$this->load->view('formcrudchild');
}
										
public function add_child(){

	// Log
	$id = $this->session->userdata('id');
	$username = $this->session->userdata('username');
	$nama = $this->session->userdata('name');
	$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
	$date_add = date('Y-m-d H:i:s');

	if(empty($_POST["id_parent"])){
		die ("Field ID Parent must be filled in!! ");
	}
	elseif(empty($_POST["order_child"])){
		die ("Field Position Child must be filled in!! ");
	}
	elseif(empty($_POST["desc_child"])){
		die ("Field Name Child must be filled in!! ");
	}
	elseif(empty($_POST["anchor_child_url"])){
		die ("Field URL Child must be filled in!! ");
	}
	else{
		$data = array(
					'id_parent' => $this->input->post('id_parent'),
					'order_child' => $this->input->post('order_child'),
					'desc_child' => $this->input->post('desc_child'),
					'anchor_child_url' => $this->input->post('anchor_child_url'),
					'date_create' => $date_add,
					'remarks' => $remarks_add
				);
			$insert = $this->M_child->add_child($data);
		  }
		  echo 'Insert';
		}
	
public function update_child(){
	// Log
	$id = $this->session->userdata('id');
	$username = $this->session->userdata('username');
	$nama = $this->session->userdata('name');
	$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
	$date_update = date('Y-m-d H:i:s');

	if(empty($_POST["id_parent"])){
		die ("Field ID Parent must be filled in!! ");
	}
	elseif(empty($_POST["order_child"])){
		die ("Field Position Child must be filled in!! ");
	}
	elseif(empty($_POST["desc_child"])){
		die ("Field Name Child must be filled in!! ");
	}
	elseif(empty($_POST["anchor_child_url"])){
		die ("Field URL Child must be filled in!! ");
	}
	else{
		$data = array(
					'id_parent' => $this->input->post('id_parent'),
					'order_child' => $this->input->post('order_child'),
					'desc_child' => $this->input->post('desc_child'),
					'anchor_child_url' => $this->input->post('anchor_child_url'),
					'date_update' => $date_update,
					'remarks' => $remarks_update
				);
			$this->M_child->update_child(array('id_child' => $this->input->post('id_child')), $data);
			}
	echo 'Insert';
}

	public function view_parent()
		{
			$this->db->from('tbl_parent_menu');
			$query=$this->db->get();
			return $query->result();
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_child->get_by_id($id);
			echo json_encode($data);
		}
		
	public function ajax_parent()
		{
			$data = $this->M_child->get_by_aaa();
			echo json_encode($data);
		}
	
	public function ajax_parentnew()
    {
        $id = $this->input->post('id_parent');
        $data['child'] = $this->M_child->get_child($id);
		$data['child2'] = $this->M_child->get_all_count($id);
        $this->load->view('child',$data);
    }


	
	
	public function update_position()
		{
				$count = 0;
				$displayed_city = array();
				$city = array();

				foreach ($_POST['pos'] as $pos_key => $pos_value){
				  $jumlah = count($_POST['pos']); 
				  $count	++;
				  $city = '';
				  if( isset($pos_value)){
					$city = $pos_value;
					if(!in_array($city, $displayed_city)){
					  $displayed_city[] = $city;
					  $total = count($displayed_city);
					}
				  }
		}
		 if($total!=$jumlah){
			die('Duplicate Data !');
			
		  }else{
			echo "Insert";
			   foreach ($_POST['pos'] as $pos_key => $pos_value){
				$dede[$pos_key] = $pos_value;
				$parentnya['order_child'] = $pos_value;
				$this->db->where("id_child",$pos_key);
				$this->db->update("tbl_child_menu",$parentnya);
			 
		   }
}
}

	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$this->db->where('id_child', $id);
				$this->db->delete('tbl_child_menu');
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}



}
