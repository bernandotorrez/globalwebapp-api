<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_print_bstk extends MY_Controller
{
 	
  public function __construct()
    {
		parent::__construct();					
	    $this->load->model('m_print_bstk/M_print_bstk','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library		
		$this->load->library('fpdf');   	
		$this->load->helper('form');
		$this->load->helper('terbilang');			
		$this->load->helper('url');			
		$this->load->database();	
	}
	
	public function index()
    {		
	       $this->show_table(); //manggil fungsi show_table		    		   		
	}	
		
	 public function show_table()
    {             					
	     $tampil_table_sell= $this->M_print_bstk->tampil_add_sell()->result();	
	     $total_rows =$this->M_print_bstk->tampil_add_sell()->num_rows();
	  									
	  if ($tampil_table_sell)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(5);
		$per_page =5;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'mazda_stock_sales/c_print_bstk/C_print_bstk/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 5;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['sell_view'] =$this->M_print_bstk->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
			
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
		$data['ceck_row'] = $total_rows;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'v_print_bstk/V_print_bstk';		
		$this->load->view('dashboard/Template',$data);				
	  }else{		   
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data sales unitt table is empty';		
		$data['show_view'] = 'v_print_bstk/V_print_bstk';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }	
  
  //------------------------------------VIEW pashing result to modal popup-------------------
	public function get_idtrans_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $idtrans= $this->input->post('id_trans_sales',TRUE); //       
	     $query_master = $this->db->query("select * from qv_complite_master_sell where id_trans_sales ='".$idtrans."'");  
         $query_detail = $this->db->query("select * from tbl_sales_unit_detail where id_trans_sales ='".$idtrans."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   		   
		      $data['str_trans_master']=$query_master->result();
			  $data['str_trans_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_idtrans'] = $idtrans ;
			  $tampil_detail = $this->load->view('v_print_bstk/V_conten_detail',$data,true);			 
			  echo $tampil_detail ;			 
		   }							   
    }    
	
	
	public function do_upload_pdffaktur()
	{
	    $str_idtransales =  str_replace('/','', $this->input->post("id_trans_sales"));
		$stridtransles_update = $this->input->post("id_trans_sales");
	    $str_path_upload = './asset/uploads/ES';
		$str_path_table = './asset/uploads/ES/'.$str_idtransales.".pdf";
		
		
	   
	    $config['upload_path'] = $str_path_upload;
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '12000kb';
		$config['overwrite'] = TRUE;
		$config['file_name']= $str_idtransales.'.pdf'; //file name nya

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( !$this->upload->do_upload('fileSelect'))
		{
			 $error = array('error' => $this->upload->display_errors());
		     print_r($error);
		}else{
		   	 echo 'Upload file '.$str_idtransales.'.pdf successfull';
			 $data = array('upload_data' => $this->upload->data());
			 
		    //update_path_faktur------------------------------------------
			 $data_upload_fak = array('flag_upload_faktur'=> '1' ,
							   'path_up_fatktur'  => $str_path_table
						   );
			 $this->db->where('id_trans_sales',$stridtransles_update);		    
			 $this->db->update('tbl_sales_unit_transaksi',$data_upload_fak);			  
			//endupload----------------------------------------------------
		}
		
		

	}
  		
	
	public function do_print_bstk()
  	{		
	   if($this->M_print_bstk->ever_print_bstk()) :
	      $this->session->set_flashdata('pesan','BSTK ALREADY PRINTED !!!!!!....'); 					
	      redirect('mazda_stock_sales/c_print_bstk/c_print_bstk');	
	   else:
			  if($this->M_print_bstk->get_data_forcetak_bstk()) :		 
				 $hasil_get_databstk = $this->M_print_bstk->get_data_forcetak_bstk();											            	 $res['data_bstk'] =$hasil_get_databstk ;
				 $res['data_branch'] =$this->M_print_bstk->get_branch();
				 $this->load->view('v_print_bstk/V_rptpdf_bstk',$res);
			  else:    		
				 $this->session->set_flashdata('pesan','PRINT BSTK FAILED !!!!!! ....'); 					
				 redirect('mazda_stock_sales/c_print_bstk/c_print_bstk');		         				
			 endif;
	  endif;		 
		
	}
	
	public function do_print_kwetansi()
  	{		
	   //if($this->M_print_bstk->ever_print_kwetansi()) :
	   //   $this->session->set_flashdata('pesan','Kwetansi ALREADY PRINTED !!!!!!....'); 					
	     // redirect('mazda_stock_sales/c_print_bstk/c_print_bstk');	
	   //else:
			  if($this->M_print_bstk->get_data_forcetak_kwetansi()) :		 
				 $hasil_get_datakwe = $this->M_print_bstk->get_data_forcetak_kwetansi();											            	 $res['data_bstk'] =$hasil_get_datakwe ;
				 $res['data_branch'] =$this->M_print_bstk->get_branch();
				 $this->load->view('v_print_bstk/V_rptpdf_kwetansi',$res);
			  else:    		
				 $this->session->set_flashdata('pesan','PRINT kwetansi FAILED !!!!!! ....'); 					
				 redirect('mazda_stock_sales/c_print_bstk/c_print_bstk');		         				
			 endif;
	 // endif;		 
		
	}
	
	public function do_search_data()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_print_bstk->search_data(); // manggil hasil cari di model 										
		$branch  = $this->session->userdata('name_branch') ; 
	    $company = $this->session->userdata('company') ;		
		
		if($tampung_cari == null){		   		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = "0";	
			$data['sell_view']  = "";
			$data['show_view'] = 'v_print_bstk/V_print_bstk';
			$this->load->view('dashboard/Template',$data);			
		}else{				   		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = "1";	
			$data['sell_view']  = $tampung_cari;				
			$data['show_view'] = 'v_print_bstk/V_print_bstk';
			$this->load->view('dashboard/Template',$data);	 
		}	  
    }
	
	public function multiple_submit()
	{
		if ($this->input->post('btnbstk')):
		   	$this->do_print_bstk();
		else:
		   if ($this->input->post('btncari')):
		   	   $this->do_search_data();	
		   else:  
		      if ($this->input->post('btnsavedetail')):
				 $this->do_insert_edit_detail_sell();
			  else:
			    if ($this->input->post('btn-kuetansi')):
				   $this->do_print_kwetansi();
			    else:
					  if ($this->input->post('btnaddeditdetail')) :
						  $this->get_add_edit_idtrans_modal();
					  else:	
						 redirect('mazda_stock_sales/c_print_bstk/c_print_bstk');
					  endif;	
			     endif;		  
				endif;	  
			endif;		
		endif;	
	}
	
	public function get_add_edit_idtrans_modal() {   
	
	if ($this->M_print_bstk->check_lunas_payment()) : 
		 $this->session->set_flashdata('pesan_fail','Payement already balance!! ....'); 
		 redirect('mazda_stock_sales/c_print_bstk/c_print_bstk');
	else:
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	    // $idtrans= $this->input->post('id_trans_sales_edit',TRUE); //       
		 $idtrans= $this->input->post('msg'); //   
		 
		for ($int=0;$int < count($idtrans);$int++) :    
	     $query_master = $this->db->query("select * from qv_complite_master_sell where id_trans_sales ='".$idtrans[$int]."'");  
         $query_detail = $this->db->query("select * from tbl_sales_unit_detail where id_trans_sales ='".$idtrans[$int]."'and status ='".$status_aktif_record."'");  
		endfor;
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0) :	   		   
		      $data['str_trans_master']=$query_master->result();
			  $data['str_trans_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_idtrans'] = $idtrans ;				 
			  $data['show_view'] =  'v_print_bstk/V_conten_add_edit_detail';					
			  $this->load->view('dashboard/Template',$data);	
	 	   else:		   		
			   $data['str_trans_master']=$query_master->result();	
			   $data['str_trans_detail']="";		     
			   $data['intno']="";			  			  			  
			   $data['phasing_idtrans'] = $idtrans ;				 
			   $data['show_view'] =  'v_print_bstk/V_conten_add_edit_detail';					
			   $this->load->view('dashboard/Template',$data);   		   		  		 			  			 
		   endif;	
	  endif ;	   						   
    }      	
	
	public function do_insert_edit_detail_sell()		
	{	
				  
			if ($this->M_print_bstk->insert_edit_detail()) :			
								
				$this->do_upload(); //manggil fungsi Upload File							 			 						
				  if ($this->session->flashdata('pesan_upload_succes') !=""):				
					  $str_upload_detail_success = "& Upload Foto";
				  else:
					  $str_upload_detail_success = "";
				  endif;  
							
				  $this->session->set_flashdata('pesan_succes','Update Customer Payment Successfull..!!');				
				  redirect('mazda_stock_sales/c_print_bstk/c_print_bstk');	 
			else:		
				  echo $this->session->set_flashdata('pesan_fail','Update Customer Payment fail or Some Mandatory Input Is Empty! ');
				 redirect('mazda_stock_sales/c_print_bstk/c_print_bstk');	  			
			endif;								
	
	}		
	
	public function do_upload()
	{		
		$str_branch = $this->session->userdata("branch_short");							
		$str_nofototrans = $this->session->userdata('ses_name_apload'); 		
		
		$config['upload_path'] = './asset/upload_foto/'.$str_branch;		
		$config['allowed_types'] = 'pdf';	
		$config['max_size']	= '40000kb';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$config['overwrite'] = TRUE;
		$config['file_name']= $str_nofototrans; 

		$this->load->library('upload', $config);
		
		if ( !$this->upload->do_upload()) :			   
			$error = array('error' => $this->upload->display_errors());
			//$this->session->set_flashdata('pesan_fail',$str_nofototrans.implode(" ",$error));				
			$this->session->set_flashdata('pesan_fail',implode(" ",$error));				
		else :			 
			$data = array('upload_data' => $this->upload->data());		
			$this->session->set_flashdata('pesan_upload_succes',"1");		
		endif;		
		$this->session->unset_userdata('ses_name_apload'); 											
	}	
	
	
}