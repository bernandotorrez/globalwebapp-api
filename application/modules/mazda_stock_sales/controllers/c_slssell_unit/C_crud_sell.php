<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_crud_sell extends MY_Controller
{
  public function __construct()
        {
            parent::__construct();		
			$this->load->helper(array('form', 'url'));    	   
		    $this->load->model('m_slssell_unit/M_crud_sell','C_crud_sell',TRUE);		
			/* di koment karena library fpdf deprecated, by Bernand
			$this->load->library('fpdf');  
			*/
			$this->load->library('fpdf'); 
            $this->load->library('form_validation');
			$this->load->helper('terbilang');
			$this->load->helper('form');
			$this->load->helper('url');			
		    $this->load->database();					
        }		  	
		
  public function index()
	{	 
	   
	  $data['tampil_sales']=$this->C_crud_sell->get_sales();
	  $data['tampil_customer']=$this->C_crud_sell->getAllDataCustomer();		
	  $data['tampil_payment_type']=$this->C_crud_sell->get_tampil_payment();	
	  $data['transnumber']=$this->C_crud_sell->get_trans_number();  		   		  	 
	  $data['show_view']='v_slsstock_selling/V_form_addnew_sell';
	  $this->load->view('dashboard/Template',$data);		
	}
		
	function select_warna_combo_jquery(){
		 		
		 $result_warna= $this->C_crud_stock->get_color(); //from model							 
		 foreach ($result_warna->result() as $row_warna):
			 echo '<option value="'.$row_warna->id_colour.'">'.$row_warna->colour.'</option>';
		 endforeach;	
    } 
	
	function doget_price_unit_jquery()
	{
		$result_sellprice = $this->C_crud_stock->get_price_unit();//from model							  		     		 		 					
			foreach ($result_sellprice->result() as $row_price):				
				$data['price_sell'] = $row_price->price_sell;
				$data['currency'] = $row_price->currency; 			
			endforeach;	
			
		echo json_encode($data);
	}
	
	public function do_upload()
	{		
		$str_branch = $this->session->userdata("branch_short");							
		$str_nofototrans = $this->session->userdata('ses_name_apload'); 		
		
		$config['upload_path'] = './asset/upload_foto/'.$str_branch;		
		$config['allowed_types'] = 'pdf';	
		$config['max_size']	= '40000kb';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$config['overwrite'] = TRUE;
		$config['file_name']= $str_nofototrans; 

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
		if ( !$this->upload->do_upload()) :			   
			$error = array('error' => $this->upload->display_errors());
			//$this->session->set_flashdata('pesan_fail',$str_nofototrans.implode(" ",$error));				
			$this->session->set_flashdata('pesan_fail',implode(" ",$error));				
		else :			 
			$data = array('upload_data' => $this->upload->data());		
			$this->session->set_flashdata('pesan_upload_succes',"1");		
		endif;		
		$this->session->unset_userdata('ses_name_apload'); 											
	}	
	
	
	
	public function do_insert_sell()		
	{		
	    
	    $this->form_validation->set_rules('txtspk','txtspk','required');	
		$this->form_validation->set_rules('txtvinno','txtvinno','required');
		$this->form_validation->set_rules('txtdatealoc','txtdatealoc','required');	
		//$this->form_validation->set_rules('txtwrsdate','txtwrsdate','required');				
		$this->form_validation->set_rules('txtidcust','txtidcust','required');
		$this->form_validation->set_rules('cbosales','cbosales','required');
		$this->form_validation->set_rules('cbopaystatus','cbopaystatus','required');
		$this->form_validation->set_rules('txtdescpay','txtdescpay','required');
		$this->form_validation->set_rules('txtdatedet','txtdatedet','required');
		$this->form_validation->set_rules('txtdppay','txtdppay','required');	
		$this->form_validation->set_rules('txtnote','txtnote','required');	
		if (empty($_FILES['userfile']['name'])):		
			$this->form_validation->set_rules('userfile', 'Document', 'required');
		endif;					
		
		if($this->form_validation->run()==FALSE):			
		   echo $this->session->set_flashdata('pesan_fail','Some Mandatory Input Is Missing, Insert can not be prosses!!');
		   redirect('mazda_stock_sales/c_slssell_unit/c_crud_sell'); 			   
		else:
			if ($this->C_crud_sell->check_addnewduplicate_spk()):
				echo $this->session->set_flashdata('pesan_fail','SPK Duplicate, Insert can not be prosses!');
				 redirect('mazda_stock_sales/c_slssell_unit/c_crud_sell');
			else:	
				if ($this->C_crud_sell->check_ready_vin()):
				   echo $this->session->set_flashdata('pesan_fail','VIN Not Registered, Insert can not be prosses!');
				    redirect('mazda_stock_sales/c_slssell_unit/c_crud_sell');
			   else:	
			      if ($this->C_crud_sell->check_ready_customer()):
				      echo $this->session->set_flashdata('pesan_fail','Customer Not Registered, Insert can not be prosses!');
				      redirect('mazda_stock_sales/c_slssell_unit/c_crud_sell');
			      else:	
				       if ($this->C_crud_sell->check_ready_sales()):
				           echo $this->session->set_flashdata('pesan_fail','sales Not Registered, Insert can not be prosses!');
				           redirect('mazda_stock_sales/c_slssell_unit/c_crud_sell');
			           else:		  			  
							if ($this->C_crud_sell->insert_master_sell()) :					 
								  $this->do_upload(); //manggil fungsi Upload File	
								  			    
								  if ($this->session->flashdata('pesan_upload_succes') !=""):				
									  $str_upload_success = "& Upload Foto";
								  else:
									  $str_upload_success = "";
								  endif; 
								  
								 $this->C_crud_sell->counter_sell_number();  //add and manggil counter numberpp														
								  $this->session->set_flashdata('pesan_succces','Insert Selling Unit Successfull..!!');				
								  $url=  base_url('mazda_stock_sales/c_slssell_unit/c_crud_sell');		         
								  echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
							else:		
								  echo $this->session->set_flashdata('pesan_insert_fail','fail! ');
								  $url=  base_url('mazda_stock_sales/c_slssell_unit/c_crud_sell');		         
								  echo "<script type='text/javascript'> window.location='" . $url . "';</script>";  			
							endif;
						endif;		
				   endif;				
			   	endif;						
			endif;
		endif;	
	}			 

public function show_form_edit_sell()
 {
	$tampil_edit_sell = $this->C_crud_sell->getedit_sell();		
	$tampil_detail_sell = $this->C_crud_sell->getedit_detail_sell();						
				  	   		  	
	if ($tampil_edit_sell == null)
	{   	   	  
		echo $this->session->set_flashdata('pesan_fail','Data Edit Empty !');				
	    $url=  base_url('mazda_stock_sales/c_slssell_unit/c_slsssell');		         
	    echo "<script type='text/javascript'> window.location='" . $url . "'; </script>"; 
     }else{		 	 
	 	$data_edit['check_row'] = "1"; // jika  ketemu				  
	    $data_edit['intno'] = ""; // variable kosong buat nocounter perulangan detail						
		$data_edit['tampil_sales'] =  $this->C_crud_sell->get_sales();
		$data_edit['tampil_customer']=$this->C_crud_sell->getAllDataCustomer();
		$data_edit['tampil_payment_type']=$this->C_crud_sell->get_tampil_payment();						
		$data_edit['tampil_edit_sell'] = $tampil_edit_sell;									   
		$data_edit['tampil_detail_sell'] = $tampil_detail_sell;									   
		$data_edit['show_view'] ='v_slsstock_selling/V_form_edit_sell';
		$this->load->view('dashboard/Template',$data_edit);			   		
     }	  
	   
 }
	
public function do_edit_sell() //lakukan submit edit data	
{	
       // $this->form_validation->set_rules('txtspk','txtspk','required');	
		$this->form_validation->set_rules('txtvinno','txtvinno','required');
		$this->form_validation->set_rules('txtdatealoc','txtdatealoc','required');	
		//$this->form_validation->set_rules('txtwrsdate','txtwrsdate','required');				
		$this->form_validation->set_rules('txtidcust','txtidcust','required');
		$this->form_validation->set_rules('cbosales','cbosales','required');
		$this->form_validation->set_rules('cbopaystatus','cbopaystatus','required');
		$this->form_validation->set_rules('txtdescpay','txtdescpay','required');
		$this->form_validation->set_rules('txtdatedet','txtdatedet','required');
		$this->form_validation->set_rules('txtdppay','txtdppay','required');		
		$this->form_validation->set_rules('txtnote','txtnote','required');				
		//$this->form_validation->set_rules('txtspkedit','txtspkedit','required');				
		
		if($this->form_validation->run()==FALSE):			
		   echo $this->session->set_flashdata('pesan_fail','Some Mandatory Input Is Missing, Update can not be prosses!!');
		   redirect('mazda_stock_sales/c_slssell_unit/c_slsssell'); 			   
		else:
			if ($this->C_crud_sell->check_duplicate_spk()):
				echo $this->session->set_flashdata('pesan_fail','SPK Duplicate,Update can not be prosses!');
				 redirect('mazda_stock_sales/c_slssell_unit/c_slsssell');
				
			else:	
				if ($this->C_crud_sell->check_ready_vin()):
				   echo $this->session->set_flashdata('pesan_fail','VIN Not Registered,Update can not be prosses!');
				    redirect('mazda_stock_sales/c_slssell_unit/c_slsssell(');
							
			   else:	
			      if ($this->C_crud_sell->check_ready_customer()):
				      echo $this->session->set_flashdata('pesan_fail','Customer Not Registered,Update can not be prosses!');
				      redirect('mazda_stock_sales/c_slssell_unit/c_slsssell');
					 
			      else:	
				       if ($this->C_crud_sell->check_ready_sales()):
				          echo $this->session->set_flashdata('pesan_fail','sales Not Registered,Update can not be prosses!');							
						   redirect('mazda_stock_sales/c_slssell_unit/c_slsssell');
			           else:		  			  
							if ($this->C_crud_sell->edit_master_sell()) :					 
								  /*$this->do_upload(); //manggil fungsi Upload File	
								  			    
								  if ($this->session->flashdata('pesan_upload_succes') !=""):				
									  $str_upload_success = "& Upload Foto";
								  else:
									  $str_upload_success = "";
								  endif;*/ 
								  
								  $this->C_crud_sell->return_dp_jadi_ready(); //balikin stock klo ada yg dirubah vin nya								  								  														
								  redirect('mazda_stock_sales/c_slssell_unit/c_slsssell');  
							else:		
								  echo $this->session->set_flashdata('pesan_fail','fail! ');
								  redirect('mazda_stock_sales/c_slssell_unit/c_slsssell');			
							endif;
						endif;		
				   endif;				
			   	endif;						
			endif;
		endif;	
}

	 
public function do_delete_sell()	 //lakukan submit delete data
{		
	 if ($this->C_crud_sell->delete_with_edit_flag())
	 { 	 	
		 $this->session->set_flashdata('pesan','Delete Successfully....');		
		  redirect('mazda_stock_sales/c_slssell_unit/c_slsssell');  
	  }else{
		  $this->session->set_flashdata('pesan','No Data Selected To Remove!!');		  
		  redirect('mazda_stock_sales/c_slssell_unit/c_slsssell');  
	 }
}			

public function do_search_data()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->C_crud_sell->search_data(); // manggil hasil cari di model 										
		$branch  = $this->session->userdata('name_branch') ; 
	    $company = $this->session->userdata('company') ;		
		
		if($tampung_cari == null){		   		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = "0";	
			$data['sell_view']  = "";
			$data['show_view'] = 'v_slsstock_selling/V_table_sell';
			$this->load->view('dashboard/Template',$data);			
		}else{				   		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = "1";	
			$data['sell_view']  = $tampung_cari;				
			$data['show_view'] = 'v_slsstock_selling/V_table_sell';
			$this->load->view('dashboard/Template',$data);	 
		}	  
    }
	

	

public function do_insert_edit_detail_sell()		
	{	
	
			  
			if ($this->C_crud_sell->insert_edit_detail()) :			
								
				$this->do_upload(); //manggil fungsi Upload File							 			 						
				  if ($this->session->flashdata('pesan_upload_succes') !=""):				
					  $str_upload_detail_success = "& Upload Foto";
				  else:
					  $str_upload_detail_success = "";
				  endif;  
							
				  $this->session->set_flashdata('pesan_succes','Update Customer Payment Successfull..!!');				
				  redirect('mazda_stock_sales/c_slssell_unit/c_slsssell'); 	 
			else:		
				  echo $this->session->set_flashdata('pesan_fail','Update Customer Payment fail or Some Mandatory Input Is Empty! ');
				  redirect('mazda_stock_sales/c_slssell_unit/c_slsssell'); 	  			
			endif;								
	
	}		
	
public function send_flag_aproval()	
{
	if ($this->C_crud_sell->update_send_flag_aproval())
    {	 
	    $this->C_crud_sell->upddate_date_send();		    	
		$this->kirim_email();		
		redirect('mazda_stock_sales/c_slssell_unit/c_slsssell');   
	}else{
		$this->session->set_flashdata('pesan','Sending Approval Failed Or Payment CASH Not Balanced yet');		
		redirect('mazda_stock_sales/c_slssell_unit/c_slsssell');   
	}
}	

 public function kirim_email(){          
 		   
		   $this->load->library('email');				 								   		   		  	        
		  		
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;		
		  
		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   $config['smtp_port']  = '587';		  		  
		 
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   
		 
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5"; 
		   //--------------------------------		   		  
		    		   		 		          				   				     		   				  									
			$strid_trans_sales =$this->session->userdata("ses_notrans");	
			$status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete		
					
		   if ($strid_trans_sales != '') :			        	  	
				//Select untuk email master sales----------------------------
				   $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
				$query =$this->db->query("select * from qv_complite_master_sell where id_trans_sales ='".$strid_trans_sales."'and status ='".$status_aktif_record."'");				
			    if ($query->num_rows() > 0 ) :					   					   
					   $query_detail =$this->db->query("select * from tbl_sales_unit_detail where id_trans_sales ='".$strid_trans_sales."'and status ='".$status_aktif_record."'");
					   $data_email['str_selling_master'] = $query->result();
					   $data_email['str_selling_detail'] = $query_detail->result();						
					   $data_email['intno'] = ""; //untuk counter angka table	
					   			
					   //simpan di dalam variable message untuk di kirim sebagai Content Email			
					   $message = $this->load->view('v_slsstock_selling/V_content_email',$data_email,true);					 											 			   endif;
			endif;
					 
			$result = $this->email ;							   
			$this->email->initialize($config);  
			$this->email->set_newline("\r\n"); 
		   
		    //Exchange						
			
			$sender_email = "helpdesk@eurokars.co.id"; 					   
			$sender_name = "Epurchasing Notification";					   					    
			
			
		   //simpan session alamat email kedalam variable..
				$struseremail = $this->session->userdata('email');
				$strheademail = $this->session->userdata('email_head');
				$strfaemail = $this->session->userdata('email_fa');
			//end-----------------------------------------
		   
			//$to = 'brian.yunanda@eurokars.co.id';
			
			$to = $struseremail.",".$strheademail.",".$strfaemail;
			
			$subject = "Sending Aproval BSTK / D.O -- Eurokar Surya Utama";				   
			$this->email->from($sender_email, $sender_name);
			$this->email->to($to);
			$this->email->subject($subject);  
			
			//$this->email->message(print_r($message, true));			   
		  
		    $this->email->message($message);// tanpa array	
												  	 
		     if ($this->email->send()) :		  	
			    $this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
			    //redirect(current_url());
		     //} else {
			//    show_error($this->email->print_debugger());
		     endif; 
								
		
			 //Destroy session per variable			 $this->session->unset_userdata('ses_noppnew');			 			 
	      
 }	 	

public function multiple_submit() //fungsi submit dengan banyak button dalam satu form
{  	
	if ($this->input->post('btnadd')){	
		redirect('mazda_stock_sales/c_slssell_unit/c_crud_sell');
	}else{
		if ($this->input->post('btnsave')){	
			$this->do_insert_sell();
		}else{			
			 if ($this->input->post('btncaridate')){
				$this->do_search_date();
			 }else{
				if ($this->input->post('btncari')){	
					$this->do_search_data();
				}else{   		
					if ($this->input->post('btndel')){
						$this->do_delete_sell();
					}else{
						if ($this->input->post('btnedit')){
							$this->show_form_edit_sell();
						}else{
							if ($this->input->post('btnsavedetail')){
							   $this->do_insert_edit_detail_sell();
							}else{	
							   if ($this->input->post('btnaddeditdetail')){
							       $this->get_add_edit_idtrans_modal();
							   }else{	
							   	   if ($this->input->post('btnsendaproval')){
							       	   $this->send_flag_aproval();//kirim email
							   	   }else{																			
							           redirect('mazda_stock_sales/c_slssell_unit/c_slsssell');  
								   }
							   }
							}
						 }
					  }
				   }
			   }
		   }
	  }
   // redirect('mazda_stock_sales/c_slssell_unit/c_slsssell');
}

public function get_add_edit_idtrans_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	    // $idtrans= $this->input->post('id_trans_sales_edit',TRUE); //       
		 $idtrans= $this->input->post('msg'); //   
		 
		for ($int=0;$int < count($idtrans);$int++) :    
	     $query_master = $this->db->query("select * from qv_complite_master_sell where id_trans_sales ='".$idtrans[$int]."'");  
         $query_detail = $this->db->query("select * from tbl_sales_unit_detail where id_trans_sales ='".$idtrans[$int]."'and status ='".$status_aktif_record."'");  
		endfor;
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0) :	   		   
		      $data['str_trans_master']=$query_master->result();
			  $data['str_trans_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_idtrans'] = $idtrans ;				 
			  $data['show_view'] =  'v_slsstock_selling/V_conten_add_edit_detail';					
			  $this->load->view('dashboard/Template',$data);	
	 	   else:		   		
			   $data['str_trans_master']=$query_master->result();	
			   $data['str_trans_detail']="";		     
			   $data['intno']="";			  			  			  
			   $data['phasing_idtrans'] = $idtrans ;				 
			   $data['show_view'] =  'v_slsstock_selling/V_conten_add_edit_detail';					
			   $this->load->view('dashboard/Template',$data);   		   		  		 			  			 
		   endif;							   
    }      	

//auto complet vin engine-----------------------------
   public function suggest_vin_engine()//jquery
   {
						
		$hasil_model_rows = $this->C_crud_sell->get_data_novin();								
										
		$json_array = array();
		foreach ($hasil_model_rows as $row)	
				
										     					
		$json_array[]= array("value" => $row->vin, 
							  "label" => $row->vin." | ".$row->engine." | ".$row->desc_type,	
							  "txtnostock"=> $row->stock_no,				 					
							  "txtengine"=> $row->engine,
							  "txtmodel"=> $row->desc_model,
							  "txttype"=> $row->desc_type,
							  "txtwarna"=> $row->colour,
							  "txtlokasi"=> $row->location_stock,
							  "curr"=> $row->currency, 
							  "temp_date_wrs"=> date("d-m-Y",strtotime($row->temp_date_wrs)),
							  "price_unit"=> number_format($row->price_sell,2),	
							  "result"=> number_format($row->price_sell,2),					 					
					          );					   
		echo json_encode($json_array);
	}
	
	public function get_vin_engine_full()//jquery
    {
						
		$result_sellprice = $this->C_crud_sell->get_data_novin_full();//from model							  		     		 		 					
			foreach ($result_sellprice as $row_full):
			    $data['txtnostock'] = $row_full->stock_no; 									
				$data['txtvin'] = $row_full->vin; 	
				$data['txtengine']= $row_full->engine;
				$data['txtmodel']= $row_full->desc_model;
				$data['txttype']= $row_full->desc_type;
				$data['txtwarna']= $row_full->colour;
				$data['txtlokasi']= $row_full->location_stock;
				$data['curr']= $row_full->currency;
				$data['temp_date_wrs'] = date("d-m-Y",strtotime($row_full->temp_date_wrs));
				$data['price_unit']= number_format($row_full->price_sell,2);	
				$data['result']= number_format($row_full->price_sell,2);			
			endforeach;	
			
		echo json_encode($data);
	}
//end -----------------------------	

//auto complete customer-----------------------------
	
	public function suggest_customer()//jquery
	{							
		$hasil_model_rows = $this->C_crud_sell->get_data_customer();								
										
		$json_array = array();
		foreach ($hasil_model_rows as $row)				
		$json_array[]= array("value" => $row->cust_name, 
							 "label" => $row->cust_name,
							 "txtidcust"=> $row->id_cust,					 												
							 );					   
		echo json_encode($json_array);
     }
	 
	 
	public function get_customer_full()//jquery
    {
						
		$customer_full = $this->C_crud_sell->get_data_customer_full();//from model							  		     		 		 					
			foreach ($customer_full as $row_full):								
				$data['txtcust'] = $row_full->cust_name; 					
				$data['txtidcust'] = $row_full->id_cust; 											
			endforeach;	
			
		echo json_encode($data);
	}
//end -----------------------------		



}

