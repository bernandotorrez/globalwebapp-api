<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class m_approval_cross_sell extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_master_stockin';
	 
    public function tampil_add_sell(){ //tampil table untuk ngececk num_row						     	     		  	   
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_cross_sell = "1" ;				 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		  
		 $this->load->database();		
		 $this->db->select('stock_no,name_branch,branch_id,user_stock,dst_cross_sell,vin,engine,desc_type,colour,temp_date_wrs,remarks_cross_sell,status,status_flag_stock,attach_foto,user_send_cross_sell,from_cross_sell,date_send_approval');						
		 $this->db->where('flag_cross_sell',$flag_cross_sell);		 		 
		 $this->db->where('status',$status_delete);							
		 $this->db->order_by('date_send_approval','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_cross_sell = "1" ;				 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  
		 $this->load->database();		
		 $this->db->select('stock_no,name_branch,branch_id,user_stock,dst_cross_sell,vin,engine,desc_type,colour,temp_date_wrs,remarks_cross_sell,status,status_flag_stock,attach_foto,user_send_cross_sell,from_cross_sell,date_send_approval');										
		 $this->db->where('flag_cross_sell',$flag_cross_sell);		 
		 $this->db->where('status',$status_delete);		 				
		 $this->db->order_by('date_send_approval','desc');		
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_master_stockin')->result();		 		   
		 return $result ;
	}							  		
	
	public function give_flag_approval_cross()
	{
		$stridtrans	= $this->input->post('msg') ;
		
		for($intloop=0;$intloop < count($stridtrans);$intloop++) :		   
			 //Select ceck aproval master sales---------------------------- 
 	        $query_check =$this->db->query("select * from qv_complite_master_sell where id_trans_sales='".$stridtrans[$intloop]."'and aprove_bm ='"."1"."'");		   			
			  if ($query_check->num_rows() == 0) :	
				  $data_flag_bm = array('aprove_bm' =>"1");
				  $this->db->where('id_trans_sales',$stridtrans[$intloop]);
				  $this->db->update('tbl_sales_unit_transaksi',$data_flag_bm);			
				  
				   //-----bikin flag terjual pada table sales
				  $strbuf_Sell = array('status_flag_sell'=> '0');				
				  $this->db->where('id_trans_sales',$stridtrans[$intloop]);
				  $this->db->update('tbl_sales_unit_transaksi',$strbuf_Sell);
				  
				  $strbuff_idtrans = $stridtrans[$intloop] ;
				  
				  $data_approve = array("ses_approval_idtrans_bm"=>$strbuff_idtrans);
				  $this->session->set_userdata($data_approve);		
				  //end
				  
				
				  //--bikin flag terjual pada table table stock.
				  $query_stock =$this->db->query("select * from qv_complite_master_sell where id_trans_sales='".$stridtrans[$intloop]."'and status ='"."1"."'");	
				   if ($query_stock->num_rows() > 0) :									   	 
					  foreach ($query_stock ->result() as $row_update) :									
							   $strbuf_stock = array('status_flag_stock'=> '0');
							   $this->db->where('stock_no',$row_update->stock_no);					  
							   $this->db->update('tbl_stock_master',$strbuf_stock);
					  endforeach ; 	   			
				    endif;	  
				  //end	---------------------------------------			 
				  
				  
				  return true ;
			  else: 	  
			  	  return false ;	
			  endif;	
			  
		endfor;						
	}
	
	public function tampil_cabang_mazda()
	{
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $id_company = "3"; //idcompany 3 MAZDA
		 
		 $this->load->database();		
		 $this->db->select('branch_id,name_branch,place,');									 		 		
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('id_company',$id_company); 
		 $this->db->order_by('branch_id','desc');
		 $result = $this->db->get('tbl_branches');			     		 
		 return $result;
	}						  	
	
	function give_rejected()						  		
	{	    			  		 	
		 $txtidstock = $this->input->post('txtidstock');
		 $str_name_reject = "By"." "."Finance"." : ".$this->session->userdata('name')." " . ":";
		 $strid_reason_reject =  $str_name_reject." ".$this->input->post('txtreject');		 		 		 
		 $flag_reject = "1" ; // flag 1 flag_reject	back to stock			 			
		
				 					 		 			
		  if (isset($txtidstock) && trim($txtidstock!='')) :
		      $query_check =$this->db->query("select * from qv_complite_master_stockin where stock_no='".$txtidstock."'");		   			
			  if ($query_check->num_rows() != 0) :	
				  $data_reject= array( "status_flag_stock"=>$flag_reject,
										"remarks_reject_cross"=> $strid_reason_reject 
										);																																							
				   $this->db->where('stock_no',$txtidstock);
				   $this->db->update('tbl_stock_master',$data_reject); 
					
				   $buff_sesidreject= array("sess_stockno_reject" => $txtidstock );
				   $this->session->set_userdata($buff_sesidreject);		
					
				   return true;			 				  
			    else:
					return false;			 				  
				endif;	   				
		  else :
					
			  return false;
		  endif;
	  		  
	}	    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
	function get_detail_modal_reject()//model auto complete	wrs bodong
	{		       
	  $txtidtrans = $this->input->post('txtidtrans',TRUE); 		
	  $status_del = "1";		 
					 
	  $this->load->database();
	  $this->db->select('vin,engine,desc_type,colour');		 
	  $this->db->where("id_trans_sales",$txtidtrans);	  	 
	  $query = $this->db->get('qv_complite_master_sell');					
	
	  return $query->result();		  
	}							 
	
	function get_detail_modal_cross_selling()//model auto complete	Cross Selling
	{		       
	  $txtidstock = $this->input->post('txtidstock',TRUE); 			  
	  $status_del = "1";		 
					 
	  $this->load->database();
	  $this->db->select('stock_no,vin,engine,desc_type,colour,name_branch,dst_id_branch_cs,dst_cross_sell,remarks_cross_sell');		 
	  $this->db->where("stock_no",$txtidstock);	  	 	 	 
	  $query = $this->db->get('qv_complite_master_stockin');					
	
	  return $query->result();		  	  
	}			
	
	public function update_approval_cross_selling()
	{
		$txnostock = $this->input->post('txtnostock');
		$txtid_b = $this->input->post('txtid_b');
				
		$flag_status_stock = '1' ;
		
		if ($txnostock !="") :
			$data_send_cross=array('status_flag_stock'=>$flag_status_stock,	
								   'branch_id'=>$txtid_b							
									);
			$this->db->where('stock_no',$txnostock);
			$this->db->update('tbl_stock_master',$data_send_cross);
			
			$buff_ses = array('ses_srtock_no'=>$txnostock);
			$this->session->set_userdata($buff_ses );
			
			return true;
		else:
			return false;
		endif;	
		
	}
	
	function search_data() 	// fungsi cari data pp model
	{
	   $cari          =  $this->input->post('txtcari');
	   $kategory      =  $this->input->post('cbostatus');	  	   	 	  
	   $flag_status   = "1" ;  //jika 0 terdelete
	  
	   $flag_cross = "1" ; //jika 1 cross selling
	   
	   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
	   {
		   redirect('mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell');  //direct ke url add_menu_booking/add_menu_booking
	   }else{	
	     $this->load->database();			  	   		  		 	   
	     $this->db->where('status',$flag_status);
	     $this->db->where('flag_cross_sell', $flag_cross ); // penggunaan and dan or
	     $this->db->like($kategory,$cari);		    		   
	     $result = $this->db->get('qv_complite_master_stockin');		 
	   
	   $this->session->set_flashdata('cari',$cari);	
	   return $result->result(); 
	   }
	} 
}