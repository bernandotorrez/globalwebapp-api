<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_slsstock extends CI_Model {	
				    
	public $db_tabel = 'qv_complite_master_stockin';
	var $table = 'tbl_model_group';
    var $column_order = array(null, null,'stock_no', 'name_branch','desc_type','vin', 'engine', 'colour','csi', 'location_stock', 'intake', 'year', 'status_flag_stock', 'rec_date','pay_date_mmi', 'name_bank', 'status', 'attach_foto'); //set column field database for datatable orderable
	var $column_search = array('stock_no', 'name_branch','desc_type','vin', 'engine', 'colour','csi', 'location_stock', 'intake', 'year', 'status_flag_stock', 'rec_date','pay_date_mmi', 'name_bank', 'status', 'attach_foto'); //set column field database for datatable searchable 
    var $order = array('stock_no' => 'desc'); // default order 
    var $view_table = 'qv_complite_master_stockin';
	 
    public function tampil_add_stock(){ //tampil table untuk ngececk num_row						     	     		  	   
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  
		 $flag_cross = "3" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  2 cross selling		 		 		
		 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('branch_id',$ses_id_branch );				 		 				
		 $this->db->where('status',$status_delete);		 
		 $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' OR status_flag_stock ='".$flag_cross."' )", NULL, FALSE); 
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}	    

	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS		  		 		 		
		 $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS		  		 		 	     
		 $flag_cross = "3" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  2 cross selling		  		 		 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('branch_id',$ses_id_branch );				 
		 $this->db->where('status',$status_delete);		 
		 $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' OR status_flag_stock ='".$flag_cross."' )", NULL, FALSE);  				
		 $this->db->order_by('rec_date','desc');		
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_master_stockin')->result();		 		   
		 return $result ;
	}							  		
	
	public function tampil_cabang_mazda(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 		
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $id_company = "3"; //idcompany 3 MAZDA
		 
		 $this->load->database();		
		 $this->db->select('branch_id,name_branch,place,');									 		 		
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('id_company',$id_company); 
		 $this->db->order_by('branch_id','desc');
		 $result = $this->db->get('tbl_branches');			     		 
		 return $result;
	}			
	
	 public function hitung_jumlah_stock(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 
		 $ses_id_branch = $this->session->userdata('branch_id');	
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');	
		 $this->db->where('branch_id',$ses_id_branch );							 		 		 				
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('status_flag_stock',$flag_stock); 
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}
	
	 public function hitung_jumlah_dp(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 
		 $ses_id_branch = $this->session->userdata('branch_id');	
		 $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');
		 $this->db->where('branch_id',$ses_id_branch );								 		 		 				
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('status_flag_stock',$flag_dp); 
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}	
	
	 public function hitung_jumlah_wrs(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 
		 $ses_id_branch = $this->session->userdata('branch_id');	
		  	
		 $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('branch_id',$ses_id_branch );						 		 		 				
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('status_flag_stock',$flag_wrs); 
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}	    	    		    	
	
	function get_detail_modal_cross_selling()//model auto complete	Cross Selling		
	{		       
	  $txtidstock = $this->input->post('txtidstock',TRUE); 		
	  $str_branch = $this->session->userdata('branch_id');	  
	  $status_del = "1";		 
					 
	  $this->load->database();
	  $this->db->select('vin,engine,desc_type,colour');		 
	  $this->db->where("stock_no",$txtidstock);	  	 
	   $this->db->where("branch_id",$str_branch);	  	 
	  $query = $this->db->get('qv_complite_master_stockin');					
	
	  return $query->result();	
	}		
	
	function get_datatables()
	{
		$ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		$flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		$flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		$flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  
		$flag_cross = "3" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  2 cross selling		 		 		
		
		$status_delete = "1"; //jika 0 terdelete jika 1 aktif
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->where('branch_id',$ses_id_branch );				 
		$this->db->where('status',$status_delete);		 
		$this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' OR status_flag_stock ='".$flag_cross."' )", NULL, FALSE);
		$query = $this->db->get();
		return $query->result();
	}
	
	private function _get_datatables_query()
	{
		
		$this->db->from($this->view_table);
		

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	
	function count_filtered()
	{
		$ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		$flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		$flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		$flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  
		$flag_cross = "3" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  2 cross selling		 		 		
		
		$status_delete = "1"; //jika 0 terdelete jika 1 aktif
		$this->_get_datatables_query();
		$this->db->where('branch_id',$ses_id_branch );				 
		 $this->db->where('status',$status_delete);		 
		 $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' OR status_flag_stock ='".$flag_cross."' )", NULL, FALSE);
		$query = $this->db->get();
		return $query->num_rows();
    }

    public function count_all()
	{
		$ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		$flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		$flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		$flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  
		$flag_cross = "3" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  2 cross selling		 		 		
		
		$status_delete = "1"; //jika 0 terdelete jika 1 aktif
		$this->db->from($this->view_table);
		$this->db->where('branch_id',$ses_id_branch );				 
		 $this->db->where('status',$status_delete);		 
		 $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' OR status_flag_stock ='".$flag_cross."' )", NULL, FALSE);
		return $this->db->count_all_results();
	}
				    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}