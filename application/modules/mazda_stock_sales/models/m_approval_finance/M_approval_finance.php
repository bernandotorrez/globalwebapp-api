<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_approval_finance extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_master_stockin';
	 
    public function tampil_add_sell(){ //tampil table untuk ngececk num_row						     	     		  	   
		// $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_sell = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		 		 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $status_flagaproval = "1"; //jika 0 terdelete jika 1 aktif
		 $status_flagbm = "1";
		 
		 $this->load->database();		
		 $this->db->select('id_trans_sales,name_branch,branch_id,nospk,stock_no,vin,engine,desc_type,colour,curr_sell,price_sell,remaining_amount_customer,discount,total_paymet_customer,date_aloc,lama_spk,cust_name,sal_name,wrs_date,remarks,date_transaksi,date_reminder_spk,status_flag_sell,flag_send_approval,status,approve_fin,aprove_bm,user_sell,date_send_aproval,date_dudate_spk');				
		// $this->db->where('branch_id',$ses_id_branch );				 		 				
		// $this->db->where('flag_send_approval',$status_flagaproval);		 
		 $this->db->where('status',$status_delete);	
		// $this->db->where("aprove_bm",$status_flagbm); 					
		 $this->db->where("(status_flag_sell='".$flag_sell."' OR status_flag_sell='".$flag_dp."' )", NULL, FALSE);   	
		 //$this->db->where("status_flag_sell",$flag_dp); 
		 $this->db->order_by('date_send_aproval','desc');
		 $result = $this->db->get('qv_complite_master_sell');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    
		 //$ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_sell = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		  		 		 			  		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $status_flagaproval = "1"; //jika 0 terdelete jika 1 aktif
		 $status_flagbm = "1";
		  
		 $this->load->database();		
		 $this->db->select('id_trans_sales,name_branch,branch_id,nospk,stock_no,vin,engine,desc_type,colour,curr_sell,price_sell,remaining_amount_customer,discount,total_paymet_customer,date_aloc,lama_spk,cust_name,sal_name,wrs_date,date_transaksi,date_reminder_spk,remarks,status_flag_sell,flag_send_approval,status,status,approve_fin,aprove_bm,user_sell,date_send_aproval,date_dudate_spk');						
		// $this->db->where('branch_id',$ses_id_branch );				 
		// $this->db->where('flag_send_approval',$status_flagaproval);		 
		 $this->db->where('status',$status_delete);	
		// $this->db->where('aprove_bm',$status_flagbm); 
		 $this->db->where("(status_flag_sell='".$flag_sell."' OR status_flag_sell='".$flag_dp."')", NULL, FALSE);   		 				
		 //$this->db->where('status_flag_sell',$flag_dp); 
		 $this->db->order_by('date_send_aproval','desc');		
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_master_sell')->result();		 		   
		 return $result ;
	}							  		
	
	public function check_send_approval()
	{
		
		$idtrans = $this->input->post('msg');
		$empty_send_apporval = "0"; 
		
		for ($intloop=0; $intloop < count($idtrans) ;$intloop++ ) :		
			$this->load->database();
			$this->db->select('*');
			$this->db->where('id_trans_sales',$idtrans[$intloop]);
			$this->db->where('flag_send_approval',$empty_send_apporval);
			$query_hasil = $this->db->get('tbl_sales_unit_transaksi');
			
			if ($query_hasil->num_rows() == 1) :					  
			   return true ;
			endif ;
			
		endfor;	
	}
	
	public function check_reject()
	{
		
		$idtrans = $this->input->post('msg');
		$reject = "-1"; 
		
		for ($intloop=0; $intloop < count($idtrans) ;$intloop++ ) :		
			$this->load->database();
			$this->db->select('*');
			$this->db->where('id_trans_sales',$idtrans[$intloop]);
			$this->db->where('flag_send_approval',$reject);
			$query_hasil = $this->db->get('tbl_sales_unit_transaksi');
			
			if ($query_hasil->num_rows() == 1) :					  
			   return true ;
			endif ;
			
		endfor;	
	}
	
	public function give_flag_approval_fin()
	{
		$stridtrans	= $this->input->post('msg') ;
		
		for($intloop=0;$intloop < count($stridtrans);$intloop++) :		   
		
		 //Select ceck aproval master sales---------------------------- 
 	        $query_check =$this->db->query("select * from qv_complite_master_sell where id_trans_sales='".$stridtrans[$intloop]."'and approve_fin ='"."1"."'");		   
			
			  if ($query_check->num_rows() == 0) :	
			     
				  $data_flag_fin = array('approve_fin' =>"1");
				  $this->db->where('id_trans_sales',$stridtrans[$intloop]);
				  $this->db->update('tbl_sales_unit_transaksi',$data_flag_fin);							
			 	  $strbuff_idtrans = $stridtrans[$intloop] ;
				  
				  $data_approve = array("ses_approval_idtrans_finance"=>$strbuff_idtrans);
				  $this->session->set_userdata($data_approve);														
				  
				/*  //-----bikin flag terjual pada table sales
				  $strbuf_Sell = array('status_flag_sell'=> '0');				
				  $this->db->where('id_trans_sales',$stridtrans[$intloop]);
				  $this->db->update('tbl_sales_unit_transaksi',$strbuf_Sell);
				  //end
				  
				
				  //--bikin flag terjual pada table table stock.
				  $query_stock =$this->db->query("select * from qv_complite_master_sell where id_trans_sales='".$stridtrans[$intloop]."'and status ='"."1"."'");	
				   if ($query_stock->num_rows() > 0) :									   	 
					  foreach ($query_stock ->result() as $row_update) :									
							   $strbuf_stock = array('status_flag_stock'=> '0');
							   $this->db->where('stock_no',$row_update->stock_no);					  
							   $this->db->update('tbl_stock_master',$strbuf_stock);
					  endforeach ; 	   			
				    endif;	  
				  //end	---------------------------------------	
				  */		  
				  return true ;
				  
			  else :
			      return false ;
			  endif;	  			  	
		endfor;
		
		
		
	}
				  
	/*public function give_flag_reject_fin()
	{
		$stridtrans	= $this->input->post('msg') ;
		
		for($intloop=0;$intloop < count($stridtrans);$intloop++) :
		    $data_flag_bm = array('flag_send_approval' =>"-1");
			$this->db->where('id_trans_sales',$stridtrans[$intloop]);
			$this->db->update('tbl_sales_unit_transaksi',$data_flag_bm);	
			
			$buff_sesidtrans = array("sess_idtrans_reject" => $stridtrans[$intloop]);
			$this->session->set_userdata($buff_sesidtrans);		
		endfor;
		
	    return true;
	} */			    
	
	public function give_flag_cancel_fin()
	{
		$stridtrans	= $this->input->post('msg') ;
		
		//table sale ----------------------------------------
		for($intloop=0;$intloop < count($stridtrans);$intloop++) :
		    $data_flag_tblsal = array('status' =>"0");
			$this->db->where('id_trans_sales',$stridtrans[$intloop]);
			$this->db->update('tbl_sales_unit_transaksi',$data_flag_tblsal);	
			
			$data_flag_tbldetailsal = array('status' =>"0");
			$this->db->where('id_trans_sales',$stridtrans[$intloop]);
			$this->db->update('tbl_sales_unit_detail',$data_flag_tbldetailsal);	
									
			$this->load->database();
			$this->db->select('stock_no');
			$this->db->where('id_trans_sales',$stridtrans[$intloop]);
			$querymaster = $this->db->get('tbl_sales_unit_transaksi');
			
			//table stock ----------------------------------------
			if ($querymaster->num_rows() >0) :
			    foreach($querymaster->result() as $rowstock) :
					 $data_flag_tblstock = array('status_flag_stock' =>"1");
					 $this->db->where('stock_no',$rowstock->stock_no);
					 $this->db->update('tbl_stock_master',$data_flag_tblstock);	
				endforeach;
			endif;						
						
			$buff_sesidtrans = array("sess_idtrans_cancel" => $stridtrans[$intloop]);
			$this->session->set_userdata($buff_sesidtrans);								
			
			return true;
		endfor;
			    
	}	
	
  function give_rejected()						  		
	{	    			  		 	
		 $strid_trans = $this->input->post('txtidtrans');
		 $str_name_reject = "By"." "."FINANCE"." : ".$this->session->userdata('name')." " . ":";
		 $strid_reason_reject =  $str_name_reject." ".$this->input->post('txtreject');		 		 		 
		 $flag_reject = "-1" ; // flag 1 flag_reject				 			
		 $flag_approve_fin = "1" ; // 1 jika sudah pernah di approve oleh finance		 		 			
		  if (isset($strid_trans) && trim($strid_trans!='')) :
		     //Select ceck aproval master sales---------------------------- 
 	         $query_check =$this->db->query("select * from qv_complite_master_sell where id_trans_sales='".$strid_trans."'and approve_fin ='".$flag_approve_fin."'");		   
			
			  if ($query_check->num_rows() == 0) :	
		          $data_reject= array("flag_send_approval"=>$flag_reject,
									 "reason_reject" => $strid_reason_reject);																																							
				  $this->db->where('id_trans_sales',$strid_trans);
			 	  $this->db->update('tbl_sales_unit_transaksi',$data_reject); 
				
				  $buff_sesidtrans = array("sess_idtrans_reject_finance" => $strid_trans );
				  $this->session->set_userdata($buff_sesidtrans);		
				
				  return true;			 				  
		  	  else :					
			      return false;
			  endif;
		  else :	  	  
		  	  return false;
		  endif;
	  		  
	}	 
	
	
	function search_data() 	// fungsi cari data pp model
	{
	   $cari          =  $this->input->post('txtcari');
	   $kategory      =  $this->input->post('cbostatus');	  	   	 
	  // $ses_branch_id =  $this->session->userdata('branch_id');
	   $flag_status   = "1" ;  //jika 0 terdelete
	  
	   $flag_sell = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	   $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP
	   	   	   
	   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
	   {
		   redirect('mazda_stock_sales/c_approval_finance/c_approval_finance');	  
	   }else{	
	     $this->load->database();			  	   		  		 
	    // $this->db->where('branch_id',$ses_branch_id);
	     $this->db->where('status',$flag_status);
	     $this->db->where("(status_flag_sell='".$flag_sell."' OR status_flag_sell='".$flag_dp."' )", NULL, FALSE);
	     $this->db->like($kategory,$cari);		    		   
	     $result = $this->db->get('qv_complite_master_sell');		 
	   
	     $this->session->set_flashdata('cari',$cari);	
	     return $result->result(); 
	   }
	}    	   					    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
	function get_detail_modal_cancel_bstk()//model auto complete	wrs bodong
	{		       
	  $txtidtrans = $this->input->post('txtidtrans',TRUE); 		
	  $status_del = "1";		 
					 
	  $this->load->database();
	  $this->db->select('vin,engine,desc_type,colour');		 
	  $this->db->where("id_trans_sales",$txtidtrans);	  	 
	  $query = $this->db->get('qv_complite_master_sell');					
	
	  return $query->result();	
	}				 
	
	function update_reason_cancel()
	{
		 $txtidtrans = $this->input->post('txtidtrans_cancel'); 	
		 $txtreason_cancel = $this->input->post('txtcancel'); 	
		 
		 if ($txtidtrans != "") :			 
			 $data_edit =array('reason_cancel_bstk'=>$txtreason_cancel);		 
			 $this->db->where('id_trans_sales',$txtidtrans);
			 $this->db->update('tbl_sales_unit_transaksi',$data_edit);		 
		 	 return true;
		else :
			return false;
		endif;	 
		 
		 	
	}
}