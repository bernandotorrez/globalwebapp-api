<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_crud_stock extends CI_Model {	
				
    public $db_tabel_qv = 'qv_complite_master_stockin';	    
		
	
	function getstocknumber(){	// view counter number when add new
	    $strbranchshort = $this->session->userdata('branch_short');				
	 	$stridbranch = $this->session->userdata('branch_id');				
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");			    
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	    $qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_stock_unit_no where branch_id='".$stridbranch."'");
									  	   
	    $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); 	//check tanggal
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
				if($qcounter->num_rows()== 1):
					foreach($qcounter->result() as $cd):
						$tmp = ((int)$cd->code_max);
						$code = sprintf("%04s", $tmp);;
					endforeach;
					
					 if ($code== '0000') :						
						 $code = "0001";					 
					 endif;
					 
				 else:					  
					$code = "0001";								
				 endif;		
		 else:		
		       $code = "0001";					 
		 endif;
		 
	 //update tanggal ketable date--------------
	 $data = array('date_add'=>$strdates );																			
	 $this->db->update('tbl_date_compare',$data); 		 
	 //end--------------------
		 
	 //update tanggal ketable counter--------------
	  $data = array('counter'=> $code );	
	  $this->db->where('branch_id',$stridbranch);															  									
	  $this->db->update('tbl_counter_stock_unit_no',$data); 		 
	 //end--------------------
		 
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	  return "M"."/".$strbranchshort."/".$stryear."/".$strmonth."/".$strdates."/".$code ;			        //end------------------------------------------			 
    }	
	
	function counterstocknumber() //counter stock no whesn save
	{
		 $strbranchshort = $this->session->userdata('branch_short');	 
		$stridbranch = $this->session->userdata('branch_id');				
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");			    
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	    $qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_stock_unit_no where branch_id='".$stridbranch."'");
									  	   
	    $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); 	//check tanggal
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
				if($qcounter->num_rows()== 1):
					foreach($qcounter->result() as $cd):
						$tmp = ((int)$cd->code_max)+1;
						$code = sprintf("%04s", $tmp);
					endforeach;					
					 if ($code== '0000') :						
						 $code = "0001";					 
					 endif;
					 
				 else:					  
					$code = "0001";								
				 endif;		
		 else:		
		       $code = "0001";					 
		 endif;
		 
	 //update tanggal ketable date--------------
	 $data = array('date_add'=>$strdates );																			
	 $this->db->update('tbl_date_compare',$data); 		 
	 //end--------------------
		 
	 //update tanggal ketable counter--------------
	  $data = array('counter'=> $code );	
	  $this->db->where('branch_id',$stridbranch);															  									
	  $this->db->update('tbl_counter_stock_unit_no',$data); 		 
	 //end--------------------
		 
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	  return true ;
	  //end------------------------------------------		
	}
	
	public function tampil_cabang_mazda(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 		
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $id_company = "3"; //idcompany 3 MAZDA
		 
		 $this->load->database();		
		 $this->db->select('branch_id,name_branch,place,');									 		 		
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('id_company',$id_company); 
		 $this->db->order_by('branch_id','desc');
		 $result = $this->db->get('tbl_branches');			     		 
		 return $result;
	}				
	
	function get_lokasi()
	{		
	     $this->load->database();		
		 $this->db->select('name_loc');	
		 $this->db->where('status','1');			
		 $result = $this->db->get('tbl_location_stock');			     		 
		 return $result->result();	 	
	}	
	
	function get_type_vehicle()
	{			      
	     $this->load->database();		
		 $this->db->select('id_type,desc_type');			 
		 $this->db->where('status','1');			
		 $result = $this->db->get('tbl_brand_type');			     		 
		 return $result->result();	 	
	}	
	
	function get_color()
	{			       	
	     $strid_type = $this->input->post('id_type_post'); //post from jquery
	     
		 $this->load->database();		
		 $this->db->select('*');			 
		 $this->db->where('id_type',$strid_type);	
		 $this->db->where('status','1');	
		 $this->db->order_by('colour','ASC');		
		 $result = $this->db->get('tbl_colour'); 		     		 		 						 		     		 		 					   
		 return $result; 		 

	}
	
	function get_price_unit()
	{			       	
	     $strid_type = $this->input->post('id_type_post'); //post from jquery	     
		 $this->load->database();		
		 $this->db->select('id_type,price_sell,currency');			 
		 $this->db->where('id_type',$strid_type);	
		 $this->db->where('status','1');			
		 $result = $this->db->get('qv_curr_brand_type'); 		     		 		 						 		     	 		 					   
		 return $result; 		 

	}
	
	function get_bank()
	{		
	     $this->load->database();		
		 $this->db->select('id_bank,name_bank');				
		 $result = $this->db->get('tbl_bank');	
		 		     		 
		 return $result->result();	 	
	}	
	
	function get_currency()
	{		
	     $this->load->database();		
		 $this->db->select('id_curr,currency');					
		 $result = $this->db->get('tbl_curr');			     		 
		 return $result->result();	 	
	}	
	 
	function insert_address_foto()
	{   
	    $str_nostock =$this->session->userdata("ses_nostock") ;	 		
		$str_branch= $this->session->userdata("branch_short");
		//-----------------------------address attach quo
		$buff_dept=  str_replace(".","",$this->session->userdata("dept"));
		$str_dept=  str_replace(" ","_",$buff_dept);
		//-----------------------------------------------
		$stryear =  substr(date('Y'),-3);
		$str_date = $stryear."-".date("m")."-".date("d");
	    $str_count= substr($str_nostock,-3);
		
		$str_path='asset/upload_foto/'.$str_branch; // alamat upload quatation nya. 	
		
		//alamat upload quatation beserta filenya.		    		
		$str_address=$str_path."/"."M"."-".$str_branch."-".$str_date."-".$str_count.".pdf";			  				   			  			   			
		$data = array('attach_foto'=>$str_address);																						
		$this->db->where('stock_no',$str_nostock);
		$this->db->update('tbl_stock_master',$data); 
		return true;					  	
	}
		
	function check_duplicate_vin()
	{
	   $txtvin	= $this->input->post('txtvin');	
	   
	   $this->load->database() ;
	   $this->db->select('vin');
	   $this->db->where('vin',$txtvin);
	   $result = $this->db->get('tbl_stock_master');
	   
	   if ($result->num_rows() > 0 ) :
	   	   return true;
	   endif ;  	   	   
			  
	}	
	
	function check_duplicate_engine()
	{
	   $txtengine	= $this->input->post('txtengine');		
	  
	   $this->load->database() ;
	   $this->db->select('engine');
	   $this->db->where('engine',$txtengine);
	   $result = $this->db->get('tbl_stock_master');
	   
	   if ($result->num_rows() > 0 ) :
	   	   return true;
	   endif ;  	
	}	
	
	function check_edit_duplicate_vin()
	{
	   $txtvin			= $this->input->post('txtvin');	
	   $txtvinupdate	= $this->input->post('txtvinupdate');	
	   
	     if ($txtvin != $txtvinupdate) :
			   $this->load->database() ;
			   $this->db->select('vin');
			   $this->db->where('vin',$txtvinupdate);
			   $result = $this->db->get('tbl_stock_master');
			   
			   if ($result->num_rows() > 0 ) :
				   return true;
			   endif ;		   
		 endif;    	   	   
			  
	}	
	
	function check_edit_duplicate_engine()
	{
	   $txtengine		= $this->input->post('txtengine');	
	   $txtengineupdate	= $this->input->post('txtengineupdate');		
	  
	   if ( $txtengine != $txtengineupdate) :
		   $this->load->database() ;
		   $this->db->select('engine');
		   $this->db->where('engine',$txtengineupdate);
		   $result = $this->db->get('tbl_stock_master');
		   
		   if ($result->num_rows() > 0 ) :
			   return true;
		   endif ;  	
		   
		endif;   
	}	
	
	function insert_master_stock()	
	{ 
	     //-------------------Checck Duplicate NOPP / Idmaster		 
		 $cektxtnostock= $this->input->post('txtnostock'); 		 		 
		 $query2=   $this->db->select('*')
						 ->where('stock_no',$cektxtnostock)	 //Checck Duplicate NOpo / Idmaster					
						 ->get('tbl_stock_master');		
		 if ($query2->num_rows() == 1) // Jika ketemu buat NOPP baru.
		 {			
				 	
   		        $stridbranch = $this->session->userdata('branch_id');			 			  	
				$strbranchshort = $this->session->userdata('branch_short');	
				$stryear = substr(date("Y"),-3,3);
				$strmonth = date("m");			    
				$strdates = date("d");	//date (tanggal buat comparasi pada table)
							
				 
				$qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_stock_unit_no where branch_id='".$stridbranch."'");
												   
				$qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); 	//check tanggal
			   
				$code = ""; //empty code
				 if ($qdate->num_rows()== 1) :			
						if($qcounter->num_rows()== 1):
							foreach($qcounter->result() as $cd):
								$tmp = ((int)$cd->code_max)+1;
								$code = sprintf("%04s", $tmp);;
							endforeach;							
							 if ($code== '0000') :						
								 $code = "0001";					 
							 endif;
							 
						 else:					  
							$code = "0001";								
						 endif;		
				 else:		
					   $code = "0001";					 
				 endif;
				 
			 //update tanggal ketable date--------------
			 $data = array('date_add'=>$strdates );																			
			 $this->db->update('tbl_date_compare',$data); 		 
			 //end--------------------
				 
			 //update tanggal ketable counter--------------
			  $data = array('counter'=> $code );	
			  $this->db->where('branch_id',$stridbranch);															  									
			  $this->db->update('tbl_counter_stock_unit_no',$data); 		 
			 //end--------------------
				 
			 //buat counter			
			   $txtnostock = "M"."/".$strbranchshort."/".$stryear."/".$strmonth."/".$strdates."/".$code ;			        			
			  //end------------------------------------------			 			
		 }else{			
		      //Ambil NOPP yang ada di inputbox posting	 jika tidak duplicate			 
			  $txtnostock =$this->input->post('txtnostock');  	   	  
	     }
		      //----mengambil data dari inputbox/textbox lalu di simpan didalam variable
			  
			  $status_aktif= "1" ; // jik 0 sudah terhapus			  
			  $flag_status_stock= "1" ;
			  
			  $txtnostock =$this->input->post('txtnostock');			 
			  $ses_branch = $this->session->userdata('branch_id');	
			  $cboloc = $this->input->post('cboloc');  			 
			  $txtuser	= $this->input->post('txtuser');	
			  
			  $cbotype	= $this->input->post('cbotype');	
			  $cbowarna	= $this->input->post('cbowarna');	
			  
			 // $curr	= $this->input->post('curr');
			  //---------------------unformat price---------
			 //  $price_unit	= $this->input->post('price_unit');	 
			 // if ( strstr( $price_unit, ',' ) ) $price_unit = str_replace( ',','', $price_unit );  
			  //-------------------------------------
			  
			  $txtvin	= $this->input->post('txtvin');				
			  $txtengine	= $this->input->post('txtengine');	
			  
			  
			  $txtcsi	= $this->input->post('txtcsi');	
			  $cbointake	= $this->input->post('cbointake');	
			  
			  if ($this->input->post('txtdatericived') !="") :
			      $txtdate_reciep = date('Y-m-d',strtotime($this->input->post('txtdatericived')));		
			 else:
			 	  //$txtdate_reciep = null	;
				  $txtdate_reciep = date('Y-m-d');
			 endif;
			
			if ($this->input->post('txtdatetommi') != ""): 	  	 			  
			    $txtdatetommi = date('Y-m-d',strtotime($this->input->post('txtdatetommi')));
			else:
				$txtdatetommi=null;
			endif; 		  		
				 			  			  
			  $cbobank   = $this->input->post('cbobank');	
			  $txtyear	 = $this->input->post('txtyear');						  			  			  			  			  $txtremark =	$this->input->post('txtremarks');
			   $cbo_flag_demo = $this->input->post('cbo_demo_unit');
			  			 			  												 
			//---------------------------------------------- insert tbl_master_pp		      			
			$data=array('stock_no'=>$txtnostock,
						'branch_id'=>$ses_branch,	
						'id_type'=>$cbotype, 
						'user_stock'=> $this->session->userdata('name'),	
						'location_stock'=>$cboloc,
						'csi'=>$txtcsi,
						'vin'=>$txtvin,																		
						'engine'=>$txtengine, 
						'id_colour'=>$cbowarna,							
						'intake'=> $cbointake,		
						'year' =>$txtyear,	
						'pay_date_mmi' => $txtdatetommi,																											
						'rec_date'=>$txtdate_reciep,						
						'id_bank' =>$cbobank,																												
						'status'=>$status_aktif,								
						'status_flag_stock'=>  $flag_status_stock,																					
						'email_user_stock'=> $this->session->userdata('email'),
						'remarks_stock'=>$txtremark,
						'flag_demo_unit' =>$cbo_flag_demo,
						'flag_cross_sell'=>'0',					
						'date_create'=> date('Y-m-d')						
						);															
			$this->db->insert('tbl_stock_master',$data);	
			
			$data_ses = array('ses_nostock' => $txtnostock);
			$this->session->set_userdata($data_ses);
			
			return true;											     							 
	}
	
	
	function edit_master_stock()	
	{
			  $txtnostock =$this->input->post('txtnostock');		
		      $cboloc = $this->input->post('cboloc');  			 
			  $txtuser	= $this->input->post('txtuser');	
			  
			  $cbotype	= $this->input->post('cbotype');	
			  $cbowarna	= $this->input->post('cbowarna');	
			  
			 // $curr	= $this->input->post('curr');
			  //---------------------unformat price---------
			  //$price_unit	= $this->input->post('price_unit');	 
			  //if ( strstr( $price_unit, ',' ) ) $price_unit = str_replace( ',','', $price_unit );  
			  //-------------------------------------
			  
			  $txtvin	= $this->input->post('txtvin');	
			  $txtengine	= $this->input->post('txtengine');	
			  $txtvinupdate	= $this->input->post('txtvinupdate');	
			  $txtengineupdate	= $this->input->post('txtengineupdate');
			  
			  $txtcsi	= $this->input->post('txtcsi');	
			  $cbointake	= $this->input->post('cbointake');	
			  
			   if ($this->input->post('txtdatericived') !="") :
			      $txtdate_reciep = date('Y-m-d',strtotime($this->input->post('txtdatericived')));		
			   else:
			 	   $txtdate_reciep = null	;
			   endif;
			
			   if ($this->input->post('txtdatetommi') != ""): 	  	 			  
			      $txtdatetommi = date('Y-m-d',strtotime($this->input->post('txtdatetommi')));
			   else:
				  $txtdatetommi=null;
			   endif; 		  		
			 		  			 			  			  
			  $cbobank = $this->input->post('cbobank');	
			  $txtyear	= $this->input->post('txtyear');
			  $txtremark =	$this->input->post('txtremarks');
			  $cbo_flag_demo = $this->input->post('cbo_demo_unit');
				 
			  $data=array(							
						'id_type'=>$cbotype, 						
						'location_stock'=>$cboloc,
						'user_stock'=>$txtuser,
						'csi'=>$txtcsi,
						'vin'=>$txtvinupdate,																		
						'engine'=>$txtengineupdate, 
						'id_colour'=>$cbowarna,							
						'intake'=> $cbointake,		
						'year' =>$txtyear,	
						'pay_date_mmi' => $txtdatetommi,																											
						'rec_date'=>$txtdate_reciep,						
						'id_bank' =>$cbobank,
						'email_user_stock'=> $this->session->userdata('email'),	
						  'flag_demo_unit' =>$cbo_flag_demo,
						'remarks_stock'=>$txtremark,																																	
						//'total_amount' =>$price_unit ,
						//'curr_temp' => $curr, 																				
						);	
			$this->db->where('stock_no', $txtnostock);																	
			$this->db->update('tbl_stock_master',$data);	
			
			$this->session->unset_userdata('ses_nostock'); //unset session stock number dulu
			
			$data_ses = array('ses_nostock' => $txtnostock);
			$this->session->set_userdata($data_ses);
			
			return true;								
		  
	}
	
	
	function getedit_stock()
	{
	  $get_edit_stockno =$this->input->post('msg');	  		 				    							   				      $flag_status_del = "1" ;
	 
	  if (isset($get_edit_stockno) && trim($get_edit_stockno!=''))
	   {		
	           for ($i=0; $i < count($get_edit_stockno) ; $i++) { 		
					 $this->db->select("stock_no,branch_id,name_branch,id_type,desc_type,user_stock,location_stock,csi,vin,engine,intake,year,pay_date_mmi,rec_date,id_bank,name_bank,id_colour,colour,attach_foto,currency,price_sell,remarks_stock,flag_demo_unit")	;
					 $this->db->where('stock_no',$get_edit_stockno[$i]);
					 $this->db->where('status',$flag_status_del);											  	
					 $result_master = $this->db->get('qv_complite_master_stockin');							    				
			   }						
		      return $result_master->result() ;							  		  
		}else{
			  return false;
	    }   
	}
									
	
	function delete_with_edit_flag() {				
		  $delete = $this->input->post('msg');		  				  		 
		  $flag_del = '0';
		  
		  if(isset($delete) && trim($delete!=''))
		  {	   
			  // --------delete table master dan detail pp menggunakan flag status			    
				for ($i=0; $i < count($delete) ; $i++) { 				    
				    //array variable flag del					
				    $data = array('status'=>$flag_del);											
					$this->db->where('stock_no',$delete[$i]);
					$this->db->update('tbl_stock_master',$data); 						   				   	   				
			    }			   					  	   
			   return true;
		  }else{
			   return false;	  			  		  
		  }
	}
	
	
	/*function delete_checked_bmk() {			// true delete	
		  $delete = $this->input->post('msg');		  				  		 
		  
		  if(isset($delete) && trim($delete!=''))
		  {	   
			 // --------delete table master dan detail pp langsung ke table
			    
				for ($i=0; $i < count($delete) ; $i++) { 		
				   //ubah status delete menjadi 0 pada table master		    
				   $this->db->where('id_master', $delete[$i]);
				   $this->db->delete('tbl_master_bmk');						   				   	   
				   
				   //ubah status delete menjadi 0 pada table detail
				   $this->db->where('id_master', $delete[$i]);
				   $this->db->delete('tbl_detail_bmk');						   				   	   			  				 			    }			   					  	   
			   return true;
		  }else{
			   return false;	  			  		  
		  }
	} */
	public function get_search_date()
	{
	    $strdatestart =$this->input->post('txtdatestart');	
	    $strdatend =$this->input->post('txtdateend');	
	    $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	    $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		$status_fpb_flag = "1";  //flag sudah di print FPB
	    $ses_id_company = $this->session->userdata('id_company');		
		 
	    if($strdatestart !="" and $strdatend !="" ){	
		   $this->load->database();		
	       $this->db->select('id_master,no_voucher,id_company,id_dept,company,dept,paye_from,header_desc,date_create,date_reciep,user_reciep,status,currency,gran_total,short');
		   $this->db->where('id_company',$ses_id_company);		  
		   $this->db->where('date_reciep BETWEEN "'.$datestart.'" and "'.$dateend.'"');
		   $this->db->order_by('date_reciep','desc');
	 	   $result = $this->db->get('qv_complite_master_bmk');		
	       return $result;		 			  		 	   	 
	    }else{
		    redirect('c_bmk/c_create_bmk');	
		}
		  
	}			
	
	
	function search_data() 	// fungsi cari data pp model
	{
	   $cari          =  $this->input->post('txtcari');
	   $kategory      =  $this->input->post('cbostatus');	  	   	 
	   $ses_branch_id =  $this->session->userdata('branch_id');
	   $flag_status   = "1" ;  //jika 0 terdelete
	  
	   $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	   $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	   $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP
	   	   	   
	   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
	   {
		   redirect('c_slsstock/c_slsstock');  //direct ke url add_menu_booking/add_menu_booking
	   }else{	
	     $this->load->database();			  	   		  		 
	     $this->db->where('branch_id',$ses_branch_id);
	     $this->db->where('status',$flag_status);
	     $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."'  OR status_flag_stock='".$flag_wrs."' )", NULL, FALSE); // penggunaan and dan or
	     $this->db->like($kategory,$cari);		    		   
	     $result = $this->db->get('qv_complite_master_stockin');		 
	   
	   $this->session->set_flashdata('cari',$cari);	
	   return $result->result(); 
	   }
	} 
	  
 	 public function hitung_jumlah_stock(){ //tampil table untuk ngececk num_row						     	     		  	   			
	     $cari          =  $this->input->post('txtcari');
	     $kategory      =  $this->input->post('cbostatus');	
	    		 	 
		 $ses_id_branch = $this->session->userdata('branch_id');	
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');	
		 $this->db->where('branch_id',$ses_id_branch );							 		 		 				
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('status_flag_stock',$flag_stock); 
		 $this->db->like($kategory,$cari);	
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}
	
	 public function hitung_jumlah_dp(){ //tampil table untuk ngececk num_row						     	     		  	   			 		  
	     $cari          =  $this->input->post('txtcari');
	     $kategory      =  $this->input->post('cbostatus');	
	    	 
		 $ses_id_branch = $this->session->userdata('branch_id');	
		 $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');
		 $this->db->where('branch_id',$ses_id_branch );								 		 		 				
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('status_flag_stock',$flag_dp); 
		 $this->db->like($kategory,$cari);	
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}	
	
	 public function hitung_jumlah_wrs(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 
	     $cari          =  $this->input->post('txtcari');
	     $kategory      =  $this->input->post('cbostatus');	
		 
		 $cari          =  $this->input->post('txtcari');
	     $kategory      =  $this->input->post('cbostatus');	 
	 	 
		 $ses_id_branch = $this->session->userdata('branch_id');	
		  	
		 $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('branch_id',$ses_id_branch );						 		 		 				
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('status_flag_stock',$flag_wrs); 
		 $this->db->like($kategory,$cari);	
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}	    	    		    					 	 	 	    

public function update_send_cross_selling()
{
	$txnostock = $this->input->post('txtnostock');
	$id_cbobranch_utama = $this->input->post('cbobranchutama');
	$txtremarks_cross = $this->input->post('txtremarks_cross');	
	$ses_branch_cross = $this->session->userdata('name_branch');
	$ses_user_name_send = $this->session->userdata('name');
	$flag_cross_sell = "1" ;
	$flag_status_stock = "3" ;
	
	//select nama branch tujuan Cross Sellin buat di update kedalam table stock untuk Cross selling
	$this->load->database();
	$this->db->select('*');
	$this->db->where('branch_id',$id_cbobranch_utama);
	$query_branch = $this->db->get('tbl_branches');
	
	foreach($query_branch->result() as $row_branch) :
			$hasil_nama_branch = $row_branch->name_branch;
	endforeach ;	
	//end--------------------------------------------
	
	if ($txnostock !="") :
		$data_send_cross=array('status_flag_stock'=>$flag_status_stock,
								'flag_cross_sell'=> $flag_cross_sell,
								'user_send_cross_sell'=> $ses_user_name_send,
								'from_cross_sell'=> $ses_branch_cross,
								'dst_id_branch_cs'=> $id_cbobranch_utama,
								'dst_cross_sell'=> $hasil_nama_branch,	
								'date_send_approval' =>date('Y-m-d'),
								'remarks_cross_sell' =>$txtremarks_cross
								);
		$this->db->where('stock_no',$txnostock);
		$this->db->update('tbl_stock_master',$data_send_cross);
		
		$buff_ses = array('ses_srtock_no'=>$txnostock);
		$this->session->set_userdata($buff_ses );
		
		return true;
	else:
		return false;
	endif;	
	
}
  
//------------------------------------------------------------------------------------------------------------------		
		   
				 
}