<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_slssell_wrs extends CI_Model {	
				     
	 
    public function tampil_add_sell(){ //tampil table untuk ngececk num_row						     	     		  	   
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 		 		 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		
		 
		 $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('branch_id',$ses_id_branch );				 		 						
		 $this->db->where('delete_flag',$status_delete);		 		
		 $this->db->order_by('date_wrs_bodong','desc');
		 $result = $this->db->get('qv_complite_wrs_bodong');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 		 		 		 			
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		

		 $this->load->database();		
		 $this->db->select('*');						
		 $this->db->where('branch_id',$ses_id_branch );				 			 
		 $this->db->where('delete_flag',$status_delete);		 					
		 $this->db->order_by('date_wrs_bodong','desc');		
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_wrs_bodong')->result();		 		   
		 return $result ;
	}							  		
				    	   			  	   			 	 	 	   	
// get for wrs bodong ------------------------------------------------------------------------------------------------------------------		

	function get_data_novin()//model auto complete	wrs bodong
	{		       
	  $txtvinno = $this->input->post('txtvinno',TRUE); 
	
	  $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP	 
	  $status_del = "1";
	  $branch_id = $this->session->userdata('branch_id');
	  			 
	  $this->load->database();
	  $this->db->select('branch_id,stock_no,desc_model,desc_type,vin,engine,currency,price_sell,colour,location_stock,status_flag_stock,status');	
	  $this->db->where("branch_id",$branch_id);	
	  $this->db->where("status_flag_stock",$flag_stock);
	  $this->db->where("(vin like '%".$txtvinno."%' OR engine like '%".$txtvinno."%' )", NULL, FALSE);
	  $this->db->where("status",$status_del);	
	  $query = $this->db->get('qv_complite_master_stockin');					
	
	  return $query->result();		
	}
	
	function get_data_novin_full()//model auto complete	wrs bodong
	{		       
	  $txtvinno = $this->input->post('txtvinno',TRUE); 
	
	  $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP	 
	  $status_del = "1";
	  $branch_id = $this->session->userdata('branch_id');
					 
	  $this->load->database();
	  $this->db->select('branch_id,stock_no,desc_model,desc_type,vin,engine,currency,price_sell,colour,location_stock,status_flag_stock,status');	
	  $this->db->where("branch_id",$branch_id);	
	  $this->db->where("status_flag_stock",$flag_stock);	
	  $this->db->where("(vin = '".$txtvinno."' OR engine = '".$txtvinno."' )", NULL, FALSE);	  
	  $this->db->where("status",$status_del);
	  $query = $this->db->get('qv_complite_master_stockin');					
	
	  return $query->result();		  
	}	
	
	function get_edit_wrs_id()//model auto complete	wrs bodong
	{		       
	  $txtidwrs = $this->input->post('txtidwrs',TRUE); 
	
	  $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP	 
	  $status_del = "1";	
	  $branch_id = $this->session->userdata('branch_id');
					 
	  $this->load->database();
	  $this->db->select('*');	
	  $this->db->where("branch_id",$branch_id);		  
	  $this->db->where("id_wrs",$txtidwrs);	  	 
	  $query = $this->db->get('qv_complite_wrs_bodong');					
	
	  return $query->result();	
	}	
	
//get for real wrs-------------------------------------------------------------------------------------	
	function get_data_novin_from_sell()//model auto complete real wrs	
	{		       
	  $txtvinno = $this->input->post('txtvinno_sell',TRUE); 	
	  $flag_terjual= "0";			  	 
	  $status_del = "1";
	  $branch_id = $this->session->userdata('branch_id');	  	
	  			 
	  $this->load->database();
	  $this->db->select('id_trans_sales,branch_id,desc_model,desc_type,vin,engine,colour,status,status_flag_sell');	
	  $this->db->where("branch_id",$branch_id);		
	  $this->db->where("status_flag_sell",$flag_terjual);
	  $this->db->where("wrs_date",null);
	  $this->db->where("status",$status_del);	
	  $this->db->where("(vin like '%".$txtvinno."%' OR engine like '%".$txtvinno."%' )", NULL, FALSE);			 
	  $query_sell = $this->db->get('qv_complite_master_sell');					
	 
	  return $query_sell->result();		
	}
	
	function get_data_novin_from_sell_full()//model auto complete	
	{		       
	  $txtvinno = $this->input->post('txtvinno_sell',TRUE); 
	  $flag_terjual= "0";		 
	  $status_del = "1";	  
	  $branch_id = $this->session->userdata('branch_id');
					 
	  $this->load->database();
	  $this->db->select('id_trans_sales,branch_id,desc_model,desc_type,vin,engine,wrs_date,colour,status,status_flag_sell');	
	  $this->db->where("branch_id",$branch_id);		 
	  $this->db->where("status_flag_sell",$flag_terjual);
	  $this->db->where("wrs_date",null);
	  $this->db->where("status",$status_del);
	  $this->db->where("(vin = '".$txtvinno."' OR engine = '".$txtvinno."' )", NULL, FALSE);	 		
	  $query_sell = $this->db->get('qv_complite_master_sell');					
	
	  return $query_sell->result();		  
	}	
//end------------------------------------------------------------------------------------
	
	
  public function save_data()
  {
	$txtstocmod = $this->input->post('txtstocmod');  
	
	$txtwrsdatemod = date("Y-m-d",strtotime($this->input->post('txtwrsdatemod')));  		
	$str_wrs = "WRS" ;	
	$str_status = "1";  
	$str_delete = "1";  
	$flag_wrs_fake = "0";  
	
	$this->load->database();
	$this->db->select('*');
	$this->db->where('stock_no',$txtstocmod);
	$hasil_cek = $this->db->get('tbl_wrs_bodong');
	
	if ($hasil_cek->num_rows() == 0) :	
		$insert_data = array(
						"stock_no" => $txtstocmod,
						"date_wrs_bodong" => $txtwrsdatemod,
						"status_info_unit" => $str_wrs,					
						"status"=> $str_status,
						"delete_flag" =>$str_delete
						);
		$this->db->insert('tbl_wrs_bodong',$insert_data);	
		
		//edit flag stock master and temp wrs date---------------------------------
		$strstock_wrs = "-1" ;
		$edit_stock_wrs = array("status_flag_stock"=>$strstock_wrs,
								"temp_date_wrs"=>$txtwrsdatemod ,//update tambahan wrsdate di stock	
								);				
		$this->db->where('stock_no',$txtstocmod);
		$this->db->update('tbl_stock_master',$edit_stock_wrs);		
		
		return true;							
	else :	
		  $this->session->set_flashdata('pesan','VIN ALREADY REGISTER ON WRS');		
		  redirect('c_slssell_wrs/c_slsssell_wrs'); 
	endif;								
  }		
  
   public function save_edit_data_real_wrs()
   {
		$txttransid = $this->input->post('txtidtrans');  		
		$txtwrsdatemod = date("Y-m-d",strtotime($this->input->post('txtwrsdatemod_sell')));  
		if ($txttransid != "" ): 				
			$edit_stock_wrs = array("wrs_date"=>$txtwrsdatemod);				
			$this->db->where('id_trans_sales',$txttransid);
			$this->db->update('tbl_sales_unit_transaksi',$edit_stock_wrs);					
			return true;							
		else:
			return false;							
  		endif;
   }
  
    public function edit_data()
	{	
	    $txtidwrs = $this->input->post('txtidwrs');  	 		
		$txtstocmod = $this->input->post('txtstocmod');  
		$txtwrsdatemod = date("Y-m-d",strtotime($this->input->post('txtwrsdatemod')));  
		$txtstocnoold  = $this->input->post('txtstocnoold');  			
				
		if ($txtstocmod != $txtstocnoold ) :		  
		   if ($txtstocmod !='' && $txtstocnoold !="" ) :
		       echo"masuk tidak sama";
				$edit_data = array(
								"stock_no" => $txtstocmod,
								"date_wrs_bodong" => $txtwrsdatemod,																	
								);
								
				$this->db->where('id_wrs',$txtidwrs);
				$this->db->update('tbl_wrs_bodong',$edit_data); 																			
				
				//update with old no stock on table stock.		
				$str_ready_flag_stock = "1";							
				$edit_stock = array("status_flag_stock" => "1",
									"temp_date_wrs"=>$txtwrsdatemod,//update tambahan wrsdate di stock	
									);				
				$this->db->where('stock_no',$txtstocnoold);
				$this->db->update('tbl_stock_master',$edit_stock);
				
				//update with new no stock on table to wrs status.						
				$str_wrs_flag_stock = "-1";
				$edit_stock_wrs = array("status_flag_stock" => $str_wrs_flag_stock,
										"temp_date_wrs"=>$txtwrsdatemod, //update tambahan wrsdate di stock		
										 );				
				$this->db->where('stock_no',$txtstocmod);
				$this->db->update('tbl_stock_master',$edit_stock_wrs);
				
				return true;	
			 else :		
			 	return false;	
			 endif;		
		else: 			    
			if ($txtstocmod !='') :
				$edit_data = array(
								"stock_no" => $txtstocmod,
								"date_wrs_bodong" => $txtwrsdatemod,																	
								);
								
				$this->db->where('id_wrs',$txtidwrs);
				$this->db->update('tbl_wrs_bodong',$edit_data); 	
				return true;
			else:															
				return false;		
			endif;	
		endif;
								
	  }		
	  
	    public function delete_data()
	    {		 		
			$delete = $this->input->post('msg');  
			$str_status_delete = "0" ;
								
			if ($delete !='') {						
				for ($i=0; $i < count($delete) ; $i++) {	
					$del_data = array("delete_flag" => $str_status_delete,);																																																					
					$this->db->where('id_wrs',$delete[$i]);
					$this->db->update('tbl_wrs_bodong',$del_data); 
				}		
				return true;		
			}else{
				return false;		
			}								
	  }		
	  
	  public function send_ready_stock()
	  {
		  $strid_wrs = $this->input->post("msg");
		 
		  for ($intloop=0; $intloop < count($strid_wrs);$intloop++):		  	   		  
			   $this->load->database();
			   $this->db->select('*');
			   $this->db->where('id_wrs',$strid_wrs[$intloop]);
			   $result = $this->db->get('tbl_wrs_bodong');
			   
			   if ($result->num_rows() > 0 ):
					foreach($result->result() as $row_ready) : 					 					 		      
					  //update flag stock menjadi ready
					  $str_del = "0" ;		
					  $update_wrs_del = array('status' => $str_del); 
					  $this->db->where('id_wrs',$strid_wrs[$intloop]);
					  $this->db->update('tbl_wrs_bodong',$update_wrs_del);		   			   				  
				 
					  //update flag wrs menjadi menjadi remove
					  $str_give_ready = "1" ;				  
					  $update_stock_ready = array('status_flag_stock' => $str_give_ready ); 
					  $this->db->where('stock_no',$row_ready->stock_no);
					  $this->db->update('tbl_stock_master',$update_stock_ready);						  					 
			      endforeach;		  
				  
			endif;	  			
		  endfor; 
		  
		  return true;
		  
	  }
				
	  public function search_data() 	// fungsi cari data vendor
	  {
		   $cari = $this->input->post('txtcari');
		   $kategory =  $this->input->post('cbostatus');	  		   
		   //$ses_company = $this->session->userdata('id_company');
		   //$ses_id_dept = $this->session->userdata('id_dept');
		   $ses_branch_id = $this->session->userdata('branch_id');
		   $status_del = "1";	   
		   
		   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
		   {
			   redirect('c_slsssell_wrs/c_slsssell_wrs');  //direct ke url c_vendor/c_vendor
		   }else{	
			   $this->load->database();		   
			 //  $this->db->where('id_company',$ses_company);
			   //$this->db->where('id_dept',$ses_id_dept);
			   $this->db->where('branch_id',$ses_branch_id);
			   $this->db->where('delete_flag', $status_del);
			   $this->db->like($kategory,$cari);			  
			   $result = $this->db->get('qv_complite_wrs_bodong');		 
			   return $result->result(); 
		   }
		} 		    	   	
				 
}