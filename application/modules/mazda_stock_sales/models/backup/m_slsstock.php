<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_slsstock extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_master_stockin';
	 
    public function tampil_add_stock(){ //tampil table untuk ngececk num_row						     	     		  	   
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  
		 $flag_cross = "3" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  2 cross selling		 		 		
		 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('branch_id',$ses_id_branch );				 		 				
		 $this->db->where('status',$status_delete);		 
		 $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' OR status_flag_stock ='".$flag_cross."' )", NULL, FALSE); 
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS		  		 		 		
		 $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS		  		 		 	     
		 $flag_cross = "3" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  2 cross selling		  		 		 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('branch_id',$ses_id_branch );				 
		 $this->db->where('status',$status_delete);		 
		 $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' OR status_flag_stock ='".$flag_cross."' )", NULL, FALSE);  				
		 $this->db->order_by('rec_date','desc');		
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_master_stockin')->result();		 		   
		 return $result ;
	}							  		
	
	public function tampil_cabang_mazda(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 		
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $id_company = "3"; //idcompany 3 MAZDA
		 
		 $this->load->database();		
		 $this->db->select('branch_id,name_branch,place,');									 		 		
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('id_company',$id_company); 
		 $this->db->order_by('branch_id','desc');
		 $result = $this->db->get('tbl_branches');			     		 
		 return $result;
	}			
	
	 public function hitung_jumlah_stock(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 
		 $ses_id_branch = $this->session->userdata('branch_id');	
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');	
		 $this->db->where('branch_id',$ses_id_branch );							 		 		 				
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('status_flag_stock',$flag_stock); 
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}
	
	 public function hitung_jumlah_dp(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 
		 $ses_id_branch = $this->session->userdata('branch_id');	
		 $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');
		 $this->db->where('branch_id',$ses_id_branch );								 		 		 				
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('status_flag_stock',$flag_dp); 
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}	
	
	 public function hitung_jumlah_wrs(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 
		 $ses_id_branch = $this->session->userdata('branch_id');	
		  	
		 $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('branch_id',$ses_id_branch );						 		 		 				
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('status_flag_stock',$flag_wrs); 
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin');			     		 
		 return $result;
	}	    	    		    	
	
	function get_detail_modal_cross_selling()//model auto complete	Cross Selling		
	{		       
	  $txtidstock = $this->input->post('txtidstock',TRUE); 		
	  $str_branch = $this->session->userdata('branch_id');	  
	  $status_del = "1";		 
					 
	  $this->load->database();
	  $this->db->select('vin,engine,desc_type,colour');		 
	  $this->db->where("stock_no",$txtidstock);	  	 
	   $this->db->where("branch_id",$str_branch);	  	 
	  $query = $this->db->get('qv_complite_master_stockin');					
	
	  return $query->result();	
	}				 
				    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}