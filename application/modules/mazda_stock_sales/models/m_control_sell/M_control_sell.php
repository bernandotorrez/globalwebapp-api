<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_control_sell extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_master_sell';
	 
    public function tampil_add_stock(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 		 			
		 $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_terjual = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');						 		 		 				
		 $this->db->where('status',$status_delete);		 
		// $this->db->where('status_flag_sell',$flag_stock); 
		 $this->db->where("(status_flag_sell='".$flag_dp."' OR status_flag_sell='".$flag_terjual."' )", NULL, FALSE); 
		 $this->db->order_by('date_transaksi','desc');
		 $result = $this->db->get('qv_complite_master_sell');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    
		 		 
			     		  	   			 		 	 		 			
		 $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_terjual = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');										 
		 $this->db->where('status',$status_delete);		 
		// $this->db->where('status_flag_sell',$flag_stock); 			
		 $this->db->where("(status_flag_sell='".$flag_dp."' OR status_flag_sell='".$flag_terjual."' )", NULL, FALSE); 
		 $this->db->order_by('date_transaksi','desc');		
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_master_sell')->result();		 		   
		 return $result ;
	}
	
	
		
	
	 public function hitung_jumlah_dp(){ //tampil table untuk ngececk num_row						     	 
	     $strbranch =  $this->input->post('cbobranchutama');	    		  	   			 		 	 			
		 $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 if  ($strbranch =="" ) : // jika combo branch tidak ada	 
			 $this->load->database();		
			 $this->db->select('*');		 						 		 		 				
			 $this->db->where('status',$status_delete);		 
			 $this->db->where('status_flag_sell',$flag_dp); 
			 $this->db->order_by('date_transaksi','desc');
			 $result = $this->db->get('qv_complite_master_sell');			     		 
			 return $result;			 
		 else:	 		  
						 
			 $this->load->database();		
			 $this->db->select('*');	
			 $this->db->where('branch_id',$strbranch);		 						 		 		
			 $this->db->where('status',$status_delete);		 
			 $this->db->where('status_flag_sell',$flag_dp); 
			 $this->db->order_by('date_transaksi','desc');
			 $result = $this->db->get('qv_complite_master_sell');			     		 
			 return $result;			 
	     endif;		 
	}	
	
	 public function hitung_jumlah_sell(){ //tampil table untuk ngececk num_row						
	 
	     $strbranch =  $this->input->post('cbobranchutama');    		  	   			 		 	 		
		 $flag_sell = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
	     if  ($strbranch =="" ) : // jika combo branch tidak ada
			  $this->load->database();		
			  $this->db->select('*');								 		 		 				
			  $this->db->where('status',$status_delete);		 
			  $this->db->where('status_flag_sell',$flag_sell); 
			  $this->db->order_by('date_transaksi','desc');
			  $result = $this->db->get('qv_complite_master_sell');			     		 
			  return $result;
		 else:			      
				  $this->load->database();		
				  $this->db->select('*');	
				  $this->db->where('branch_id',$strbranch);								 		 		
				  $this->db->where('status',$status_delete);		 
				  $this->db->where('status_flag_sell',$flag_sell); 
				  $this->db->order_by('date_transaksi','desc');
				  $result = $this->db->get('qv_complite_master_sell');			     		 
				  return $result;		    		  
		 endif;	 
	}	    	    		
	
	public function tampil_cabang_mazda(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 		
		 //$flag_sell = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $id_company = "3"; //idcompany 3 MAZDA
		 
		 $this->load->database();		
		 $this->db->select('branch_id,name_branch,place,');									 		 		
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('id_company',$id_company); 
		 $this->db->order_by('branch_id','desc');
		 $result = $this->db->get('tbl_branches');			     		 
		 return $result;
	}							  		
		
	public function get_search_date()
	{
	    $strdatestart =$this->input->post('txtdatestart');	
	    $strdatend =$this->input->post('txtdateend');	
	    $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	    $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		$status_delete = "1"; //jika 0 terdelete jika 1 aktif
		
		
	    $flag_sell = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	    $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		 
		 
	    if($strdatestart !="" and $strdatend !="" ){	
		   $this->load->database();		
	       $this->db->select('*');
		   $this->db->where('status',$status_delete);
		  $this->db->where("(status_flag_sell='".$flag_dp."' OR status_flag_sell='".$flag_sell."' )", NULL, FALSE);  
		   $this->db->where('date_transaksi BETWEEN "'.$datestart.'" and "'.$dateend.'"');
		   $this->db->order_by('date_transaksi','desc');
	 	   $result = $this->db->get('qv_complite_master_sell');		
	       return $result;		 			  		 	   	 
	    }else{
		    redirect('mazda_stock_sales/c_control_sell/c_control_sell');	
		}
		  
	}
	
	function search_data() 	// fungsi cari data pp model
	{
	  // $strbranch          =  $this->input->post('cbobranch');	 	   	   	  	  
	   $kategory = $this->input->post('cbostatus');
	   $cari = $this->input->post('txtcari');
	   	
	   $flag_sell = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	   $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		 
	   $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	
	   	   	   
	   if ($cari == null or $kategory =='--Choose--') 
	   {
		   redirect('mazda_stock_sales/c_control_sell/c_control_sell');  
	   }else{	
	     $this->load->database();			  	   		  		 	    
	     $this->db->where('status',$status_delete);
	     $this->db->where("(status_flag_sell='".$flag_dp."' OR status_flag_sell='".$flag_sell."' )", NULL, FALSE);  
	     $this->db->like($kategory,$cari);		    		   
	     $result = $this->db->get('qv_complite_master_sell');		 	   
	     $this->session->set_flashdata('cari',$cari);	
		 
	     return  $result;
	   }
	} 				
	
	function shorting_table_branch() 	// fungsi cari data pp model
	{	   
	   $str_tampil_branch =  $this->input->post('cbobranchutama');	 	   	   	  	  	   	   	   	 
	   $flag_sell = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	   $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		 
	   $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	   
	   	   	   
	  if ($str_tampil_branch == "") 
	   {
		   redirect('mazda_stock_sales/c_control_sell/c_control_sell');  
	   }else{	
	       $this->load->database();			  	   		  		 	    
		   $this->db->where('branch_id',$str_tampil_branch);
	       $this->db->where('status',$status_delete);
	        $this->db->where("(status_flag_sell='".$flag_dp."' OR status_flag_sell='".$flag_sell."' )", NULL, FALSE);   	    		   
	       $result = $this->db->get('qv_complite_master_sell');		 	   	       
		 
	       return  $result;
	   } 
	} 			
	
	function get_type_vehicle()
	{			      
	     $this->load->database();		
		 $this->db->select('id_type,desc_type');			 
		 $this->db->where('status','1');			
		 $result = $this->db->get('tbl_brand_type');			     		 
		 return $result->result();	 	
	}	
	
	function total_stock_date()
	{
		 $strdatestart =$this->input->post('txtdatestart');	
		 $strdatend =$this->input->post('txtdateend');
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif	
		  
		 $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
		 $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		 $flag_sale = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		  
	   if($strdatestart !="" and $strdatend !="" ):
		  $this->load->database();		
		  $this->db->select('*');								 		 		 				
		  $this->db->where('status',$status_delete);		
		  $this->db->where('date_transaksi BETWEEN "'.$datestart.'" and "'.$dateend.'"'); 
		  $this->db->where('status_flag_sell',$flag_sale); 
		  $this->db->order_by('date_transaksi','desc');
		  $result = $this->db->get('qv_complite_master_sell');			     		 
		  return $result;
		endif;
		
	}		
	
	function total_stock_dp()
	{
		  $strdatestart =$this->input->post('txtdatestart');	
		  $strdatend =$this->input->post('txtdateend');	
		  $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  
		  $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
		  $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		  $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		  
		if($strdatestart !="" and $strdatend !="" ):
			 $this->load->database();		
			 $this->db->select('*');								 		 		 				
			 $this->db->where('status',$status_delete);		
			 $this->db->where('date_transaksi BETWEEN "'.$datestart.'" and "'.$dateend.'"'); 
			 $this->db->where('status_flag_sell',$flag_dp); 
			 $this->db->order_by('date_transaksi','desc');
			 $result = $this->db->get('qv_complite_master_sell');			     		 
			 return $result;	
		endif;	 
	}		
	
	
	
	
	function total_selling_cari_data()
	{
		$strcari = $this->input->post('txtcari');
		$strcategory = $this->input->post('cbostatus');
		$str_flag_sell= '0';
		$str_flag_delete = '1';
					
		$this->load->database();
		$this->db->select('*') ;
		$this->db->like($strcategory,$strcari);
		$this->db->where('status_flag_sell',$str_flag_sell);		
		$this->db->where('status',$str_flag_delete);				
		$result = $this->db->get('qv_complite_master_sell');
		
		return $result;
			
	} 			  	   			 	
	
	function total_dp_cari_data()
	{
		$strcari = $this->input->post('txtcari');
		$strcategory = $this->input->post('cbostatus');
		$str_flag_dp= '2';
		$str_flag_delete = '1';				
		
		$this->load->database();
		$this->db->select('*') ;
		$this->db->like($strcategory,$strcari);
		$this->db->where('status_flag_sell',$str_flag_dp);		
		$this->db->where('status',$str_flag_delete);		
		
		$result = $this->db->get('qv_complite_master_sell');		
		return $result;
	} 			  	   			 	
		
	
//EXCEL ALL -----------------------------------------------------------------------	  	   			 	
	 function ToExcelAll(){ // buat narik table to excel						    
	 			 
		 $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		 $str_cbobranch =$this->input->post("cbobranch");
		 $str_cbo_unit_status = $this->input->post("cbostatusunit");
		 $tglstart = $this->input->post('txtrptdtstart');
		 $tglend =  $this->input->post('txtrptdtend'); 
		 
		 $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	     $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal 
		 
		 $str_flag_sell= '0'; // flag sell berarti terjual	
		 $str_flag_dp= '2'; // flag dp berarti 2				  
	   
	  	 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
				 		           		    			 
			 $this->load->database();				 	
			 $this->db->select('*');								 		 
			 
			 if ($str_cbobranch != "") :			 		  
			 	 $this->db->where('branch_id',$str_cbobranch);
			 endif;		
			 						 	
			 if ($str_cbotype != "") :			 		  
			 	 $this->db->where('id_type',$str_cbotype);
			 endif;	 
			 
			  if ($str_cbo_unit_status == "0") :			 		  
			 	 $this->db->where('status_flag_sell',$str_cbo_unit_status);
			  else :
			  		if ($str_cbo_unit_status == "2") :			 		  
			 		   $this->db->where('status_flag_sell',$str_cbo_unit_status);
					endif;								 
			  endif;	 
			 							 	 
			 $this->db->where('status',$status_delete );					 			 											
			 $this->db->where('date_aloc BETWEEN "'. $datestart. '" and "'.$dateend.'"');
			 $this->db->where("(status_flag_sell='".$str_flag_sell."' OR status_flag_sell='".$str_flag_dp."' )", NULL, FALSE);			
			 $this->db->order_by('date_aloc','desc');
			 $result = $this->db->get('qv_complite_master_sell');
			 			
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
				
		 }else{
				 return null ;				
		 } 		 						 		  								 
	}	 
	
	 function ToExcelAll_total_selling(){ // buat narik table to excel						    
	 			 
		 $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		 $str_cbobranch =$this->input->post("cbobranch");
		 
		 $tglstart = $this->input->post('txtrptdtstart');
		 $tglend =  $this->input->post('txtrptdtend'); 
		 
		 $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	     $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal 
		 
		 $str_flag_sell= '0'; // flag stock berari 1				   
	  	 $status_delete = '1'; //jika 0 terdelete jika 1 aktif			
				 		           		    			 
			 $this->load->database();				 	
			 $this->db->select('*');								 		 
			 
			 if ($str_cbobranch != "") :			 		  
			 	 $this->db->where('branch_id',$str_cbobranch);
			 endif;		
			 						 	
			 if ($str_cbotype != "") :			 		  
			 	 $this->db->where('id_type',$str_cbotype);
			 endif;	 
			 							 	 
			 $this->db->where('status',$status_delete );	
			 $this->db->where('status_flag_sell', $str_flag_sell );	
			 $this->db->where('date_aloc BETWEEN "'. $datestart. '" and "'.$dateend.'"');			
			 $this->db->order_by('date_aloc','desc');
			 $result = $this->db->get('qv_complite_master_sell');
			 					
		     return  $result;				
		 
	 }
				
	 function ToExcelAll_total_dp(){ // buat narik table to excel						    
	 			 
		 $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		 $str_cbobranch =$this->input->post("cbobranch");
		 
		 $tglstart = $this->input->post('txtrptdtstart');
		 $tglend =  $this->input->post('txtrptdtend');  
		 
		 $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	     $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal
		 
		 $str_flag_dp= '2'; // flag dp berari 2				   
	  	 $status_delete = '1'; //jika 0 terdelete jika 1 aktif			
				 		           		    			 
			 $this->load->database();				 	
			 $this->db->select('*');								 		 
			 
			 if ($str_cbobranch != "") :			 		  
			 	 $this->db->where('branch_id',$str_cbobranch);
			 endif;		
			 						 	
			 if ($str_cbotype != "") :			 		  
			 	 $this->db->where('id_type',$str_cbotype);
			 endif;	 
			 							 	 
			 $this->db->where('status',$status_delete );	
			 $this->db->where('status_flag_sell', $str_flag_dp );	
			 $this->db->where('date_aloc BETWEEN "'. $datestart. '" and "'.$dateend.'"');		
			 $this->db->order_by('date_aloc','desc');
			 $result = $this->db->get('qv_complite_master_sell');
			 			
		 	return  $result;
	 }			    	  	
	 		
//end------------------------------------------------------------------------------------------------------------------			
				 
}