<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">


 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>
      

  
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);;color:#B66F03; font-size:16px;" ><?php echo "Create Customer"." | ".$header  ; ?> </div>
   <div class="panel-body">               
   
          
 <?php  echo form_open('mazda_stock_sales/c_customer/c_customer/multiple_submit','id = form_my');  ?>      
 <table>
     <tr>
     <td colspan="3" width="6%">
        <label for="Selected" >Selected: </label></td>
       <td>
       <select name="cbostatus" id="cbostatus" class="form-control">             
             <option value="cust_name">Customer</option>
             <option value="address">Address</option>
             <option value="phone">Phone</option>
        </select>               
       </td>
       <td>&nbsp;
       	
       </td>

       <td colspan="3">
        <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari" value="<?php echo $this->session->flashdata('cari'); ?>" > 
        </td>

       <td>
       <button id="btncari" name="btncari" class="btn btn-app btn-primary btn-xs radius-4" value="Search" type="submit">
                  <i class="ace-icon fa fa-search bigger-160"></i>
                   Search
                </button> 
       </td>
       <td colspan="3">&nbsp;  
             
       </td> 
       <td colspan="3">                        
     
        <button id="btnadd" name="btnadd" class="btn btn-app btn-primary btn-xs radius-4 addnew" value="Add New" type="button">
          <i class="ace-icon fa fa-book bigger-160"></i>
            Add New
        </button> 

        <button id="btnedit" name="btnedit" class="btn btn-app btn-success btn-xs radius-4 edit_vendor" value="Edit" type="button" disabled>
           <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
            Edit
        </button> 

		    <button id="btndel" name="btndel" class="btn btn-app btn-warning btn-xs radius-4" value="Delete" type="submit" onclick="return confirm('are you sure want to delete this data!')" disabled>
          <i class="ace-icon fa fa-ban bigger-160"></i>
            Delete
        </button>  
         
         </td>
      </tr> 
    </table>   
 <br>
 
 
<div class="row">
 <div class="col-md-12 col-md-offset-0">   
	<?php 
		echo '<table width="100%" class="table-striped table-bordered table-hover table-condensed"  >';		
		echo '<tr style="background:url('.base_url("asset/images/child-panel-bg.png").');font-weight:bold;font-size: 12px">';		
		echo '<td align="center" width="2%" >No</td>';							
		echo '<td align="center" width="9%">Company</td>' ;
		echo '<td align="center" width="9%">Branch</td>' ;
		echo '<td align="center" width="10%">customer</td>';			
		echo '<td align="center" width="15%">Address</td>' ;		
		echo '<td align="center" width="7%">Phone</td>' ;												
		echo '<td align="center" width="7%" align="center"><a href="javascript:void(0)" onclick="CheckAll()" style="text-decoration:none"> All</a>';
		echo ' / ';
		echo '<a href="javascript:void(0)" onclick="UncheckAll()" style="text-decoration:none">Uncheck</a></td>';
		echo '</tr>' ;		    		
		
		if ($ceck_row >= 1) { 	  
			  foreach($pp_cust as $row){ $intno++;				   			
     ?>         
       <?php if ($intno % 2 == 0) : ?>                          
                 <tr style="background:#F0F0F0;font-size:12px;color:#B90020" >
       <?php else:	?>
                 <tr style="font-size: 12px" >
       <?php endif?>                                
             <td width="2%" align="center"><?php echo $intno ; ?></td>                 
                <td><?php echo $row->company ;?></td>               	
                <td><?php echo $row->name_branch ;?></td>						
                <td width="16%"><?php echo $row->cust_name ; ?></td>                       
                <td><?php echo $row->address ; ?></td>                             
                <td><?php echo $row->phone ;?></td>
                                                                                               
                <td><div align="center"><input id="checkchild" name="msg[]" type="checkbox" value="<?php echo $row->id_cust ; ?>" />
    <?php form_close(); ?>             			            					
                </div>
            </td>
		  </tr> 	  
		 <?php 
		  		}
		 	}
   echo'</table>';	  				
		 ?> 
		 
		<?php		   			
		     if (! empty($pagination))			
			 { 
		  	   echo '<center><table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table></center>';				 
			 }			  	  						
		?>
        
 </div>  
</div> 
  
  
  
          
  </div>
  </div>
 
</div>   	
		 

<style>
/*label berwarna merah ketika validasi fom modal popup. */
	label
	{
		width:50%;
		display: block;
		margin-right:2%;
		text-align:left;
		line-height:22px;
	}	
    span.error
	{
		font: 12px ;
		color:red;
		margin-left:8px;
		line-height:22px;
	}	
/*------------------------- */
</style>
	
 <!-- Modal Buat save and edit -->
     <div id="tampilanform"></div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    
                    <div class="modal-body">
                          <table width="90%" class="table-condensed" align="center" >
						
                         <tr> 
                        <td id="row_label"> <label for="Id Vendor"  class="col-xs-8" style="color:#C00">ID Customer</label></td>                              
                        
                          <td id="row_input">
                          <div class="control-group">
                               <div class="col-xs-12">
                               <input name="txtid"  type="text" class="form-control" id="txtid"  readonly="readonly" >    
                               </div> 
                           </div>
                          </td>                                  
                        </tr>    
                        <tr> 
                        <td> <label for="Vendor" style="color:#C00"  class="col-xs-9"> Name *</label></td>                              
                        
                          <td>
                          <div class="control-group">
                               <div class="col-xs-12">   
                                <input name="txtcust"  type="text" class="form-control txtfocus" id="txtvendor" maxlength="49"  >
                               </div> 
                           </div>    
                          </td>                                  
                        </tr>                             
                         <tr> 
                        <td> <label for="Phone" style="color:#C00"  class="col-xs-8">Phone *</label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-8">
                            <input name="txtphone" type="text" class="form-control" id="txtphone" maxlength="20" onkeypress="return isNumberKey(event)"  >
                           </div> 
                           </div>
                          </td>                                  
                        </tr>    
                         <tr> 
                        <td> <label for="Phone" style="color:#C00"  class="col-xs-8">Phone 2 *</label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-8">
                            <input name="txtphone2" type="text" class="form-control" id="txtphone2" maxlength="20" onkeypress="return isNumberKey(event)"  >
                           </div> 
                           </div>
                          </td>                                  
                        </tr>    
                         <tr> 
                        <td> <label for="Phone" style="color:#C00"  class="col-xs-8">Phone Office *</label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-8">
                            <input name="txtphoneoff" type="text" class="form-control" id="txtphoneoff" maxlength="20" onkeypress="return isNumberKey(event)"  >
                           </div> 
                           </div>
                          </td>                                  
                        </tr>    
                         <tr> 
                         
                        <td> <label for="Phone" style="color:#C00"  class="col-xs-8">Birth Date *</label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-8">
                             <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
            <input id="txtdatettg" name="txtdatettg" readonly type='txtdatettg' data-date-format="dd-mm-yyyy" class="form-control datepicker" />  
                           </div> 
                           </div>
                          </td>                                  
                        </tr>  
                        <tr> 
                        <td> <label for="Religion" style="color:#C00" class="col-xs-9">Religion * </label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-12">
                           
                           <select id="cboagama" name="cboagama" class="form-control"  >
                          
                           <option value="">- Choose -</option>                          
                           <option value="ISLAM">ISLAM</option>
                           <option value="KRISTEN">KRISTEN</option>
                           <option value="KATOLIK">KATOLIK</option>
                           <option value="HINDU">HINDU</option>
                           <option value="BUDHA">BUDHA</option>
                           <option value="OTHERS">OTHERS</option>
                          
                           </select>
                          
                            </div> 
                           </div>
                        
                          </td>                                  
                        </tr>  
                        
                        <tr>
                        	   <td> <label for="Address" style="color:#C00" class="col-xs-9">Gender * </label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-12"> 
                            <table>
                            <tr>
                            <td class="col-xs-3" >                         
                              <div align="center">
                                <input type="radio" value="L" class="form-contol" id="rdl" name="rdl" t /> 
                                L<br>
                              </div></td>                              
                            <td>
                            
                              <div align="center">
                                <input type="radio" value="P" class="form-contol" id="rdp" name="rdp" t /> 
                                P<br>	
                              </div></td>
                            </tr>
                            </table>	
                           </div> 
                           </div>
                            
                            
                        </tr>                     
                        <tr> 
                        <td> <label for="Address" style="color:#C00" class="col-xs-9">Address * </label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-12">
                            
                             <textarea id="txtalamat" name="txtalamat"  class="form-control"></textarea>
                           </div> 
                           </div>
                        
                          </td>                                  
                        </tr>  
                        
                        
                          <tr> 
                        <td> <label for="Email" style="color:#C00" class="col-xs-9">Email * </label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-12">
                            
                             <input id="txtemail" name="txtemail"  class="form-control"></textarea>
                           </div> 
                           </div>
                        
                          </td>                                  
                        </tr>  
                        
                                                               
                        </table>                 
                    </div> 
                    
                     <div class="modal-footer">
                        <input type="submit" name="btnsave" id="btnsave" value="Submit " class="btn btn-primary"/>
                        <input type="submit" name="btnupdate" id="btnupdate" value="Submit Update" class="btn btn-primary"/>                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal Buat edit -->  	



<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.validate.js" charset="UTF-8"></script>

<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->

<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 

  
<script>
// fungsi untik checklist semua otomatis
function CheckAll()
{
    l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = true;		
		}	
		document.getElementById('btnedit').disabled = true;	
		document.getElementById('btndel').disabled = false;	
		document.getElementById('btnsendtab').disabled = true;	
}

function UncheckAll()
{
   l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = false;			 
		}
		document.getElementById('btnedit').disabled = true;
		document.getElementById('btndel').disabled = true;
		document.getElementById('btnsendtab').disabled = true;
}


//--------------------function disbaled enabled button with check book

$(function() {  //.btn hnya untuk tombol edit yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn-success').removeAttr('disabled');			
        }
        else{
            $('.btn-success').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btn-warning').removeAttr('disabled');			
        }
        else{
            $('.btn-warning').attr('disabled', 'disabled');								
        }
    });
});


  
//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});		  


//--------------function number only
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 45 && charCode != 41 && charCode != 40 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	
</script>

<script>
// Show modal pop up for addnew----------
$(function(){ 
	  $(document).on('click','.addnew',function(e){		       		  
			$("#myModal").modal('show');						  
			$("#myModalLabel").html("Create New Customer");	
			$("#txtcust").val("")	;
			$("#txtphone").val("")	;
			$("#txtphone1").val("")	;
			$("#txtphoneoff").val("")	;
			$("#cboagama").val("- Choose -") ;
			$("#txtalamat").val("")	;			
			$("#txtemail").val("")	;			
		    $("#row_label").hide();	
			$("#row_input").hide();		
			$("#btnupdate").hide();	
		    $("#btnsave").show();		       												
	  });																						   	 	   
});		
</script>

<script>
 // Show modal pop up for edit and then get data table edit vendor to modal popup
$(function(){
  $(document).on('click','.edit_vendor',function(e){
	
		var req_id = $('input[type="checkbox"]:checked').val()
		var url = '<?php echo base_url("mazda_stock_sales/c_customer/c_customer/get_idcust_modal_update"); ?>';
					
		$("#myModal").modal('show');  	
		$("#myModalLabel").html("Edit Customer");			
						
		$.ajax({			
			type:'POST',
			url: url,
			dataType: "json",
			data: {id_customer:req_id}, //cari berdasar kan ID_vendor yang di kirim dan di simpan pada var req_id 
			success: function(data, textStatus, jqXHR ) {				    			
			    $("#row_label").show();	
			    $("#row_input").show();	
				$("#btnsave").hide();
				$("#btnupdate").show();				
				$('[name="txtid"]').val(req_id);
				$('[name="txtcust"]').val(data.cust_name);
				$('[name="txtphone"]').val(data.phone);	
				$('[name="txtphone2"]').val(data.phone2);	
				$('[name="txtphoneoff"]').val(data.phone_office);	
				$('[name="cboagama"]').val(data.religion);		
				$('[name="txtdatettg"]').val(data.ttg);
				$('[name="txtemail"]').val(data.email);	
				$('[name="txtalamat"]').val(data.address);	
				if(data.jenkel == "L") {
					$("#rdl").attr("checked", "checked");
					$("#rdp").attr("checked", false)	
			    }else{
					$("#rdp").attr("checked", "checked" )			
					("#rdl").attr("checked", false);
				}
							
			}
						
		});				
		
   });	
});	
</script>

<script>
$(function(){
	//jquery untuk post submit save / edit
    $('#myform').on('submit', function(e){ //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
        e.preventDefault();		
		var url = '<?php  echo base_url('mazda_stock_sales/c_customer/c_customer/multiple_submit');  ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
            success: function(data){              
            }
        });
    });
});
</script>

<script>
$(document).on('change','#rdl',function(e){ 
	if ($("#rdl").attr("checked", "checked")){
		$("#rdp").attr("checked", false)	
	}							
});

$(document).on('change','#rdp',function(e){ 
	if ($("#rdp").attr("checked", "checked")){
		$("#rdl").attr("checked", false)	
	}							
});
</script>