<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet"
  media="screen">

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>
<div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Info:</span>
  <?php  echo $this->session->flashdata('pesan_fail') ;?>
</div>
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_insert_fail') != ""){ ?>
<div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Info:</span>
  <?php  echo  $this->session->flashdata('pesan_insert_fail');?>
</div>
<?php } ?>



<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">
  <div class="panel panel-default ">
    <div class="panel-heading" id="label_head_panel" style="color:#B66F03; font-size:18px;">Selling Unit</div>
    <?php  echo form_open_multipart('mazda_stock_sales/c_slssell_unit/c_crud_sell/multiple_submit',array('class'=>'form-multi'));  ?>

    <!-- Responsive Form for Mobile and Web Start -->
    <div class="container">

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Company</label>

            <input type="text" id="txtcom" name="txtcom" class="form-control" readonly
              value="<?php echo $this->session->userdata('company') ;?>" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Transaction No</label>

            <input type="text" id="txtnotrans" name="txtnotrans" class="form-control" readonly
              value="<?php echo $transnumber ;?>" />
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Branch</label>

            <input type="text" id="txtbranch" name="txtbranch" class="form-control" readonly
              value="<?php echo $this->session->userdata('name_branch') ;?>" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">User Created</label>

            <input type="text" id="txtusersell" name="txtusersell" class="form-control " readonly
              value="<?php echo $this->session->userdata('name'); ?>">
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Engine</label>

            <input type="text" id="txtengine" name="txtengine" class="form-control" readonly />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">VIN / Engine *</label>

            <input type="text" id="txtvinno" name="txtvinno" class="form-control" />
            <input name="txtnostock" type="text" id="txtnostock" size="30" hidden="true" />
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Model Unit</label>

            <input type="text" id="txtengine" name="txtengine" class="form-control" readonly />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">SPK No *</label>

            <div class="input-group">
              <span class="input-group-addon">
                <label id="txt_curr"> <?php echo $this->session->userdata('branch_short') ?> </label>
              </span>

              <input type="text" id="txtspk" name="txtspk" class="form-control col-xs-2 col-xs-offset-0" />

              <input type="hidden" id="txtbrpk" name="txtbrpk" class="form-control"
                value="<?php echo $this->session->userdata('branch_short') ?>" readonly="readonly" />


            </div>
          </div>

        </div>
      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Type Unit</label>

            <input type="text" id="txttype" name="txttype" class="form-control" readonly />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">Date Alocation</label>

            <input type="text" id="txtdatealoc" name="txtdatealoc" class="form-control" readonly
              value="<?php echo date('d-m-Y') ?>">
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Colour</label>

            <input type="text" id="txtwarna" name="txtwarna" class="form-control" readonly />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">WRS Date</label>

            <input type="text" id="txtwrsdate" name="txtwrsdate" class="form-control" readonly="readonly">
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Location Stock</label>

            <input type="text" id="txtlokasi" name="txtlokasi" class="form-control" readonly="readonly" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">Customer *</label>

            <!-- <input name="txtcust" type="text" class="form-control" id="txtcust" placeholder="Customer" maxlength="40" /> -->
            
            <select name="txtcust" class="select2 form-control" id="txtcust">
							
							<option value="">- Customer - </option>

							<?php foreach($tampil_customer as $cust) { ?>
								<option value="<?=$cust->cust_name;?>"> <?=$cust->cust_name;?> </option>
							<?php } ?>

							</select>

            <input name="txtidcust" type="text" id="txtidcust" size="20" hidden="true" />
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#900">Currency</label>

            <input maxlength="20" id="curr" name="curr" class="form-control" readonly />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">Sales*</label>

            <select id="cbosales" name="cbosales" class="select2 form-control">
              <option value="">--Choose--</option>
              <?php foreach($tampil_sales as $row_sal){ ?>
              <option value="<?php echo $row_sal->id_sales ;?>"><?php echo $row_sal->sal_name ;?></option>
              <?php } ?>
            </select>
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#900">Price Unit</label>

            <input maxlength="70" id="price_unit" name="price_unit" class="form-control unit_price" readonly
              style="color:#A6090D" placeholder="0.00" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">Lama SPK</label>

            <input name="txtlamaspk" type="text" class="form-control" id="txtlamaspk" value="7" maxlength="4"
              readonly="readonly" />
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#900">Discount</label>

            <input  maxlength="70" id="txtdixc" name="txtdixc" class="discount form-control"  
            style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" 
            disabled="disabled" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">Remarks *</label>

            <textarea name="txtnote" class="form-control" id="txtnote"></textarea>
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#900">Payment + disc</label>

            <input  maxlength="70" id="result" name="result" class="amount form-control" 
       readonly style="color:#A6090D" placeholder="0.00"  /> 

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">Payment Type : *</label>

            <select id="cbopaystatus" name="cbopaystatus"  class="form-control">
                 <option value="">--Choose--</option>
               <?php foreach($tampil_payment_type as $row_pay){ ?>               
               		 <option value="<?php echo $row_pay->flag_payment_status ;?>"><?php echo $row_pay->desc_pay ;?></option>
                 <?php } ?>           
               </select>
          </div>
        </div>

      </div>
      <!-- Row End -->

    </div>
    <!-- Responsive Form for Mobile and Web End -->

    <div class="panel panel-default ">
      <div class="panel-heading" id="label_head_panel">
        <span style="color:#A6090D; font-weight:bold; ">
          Remaining Payment :
        </span>

        <input maxlength="70" id="txt_remain" name="txt_remain" class="r_payment" readonly style="color:#A6090D"
          placeholder="0.00" />
      </div>
      <div class="panel-body">
        <div class="col-md-12 col-md-offset-0">
        
        <!-- Table Responsive -->
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>No</th>
                <th style="color:#A6090D;">Payment Description *</th>
                <th style="color:#A6090D;">Date</th>
                <th style="color:#A6090D;">DP Or Payment Customer *</th>
                <th style="color:#A6090D;">Upload Transfer PDF*</th>
              </tr>
            </thead>
            <tbody class="detail">
            <tr>
                         <td class="no" align="center">1</td>                                            
                        <td>
                      
                        <input type="text" maxlength="70" id="txtdescpay"  name="txtdescpay"  placeholder="Description" size="24" class="form-control txtdescpay" disabled="disabled" />
                        </td>
                        <td>
                          <input type="text" maxlength="70"  name="txtdatedet"  placeholder="Date Payment" size="30" class="form-control datepicker"   data-date-format="dd-mm-yyyy" readonly disabled="disabled"/>
                        </td>                     
                      
                        <td>
                         <input maxlength="14"  name="txtdppay" type="text" placeholder="0.00" size="20" onkeypress="return isNumberKey(event)" class="form-control price" style="color:#A6090D"  disabled="disabled" id="txtdatedet"  /> 
                        </td>                      
                        <td>                                                
                         <input type="file" id="txtupload" name="userfile" class="btn btn-success uploadfoto" disabled="disabled" />  
                        </td>  
                     </tr>


            </tbody>
          </table>
        </div>
        <!-- Table Responsive -->

        <div class="row">
            <div class="form-group text-center">
                <button id="btnsave" name="btnsave" value="Save" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
                    <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                    Save
                </button>


                <a href="<?=base_url('mazda_stock_sales/c_slssell_unit/c_slsssell');?>" class="btn btn-app btn-danger btn-xs radius-4">
                    <i class="ace-icon fa fa-close bigger-160"></i>
                    Cancel
                </a>

            </div>
        </div>

        <!-- <div class="col-xs-4 col-xs-offset-0 ">
          <br />
          <table>
            <tr>
              <td>
                <input id="btnsave" name="btnsave" type="submit" value="save" class="btn btn-danger " />
              </td>
              <td>&nbsp;</td>
              <td>&nbsp; </td>
              <td>
                <a href="<?php echo base_url('mazda_stock_sales/c_slssell_unit/c_slsssell');?>"
                  style="text-decoration:none;"><input id="btnback" name="btnback" type="button" value="Back To Table"
                    class="btn btn-warning" /> </a> </td>
            </tr>
          </table>
        </div> -->
      </div>
    </div>

    <?php form_close(); ?>

  </div> <!-- /panel -->
</div> <!-- /div class="col-lg-12 -->

<?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>


<script type="text/javascript">
  $('.datepicker').datetimepicker({
    language: 'id',
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
  });
</script>


<script type="text/javascript">
  //--------------function number only
  $('.select2').select2();
  function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }
  //--------------function back only 

  function isbackspacerKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31) {
      return false;
    } else {
      return true;
    }
  }


  //----------------set focus----
  $(function () {
    $("#txtpayfrom").focus();
  });
</script>


<script script type="text/javascript">
  //calculate and add field--------------
  $(function () {


    $('body').delegate('.discount', 'change', function () {
      var unit_price = numeral().unformat($('.unit_price').val());
      var discount = numeral().unformat($('.discount').val());
      var price = numeral().unformat($('.price').val());



      var hasil_payment_total = discount + price;
      var hasil_r_payment = unit_price - hasil_payment_total;

      $('.r_payment').val(hasil_r_payment);
      $('.amount').val(hasil_payment_total);

    });

    $('body').delegate('.discount', 'blur', function () {
      var discount = $('.discount').val();
      var r_payment = $('.r_payment').val();
      var amount = $('.amount').val();

      $('.discount').val(numeral(discount).format('0,0.00'));
      $('.r_payment').val(numeral(r_payment).format('0,0.00'));
      $('.amount').val(numeral(amount).format('0,0.00'));
    });

    $('body').delegate('.price', 'change', function () {
      var price = numeral().unformat($('.price').val());
      var discount = numeral().unformat($('.discount').val());
      var unit_price = numeral().unformat($('.unit_price').val());

      var hasil_payment_total = discount + price;
      var hasil_r_payment = unit_price - hasil_payment_total;

      $('.r_payment').val(hasil_r_payment);
      $('.amount').val(hasil_payment_total);
    });

    $('body').delegate('.price', 'blur', function () {
      var price = $('.price').val();
      var r_payment = $('.r_payment').val();
      var amount = $('.amount').val();

      $('.price').val(numeral(price).format('0,0.00'));
      $('.r_payment').val(numeral(r_payment).format('0,0.00'));
      $('.amount').val(numeral(amount).format('0,0.00'));
    });


  });
</script>

<script type="text/javascript">
  //---------function autocomplite jquery
  $(document).ready(function () {
    $(function () {
      $("#txtvinno").autocomplete({
        minLength: 0,
        delay: 0,
        source: function (request, response) {

          var str_url = '<?php echo site_url('mazda_stock_sales/c_slssell_unit/c_crud_sell/suggest_vin_engine '); ?>';
          var str_novin = $("#txtvinno").val();



          $.ajax({
            url: str_url,
            data: {
              txtvinno: str_novin
            },
            dataType: "json",
            type: "POST",
            success: function (data) {
              response(data);
            }
          });
        },
        select: function (event, ui) {
          $('#txtnostock').val(ui.item.txtnostock);
          $('#txtengine').val(ui.item.txtengine);
          $('#txtmodel').val(ui.item.txtmodel);
          $('#txttype').val(ui.item.txttype);
          $('#txtwarna').val(ui.item.txtwarna);
          $('#txtlokasi').val(ui.item.txtlokasi);
          $('#curr').val(ui.item.curr);

          if (ui.item.temp_date_wrs != '01-01-1970') {
            $('#txtwrsdate').val(ui.item.temp_date_wrs);
          } else {
            $('#txtwrsdate').val('');
          }

          $('#price_unit').val(ui.item.price_unit);
          $('#txt_remain').val(ui.item.price_unit);

          //enabled detail
          $('.txtdescpay').removeAttr('disabled');
          $('.datepicker').removeAttr('disabled');
          $('.price').removeAttr('disabled');
          $('.discount').removeAttr('disabled');
          $('.uploadfoto').removeAttr('disabled');
        }
      });

    });

  });
</script>

<script type="text/javascript">
  //automatic show price 

  var htmlobjek;
  $(document).ready(function () {
    $("#txtvinno").change(function () {
      var txtvinno_full = $("#txtvinno").val();
      var url = '<?php echo site_url('mazda_stock_sales/c_slssell_unit/c_crud_sell/get_vin_engine_full'); ?>';

      $.ajax({
        type: 'POST',
        url: url,
        dataType: "json",
        data: {
          txtvinno: txtvinno_full
        },
        cache: false,
        success: function (data, textStatus, jqXHR) {
          $('[name="txtnostock"]').val(data.txtnostock);
          $('[name="txtvinno"]').val(data.txtvin);
          $('[name="txtengine"]').val(data.txtengine);
          $('[name="txtmodel"]').val(data.txtmodel);
          $('[name="txttype"]').val(data.txttype);
          $('[name="txtwarna"]').val(data.txtwarna);
          $('[name="txtlokasi"]').val(data.txtlokasi);
          $('[name="curr"]').val(data.curr);

          if (data.temp_date_wrs != '01-01-1970') {
            $('[name="txtwrsdate"]').val(data.temp_date_wrs);
          } else {
            $('[name="txtwrsdate"]').val('');
          }

          $('[name="price_unit"]').val(data.price_unit);
          $('[name="txt_remain"]').val(data.result);

          //enabled detail
          $('.txtdescpay').removeAttr('disabled');
          $('.datepicker').removeAttr('disabled');
          $('.price').removeAttr('disabled');
          $('.discount').removeAttr('disabled');
          $('.uploadfoto').removeAttr('disabled');

        }
      });
    });
  });
</script>


<script type="text/javascript">
  //---------function autocomplite jquery
  $(document).ready(function () {
    $(function () {
      $("#txtcust").autocomplete({
        minLength: 0,
        delay: 0,
        source: function (request, response) {

          var str_url = '<?php echo site_url('mazda_stock_sales/c_slssell_unit/c_crud_sell/suggest_customer'); ?>';
          var str_customer = $("#txtcust").val();

          $.ajax({
            url: str_url,
            data: {
              txtcust: str_customer
            },
            dataType: "json",
            type: "POST",
            success: function (data) {
              response(data);
            }
          });
        },
        select: function (event, ui) {
          $('#txtidcust').val(ui.item.txtidcust);
        }
      });

    });

  });
</script>

<script type="text/javascript">
  //automatic show price 

  var htmlobjek;
  $(document).ready(function () {
    $("#txtcust").change(function () {
      var txtcust_full = $("#txtcust").val();
      var url = '<?php echo site_url('mazda_stock_sales/c_slssell_unit/c_crud_sell/get_customer_full'); ?>';

      $.ajax({
        type: 'POST',
        url: url,
        dataType: "json",
        data: {
          txtcust: txtcust_full
        },
        cache: false,
        success: function (data, textStatus, jqXHR) {
          $('[name="txtcust"]').val(data.txtcust);
          $('[name="txtidcust"]').val(data.txtidcust);
        }
      });
    });
  });
</script>