<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet"
  media="screen">

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>
<div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Info:</span>
  <?php  echo $this->session->flashdata('pesan_fail') ;?>
</div>
<?php } ?>




<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">
  <div class="panel panel-default ">
    <div class="panel-heading" id="label_head_panel" style="color:#B66F03; font-size:18px;">Selling Unit</div>
    <?php  echo form_open_multipart('mazda_stock_sales/c_slssell_unit/c_crud_sell/do_edit_sell',array('class'=>'form-multi'));  ?>

    <!-- Responsive Form for Mobile and Web Start -->
    <div class="container">
      <?php
			      if ($check_row >= 1) :  
				    foreach($tampil_edit_sell as $row_sell) :
	          ?>

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Company</label>

            <input type="text" id="txtcom" name="txtcom" class="form-control" readonly
              value="<?php echo $this->session->userdata('company') ;?>" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Transaction No</label>

            <input type="text" id="txtnotrans" name="txtnotrans" class="form-control" readonly
              value="<?php echo $row_sell->id_trans_sales ; ?>" />
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Branch</label>

            <input type="text" id="txtbranch" name="txtbranch" class="form-control" readonly
              value="<?php echo $this->session->userdata('name_branch') ;?>" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">User Created</label>

            <input type="text" id="txtusersell" name="txtusersell" class="form-control " readonly
              value="<?php echo $this->session->userdata('name'); ?>">
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Engine</label>

            <input type="text" id="txtengine" name="txtengine" class="form-control" readonly
              value="<?php echo $row_sell->engine ; ?>" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">VIN / Engine *</label>

            <input type="text" id="txtvinno" name="txtvinno" class="form-control" value="<?php echo $row_sell->vin ; ?>"
              readonly="readonly" />
            <input name="txtnostock" type="text" id="txtnostock" size="30" value="<?php echo $row_sell->stock_no ; ?>"
              hidden="true" />
            <input name="txtnostockbuff" type="text" id="txtnostockbuff" size="30"
              value="<?php echo $row_sell->stock_no ; ?>" hidden="true" />
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Model Unit</label>

            <input type="text" id="txtmodel" name="txtmodel" class="form-control" readonly
              value="<?php echo $row_sell->desc_model ; ?>" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">SPK No Current</label>

            <input type="text" id="txtspk" name="txtspk" class="form-control" value="<?php echo $row_sell->nospk ; ?>"
              readonly />
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">


          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">SPK No Edit*</label>

            <input type="text" id="txtnospkupdate" name="txtnospkupdate" class="form-control"
              value="<?php echo $row_sell->nospk ; ?>" />
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">


        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Type Unit</label>

            <input type="text" id="txttype" name="txttype" class="form-control" readonly
              value="<?php echo $row_sell->desc_type ; ?>" />
          </div>
        </div>



        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Date Alocation</label>

            <input type="text" id="txtdatealoc" name="txtdatealoc" class="form-control" readonly
              value="<?php  echo date('d-m-Y', strtotime($row_sell->date_aloc)) ; ?>">

          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Colour</label>

            <input type="text" id="txtwarna" name="txtwarna" class="form-control" readonly
              value="<?php echo $row_sell->colour ; ?>" />
          </div>
        </div>


        <div class="col-sm-1">


        </div>
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">WRS Date</label>

            <input type="text" id="txtwrsdate" name="txtwrsdate" data-date-format="dd-mm-yyyy"
              class="form-control datepicker" onkeypress="return isbackspacerKey(event)" value="<?php  
				 if ($row_sell->wrs_date == null || $row_sell->wrs_date == ""):
				     echo $row_sell->wrs_date ="";
				 else:
				     echo date('d-m-Y', strtotime($row_sell->wrs_date)) ; 
				 endif;  
				 ?>" readonly>

          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Location Stock</label>

            <input type="text" id="txtlokasi" name="txtlokasi" class="form-control" readonly="readonly"
              value="<?php echo $row_sell->location_stock ; ?>" />
          </div>
        </div>

        <div class="col-sm-1">


        </div>
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">Customer *</label>

            <!-- <input name="txtcust" type="text" class="form-control" id="txtcust" placeholder="Customer" maxlength="40"
              value="<?php echo $row_sell->cust_name ; ?>" />  -->
              
              <select name="txtcust" class="select2 form-control" id="txtcust">
							
							<option value="">- Customer - </option>

							<?php foreach($tampil_customer as $cust) { ?>
								<option value="<?=$cust->cust_name;?>"
								
								<?php if($cust->id_cust == $row_sell->id_cust) { echo 'selected'; } ?>
								
								> <?=$cust->cust_name;?> </option>
							<?php } ?>

					</select>
              
              <input name="txtidcust" type="text" id="txtidcust"
              size="20" value="<?php echo $row_sell->id_cust ; ?>" hidden="true" />

          </div>
        </div>


      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Currency</label>

            <input maxlength="20" id="curr" name="curr" class="form-control" readonly
              value="<?php echo $row_sell->curr_sell ; ?>" />

          </div>
        </div>


        <div class="col-sm-1">


        </div>
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">Sales*</label>

            <select id="cbosales" name="cbosales" class="form-control">
              <option value=""> - Sales - </option>
              <?php foreach($tampil_sales as $row_sal){ ?>
              <option value="<?php echo $row_sal->id_sales ;?>"
              
              <?php if($row_sal->id_sales == $row_sell->id_sales) { echo 'selected'; } ?>
              
              ><?php echo $row_sal->sal_name ;?></option>
              <?php } ?>
            </select>
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#900">Price Unit </label>

            <input maxlength="70" id="price_unit" name="price_unit" class="form-control unit_price" readonly
              style="color:#A6090D" placeholder="0.00"
              value="<?php echo  number_format($row_sell->price_sell ,2,'.',',');  ?>" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Lama SPK</label>

            <input name="txtlamaspk" type="text" class="form-control" id="txtlamaspk" maxlength="4"
              value="<?php echo "7" //echo $row_sell->lama_spk ; ?>" readonly="readonly" />
            <input hidden="true" type="text" name="txtdatetrans" id="txtdatetrans"
              value="<?php echo $row_sell->date_transaksi ?>" />
          </div>
        </div>


      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label">Discount </label>

            <input maxlength="70" id="txtdixc" name="txtdixc" class="discount form-control" style="color:#A6090D"
              placeholder="0.00" value="<?php echo  number_format($row_sell->discount ,2,'.',','); ?>"
              onkeypress="return isNumberKey(event)" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">Remarks *</label>

            <textarea name="txtnote" class="form-control" id="txtnote"><?php echo $row_sell->remarks; ?>
                </textarea>
          </div>
        </div>

      </div>
      <!-- Row End -->

      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" tyle="color:#900">Payment + Disc </label>
            <input maxlength="70" id="result" name="result" class="amount form-control" readonly style="color:#A6090D"
              placeholder="0.00" value="<?php echo  number_format($row_sell->payment_plus_discount ,2,'.',','); ?>" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D">Payment Status :</label>

            <select id="cbopaystatus" name="cbopaystatus" class="form-control">
              <option value="<?php echo $row_sell->flag_payment_status ;?>"><?php echo $row_sell->desc_pay ;?>
              </option>
              <?php foreach($tampil_payment_type as $row_pay){ ?>
              <option value="<?php echo $row_pay->flag_payment_status ;?>"><?php echo $row_pay->desc_pay ;?></option>
              <?php } ?>
            </select>
          </div>
        </div>

      </div>
      <!-- Row End -->

      <?php endforeach ; ?>
      <?php endif ;?>

    </div>
    <!-- Responsive Form for Mobile and Web End -->
   
    
    <div class="panel panel-default ">

      <div class="panel-heading" id="label_head_panel">
      <!-- Row Start -->
      <div class="row">
        <div class="col-sm-5">
          <div class="form-group">
            <label class="control-label" style="color:#A6090D; font-weight:bold; ">Total Payment :</label>

            <input maxlength="70" id="txt_tot_pay" name="txt_tot_pay" class="tot_pay " readonly style="color:#A6090D"
          placeholder="0.00" value="<?php echo number_format($row_sell->total_paymet_customer ,2,'.',',');  ?>" />

          </div>
        </div>

        <div class="col-sm-1">


        </div>

        <div class="col-sm-5">
          <div class="form-group">
          <label class="control-label" style="color:#A6090D; font-weight:bold; ">Remaining Payment :</label>

          <input maxlength="70" id="txt_remain" name="txt_remain" class="r_payment " readonly style="color:#A6090D"
          placeholder="0.00" value="<?php echo number_format($row_sell->remaining_amount_customer ,2,'.',',');  ?>" />
          </div>
        </div>

      </div>
      <!-- Row End -->
    
    
    </div>
    <!-- Responsive Form for Mobile and Web End -->


      <!-- Table Responsive -->
      <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>No</th>
                <th style="color:#A6090D;">Payment Description *</th>
                <th style="color:#A6090D;">Date</th>
                <th style="color:#A6090D;">DP Or Payment Customer *</th>
                <th style="color:#A6090D;">Upload Transfer PDF*</th>
                <th style="color:#A6090D;">Evidence</th>
              </tr>
            </thead>
            <tbody class="detail">
            <?php
                if ($check_row >= 1) :  
                  foreach($tampil_detail_sell as $row_det)  : $intno = $intno +1;
	          	 ?>
              <tr>
                <td class="no" align="center"><?php echo $intno ?></td>
                <td>
                  <input type="text" id="txtiddet" name="txtiddet[]" value="<?php echo $row_det->id_detail ; ?>"
                    hidden="true" /> <!-- iddet --->

                  <input type="text" maxlength="70" id="txtdescpay" name="txtdescpay" placeholder="Description"
                    size="24" class="form-control txtdescpay" value="<?php echo $row_det->desc_det ; ?>" readonly />
                </td>
                <td>
                  <input type="text" maxlength="70" name="txtdatedet" placeholder="Date Payment" size="30"
                    class="form-control" data-date-format="dd-mm-yyyy" readonly
                    value="<?php  echo date('d-m-Y', strtotime($row_det->pay_date)) ; ?>" />
                </td>

                <td>
                  <input maxlength="14" name="txtdppay" type="text" placeholder="0.00" size="20"
                    onkeypress="return isNumberKey(event)" class="form-control price" style="color:#A6090D"
                    id="txtdatedet" value="<?php echo  number_format($row_det->amount ,2,'.',','); ?>" readonly />
                </td>
                <td>
                  <input type="file" id="txtupload" name="userfile" class="btn btn-success uploadfoto"
                    disabled="disabled" />
                  <input type="text" name"txtaddresedit" id="txtaddresedit" value="<?php echo $row_det->attach_trans ?>"
                    hidden="true" />
                </td>
                <td align="center">
                  <?php
						   echo anchor_popup($row_det->attach_trans,"<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".'View'.'</a>'); 
                        ?>
                </td>
              </tr>
            </tbody>
            <?php  endforeach; ?>
            <?php endif; ?>
          </table>
        </div>
        <!-- Table Responsive -->

        <div class="row">
            <div class="form-group text-center">
                <button id="btnsave" name="btnsave" value="Save" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
                    <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                    Save
                </button>


                <a href="<?=base_url('mazda_stock_sales/c_slssell_unit/c_slsssell');?>" class="btn btn-app btn-danger btn-xs radius-4">
                    <i class="ace-icon fa fa-close bigger-160"></i>
                    Cancel
                </a>

            </div>
        </div>
      </div>
    </div>

    <?php form_close(); ?>

  </div> <!-- /panel -->
</div> <!-- /div class="col-lg-12 -->

<?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>


<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- jQuery_ui -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>"></script>

<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js"
  charset="UTF-8"></script>

<script type="text/javascript"
  src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js" charset="UTF-8">
</script>
<!-- Fungsi datepickier yang digunakan -->

<script type="text/javascript">
  $('.datepicker').datetimepicker({
    language: 'id',
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
  });
</script>


<script type="text/javascript">
  //--------------function number only

  function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }

  //--------------function back only only

  function isbackspacerKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31) {
      return false;
    } else {
      return true;
    }
  }


  //----------------set focus----
  $(function () {
    $("#txtpayfrom").focus();
  });
</script>


<script script type="text/javascript">
  //calculate and add field--------------
  $(function () {


    $('body').delegate('.discount', 'change', function () {
      var tr = $(this).parent().parent();
      var unit_price = numeral().unformat($('.unit_price').val());
      var discount = numeral().unformat($('.discount').val());
      var price = numeral().unformat(tr.find('.price').val());

      var t = 0;
      var jmlbaris = parseInt($('.price').length);

      for (var i = 0; i < jmlbaris; ++i) {
        t += parseFloat(numeral().unformat($('.price').eq(i).val()));
      }

      $('.tot_pay').val(t);
      var hasil_payment_total = discount + t;
      var hasil_r_payment = unit_price - hasil_payment_total;


      $('.r_payment').val(hasil_r_payment);
      $('.amount').val(hasil_payment_total);
    });

    $('body').delegate('.discount', 'blur', function () {
      var discount = $('.discount').val();
      var tot_pay = $('.tot_pay').val();
      var r_payment = $('.r_payment').val();
      var amount = $('.amount').val();

      $('.discount').val(numeral(discount).format('0,0.00'));
      $('.tot_pay').val(numeral(tot_pay).format('0,0.00'));
      $('.r_payment').val(numeral(r_payment).format('0,0.00'));
      $('.amount').val(numeral(amount).format('0,0.00'));
    });

    /* $('body').delegate('.price','change',function(){  	
	       var tr =  $(this).parent().parent();			 
		   var price = numeral().unformat(tr.find('.price').val());	
		   var discount=numeral().unformat($('.discount').val());
		   var unit_price = numeral().unformat($('.unit_price').val());
		   
		   
		   var t=0;
		   var jmlbaris=parseInt($('.evidence').length);
		   		  		   
		   for(var i=0;i < jmlbaris;++i){
			   t+=parseFloat(numeral().unformat($('.price').eq(i).val()));
		   }	
		   
		  		  			 									
		   var hasil_payment_total = discount + t ;
		   var hasil_r_payment = unit_price - hasil_payment_total	;		
		        		        		   
		   $('.r_payment').val(hasil_r_payment);
		   $('.amount').val(hasil_payment_total);
 	 });
	 
	 	$('body').delegate('.price','blur',function(){  		 
		   var price=$('.price').val();
		   var r_payment=$('.r_payment').val();
		   var amount=$('.amount').val();
		   		   
		   $('.price').val(numeral(price).format('0,0.00')) ;		  	  
		   $('.r_payment').val(numeral(r_payment).format('0,0.00')) ;
		   $('.amount').val(numeral(amount).format('0,0.00')) ;		  		 		 		 
 	 });  	*/


  });
</script>

<script type="text/javascript">
  //---------function autocomplite jquery
  $(document).ready(function () {
    $(function () {
      $("#txtvinno").autocomplete({
        minLength: 0,
        delay: 0,
        source: function (request, response) {

          var str_url = '<?php echo site_url('
          mazda_stock_sales / c_slssell_unit / c_crud_sell / suggest_vin_engine '); ?>';
          var str_novin = $("#txtvinno").val();



          $.ajax({
            url: str_url,
            data: {
              txtvinno: str_novin
            },
            dataType: "json",
            type: "POST",
            success: function (data) {
              response(data);
            }
          });
        },
        select: function (event, ui) {
          $('#txtnostock').val(ui.item.txtnostock);
          $('#txtengine').val(ui.item.txtengine);
          $('#txtmodel').val(ui.item.txtmodel);
          $('#txttype').val(ui.item.txttype);
          $('#txtwarna').val(ui.item.txtwarna);
          $('#txtlokasi').val(ui.item.txtlokasi);
          $('#curr').val(ui.item.curr);

          if (ui.item.temp_date_wrs != '01-01-1970') {
            $('#txtwrsdate').val(ui.item.temp_date_wrs);
          } else {
            $('#txtwrsdate').val('');
          }

          $('#price_unit').val(ui.item.price_unit);
          $('#txt_remain').val(ui.item.price_unit);
          $('#txt_tot_pay').val('0.00'); // total payment
          //enabled detail
          $('.txtdescpay').removeAttr('disabled');
          $('.datepicker').removeAttr('disabled');
          $('.price').removeAttr('disabled');
          $('.discount').removeAttr('disabled');
          $('.uploadfoto').removeAttr('disabled');

          //=============================					
          $('.discount').val('0.00');
          $('.amount').val('0.00');
          $('.price').val('0.00');

        }
      });

    });

  });
</script>

<script type="text/javascript">
  //automatic show price 

  var htmlobjek;
  $(document).ready(function () {
    $("#txtvinno").change(function () {
      var txtvinno_full = $("#txtvinno").val();
      var url = '<?php echo site_url('
      mazda_stock_sales / c_slssell_unit / c_crud_sell / get_vin_engine_full '); ?>';

      $.ajax({
        type: 'POST',
        url: url,
        dataType: "json",
        data: {
          txtvinno: txtvinno_full
        },
        cache: false,
        success: function (data, textStatus, jqXHR) {
          $('[name="txtnostock"]').val(data.txtnostock);
          $('[name="txtvinno"]').val(data.txtvin);
          $('[name="txtengine"]').val(data.txtengine);
          $('[name="txtmodel"]').val(data.txtmodel);
          $('[name="txttype"]').val(data.txttype);
          $('[name="txtwarna"]').val(data.txtwarna);
          $('[name="txtlokasi"]').val(data.txtlokasi);
          $('[name="curr"]').val(data.curr);

          if (data.temp_date_wrs != '01-01-1970') {
            $('[name="txtwrsdate"]').val(data.temp_date_wrs);
          } else {
            $('[name="txtwrsdate"]').val('');
          }

          $('[name="price_unit"]').val(data.price_unit);
          $('[name="txt_remain"]').val(data.result);
          $('[name="txt_tot_pay"]').val('0.00');

          //enabled detail
          $('.txtdescpay').removeAttr('disabled');
          $('.datepicker').removeAttr('disabled');
          $('.price').removeAttr('disabled');
          $('.discount').removeAttr('disabled');
          $('.uploadfoto').removeAttr('disabled');

          //------------------------
          $('.discount').val('0.00');
          $('.amount').val('0.00');
          $('.price').val('0.00');

        }
      });
    });
  });
</script>


<script type="text/javascript">
  //---------function autocomplite jquery
  $(document).ready(function () {
    $(function () {
      $("#txtcust").autocomplete({
        minLength: 0,
        delay: 0,
        source: function (request, response) {

          var str_url = '<?php echo site_url('
          mazda_stock_sales / c_slssell_unit / c_crud_sell / suggest_customer '); ?>';
          var str_customer = $("#txtcust").val();

          $.ajax({
            url: str_url,
            data: {
              txtcust: str_customer
            },
            dataType: "json",
            type: "POST",
            success: function (data) {
              response(data);
            }
          });
        },
        select: function (event, ui) {
          $('#txtidcust').val(ui.item.txtidcust);
        }
      });

    });

  });
</script>

<script type="text/javascript">
  //automatic show price 

  var htmlobjek;
  $(document).ready(function () {
    $("#txtcust").change(function () {
      var txtcust_full = $("#txtcust").val();
      var url = '<?php echo site_url('
      mazda_stock_sales / c_slssell_unit / c_crud_sell / get_customer_full '); ?>';

      $.ajax({
        type: 'POST',
        url: url,
        dataType: "json",
        data: {
          txtcust: txtcust_full
        },
        cache: false,
        success: function (data, textStatus, jqXHR) {
          $('[name="txtcust"]').val(data.txtcust);
          $('[name="txtidcust"]').val(data.txtidcust);
        }
      });
    });
  });
</script>