<!-- <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js');?>" ></script>
      
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />      
<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<link href="<?php echo base_url()?>asset/css/jquery.fileupload-ui.css" rel="stylesheet" media="screen"> -->

<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">
  <div class="panel panel-default" align="center">
    <div class="panel-heading" id="label_head_panel"
      style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;">
      <?php echo "Add / Edit Detail Unit" ; ?> </div>
    <div class="panel-body">
      <label>Header Info</label>
      <?php  echo form_open_multipart('mazda_stock_sales/c_slssell_unit/c_crud_sell/multiple_submit',array('class'=>'form-multi'),'id = my_form');  ?>

      <!-- Table Responsive -->
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>SPK NO</th>
              <th>TYPE</th>
              <th>COLOUR</th>
              <th>VIN</th>
              <th>ENGINE</th>
              <th>CUSTOMER</th>
              <th>SALES</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($str_trans_master as $row_head) :  ?>
            <tr>
              <td hidden="true"> <input value="<?php echo $row_head->id_trans_sales ?>" name="txtidtrans" /> </td>
              <td> <?=$row_head->nospk;?> </td>
              <td> <?=$row_head->desc_type;?> </td>
              <td> <?=$row_head->colour;?> </td>
              <td> <?=$row_head->vin;?> <input name="txtvin" hidden="true" value="<?=$row_head->vin;?>"> </td>
              <td> <?=$row_head->engine;?> <input name="txtengin" hidden="true" value="<?=$row_head->engine;?>"> </td>
              <td> <?=$row_head->cust_name;?> </td>
              <td> <?=$row_head->sal_name;?> </td>
            </tr>
            <?php  endforeach; ?>
          </tbody>
        </table>
      </div>
      <!-- Table Responsive -->

      <label>Detail </label>

      <!-- Table Responsive -->
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>NO</th>
              <th>DESCRIPTION</th>
              <th>DATE</th>
              <th>PAYMENT</th>
              <th>Evidence / Upload PDF</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody class="detail">
            <?php if ($str_trans_detail != "") :
		              foreach ($str_trans_detail as $row_detail)  : $intno = $intno +1;    ?>
            <tr>
              <td class="no" align="center"><?php echo $intno ?>
                <input name="txtiddet[]" value="<?php echo $row_detail->id_detail ?>" class="catchtxtiddet"
                  hidden="true" />
              </td>
              <td><input name="txdescdet[]" class="form-control desc_det" id="txdescdet[]" placeholder="Description"
                  value="<?php echo $row_detail->desc_det ?>" /></td>
              <td align="center"><input name="txtdatedet[]" class="form-control datepicker" id="txtdatedet[]"
                  placeholder="Date" value="<?php 
                    if ($row_detail->pay_date  !=""):
                    echo  date('d-m-Y', strtotime($row_detail->pay_date)); 
                  endif;	
                ?>" size="24" maxlength="70" readonly data-date-format="dd-mm-yyyy" />
              </td>
              <td align="center">
                <input name="txtpaydet[]" value="<?php echo  number_format($row_detail->amount,2,'.',',') ?>"
                  placeholder="0.00" size="35" class="form-control price" maxlength="70"
                  onkeypress="return isNumberKey(event)">
              </td>
              <td align="center">
                <?php
                if ($row_detail->attach_trans!= "") :
                    echo anchor_popup($row_detail->attach_trans,"<span class='glyphicon glyphicon-search ' aria-hidden='true'>".'View'."</span>");		
                else:
                  echo "No Data";  	
                  endif; 
                ?>
              </td>
              <td align="center">
                <button class="remove"><img src="<?php echo base_url('asset/images/delete.png'); ?>" /></button>

              </td>
            </tr>
            <?php  endforeach; ?>
            <?php endif; ?>

            <tr>
              <td class="no" align="center"><?php echo $intno + 1 ?>
                <input name="txtiddet[]" hidden="true" />
              </td>
              <td>
                <input id="descdet_ksosong" name="txdescdet[]" placeholder="Description"
                  class="form-control desc_det" />
              </td>
              <td align="center">
                <input name="txtdatedet[]" placeholder="Date" size="24" class="form-control datepicker"
                  data-date-format="dd-mm-yyyy" />
              </td>
              <td align="center">
                <input name="txtpaydet[]" placeholder="0.00" size="35" class="form-control price" maxlength="70"
                  onkeypress="return isNumberKey(event)">
              </td>
              <td>
                <div class="btn-success">
                  <input type="file" name="userfile" value="Upload" class="btn-success" />
                </div>
              </td>
              <td align="center">
                <button class="remove"><img src="<?php echo base_url('asset/images/delete.png'); ?>" /></button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- Table Responsive -->

      <br>

      <table width="95%" class="table-striped  table-hover table-condensed table-bordered">
        <?php foreach ($str_trans_master as $row) : ?>
        <tr style="background:#EFEFEF; font-size:13px;">
          <td style="font-weight:bold;color:#C00114;">Payment Type :
            <input hidden="true" type="text" id="txtbuffid_det" name="txtbuffid_det" class="buffiddetdel" />
          </td>
          <td style="font-weight:bold;color:#C00114;"><?php  echo $row->desc_pay ; ?> </td>
        </tr>
        <tr style="background:#EFEFEF; font-size:13px;">
          <td style="font-weight:bold" width="25%">Price Unit Vehicle:</td>
          <td style="font-weight:bold;" width="80%" align="left">
            <div class="col-xs-10">
              <?php echo $row->curr_sell.":"; ?>
              <input name="txtunitprice" align="right" value="<?php  echo number_format($row->price_sell,2,'.',',') ?>"
                class="unit_price" readonly />
            </div>
          </td>

        </tr>

        <tr style="background:#EFEFEF; font-size:13px;">
          <td style="font-weight:bold" width="20%">Discount :</td>
          <td style="font-weight:bold;" width="80%" align="left">
            <div class="col-xs-10">
              <?php echo $row->curr_sell.":" ;?>
              <input name="txtdiscount" align="right" value="<?php echo number_format($row->discount,2,'.',',') ?>"
                class="discount" readonly />
            </div>
          </td>
        </tr>

        <tr style="background:#EFEFEF; font-size:13px;">
          <td style="font-weight:bold" width="20%">Total Payment</td>
          <td style="font-weight:bold;" width="80%" align="left">
            <div class="col-xs-10">
              <?php echo $row->curr_sell.":" ; ?>
              <input name="txttotalpayment" value="<?php echo number_format($row->total_paymet_customer,2,'.',',') ?>"
                class="amount" readonly />
            </div>
          </td>
        </tr>

        <tr style="background:#EFEFEF; font-size:13px;">
          <td style="font-weight:bold" width="20%">Total Payment / DP + Discount:</td>
          <td style="font-weight:bold;" width="80%" align="left">
            <div class="col-xs-10">
              <?php echo $row->curr_sell.":" ; ?>
              <input name="txtdpplusdiscount" value="<?php echo number_format($row->payment_plus_discount,2,'.',',') ?>"
                class="amount_plus_dp" readonly />
            </div>
          </td>
        </tr>

        <tr style="background:#FAFECB; font-size:13px;">
          <td style="font-weight:bold;color:#C00114" width="20%">Payment Customer Remaining :</td>
          <td width="80%" style="font-weight:bold;color:#C00114;" align="left">
            <div class="col-xs-10">
              <?php echo $row->curr_sell.":" ?> <input name="txtpcr"
                value="<?php echo number_format($row->remaining_amount_customer,2,'.',',') ?>" readonly
                class="r_payment">
              <?php if ($row->remaining_amount_customer =="0.00") :?>
              <span id="imglunas"><img src="<?php 
			 echo base_url("asset/images/success_ico.png") ?>">
                <label>Ready To Send Approval D.O / BSTK</label>
              </span>
              <?php else :  ?>
              <span id="imglunas" hidden="true"><img src="<?php 
			 echo base_url("asset/images/success_ico.png") ?>"></span>
              <?php endif; ?>
            </div>

          </td>
        </tr>
        <?php endforeach  ?>

       

        <tr>
          <td colspan="3">
            <input type="submit" name="btnsavedetail" id="btnsavedetail" value="Submit " class="btn btn-primary" />
            <a href="<?php echo base_url('mazda_stock_sales/c_slssell_unit/c_slsssell');?>"
              style="text-decoration:none;"><input id="btnback" name="btnback" type="button" value="Back To Table"
                class="btn btn-warning" /> </a>
        </tr>
      </table>
      
      <?php form_close(); ?>
    </div>
    <!--panel  -->
  </div>
  <!--panel  -->
</div>
<!--col-md-12  -->


<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- jQuery_ui -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>"></script>

<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js"
  charset="UTF-8"></script>

<script type="text/javascript"
  src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js" charset="UTF-8">
</script>
<!-- Fungsi datepickier yang digunakan -->



<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
  $('.datepicker').datetimepicker({
    language: 'id',
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
  });
</script>


<script type="text/javascript">
  //--------------function number only

  function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }
</script>

<script>
  $(function () {
    $('body').delegate('.r_payment', 'change', function () {
      //var tr =  $(this).parent().parent();	  		 
      //tr.find('.price').;  		
      if ($('.r_payment').val("0.00")) {
        alert('lunas')
      };
    });
  });
</script>

<script script type="text/javascript">
  //calculate and add field--------------
  $(function () {



    $('body').delegate('.remove', 'click', function () {
      $(this).parent().parent().remove();
      var tr = $(this).parent().parent();
      var price = numeral().unformat(tr.find('.price').val());
      var discount = numeral().unformat($('.discount').val());
      var unit_price = numeral().unformat($('.unit_price').val());
      var total_payment_cust = numeral().unformat($('.amount').val());
      var amount_plus_dp = numeral().unformat($('.amount_plus_dp').val());
      var r_payment = numeral().unformat($('.r_payment').val());
      var get_iddet = tr.find('.catchtxtiddet').val();

      hasilamount = total_payment_cust - price;
      pengurangandpdisc = amount_plus_dp - price;
      r_payment = unit_price - pengurangandpdisc;


      $('.amount').val(numeral(hasilamount).format('0,0.00'));
      $('.amount_plus_dp').val(numeral(pengurangandpdisc).format('0,0.00'));
      $('.r_payment').val(numeral(r_payment).format('0,0.00'));

      var buff_iddet = $('.buffiddetdel').val()
      if (buff_iddet == "") {
        $('.buffiddetdel').val(get_iddet);
      } else {
        $('.buffiddetdel').val($('.buffiddetdel').val() + "," + get_iddet);
      }

      if (r_payment != 0) {
        $('#imglunas').hide("slow"); //hiden image lunas aka checklist
      }

    });

    $('body').delegate('.price', 'change', function () {
      var tr = $(this).parent().parent();
      var price = numeral().unformat(tr.find('.price').val());
      var discount = numeral().unformat($('.discount').val());
      var unit_price = numeral().unformat($('.unit_price').val());
      var total_payment_cust = numeral().unformat($('.amount').val());


      var t = 0;
      var jmlbaris = parseInt($('.remove').length);

      for (var i = 0; i < jmlbaris; ++i) {
        t += parseFloat(numeral().unformat($('.price').eq(i).val()));
      }

      var akmulasi_pay = t;
      var hasil_payment_total = discount + akmulasi_pay;
      var hasil_r_payment = unit_price - hasil_payment_total;

      $('.amount').val(akmulasi_pay);
      $('.amount_plus_dp').val(hasil_payment_total)
      $('.r_payment').val(hasil_r_payment);
    });

    $('body').delegate('.price', 'blur', function () {
      var tr = $(this).parent().parent();
      var price = tr.find('.price').val();
      var r_payment = $('.r_payment').val();
      var amount = $('.amount').val();
      var amount_plus_dp = $('.amount_plus_dp').val();

      tr.find('.price').val(numeral(price).format('0,0.00'));
      $('.r_payment').val(numeral(r_payment).format('0,0.00'));
      $('.amount').val(numeral(amount).format('0,0.00'));
      $('.amount_plus_dp').val(numeral(amount_plus_dp).format('0,0.00'));
    });


  });
</script>

<script type="text/javascript">
  $(function () {
    //jquery untuk post submit save / edit
    $('#myform').on('submit', function (
    e) { //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
      e.preventDefault();
      var url = '<?php  echo base_url('
      mazda_stock_sales / c_slssell_unit / c_crud_sell / do_insert_edit_detail_sell ');  ?>'; // url			
      $.ajax({
        url: url, //this is the submit URL
        type: 'POST', //or POST
        data: $('#myform')
      .serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
        success: function (data) {}
      });
    });
  });
</script>

<script>
  function openWin(img) {
    window.open(img);
  }
</script>