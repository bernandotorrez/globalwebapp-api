<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">


 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>
      

  
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);;color:#B66F03; font-size:16px;" ><?php echo "Create New Type unit"." | ".$header  ; ?> </div>
   <div class="panel-body">               
   
          
 <?php  echo form_open('mazda_stock_sales/c_type_vehicle/c_type_vehicle/multiple_submit','id = form_my');  ?>      
 
 <!-- Menu Button Start -->
 <div class="row text-right">
   
     <button id="btnadd" name="btnadd" class="btn btn-app btn-primary btn-xs radius-4 addnew" value="Add New"
       type="button">
       <i class="ace-icon fa fa-book bigger-160"></i>
       Add New
     </button>

     <button id="btnedit" name="btnedit" class="btn btn-app btn-success btn-xs radius-4 edit_vendor" value="Edit"
       type="button" disabled>
       <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
       Edit
     </button>

     <button id="btndel" name="btndel" class="btn btn-app btn-warning btn-xs radius-4 btn_delete" value="Delete"
       type="submit" onclick="return confirm('are you sure want to delete this data!')" disabled>
       <i class="ace-icon fa fa-ban bigger-160"></i>
       Delete
     </button>
   
    <!-- Search Menu Start -->
    <table align="left">

      <td >
          <label>Selected: </label>
      </td>
      <td>&nbsp;</td>
      <td>
      <select name="cbostatus" id="cbostatus" class="form-control">
         <option value="desc_type">Type</option>
         <option value="desc_model">model</option>
         <option value="price_cost">Cost Price</option>
         <option value="price_sell">Selling Price</option>
         <option value="fuel">Fuel</option>
         <option value="tranmission">Tranmission</option>
       </select>  
      </td>
      <td>&nbsp;</td>

      <td>
          <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari"
              value="<?php echo $this->session->flashdata('cari'); ?>">
      </td>

      <td>

          <button id="btncari" name="btncari" class="btn btn-app btn-primary btn-xs radius-4" value="Search"
              type="submit">
              <i class="ace-icon fa fa-search bigger-160"></i>
              Search
          </button>

          <!-- <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF" />               -->

      </td>
      </tr>
      </table>
      <!-- Search Menu Start -->

 </div>
 <!-- Menu Button End -->
      <div class="space-12"></div>

    <!-- Table Responsive -->
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>ID</th>
            <th>Model</th>
            <th>Type</th>
            <th>IDR</th>
            <th>Cost Price</th>
            <th>Selling Price</th>
            <th>Fuel</th>
            <th>Tranmission</th>
            <th>Date End Waranty</th>
            <th>Selling Price</th>
            <th hidden="true">Milieage End Waranty</th>
            <th><a href="javascript:void(0)" onclick="CheckAll()" style="text-decoration:none"> All</a>
              \
              <a href="javascript:void(0)" onclick="UncheckAll()" style="text-decoration:none">Uncheck</a>

            </th>
          </tr>
        </thead>
        <tbody>
          <?php if ($ceck_row >= 1) { 	  
              foreach($type_view as $row){ $intno++;				   			
          ?>
          <?php if ($intno % 2 == 0) : ?>
          <tr style="background:#F0F0F0;font-size:12px;color:#B90020">
            <?php else:	?>
          <tr style="font-size: 12px">
            <?php endif?>
            <td align="center"><?php echo $row->id_type  ; ?></td>
            <td><?php echo $row->desc_model ; ?></td>
            <td><?php echo $row->desc_type ; ?></td>
            <td><?php echo $row->currency ;?></td>
            <td><?php echo number_format($row->price_cost,2,'.',',') ;?></td>
            <td><?php echo number_format($row->price_sell,2,'.',',') ;?></td>
            <td align="center"><?php echo $row->fuel ;?></td>
            <td align="center"><?php echo $row->tranmission ;?></td>
            <td align="center"><?php 
						if  ($row->man_warnty == null or $row->man_warnty == "" ) :
							 echo "-";
						else:
					  		echo date('d-m-Y',strtotime($row->man_warnty)) ;
					    endif;		
					 ?>
            </td>
            <td align="center"><?php echo $row->man_mileage ;?></td>
            <td>
              <div align="center"><input id="checkchild" name="msg[]" type="checkbox"
                  value="<?php echo $row->id_type ; ?>" />
                <?php form_close(); ?>
              </div>
            </td>
          </tr>
          <?php  } }?>
        </tbody>
      </table>
    </div>
    <!-- Table Responsive -->
		 
		<?php		   			
		     if (! empty($pagination))			
			 { 
		  	   echo '<center><table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table></center>';				 
			 }			  	  						
		?>
        
 </div>  
</div> 
  
  
  
          
  </div>
  </div>
 
</div>   	
		 

<style>
/*label berwarna merah ketika validasi fom modal popup. */
	label
	{
		width:50%;
		display: block;
		margin-right:2%;
		text-align:left;
		line-height:22px;
	}	
    span.error
	{
		font: 12px ;
		color:red;
		margin-left:8px;
		line-height:22px;
	}	
/*------------------------- */
</style>
	
 <!-- Modal Buat save and edit -->
     <div id="tampilanform"></div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    
                    <div class="modal-body">
                          <table width="90%" class="table-condensed" align="center">
						
                         <tr> 
                        <td id="row_label"> <label for="Id Vendor"  class="col-xs-8" style="color:#C00">Id type</label></td>                              
                        
                          <td id="row_input">
                          <div class="control-group">
                               <div class="col-xs-12">
                               <input name="txtid"  type="text" class="form-control" id="txtid"  readonly="readonly" >    
                               </div> 
                           </div>
                          </td>                                  
                        </tr>    
                        <tr> 
                        <td> <label for="Vendor" style="color:#C00"  class="col-xs-9">Model *</label></td>                              
                        
                          <td>
                          <div class="control-group">
                               <div class="col-xs-12">   
                               <select name="cbomodel" id="cbomodel" class="form-control">
                               		<option>- Choose -</option>
                                    <?php
									 foreach($tampil_model as $row_model):
										echo '<option value="'.$row_model->id_model.'">'.$row_model->desc_model.'</option>' ;
									 endforeach;
									?>
                               </select>
                               </div> 
                           </div>    
                          </td>                                  
                        </tr>                             
                         <tr> 
                        <td> <label for="Phone" style="color:#C00"  class="col-xs-8">Type *</label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-12">
                            <input name="txttype" type="text" class="form-control" id="txttype" maxlength="50"   >
                           </div> 
                           </div>
                          </td>                                  
                        </tr>
                        
                             <tr> 
                        <td> <label for="fax"  class="col-xs-8" style="color:#C00">Currency *</label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-12">
                            <select name="cbocurr" id="cbocurr" class="form-control">
                               		<option>- Choose -</option>
                                    <?php
									 foreach($tampil_curr as $row_curr):
										echo '<option value="'.$row_curr->id_curr.'">'.$row_curr->currency.'</option>' ;
									 endforeach;
									?>
                               </select>
                           </div> 
                           </div>
                          </td>                                  
                        </tr>
                         <tr> 
                        <td ><label for="Email"  class="col-xs-9" style="color:#C00">Cost Price *</label></td> 
                        <td>
                        <div class="control-group">
                          <div class="col-xs-12">
                            <input  name="txtcost" type="text" class="form-control cost_price" id="txtcost" maxlength="30" onkeypress="return isNumberKey(event)"  >
                          </div>  
                         </div> 
                        </td>    
                        </tr>
                        <tr> 
                        <td ><label for="Email"  class="col-xs-9" style="color:#C00">Sell Price *</label></td> 
                        <td>
                        <div class="control-group">
                          <div class="col-xs-12">
                            <input  name="txtprice" type="text" class="form-control price" id="txtprice" maxlength="30" onkeypress="return isNumberKey(event)"  >
                          </div>  
                         </div> 
                        </td>    
                        </tr>
                         <tr> 
                        <td> <label for="Phone"   class="col-xs-8">Fuel</label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-12">
                            <input name="txtfuel" type="text" class="form-control" id="txtfuel" maxlength="40" >
                           </div> 
                           </div>
                          </td>                                  
                        </tr>
                         <tr> 
                        <td> <label for="Phone" class="col-xs-8">Tranmission</label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-12">
                           <select name="cbotransmision" id="cbotransmision" class="form-control">
                            <option>Automatic</option>
                            <option>Manual</option>
                           </select>
                           </div> 
                           </div>
                          </td>                                  
                        </tr>
                         <tr> 
                        <td> <label for="Phone" class="col-xs-8">Date End Waranty</label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-12">
                            <input type="text" id="txtdatewaranty" name="txtdatewaranty"  data-date-format="dd-mm-yyyy" class="form-control datepicker" readonly>
                           </div> 
                           </div>
                          </td>                                  
                        </tr>
                         <tr> 
                        <td> <label for="Phone" class="col-xs-8">Miliage End Waranty</label></td> 
                        
                          <td>
                          <div class="control-group">
                           <div class="col-xs-12">
                            <input name="txtmill" type="text" class="form-control" id="txtmill" maxlength="20" onkeypress="return isNumberKey(event)" >
                           </div> 
                           </div>
                          </td>                                  
                        </tr>
                                                                 
                        </table>                 
                    </div> 
                    
                     <div class="modal-footer">
                        <input type="submit" name="btnsave" id="btnsave" value="Submit " class="btn btn-primary"/>
                        <input type="submit" name="btnupdate" id="btnupdate" value="Submit Update" class="btn btn-primary"/>                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal Buat edit -->  	



<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- jQuery_ui -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->

<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 

  
<script>
// fungsi untik checklist semua otomatis
function CheckAll()
{
    l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = true;		
		}	
		document.getElementById('btnedit').disabled = true;	
		document.getElementById('btndel').disabled = false;	
		document.getElementById('btnsendtab').disabled = true;	
}

function UncheckAll()
{
   l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = false;			 
		}
		document.getElementById('btnedit').disabled = true;
		document.getElementById('btndel').disabled = true;
		document.getElementById('btnsendtab').disabled = true;
}


//--------------------function disbaled enabled button with check book

$(function() {  //.btn hnya untuk tombol edit yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn-success').removeAttr('disabled');			
        }
        else{
            $('.btn-success').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btn-warning').removeAttr('disabled');			
        }
        else{
            $('.btn-warning').attr('disabled', 'disabled');								
        }
    });
});


  
//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});		  


//--------------function number only
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 45 && charCode != 41 && charCode != 40 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	
</script>

<script>
// Show modal pop up for addnew----------
$(function(){ 
	  $(document).on('click','.addnew',function(e){		       		  
			$("#myModal").modal('show');						  
			$("#myModalLabel").html("Create New Type unit");	
			$("#cbomodel").val("- Choose -"); 
			$("#cbocurr").val("- Choose -"); 						
			$("#cbotransmision").val("Automatic");
			$("#txttype").val("");
			$("#txtprice").val("");				  				
			$("#txtfuel").val("");
			$("#txtdatewaranty").val("");
			$("#txtmill").val("")	;
		    $("#row_label").hide();	
			$("#row_input").hide();		
			$("#btnupdate").hide();	
		    $("#btnsave").show();		       												
	  });																						   	 	   
});		
</script>

<script>
 // Show modal pop up for edit and then get data table edit vendor to modal popup
$(function(){
  $(document).on('click','.edit_vendor',function(e){
	
		var req_id = $('input[type="checkbox"]:checked').val()
		var url = '<?php echo base_url("mazda_stock_sales/c_type_vehicle/c_type_vehicle/get_idtype_modal_update"); ?>';
					
		$("#myModal").modal('show');  	
		$("#myModalLabel").html("Edit Type unit");			
						
		$.ajax({			
			type:'POST',
			url: url,
			dataType: "json",
			data: {id_type:req_id}, //cari berdasar kan ID_vendor yang di kirim dan di simpan pada var req_id 
			success: function(data, textStatus, jqXHR ) {				    			
			    $("#row_label").show();	
			    $("#row_input").show();	
				$("#btnsave").hide();
				$("#btnupdate").show();				
				$('[name="txtid"]').val(req_id);
				
				$('[name="cbomodel"]').val(data.id_model); 								
				$('[name="cbocurr"]').val(data.id_curr); 
				$('[name="cbotransmision"]').val(data.tranmission);
				
				$('[name="txttype"]').val(data.desc_type);
				
				$('[name="txtcost"]').val(data.price_cost);
				$('[name="txtprice"]').val(data.price_sell);			
				$('[name="txtfuel"]').val(data.fuel);
				$('[name="txtalamat"]').val(data.alamat);	
			    $('[name="txtdatewaranty"]').val(data.man_warnty);
			    $('[name="txtmill"]').val(data.man_mileage);			
			}
						
		});				
		
   });	
});	
</script>

<script>
$(function(){
	//jquery untuk post submit save / edit
    $('#myform').on('submit', function(e){ //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
        e.preventDefault();		
		var url = '<?php  echo base_url('mazda_stock_sales/c_vendor/c_vendor/multiple_submit');  ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
            success: function(data){              
            }
        });
    });
});
</script>

<script>
	$('body').delegate('.cost_price','blur',function(){  	
	        var price=$('#txtcost').val(); 			  	 
		    $('#txtcost').val(numeral(price).format('0,0.00')) ;		  	
 	 });  	

	$('body').delegate('.price','blur',function(){  	
	        var price=$('#txtprice').val(); 			  	 
		    $('#txtprice').val(numeral(price).format('0,0.00')) ;		  	
 	 });  	
</script>