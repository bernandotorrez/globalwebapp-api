<?php 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=report_excel_stock_unit.xls");
	header("Pragma: no-cache");
	header("Expires: 0"); 	
?>
<?php 
		echo '<table width="100%" border="1"  >';		
		echo '<tr style=" color:#144b79;font-weight:bold;font-size: 12px; ">';		
		echo '<td width="8%"align="center">Stock Number </td>';
		echo '<td width="5%" align="center">Branch</td>';													
		echo '<td width="9%" align="center">Type</td>' ;
		echo '<td width="3%" align="center">Vin</td>' ;
		echo '<td width="7%" align="center">Engine</td>' ;	
		echo '<td width="8%" align="center">Colour</td>' ;				
		echo '<td width="3%" align="center">CSI</td>' ;
		echo '<td width="6%" align="center">Location Stock</td>';	
		echo '<td width="5%" align="center">Intake</td>' ;	
		echo '<td width="5%" align="center">Year</td>' ;		
		echo '<td width="5%" align="center">Status Stock</td>' ;	
		echo '<td width="5%" align="center">Date Received</td>' ;
		echo '<td width="5%" align="center">Date Pay MMI</td>' ;
		echo '<td width="5%" align="center">Bank</td>' ;	
		echo '<td width="5%" align="center">Status</td>' ;										
		echo '<td width="5%" align="center">Remarks Stock</td>' ;
		echo '<td width="5%" align="center">DP Date</td>' ;
		echo '<td width="5%" align="center">DP Payment</td>' ;
		echo '<td width="5%" align="center">Total Payment Customer</td>' ;										
		echo '</tr>' ;
?>			
	<?php if(!empty($data_excel)) : ?>
	  <?php foreach($data_excel as $item) {	?>
           <?php
					  if ($item['status_flag_stock']==2) : 	 
					     echo '<tr style=" background:#3C0">';	 
					  else:
					      if ($item['status_flag_stock']==-1) : 	 
						      echo '<tr style="background:#FFFF00">';
						  else:	 
						  	 if ($item['status_flag_stock']==3) : 	 
						        echo '<tr style="background:#FF9747">';
						     else:	 
							    echo '<tr style="background:#B4C9FA">';	
							 endif;	
						  endif;	 
					  endif;
				?>       
                <td><?=$item['stock_no']?></td>
                <td><?=$item['name_branch']?></td>
                <td><?=$item['desc_type']?></td>
                <td><?=$item['vin']?></td>
                <td><?=$item['engine']?></td>                                        
                <td><?=$item['colour']?></td> 
                <td><?=$item['csi']?></td>  
                <td><?=$item['location_stock']?></td>               
                <td><?=$item['intake']?></td>
                <td><?=$item['year']?></td>
                <?php
						if ($item['flag_demo_unit'] == "0" ) :
						   echo '<td style="color:blue">'.'STOCK UNIT'.'</td>';
						else:
						   echo '<td style="color:red">'.'DEMO CAR'.'</td>';
						endif;
		        ?>                
                <td><?=$item['rec_date'] ?></td>
                <td><?=$item['pay_date_mmi'] ?></td>
                <td><?=$item['name_bank']?></td>                    
				<?php
					  if ($item['status_flag_stock']==2) : 	 
					     echo '<td style="color:blue">'."DP".'</td>';	 
					  else:
					      if ($item['status_flag_stock']==-1) : 	 
						      echo '<td style="color:red">'.'WRS'.'</td>';
						  else:	 
						     if ($item['status_flag_stock']==1) :
							   	echo '<td>'."READY".'</td>';	
							 else:	
							     if ($item['status_flag_stock']==3) :
							     	echo '<td>'."CS".'</td>';	 
								 else:	
							  		echo '<td>'."BSTK".'</td>';	
								endif;	
							 endif	;
						  endif;	 
					  endif;
				?>                               
                <td><?=$item['remarks_stock']?></td>
                <td><?=$item['remarks_stock']?></td>
                <td><?=$item['remarks_stock']?></td>
                <td><?=$item['remarks_stock']?></td>        
            </tr>
		<?php } ?>
     <?php endif ?>   
</table>
<table>
	<tr>
      <?php if($str_flag_stock =="1") : ?>
		<td style=" background:#B4C9FA">Total Stock Free : <?php echo $total_stock ?></td>
    	<td style="background:#3C0">Total Booked : <?php echo $total_dp ?></td>
    	<td style="background:#FF0">Total WRS : <?php echo $total_wrs ?></td>
        <td style="background:#FF9747">Total Cross Selling : <?php echo $total_cross_sale ?></td>
      <?php else: ?>  
      	<td style=" background:#B4C9FA">Total Stock BSTK : <?php echo $total_stock_bstk ?></td>      
      <?php endif;?>  
	</tr>    
 </table>   