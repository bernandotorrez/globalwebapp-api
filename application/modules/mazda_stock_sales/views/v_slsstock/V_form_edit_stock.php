<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">


<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_fail') ;?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_insert_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_insert_fail');?>  
        </div>	
<?php } ?>



<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">                 
<div class="panel panel-default">
  <div class="panel-heading" id="label_head_panel" style="color:#B66F03; font-size:18px;">Edit  Stock Unit Vehicle PT.<?php echo $this->session->userdata('company'); ?></div>                 
     <?php  echo form_open_multipart('mazda_stock_sales/c_slsstock/c_crud_stock/do_edit_stock',array('class'=>'form-multi'));  ?> 
  
       <!-- Responsive Form for Mobile and Web Start -->
       <div class="container">
       <?php
			  if ($check_row >= 1) :  
				 foreach($tampil_edit_stock as $row_stock) :
	      ?> 
         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Stock Number</label>

               <input type="text" id="txtnostock" name="txtnostock" class="form-control" 
                    readonly value="<?php echo $row_stock->stock_no ;?>"  />
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Branch</label>

               <input type="text" id="txtbranch" name="txtbranch" 
                     class="form-control"  value="<?php echo $this->session->userdata('name_branch'); ?>"  
                     readonly />
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Location Stock</label>

               <select id="cboloc" name="cboloc"  class="form-control">
					 <option><?php echo $row_stock->location_stock ; ?></option>	                    
					   <?php foreach($tampil_lokasi as $row){ ?>                         
                         <option ><?php echo $row->name_loc ;?></option>
                       <?php } ?>  
               </select>
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Stock keeper</label>

               <input type="text" id="txtuser" name="txtuser" 
                     class="form-control" value="<?php echo $this->session->userdata('name');  ?>"  
                     readonly />
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Type *</label>

               <select id="cbotype" name="cbotype" class="form-control">             											    	           <option value="<?php  echo $row_stock->id_type ?>"><?php echo $row_stock->desc_type ?></option>	 
                    <?php
						foreach ($tampil_type_vehicle as $row_type) :						
							echo '<option value="'.$row_type->id_type.'">'.$row_type->desc_type.'</option>';
						endforeach;
					?>
                 </select>
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Colour *</label>

               <select id="cbowarna" name="cbowarna" class="form-control">         
                   <option value="<?php echo $row_stock->id_colour ; ?>"><?php echo $row_stock->colour ; ?></option>	
                     <?php
						foreach($warna_sesuai_type as $row_warna_type) :						
						      echo '<option value="'.$row_warna_type->id_colour.'">'.$row_warna_type->colour.'</option>';
						endforeach;
					 ?>                                        
                </select>
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Price Unit *</label>

               <div class="input-group">
                 <span class="input-group-addon">
                   <label id="txt_curr"> <?php echo $row_stock->currency ; ?> </label>
                 </span>

                 <input id="price_unit" name="price_unit" class="form-control" 
                 readonly style="color:#A6090D" value="<?php echo number_format($row_stock->price_sell,2) ; ?>"/>
               </div>
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Upload VIN/Engine Unit</label>

               <input type="file" id="txtupload" class="form-control btn btn-success" name="userfile" 
               value="<?php echo $row_stock->attach_foto ?>"/>

               <?php      				
                  If ($row_stock->attach_foto != "")
                  {  
                   echo  anchor_popup($row_stock->attach_foto,'PDF Unit Already Exsit');
                  }else{
                    echo '<label style="background-color:#f4f497;color:#900;">'.'Empty Foto Attachment!!!'.'</label>' ;	  
                  }
                ?>    
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Curent VIN *</label>

               <input name="txtvin" type="text" class="form-control" id="txtvin"  
                value="<?php echo $row_stock->vin ; ?>"   maxlength="40" readonly  /> 
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Curent Engine *</label>

               <input type="text" id="txtengine" name="txtengine" 
                 class="form-control"  value="<?php echo $row_stock->engine ; ?>"  
                  readonly="readonly"   />
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">VIN Update *</label>

               <input name="txtvinupdate" type="text" class="form-control" 
                         id="txtvinupdate"  value="<?php echo $row_stock->vin ; ?>"  
                         placeholder="Vin" style="color:#900"  maxlength="40"  />  
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Engine Update *</label>

               <input type="text" id="txtengineupdate" name="txtengineupdate" 
                 class="form-control"  value="<?php echo $row_stock->engine ; ?>" 
                 style="color:#900"  placeholder="Engine"   />
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Intake *</label>

               <select id="cbointake" name="cbointake" class="form-control">
                   
					<?php if($row_stock->intake == ""): ?>
					   <option value="" >--Choose--</option>					
                    <?php else: ?>  
					   <option ><?php  echo $row_stock->intake ; ?></option>
                    <?php  endif;?>
                    
                        <option>JANUARI</option>
                        <option>FEBRUARY</option>
                        <option>MARET</option>
                        <option>APRIL</option>
                        <option>MEI</option>
                        <option>JUNI</option>
                        <option>JULY</option>
                        <option>AGUSTUS</option>
                        <option>SEPTEMBER</option>					
                        <option>OKTOBER</option>					
                        <option>NOVEMBER</option>					
                        <option>DESEMBER</option>					
                </select>
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">CSI</label>

               <input type="text" id="txtcsi" name="txtcsi" class="form-control"  
                 value="<?php echo $row_stock->csi ; ?>" placeholder="Csi" />
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Receive Date Unit</label>

               <input type="text" id="txtdatericived" name="txtdatericived"  
                 data-date-format="dd-mm-yyyy" class="form-control datepicker" 
                 readonly  value="<?php echo date('d-m-Y',strtotime($row_stock->rec_date)) ; ?>">
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Pay Date To MMI *</label>

               <input type="text" id="txtdatetommi" name="txtdatetommi"  
               data-date-format="dd-mm-yyyy" class="form-control datepicker" 
               readonly value="<?php 
                if ($row_stock->pay_date_mmi!="") :				 
                  echo date('d-m-Y',strtotime($row_stock->pay_date_mmi)) ; 
                else :
                  echo "";
                endif   
                  ?>">
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Bank Form A</label>

               <select id="cbobank" name="cbobank" class="form-control">
                      <option value="<?php echo $row_stock->id_bank ; ?>"><?php echo $row_stock->name_bank ; ?></option>
                    <?php
                      foreach ($tampil_bank as $row_bank) :						
                          echo '<option value="'.$row_bank->id_bank.'">'.$row_bank->name_bank.'</option>';
                      endforeach;
                    ?>                     
                      </select>
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Year</label>

               <input name="txtyear" type="text" class="form-control" 
                id="txtyear"  placeholder="Year Production" 
                onkeypress="return isNumberKey(event)" 
                value="<?php echo $row_stock->year ; ?>"  maxlength="4"  />
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Stock Status</label>

               <select  name="cbo_demo_unit" class="form-control">
					    
					<?php if($row_stock->flag_demo_unit == "0"): ?>
					   <option value="0" >STOCK UNIT</option>					
                    <?php else: ?>  
					   <option value="1" >DEMO CAR</option>
                    <?php  endif;?>
                 	   <option value="1">DEMO CAR</option>
                    </select>
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Remarks</label>

               <textarea id="txtremarks" class="form-control" 
                  name="txtremarks" ><?php echo $row_stock->remarks_stock ;?></textarea>
             </div>
           </div>

         </div>
         <!-- Row End -->
         <?php endforeach ; ?> 				 			 
      <?php endif ;?>
       </div>
       <!-- Responsive Form for Mobile and Web End -->
     
     <!-- <div class="panel-body">                         
         <table>
           <tr>
              <td>                     
                 <input id="btnsaveedit" name="btnsavebtnsaveedit" type="submit" value="save"  class="btn btn-danger " />                
              </td>
               <td>&nbsp;</td>                   
               <td>&nbsp;  </td>
            <td>                                      			
            <a href="<?php echo base_url('mazda_stock_sales/c_slsstock/c_slsstock');?>" style="text-decoration:none;"><input id="btnback" name="btnback" type="button" value="Back To Table" class="btn btn-warning" /> </a>	                  
            </td>
          </tr>
         
        </table>    
     </div> -->

     <div class="row">
            <div class="form-group text-center">
                <button id="btnsaveedit" name="btnsavebtnsaveedit" value="Save" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
                    <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                    Save
                </button>


                <a href="<?=base_url('mazda_stock_sales/c_slsstock/c_slsstock');?>" class="btn btn-app btn-danger btn-xs radius-4">
                    <i class="ace-icon fa fa-close bigger-160"></i>
                    Cancel
                </a>

            </div>
        </div>
                                            	                     					           			 
 		
		<?php form_close(); ?> 
      
   </div> <!-- /panel -->             
</div>  <!-- /div class="col-lg-12 -->   
 
 <?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>    
 
<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 


<script type="text/javascript">
//--------------function number only
 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	

//----------------set focus----
 $(function() {
  $("#txtpayfrom").focus();    
});	

</script>

<script type="text/javascript">
//automatic multiple combo.

var htmlobjek;
$(document).ready(function(){  
	
	  $("#cbotype").change(function(){
		var cbottype_val = $("#cbotype").val();
		var url = '<?php echo site_url("mazda_stock_sales/c_slsstock/c_crud_stock/select_warna_combo_jquery"); ?>';
		
		$.ajax({
			type:'POST',
			url: url,		
			data:{id_type_post:cbottype_val},	
			cache: false,          
			success: function(msg){            
				$("#cbowarna").html(msg);							
			}
		});
	  });	  	   
	    
});

</script>

<script type="text/javascript">
//automatic show price 

var htmlobjek;
$(document).ready(function(){  
	 $("#cbotype").change(function(){
			var cbottype_price = $("#cbotype").val();
			var url = '<?php echo site_url("mazda_stock_sales/c_slsstock/c_crud_stock/doget_price_unit_jquery"); ?>';
			
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{id_type_post:cbottype_price},	
				cache: false,          
				success: function(data, textStatus, jqXHR){            
					$('[name="curr"]').val(data.currency);					
					$('[name="price_unit"]').val(numeral(data.price_sell).format('0,0.00'));			
				}
			});
		  });
});	  
</script>