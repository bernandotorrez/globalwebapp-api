<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">



<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_fail') ;?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_insert_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_insert_fail');?>  
        </div>	
<?php } ?>



<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">                 
<div class="panel panel-default">
  <div class="panel-heading" id="label_head_panel" style="color:#B66F03; font-size:18px;">Add In Stock Unit Vehicle PT.<?php echo $this->session->userdata('company'); ?></div>                 
     <?php  echo form_open_multipart('mazda_stock_sales/c_slsstock/c_crud_stock/multiple_submit',array('class'=>'form-multi'));  ?> 
 
       <table class="table " width="100%">                               
                        
           <tr> 
             <td>  
                 <div class="form-group">
                   <label for="Stock Number"  class="col-xs-3">Stock Number</label>
                    <div class="col-xs-6">
                    
                    <input type="text" id="txtnostock" name="txtnostock" class="form-control" readonly value="<?php echo $stocknumber ;?>"  />
                   
                    </div>
                </div>
             </td> 
               <td>  
                 <div class="form-group">
                   <label for="Branch"  class="col-xs-3"> Branch </label>
                   <div class="col-xs-5">
                     <input type="text" id="txtbranch" name="txtbranch" class="form-control" value="<?php echo $this->session->userdata('name_branch'); ?>"  readonly />
                   </div>
                </div>
               </td> 
                   		                                                        
          </tr>
           <tr>
            <td>   
              <div class="form-group">
                <label for="Location Stock"  class="col-xs-3">Location Stock</label>
                <div class="col-xs-7">               
                 <select id="cboloc" name="cboloc"  class="form-control">
                   <!--  <option> <?php //echo $this->session->userdata('name_branch') ;?></option> -->
	               <?php foreach($tampil_lokasi as $row){ ?>                         
       		         <option ><?php echo $row->name_loc ;?></option>
                   <?php } ?>  
               </select>
                </div>
              </div>                
             </td>  
              <td>   
                  <div class="form-group">
                   <label for="Stock keeper"  class="col-xs-3">Stock keeper</label>
                     <div class="col-xs-6">
                     <input type="text" id="txtuser" name="txtuser" class="form-control" value="<?php echo $this->session->userdata('name');  ?>"  readonly />
                    </div>
                </div>     
             </td>                         
       </tr>             
             <tr>   
               <td>
				    <div class="form-group">
            	<label for="Type"  class="col-xs-3" style="color:#900">Type *</label>
            <div class="col-xs-9">                 
                 <select id="cbotype" name="cbotype" class="form-control">                 	<option value="">--Choose--</option>	 
                    <?php
						foreach ($tampil_type_vehicle as $row_type) :						
							echo '<option value="'.$row_type->id_type.'">'.$row_type->desc_type.'</option>';
						endforeach;
					?>
                 </select>
            </div>
          </div>
            </td>
             <td>
                
       	  <div class="form-group">
            		<label for="Colour"  class="col-xs-3" style="color:#900">Colour *</label>
            <div  class="col-xs-6">                  
                <select id="cbowarna" name="cbowarna" class="form-control">                 	
                <option value="">--Choose--</option>	
                                  
                 </select>
            </div>
		</div>
             </td>                        
        </tr>             
         <tr>   
           <td>
            <div class="form-group">
             <label for="Price Unit" style="color:#900" class="col-xs-3">Price Unit *</label>
             <table class="col-xs-5">
               <tr>  
                  <td class="col-xs-4" >             
                     <input  maxlength="20" id="curr" name="curr" class="form-control" readonly  />
                 </td>
                 <td>  
 <input  maxlength="70" id="price_unit" name="price_unit" class="form-control" readonly style="color:#A6090D"  />
              	  </td>            
           		</tr>   
             </table>  
            </div>          
          </td>
          
          <td>
            <div class="form-group">
                <label for="attach"  class="col-xs-3">Upload VIN/Engine Unit</label>
                
               <div class="col-xs-5">      
                 <div class="btn btn-success">
                   <input type="file" id="txtupload" name="userfile"   />      
          		</div>		
              </div>
           </div>      
          </td>                                                
        </tr> 
          
           <tr>   
         	<td>
             <div class="form-group">
                        <label for="Term Of VIN" style="color:#900"  class="col-xs-3">VIN *</label>
                        <div class="col-xs-6">
                         <input name="txtvin" type="text" class="form-control" id="txtvin"  placeholder="Vin"  maxlength="40"  />                                                
                   </div>
            </div>                       
           </td>           
           <td>            
              <div class="form-group">
                <label for="Engine" style="color:#900" class="col-xs-3">Engine *</label>
                <div class="col-xs-6">
                 <input type="text" id="txtengine" name="txtengine" class="form-control"  placeholder="Engine"   />
                </div>
            </div>                 
          </td>                                                 
        </tr>                    
    
	 <tr>   
           <td>    
            <div class="form-group">
                <label for="Intake" style="color:#900" class="col-xs-3">Intake *</label>
                <div class="col-xs-7">
                     <select id="cbointake"  name="cbointake" class="form-control">
                        <option value="">--Choose--</option>
                        <option>JANUARI- <?php echo date("Y") ?></option>
                        <option>FEBRUARY- <?php echo date("Y") ?></option>
                        <option>MARET - <?php echo date("Y") ?></option>
                        <option>APRIL - <?php echo date("Y") ?></option>
                        <option>MEI - <?php echo date("Y") ?></option>
                        <option>JUNI - <?php echo date("Y") ?></option>
                        <option>JULY - <?php echo date("Y") ?></option>
                        <option>AGUSTUS - <?php echo date("Y") ?></option>
                        <option>SEPTEMBER - <?php echo date("Y") ?></option>					
                        <option>OKTOBER - <?php echo date("Y") ?></option>					
                        <option>NOVEMBER - <?php echo date("Y") ?></option>					
                        <option>DESEMBER - <?php echo date("Y") ?></option>					
                    </select>
                </div>
            </div>
           </td>
           <td>
        		 <div class="form-group">
                <label for="CSI"  class="col-xs-3">CSI</label>
                <div class="col-xs-6">
                 <input type="text" id="txtcsi" name="txtcsi" class="form-control"  placeholder="Csi" />
                </div>
            </div>    
         </td>       
    </tr>        
    <tr>
    	<td>
        <div class="form-group">
         			 <label for="Receive Date Unit" style="color:#900"  class="col-xs-3">Receive  Date Unit</label>
         			<div class="col-xs-4">
          			 <input type="text" id="txtdatericived" name="txtdatericived"  data-date-format="dd-mm-yyyy" class="form-control datepicker" readonly>
                    </div>
             </div>             
                   
        </td>
        <td>
         <div class="form-group">
         			 <label for="Pay Date To MMI" style="color:#900"  class="col-xs-3">Pay Date To MMI *</label>
         			<div class="col-xs-4">
          		 <input type="text" id="txtdatetommi" name="txtdatetommi"  data-date-format="dd-mm-yyyy" class="form-control datepicker" readonly>
               </div>
            </div>  
                                	
        </td>
    </tr>    
    <tr>
    	<td>
        	 <div class="form-group">
         			 <label for="Bank Form A"  class="col-xs-3">Bank Form A</label>
         			<div class="col-xs-5">
          			  <select id="cbobank" name="cbobank" class="form-control">                    
                    <?php
						foreach ($tampil_bank as $row_bank) :						
				  			 echo '<option value="'.$row_bank->id_bank.'">'.$row_bank->name_bank.'</option>';
						endforeach;
					?>                     
                      </select>
                    </div>
          </div>    
        </td>
        <td>         		         	        	
              
               <div class="form-group">
                 <label for="txtyear"  class="col-xs-3">Year</label>
                <div class="col-xs-3">
                <input name="txtyear" type="text" class="form-control" id="txtyear"  placeholder="Year" onkeypress="return isNumberKey(event)"  maxlength="4"  />
                </div>
             </div>  
        </td>       
    </tr>  
    <tr>
    	<td >
          <div class="form-group">
                 <label for="Stock Status" class="col-xs-3" style="color:#900" >Stock Status</label>
                  <div class="col-xs-5">
       			  <select  name="cbo_demo_unit" class = "form-control" > 
                  	<option value="0">STOCK UNIT</option>
                    <option value="1">DEMO CAR</option>
                  </select>
                  </div>
         </div>        
        </td>
        <td>
            	  <div class="form-group">
                 <label for="txtyear" class="col-xs-3" >Remarks </label>
                  <div class="col-xs-7">
       			     <textarea id="txtremarks" class="form-control" name="txtremarks" > </textarea>
                </div>
         </div>        
          
        </td>
     </tr> 
        
   </table>  
     
     <div class="panel-body">                         
         <table>
           <tr>
              <td>                     
                 <input id="btnsave" name="btnsave" type="submit" value="Save"  class="btn btn-danger " />                
              </td>
               <td>&nbsp;</td>                   
               <td>&nbsp;  </td>
            <td>                                      			
            <a href="<?php echo base_url('mazda_stock_sales/c_slsstock/c_slsstock');?>" style="text-decoration:none;"><input id="btnback" name="btnback" type="button" value="Back To Table" class="btn btn-warning" /> </a>	                  
            </td>
          </tr>
         
        </table>    
     </div>
                                            	                     					           			 
 		
		<?php form_close(); ?> 
      
   </div> <!-- /panel -->             
</div>  <!-- /div class="col-lg-12 -->   
 
 <?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>    
 
<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url()?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 


<script type="text/javascript">
//--------------function number only
 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	

//----------------set focus----
 $(function() {
  $("#txtpayfrom").focus();    
});	

</script>

<script type="text/javascript">
//automatic multiple combo.

var htmlobjek;
$(document).ready(function(){  
	
	  $("#cbotype").change(function(){
		var cbottype_val = $("#cbotype").val();
		var url = '<?php echo site_url("mazda_stock_sales/c_slsstock/c_crud_stock/select_warna_combo_jquery"); ?>';
		
		$.ajax({
			type:'POST',
			url: url,		
			data:{id_type_post:cbottype_val},	
			cache: false,          
			success: function(msg){            
				$("#cbowarna").html(msg);							
			}
		});
	  });	  	   
	    
});

</script>

<script type="text/javascript">
//automatic show price 

var htmlobjek;
$(document).ready(function(){  
	 $("#cbotype").change(function(){
			var cbottype_price = $("#cbotype").val();
			var url = '<?php echo site_url("mazda_stock_sales/c_slsstock/c_crud_stock/doget_price_unit_jquery"); ?>';
			
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{id_type_post:cbottype_price},	
				cache: false,          
				success: function(data, textStatus, jqXHR){            
					$('[name="curr"]').val(data.currency);					
					$('[name="price_unit"]').val(numeral(data.price_sell).format('0,0.00'));			
				}
			});
		  });
});	  
</script>