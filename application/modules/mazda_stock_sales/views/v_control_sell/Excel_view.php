<?php 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=report_excel_stock_unit.xls");
	header("Pragma: no-cache");
	header("Expires: 0"); 	
?>
<?php 
		echo '<table width="100%" border="1"  >';		
		echo '<tr style=" color:#144b79;font-weight:bold;font-size: 12px; ">';		
		echo '<td width="5%"align="center">SPK No</td>';
		echo '<td width="5%"align="center">Stock Number</td>';		
		echo '<td width="5%" align="center">Branch</td>';													
		echo '<td width="9%" align="center">Type</td>' ;
		echo '<td width="3%" align="center">Vin</td>' ;
		echo '<td width="7%" align="center">Engine</td>' ;	
		echo '<td width="8%" align="center">Colour</td>' ;				
		echo '<td width="5%" align="center">Year</td>' ;			
		echo '<td width="6%" align="center">Sales</td>';			
		echo '<td width="5%" align="center">Customer</td>' ;
		echo '<td width="5%" align="center">Curr</td>' ;
		echo '<td width="5%" align="center">DP Date Customer</td>' ;
		echo '<td width="5%" align="center">DP Customer</td>' ;		
		echo '<td width="3%" align="center">Cost Price</td>' ;	
		echo '<td width="3%" align="center">Sell Price</td>' ;	
		echo '<td width="3%" align="center">Discount</td>' ;	
		echo '<td width="3%" align="center">Total Pay Customer</td>' ;
		echo '<td width="3%" align="center">Pay + Disc</td>' ;
		echo '<td width="3%" align="center">Remain Pay </td>' ;		
		echo '<td width="3%" align="center">Date Alocation</td>' ;
		echo '<td width="3%" align="center">Date Remain</td>' ;		
		echo '<td width="3%" align="center">Payment Status</td>' ;
		echo '<td width="5%" align="center">Remarks Selling</td>' ;															
		echo '</tr>' ;		    		
?>			
	<?php if(!empty($data_excel)) : ?>
	  <?php foreach($data_excel as $item) {	?>
         <?php
					 			  	
						/*if ($item['date_dudate_spk'] <= date('Y-m-d')) :
						   if ($item['status_flag_sell'] == "2") : 		
							  echo '<tr style="background:red">'; 	
							endif;
						endif; */
						
						if ($item['status_flag_sell']== 0 ) :  
						   echo '<tr style="background:#BBDBFB">';	 
						else:		
						   	if ($item['status_flag_sell'] == "2") : 
								if ($item['date_dudate_spk'] <= date('Y-m-d')) :		
							       echo '<tr style="background:red">'; 	
								else:   
								   echo '<tr style="background:#6F6">';	 
							    endif;	
							endif;													 
						endif ;							
			         	
				?>                  
                <td align="center"><?=$item['nospk']?></td>
                <td align="center"><?=$item['stock_no']?></td>
                <td><?=$item['name_branch']?></td>
                <td><?=$item['desc_type']?></td>
                <td><?=$item['vin']?></td>
                <td><?=$item['engine']?></td>                                        
                <td><?=$item['colour']?></td> 
                <td><?=$item['year']?></td>
                <td><?=$item['sal_name']?></td>                                        
                <td><?=$item['cust_name']?></td>                 
                <td><?=$item['curr_sell']?></td> 
                <td><?=$item['date_dp_cust']?></td>  
                <td><?php echo number_format($item['dp_customer'],2,'.',',') ?></td>              
                <td><?php echo number_format($item['price_cost'],2,'.',',') ?></td>              
                <td><?php echo number_format($item['price_sell'],2,'.',',') ?></td>               
                <td><?php echo number_format($item['discount'],2,'.',',') ?></td>  
                <td><?php echo number_format($item['total_paymet_customer'],2,'.',',') ?></td>  
                <td><?php echo number_format($item['payment_plus_discount'],2,'.',',') ?></td>   
                <td  align="center"  style="color:#000000;font-weight:bold;">
				 <?php 
						
					  $str_balanced = "0.00";			  
		   			  $str_flag_sell = "2" ;
					  $str_flag_sold = "0" ;
					  $str_flag_send_approval = "1" ;
					  $str_flag_bm_approval = "0" ;
		            
											
				 if ($item['status_flag_sell'] == $str_flag_sold) : 		
					echo '<label>'."SOLD".'</label>'; 
				 else:		
					if ($item['status_flag_sell'] == $str_flag_sell && $item['status_send_approval'] = $str_flag_send_approval) : 			
						if ($item['date_dudate_spk'] <= date('Y-m-d')) :								   	
							echo number_format($item['remaining_amount_customer'],2,'.',',')." ".'Due Date'; 	
						 else:													
						 	  if ($item['status_flag_sell'] == $str_flag_sell && $item['approve_bm'] =$str_flag_bm_approval): 	
							      echo number_format($item['remaining_amount_customer'],2,'.',',')." "."Not Approval Yet";	
							  else:							 
							      echo number_format($item['remaining_amount_customer'],2,'.',',');	
							  endif;	 								  							
						 endif;
					   endif;	 						      
				  endif;				            
		   				
				 ?>
                </td>          
                <td><?=$item['date_aloc']?></td> 
                <td><?=$item['date_reminder_spk']?></td>
                <td><?= $item['desc_pay'] ?></td>    
                <td><?= $item['remarks'] ?></td>                           			
            </tr>
		<?php } ?>
     <?php endif ?>   
</table>
<table>
	<tr>		
    	<td style="background:#6F6">Total Booked : <?php echo $count_row_dp ?></td>    	
        <td style=" background:#BBDBFB">Total Selling : <?php echo $count_row_selling ?></td>     
	</tr>    
 </table>   