<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js');?>" ></script>
<script>


// fungsi untik checklist semua otomatis
function CheckAll()
{
    l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = true;		
		}	
		document.getElementById('btnedit').disabled = true;	
		document.getElementById('btndel').disabled = false;	
		document.getElementById('btnsendtab').disabled = true;	
}

function UncheckAll()
{
   l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = false;			 
		}
		document.getElementById('btnedit').disabled = true;
		document.getElementById('btndel').disabled = true;
		document.getElementById('btnsendtab').disabled = true;
}


//--------------------function disbaled enabled button with check book

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_reject').removeAttr('disabled');			
        }
        else{
            $('.btn_reject').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_send').removeAttr('disabled');			
        }
        else{
            $('.btn_send').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_aproved').removeAttr('disabled');			
        }
        else{
            $('.btn_aproved').attr('disabled', 'disabled');								
        }
    });
});

//-------------------------------------------------------------------------		 	

//-------------------------------------------------------------------------		 	
  
//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});		  
//set focus ----------------------------  
 
</script>

 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_fail');?>  
        </div>	
<?php } ?>

      
  
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" ><?php echo "Table Approval Cross Selling Finance "." | ".$header ; ?> </div>
   <div class="panel-body">               
   
          
 <?php  echo form_open('mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell/multiple_submit','id = form_my');  ?>     
 
    <!-- Menu Button Start -->
			<div class="row text-right">
				
        <button id="btnrefresh" name="btnrefresh" class="btn btn-app btn-success btn-xs radius-4" value="Refresh" type="submit">
            <i class="ace-icon fa fa-refresh bigger-160"></i>
              Refresh
          </button> 
          
          <button id="btnaprove_btnaprove_show" name="btnaprove_show" class="btn btn-app btn-primary btn-xs radius-4 btn_aproved" value="Do Approved" type="button" disabled>
            <i class="ace-icon fa fa-check bigger-160"></i>
            Approve
          </button>
				
        <!-- Search Menu Start -->
        <table align="left">

          <td>
            <label>Selected: </label>
          </td>
          <td>&nbsp;</td>
          <td>
            <select name="cbostatus" id="cbostatus" class="form-control">
              <option value="vin">VIN</option>
              <option value="engine">ENGINE</option>
              <option value="stock_no">STOCK NO</option>
              <option value="csi">CSI</option>
              <option value="desc_type">TYPE</option>
              <option value="location_stock">LOCATION</option>
            </select>
          </td>
          <td>&nbsp;</td>

          <td>
            <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari"
              value="<?php echo $this->session->flashdata('cari'); ?>">
          </td>

          <td>

            <button id="btncari" name="btncari" class="btn btn-app btn-primary btn-xs radius-4" value="Search"
              type="submit">
              <i class="ace-icon fa fa-search bigger-160"></i>
              Search
            </button>

            <!-- <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF" />               -->

          </td>
          </tr>
        </table>
        <!-- Search Menu Start -->

			</div>
			<!-- Menu Button End -->
        <div class="space-12"></div>
 
    <!-- Table Responsive -->
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Stock No</th>
            <th>Type</th>
            <th>Vin</th>
            <th>Engine</th>
            <th>Colour</th>
            <th>WRS Date</th>
            <th>User Submission</th>
            <th>Unit Form</th>
            <th>Unit To</th>
            <th>Date Req Approv</th>
            <th>Remarks</th>
            <th>Evidence</th>
            <th>Action</th>
            <th hidden="true">Add/Edit Detail</th>
            <th><a href="javascript:void(0)" onclick="CheckAll()" style="text-decoration:none"> All</a>
              \
              <a href="javascript:void(0)" onclick="UncheckAll()" style="text-decoration:none">Uncheck</a>

            </th>
          </tr>
        </thead>
        <tbody>
          <?php if ($ceck_row >= 1) :		  
          foreach($stock_view as $row):		 
          if ($row->status_flag_stock == 3) : 	  		  
            echo '<tr style="font-size:11px;" class="btn-warning">'; 
          else:			    					   		  
            echo '<tr style="font-size:11px;" class="btn-success">';		
          endif;		 
          ?>
          <td width="8%" align="center"><?php echo $row->stock_no; ?></td>
          <td><?php echo $row->desc_type ; ?></td>
          <td><?php echo $row->vin ; ?></td>
          <td align="center"><?php echo $row->engine ; ?></td>
          <td><?php echo $row->colour ; ?></td>
          <td align="center">
            <?php 
						if ($row->temp_date_wrs != null):
							echo date('d-m-Y', strtotime($row->temp_date_wrs)) ; 
						else:	
							echo "-" ;
						endif;	
					?>
          </td>
          </td>
          <td align="center"><?php echo $row->user_send_cross_sell ; ?></td>
          <td align="center"><?php echo $row->from_cross_sell ; ?></td>
          <td align="center"><?php echo $row->dst_cross_sell ; ?></td>
          <td align="center">
            <?php 
						if ($row->date_send_approval != null):
							echo date('d-m-Y', strtotime($row->date_send_approval)) ; 
						else:	
							echo "-" ;
						endif;	
					?>
          </td>
          <td align="center"><?php echo $row->remarks_cross_sell ; ?></td>

          <td align="center"><?php 
				    If ($row->attach_foto != "")
					  {  
					  ;
					   echo anchor_popup($row->attach_foto,"<span class='glyphicon glyphicon-search btn btn-primary' aria-hidden='true'>".'View'."</span>");
					 }else{
					    echo '<div style=" color:#EB293D" align="center">'."No PDF".'</div>';	  
					 }
					?>
          </td>

          <td align="center">
            <?php
				 if ($row->status_flag_stock != 3) : 	  		  
						echo 'Cross selling Approved'; 
					 else:			 
				?>
            <a href="#" class="btn btn-danger btn-setting reject" id="reject" style="text-decoration:none"
              req_id_reject="<?php echo $row->stock_no ?>"><span class="glyphicon glyphicon-remove-sign"
                aria-hidden="true"></span>Reject</a>
            <?php endif; ?>
          </td>

          <td align="center">
            <?php
				 if ($row->status_flag_stock != 3) : 	  		  
						echo 'Cross selling Approved'; 
					 else:			 
				?>
            <div align="center"><input id="checkchild" name="msg[]" type="checkbox"
                value="<?php echo $row->stock_no ; ?>" />
              <?php endif; ?>
              <?php form_close(); ?>
            </div>
          </td>
          </tr>
          <?php 		    
		  endforeach;
 	   endif;	  				
		 ?>
        </tbody>
      </table>
    </div>
    <!-- Table Responsive -->
		 
		<?php		   			
		     if (! empty($pagination))			
			 { 
		  	   echo '<center><table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table></center>';				 
			 }			  	  						
		?>
        
 </div>  
</div> 
      
          
  </div>
 </div>
   
</div>

<!-- Modal Buat Cross Selling -->

  <div class="modal fade" id="myModal-show-aprove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Verification Approval Cross Selling</h4>
                    </div>
                    
                    <div class="modal-body-reject">
                       <table width="90%" class="table-condensed" align="center">
                            <tr> 
                                <td> <label for="Transaction ID"  class="col-xs-8">Stock No</label></td> 
                             
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtnostock" name="txtnostock" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                              <tr> 
                                <td> <label for="VIN"  class="col-xs-8">VIN</label></td>                              
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtvin_cross" name="txtvin_reject" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                              <tr> 
                                <td> <label for="Engine"  class="col-xs-8">Engine</label></td>                              
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtengine_cross" name="txtengine_reject" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                              <tr> 
                                <td> <label for="Type"  class="col-xs-8">Type</label></td>                              
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txttype_cross" name="txttype_reject" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                              <tr> 
                                <td> <label for="Colour"  class="col-xs-8">Colour</label></td>                              
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtcolour_cross" name="txtcolour_reject" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>   
                               <tr> 
                                <td><label for="from"  class="col-xs-8">Branch From</label></td>                              
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtfrom_b" name="txtfrom_b" class="form-control" readonly>
                                   </div> 
                                  </td> 
                             </tr>  
                              <tr>
                                   <td><label for="dest"  class="col-xs-8">Branch Id Destination</label></td>                    
                                	<td>
                                    	<div class="col-xs-4">
                                    <input type="text" id="txtid_b" name="txtid_b" class="form-control" readonly>
                                   </div>
                                    </td>
                                </tr>
                               <tr> 
                                <td><label for="dest"  class="col-xs-8">Branch Destination</label></td>                              
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtdest_b" name="txtdest_b" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>   
                             
                             <tr> 
                                <td ><label for="Remarks Cross Selling"  class="col-xs-9">Remarks Cross Selling</label></td> 
                                <td>
                                  <div class="col-xs-12">
                                    <textarea id="txtremarks_cross" name="txtremarks_cross"  class="form-control" readonly></textarea>
                                  </div>  
                                </td>    
                             </tr>                             
                             
                             <tr> 
                                <td></td> 
                                <td>
                                   <div class="col-xs-12">
                                   
                                   </div>        
                                </td>    
                             </tr>
                            </table> 
           
                    </div> 
                    
                     <div class="modal-footer">
                     <input type="submit" name="btncross-approved" value="Give Approval" class="btn btn-danger"/>
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>           
        </div>
    
<!-- Modal Buat Cross Selling -->	


<!-- Modal Buat Reject -->

  <div class="modal fade" id="myModal-reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Remarks For Reject Status</h4>
                    </div>
                    
                    <div class="modal-body-reject">
                       <table width="90%" class="table-condensed" align="center">
                            <tr> 
                                <td> <label for="Transaction ID"  class="col-xs-8">Stock No</label></td> 
                             
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtidstock" name="txtidstock" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                             </tr>
                              <tr> 
                                <td> <label for="VIN"  class="col-xs-8">VIN</label></td>                              
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtvin_reject" name="txtvin_reject" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                              <tr> 
                                <td> <label for="Engine"  class="col-xs-8">Engine</label></td>                              
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtengine_reject" name="txtengine_reject" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                              <tr> 
                                <td> <label for="Type"  class="col-xs-8">Type</label></td>                              
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txttype_reject" name="txttype_reject" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                              <tr> 
                                <td> <label for="Colour"  class="col-xs-8">Colour</label></td>                              
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtcolour_reject" name="txtcolour_reject" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                             <tr> 
                                <td ><label for="Reject Reason"  class="col-xs-9">Reject Reason</label></td> 
                                <td>
                                  <div class="col-xs-12">
                                    <textarea id="txtreject" name="txtreject"  class="form-control"></textarea>
                                  </div>  
                                </td>    
                             </tr>
                             <tr> 
                                <td></td> 
                                <td>
                                   <div class="col-xs-12">
                                   
                                   </div>        
                                </td>    
                             </tr>
                            </table> 
           
                    </div> 
                    
                     <div class="modal-footer">
                     <input type="submit" name="btnreject" value="Submit Reject" class="btn btn-danger"/>
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>           
        </div>
    
<!-- Modal Buat Reject -->  	



<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- jQuery_ui -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->


<script>
//get data table remarks to modal popup.
$(function(){
  $(document).on('click','.print',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("mazda_stock_sales/c_approval_bm/c_approval_bm/get_idtrans_modal"); ?>';
		
		$("#ViewDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_trans_sales:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});
</script>

<script>
//get data table remarks to modal popup. add edit
$(function(){
  $(document).on('click','.add_detail',function(e){
		var req_id_add_edit_detail = $(this).attr('req_id_add_edit_detail'); //atribut from <a ref 	php echo $row->id_master 
		var url = '<?php echo site_url("mazda_stock_sales/c_approval_bm/c_approval_bm/get_add_edit_idtrans_modal"); ?>';
		
		$("#AddEditDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_trans_sales_edit:req_id_add_edit_detail},
			success: function(msg) {
				$('.modal-body').html(msg);				
			}
						
		}); 				
		
   });	
});
</script>

<script>
//get data table remarks to modal popup. add edit
/*
$(function(){
  $(document).on('click','.add_detail',function(e){
		var req_id_add_edit_detail = $(this).attr('req_id_add_edit_detail'); //atribut from <a ref 	php echo $row->id_master 
		var url = '<?php echo site_url("mazda_stock_sales/c_approval_bm/c_approval_bm/get_add_edit_idtrans_modal"); ?>';
		
		//$("#AddEditDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_trans_sales_edit:req_id_add_edit_detail},
			success: function(xx) {
				$('#tempmodal').html(xx);
				$("#testmodal").modal('show');
			}
						
		}); 				
		
   });	
});
*/
</script>


<script type="text/javascript">

$(function(){
	//jquery untuk post submit save / edit
    $('#myform').on('submit', function(e){ //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
        e.preventDefault();		
		var url = '<?php  echo base_url('mazda_stock_sales/c_approval_bm/c_approval_bm/multiple_submit');  ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL			
            type: 'POST', //or POST
            data: $('#myform').serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
            success: function(data){              
            }
        });
    });
}); 
</script>

<script>
$(function(){ // Show modal pop up send REJECT
  $(document).on('click','.reject',function(e){
	 var req_id_stock = $(this).attr('req_id_reject'); 
	 var url = '<?php echo site_url('mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell/get_stock_no'); ?>';												
	  $("#myModal-reject").modal('show');
	  $('#txtidstock').val(req_id_stock); 	
	  $('#txtreject').val(''); 	
	  $.ajax({			
			type:'POST',
			url: url,
			dataType: "json",
			data: {txtidstock:req_id_stock}, 
			success: function(data, textStatus, jqXHR ) {				    									
				$('#txtvin_reject').val(data.txtvin);			
				$('#txtengine_reject').val(data.txtengine);				
				$('#txttype_reject').val(data.txttype);
				$('#txtcolour_reject').val(data.txtwarna);									
			}
		});		
			  				
  });																					
});		
</script>




<!----- view moodal approved --->
<script>

$(function(){
  $(document).on('click','.btn_aproved',function(e){	
		var req_id_stock = $('input[type="checkbox"]:checked').val()	
		var url = '<?php echo site_url('mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell/get_stock_no'); ?>';
							
		$("#myModal-show-aprove").modal('show'); 
		$("#txtnostock").val(req_id_stock); 			
		
		$.ajax({			
			type:'POST',
			url: url,
			dataType: "json",
			data: {txtidstock:req_id_stock}, 
			success: function(data, textStatus, jqXHR ) {				    									
				$('#txtvin_cross').val(data.txtvin);			
				$('#txtengine_cross').val(data.txtengine);				
				$('#txttype_cross').val(data.txttype);
				$('#txtcolour_cross').val(data.txtwarna);	
				$('#txtfrom_b').val(data.txtfrom);
				$('#txtid_b').val(data.txtiddest);
				$('#txtdest_b').val(data.txtdest);
				$('#txtremarks_cross').val(data.txtremark);			
						
			}
		});		
   });	
});

</script>

<script>
$(function(){
	//jquery untuk post submit save / edit
    $('#myform').on('submit', function(e){ //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
        e.preventDefault();		
		var url = '<?php  echo base_url('mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell/multiple_submit');  ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
            success: function(data){              
            }
        });
    });
});
</script>
