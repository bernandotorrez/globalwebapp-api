<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.css');?>" />  
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" />        
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />       -->


        <?php 
			foreach ($str_trans_master as $row_head) : 
				 echo '<table  class="table-striped  table-hover table-condensed table-bordered" width="100%" style="font-size:12px" >';						 				 echo '<tr>';
				 echo "<td>SPK NO"." : </td>".'<td>'.$row_head->nospk.'</td>';
				 echo '</tr>';	
				 echo '<tr>';
				 echo "<td>TYPE"." : </td>".'<td>'.$row_head->desc_type.'</td>';
				 echo '</tr>';	
				 echo '<tr>';
				 echo "<td>COLOUR"." :</td> ".'<td>'.$row_head->colour.'</td>';
				 echo '</tr>';	 
				 echo '<tr>';
				 echo "<td>VIN"." : </td>".'<td>'.$row_head->vin.'</td>';
				 echo '</tr>';	 
				 echo '<tr>';
				 echo "<td>ENGINE"." : </td>".'<td>'.$row_head->engine.'</td>';
				 echo '</tr>';	 
				 echo '<tr>';
				 echo "<td>CUSTOMER"." : </td>".'<td>'.$row_head->cust_name.'</td>';
				 echo '</tr>';	
				 echo '<tr>';
				 echo "<td>SALES"." : </td>".'<td>'.$row_head->sal_name.'</td>';
				 echo '</tr>';	
				 echo '</table>';	       
			 endforeach;
		 ?>
<br>
       
<table class="table-condensed table-bordered"  width="100%" >        
    <tr style="background:#EFEFEF;font-weight:bold; font-size:12px;">
        <td width="5%">No</td>
        <td  align="center" width="25%">Description</td>
        <td  align="center" width="20%">Date Payment</td>
        <td  align="center" width="30%">Payment</td>
        <td width="20%" align="center">Evidence</td>							     
    </tr>
    <?php foreach ($str_trans_detail as $row_detail)  : $intno = $intno +1;  ?>
   		<tr style="font-size:12px;">	
          <td><?php echo $intno ?></td>							  
          <td><?php echo $row_detail->desc_det ?></td>							  
          <td align="center">
		  <?php 
		  	  if ($row_detail->pay_date  !=""):
		  		echo  date('d-m-Y', strtotime($row_detail->pay_date)); 
			  endif;
			?>
          </td>																																																			
          <td align="center"><?php echo  number_format($row_detail->amount,2,'.',',') ?></td>	
          <td align="center">						
          <?php
			  if ($row_detail->attach_trans!= "") :
			     echo anchor_popup($row_detail->attach_trans,"<span class='glyphicon glyphicon-search ' aria-hidden='true'>".'View'."</span>");		
			 else:
				 echo "No Data";  	
			 endif; 
			 ?>    		  	
           </td>
        </tr>
    <?php endforeach; ?>
</table>

<br>

<table width="95%" class="table-striped  table-hover table-condensed table-bordered" style="font-size:12px">



  <?php foreach ($str_trans_master as $row) : ?>
       <tr style="background:#EFEFEF;"  >
           <td  style="font-weight:bold" width="50%">Payment Status </td>
           <td  style="font-weight:bold; color:#FF4246"  width="32%" align="left">
            <div class="col-xs-10">
                <label>
			   <?php 
                    echo $row->desc_pay ;					
               ?>
               </label>
              </div> 
          </td>
    </tr>
    
     <tr style="background:#EFEFEF;"  >
           <td  style="font-weight:bold" width="30%">Price Unit :</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
                <label>
               		 <?php echo $row->curr_sell." ".number_format($row->price_sell,2,'.',',') ?>
                </label>
            </div>
           </td>
    </tr>
    
    <tr style="background:#EFEFEF;"  >
           <td  style="font-weight:bold" width="30%">Discount :</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
		      <label>
		       <?php echo $row->curr_sell." ".number_format($row->discount,2,'.',',') ?>
             </label>
           </div>
           </td>
    </tr>
    
     <tr style="background:#EFEFEF;"   >
           <td  style="font-weight:bold" width="30%">Total Payment Customer:</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
		    <label><?php echo $row->curr_sell." ".number_format($row->total_paymet_customer,2,'.',',') ?></label>
            </div>
           </td>
    </tr>
    
     <tr style="background:#EFEFEF;"   >
           <td  style="font-weight:bold" width="30%">Payment / DP + Discount:</td>
           <td  style="font-weight:bold;" width="70%" align="left">
            <div class="col-xs-10">
		    <label><?php echo $row->curr_sell." ".number_format($row->payment_plus_discount,2,'.',',') ?></label>
            </div>
           </td>
    </tr>
             
    <tr style="background:#FAFECB;"   >           
           <td style="font-weight:bold" width="30%">Payment Customer Remaining  :</td>
           <td width="70%" style="font-weight:bold; color:#FF4246"  >          
             <div class="col-xs-10">
              <label><?php echo $row->curr_sell." ".number_format($row-> remaining_amount_customer,2,'.',',') ?></label>
               <?php if ($row->remaining_amount_customer =="0.00") :?>
                  <span id="imglunas">
                   <label> | Ready To BSTK</label>
                  <img src="<?php echo base_url("asset/images/success_ico.png")." " ?>">
                 </span>
               <?php else :  ?>   
           	    <span id="imglunas" hidden="true"><img src="<?php 
			        echo base_url("asset/images/success_ico.png") ?>">
                </span>	
               <?php endif; ?>
           </div>    
           </td>
    </tr> 
      
     <tr style="background:#FAFECB;"   >           
           <td style="font-weight:bold" width="30%">Remarks Selling : </td>
           <td width="70%" style="font-weight:bold; color:#FF4246"   >		
           <div class="col-xs-10">
            <?php                     
			   echo	$row->remarks ;					
             ?>
            </div> 
           </td>          
    </tr>
  <?php endforeach  ?>
</table> 