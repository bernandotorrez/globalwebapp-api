


 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>
      

  
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" ><?php echo "Add Colour Type"." | ".$header ; ?> </div>
   <div class="panel-body">               
   
          
 <?php  echo form_open('mazda_stock_sales/c_colour_vehicle/c_colour_vehicle/multiple_submit','id = form_my');  ?>      
 
 <!-- Menu Button Start -->
 <div class="row text-right">
     
         <button id="btnadd" name="btnadd" class="btn btn-app btn-primary btn-xs radius-4 addnew" value="Add New"
             type="button">
             <i class="ace-icon fa fa-book bigger-160"></i>
             Add New
         </button>

         <button id="btnedit" name="btnedit" class="btn btn-app btn-success btn-xs radius-4 edit_vendor" value="Edit"
             type="button" disabled>
             <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
             Edit
         </button>

         <button id="btndel" name="btndel" class="btn btn-app btn-warning btn-xs radius-4 btn_delete" value="Delete"
             type="submit" onclick="return confirm('are you sure want to delete this data!')" disabled>
             <i class="ace-icon fa fa-ban bigger-160"></i>
             Delete
         </button>
     
    <!-- Search Menu Start -->
    <table align="left">

        <td>
            <label>Selected: </label>
        </td>
        <td>&nbsp;</td>
        <td>
            <select name="cbostatus" id="cbostatus" class="form-control">
                <option value="desc_type">Type</option>
                <option value="colour">Colour</option>
            </select>
        </td>
        <td>&nbsp;</td>

        <td>
            <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari"
                value="<?php echo $this->session->flashdata('cari'); ?>">
        </td>

        <td>

            <button id="btncari" name="btncari" class="btn btn-app btn-primary btn-xs radius-4" value="Search"
                type="submit">
                <i class="ace-icon fa fa-search bigger-160"></i>
                Search
            </button>

            <!-- <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF" />               -->

        </td>
        </tr>
    </table>
    <!-- Search Menu Start -->

 </div>
 <!-- Menu Button End -->
    <div class="space-12"></div>

    <!-- Table Responsive -->
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Type ID</th>
                    <th>Vehicle Type</th>
                    <th>Colour</th>
                    <th><a href="javascript:void(0)" onclick="CheckAll()" style="text-decoration:none"> All</a>
                        \
                        <a href="javascript:void(0)" onclick="UncheckAll()" style="text-decoration:none">Uncheck</a>

                    </th>
                </tr>
            </thead>
            <tbody>
                <?php if ($ceck_row >= 1) { 	  
			        foreach($colour_view as $row){ $intno++;				   			
                ?>
                <?php if ($intno % 2 == 0) : ?>
                <tr style="background:#F0F0F0;font-size:12px;color:#B90020">
                    <?php else:	?>
                <tr style="font-size: 12px">
                    <?php endif?>
                    <td align="center"><?php echo $row->id_colour; ?></td>
                    <td><?php echo $row->desc_type ; ?></td>
                    <td><?php echo $row->colour ; ?></td>
                    <td>
                        <div align="center"><input id="checkchild" name="msg[]" type="checkbox"
                                value="<?php echo $row->id_colour ; ?>" />
                            <?php form_close(); ?>
                        </div>
                    </td>
                </tr>
                <?php  } }?>
            </tbody>
        </table>
    </div>
    <!-- Table Responsive -->
		 
		<?php		   			
		     if (! empty($pagination))			
			 { 
		  	   echo '<center><table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table></center>';				 
			 }			  	  						
		?>
        
 </div>  
</div> 
  
  
  
          
  </div>
  </div>
 
</div>   	
		 

<style>
/*label berwarna merah ketika validasi fom modal popup. */
	label
	{
		width:50%;
		display: block;
		margin-right:2%;
		text-align:left;
		line-height:22px;
	}	
    span.error
	{
		font: 12px ;
		color:red;
		margin-left:8px;
		line-height:22px;
	}	
/*------------------------- */
</style>
	
 <!-- Modal Buat save and edit -->
     <div id="tampilanform"></div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    
                    <div class="modal-body">
                          <table width="90%" class="table-condensed" align="center">
						
                         <tr> 
                        <td id="row_label"> <label for="Id Vendor"  class="col-xs-12" style="color:#C00">ID Model</label></td>                              
                        
                          <td id="row_input">
                          <div class="control-group">
                               <div class="col-xs-12">
                               <input name="txtid"  type="text" class="form-control" id="txtid"  readonly="readonly" >    
                               </div> 
                           </div>
                          </td>                                  
                        </tr>  
                        <tr> 
                        <td> <label for="Vendor" style="color:#C00"  class="col-xs-12">Type Vehicle *</label></td>                              
                        
                          <td>
                          <div class="control-group">
                               <div class="col-xs-12">   
                               <select name="cbotype" id="cbotype" class="form-control">
                               		<option>-Choose-</option>
                                    <?php
										foreach ($tampil_type as $row_type) :
											echo '<option value="'.$row_type->id_type.'">'.$row_type->desc_type.'</option>' ;
										endforeach
									 ?>
                               </select>
                               </div> 
                           </div>    
                          </td>                                  
                        </tr>        
                        <tr> 
                        <td> <label for="Vendor" style="color:#C00"  class="col-xs-12">Colour *</label></td>                              
                        
                          <td>
                          <div class="control-group">
                               <div class="col-xs-12">   
                                <input name="txtcolour"  type="text" class="form-control txtfocus" id="txtcolour" maxlength="60"  >
                               </div> 
                           </div>    
                          </td>                                  
                        </tr>                                                       
                        </table>                 
                    </div> 
                    
                     <div class="modal-footer">
                        <input type="submit" name="btnsave" id="btnsave" value="Submit " class="btn btn-primary"/>
                        <input type="submit" name="btnupdate" id="btnupdate" value="Submit Update" class="btn btn-primary"/>                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal Buat edit -->  	



<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.validate.js" charset="UTF-8"></script>

  
<script>
// fungsi untik checklist semua otomatis
function CheckAll()
{
    l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = true;		
		}	
		document.getElementById('btnedit').disabled = true;	
		document.getElementById('btndel').disabled = false;	
		document.getElementById('btnsendtab').disabled = true;	
}

function UncheckAll()
{
   l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = false;			 
		}
		document.getElementById('btnedit').disabled = true;
		document.getElementById('btndel').disabled = true;
		document.getElementById('btnsendtab').disabled = true;
}


//--------------------function disbaled enabled button with check book

$(function() {  //.btn hnya untuk tombol edit yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn-success').removeAttr('disabled');			
        }
        else{
            $('.btn-success').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btn-warning').removeAttr('disabled');			
        }
        else{
            $('.btn-warning').attr('disabled', 'disabled');								
        }
    });
});


  
//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});		  


//--------------function number only
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 45 && charCode != 41 && charCode != 40 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	
</script>

<script>
// Show modal pop up for addnew----------
$(function(){ 
	  $(document).on('click','.addnew',function(e){		       		  
			$("#myModal").modal('show');						  
			$("#myModalLabel").html("Add colour Vehicle");	
			$("#cbotype").val("-Choose-")	;
			$("#txtcolour").val("")	;			
		    $("#row_label").hide();	
			$("#row_input").hide();		
			$("#btnupdate").hide();	
		    $("#btnsave").show();		       												
	  });																						   	 	   
});		
</script>

<script>
 // Show modal pop up for edit and then get data table edit vendor to modal popup
$(function(){
  $(document).on('click','.edit_vendor',function(e){
	
		var req_id = $('input[type="checkbox"]:checked').val()
		var url = '<?php echo base_url("mazda_stock_sales/c_colour_vehicle/c_colour_vehicle/get_colour_modal_update"); ?>';
					
		$("#myModal").modal('show');  	
		$("#myModalLabel").html("Edit colour Vehicle");			
						
		$.ajax({			
			type:'POST',
			url: url,
			dataType: "json",
			data: {id_colour:req_id}, //cari berdasar kan ID_vendor yang di kirim dan di simpan pada var req_id 
			success: function(data, textStatus, jqXHR ) {				    			
			    $("#row_label").show();	
			    $("#row_input").show();	
				$("#btnsave").hide();
				$("#btnupdate").show();				
				$('[name="txtid"]').val(req_id);
				$('[name="cbotype"]').val(data.id_type);	
				$('[name="txtcolour"]').val(data.colour);					
			}
						
		});				
		
   });	
});	
</script>

<script>
$(function(){
	//jquery untuk post submit save / edit
    $('#myform').on('submit', function(e){ //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
        e.preventDefault();		
		var url = '<?php  echo base_url('mazda_stock_sales/c_colour_vehicle/c_colour_vehicle/multiple_submit');  ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
            success: function(data){              
            }
        });
    });
});
</script>


