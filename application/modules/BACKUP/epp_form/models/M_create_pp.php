<<<<<<< HEAD
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_create_pp extends CI_Model {	
				    
    public $db_tabel = 'qv_head_pp_complite';
	 
    public function tampil_add_pp(){ //tampil table untuk ngececk num_row						     	     		  
	     $ses_id_dept = $this->session->userdata('id_dept'); //manggili session id 
		 $ses_id_company = $this->session->userdata('id_company');  	
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 
		 $short_company = $this->session->userdata('short');	 		 		
		 $ses_branch_short = $this->session->userdata('branch_short'); 
		 $ses_dept = $this->session->userdata('dept');	 		 
		 
		  		 		 
		 $send_aprove = "0" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject
		 $send_reject = "-1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		// $status_purchase = "1" ; //1 pp biasa, 2 pp unit , 3 pp part, 4 pp project
		  
		 $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('id_company',$ses_id_company);	
		 $this->db->where('branch_id',$ses_id_branch );		
		 $this->db->where('id_dept',$ses_id_dept);		 
		 $this->db->where('status',$status_delete);
		 $this->db->where('status_send_aprove',$send_aprove);		
		 $this->db->or_where('status_send_aprove',$send_reject);
		 $this->db->where('status',$status_delete);		
		 $this->db->where('short',$short_company);						 
		 $this->db->where('branch_short',$ses_branch_short);	
		 $this->db->where('dept',$ses_dept);		
		 $this->db->order_by('date_pp','desc');
		 $result = $this->db->get('qv_head_pp_complite');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	     $ses_id_dept = $this->session->userdata('id_dept'); 
		 $ses_id_company = $this->session->userdata('id_company');  	
		 $ses_id_branch = $this->session->userdata('branch_id');
		 $short_company = $this->session->userdata('short');	 		 		
		 $ses_branch_short = $this->session->userdata('branch_short'); 
		 $ses_dept = $this->session->userdata('dept');	 		 		
		// $status_purchase = "1" ; //1 pp biasa, 2 pp unit , 3 pp part, 4 pp project
		 
		 $send_aprove = "0" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject
		 $send_reject = "-1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  	     		 
	     $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('id_company',$ses_id_company);					
		 $this->db->where('branch_id',$ses_id_branch );	
		 $this->db->where('id_dept',$ses_id_dept);
		 $this->db->where('status',$status_delete);
		// $this->db->where('flag_purchase',$status_purchase);
		 $this->db->where('status_send_aprove',$send_aprove);	
		 $this->db->or_where('status_send_aprove',$send_reject);
		 $this->db->where('status',$status_delete);	
		 $this->db->where('short',$short_company);						 
		 $this->db->where('branch_short',$ses_branch_short);	
		 $this->db->where('dept',$ses_dept);				  		 
		 $this->db->order_by('date_pp','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_head_pp_complite')->result();		 		   
		 return $result ;
	}							  		
				    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------		



	/* function getppnumber(){	//old counter 
	 	 
		$strshort = $this->session->userdata('short');//short company
        $strdept = substr(($this->session->userdata('dept')),0,3);
		$strbranchshort = $this->session->userdata('branch_short');
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");	
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
		 		    
		 
 	    $q = $this->db->query("select MAX(RIGHT(id_master,4)) as code_max from tbl_master_pp where left(id_master,2)='".substr($strshort,0,2)."'");
								
			 $code = "";			
				if($q->num_rows()>0){
					foreach($q->result() as $cd){
						$tmp = ((int)$cd->code_max)+1;
					    $code = sprintf("%04s", $tmp);;
					}
				}else{
					$code = "0001";
				}		
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	    return substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$code ;			
    } */
	
	function getppnumber(){	// view counter number when add new
	 	 
		$strshort = $this->session->userdata('short');//short company
		$stridcom = $this->session->userdata('id_company');//id company
		$striddept = $this->session->userdata('id_dept');//id company
		//$strdept = $this->session->userdata('id_initial');//id_initial
        $strdept = substr(($this->session->userdata('dept')),0,3);
		$strbranchshort = $this->session->userdata('branch_short');
		//$stryear = substr(date("Y"),-3,3);
		$stryear = substr(date("Y"),-2,2);
		$strmonth = date("m");			    
		//$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	   $qcounter = $this->db->query("select RIGHT(counter,3) as code_max from tbl_counter_ppno where id_dept='".$striddept."'");
									  	   
	   // $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); 	//check tanggal
	   $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strmonth."'"); 	//check bulan
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
				if($qcounter->num_rows()== 1):
					foreach($qcounter->result() as $cd):
						$tmp = ((int)$cd->code_max);
						$code = sprintf("%03s", $tmp);;
					endforeach;
					
					 if ($code== '000') :						
						 $code = "001";					 
					 endif;
					 
				 else:					  
					$code = "001";								
				 endif;		
		 else:		
		       $code = "001";					 
		 endif;
		 
	 //update tanggal ketable date--------------
	// $data = array('date_add'=>$strdates );																			
	 $data = array('date_add'=>$strmonth );																			
	 $this->db->update('tbl_date_compare',$data); 		 
	 //end--------------------
		 
	 //update tanggal ketable counter--------------
	  $data = array('counter'=> $code );	
	 // $this->db->where('id_company',$stridcom);															  									
	  $this->db->where('id_dept',$striddept);
	  $this->db->update('tbl_counter_ppno',$data); 		 
	 //end--------------------
		 
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	  //return substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$strdates."/".$code ;
	  return substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$code ;
	  //end------------------------------------------			 
    }
	
	function counterppnumber(){	//counter number when save data
	 	 
		$strshort = $this->session->userdata('short');//short company
		$stridcom = $this->session->userdata('id_company');//id company
		$striddept = $this->session->userdata('id_dept');//id company
        $strdept = substr(($this->session->userdata('dept')),0,3);
		//$strdept = $this->session->userdata('id_initial');//id_initial
		$strbranchshort = $this->session->userdata('branch_short');
		//$stryear = substr(date("Y"),-3,3);
		$stryear = substr(date("Y"),-2,2);
		$strmonth = date("m");			    
		//$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	    $qcounter = $this->db->query("select RIGHT(counter,3) as code_max from tbl_counter_ppno where id_dept='".$striddept."'");
									  	   
	    //$qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); //check tanggal
		$qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strmonth."'"); 	//check bulan
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
					if($qcounter->num_rows()>0):
						foreach($qcounter->result() as $cd):
							$tmp = ((int)$cd->code_max)+1;
						    $code = sprintf("%03s", $tmp);;
						endforeach;
						 if ($code== '000') :						
						     $code = "001";					 
					 	 endif;
					 else:					
						 $code = "001";					 
					 endif;		
		 else:	
		       $code = "001";					 
		 endif;
		 
		 //update tanggal ketable date--------------
			 $data = array('date_add'=>$strmonth );																			
			 $this->db->update('tbl_date_compare',$data); 		 
		 //end--------------------
		 
		 //update tanggal ketable counter--------------
			 $data = array('counter'=> $code );	
			// $this->db->where('id_company',$stridcom);															  									
			 $this->db->where('id_dept',$striddept);
			 $this->db->update('tbl_counter_ppno',$data); 		 
		 //end--------------------
		 
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	    //end------------------------------------------					 
	      return true;
    }
	
	function get_vendor()
	{
		 $id_company = $this->session->userdata('id_company');
	     $id_dept = $this->session->userdata('id_dept');
		 $status_del = "1"; //flag status delete pada table vendor
		 		 		 
	     $this->load->database();		
		 $this->db->select('id_vendor,vendor');		
		 $this->db->where('id_company',$id_company);
		 $this->db->where('id_dept',$id_dept);
		 $this->db->where('status',$status_del);
		 $result = $this->db->get('tbl_vendor');			     		 
		 return $result->result();	 	
	}
	
	function get_currency()
	{		
	     $this->load->database();		
		 $this->db->select('id_curr,currency');				
		 $result = $this->db->get('tbl_curr');			     		 
		 return $result->result();	 	
	}	
	 
	function get_purchase_type()
	{	
	     $id_user = $this->session->userdata('id');
		 $query =$this->db->query("select status_pur,id from tbl_user where id ='".$id_user."'") ;
		 $hasil =$query->result();
		 
		 foreach($hasil as $row) {
			 $cath_status_pur = $row->status_pur;
		 }
		 
		  $saparate_typepur = explode(",",$cath_status_pur );			 					 		 		   	      
		  $this->db->select('*');
			 if (count($saparate_typepur)=="5")	{	
									 
				 $this->db->where('id_flagpur',$saparate_typepur[0]);				
				 $this->db->or_where('id_flagpur',$saparate_typepur[1]);	
				 $this->db->or_where('id_flagpur',$saparate_typepur[2]);			 
				 $this->db->or_where('id_flagpur',$saparate_typepur[3]);		  								
				 $this->db->or_where('id_flagpur',$saparate_typepur[4]);	
			}else{
				if (count($saparate_typepur)=="4")	{				
					 $this->db->select('id_flagpur','flag,type_purchase');	 
					 $this->db->where('id_flagpur',$saparate_typepur[0]);				
					 $this->db->or_where('id_flagpur',$saparate_typepur[1]);	
					 $this->db->or_where('id_flagpur',$saparate_typepur[2]);			 
					 $this->db->or_where('id_flagpur',$saparate_typepur[3]);		  											
				}else{
					if (count($saparate_typepur)=="3")	{	
				
						 $this->db->select('id_flagpur','flag,type_purchase');	 
						 $this->db->where('id_flagpur',$saparate_typepur[0]);				
						 $this->db->or_where('id_flagpur',$saparate_typepur[1]);	
						 $this->db->or_where('id_flagpur',$saparate_typepur[2]);			 					
					}else{	
						if (count($saparate_typepur)=="2")	{	
											
						   $this->db->select('id_flagpur','flag,type_purchase');	 
						   $this->db->where('id_flagpur',$saparate_typepur[0]);										                           $this->db->or_where('id_flagpur',$saparate_typepur[1]);								 				
						}else{								
							$this->db->select('id_flagpur','flag,type_purchase');
							$this->db->where('id_flagpur',$saparate_typepur[0]);											 			    	    }
					}
				}
			}				   			 									
		 $result = $this->db->get('tbl_type_purchase'); 		     		 
		 return $result->result();	  
	}	
	
	function get_coa()
	{
		$id_company = $this->session->userdata('id_company');
		$result = $this->db->query("select * from tbl_coa where id_company = '".$id_company."'");
		return $result->result();	 	
	} 
		
	function insert_master_and_detail()	
	{ 
	     //-------------------Checck Duplicate NOPP / Idmaster		 
		 $ponumber = $this->input->post('txtnopp'); 		 		 
		 $query2=   $this->db->select('*')
						 ->where('id_master',$ponumber)	 //Checck Duplicate NOpo / Idmaster					
						 ->get('tbl_master_pp');		
		 
		 if ($query2->num_rows() == 1) // Jika ketemu buat NOPP baru.		 
		 {					 			  			   
			$strshort = $this->session->userdata('short');//short company
			$stridcom = $this->session->userdata('id_company');//id company
			$strdept = substr(($this->session->userdata('dept')),0,3);
			//$strdept = $this->session->userdata('id_initial');//id_initial
			$strbranchshort = $this->session->userdata('branch_short');
			//$stryear = substr(date("Y"),-3,3);
			$stryear = substr(date("Y"),-2,2);
			$strmonth = date("m");			    
			//$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	        $qcounter = $this->db->query("select RIGHT(counter,3) as code_max from tbl_counter_ppno where id_dept='".$striddept."'");
									  	   
	       // $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); //check tanggal
		   $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strmonth."'"); 	//check bulan
	   
			$code = ""; //empty code
			 if ($qdate->num_rows()== 1) :			
						if($qcounter->num_rows()>0):
							foreach($qcounter->result() as $cd):
								$tmp = ((int)$cd->code_max)+1;
								$code = sprintf("%03s", $tmp);;
							endforeach;
							 if ($code== '000') :						
								 $code = "001";					 
							 endif;
						 else:					
							 $code = "001";					 
						 endif;		
			 else:	
				   $code = "001";					 
			 endif;
		 
		    //update tanggal ketable date--------------
			  $data = array('date_add'=>$strmonth );																			
			  $this->db->update('tbl_date_compare',$data); 		 
		   //end--------------------
		 
		     //update tanggal ketable counter--------------
			  $data = array('counter'=> $code );	
			  $this->db->where('id_company',$stridcom);															  									
			  $this->db->update('tbl_counter_ppno',$data); 
			  //buat NOPP Baru jika duplicate						  
		      return  substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$code;						
		 }else{					     
		      //Ambil NOPP yang ada di inputbox posting	 jika tidak duplicate			 
			  $txtmasterid = $this->input->post('txtnopp'); 	  	   	  
	     }
		      //----mengambil data dari inputbox/textbox lalu di simpan didalam variable
			  $ses_idcom = $this->session->userdata('id_company');
			  $ses_dept = $this->session->userdata('id_dept');
			  $ses_branch = $this->session->userdata('branch_id');	  
			  $strhead_user= $this->session->userdata('head_user');
			  $str_emailhead_user= $this->session->userdata('email_head');
			  $str_emailcc_user= $this->session->userdata('email_cc');
			  $str_emailfc = $this->session->userdata('email_fc');
			  $str_emailbod = $this->session->userdata('email_bod');
			  $cbovendor = $this->input->post('cbovendor'); 
			 // $cbocoa = $this->input->post('cbocoa'); 
			  $txtsub	= $this->input->post('txtsub');	
			  $txt_desc_detail = $this->input->post('txtdesc[0]'); //*from description detail	  		  		  	  		   	  
			  $txtdate = date("Y-m-d h:i:sa");
			  
			  $txtemail =  $this->input->post('txtmail'); 	 	  
			  $txtphone = $this->session->userdata('phone'); 			
			  $idcurr = $this->input->post('cbocur');
			  
			  //---------------------unformat grandtotal---------
			  $txt_grand_total =$this->input->post('result'); 
			  if ( strstr( $txt_grand_total, ',' ) ) $txt_grand_total = str_replace( ',','', $txt_grand_total );  
			  //-------------------------------------
			  
			   //---------------------unformat ppn---------
			  $txt_ppn =$this->input->post('txtppn'); 
			  if ( strstr( $txt_ppn, ',' ) ) $txt_ppn = str_replace( ',','', $txt_ppn );  
			  //-------------------------------------
			  
			    //---------------------unformat ppn---------
			  $txt_pph =$this->input->post('txtpph'); 
			  if ( strstr( $txt_pph, ',' ) ) $txt_pph = str_replace( ',','', $txt_pph );  
			  //-------------------------------------
			  
			   //---------------------unformat grandtotalppn---------
			  $txt_grand_totalppn =$this->input->post('gpppn'); 
			  if ( strstr( $txt_grand_totalppn, ',' ) ) $txt_grand_totalppn = str_replace( ',','', $txt_grand_totalppn );  
			  //-------------------------------------
			  			
			  
			  $txtterm = $this->input->post('txtterm'); 				
			  $straprove_head = '0';
			  $straprove_fc = '0';
			  $straprove_purchasing = '0';
			  $straprove_bod = '0';	
			  $str_flag_fpb = '0';	
			  $str_flag_print_fpb = '0';	
			  $str_flag_bpk='0';
			  $str_flag_print_bpk = '0';	
			  $str_flag_purchase = $this->input->post('cbotype_pur');  ////'1 =inderect, 2= direct unit , 3 = direct part, 4 = project , 5 shipping.
			  	
			  $status_aktif = '1';
			  			 			  	  	  	  	  	  	  			  			  
			  /*if($this->input->post('btnsave')){ //Check apakah dari button save atau dari button send
			     $strsend_aproval = '0'; //flag 0 hanya disampan saja
				 $date_send ="";
			  }else{
			  	 $strsend_aproval = '1'; //flag 1 disimpan sekaligus di kirim.
				 $date_send = date("Y-m-d");
			  } */
			  
			  $strsend_aproval = '0'; 
			  $date_send ="";
			  	
			  $strremrks = $this->input->post('txtremarks');

			//$strid_user = $this->session->userdata('id');					
			//$ses_divisi = $this->session->userdata('id_divisi');
		  
			//---------------------------------------------- insert tbl_master_pp		      
			
			$data=array(
			            'id_master'=> $txtmasterid,
						'id_company'=>$ses_idcom, 
						//'id_divisi'=>$ses_divisi,
						'id_dept'=>$ses_dept,
						'branch_id'=>$ses_branch,
						'id_vendor'=>$cbovendor, 
						'user_submission'=>$txtsub,
						'head_user'=>$strhead_user,
						'header_desc'=>$txt_desc_detail, //*from description detail
						'date_pp'=>$txtdate,
						'email_pp'=>$txtemail,
						'email_head_pp'=>$str_emailhead_user,
						'email_cc_pp'=>$str_emailcc_user,
						'email_fc_pp'=>$str_emailfc,
						'email_bod_pp'=>$str_emailbod,
						'phone_pp'=>$txtphone,																													
						'gran_total'=>$txt_grand_total,	
						'ppn'=>$txt_ppn,
						'pph'=>$txt_pph,	
						'gran_totalppn'=>$txt_grand_totalppn,	
						//'id_coa'=>$cbocoa,							
						'term_top'=>$txtterm,
						'id_curr' =>$idcurr,								
						'aprove_head'=>$straprove_head,
						'approve_purchasing' =>$straprove_purchasing,
						'aprove_fc'=>$straprove_fc,									
						'aprove_bod'=>$straprove_bod,							
						'status'=>$status_aktif,						
						'status_send_aprove'=> $strsend_aproval,
						'remarks'=> $strremrks,
						'flag_fpb'=> $str_flag_fpb,
						'flag_print_fpb'=> $str_flag_print_fpb,	
						'flag_bpk' => $str_flag_bpk,
						'flag_print_bpk'=> $str_flag_print_bpk,	
						'flag_purchase' =>	$str_flag_purchase,						
						'date_send_aproval' => $date_send,
						//'id_user' =>$strid_user,
						'counter_reprint' => "0"									
						);					
										
			$this->db->insert('tbl_master_pp',$data);	
			
			
			//nyimpen session di array untuk session
			$buff=array('ses_noppnew'=> $txtmasterid,			 			 
						 'ses_usersubnew'=> $txtsub,
						 'ses_desc_detail'=> $txt_desc_detail,						 						
						 'ses_grandtotal'=>$txt_grand_total,
						 'ses_term'=>$txtterm,
						 'ses_strsend_aproval'=>$strsend_aproval ,
						 'ses_remarks'=>$strremrks) ;					   
		    $this->session->set_userdata($buff); //simpan di  dalam session untuk di tampilakan kedalam email
			//---------------------------------------------------------------------------					

			//------------------detail------------------------------------------------													 				
			$desc = $this->input->post('txtdesc');										
			$result = array();					
				foreach($desc AS $key => $val)
				{					 											  
					 $result[1] = array(			
					  "id_master"     => $_POST['txtidmasdet'][$key],			  
					  "desc"  		  => $_POST['txtdesc'][$key],						  
					  "spec"  		  => $_POST['txtspecs'][$key],
					  //"po_reff"  	  => $_POST['txtporeff'][$key],
					  //"coa"  	      => $_POST['cbocoa_search'][$key],
				      //"no_act"  	  => $_POST['cboact_search'][$key],
					  "qty"  		  => str_replace(',','',$_POST["txtqty"][$key]),
					  "harga"  		  => str_replace(',','',$_POST["txtprice"][$key]),
					  "total"  		  => str_replace(',','',$_POST["txtamount"][$key]),
					  "tax_type"	  => $_POST['cbotax'][$key],	
					  "tax_typepph"	  => $_POST['cbotaxpph'][$key],									
					  "tax_detail"	  => str_replace(',','',$_POST["txttaxcharge"][$key]),
					  "tax_detailpph" => str_replace(',','',$_POST["txttaxchargepph"][$key]),
					  "flagadjust"    => '0',
					  "keterangan"    => 'Normal',
					  "status"		  => $_POST['flagstat'][$key] 					  			
					 );
					 
				    $this->db->insert_batch('tbl_detail_pp', $result);
				} 
				
				//create log-----------------------------------
					$this->load->helper('file');
					$strcomlog = $this->session->userdata('company');
					$strbranchlog = $this->session->userdata('name_branch');
					$strdeptlog = $this->session->userdata('dept');
					
					$data = $txtmasterid.",".$strcomlog.",".$strbranchlog.",".$strdeptlog .",".$txtsub.",".$txtdate.",".$cbovendor.",".$txt_desc_detail.","."Action:insert \n";
					
					$file_path ='./asset/log/logeppinsert.txt';
					if(file_exists($file_path)){
						//write_file($file_path, $data, 'a');
						write_file($file_path, $data , "a+");
					}else{
						write_file($file_path, $data);
					}
					 
				 //end log--------------------------------------------
									
				return true;			     					
		 
	}
	
	
	function edit_pp()	
	{
		//master---------------------------------------post		  
	 	  $txtmasterid = $this->input->post('txtnopp'); 	
		  $txtsub	= $this->input->post('txtsub');		
		  $cbovendor = $this->input->post('cbovendor'); 	
		  $cbocur = $this->input->post('cbocur'); 		
		  $cbocoa = $this->input->post('cbocoa'); 
		  $txtterm = $this->input->post('txtterm');	 
		  $txtremarks = $this->input->post('txtremarks'); 			 		 
		  $txtdesc_detail = $this->input->post('txtdesc[0]');	
		  $str_flag_purchase = $this->input->post('cbotype_pur');  ////'1 =inderect, 2= direct unit , 3 = direct part, 4 = project , 5 shipping.			  
		  
		  //---remove row detail pp from fom_edit pp array format ------------		  
		   $get_iddet_remove_row = $this->input->post('txtiddet_buff') ;
		
	       if(isset($get_iddet_remove_row) && trim($get_iddet_remove_row!='')){					 			   
			   $stsrconvert_iddet = explode(",",$get_iddet_remove_row) ;				   
			   //print_r($stsrconvert_iddet) ;
			   //echo count($stsrconvert_iddet);			   
			   $flag_remove = "0";		   			   
			   for ($intiddet=0; $intiddet < count($stsrconvert_iddet) ; $intiddet++)
			   {				   									   				   	   
					//table detail 								
					$data = array('status'=>$flag_remove);	
										
					$this->db->where('id_detail', $stsrconvert_iddet[$intiddet]);
					$this->db->update('tbl_detail_pp',$data);						   	 				  
			   }  			
		   }		  
		  //end---------------------------------------------------------------------------
		  
		  
		  
		  //---------------------unformat grandtotal---------
			  $txt_grand_total =$this->input->post('result'); 
			  if ( strstr( $txt_grand_total, ',' ) ) $txt_grand_total = str_replace( ',','', $txt_grand_total );  
			  //-------------------------------------
			  
			   //---------------------unformat ppn---------
			  $txt_ppn =$this->input->post('txtppn'); 
			  if ( strstr( $txt_ppn, ',' ) ) $txt_ppn = str_replace( ',','', $txt_ppn );  
			  //-------------------------------------
			  
			    //---------------------unformat ppn---------
			  $txt_pph =$this->input->post('txtpph'); 
			  if ( strstr( $txt_pph, ',' ) ) $txt_pph = str_replace( ',','', $txt_pph );  
			  //-------------------------------------
			  
			   //---------------------unformat grandtotalppn---------
			  $txt_grand_totalppn =$this->input->post('gpppn'); 
			  if ( strstr( $txt_grand_totalppn, ',' ) ) $txt_grand_totalppn = str_replace( ',','', $txt_grand_totalppn );  
			  //-------------------------------------
		  
		   //if($this->input->post('btnsave_edit')){ //Check apakah dari button save atau dari button send
			   $strsend_aproval = '0'; //flag 0 hanya disimpan saja
			   $date_send ="";
		   //}else{
			//  	 $strsend_aproval = '1'; //flag 1 disimpan sekaligus di kirim.
			//	 $date_send = date("Y-m-d");
		  // }		 		 
		 		 
		
		  $txtmasterid = $this->input->post('txtnopp'); 			 
		  if (isset($txtmasterid) && trim($txtmasterid!=''))
		  {
			  
			    $query = $this->db->select("id_master") //check id booking pada table_parent 
									->where('id_master',$txtmasterid)
									->get('tbl_master_pp');
				  if ($query->num_rows() == 1)
				  {				
				    //master------------------------------
				  	$data = array( 'user_submission'=>$txtsub,
								   'header_desc'=>$txtdesc_detail, 								  
								   'id_vendor'=>$cbovendor,
								   'id_curr'=>$cbocur,
								//   'id_coa'=>$cbocoa,	
								   'flag_purchase' =>	$str_flag_purchase,							
								   'term_top'=>$txtterm,								
								   'remarks'=>$txtremarks,	
								   'status_send_aprove'=>$strsend_aproval,								
								   'gran_total'=>$txt_grand_total,	
								   'ppn'=>$txt_ppn,
								   'pph'=>$txt_pph,	
								   'gran_totalppn'=>$txt_grand_totalppn,	
								   'date_send_aproval' => $date_send	
									);					
					$this->db->where('id_master',$txtmasterid);
					$this->db->update('tbl_master_pp',$data); 
					
					
					//nyimpen session di array untuk session
					$buff=array('ses_noppnew'=> $txtmasterid,		//remarks			          		 			
								 'ses_usersubnew'=> $txtsub,
								 'ses_desc_detail'=> $txtdesc_detail,						 						
								 'ses_grandtotal'=>$txt_grand_total,
								 'ses_term'=>$txtterm,
								 'ses_strsend_aproval'=>$strsend_aproval ,
								 'ses_remarks'=>$txtremarks) ;					   
					$this->session->set_userdata($buff); //simpan di session untuk di tampilkan kedalam email
					//---------------------------------------------------------------------------	
					
					
					
					
					
					
					//detail---------------------------					
					  $id_det = $this->input->post('txtiddet');											
					  if ($id_det != "") {						 
							foreach($id_det AS $key => $val)
							{  							    
								if ($_POST['txtiddet'][$key]!=""){ //jika hanya edit row detail 
									 $result[1] = array(								   
									  "id_detail"     => $_POST['txtiddet'][$key],										
									  "desc"  		  => $_POST['txtdesc'][$key],						  									 									  "spec"  		  => $_POST['txtspecs'][$key],
									  //"po_reff"  	  => $_POST['txtporeff'][$key],
									  //"coa"  	      => $_POST['cbocoa_search'][$key],
									  //"no_act"  	  => $_POST['cboact_search'][$key],
									  "qty"  		  => str_replace(',','',$_POST["txtqty"][$key]),
									  "harga"  		  => str_replace(',','',$_POST["txtprice"][$key]),
									  "total"  		  => str_replace(',','',$_POST["txtamount"][$key]),
									  "tax_type"	  => $_POST['cbotax'][$key],	
									  "tax_typepph"	  => $_POST['cbotaxpph'][$key],									
					 				  "tax_detail"	  => str_replace(',','',$_POST["txttaxcharge"][$key]),
									  "tax_detailpph" => str_replace(',','',$_POST["txttaxchargepph"][$key]),
									  "flagadjust"    => '0',
					  				  "keterangan"    => 'Normal'							
									  );												
									  $this->db->update_batch('tbl_detail_pp',$result,'id_detail');
								}else{  //jika edit row detail dan juga addnew rowdetail	
								      echo $_POST['cbotax'][$key];
								      $result[1] = array(			
									  "id_master"     => $_POST['txtidmasdet'][$key],			  
									  "desc"  		  => $_POST['txtdesc'][$key],						  								     								  "spec"  		  => $_POST['txtspecs'][$key],
									  //"po_reff"  	  => $_POST['txtporeff'][$key],
									  //"coa"  	      => $_POST['cbocoa_search'][$key],
									  //"no_act"  	  => $_POST['cboact_search'][$key],
									  "qty"  		  => str_replace(',','',$_POST["txtqty"][$key]),
									  "harga"  		  => str_replace(',','',$_POST["txtprice"][$key]),
									  "total"  		  => str_replace(',','',$_POST["txtamount"][$key]),
									  "tax_type"	  => $_POST['cbotax'][$key],	
									  "tax_typepph"	  => $_POST['cbotaxpph'][$key],									
					 				  "tax_detail"	  => str_replace(',','',$_POST["txttaxcharge"][$key]),
									  "tax_detailpph" => str_replace(',','',$_POST["txttaxchargepph"][$key]),
									  "flagadjust"    => '0',
					  				  "keterangan"    => 'Normal',
									  "status"		  => '1' 					  			
									 );									 
								    $test= $this->db->insert_batch('tbl_detail_pp', $result);
								}
									
							} 
					     }
						 
					   //logedit-----------
						$this->load->helper('file');
						$strcomlog = $this->session->userdata('company');
						$strbranchlog = $this->session->userdata('name_branch');
						$strdeptlog = $this->session->userdata('dept');
						$txtdatelog = date("Y-m-d H:i:s");
						
					    $data = $txtmasterid.",".$strcomlog.",".$strbranchlog.",".$strdeptlog .",".$txtsub.",".$txtdatelog.",".$txtremarks.",".$txt_grand_totalppn.","."Action:edit \n";
					
					    $file_path ='./asset/log/logeppedit.txt';
						
						if(file_exists($file_path)){
							write_file($file_path,$data, "a+");
						}else{
							write_file($file_path,$data);
						}	 
						//endlogedit-----------------------------------
				  	  return true;
				  }	  
				  
				   
		  }else{
			  return false;
		  }
		  
	}
	
	function get_edit_pp()
	{		
	  $get_edit_nopo =$this->input->post('msg');
	  $flag_status_mas = "1" ;
			  
	  if (isset($get_edit_nopo) && trim($get_edit_nopo!=''))
	  {		
		  for ($i=0; $i < count($get_edit_nopo) ; $i++) { 	
			 $this->db->select("*")	;
			 $this->db->where('id_master', $get_edit_nopo[$i]);
			 $this->db->where('status',  $flag_status_mas);											  	
			 $result = $this->db->get('qv_head_pp_complite');							    				
		   }						
			 return $result->result() ;									  		  
	  }else{
		   return false;
	  }
		
	}
	
	function get_edit_detail_pp()
	{		
		 
      $get_edit_nopo =$this->input->post('msg');	  		 				    							   				      	      $flag_status_det = "1" ;
	    	   					  				   			  			   	 		  
	  if (isset($get_edit_nopo) && trim($get_edit_nopo!=''))
	  {		
		  for ($i=0; $i < count($get_edit_nopo) ; $i++) { 	
			 $this->db->select("id_detail,id_master,desc,spec,qty,harga,total,po_reff,coa,no_act,tax_type,tax_typepph,tax_detail,tax_detailpph")	;
			 $this->db->where('id_master', $get_edit_nopo[$i]);	
			 $this->db->where('status', $flag_status_det);										  	
			 $result = $this->db->get('tbl_detail_pp');							    				
		   }						
			 return $result->result() ;							  		  
	  }else{
		  return false;
	  }
		
	}
							
	function update_send_flag_aproval()
	{
		  $strid_master =$this->input->post('msg'); 			  
		  $date_send = date("Y-m-d H:i:s");
		  $flag_aproval = "1" ;	// flag 1 jika status nya sudah terkirim sebagai aproval		  					    							   				   	   					  				   			  			   	
		  if (isset($flag_aproval) && trim($flag_aproval!=''))
		  {		
		  	  for ($i=0; $i < count($strid_master) ; $i++) { 				
			    $query = $this->db->select("id_master") 
									->where('id_master',$strid_master[$i])
									->get('tbl_master_pp');
				  if ($query->num_rows() == 1)
				  {	
				  
				    //session for email------------------------------
					  $buff=array('ses_noppnew'=> $strid_master[$i]);			 			 												
					  $this->session->set_userdata($buff);							
					//end session for email--------------------------
				  									    
				  	$data = array(
									'status_send_aprove'=>$flag_aproval,
									'date_send_aproval'=>$date_send 
								 );					
					$this->db->where('id_master',$strid_master[$i]);
					$this->db->update('tbl_master_pp',$data); 															
				  	return true;
									   									 						 
				  } 
			  }
			  
		  }else{
			  return false;
		  }
		
	}
	
	function delete_with_edit_flag() {				
		  $delete = $this->input->post('msg');		  				  		 
		  $flag_del = '0';
		  
		  if(isset($delete) && trim($delete!=''))
		  {	   
			  // --------delete table master dan detail pp menggunakan flag status
			    
				for ($i=0; $i < count($delete) ; $i++) { 				    
				    //array variable flag del
					
				    $data = array('status'=>$flag_del);	
					
					//master table 													
					$this->db->where('id_master',$delete[$i]);
					$this->db->update('tbl_master_pp',$data); 						   				   	   
				   
				    //master detail 													
				    $this->db->where('id_master', $delete[$i]);
				    $this->db->update('tbl_detail_pp',$data);	
					
				    //create log-----------------------------------
					$this->load->helper('file');
					$txtmasterid = $delete[$i] ;
					$strcomlog = $this->session->userdata('company');
					$strbranchlog = $this->session->userdata('name_branch');
					$strdeptlog = $this->session->userdata('dept');
					$txtsub = $this->session->userdata('name');
					$txtdate = date("d-m-Y H:i:s");
					
					$data = $txtmasterid.",".$strcomlog.",".$strbranchlog.",".$strdeptlog .",".$txtsub.",".$txtdate.","."Action:Delete \n";
					
					$file_path ='./asset/log/logeppdelete.txt';
					if(file_exists($file_path)){
						//write_file($file_path, $data, 'a');
						write_file($file_path, $data , "a+");
					}else{
						write_file($file_path, $data);
					}
					 
				 //end log--------------------------------------------					   				   	   					  				  
			    }
			   					  	   
			   return true;
			   
			  
		  }else{
			   return false;	  			  		  
		  }
	}
	
	
	/*function delete_checked_pp() {				
		  $delete = $this->input->post('msg');		  				  		 
		  
		  if(isset($delete) && trim($delete!=''))
		  {	   
			 // --------delete table master dan detail pp langsung ke table
			    
				for ($i=0; $i < count($delete) ; $i++) { 				    
				   $this->db->where('id_master', $delete[$i]);
				   $this->db->delete('tbl_master_pp');						   				   	   
				   
				   $this->db->where('id_master', $delete[$i]);
				   $this->db->delete('tbl_detail_pp');						   				   	   			  				   
			    }
			   					  	   
			   return true;
		  }else{
			   return false;	  			  		  
		  }
	} */
	
	
	
	function search_data() 	// fungsi cari data pp model
	{
	   $cari = $this->input->post('txtcari');
	   $kategory =  $this->input->post('cbostatus');	  
	   
	   $ses_company = $this->session->userdata('id_company');
	   $ses_id_dept = $this->session->userdata('id_dept');
	   $ses_branch_id = $this->session->userdata('branch_id');
	   $str_flagsend_aprove ="0" ;	   
	   
	   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
	   {
		   redirect('c_create_pp/c_create_pp');  //direct ke url add_menu_booking/add_menu_booking
	   }else{	
		   $this->load->database();		   		  
		   $this->db->where('id_company',$ses_company);
		   $this->db->where('id_dept',$ses_id_dept);
		   $this->db->where('branch_id',$ses_branch_id);
		   $this->db->where('status_send_aprove', $str_flagsend_aprove);
		   $this->db->like($kategory,$cari);
		   $result = $this->db->get('qv_head_pp_complite');		 
		   return $result->result(); 
	   }
	} 
	
	 
							    					 	 	 	    

//------------------------------------------------------------------------------------------------------------------		
    function m_mastergetid_pp()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('id_master,gran_total,ppn,pph,gran_totalppn,remarks');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_master_pp');		
		return $result->result();
		
	}	   	
	
	function m_getid_pp()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('id_master,desc,spec,coa,no_act,qty,harga,total,po_reff,tax_type,tax_typepph,tax_detail,tax_detailpph');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_detail_pp');		
		return $result->result();
		
	}

	public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}
				 
=======
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_create_pp extends CI_Model {	
				    
    public $db_tabel = 'qv_head_pp_complite';
	 
    public function tampil_add_pp(){ //tampil table untuk ngececk num_row						     	     		  
	     $ses_id_dept = $this->session->userdata('id_dept'); //manggili session id 
		 $ses_id_company = $this->session->userdata('id_company');  	
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 
		 $short_company = $this->session->userdata('short');	 		 		
		 $ses_branch_short = $this->session->userdata('branch_short'); 
		 $ses_dept = $this->session->userdata('dept');	 		 
		 
		  		 		 
		 $send_aprove = "0" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject
		 $send_reject = "-1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		// $status_purchase = "1" ; //1 pp biasa, 2 pp unit , 3 pp part, 4 pp project
		  
		 $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('id_company',$ses_id_company);	
		 $this->db->where('branch_id',$ses_id_branch );		
		 $this->db->where('id_dept',$ses_id_dept);		 
		 $this->db->where('status',$status_delete);
		 $this->db->where('status_send_aprove',$send_aprove);		
		 $this->db->or_where('status_send_aprove',$send_reject);
		 $this->db->where('status',$status_delete);		
		 $this->db->where('short',$short_company);						 
		 $this->db->where('branch_short',$ses_branch_short);	
		 $this->db->where('dept',$ses_dept);		
		 $this->db->order_by('date_pp','desc');
		 $result = $this->db->get('qv_head_pp_complite');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	     $ses_id_dept = $this->session->userdata('id_dept'); 
		 $ses_id_company = $this->session->userdata('id_company');  	
		 $ses_id_branch = $this->session->userdata('branch_id');
		 $short_company = $this->session->userdata('short');	 		 		
		 $ses_branch_short = $this->session->userdata('branch_short'); 
		 $ses_dept = $this->session->userdata('dept');	 		 		
		// $status_purchase = "1" ; //1 pp biasa, 2 pp unit , 3 pp part, 4 pp project
		 
		 $send_aprove = "0" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject
		 $send_reject = "-1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  	     		 
	     $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('id_company',$ses_id_company);					
		 $this->db->where('branch_id',$ses_id_branch );	
		 $this->db->where('id_dept',$ses_id_dept);
		 $this->db->where('status',$status_delete);
		// $this->db->where('flag_purchase',$status_purchase);
		 $this->db->where('status_send_aprove',$send_aprove);	
		 $this->db->or_where('status_send_aprove',$send_reject);
		 $this->db->where('status',$status_delete);	
		 $this->db->where('short',$short_company);						 
		 $this->db->where('branch_short',$ses_branch_short);	
		 $this->db->where('dept',$ses_dept);				  		 
		 $this->db->order_by('date_pp','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_head_pp_complite')->result();		 		   
		 return $result ;
	}							  		
				    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------		



	/* function getppnumber(){	//old counter 
	 	 
		$strshort = $this->session->userdata('short');//short company
        $strdept = substr(($this->session->userdata('dept')),0,3);
		$strbranchshort = $this->session->userdata('branch_short');
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");	
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
		 		    
		 
 	    $q = $this->db->query("select MAX(RIGHT(id_master,4)) as code_max from tbl_master_pp where left(id_master,2)='".substr($strshort,0,2)."'");
								
			 $code = "";			
				if($q->num_rows()>0){
					foreach($q->result() as $cd){
						$tmp = ((int)$cd->code_max)+1;
					    $code = sprintf("%04s", $tmp);;
					}
				}else{
					$code = "0001";
				}		
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	    return substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$code ;			
    } */
	
	function getppnumber(){	// view counter number when add new
	 	 
		$strshort = $this->session->userdata('short');//short company
		$stridcom = $this->session->userdata('id_company');//id company
		$striddept = $this->session->userdata('id_dept');//id company
        $strdept = substr(($this->session->userdata('dept')),0,3);
		$strbranchshort = $this->session->userdata('branch_short');
		//$stryear = substr(date("Y"),-3,3);
		$stryear = substr(date("Y"),-2,2);
		$strmonth = date("m");			    
		//$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	   $qcounter = $this->db->query("select RIGHT(counter,3) as code_max from tbl_counter_ppno where id_dept='".$striddept."'");
									  	   
	   // $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); 	//check tanggal
	   $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strmonth."'"); 	//check bulan
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
				if($qcounter->num_rows()== 1):
					foreach($qcounter->result() as $cd):
						$tmp = ((int)$cd->code_max);
						$code = sprintf("%03s", $tmp);;
					endforeach;
					
					 if ($code== '000') :						
						 $code = "001";					 
					 endif;
					 
				 else:					  
					$code = "001";								
				 endif;		
		 else:		
		       $code = "001";					 
		 endif;
		 
	 //update tanggal ketable date--------------
	// $data = array('date_add'=>$strdates );																			
	 $data = array('date_add'=>$strmonth );																			
	 $this->db->update('tbl_date_compare',$data); 		 
	 //end--------------------
		 
	 //update tanggal ketable counter--------------
	  $data = array('counter'=> $code );	
	 // $this->db->where('id_company',$stridcom);															  									
	  $this->db->where('id_dept',$striddept);
	  $this->db->update('tbl_counter_ppno',$data); 		 
	 //end--------------------
		 
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	  //return substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$strdates."/".$code ;
	  return substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$code ;
	  //end------------------------------------------			 
    }
	
	function counterppnumber(){	//counter number when save data
	 	 
		$strshort = $this->session->userdata('short');//short company
		$stridcom = $this->session->userdata('id_company');//id company
		$striddept = $this->session->userdata('id_dept');//id company
        $strdept = substr(($this->session->userdata('dept')),0,3);
		$strbranchshort = $this->session->userdata('branch_short');
		//$stryear = substr(date("Y"),-3,3);
		$stryear = substr(date("Y"),-2,2);
		$strmonth = date("m");			    
		//$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	    $qcounter = $this->db->query("select RIGHT(counter,3) as code_max from tbl_counter_ppno where id_dept='".$striddept."'");
									  	   
	    //$qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); //check tanggal
		$qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strmonth."'"); 	//check bulan
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
					if($qcounter->num_rows()>0):
						foreach($qcounter->result() as $cd):
							$tmp = ((int)$cd->code_max)+1;
						    $code = sprintf("%03s", $tmp);;
						endforeach;
						 if ($code== '000') :						
						     $code = "001";					 
					 	 endif;
					 else:					
						 $code = "001";					 
					 endif;		
		 else:	
		       $code = "001";					 
		 endif;
		 
		 //update tanggal ketable date--------------
			 $data = array('date_add'=>$strmonth );																			
			 $this->db->update('tbl_date_compare',$data); 		 
		 //end--------------------
		 
		 //update tanggal ketable counter--------------
			 $data = array('counter'=> $code );	
			// $this->db->where('id_company',$stridcom);															  									
			 $this->db->where('id_dept',$striddept);
			 $this->db->update('tbl_counter_ppno',$data); 		 
		 //end--------------------
		 
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	    //end------------------------------------------					 
	      return true;
    }
	
	function get_vendor()
	{
		 $id_company = $this->session->userdata('id_company');
	     $id_dept = $this->session->userdata('id_dept');
		 $status_del = "1"; //flag status delete pada table vendor
		 		 		 
	     $this->load->database();		
		 $this->db->select('id_vendor,vendor');		
		 $this->db->where('id_company',$id_company);
		 $this->db->where('id_dept',$id_dept);
		 $this->db->where('status',$status_del);
		 $result = $this->db->get('tbl_vendor');			     		 
		 return $result->result();	 	
	}
	
	function get_currency()
	{		
	     $this->load->database();		
		 $this->db->select('id_curr,currency');				
		 $result = $this->db->get('tbl_curr');			     		 
		 return $result->result();	 	
	}	
	 
	function get_purchase_type()
	{	
	     $id_user = $this->session->userdata('id');
		 $query =$this->db->query("select status_pur,id from tbl_user where id ='".$id_user."'") ;	         $hasil =$query->result();
		 
		 foreach($hasil as $row) {
			 $cath_status_pur = $row->status_pur;
		 }
		 
		  $saparate_typepur = explode(",",$cath_status_pur );			 					 		 		   	      
		  $this->db->select('*');
			 if (count($saparate_typepur)=="5")	{	
									 
				 $this->db->where('id_flagpur',$saparate_typepur[0]);				
				 $this->db->or_where('id_flagpur',$saparate_typepur[1]);	
				 $this->db->or_where('id_flagpur',$saparate_typepur[2]);			 
				 $this->db->or_where('id_flagpur',$saparate_typepur[3]);		  								
				 $this->db->or_where('id_flagpur',$saparate_typepur[4]);	
			}else{
				if (count($saparate_typepur)=="4")	{				
					 $this->db->select('id_flagpur','flag,type_purchase');	 
					 $this->db->where('id_flagpur',$saparate_typepur[0]);				
					 $this->db->or_where('id_flagpur',$saparate_typepur[1]);	
					 $this->db->or_where('id_flagpur',$saparate_typepur[2]);			 
					 $this->db->or_where('id_flagpur',$saparate_typepur[3]);		  											
				}else{
					if (count($saparate_typepur)=="3")	{	
				
						 $this->db->select('id_flagpur','flag,type_purchase');	 
						 $this->db->where('id_flagpur',$saparate_typepur[0]);				
						 $this->db->or_where('id_flagpur',$saparate_typepur[1]);	
						 $this->db->or_where('id_flagpur',$saparate_typepur[2]);			 					
					}else{	
						if (count($saparate_typepur)=="2")	{	
											
						   $this->db->select('id_flagpur','flag,type_purchase');	 
						   $this->db->where('id_flagpur',$saparate_typepur[0]);										                           $this->db->or_where('id_flagpur',$saparate_typepur[1]);								 				
						}else{								
							$this->db->select('id_flagpur','flag,type_purchase');
							$this->db->where('id_flagpur',$saparate_typepur[0]);											 			    	    }
					}
				}
			}				   			 									
		 $result = $this->db->get('tbl_type_purchase'); 		     		 
		 return $result->result();	  
	}	
	
	function get_coa()
	{
		$id_company = $this->session->userdata('id_company');
		$result = $this->db->query("select * from tbl_coa where id_company = '".$id_company."'");
		return $result->result();	 	
	} 
		
	function insert_master_and_detail()	
	{ 
	     //-------------------Checck Duplicate NOPP / Idmaster		 
		 $ponumber = $this->input->post('txtnopp'); 		 		 
		 $query2=   $this->db->select('*')
						 ->where('id_master',$ponumber)	 //Checck Duplicate NOpo / Idmaster					
						 ->get('tbl_master_pp');		
		 
		 if ($query2->num_rows() == 1) // Jika ketemu buat NOPP baru.		 
		 {					 			  			   
			$strshort = $this->session->userdata('short');//short company
			$stridcom = $this->session->userdata('id_company');//id company
			$striddept = $this->session->userdata('id_dept');//id company
			$strdept = substr(($this->session->userdata('dept')),0,3);
			$strbranchshort = $this->session->userdata('branch_short');
			//$stryear = substr(date("Y"),-3,3);
			$stryear = substr(date("Y"),-2,2);
			$strmonth = date("m");			    
			//$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	        $qcounter = $this->db->query("select RIGHT(counter,3) as code_max from tbl_counter_ppno where id_dept='".$striddept."'");
									  	   
	       // $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); //check tanggal
		   $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strmonth."'"); 	//check bulan
	   
			$code = ""; //empty code
			 if ($qdate->num_rows()== 1) :			
						if($qcounter->num_rows()>0):
							foreach($qcounter->result() as $cd):
								$tmp = ((int)$cd->code_max)+1;
								$code = sprintf("%03s", $tmp);;
							endforeach;
							 if ($code== '000') :						
								 $code = "001";					 
							 endif;
						 else:					
							 $code = "001";					 
						 endif;		
			 else:	
				   $code = "001";					 
			 endif;
		 
		    //update tanggal ketable date--------------
			  $data = array('date_add'=>$strmonth );																			
			  $this->db->update('tbl_date_compare',$data); 		 
		   //end--------------------
		 
		     //update tanggal ketable counter--------------
			  $data = array('counter'=> $code );	
			  $this->db->where('id_company',$stridcom);															  									
			  $this->db->update('tbl_counter_ppno',$data); 
			  //buat NOPP Baru jika duplicate						  
		      return  substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$code;						
		 }else{					     
		      //Ambil NOPP yang ada di inputbox posting	 jika tidak duplicate			 
			  $txtmasterid = $this->input->post('txtnopp'); 	  	   	  
	     }
		      //----mengambil data dari inputbox/textbox lalu di simpan didalam variable
			  $ses_idcom = $this->session->userdata('id_company');
			  $ses_dept = $this->session->userdata('id_dept');
			  $ses_branch = $this->session->userdata('branch_id');	  
			  $strhead_user= $this->session->userdata('head_user');
			  $str_emailhead_user= $this->session->userdata('email_head');
			  $str_emailcc_user= $this->session->userdata('email_cc');
			  $str_emailfc = $this->session->userdata('email_fc');
			  $str_emailbod = $this->session->userdata('email_bod');
			  $cbovendor = $this->input->post('cbovendor'); 
			  $cbocoa = $this->input->post('cbocoa'); 
			  $txtsub	= $this->input->post('txtsub');	
			  $txt_desc_detail = $this->input->post('txtdesc[0]'); //*from description detail	  		  		  	  		   	  
			  $txtdate = date("Y-m-d");
			  
			  $txtemail =  $this->input->post('txtmail'); 	 	  
			  $txtphone = $this->session->userdata('phone'); 			
			  $idcurr = $this->input->post('cbocur');
			  
			  //---------------------unformat grandtotal---------
			  $txt_grand_total =$this->input->post('result'); 
			  if ( strstr( $txt_grand_total, ',' ) ) $txt_grand_total = str_replace( ',','', $txt_grand_total );  
			  //-------------------------------------
			  
			   //---------------------unformat ppn---------
			  $txt_ppn =$this->input->post('txtppn'); 
			  if ( strstr( $txt_ppn, ',' ) ) $txt_ppn = str_replace( ',','', $txt_ppn );  
			  //-------------------------------------
			  
			    //---------------------unformat ppn---------
			  $txt_pph =$this->input->post('txtpph'); 
			  if ( strstr( $txt_pph, ',' ) ) $txt_pph = str_replace( ',','', $txt_pph );  
			  //-------------------------------------
			  
			   //---------------------unformat grandtotalppn---------
			  $txt_grand_totalppn =$this->input->post('gpppn'); 
			  if ( strstr( $txt_grand_totalppn, ',' ) ) $txt_grand_totalppn = str_replace( ',','', $txt_grand_totalppn );  
			  //-------------------------------------
			  			
			  
			  $txtterm = $this->input->post('txtterm'); 				
			  $straprove_head = '0';
			  $straprove_fc = '0';
			  $straprove_purchasing = '0';
			  $straprove_bod = '0';	
			  $str_flag_fpb = '0';	
			  $str_flag_print_fpb = '0';	
			  $str_flag_bpk='0';
			  $str_flag_print_bpk = '0';	
			  $str_flag_purchase = $this->input->post('cbotype_pur');  //'1'; //1  = purchas umum		
			  $status_aktif = '1';
			  			 			  	  	  	  	  	  	  			  			  
			 /* if($this->input->post('btnsave')){ //Check apakah dari button save atau dari button send
			     $strsend_aproval = '0'; //flag 0 hanya disampan saja
				 $date_send ="";
			  }else{
			  	 $strsend_aproval = '1'; //flag 1 disimpan sekaligus di kirim.
				 $date_send = date("Y-m-d");
			  } */
			  
			  $strsend_aproval = '0'; 
			  $date_send ="";
			  	
			  $strremrks = $this->input->post('txtremarks');
								
									 		  				 
		  
			//---------------------------------------------- insert tbl_master_pp		      
			
			$data=array('id_master'=>$txtmasterid,
						'id_company'=>$ses_idcom, 
						'id_dept'=>$ses_dept,
						'branch_id'=>$ses_branch,
						'id_vendor'=>$cbovendor, 
						'user_submission'=>$txtsub,
						'head_user'=>$strhead_user,
						'header_desc'=>$txt_desc_detail, //*from description detail
						'date_pp'=>$txtdate,
						'email_pp'=>$txtemail,
						'email_head_pp'=>$str_emailhead_user,
						'email_cc_pp'=>$str_emailcc_user,
						'email_fc_pp'=>$str_emailfc,
						'email_bod_pp'=>$str_emailbod,
						'phone_pp'=>$txtphone,																													
						'gran_total'=>$txt_grand_total,	
						'ppn'=>$txt_ppn,
						'pph'=>$txt_pph,	
						'gran_totalppn'=>$txt_grand_totalppn,	
						'id_coa'=>$cbocoa,							
						'term_top'=>$txtterm,
						'id_curr' =>$idcurr,								
						'aprove_head'=>$straprove_head,
						'approve_purchasing' =>$straprove_purchasing,
						'aprove_fc'=>$straprove_fc,									
						'aprove_bod'=>$straprove_bod,							
						'status'=>$status_aktif,						
						'status_send_aprove'=> $strsend_aproval,
						'remarks'=> $strremrks,
						'flag_fpb'=> $str_flag_fpb,
						'flag_print_fpb'=> $str_flag_print_fpb,	
						'flag_bpk' => $str_flag_bpk,
						'flag_print_bpk'=> $str_flag_print_bpk,	
						'flag_purchase' =>	$str_flag_purchase,						
						'date_send_aproval' => $date_send,
						'counter_reprint' => "0"									
						);					
										
			$this->db->insert('tbl_master_pp',$data);	
			
			 //nyimpen session di array untuk session
			$buff=array('ses_noppnew'=> $txtmasterid,			 			 
						 'ses_usersubnew'=> $txtsub,
						 'ses_desc_detail'=> $txt_desc_detail,						 						
						 'ses_grandtotal'=>$txt_grand_total,
						 'ses_term'=>$txtterm,
						 'ses_strsend_aproval'=>$strsend_aproval ,
						 'ses_remarks'=>$strremrks) ;					   
		    $this->session->set_userdata($buff); //simpan di  dalam session untuk di tampilakan kedalam email
			//---------------------------------------------------------------------------					

			//------------------detail------------------------------------------------													 				
			$desc = $this->input->post('txtdesc');										
			$result = array();					
				foreach($desc AS $key => $val)
				{					 											  
					 $result[1] = array(			
					  "id_master"     => $_POST['txtidmasdet'][$key],			  
					  "desc"  		  => $_POST['txtdesc'][$key],						  
					  "spec"  		  => $_POST['txtspecs'][$key],
					  "po_reff"  	  => $_POST['txtporeff'][$key],
					  "coa"  	      => $_POST['cbocoa_search'][$key],
				      "no_act"  	  => $_POST['cboact_search'][$key],
					  "qty"  		  => str_replace(',','',$_POST["txtqty"][$key]),
					  "harga"  		  => str_replace(',','',$_POST["txtprice"][$key]),
					  "total"  		  => str_replace(',','',$_POST["txtamount"][$key]),
					  "tax_type"	  => $_POST['cbotax'][$key],	
					  "tax_typepph"	  => $_POST['cbotaxpph'][$key],									
					  "tax_detail"	  => str_replace(',','',$_POST["txttaxcharge"][$key]),
					  "tax_detailpph" => str_replace(',','',$_POST["txttaxchargepph"][$key]),
					  "flagadjust"    => '0',
					  "keterangan"    => 'Normal',
					  "status"		  => $_POST['flagstat'][$key] 					  			
					 );
					 
				    $this->db->insert_batch('tbl_detail_pp', $result);
				} 					
				return true;			     					
		 
	}
	
	
	function edit_pp()	
	{
		//master---------------------------------------post		  
	 	  $txtmasterid = $this->input->post('txtnopp'); 	
		  $txtsub	= $this->input->post('txtsub');		
		  $cbovendor = $this->input->post('cbovendor'); 	
		  $cbocur = $this->input->post('cbocur'); 		
		  $cbocoa = $this->input->post('cbocoa'); 
		  $txtterm = $this->input->post('txtterm');	 
		  $txtremarks = $this->input->post('txtremarks'); 			 		 
		  $txtdesc_detail = $this->input->post('txtdesc[0]');				  
		  
		  //---remove row detail pp from fom_edit pp array format ------------		  
		   $get_iddet_remove_row = $this->input->post('txtiddet_buff') ;
		
	       if(isset($get_iddet_remove_row) && trim($get_iddet_remove_row!='')){					 			   
			   $stsrconvert_iddet = explode(",",$get_iddet_remove_row) ;				   
			   //print_r($stsrconvert_iddet) ;
			   //echo count($stsrconvert_iddet);			   
			   $flag_remove = "0";		   			   
			   for ($intiddet=0; $intiddet < count($stsrconvert_iddet) ; $intiddet++)
			   {				   									   				   	   
					//table detail 								
					$data = array('status'=>$flag_remove);	
										
					$this->db->where('id_detail', $stsrconvert_iddet[$intiddet]);
					$this->db->update('tbl_detail_pp',$data);						   	 				  
			   }  			
		   }		  
		  //end---------------------------------------------------------------------------
		  
		  
		  
		  //---------------------unformat grandtotal---------
			  $txt_grand_total =$this->input->post('result'); 
			  if ( strstr( $txt_grand_total, ',' ) ) $txt_grand_total = str_replace( ',','', $txt_grand_total );  
			  //-------------------------------------
			  
			   //---------------------unformat ppn---------
			  $txt_ppn =$this->input->post('txtppn'); 
			  if ( strstr( $txt_ppn, ',' ) ) $txt_ppn = str_replace( ',','', $txt_ppn );  
			  //-------------------------------------
			  
			    //---------------------unformat ppn---------
			  $txt_pph =$this->input->post('txtpph'); 
			  if ( strstr( $txt_pph, ',' ) ) $txt_pph = str_replace( ',','', $txt_pph );  
			  //-------------------------------------
			  
			   //---------------------unformat grandtotalppn---------
			  $txt_grand_totalppn =$this->input->post('gpppn'); 
			  if ( strstr( $txt_grand_totalppn, ',' ) ) $txt_grand_totalppn = str_replace( ',','', $txt_grand_totalppn );  
			  //-------------------------------------
		  
		   //if($this->input->post('btnsave_edit')){ //Check apakah dari button save atau dari button send
			 $strsend_aproval = '0'; //flag 0 hanya disimpan saja
			 $date_send ="";
		   //}else{
			//  	 $strsend_aproval = '1'; //flag 1 disimpan sekaligus di kirim.
			//	 $date_send = date("Y-m-d");
		  // }		 		 
		 		 
		
		  $txtmasterid = $this->input->post('txtnopp'); 			 
		  if (isset($txtmasterid) && trim($txtmasterid!=''))
		  {
			  
			    $query = $this->db->select("id_master") //check id booking pada table_parent 
									->where('id_master',$txtmasterid)
									->get('tbl_master_pp');
				  if ($query->num_rows() == 1)
				  {				
				    //master------------------------------
				  	$data = array( 'user_submission'=>$txtsub,
								   'header_desc'=>$txtdesc_detail, 								  
								   'id_vendor'=>$cbovendor,
								   'id_curr'=>$cbocur,
								   'id_coa'=>$cbocoa,							
								   'term_top'=>$txtterm,								
								   'remarks'=>$txtremarks,	
								   'status_send_aprove'=>$strsend_aproval,								
								   'gran_total'=>$txt_grand_total,	
								   'ppn'=>$txt_ppn,
								   'pph'=>$txt_pph,	
								   'gran_totalppn'=>$txt_grand_totalppn,	
								   'date_send_aproval' => $date_send	
									);					
					$this->db->where('id_master',$txtmasterid);
					$this->db->update('tbl_master_pp',$data); 
					
					
					//nyimpen session di array untuk session
					$buff=array('ses_noppnew'=> $txtmasterid,					          		 			
								 'ses_usersubnew'=> $txtsub,
								 'ses_desc_detail'=> $txtdesc_detail,						 						
								 'ses_grandtotal'=>$txt_grand_total,
								 'ses_term'=>$txtterm,
								 'ses_strsend_aproval'=>$strsend_aproval ,
								 'ses_remarks'=>$txtremarks) ;					   
					$this->session->set_userdata($buff); //simpan di session untuk di tampilkan kedalam email
					//---------------------------------------------------------------------------	
					
					
					//detail---------------------------					
					  $id_det = $this->input->post('txtiddet');											
					  if ($id_det != "") {						 
							foreach($id_det AS $key => $val)
							{  							    
								if ($_POST['txtiddet'][$key]!=""){ //jika hanya edit row detail 
									 $result[1] = array(								   
									  "id_detail"     => $_POST['txtiddet'][$key],										
									  "desc"  		  => $_POST['txtdesc'][$key],						  									 									  "spec"  		  => $_POST['txtspecs'][$key],
									  "po_reff"  	  => $_POST['txtporeff'][$key],
									  "coa"  	      => $_POST['cbocoa_search'][$key],
									  "no_act"  	  => $_POST['cboact_search'][$key],
									  "qty"  		  => str_replace(',','',$_POST["txtqty"][$key]),
									  "harga"  		  => str_replace(',','',$_POST["txtprice"][$key]),
									  "total"  		  => str_replace(',','',$_POST["txtamount"][$key]),
									  "tax_type"	  => $_POST['cbotax'][$key],	
									  "tax_typepph"	  => $_POST['cbotaxpph'][$key],									
					 				  "tax_detail"	  => str_replace(',','',$_POST["txttaxcharge"][$key]),
									  "tax_detailpph" => str_replace(',','',$_POST["txttaxchargepph"][$key]),
									  "flagadjust"    => '0',
					  				  "keterangan"    => 'Normal'							
									  );												
									  $this->db->update_batch('tbl_detail_pp',$result,'id_detail');
								}else{  //jika edit row detail dan juga addnew rowdetail	
								      echo $_POST['cbotax'][$key];
								      $result[1] = array(			
									  "id_master"     => $_POST['txtidmasdet'][$key],			  
									  "desc"  		  => $_POST['txtdesc'][$key],						  								     								  "spec"  		  => $_POST['txtspecs'][$key],
									  "po_reff"  	  => $_POST['txtporeff'][$key],
									  "coa"  	      => $_POST['cbocoa_search'][$key],
									  "no_act"  	  => $_POST['cboact_search'][$key],
									  "qty"  		  => str_replace(',','',$_POST["txtqty"][$key]),
									  "harga"  		  => str_replace(',','',$_POST["txtprice"][$key]),
									  "total"  		  => str_replace(',','',$_POST["txtamount"][$key]),
									  "tax_type"	  => $_POST['cbotax'][$key],	
									  "tax_typepph"	  => $_POST['cbotaxpph'][$key],									
					 				  "tax_detail"	  => str_replace(',','',$_POST["txttaxcharge"][$key]),
									  "tax_detailpph" => str_replace(',','',$_POST["txttaxchargepph"][$key]),
									  "flagadjust"    => '0',
					  				  "keterangan"    => 'Normal',
									  "status"		  => '1' 					  			
									 );									 
								    $test= $this->db->insert_batch('tbl_detail_pp', $result);
								}
									
							} 
					     }
				  	  return true;
				  }
				  
				  
		  }else{
			  return false;
		  }
		  
	}
	
	function get_edit_pp()
	{		
	  
	  $get_edit_nopo =$this->input->post('msg');	 
	  $flag_status_mas = "1" ;
			  
	  if (isset($get_edit_nopo) && trim($get_edit_nopo!=''))
	  {		
		  for ($i=0; $i < count($get_edit_nopo) ; $i++) { 	
			 $this->db->select("*")	;
			 $this->db->where('id_master', $get_edit_nopo[$i]);
			 $this->db->where('status',  $flag_status_mas);											  	
			 $result = $this->db->get('qv_head_pp_complite');							    				
		   }						
			 return $result->result() ;									  		  
	  }else{
		   return false;
	  }
		
	}
	
	function get_edit_detail_pp()
	{		
		 
      $get_edit_nopo =$this->input->post('msg');	  		 				    							   				      $flag_status_det = "1" ;
	    	   					  				   			  			   	 		  
	  if (isset($get_edit_nopo) && trim($get_edit_nopo!=''))
	  {		
		  for ($i=0; $i < count($get_edit_nopo) ; $i++) { 	
			 $this->db->select("id_detail,id_master,desc,spec,qty,harga,total,po_reff,coa,no_act,tax_type,tax_typepph,tax_detail,tax_detailpph")	;
			 $this->db->where('id_master', $get_edit_nopo[$i]);	
			 $this->db->where('status', $flag_status_det);										  	
			 $result = $this->db->get('tbl_detail_pp');							    				
		   }						
			 return $result->result() ;							  		  
	  }else{
		  return false;
	  }
		
	}
							
	function update_send_flag_aproval()
	{
		  $strid_master =$this->input->post('msg'); 			  
		  $date_send = date("Y-m-d");
		  $flag_aproval = "1" ;	// flag 1 jika status nya sudah terkirim sebagai aproval		  					    							   				   	   					  				   			  			   	
		  if (isset($flag_aproval) && trim($flag_aproval!=''))
		  {		
		  	  for ($i=0; $i < count($strid_master) ; $i++) { 				
			    $query = $this->db->select("id_master") 
									->where('id_master',$strid_master[$i])
									->get('tbl_master_pp');
				  if ($query->num_rows() == 1)
				  {	
				  
				    //session for email------------------------------
					  $buff=array('ses_noppnew'=> $strid_master[$i]);			 			 												
					  $this->session->set_userdata($buff);							
					//end session for email--------------------------
				  									    
				  	$data = array(
									'status_send_aprove'=>$flag_aproval,
									'date_send_aproval'=>$date_send 
								 );					
					$this->db->where('id_master',$strid_master[$i]);
					$this->db->update('tbl_master_pp',$data); 															
				  	return true;
									   									 						 
				  } 
			  }
			  
		  }else{
			  return false;
		  }
		
	}
	
	function delete_with_edit_flag() {				
		  $delete = $this->input->post('msg');		  				  		 
		  $flag_del = '0';
		  
		  if(isset($delete) && trim($delete!=''))
		  {	   
			  // --------delete table master dan detail pp menggunakan flag status
			    
				for ($i=0; $i < count($delete) ; $i++) { 				    
				    //array variable flag del
					
				    $data = array('status'=>$flag_del);	
					
					//master table 													
					$this->db->where('id_master',$delete[$i]);
					$this->db->update('tbl_master_pp',$data); 						   				   	   
				   
				    //master detail 													
				    $this->db->where('id_master', $delete[$i]);
				    $this->db->update('tbl_detail_pp',$data);						   				   	   					  				  
			    }
			   					  	   
			   return true;
		  }else{
			   return false;	  			  		  
		  }
	}
	
	
	function delete_checked_pp() {				
		  $delete = $this->input->post('msg');		  				  		 
		  
		  if(isset($delete) && trim($delete!=''))
		  {	   
			 // --------delete table master dan detail pp langsung ke table
			    
				for ($i=0; $i < count($delete) ; $i++) { 				    
				   $this->db->where('id_master', $delete[$i]);
				   $this->db->delete('tbl_master_pp');						   				   	   
				   
				   $this->db->where('id_master', $delete[$i]);
				   $this->db->delete('tbl_detail_pp');						   				   	   			  				   
			    }
			   					  	   
			   return true;
		  }else{
			   return false;	  			  		  
		  }
	}
	
	
	
	function search_data() 	// fungsi cari data pp model
	{
	   $cari = $this->input->post('txtcari');
	   $kategory =  $this->input->post('cbostatus');	  
	   
	   $ses_company = $this->session->userdata('id_company');
	   $ses_id_dept = $this->session->userdata('id_dept');
	   $ses_branch_id = $this->session->userdata('branch_id');
	   $str_flagsend_aprove ="0" ;	   
	   
	   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
	   {
		   redirect('c_create_pp/c_create_pp');  //direct ke url add_menu_booking/add_menu_booking
	   }else{	
		   $this->load->database();		   		  
		   $this->db->where('id_company',$ses_company);
		   $this->db->where('id_dept',$ses_id_dept);
		   $this->db->where('branch_id',$ses_branch_id);
		   $this->db->where('status_send_aprove', $str_flagsend_aprove);
		   $this->db->like($kategory,$cari);
		   $result = $this->db->get('qv_head_pp_complite');		 
		   return $result->result(); 
	   }
	} 
	
	 
							    					 	 	 	    

//------------------------------------------------------------------------------------------------------------------		
    function m_mastergetid_pp()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('id_master,gran_total,ppn,pph,gran_totalppn,remarks');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_master_pp');		
		return $result->result();
		
	}	   	
	
	function m_getid_pp()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('id_master,desc,spec,coa,no_act,qty,harga,total,po_reff,tax_type,tax_typepph,tax_detail,tax_detailpph');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_detail_pp');		
		return $result->result();
		
	}

	public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}
				 
>>>>>>> 21afb4d3e228d34061b2e6c2558bc18fdbc9dbc1
}