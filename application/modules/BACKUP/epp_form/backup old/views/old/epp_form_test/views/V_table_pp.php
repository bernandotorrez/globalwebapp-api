<style>
.tbody{
    table-layout: fixed !important;
    word-wrap:break-word;
}
</style>
<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>
      
<?php   	
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>      
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>                        
                <?php  echo "Send Approval Successfully"	?>  
		  </div>		
<?php } ?>

<?php   	
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>                
<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>                        
			<?php  echo "Send Approval failed"	?>  
        </div>		      
<?php } ?>	
  
 <div class="widget-box">
    <div class="widget-header">
        <h5 class="widget-title"><?php echo " ".$header ;?></h5>
    </div>

    <div class="widget-body">
        <div class="widget-main">                
     
   
          
 <?php  echo form_open('epp_form/c_create_pp/multiple_submit',array('id' => 'form-tablepp'));  ?>     
 
   <p>                    
        <a href="<?php echo site_url('epp_form_test/c_test_pp/view_form_pp'); ?>" class="btn btn-success btn-round caddnew">Add New </a>
       <input id="btnedit" name="btnedit" type="submit" value="Edit"   class="btn btn-warning btn-default btn-round btn_edit" disabled="disabled"/>
         
       <input id="btndel" name="btndel" type="button" value="Delete"  class="btn btn-danger btn-round btn_delete" disabled="disabled"/>
             	  	
       <input id="btnsendtab" name="btnsendtab" type="submit" value="Send Approval"  class="btn btn-primary  btn-round btn_send" disabled="disabled" />     
    </p>

 
<div class="row">
 <div class="col-md-12 col-md-offset-0">   
	
		
        <table id="myTable" class="table table-striped table-bordered table-hover">
	      <thead align="center" >   
                <th style="text-align:center">                 
                <label class="pos-rel" >
                    <input type="checkbox" id="checkAll" class="ace ace-checkbox-1" />
                    <span class="lbl"></span>
                </label>
				</th>	       		          
                <th >PP No </th>
                <th >Company</th>
                <th >Dept</th>						
                <th  align="center" >Vendor</th>			                	
                <th >Date Created</th>
                <th >Header Desc</th>
                <th >Remarks</th>
                <th >Cur</th>               
                <th >Purchase Type</th>                
                <th >Total Cost</th>
                <th >PPN</th>
                <th >PPH</th>
                <th >Total + TAX</th>
                <th >Term</th>
                <th width="10%" align="center">Submission</th>				
                <th width="8%">Info</th>	
                <th width="5%" align="center">Description Detail</th>		
                <th width="5%">Attach_quo</th>	
               
           </thead>    
    </table> 
	
<div class="hr hr-18 dotted hr-double"></div>



    <div class="clearfix">
        <div class="pull-right tableTools-container"></div>
    </div>
    <div>
        <table id="dynamic-table" class="table table-striped table-bordered table-hover red">
            <thead>
                <tr>
                    <th class="center">
                        <label class="pos-rel">
                            <input type="checkbox" class="ace" />
                            <span class="lbl"></span>
                        </label>
                    </th>
                    <th >PP No </th>
                    <th >Company</th>
                    <th >Dept</th>						
                    <th >Vendor</th>			                	
                    <th >Date Created</th>
                   
                </tr>
            </thead>
            	<tbody>
                   <tr>
                    <td class="center">
                        <label class="pos-rel">
                            <input type="checkbox" class="ace" />
                            <span class="lbl"></span>
                        </label>
                    </td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    <td>test</td>
                    
                    
                   </tr>
                 </tbody> 
        </table>
     </div>
     
           	 

            
     </div>  
    </div> 
    </div>
 </div>
</div>     
  

<!-- Modal -->
        <div class="modal fade" id="ViewDetailModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Purchase</h4>
                    </div>
                    
                    <div class="modal-body">                      
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal -->  	


<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.reject',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("epp_form/c_create_pp/get_info_reject"); ?>';
		
		$("#myModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});


</script>

<script>
//kunci modal




//
//-----------------------------------------data table custome----
/*var tableUsers = $('#myTable').DataTable({
            searching:true,
			width:'100%',
			bAutoWidth: true,
  		    responsive : true,
            "sPaginationType": "full_numbers",
            "bPaginate": true,			
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true,
            "bProcessing": true,
			select: {style: 'multi'}
			
        });		*/
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){	
		$('#btnedit').prop("disabled", true);
		var table = $('#myTable').DataTable();
		var cells = table.cells( ).nodes();
		$( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));							
		   
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#btnedit').prop("disabled", false): $('#btnedit').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#btndel').prop("disabled", false): $('#btndel').prop("disabled", true);

});
//--------------------------------------------------------------------


//aktif button send email--------------------------------------------------------

var counterCheckesendeamail = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckesendeamail++ : counterCheckesendeamail--;
    counterCheckesendeamail == 1 ? $('#btnsendtab').prop("disabled", false): $('#btnsendtab').prop("disabled", true);
});

//end---------------------------------------------------------------

</script>

<script>
//get data from control to modal popup.
$(function(){
  $(document).on('click','.detail',function(e){
		var req_id = $(this).attr('req_id');  
		var url = '<?php echo site_url("epp_form/c_create_pp/get_idtrans_modal"); ?>';
		
		$("#ViewDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_pp:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		}); 			
		
   });	
});
</script>

<script>
$(function(){
    $(document).on('click','.btn_delete',function(e){
	  
	  if (confirm('Are you sure you want to cancel?'))
	  {
		  var url = "<?php echo base_url('epp_form/c_create_pp/do_delete_pp'); ?>";		 
		  var formData = new FormData(document.getElementById('form-tablepp'));	  		       
			  
		  $.ajax({
		   url:url,      
		   method:'POST',
		   //enctype: "multipart/form-data", 					
		   data: formData,										
		   processData: false,															
		   async: false,
		   processData: false,
		   contentType: false,		
		   cache : false,	
		   success:function(){ 	    
			   alert('Delete Successfully !!');   
			   location.reload();		  	               		  
		   }
		  });			  
	  }
	  
	});  	 
});
</script>

<script type="text/javascript">

$("#myTable").DataTable({
	/*ordering: true,*/
	bAutoWidth: false,
	processing: true,
	serverSide: true,
	"scrollY": 250,
	"scrollX": true,

	ajax: {
	  url: "<?php echo base_url('epp_form_test/C_test_pp/cath_data_fordatatables') ?>",
	  type:'POST',
	},
		
	 "columnDefs": [
    	{ 
		  "width": "50%", "targets": [0,1,2],
		  //"targets": 0,"orderable": false
		   "targets": 0,"orderable": [[6, 'desc']]
		   'targets': [0],
		   'orderable': false,
		   
		}
  	 ],
	 

	 //end----------------------------------------			
	 
});

</script>

	<script type="text/javascript">
			jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					"aoColumns": [
					  { "bSortable": false },
					  null, null,null, null, null,
					  { "bSortable": false }
					],
					"aaSorting": [],
					
					
					//"bProcessing": true,
			        //"bServerSide": true,
			        //"sAjaxSource": "http://127.0.0.1/table.php"	,
			
					//,
					//"sScrollY": "200px",
					//"bPaginate": false,
			
					//"sScrollX": "100%",
					//"sScrollXInner": "120%",
					//"bScrollCollapse": true,
					//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
					//you may want to wrap the table inside a "div.dataTables_borderWrap" element
			
					//"iDisplayLength": 50
			
			
					select: {
						style: 'multi'
					}
			    } );
			
				
				
				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable, {
					buttons: [
					  {
						"extend": "colvis",
						"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
						"className": "btn btn-white btn-primary btn-bold",
						columns: ':not(:first):not(:last)'
					  },
					  {
						"extend": "copy",
						"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					  {
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					  {
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					  {
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					  {
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'This print was produced using the Print button for DataTables'
					  }		  
					]
				} );
				myTable.buttons().container().appendTo( $('.tableTools-container') );
				
				//style the message box
				var defaultCopyAction = myTable.button(1).action();
				myTable.button(1).action(function (e, dt, button, config) {
					defaultCopyAction(e, dt, button, config);
					$('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
				});
				
				
				var defaultColvisAction = myTable.button(0).action();
				myTable.button(0).action(function (e, dt, button, config) {
					
					defaultColvisAction(e, dt, button, config);
					
					
					if($('.dt-button-collection > .dropdown-menu').length == 0) {
						$('.dt-button-collection')
						.wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
						.find('a').attr('href', '#').wrap("<li />")
					}
					$('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
				});
			
				////
			
				setTimeout(function() {
					$($('.tableTools-container')).find('a.dt-button').each(function() {
						var div = $(this).find(' > div').first();
						if(div.length == 1) div.tooltip({container: 'body', title: div.parent().text()});
						else $(this).tooltip({container: 'body', title: $(this).text()});
					});
				}, 500);
				
				
				
				
				
				myTable.on( 'select', function ( e, dt, type, index ) {
					if ( type === 'row' ) {
						$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', true);
					}
				} );
				myTable.on( 'deselect', function ( e, dt, type, index ) {
					if ( type === 'row' ) {
						$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
					}
				} );
			
			
			
			
				/////////////////////////////////
				//table checkboxes
				$('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
				
				//select/deselect all rows according to table header checkbox
				$('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function(){
					var th_checked = this.checked;//checkbox inside "TH" table header
					
					$('#dynamic-table').find('tbody > tr').each(function(){
						var row = this;
						if(th_checked) myTable.row(row).select();
						else  myTable.row(row).deselect();
					});
				});
				
				//select/deselect a row when the checkbox is checked/unchecked
				$('#dynamic-table').on('click', 'td input[type=checkbox]' , function(){
					var row = $(this).closest('tr').get(0);
					if(this.checked) myTable.row(row).deselect();
					else myTable.row(row).select();
				});
			
			
			
				$(document).on('click', '#dynamic-table .dropdown-toggle', function(e) {
					e.stopImmediatePropagation();
					e.stopPropagation();
					e.preventDefault();
				});
				
				
				
				//And for the first simple table, which doesn't have TableTools or dataTables
				//select/deselect all rows according to table header checkbox
				var active_class = 'active';
				$('#simple-table > thead > tr > th input[type=checkbox]').eq(0).on('click', function(){
					var th_checked = this.checked;//checkbox inside "TH" table header
					
					$(this).closest('table').find('tbody > tr').each(function(){
						var row = this;
						if(th_checked) $(row).addClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', true);
						else $(row).removeClass(active_class).find('input[type=checkbox]').eq(0).prop('checked', false);
					});
				});
				
				//select/deselect a row when the checkbox is checked/unchecked
				$('#simple-table').on('click', 'td input[type=checkbox]' , function(){
					var $row = $(this).closest('tr');
					if($row.is('.detail-row ')) return;
					if(this.checked) $row.addClass(active_class);
					else $row.removeClass(active_class);
				});
			
				
			
				/********************************/
				//add tooltip for small view action buttons in dropdown menu
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				
				//tooltip placement on right or left
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					//var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
				
				
				
				
				/***************/
				$('.show-details-btn').on('click', function(e) {
					e.preventDefault();
					$(this).closest('tr').next().toggleClass('open');
					$(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');
				});
				/***************/
				
				
				
				
				
				/**
				//add horizontal scrollbars to a simple table
				$('#simple-table').css({'width':'2000px', 'max-width': 'none'}).wrap('<div style="width: 1000px;" />').parent().ace_scroll(
				  {
					horizontal: true,
					styleClass: 'scroll-top scroll-dark scroll-visible',//show the scrollbars on top(default is bottom)
					size: 2000,
					mouseWheelLock: true
				  }
				).css('padding-top', '12px');
				*/
			
			
			})
		</script>