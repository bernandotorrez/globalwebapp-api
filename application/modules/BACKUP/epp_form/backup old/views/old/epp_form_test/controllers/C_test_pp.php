<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_test_pp extends MY_Controller
{
  public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('M_test_pp','',TRUE);
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_test_pp->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}
	}

  public function index()
  {
		$this->show_table(); //manggil fungsi show_table
  }

  public function show_table()
  {
	  $tampil_table_pp= $this->M_test_pp->tampil_add_pp()->result();
	  $total_rows =$this->M_test_pp->tampil_add_pp()->num_rows();


	  if ($tampil_table_pp)
		{
		$dept  = $this->session->userdata('dept') ;
		$branch  = $this->session->userdata('name_branch') ;
		$company = $this->session->userdata('company') ;
		$data['header'] ="Purchase"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$data['pp_view'] = $tampil_table_pp;
		$data['ceck_row'] = $total_rows;
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.
		$data['show_view'] = 'epp_form_test/V_table_pp';
		$this->load->view('dashboard/Template',$data);
	  }else{
	    $dept  = $this->session->userdata('dept') ;
		$branch  = $this->session->userdata('name_branch') ;
		$company = $this->session->userdata('company') ;
		$data['header'] ="Purchase"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
	    $data['ceck_row'] = $total_rows;
		$data['pp_view'] = $tampil_table_pp;
		$data['pesan'] = 'Data PP table is empty';
		$data['show_view'] = 'epp_form_test/V_table_pp';
		$this->load->view('dashboard/Template',$data);
	  }
  }

public function get_info_reject() {
	     $id_master = $this->input->post('idmaster',TRUE); //
	     $query = $this->db->query("select reason_reject from tbl_master_pp where id_master ='".$id_master."'");

		 if($query->num_rows() >0){
			 foreach($query->result() as $row);
			 {
			   echo $row->reason_reject ;
			 }
		 }
    }


	 public function view_form_pp()
	{

	  $dept  = $this->session->userdata('dept') ;
	  $branch  = $this->session->userdata('name_branch') ;
	  $company = $this->session->userdata('company') ;

	  $data['header'] ="Purchase"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
	  $data['tampil_pp']=$this->M_test_pp->get_edit_pp();
      $data['tampil_det_pp']= $this->M_test_pp->get_edit_detail_pp();
      $data['tampil_vendor']=$this->M_test_pp->get_vendor();
	  $data['tampil_curr']=$this->M_test_pp->get_currency();
	 // $data['tampil_coa']=$this->M_test_pp->get_coa();
	  $data['tampil_type_purchase']=$this->M_test_pp->get_purchase_type();
	  $data['ponumber']=$this->M_test_pp->getppnumber();
	  $data['show_view']='epp_form_test/V_form_addnew_pp';
	  $this->load->view('dashboard/Template',$data);
	}

	public function do_upload()
	{

		$str_com =$this->session->userdata("short");
		$str_dept=$this->session->userdata("id_dept");
		$str_rep_masterid = preg_replace('~[\\\\/:*?"<>|.]~','', $this->session->userdata("ses_noppnew") );
		$str_nopp  = $str_rep_masterid.'.pdf';  // generate file name dari no pp di tambah ".pdf"
		$str_path  = './asset/uploads/'.substr($str_com,0,2)."/".$str_dept;  // generate path upload
		$str_fullpath =	$str_path.'/'.$str_nopp;

		$config['upload_path'] = $str_path;
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '120000kb';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$config['overwrite'] = TRUE;
		$config['file_name']= $str_nopp; //file name nya

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('pesan_fail',$str_nopp.implode(" ",$error));
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$data_insert = array('attach_quo'=>$str_fullpath);
			$this->db->where('id_master',$this->session->userdata("ses_noppnew"));
			$this->db->update('tbl_master_pp',$data_insert);
			$this->session->set_flashdata('pesan_upload_succes',"1");
		}


	}


	public function do_upload_multi()
	{

		$number_of_files =sizeof($_FILES['userfile']['tmp_name']);

		$file =$_FILES['userfile'];

		$str_com =$this->session->userdata("short");
		$str_rep_masterid = preg_replace('~[\\\\/:*?"<>|.]~','', $this->session->userdata("ses_noppnew") );
		$str_nopp  = $str_rep_masterid.'.pdf';  // generate file name dari no pp di tambah ".pdf"
		$str_path  = './asset/uploads/'.substr($str_com,0,2);  // generate path upload
		$str_fullpath =	$str_path.'/'.$str_nopp;

		$config['upload_path'] = $str_path;
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '120000kb';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$config['overwrite'] = TRUE;

		for ($i=0; $i < $number_of_files; $i++){
		    //$config['file_name']= $str_nopp; //file name nya
			$_FILES['userfile']['name'] =$str_nopp.$i[$i];
			$_FILES['userfile']['type'] =$file['type'][$i];
			$_FILES['userfile']['tmp_name'] =$file['tmp_name'][$i];
			$_FILES['userfile']['error'] =$file['error'][$i];
			$_FILES['userfile']['size'] =$file['size'][$i];

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
		}


		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('pesan_fail',$str_nopp.implode(" ",$error));
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$data_insert = array('attach_quo'=>$str_fullpath);
			$this->db->where('id_master',$this->session->userdata("ses_noppnew"));
			$this->db->update('tbl_master_pp',$data_insert);
			$this->session->set_flashdata('pesan_upload_succes',"1");
		}


	}

	public function do_upload_update()
	{
		$str_dept=$this->session->userdata("id_dept");
		$str_com =$this->session->userdata("short");
		$str_rep_masterid = preg_replace('~[\\\\/:*?"<>|.]~','', $this->session->userdata("ses_noppnew") );

		$str_nopp  = $str_rep_masterid.'.pdf'; 	 // generate file name dari no pp di tambah ".pdf"
		$str_path  = './asset/uploads/'.substr($str_com,0,2)."/".$str_dept;    // generate path upload
		$str_fullpath =	$str_path.'/'.$str_nopp;

		$config['upload_path'] = $str_path;
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '120000kb';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$config['overwrite'] = TRUE;
		$config['file_name']= $str_nopp;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('pesan',$str_nopp.implode(" ",$error));
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$data_update= array('attach_quo'=>$str_fullpath);
			$this->db->where('id_master',$this->session->userdata("ses_noppnew"));
			$this->db->update('tbl_master_pp',$data_update);
			$this->session->set_flashdata('pesan_upload_succes',"1");
		}
	}

	public function do_upload_faktur()
	{
		//$company_cut =substr($this->session->userdata('short'),0,-1);
		//$id_dept =$this->session->userdata('id_dept');
		//$str_path  = 'asset/uploads/'.$company_cut."/".$id_dept;  // generate path upload
		$str_path = '/asset/uploads/ES/';
	    $config['upload_path'] = $str_path;
		$config['allowed_types'] = '*';
		$config['max_size']	= '12000kb';
		$config['overwrite'] = TRUE;
		$config['file_name']= 'data_part.csv'; //file name nya

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( !$this->upload->do_upload('fileSelect'))
		{
			 $error = array('error' => $this->upload->display_errors());
		     print_r($error);
		}else{
		   	 echo 'Upload file data_part.csv successfull';
			 $data = array('upload_data' => $this->upload->data());
			//redirect('c_create_pp_direct_part/c_crud_pp_direct_part');
		}

	}

	public function clear_excel()
	{
		$company_cut =substr($this->session->userdata('short'),0,-1);
		$id_dept =$this->session->userdata('id_dept');
	    $get_file = './asset/uploads/'.$company_cut."/".$id_dept.'/data_part.csv';

		if(file_exists($get_file)) :
		   unlink($get_file);
	       echo "File Excel Remove"  ;
		   //redirect('epp_form_test/c_create_pp/view_form_pp');
		else:
		   echo "Nothing Excel File"  ;
		endif;
	}

	public function do_insert_pp()
	{

			if ($this->M_test_pp->insert_master_and_detail())

		    {
			      $this->do_upload(); //manggil fungsi Upload File
				  if ($this->session->flashdata('pesan_upload_succes') !=""){
					  $str_upload_success = "& Upload Quotation";					  							  				 
				  }else{
					   $str_upload_success = "";
				  }

				  $this->M_test_pp->counterppnumber();  //add and manggil counter numberpp
				 // echo 'Insert P.P '. $str_upload_success.' Successfull..!!';
				  if ($this->session->userdata("ses_strsend_aproval")=="1")
					 {
						 $this->session->unset_userdata("ses_strsend_aproval");
					 }

			}else{

				  //echo 'Insert P.P '. $str_upload_success.' Failed..!!';
		    }
		}


public function show_form_edit()
 {
	$tampil_edit_pp= $this->M_test_pp->get_edit_pp();
	$tampil_edit_det_pp= $this->M_test_pp->get_edit_detail_pp();

	$tampil_vendor =$this->M_test_pp->get_vendor();
	$tampil_cur = $this->M_test_pp->get_currency();

    $dept  = $this->session->userdata('dept') ;
	$branch  = $this->session->userdata('name_branch') ;
	$company = $this->session->userdata('company') ;

	foreach ($tampil_edit_det_pp as $row)
	{
		 $data['ppnumber'] =$row->id_master;
	}

  	if ($tampil_edit_pp == null and $tampil_edit_det_pp ==null)
	{
	    $data['ceck_row'] = "0"; // jika tidak ketemu
	    $data['tampil_pp'] =$tampil_edit_pp;
		//$data['tampil_coa']=$this->M_test_pp->get_coa();
		$data['tampil_type_purchase']=$this->M_test_pp->get_purchase_type();
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
	    $data['pesan'] = 'Data Type Not Found';
	    $data['show_view']='epp_form_test/V_form_edit_pp';
	    $this->load->view('dashboard/Template',$data);
     }else{
	 	$data['ceck_row'] = "1"; // jika  ketemu
	    $data['ino'] = ""; // variable kosong buat nocounter perulangan detail
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$data['tampil_pp'] =$tampil_edit_pp;
		//$data['tampil_coa']=$this->M_test_pp->get_coa();
		$data['tampil_type_purchase']=$this->M_test_pp->get_purchase_type();
		$data['tampil_det_pp']=$tampil_edit_det_pp;
		$data['tampil_vendor']=	$tampil_vendor ;
	    $data['tampil_curr']= $tampil_cur;
		$data['show_view']='epp_form_test/V_form_edit_pp';
		$this->load->view('dashboard/Template',$data);
     }

 }

public function do_edit_pp() //lakukan submit edit data
{



		   if ($this->M_test_pp->edit_pp())
		   {
		      $this->do_upload_update(); //manggil fungsi Upload File
		   	     if ($this->session->flashdata('pesan_upload_succes') !=""){
				      $str_upload_success = "& Upload Quotation";
				  }else{
				  	  $str_upload_success = "";
				  }
				  $this->session->set_flashdata('pesan_succes','update P.P '. $str_upload_success.' Successfull..!!');
				  echo "Update Successfully"	;
		   }else{
				  echo "Update failed"	;
				 //$this->session->set_flashdata('pesan_succces','Data  Update Failed!!!!!! ....');
		   }

}


public function do_delete_pp()	 //lakukan submit delete data
{
	 if ($this->M_test_pp->delete_with_edit_flag())
	 {
	     echo "Delete Successfully";
		 //$this->session->set_flashdata('pesan','Delete Successfully....');
		 redirect('epp_form_test/c_create_pp');
	  }else{
		  //$this->session->set_flashdata('pesan','No Data Selected To Remove!!');
		  redirect('epp_form_test/c_create_pp');
	 }
}

public function do_search_data()  // fungsi cari data controler
 {
		$tampung_cari = $this->M_test_pp->search_data(); // manggil hasil cari di model

		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;
		   $branch  = $this->session->userdata('name_branch') ;
		   $company = $this->session->userdata('short') ;
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		   $data['ceck_row'] = "0"; // jika tidak ketemu
		   $data['pp_view'] =$tampung_cari;
		   $data['pesan'] = 'Data PP Not a found';
		   $data['show_view'] = 'epp_form_test/V_table_pp';
		   $this->load->view('dashboard/Template',$data);
		}else{
		    $dept  = $this->session->userdata('dept') ;
		    $branch  = $this->session->userdata('name_branch') ;
		    $company = $this->session->userdata('short') ;
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;

			$data['ceck_row'] = "1"; // jika  ketemu
			$data['pp_view'] =$tampung_cari;
			$data['show_view'] = 'epp_form_test/V_table_pp';
		    $this->load->view('dashboard/Template',$data);
		}
    }

public function send_flag_aproval()
{
	if ($this->M_test_pp->update_send_flag_aproval())
    {
		$this->kirim_email();
		redirect('epp_form_test/c_create_pp');
	}else{
		$this->session->set_flashdata('pesan','Sending Approval Failed');
		redirect('epp_form_test/c_create_pp');
	}
}



 public function kirim_email(){

		   $this->load->library('email');
		   //konfigurasi email
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T";
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;
		   //------google
		   //$config['smtp_host']    = 'ssl://smtp.googlemail.com';

		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';
		   $config['smtp_port']  = '5220';
		   //$config['smtp_port']  = '465'; //port gmail
		   $config['newline']    = "\r\n";
		   $config['mailtype']   ="html" ;
		   //$config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   //$config['smtp_pass']= "3mim@zd4!";
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5";

		   //--------------------------------

			$strponumber =$this->session->userdata("ses_noppnew");

			if ($strponumber != '')
			 {
				//Select untuk email master PP----------------------------
				$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,gran_total,ppn,pph,type_purchase,gran_totalppn,flag_purchase,remarks from qv_head_pp_complite where id_master ='".$strponumber."'");
			    if ($query->num_rows() > 0 ) {
					$data_email['str_pp_master'] = $query->result();

				    //Select untuk email deteail PP--------------------------------
					$query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."'and status = '1'");
					if ($query_detail->num_rows() > 0 ) {
					   $data_email['str_pp_detail'] = $query_detail->result();
					   $data_email['intno'] = ""; //untuk counter angka table

					   //simpan di dalam variable message untuk di kirim sebagai Content Email
					   $message = $this->load->view('epp_form_test/V_content_email',$data_email,true);

					}
			   }
			}

			$result = $this->email ;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");

		    //Exchange
			//konfigurasi pengiriman

			$sender_email = "helpdesk@eurokars.co.id";
			$sender_name = "Epurchasing Notification";

			//GMAIL-----
			//$sender_email = "epurchaseeuro@gmail.com";
			//$sender_name = "Epurchasing Eurokarsgroup";

		   //simpan session alamat email kedalam variable..
				$struseremail = $this->session->userdata('email');
				$strheademail = $this->session->userdata('email_head');
				$strccemail = $this->session->userdata('email_cc');
			//end-----------------------------------------

			//$to = 'brian.yunanda@eurokars.co.id';

			$to = $struseremail.",".$strheademail.",".$strccemail;

			$subject = "Request Approval -- Eurokars Motor Indonesia";
			$this->email->from($sender_email, $sender_name);
			$this->email->to($to);
			$this->email->subject($subject);

			//$this->email->message(print_r($message, true));

		    $this->email->message($message);// tanpa array

		     if ($this->email->send()) {
			    $this->session->set_flashdata('pesan_succes','Sending Approval Successfully');
			   /*redirect(current_url());
		     } else {
			    show_error($this->email->print_debugger()); */
		     }


			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent');
			 $this->session->unset_userdata('ses_grandtotal');
			 $this->session->unset_userdata('ses_term');
			 $this->session->unset_userdata('ses_remarks');


 }

public function multiple_submit() //fungsi submit dengan banyak button dalam satu form
{

if ($this->input->post('btnsave')){
    $this->do_insert_pp();
	}else{
	if ($this->input->post('btndel')){
		$this->do_delete_pp();
	}else{
		if ($this->input->post('btnedit')){
			$this->show_form_edit();
		}else{
			if ($this->input->post('btnsend')){
				$this->do_insert_pp();
				$this->kirim_email();
			}else{
				if ($this->input->post('btnsendtab')){
					$this->send_flag_aproval();
				}else{
					if ($this->input->post('btnsave_edit')){
					   $this->do_edit_pp();
					}else{
						if ($this->input->post('btnsend_edit')){
							$this->do_edit_pp();
							$this->kirim_email();
						}else{
							if ($this->input->post('btnremovepdf')){
								$this->do_del_quotation();
							}else{
								redirect('epp_form_test/c_create_pp');
							}
						}
					}
				}
			}
		}
	}
  }
}

public function get_idtrans_modal()
{
     $tampungmaster = $this->M_test_pp->m_mastergetid_pp();
	 $tampungdetail = $this->M_test_pp->m_getid_pp();

	 $no="1";
	 echo "<div style='overflow-x:auto;'>" ;
	 echo "<table class='table table-striped table-bordered table-hover'>";
	 echo  "<tr style='font-weight:bold; font-size:11px'>";
	 echo  "<td width='2%'>No</td>";
	 echo  "<td width='22%'>Description</td>";
	 echo  "<td width='22%'>Specs</td>";
	 echo  "<td width='10%'>P.O Reff</td>";
	 echo  "<td width='10%'>C.O.A</td>";
	 echo  "<td width='10%'>No Actifity</td>";
	 echo  "<td width='7%'>Qty</td>";
	 echo  "<td width='15%'>Prices</td>";
	 echo  "<td width='40%'>Total Prices</td>";
	 echo  "<td width='7%'>PPN</td>";
	 echo  "<td width='7%'>PPH</td>";
	 echo  "<td width='25%'>PPN Amount</td>";
	 echo  "<td width='25%'>PPH Amount</td>";
	 echo  "</tr> ";
		foreach ($tampungdetail as $row_jq) {
		    echo  "<tr style='font-size:12px'> ";
			echo '<td>'.$no++.'</td>';
			echo '<td>'.$row_jq->desc.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			echo '<td>'.$row_jq->po_reff.'</td>';
			echo '<td>'.$row_jq->coa.'</td>';
			echo '<td>'.$row_jq->no_act.'</td>';
			echo '<td>'.$row_jq->qty.'</td>';
			echo '<td>'.number_format($row_jq->harga,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->total,2,'.',',').'</td>';
			echo '<td>'.$row_jq->tax_type.'</td>';
			echo '<td>'.$row_jq->tax_typepph.'</td>';
			echo '<td>'.number_format($row_jq->tax_detail,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->tax_detailpph,2,'.',',').'</td>';
		    echo "</tr>";

		}

	 foreach ($tampungmaster as $row_jm) {
	     echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='2'>Remarks :</td>";
		 echo  "<td width='40%' colspan='11'>".$row_jm->remarks."</td>";
		 echo  "</tr> ";
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Sub Total :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_total,2,'.',',')."</td>";
		 echo  "</tr> ";
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPN Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->ppn,2,'.',',')."</td>";
		 echo  "</tr> ";
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPH Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->pph,2,'.',',')."</td>";
		 echo  "</tr> ";
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Total + Tax :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_totalppn,2,'.',',')."</td>";
		 echo  "</tr> ";
		 echo "</table>";
		 echo "</div>";
	 }
}

function cath_data_fordatatables(){

		$str_iddept = $this->session->userdata("id_dept");
		$str_submission = $this->session->userdata("name");
		$str_status_send ="0";
		$str_status_sendreject ="-1";
		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_master');
		$this->db->from('qv_head_pp_complite');
		$this->db->where('id_dept',$str_iddept);
		$this->db->where('user_submission',$str_submission );
		$this->db->where('status','1');
		$total = $this->db->count_all_results();

		//$total=$this->db->count_all_results("qv_vendor");
		//$total=$this->db->count_all_results("qv_head_pp_complite");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
			$this->db->where('id_dept',$str_iddept);
		    $this->db->where('user_submission',$str_submission );
			$this->db->where("status","1");
			$this->db->like("id_master",$search);
			$this->db->or_like("user_submission",$search);
			$this->db->where('id_dept',$str_iddept);
		    $this->db->where('user_submission',$str_submission );
			$this->db->where("status","1");
			$this->db->or_like("header_desc",$search);
			$this->db->where('id_dept',$str_iddept);
		    $this->db->where('user_submission',$str_submission );
			$this->db->where("status","1");
			$this->db->or_like("vendor",$search);
			$this->db->where('id_dept',$str_iddept);
		    $this->db->where('user_submission',$str_submission );
			$this->db->where("status","1");
		}

		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		$this->db->where("status_send_aprove",$str_status_send);
		$this->db->where('id_dept',$str_iddept);
		$this->db->where('user_submission',$str_submission );
		$this->db->where("status","1");
		$this->db->or_where("status_send_aprove",$str_status_sendreject);
		$this->db->where('id_dept',$str_iddept);
		$this->db->where('user_submission',$str_submission );
		$this->db->where("status","1");
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('date_pp','DESC');
		$query=$this->db->get('qv_head_pp_complite');


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/

		if($search!=""){
		$this->db->where("status","1");
		$this->db->where('id_dept',$str_iddept);
		$this->db->where('user_submission',$str_submission );
		$this->db->like("id_master",$search);
		$this->db->or_like("user_submission",$search);
		$this->db->where('id_dept',$str_iddept);
		$this->db->where('user_submission',$str_submission );
		$this->db->where("status","1");
		$this->db->or_like("header_desc",$search);
		$this->db->where('id_dept',$str_iddept);
		$this->db->where('user_submission',$str_submission );
		$this->db->where("status","1");
		$this->db->or_like("vendor",$search);
		$this->db->where('id_dept',$str_iddept);
		$this->db->where('user_submission',$str_submission );
		$this->db->where("status","1");
		$jum=$this->db->get('qv_head_pp_complite');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}



		foreach ($query->result_array() as $row_tbl) {

			 if ($row_tbl['status_send_aprove'] == "-1")
			 {
				   $info_aproval = '<label style="color:red">'."Reject".'</label>';
			 }else{
				   $info_aproval = "New Request";
			 }

			 If ($row_tbl['attach_quo'] != "")
			  {
			   $attach_quo = '<a href='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-pink btn-default btn-round ">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".'View'.'</a>' ;
			 }else{
				$attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';
			 }

			 $chk_idmaster ='<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value='.$row_tbl["id_master"].' class="ace" req_id_del='.$row_tbl["id_master"].' />
           <span class="lbl"></span> ';


		   $btn_view_detail = '<a href="#" class="btn btn-warning btn-default btn-round detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <span class="glyphicon glyphicon-list" aria-hidden="true"></span>Detail</a>';
			

											
											
										

			$output['data'][]=array($chk_idmaster,
									$row_tbl['id_master'],
									$row_tbl['short'],
									$row_tbl['dept'],
									$row_tbl['vendor'],
									date('d-m-Y', strtotime($row_tbl['date_pp'])),
									$row_tbl['header_desc'],
									substr($row_tbl['remarks'],0,25),
									$row_tbl['currency'],
									//$row_tbl['nomor_coa'],
									$row_tbl['type_purchase'],
									number_format($row_tbl['gran_total'],2,'.',','),
									number_format($row_tbl['ppn'],2,'.',','),
									number_format($row_tbl['pph'],2,'.',','),
									number_format($row_tbl['gran_totalppn'],2,'.',','),
									$row_tbl['term_top'],
									$row_tbl['user_submission'],
									$info_aproval,
									$btn_view_detail,
								    $attach_quo

							  );
		}
		echo json_encode($output);
	}

  public function suggest_vendor()
	{
		$id_company = $this->session->userdata('id_company');
	    $id_dept = $this->session->userdata('id_dept');
		$status_del = "1"; //flag status delete pada table vendor

	    $icon = $this->input->post('vendor',TRUE);

	    $this->load->database();
	    $this->db->select('*');
	    $this->db->like("id_vendor",$icon);
		$this->db->where('id_company',$id_company);
		$this->db->where('id_dept',$id_dept);
		$this->db->where('status',$status_del);
	    $this->db->or_like("vendor",$icon);
		$this->db->where('id_company',$id_company);
		$this->db->where('id_dept',$id_dept);
		$this->db->where('status',$status_del);
		//add 6 Desember 2017 by Yoel
		$this->db->or_like("code_vendor",$icon);
		$this->db->where('id_company',$id_company);
		$this->db->where('id_dept',$id_dept);
		$this->db->where('status',$status_del);
	    $hasil = $this->db->get('tbl_vendor')->result();
       
	   
		$json_array = array();
		foreach ($hasil as $row)
		$json_array[]= array("value" =>$row->vendor,
                      "label" =>$row->code_vendor." | ".$row->vendor." | ".$row->id_vendor,
					  "code" => $row->code_vendor,
					  "idvendor" => $row->id_vendor,
					  );
		echo json_encode($json_array);

	}

	public function suggest_coa()
	{

		$id_company = $this->session->userdata('id_company');
	    $id_dept = $this->session->userdata('id_dept');
		$status_del = "1"; //flag status delete pada table vendor

	    $icon = $this->input->post('coa',TRUE);

	    $this->load->database();
	    $this->db->select('*');
	    $this->db->like("nomor_coa",$icon);
		$this->db->where('id_company',$id_company);
		$this->db->where('id_dept',$id_dept);
		$this->db->where('status',$status_del);
		$this->db->or_like("desc_coa",$icon);
		$this->db->where('id_company',$id_company);
		$this->db->where('id_dept',$id_dept);
		$this->db->where('status',$status_del);
	    $hasil = $this->db->get('tbl_coa')->result();

		$json_array = array();
		foreach ($hasil as $row2)
		$json_array[]= array(//"value"=>$row2->desc_coa." | ".$row2->nomor_coa,
							 "value"=>$row2->nomor_coa,
							 "label" =>$row2->desc_coa." | ".$row2->nomor_coa,
							 //"idcoa"=>$row2->id_coa
							 );
		echo json_encode($json_array);

	}


	public function suggest_act()
	{

		$id_company = $this->session->userdata('id_company');
	    $id_dept = $this->session->userdata('id_dept');
		$status_del = "1"; //flag status delete pada table vendor

	    $icon = $this->input->post('act',TRUE);
		$getcoa = $this->input->post('coa',TRUE);
		

		$this->load->database();
	    $this->db->select('*');
	    $this->db->like("nomor_act",$icon);
		$this->db->where('id_dept',$id_dept);
		$this->db->where('status',$status_del);
		$this->db->or_like("desc_act",$icon);
		//$this->db->where('nomor_coa',$getcoa);
	    $this->db->where('id_dept',$id_dept);
		$this->db->where('status',$status_del);
	    $hasil = $this->db->get('qv_complite_coa_act')->result();
    
		$json_array = array();
		foreach ($hasil as $row3)
		$json_array[]= array(//"value"=>$row3->desc_act." | ".$row3->nomor_act,
							 "value"=>$row3->nomor_act,
							 "label" =>$row3->desc_act." | ".$row3->nomor_act,
							 //"idact"=>$row3->id_act
							 "nomorcoa"=>$row3->nomor_coa
							 );
	
		echo json_encode($json_array);
		
		
	

	}

	public function disconnect()
	{
		$ddate = date('d F Y');
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user

		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');

  }

}
