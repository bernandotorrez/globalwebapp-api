<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>
      
<?php   	
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>      
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>                        
                <?php  echo "Successful Grant Approval"	?>  
		  </div>		
<?php } ?>

<?php   	
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>                
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>                        
			<?php  echo "Send Approval failed"	?>  
        </div>		      
<?php } ?>	

<?php   	
	If ($this->session->flashdata('pesan_reject') == "1"){ ?>                
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>                        
			<?php  echo "Approval Rejected Succesful"	?>  
        </div>		      
<?php } ?>	
  

  
<div class="widget-box">
    <div class="widget-header">
        <h4 class="widget-title"><?php echo " ".$header ;?></h4>
    </div>

    <div class="widget-body">
        <div class="widget-main">                 
   
          
 <?php    
	 echo form_open('approval_form/c_table_aproval/multiple_submit',array('id' => 'form-tablepp')); 
  ?>     
 
 <table>
     <tr>
      <td>
        <input id="btnref" name="btnref" type="submit" value="Refresh Table" class="btn  btn-succes" />
      </td>
       <td colspan="3">&nbsp;</td> 
       <td colspan="3">                                      
       <input id="btnaprove" name="btnaprove" type="submit" value="Approved"  class="btn btn-primary btn-aprove"  disabled="disabled"/>	  	           
         </td>         
      </tr> 
    </table>   
 </br>

 
   <div class="row">
    <div class="col-md-12 col-md-offset-0">   	
      <table id="myTable" class="table table-striped table-bordered table-hover" width="100%" >
	    <thead class="text-warning">
         <th >Action</th>
		 <th >PP No </th>	
		 <th>Company</th>	
         <th>Dept</th>						
		 <th >Vendor</th>			
		 <th >Description</th>
         <th >Remarks</th> 		
		 <th >Date Send</th> 
		 <th >Currency</th> 
         <th >Purchase Type</th>        
		 <th >Total </th> 
         <th >PPN </th> 
         <th >Total + PPN </th> 
         <th >Term</th> 
		 <th >Subbmision</th> 				
		 <th >Head Dept</th> 
		 <th >Purchasing</th> 
         <th >F.C</th> 
		 <th >B.O.D</th> 		
		 <th >Quotation</th> 	
		 <th >Detail</th> 		
		 <th >Reject</th> 				
         </thead>
	</table>	
	
		
        </div>  
        </div> 
     </div>  
  </div>
 </div>   
  
 <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Info & Remarks</h4>
                    </div>
                    
                    <div class="modal-body">
                      
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal -->  	

<!-- Modal Buat Reject -->

  <div class="modal fade" id="myModal-reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Remarks For Reject Status</h4>
                    </div>
                    
                    <div class="modal-body-reject">
                       <table width="90%" class="table-condensed" align="center">
                            <tr> 
                                <td> <label for="PP Number"  class="col-xs-8">PP Number</label></td> 
                             
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtppno" name="txtppno" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                             <tr> 
                                <td ><label for="Reject Reason"  class="col-xs-9">Reject Reason</label></td> 
                                <td>
                                  <div class="col-xs-12">
                                    <textarea id="txtreject" name="txtreject"  class="form-control"></textarea>
                                  </div>  
                                </td>    
                             </tr>
                             <tr> 
                                <td></td> 
                                <td>
                                   <div class="col-xs-12">
                                   
                                   </div>        
                                </td>    
                             </tr>
                            </table> 
           
                    </div> 
                    
                     <div class="modal-footer">
                     <input type="submit" name="btnreject" value="Submit Reject" class="btn btn-danger"/>
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>           
        </div>
    
<!-- Modal Buat Reject -->  	

 
<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 


<script>
//--------------------function disbaled enabled button with check book

<?php if($this->session->userdata('aproval_flag') != '1') : ?>
var counterCheckapproved = 0;
	$('body').on('change', 'input[type="checkbox"]', function() {
		this.checked ? counterCheckapproved++ : counterCheckapproved--;
		counterCheckapproved >= 1 ? $('#btnaprove').prop("disabled", false): $('#btnaprove').prop("disabled", true);
	});
<?php else: ?>
var counterCheckapproved = 0;
	$('body').on('change', 'input[type="checkbox"]', function() {
		this.checked ? counterCheckapproved++ : counterCheckapproved--;
		counterCheckapproved == 1 ? $('#btnaprove').prop("disabled", false): $('#btnaprove').prop("disabled", true);
	})
	
<?php endif ?>
//-----------------------------------------------------
  
//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});		  


</script>

<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.detail',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("approval_form/c_table_aproval/get_nopp_modal"); ?>';
		
		$("#myModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});
	
</script>

<script>


$(function(){ // Show modal pop up send REJECT
  $(document).on('click','.reject',function(e){
		var req_id = $(this).attr('req_id'); 										
		$("#myModal-reject").modal('show');
		$('#txtppno').val(req_id); //menampilkan isi dari  idmaster kedalam inputbox 	
		$('#txtreject').val(''); //mengkosongkan txtremark reject 			  				
	   });																					
});		
</script>


<script>

$(function(){
    $('#myform').on('submit', function(e){ // id #myform di buat untuk diferentsiasi dengan form open code igniter
        e.preventDefault();
		
		var url = '<?php echo base_url("approval_form/c_table_aproval/multiple_submit"); ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), // myform di buat untuk diferentsiasi dengan form open code igniter 
            success: function(data){
                 alert($('#txtppno').val() + ',' + $('#txtreject').val())								 
            }
        });
    });
});
</script>

<script type="text/javascript">

$("#myTable").DataTable({	
	ordering: false,	
	autoWidth : false,
	processing: true,
	serverSide: true,	
    "scrollY": 250,
    "scrollX": true,
	ajax: {
	  url: "<?php echo base_url('approval_form/c_table_aproval/cath_data_fordatatables') ?>",
	  type:'POST',
	}
});

</script>

<?php if ($this->session->userdata('aproval_flag')==4 ||  $this->session->userdata('aproval_flag')==2) {     ?>

<script> 

var counterCheckaprove = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckaprove++ : counterCheckaprove--;
    counterCheckaprove == 1 ? $('#btnaprove').prop("disabled", false): $('#btnaprove').prop("disabled", true);
});

</script>

<?php } ?>