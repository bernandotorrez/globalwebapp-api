<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_menu_user_grup extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_menu_user_grup');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_menu_user_grup->get_value('id',$this->session->userdata('id'),'tbl_user');
				$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}
	 	}
	
	public function ajax_list()
	{
		$list = $this->M_menu_user_grup->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $qv_menu_user_groups) {
			$chk_idmaster ='<div align="center"><input id="checkuser_grup" name="checkuser_grup" type="checkbox" value='.$qv_menu_user_groups->id_menu_user_group.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $qv_menu_user_groups->id_menu_user_group;
			$row[] = $qv_menu_user_groups->name_grp;
			$row[] = $qv_menu_user_groups->parent_menu_desc;
			$row[] = $qv_menu_user_groups->desc_child;
			$row[] = $qv_menu_user_groups->desc_sub;
			$row[] = $qv_menu_user_groups->can_view;
			$row[] = $qv_menu_user_groups->can_add;
			$row[] = $qv_menu_user_groups->can_edit;
			$row[] = $qv_menu_user_groups->can_del;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_menu_user_grup->count_all(),
						"recordsFiltered" => $this->M_menu_user_grup->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_menu_user_group');
		$this->db->from('qv_menu_user_groups');
		$this->db->where('status','1');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_menu_user_group",$search);
		$this->db->where('status','1');
		$this->db->or_like("name_grp",$search);
		$this->db->where('status','1');
		$this->db->or_like("parent_menu_desc",$search);
		$this->db->where('status','1');
		$this->db->or_like("desc_child",$search);
		$this->db->where('status','1');
		$this->db->or_like("desc_sub",$search);
		$this->db->where('status','1');
		$this->db->or_like("can_view",$search);
		$this->db->where('status','1');
		$this->db->or_like("can_add",$search);
		$this->db->where('status','1');
		$this->db->or_like("can_edit",$search);
		$this->db->where('status','1');
		$this->db->or_like("can_del",$search);
		$this->db->where('status','1');
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		$this->db->where('status','1');
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_menu_user_group','ASC');
		$query=$this->db->get('qv_menu_user_groups');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("id_menu_user_group",$search);
		$jum=$this->db->get('qv_menu_user_groups');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $qv_menu_user_groups) {
		$chk_idmaster ='<div align="center"><input id="checkvendor" name="checkvendor" type="checkbox" value='.$qv_menu_user_groups["id_menu_user_group"].' class="editRow ace" req_id_del='.$qv_menu_user_groups["id_menu_user_group"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($qv_menu_user_groups['id_menu_user_group'],$qv_menu_user_groups['name_grp'],
									$qv_menu_user_groups['parent_menu_desc'],$qv_menu_user_groups['desc_child'],
									$qv_menu_user_groups['desc_sub'],$qv_menu_user_groups['can_view'],
									$qv_menu_user_groups['can_add'],$qv_menu_user_groups['can_edit'],
									$qv_menu_user_groups['can_del'],$chk_idmaster
									);

		}

		echo json_encode($output);


	}

	
	public function index()
		{

		$data['menu_user_grup']=$this->M_menu_user_grup->get_all_menu_user_grup();
		$data['total']=$this->M_menu_user_grup->get_count_id();
		$data['group']=$this->M_menu_user_grup->get_all_group();
		$data['parent']=$this->M_menu_user_grup->get_all_parent();
		$data['parent2']=$this->M_menu_user_grup->get_all_parent();
		$data['child']=$this->M_menu_user_grup->get_all_child();
		$data['show_view'] = 'menu_user_grup_form/V_menu_user_grup';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function add_menu_user_grup()
		{
		if(empty($_POST["id_group"])){
			die ("Field Name Group must be filled in!! ");
		}
		elseif(empty($_POST["id_parent"])){
			die ("Field Name Parent must be filled in!! ");
		}
		elseif(empty($_POST["id_child"])){
			die ("Field Name Child must be filled in!! ");
		}
		elseif(empty($_POST["id_subchild"])){
			die ("Field Name Subchild must be filled in!! ");
		}
		elseif(empty($_POST["can_view"])){
			die ("Field Type View must be filled in!! ");
		}
		elseif(empty($_POST["can_add"])){
			die ("Field Type Add must be filled in!! ");
		}
		elseif(empty($_POST["can_edit"])){
			die ("Field Type Edit must be filled in!! ");
		}
		elseif(empty($_POST["can_del"])){
			die ("Field Type Delete must be filled in!! ");
		}
		else{
			$data = array(
					'id_menu_user_group' => $this->input->post('id_menu_user_group'),
					'id_group' => $this->input->post('id_group'),
					'id_parent' => $this->input->post('id_parent'),
					'id_child' => $this->input->post('id_child'),
					'id_subchild' => $this->input->post('id_subchild'),
					'can_view' => $this->input->post('can_view'),
					'can_add' => $this->input->post('can_add'),
					'can_edit' => $this->input->post('can_edit'),
					'can_del' => $this->input->post('can_del'),
					'status' => '1',
				);
			$insert = $this->M_menu_user_grup->add_menu_user_grup($data);
		  }
		  echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data= $this->M_menu_user_grup->get_by_id($id);
			echo json_encode($data);
		}
		
	function pos_parent($id_parent)
	{
    	$query = $this->db->get_where('tbl_child_menu',array('id_parent'=>$id_parent));
    	$data = "<option value=''>- Select Child -</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->id_child."'>".$value->desc_child."</option>";
    	}
    	echo $data;
	}
  
	function pos_child($id_child)
	{
		$query = $this->db->get_where('tbl_sub_child_menu',array('id_child'=>$id_child));
    	$data = "<option value=''>- Select Sub Child -</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->id_subchild."'>".$value->desc_sub."</option>";
    	}
    	echo $data;
    }

	public function update_menu_user_grup()
		{
		if(empty($_POST["id_group"])){
			die ("Field Name Group must be filled in!! ");
		}
		elseif(empty($_POST["id_parent"])){
			die ("Field Name Parent must be filled in!! ");
		}
		elseif(empty($_POST["id_child"])){
			die ("Field Name Child must be filled in!! ");
		}
		elseif(empty($_POST["id_subchild"])){
			die ("Field Name Subchild must be filled in!! ");
		}
		elseif(empty($_POST["can_view"])){
			die ("Field Type View must be filled in!! ");
		}
		elseif(empty($_POST["can_add"])){
			die ("Field Type Add must be filled in!! ");
		}
		elseif(empty($_POST["can_edit"])){
			die ("Field Type Edit must be filled in!! ");
		}
		elseif(empty($_POST["can_del"])){
			die ("Field Type Delete must be filled in!! ");
		}
		else{
			$data = array(
					'id_menu_user_group' => $this->input->post('id_menu_user_group'),
					'id_group' => $this->input->post('id_group'),
					'id_parent' => $this->input->post('id_parent'),
					'id_child' => $this->input->post('id_child'),
					'id_subchild' => $this->input->post('id_subchild'),
					'can_view' => $this->input->post('can_view'),
					'can_add' => $this->input->post('can_add'),
					'can_edit' => $this->input->post('can_edit'),
					'can_del' => $this->input->post('can_del'),
					'status' => '1',
				);
		$this->M_menu_user_grup->update_menu_user_grup(array('id_menu_user_group' => $this->input->post('id_menu_user_group')), $data);
		  }
		   echo 'Insert';
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);
				$this->db->where('id_menu_user_group', $id);
				$this->db->update('tbl_menu_user_group', $data);
			}
		}
	}

	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}

}
