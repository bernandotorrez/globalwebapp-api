<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<div>
		<button onclick="add_subchild()" class="btn btn-app btn-primary btn-xs radius-4 caddnew" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
        Add
		</button>
		<button onclick="delete_subchild()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit" id="deleteTriger" name="deleteTriger"  value ="btndel"  disabled="disabled" >
      <i class="ace-icon fa fa-ban bigger-160"></i>
        Delete
		</button>
		<button onclick="edit_subchild()" class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="editTriger"  name="editTriger"  value ="btnedit"  disabled="disabled" >
      <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
        Edit
		</button> 
		<button onclick="edit_position()" class="btn btn-app btn-flat btn-xs radius-4" type="submit">
			<i class="ace-icon glyphicon glyphicon-sort bigger-120"></i>
        Switch
		</button>
</div>
<br/>
<div class="table-header btn-info"> <?php echo " ".$header ;?>   </div>  
	<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">   

  <div class="form-group col-xs-6">
        <div class="col-xs-10">
            <div class="form-inline ">
                <div class="form-group">Date start</div>
                
                <div class="form-group">
                    <div class="input-group date">
                       <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                       </div>
                       
                       <input type="text" class="form-control input-daterange" id="start_date" name="start_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                    </div> <!-- /.input-group datep -->       
                </div>
               
               <div class="form-group">
                       <label for="From" class="col-xs-1 control-label">To</label>
               </div>
              
               <div class="form-group">
                      <div class="input-group date">
                               <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                               </div>
                                     <input type="text" class="form-control input-daterange" id="end_date" name="end_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                       </div>  
               </div>
                
            </div>
        </div>
      </div>

		<table id="myTable" cellpadding="0" cellspacing="0" width="100%"  class="table table-striped table-bordered table-hover">
			<thead class="text-warning" >
					<th width="5%" style="text-align:center">
						<label class="pos-rel">
 							<input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
              <span class="lbl"></span>
            </label>
          </th>
					<th>No</th>
	        <th>Id Sub Child</th>
					<th>Id Parent</th>
					<th>Name Parent</th>
					<th>Id Child</th>
					<th>Name Child</th>
					<th>Position Sub Child</th>
					<th>Name Sub Child</th>
					<th>URL Sub Child</th>
          <th>Date Create</th>	
	    </thead>
    </table> 
   </div>                               
</div>                                      	                       
<!-- Bootstrap modal -->
<form action="#" id="form" class="form-horizontal">
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Sub Child Form</h3>
      </div>
      <div class="modal-body form">
        <div class="widget-body">
			    <div class="widget-main">
          <input type="hidden" value="" name="id_subchild"/>
          <div>
            <label for="form-field-select-3">Name Parent</label>
              <select class="myselect form-control" name="id_parent" id="id_parent" tabindex="2" style="width: 100%;">
                <option  value="">Choose Parent</option> 
                  <?php foreach($parent as $ddd) { ?>
                <option value="<?php echo $ddd->id_parent;?>"><?php echo $ddd->parent_menu_desc;?>
                </option>
                  <?php } ?>
              </select>			
          </div>
          <div>
            <label for="form-field-select-3">Name Child</label>
              <select class="myselect form-control" name="id_child" id="id_child" tabindex="2" style="width: 100%;">
                <option  value="">Choose Child</option> 
                  <?php foreach($parent3 as $bbb) {
                  	$sqlpo	  = "select * from tbl_child_menu where id_parent='".$bbb->id_parent."'";
                    $qrypo	  = $this->db->query($sqlpo);
                    $adapo 	  = $qrypo->result();					
                    foreach($adapo as $aaa) { ?>
                <option value="<?php echo $aaa->id_child;?>"><?php echo $aaa->desc_child;?>
                </option>
                  <?php } }?>
              </select>			
          </div>
          <div>
              <label for="order_subcild">Position Sub Child</label>
              <input class="form-control" id="order_subcild" name="order_subcild" placeholder="1" type="text"/>
          </div>
          <div>
              <label for="desc_sub">Name Sub Child</label>
              <input class="form-control" id="desc_sub" name="desc_sub" placeholder="Home" type="text"/>
          </div>
          <div>
              <label for="anchor_sub_url">URL Sub Child</label>
              <input class="form-control" id="anchor_sub_url" name="anchor_sub_url" placeholder="main_menu/C_main_menu" type="text"/>
          </div>
        </div>
      </div>
    </div>
          <div class="modal-footer">
          <button onclick="save()" id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
              <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                Save
            </button>
            <button class="btn btn-app btn-danger btn-xs radius-4" type="submit" data-dismiss="modal">
              <i class="ace-icon fa fa-close bigger-160"></i>
                Cancel
            </button>
          </div>
       </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Position Form</h3>
       </div>
	    <input type="hidden" value="" name="id_subchild"/>
			<div class="modal-body form">
        <div class="widget-body">
					<div class="widget-main">
      <div>
        <label for="form-field-select-3">Name Parent</label>
          <select class="myselect form-control" name="id_parent3" id="id_parent3" tabindex="2" style="width: 100%;">
            <option  value="">Choose Parent</option> 
              <?php foreach($parent2 as $row) { ?>
            <option value="<?php echo $row->id_parent;?>"><?php echo $row->parent_menu_desc;?></option>
              <?php } ?>
          </select>			
      </div>
      <div>
        <label for="form-field-select-3">Name Child</label>
          <select class="myselect form-control" name="id_child3" id="id_child3" tabindex="2" style="width: 100%;">
            <option  value="">Select Child</option> 
          </select>			
      </div>
      <br/>
			<div class="form-group">
			  <div class="col-xs-10 col-xs-offset-1 col-md-8" id="id_subchild3" name="id_subchild3"></div>
      </div>
      </div>
    </div>
  </div>
			<div class="modal-footer">
        <button onclick="save2()" id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
          <i class="ace-icon fa fa-floppy-o bigger-160"></i>
            Save
        </button>
        <button class="btn btn-app btn-danger btn-xs radius-4" type="submit" data-dismiss="modal">
          <i class="ace-icon fa fa-close bigger-160"></i>
            Cancel
        </button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
  <!-- End Bootstrap modal -->  	   
</form>


<script type="text/javascript">
      $(".myselect").select2();
</script>
<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>
<script type="text/javascript"> 
        $("#id_parent").change(function(){
                var id_parent = {id_parent:$("#id_parent").val()};
                   $.ajax({
               type: "POST",
               url : "<?php echo site_url('subchild_form/C_subchild/ajax_parent/') ?>",
               data: id_parent,
               success: function(msg){
               $('#id_child').html(msg);
               }
            });
              });
</script>
<script>
        	$(document).ready(function(){
	            $("#id_parent3").change(function (){
	                var url = "<?php echo site_url('subchild_form/C_subchild/pos_parent3');?>/"+$(this).val();
	                $('#id_child3').load(url);
	                return false;
	            })
	  
				$("#id_child3").change(function(){
                var id_child = {id_child:$("#id_child3").val()};
                   $.ajax({
               type: "POST",
               url : "<?php echo site_url('subchild_form/C_subchild/pos_child3');?>",
               data: id_child,
               success: function(msg){
               $('#id_subchild3').html(msg);
               }
            });
              });
	        });
    	</script>	
<script>
//$('#myTable').dataTable();
//-----------------------------------------data table custome----
// var rows_selected = [];
// var tableUsers = $('#myTable').DataTable({
//         "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
// 		"dom": 'lfBrtip',
// 		"buttons": [
//             {
// 		   "extend":    'copy',
// 		   "text":      '<i class="fa fa-files-o"></i> Copy',
// 		   "titleAttr": 'Copy'
// 		   },
// 		   {
// 		   "extend":    'print',
// 		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
// 		   "titleAttr": 'Print',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   },
// 		   {
// 		   "extend":    'excel',
// 		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
// 		   "titleAttr": 'Excel'
// 		   },
// 		   {
// 		   "extend":    'pdf',
// 		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
// 		   "titleAttr": 'PDF',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   }
//         ],
// 		"autoWidth" : false,
// 		"scrollY" : '250',
// 		"scrollX" : true,
//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.
//         "order": [], //Initial no order.

//         // Load data for the table's content from an Ajax source
//         "ajax": {
//             "url": "<?php echo base_url('subchild_form/C_subchild/ajax_list') ?>",
//             "type": "POST"
//         },
// 		'columnDefs': [
// 		{
//         'targets': [0],
// 		'orderable': false,
// 		}
// 		],

//       'order': [[2, 'desc']],
//         });

/*var tableUsers = $('#myTable').DataTable({
            searching:true,
			//width:'100%',
			autoWidth : false,
  		    responsive : true,
			"scrollY" : '250',
			"scrollX" : true,
			ordering: false,
			/*oLanguage: {
				sProcessing: "<img src='' />",
			},
			processing: true,
			serverSide: true,
			ajax: {
			  url: "<?php echo base_url('subchild_form/C_subchild/ambil_data') ?>",
			  type:'POST',
	}
			
        });	*/		
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>

<!-- Datatable -->
<script>
$(document).ready(function(){ 
		$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data('no'); //
	
		function fetch_data(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
				"ajax":{
				    url: "<?php echo base_url('subchild_form/C_subchild/ajax_list') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
				 
				 'columnDefs': [
							{
							'targets': [0],
							'orderable': false,
							}
					],

						'order': [[2, 'desc']],
			 });
		  }
		  	  
		  
//proses change date-----------------------------------------------
    $('#start_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
		$('#end_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);		
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	
 
 
 
 //onblur date-------------------------------------------------------------------------
 
 		// $('#start_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
		
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		  fetch_data('no', start_date, end_date);
		// 	} 
			
		// });
 
 
 	
		// $('#end_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
			
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		 fetch_data('no', start_date, end_date);
		// 	} 
		// });
 //end onblur-------------------------------------	

  }); //end document on ready	
		
</script>

	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
    function add_subchild()
    {
      save_method = 'add';
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
	
	function edit_position()
    {
		$.ajax({
        url:"<?php echo site_url('subchild_form/C_subchild/ajax_parent/')?>/", //the page containing php script
        type: "POST", //request type
        success:function(result){
			$('#id_position').html(result);
            $('#modal_form2').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Position'); // Set title to Bootstrap modal title
       },
	    error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
     });
	}
	
	function edit_subchild(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('subchild_form/C_subchild/ajax_edit/')?>/" + eee,
		    type: "GET",
        dataType: "JSON",
        success: function(data)
        {

        $('[name="id_parent"]').val(data.id_parent);
			  $('[name="id_subchild"]').val(data.id_subchild);
        $('[name="id_child"]').val(data.id_child);
			  $('[name="order_subcild"]').val(data.order_subcild);
			  $('[name="desc_sub"]').val(data.desc_sub);
			  $('[name="anchor_sub_url"]').val(data.anchor_sub_url);
	
            
            $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Sub Child'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



     function save()
     {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('subchild_form/C_subchild/add_subchild')?>";
      }
      else
      {
		   url = "<?php echo site_url('subchild_form/C_subchild/update_subchild')?>";
      }
		var formData = new FormData(document.getElementById('form')) 	    
			$.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }
	
	function save2()
    {
      var url;
      url = "<?php echo site_url('subchild_form/C_subchild/update_position')?>";
      var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
              if(data=='Insert'){
				$('#modal_form2').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }

    function delete_subchild()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('subchild_form/C_subchild/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
      
    }
	
  </script>
</html>
