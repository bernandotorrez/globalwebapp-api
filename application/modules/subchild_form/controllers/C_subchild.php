<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_subchild extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_subchild');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_subchild->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}
	 	}
		
	public function index()
		{

		$data['subchild']=$this->M_subchild->get_all_subchild();
		$data['child']=$this->M_subchild->get_all_child();
		$data['parent']=$this->M_subchild->get_all_parent();
		$data['parent2']=$this->M_subchild->get_all_parent();
		$data['parent3']=$this->M_subchild->get_all_parent();
		$data['par']=$this->M_subchild->get_all_par();
		$data['show_view'] = 'subchild_form/V_subchild';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		}
	
	public function ajax_list()
	{
		$list = $this->M_subchild->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $qv_complite_subchild) {
			$chk_idmaster ='<div align="center"><input id="checksubchild" name="checksubchild" type="checkbox" value='.$qv_complite_subchild->id_subchild.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $qv_complite_subchild->id_subchild;
			$row[] = $qv_complite_subchild->id_parent;
			$row[] = $qv_complite_subchild->parent_menu_desc;
			$row[] = $qv_complite_subchild->id_child;
			$row[] = $qv_complite_subchild->desc_child;
			$row[] = $qv_complite_subchild->order_subcild;
			$row[] = $qv_complite_subchild->desc_sub;
			$row[] = $qv_complite_subchild->anchor_sub_url;
			$row[] = $qv_complite_subchild->date_create;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_subchild->count_all(),
						"recordsFiltered" => $this->M_subchild->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_subchild');
		$this->db->from('qv_complite_subchild');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_subchild",$search);
		$this->db->or_like("id_parent",$search);
		$this->db->or_like("parent_menu_desc",$search);
		$this->db->or_like("id_child",$search);
		$this->db->or_like("desc_child",$search);
		$this->db->or_like("order_subcild",$search);
		$this->db->or_like("desc_sub",$search);
		$this->db->or_like("anchor_sub_url",$search);
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_subchild','ASC');
		$query=$this->db->get('qv_complite_subchild');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("id_subchild",$search);
		$jum=$this->db->get('qv_complite_subchild');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $qv_complite_subchild) {
		$chk_idmaster ='<div align="center"><input id="checksubchild" name="checksubchild" type="checkbox" value='.$qv_complite_subchild["id_subchild"].' class="editRow ace" req_id_del='.$qv_complite_subchild["id_subchild"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($qv_complite_subchild['id_subchild'],$qv_complite_subchild['id_parent'],
									$qv_complite_subchild['parent_menu_desc'],$qv_complite_subchild['id_child'],
									$qv_complite_subchild['desc_child'],$qv_complite_subchild['order_subcild'],
									$qv_complite_subchild['desc_sub'],$qv_complite_subchild['anchor_sub_url'],
									$chk_idmaster
									);

		}

		echo json_encode($output);


	}
		
	public function add_subchild()
		{
			// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_add= date('Y-m-d H:i:s');

		if(empty($_POST["id_child"])){
			die ("Field ID Child must be filled in!! ");
		}
		elseif(empty($_POST["order_subcild"])){
			die ("Field Position Sub Child must be filled in!! ");
		}
		elseif(empty($_POST["desc_sub"])){
			die ("Field Name Sub Child must be filled in!! ");
		}
		elseif(empty($_POST["anchor_sub_url"])){
			die ("Field URL Sub Child must be filled in!! ");
		}
		else{
			$data = array(
					'id_child' => $this->input->post('id_child'),
					'order_subcild' => $this->input->post('order_subcild'),
					'desc_sub' => $this->input->post('desc_sub'),
					'anchor_sub_url' => $this->input->post('anchor_sub_url'),
					'date_create' => $date_add,
					'remarks' => $remarks_add
				);
			$insert = $this->M_subchild->add_subchild($data);
		}
		echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_subchild->get_by_id($id);
			echo json_encode($data);
		}


	public function update_subchild()
		{

			$id = $this->session->userdata('id');
			$username = $this->session->userdata('username');
			$nama = $this->session->userdata('name');
			$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
			$date_update = date('Y-m-d H:i:s');

		if(empty($_POST["id_child"])){
			die ("Field ID Child must be filled in!! ");
		}
		elseif(empty($_POST["order_subcild"])){
			die ("Field Position Sub Child must be filled in!! ");
		}
		elseif(empty($_POST["desc_sub"])){
			die ("Field Name Sub Child must be filled in!! ");
		}
		elseif(empty($_POST["anchor_sub_url"])){
			die ("Field URL Sub Child must be filled in!! ");
		}
		else{
		$data = array(
					'id_child' => $this->input->post('id_child'),
					'order_subcild' => $this->input->post('order_subcild'),
					'desc_sub' => $this->input->post('desc_sub'),
					'anchor_sub_url' => $this->input->post('anchor_sub_url'),
					'date_update' => $date_update,
					'remarks' => $remarks_update
				);
		$this->M_subchild->update_subchild(array('id_subchild' => $this->input->post('id_subchild')), $data);
		}
		echo 'Insert';
	}
	
	public function ajax_parent()
    {
        $id = $this->input->post('id_parent');
        $data['child'] = $this->M_subchild->get_child($id);
        $this->load->view('child',$data);
    }
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$this->db->where('id_subchild', $id);
				$this->db->delete('tbl_sub_child_menu');
			}
		}
	}
	
	function pos_parent3($id_parent3)
	{
    	$query = $this->db->get_where('tbl_child_menu',array('id_parent'=>$id_parent3));
    	$data = "<option value=''>- Select Child -</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->id_child."'>".$value->desc_child."</option>";
    	}
    	echo $data;
	}
  
	function pos_child3()
	{
		$id = $this->input->post('id_child');
        $data['subchild'] = $this->M_subchild->get_subchild($id);
		$data['subchild2'] = $this->M_subchild->get_all_count($id);
        $this->load->view('subchild',$data);
    }
	
	public function update_position()
		{
				$count = 0;
				$displayed_city = array();
				$city = array();

				foreach ($_POST['pos'] as $pos_key => $pos_value){
				  $jumlah = count($_POST['pos']); 
				  $count	++;
				  $city = '';
				  if( isset($pos_value)){
					$city = $pos_value;
					if(!in_array($city, $displayed_city)){
					  $displayed_city[] = $city;
					  $total = count($displayed_city);
					}
				  }
		}
		 if($total!=$jumlah){
			die('Duplicate Data !');
			
		  }else{
			echo 'Insert';
			   foreach ($_POST['pos'] as $pos_key => $pos_value){
				$dede[$pos_key] = $pos_value;
				$parentnya['order_subcild'] = $pos_value;
				$this->db->where("id_subchild",$pos_key);
				$this->db->update("tbl_sub_child_menu",$parentnya);
			 
		   }
}
}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }



}
