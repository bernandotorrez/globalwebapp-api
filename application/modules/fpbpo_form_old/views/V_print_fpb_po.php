<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);
$this->db->update('tbl_user',$data1);
}
?>
<?php
	If ( $this->session->flashdata('pesan_give_right') != ""){ ?>
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>
            <?php  echo $this->session->flashdata('pesan_give_right');	?>
        </div>
<?php } ?>

<?php
	If ( $this->session->flashdata('pesan_rpt') != ""){ ?>
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>
            <?php  echo $this->session->flashdata('pesan_rpt');	?>
        </div>
<?php } ?>

<?php
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>
                <?php  echo "Send Approval Successfully"	?>
		  </div>
<?php } ?>

<?php
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>
			<?php  echo "Send Approval failed"	?>
        </div>
<?php } ?>


 <?php  
 echo form_open('fpbpo_form/c_print_fpb_po/print_fpb_submit',array('id' =>'form-printfpbpo'));  
 ?>
 

<div class="table-header btn-info"> <?php echo " ".$header ;?>   </div>  
<div class="tabbable">
          <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab">
           <!-- <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4"> -->
                <li class="active">
                    <a data-toggle="tab" href="#printpo" aria-expanded="true">Print Po</a>
                </li>
                 <li class="">
                    <a data-toggle="tab" href="#recieve" aria-expanded="false">Recieve</a>
                </li>
                 <li class="">
                    <a data-toggle="tab" href="#printfpb" aria-expanded="false">Print Fpb</a>
                </li>
           </ul>     
                <div class="tab-content">
                  <div id="printpo" class="tab-pane active">
                       <!--  <table>
                             <tr>
                              <td>
                                
                               <button class="btn btn-app btn-success btn-xs radius-4 btnpo btn-clean" type="submit" id="btnprintpo" name="btnprintpo"  value ="btnprintpo"  disabled="disabled" >
                                    <i class="ace-icon fa fa-pencil-square-o  bigger-160"></i>
                                    Print P.O 
                                </button> 
                                
                              </td>
                               <td colspan="3">
                                <button class="btn btn-app btn-warning btn-xs radius-4 btnfpb btn-clean btn_grn" type="submit" id="btngrn" name="btnprintfpb"  value ="btnprintfpb"  disabled="disabled" >
                                    <i class="ace-icon fa fa-check-square-o bigger-160"></i>
                                    GRN
                                </button> 
                               </td> 
                               <td colspan="3">                                      
                                 
                                <button class="btn btn-app btn-primary btn-xs radius-4 btnfpb btn-clean btn_fpb" type="submit" id="btnprintfpb" name="btnprintfpb"  value ="btnprintfpb"  disabled="disabled" >
                                    <i class="ace-icon fa fa-folder-o  bigger-160"></i>
                                    Print FPB
                                </button> 
                                               
                                 </td>         
                              </tr> 
                        </table>  --> 
                        <p>
                           
                          <table id="tablepo"  cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped ">
                            <thead class="text-warning">
                               <!-- <th>Action</th> -->
                                <th>PP No </th>	
                                <th>Detail</th> 	
                                <th>Quote</th> 	
                                <th>Print P.O</th> 
                                <th>Company</th>	
                                <th>Subbmision</th> 
                                <th>Dept</th>
                                <th>Description</th> 								
                                <th>Vendor</th>			
                                <th>Date Send</th> 
                                <th>Purch Type</th>
                                <th>Curr</th>  
                                 <!-- <th >C.O.A</th> --> 
                                <th>Total </th> 
                                <!-- <th >PPN </th> --> 
                                <th>Total+PPN </th> 
                                <th>Term</th> 
                                <th>Head Dept</th> 
                                <th>Purchasing</th> 
                                <th>F.C</th> 
                                <th>B.O.D</th> 						
                            </thead>
                          </table>
                   </div>
                   
                <div id="recieve" class="tab-pane   fade">
                     <!--     <button class="btn btn-app btn-warning btn-xs radius-4 btnfpb btn-clean btn_grn" type="submit" id="btngrn" name="btnprintfpb"  value ="btnprintfpb"  disabled="disabled" >
                                    <i class="ace-icon fa fa-check-square-o bigger-160"></i>
                                    GRN
                         </button> 
                 
                        <p> -->
                          
                          <table id="tablerecieved"  cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped ">
                            <thead class="text-warning">
                                <th>PP No </th>	
                                <th>Detail</th> 	
                                <th>Quote</th> 	
                                <th>Input Recived</th> 
                                <th>Company</th>	
                                <th>Subbmision</th> 
                                <th>Dept</th>
                                <th>Description</th> 								
                                <th>Vendor</th>			
                                <th>Date Send</th> 
                                <th>Curr</th> 
                                <th>Purch Type</th> 
                                 <!-- <th >C.O.A</th> --> 
                                <th>Total </th> 
                                <!-- <th >PPN </th> --> 
                                <th>Total+PPN </th> 
                                <th>Term</th> 
                               					
                            </thead>
                          </table>
                   </div>
                   
                   <div id="printfpb" class="tab-pane   fade">
                   		 
                        <p>
                      
                          <table id="tablefpb"  cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-striped ">
                            <thead class="text-warning">
                              
                                <th> PP No </th>	
                                <!--<th>Detail</th> 	
                                <th>Quote</th> 	 -->
                                <th>Print FPB</th> 
                                <th>Company</th>	
                                <th>Subbmision</th> 
                                <th>Dept</th>
                                <th>Description</th> 								
                                <th>Vendor</th>			
                                <th>Date Send</th> 
                                <th>Term</th> 
                                <th>Purch Type</th> 
                                <th>Curr</th> 
                                 <!-- <th >C.O.A</th> --> 
                                <th>Total </th> 
                                <!-- <th >PPN </th> --> 
                                <th>Total+PPN </th> 
                                
                               						
                            </thead>
                          </table>
                    </div>
                  
                   		
                   </div>
                </div>   
                 
                 
 </div>                
	              
                    
                    <!-- Modal detail -->
                            <div class="modal fade" id="ViewDetailModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Detail Purchase</h4>
                                        </div>
                    
                                        <div class="modal-body">
                    
                                        </div>
                    
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- Modal -->
                    
                     <!-- Modal FPB List Print -->
                            <div class="modal fade" id="FpbPrintModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">List Recieve</h4>
                                        </div>
                    
                                        <div class="modal-body">
                    
                                        </div>
                    
                                         <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- Modal -->
                    
                    <!-- Fungsi datepickier yang digunakan -->
                    <script type="text/javascript">
                     $('.datepicker').datetimepicker({
                            language:  'id',
                            weekStart: 1,
                            todayBtn:  1,
                      autoclose: 1,
                      todayHighlight: 1,
                      startView: 2,
                      minView: 2,
                      forceParse: 0
                        });
                    </script>
                    
                    <script>
                    
                    //aktif edit--------------------------------------------------------
                    var counterCheckpo = 0;
                    $('body').on('change', 'input[type="checkbox"]', function() {
                        this.checked ? counterCheckpo++ : counterCheckpo--;
                        counterCheckpo == 1 ? $('.btnpo').prop("disabled", false): $('.btnpo').prop("disabled", true);
                    });
                    //end---------------------------------------------------------------
                    //aktif edit--------------------------------------------------------
                    var counterCheckfpb = 0;
                    $('body').on('change', 'input[type="checkbox"]', function() {
                        this.checked ? counterCheckfpb++ : counterCheckfpb--;
                        counterCheckfpb == 1 ? $('.btnfpb').prop("disabled", false): $('.btnfpb').prop("disabled", true);
                    });
                    //end---------------------------------------------------------------
                    
                    
                    
                    //set focus ----------------------------
                    $(function() {
                      $("#txtcari").focus();
                    });
                    //set focus ----------------------------
                    </script>
                    
                    <?php if ($this->session->userdata('give_righ_flag') == '0'): // jika status session user nya 7?>
                       <script>
                        $(document).ready(function() {
                            $('#btngiveright').hide();
                        });
                        </script>
                    <?php endif ?>
                    
                    <script type="text/javascript">
                    
                    $("#tablepo").DataTable({
                        ordering  : true,
                        autoWidth : false,
                        processing: true,
                        serverSide: true,
                        "scrollY": 250,
                        "scrollX": true,
                        ajax: {
                          url: "<?php echo base_url('fpbpo_form/c_print_fpb_po/cath_data_fordatatables_po') ?>",
                          type:'POST',
                        },
						
						
						"columnDefs": [ {
							 "targets": [1,2,3], /* column index */
							 "orderable": false, /* true or false */
							 // "targets": [1,2], /* column index */
						} ],
						
                    });
					
					
					    
                    $("#tablerecieved").DataTable({
                        ordering: true,
                        autoWidth : false,
                        processing: true,
                        serverSide: true,
                        "scrollY": 250,
                        "scrollX": true,
                        ajax: {
                          url: "<?php echo base_url('fpbpo_form/c_print_fpb_po/cath_data_fordatatables_rieceved') ?>",
                          type:'POST',
                        },
						
							 "columnDefs": [ {
							 "targets": [0,9,10], /* column index */
							 "orderable": false, /* true or false */
							 // "targets": [1,2], /* column index */
						} ],
                    });
					
					    
                    $("#tablefpb").DataTable({
                        ordering: true,
                        autoWidth : false,
                        processing: true,
                        serverSide: true,
                        "scrollY": 250,
                        "scrollX": true,
                        ajax: {
                          url: "<?php echo base_url('fpbpo_form/c_print_fpb_po/cath_data_fordatatables_fpb') ?>",
                          type:'POST',
                        },
						
							 "columnDefs": [ {
							 "targets": [1,2,3], /* column index */
							 "orderable": false, /* true or false */
						} ],
                    });
					
					    
                  
                    
                    </script>
                    
                    <script>
					//modal detail---------------------------------------
                    $(function(){
                      $(document).on('click','.detail',function(e){
                            var req_id = $(this).attr('req_id');
                            var url = '<?php echo site_url("fpbpo_form/c_print_fpb_po/get_idtrans_modal"); ?>';
                    
                            $("#ViewDetailModal").modal('show');
                    
                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: {id_pp:req_id},
                                success: function (msg) {
                                    $('.modal-body').html(msg);
                                }
                    
                            });
                    
                       });
                    });
					//-----------------------------------------------------
					
					//modal fpb list---------------------------------------
					 $(function(){
                      $(document).on('click','.fpbbutton',function(e){
                            var req_id = $(this).attr('req_id');
                            var url = '<?php echo site_url("fpbpo_form/c_print_fpb_po/get_recieved_modal"); ?>';
                    
                            $("#FpbPrintModal").modal('show');
                    
                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: {id_pp:req_id},
                                success: function (msg) {
                                    $('.modal-body').html(msg);
                                }
                    
                            });
                       });
                    });
					//end fpblist-------------------------------------------------------------------------------
					
					//buat adjust column pada tab................
					$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
						  $($.fn.dataTable.tables(true)).DataTable()
							 .columns.adjust()
							 .responsive.recalc();
					}); 
					//-end---------------------------------------  
                    </script>
                    
                    
                   

