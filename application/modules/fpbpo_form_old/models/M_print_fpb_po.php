<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_print_fpb_po extends CI_Model {

    public $db_tabel = 'qv_head_pp_complite';

   
	
	
	
	//-------------------function untuk cari dan narik table report fpb
	function select_data_rpt_fpb()
	{
	  $str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete		
	  $strflag_adjust = "0";		
	  $strid_master =$this->input->post('msg'); 	 	  						  					    						   	 			   	   					  				   			  			   			
	 	for ($i=0; $i < count($strid_master) ; $i++) { 				
			  $query = $this->db->select("desc,spec,qty,harga,total,vendor,currency,gran_total,ppn,pph,gran_totalppn,counter_reprint")
								->where('id_master',$strid_master[$i])
								->where('flagadjust',$strflag_adjust)
								->where('status',$str_status_terdelete)
								->get('qv_master_det_pp');
			  if ($query->num_rows() >= 1)
			  {		
			  	 foreach ($query->result() as $row)
				 {
			  		$data = array('sesidmaster' => $strid_master[$i],							
								  'ses_vendor' =>  $row->vendor,
								  'ses_grand'   => $row->gran_total,
								  'counter_reprint'   => $row->counter_reprint,								  
								  ); //buat array di session		
					$this->session->set_userdata($data); //simpan di session
				 }
											 
					 
					 return $query->result();										 
			  }else{
					 return false;
			  }
	   	}	
	   
	 }
	 
	 
	//-------------------function untuk cari dan narik table lalucetak PO 
	function select_data_rpt_po() //model untuk print po
	{			
	  $strid_master =$this->input->post('btnprintpo'); 	 	  						  					    						   	  $str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete
	  $strflag_adjust = "0";
	  $strflag_beforeprintpo = "0" ; 
	   			   	   					  				   			  			   			
			 
					  $query = $this->db->select("desc,spec,qty,quo_reff,harga,total,vendor,alamat,notelp,fax,currency,gran_total,ppn,pph,gran_totalppn")
										->where('id_master',$strid_master)
										->where('status',$str_status_terdelete)
										->where('flagadjust',$strflag_adjust)
										->get('qv_master_det_pp');
										
					  if ($query->num_rows() >= 1)
					  {						
						 foreach ($query->result() as $row)
						 {
							$data = array('sesidmaster' => $strid_master,							
										  'ses_vendor' =>  $row->vendor,
										  'ses_alamat_ven' =>  $row->alamat,
										  'ses_notelp_ven' =>  $row->notelp,
										  'ses_fax_ven' =>  $row->fax,
										  'ses_grand'   => $row->gran_total,
										  'ses_ppn'   => $row->ppn,
										  'ses_gran_totalppn'   => $row->gran_totalppn,	
										  ); //buat array di session		
							$this->session->set_userdata($data); //simpan di session
						 }
							  //Update flag p.o yg sedang di cetak ------------------------------------	
							  $data_flag =array("flag_print_po" => "1", //give flag_print_po
												"flag_po" => "1",									
												);	
							  $this->db->where('id_master',$strid_master);
							  $this->db->update('tbl_master_pp',$data_flag);  				
							  //---------------------------------------------------------------------																	
							  return $query->result();										 
					  }else{
							  return false;
					  }
			  
	  
	 } 
	
	 function check_flag_po()// validasi untuk ceatk fpb jika nilai flag = 1 sudah tidak bisa di cetak lagi 
	{
		  $strid_master =$this->input->post('btnprintpo'); 		
		  $str_give_flag_po= "1";	  						  					    						   	 					  				   			  			   					
		  $query = $this->db->select("id_master") 
							->where('id_master',$strid_master)
							->where('flag_print_po',$str_give_flag_po)
							->get('tbl_master_pp');
		  if ($query->num_rows() > 0)
		  {					  
			return true;				
		  }	  
			
	}	 
	
	
    function check_flag_fpb()// validasi untuk ceatk fpb jika nilai flag = 1 sudah tidak bisa di cetak lagi 
	{
		 $strid_master =$this->input->post('msg'); 		
		 $str_give_flag_fpb= "1";	  						  					    						   	 			   	   					  				   			  			   			
			for ($i=0; $i < count($strid_master) ; $i++) { 				
				  $query = $this->db->select("id_master") 
									->where('id_master',$strid_master[$i])
									->where('flag_print_fpb',$str_give_flag_fpb)
									->get('qv_master_det_pp');
				  if ($query->num_rows() >= 1)
				  {					  
					return true;				
				  }
				  
			}
	}	
	
	function give_right_fpb_print()
	{
					
	  $strid_master =$this->input->post('msg'); 	 	  						  					    						   	 			   	   					  				   			  			   			
	 	for ($i=0; $i < count($strid_master) ; $i++) { 				
			  $query = $this->db->select("id_master,desc,spec,qty,harga,total,vendor,currency,ppn,pph,gran_totalppn,gran_total")
								->where('id_master',$strid_master[$i])
								->get('qv_master_det_pp');
			  if ($query->num_rows() >= 1)
			  {				
			    
				  foreach ($query->result() as $row)
				  {
			  		$data_right = array('ses_nopp' => $strid_master[$i]); 																							  					$this->session->set_userdata($data_right); //simpan di session
				  }	  								  
			     							  
				 //Update flag fpb yg sedang di cetak ------------------------------------
				  $str_give_flag_fpb= "0"; //0 jika di berikan izin untuk print ulang		
				  $data_flag =array("flag_print_fpb" => $str_give_flag_fpb, //give flag_print_fpb										
									);	
				  $this->db->where('id_master',$strid_master[$i]);
				  $this->db->update('tbl_master_pp',$data_flag); 				
				 //---------------------------------------------------------------------								 
					 
					 return $query->result();										 
			  }else{
					 return false;
			  }
	   	}	
	   
	 }		
 
 function m_mastergetid_pp()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('id_master,gran_total,ppn,pph,gran_totalppn');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_master_pp');		
		return $result->result();
		
	}	   	
	
	function m_getid_pp()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('*');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_detail_pp');		
		return $result->result();
		
	}

	public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}
				 
}