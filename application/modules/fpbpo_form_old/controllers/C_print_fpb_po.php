<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_print_fpb_po extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_print_fpb_po','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('terbilang');			
		$this->load->library('fpdf');   		
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_print_fpb_po->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
			$this->disconnect();
		}					
	}		  	
		
  public function index()
  {					    																
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  { 
	 
	$dept  = $this->session->userdata('dept') ;	
	$branch  = $this->session->userdata('name_branch') ; 
	$company = $this->session->userdata('short') ;		
	$data['intno'] = ""; //variable buat looping no table.				
	$data['header'] ="P.O & Received , FPB Print"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;													
	$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
	$data['show_view'] = 'fpbpo_form/V_print_fpb_po';$this->load->view('dashboard/Template',$data);	
					
	   
  }
   
    
     public function do_print_fpb()
  	{
		if ($this->M_print_fpb_po->check_flag_fpb())
		{   
		   $this->session->set_flashdata('pesan',"Data has been printed, Please contact your finance to give access for reprint FPB!!"); 		          
		   redirect('fpbpo_form/c_print_fpb_po');	   
		}else{
			if($this->M_print_fpb_po->select_data_rpt_fpb())
			 {
				 //------give flag already print FPB--------------------
				 $data_flag =array("flag_fpb" => '1', //give_flag_printfpb
									"flag_print_fpb" => '1', //give_flag_printfpb										
									"date_print_fpb" => date('Y-m-d')				
									);	 
				 $this->db->where('id_master',$this->session->userdata("sesidmaster"));
				 $this->db->update('tbl_master_pp',$data_flag);
				 //end---------------------------------------------------------------
				 
				 $hasil_fpb = $this->M_print_fpb_po->select_data_rpt_fpb();						
				 $res['currency'] = $hasil_fpb;				  
				 $res['data'] = $hasil_fpb;				  
				 $this->load->view('fpbpo_form/V_rptpdf_fpb',$res);
			  }else{     		
				 $this->session->set_flashdata('pesan_rpt','Data Permintaan Pembayaran Update Failed!!!!!! ....'); 											         
				 redirect('fpbpo_form/c_print_fpb_po');	
			 } 
		}
	}
	
	 public function do_print_po()	 
  	 {	
			if($this->M_print_fpb_po->check_flag_po()){
				 $this->session->set_flashdata('pesan_rpt','P.O Already Print !!!!!! ....'); 					
				 redirect('fpbpo_form/c_print_fpb_po');	
			}else{
				  if($this->M_print_fpb_po->select_data_rpt_po())
				  {  	
					 $res['data'] = $this->M_print_fpb_po->select_data_rpt_po();  
					 $this->load->view('fpbpo_form/V_rptpdf_po',$res);
				   }else{     	
					 $this->session->set_flashdata('pesan_rpt','Print P.O failed there find some errors !!!!!! ....'); 					
					 redirect('fpbpo_form/c_print_fpb_po');	
				  }
			 }
	 }
	 
	 
	  public function do_give_right_print_fpb()
      {		
		if($this->M_print_fpb_po->give_right_fpb_print())
		 {								    
		    $strnopp = $this->session->userdata('ses_nopp');
			$this->session->set_flashdata('pesan_give_right','PP No.'.$strnopp." ".'has been allow for reprint FPB!!'); 	
			$this->show_table(); //manggil fungsi show_table		
		  }else{     		
			 $this->session->set_flashdata('pesan_rpt','Give right Update Failed!!!!!! ....'); 					
			redirect('fpbpo_form/c_print_fpb_po');	
		  } 		 				 
  	  }	  
		 	 
    public function print_fpb_submit() //fungsi submit dengan banyak button dalam satu form
	{  	
		if ($this->input->post('btnprintfpb')){	
			$this->do_print_fpb();
		}else{
			if ($this->input->post('btnprintpo')){
				 $this->do_print_po();
			}else{
				if ($this->input->post('btncari')){	
			 	    $this->do_search_data();
				}else{	
				   if ($this->input->post('btncaridate')){	
			 	      $this->do_search_date();
				   }else{
						if ($this->input->post('btngiveright')){	
						   $this->do_give_right_print_fpb();
						}else{			
						   redirect('fpbpo_form/c_print_fpb_po');	
						}
				   }
				}				
			}
		} 
	}
	
	
	public function get_idtrans_modal()
   {		
     $tampungmaster = $this->M_print_fpb_po->m_mastergetid_pp();
	 $tampungdetail = $this->M_print_fpb_po->m_getid_pp();	 
	  
	 $no="1";	 	
	 echo "<div style='overflow-x:auto;'>" ;
	 echo "<table class='table table-striped table-bordered table-hover'>";
	 echo "<thead>";
	 echo  "<tr style='font-weight:bold; font-size:11px'>";
	 echo  "<td width='2%'>No</td>";
	 echo  "<td width='22%'>Description</td>";
	 echo  "<td width='22%'>Specs</td>";
	 echo  "<td width='10%'>P.O Reff</td>";
	 echo  "<td width='10%'>C.O.A</td>";
	 echo  "<td width='10%'>No Activity</td>";
	 echo  "<td width='7%'>Qty</td>";
	 echo  "<td width='15%'>Prices</td>";
	 echo  "<td width='40%'>Total Prices</td>";
	 echo  "<td width='7%'>PPN</td>";
	 echo  "<td width='7%'>PPH</td>";
	 echo  "<td width='25%'>PPN Amount</td>";
	 echo  "<td width='25%'>PPH Amount</td>";
	 echo  "</tr> "; 	 
	  echo "</thead>";	 	 				
		foreach ($tampungdetail as $row_jq) {
		    echo  "<tr style='font-size:12px'> ";
			echo '<td>'.$no++.'</td>';			
			echo '<td>'.$row_jq->desc.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			echo '<td>'.$row_jq->po_reff.'</td>';
			echo '<td>'.$row_jq->coa.'</td>';
			echo '<td>'.$row_jq->no_act.'</td>';
			echo '<td>'.$row_jq->qty.'</td>';
			echo '<td>'.number_format($row_jq->harga,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->total,2,'.',',').'</td>';
			echo '<td>'.$row_jq->tax_type.'</td>';
			echo '<td>'.$row_jq->tax_typepph.'</td>';
			echo '<td>'.number_format($row_jq->tax_detail,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->tax_detailpph,2,'.',',').'</td>';
		    echo "</tr>"; 							
			
		}
				
	 foreach ($tampungmaster as $row_jm) {	
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Sub Total :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_total,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPN Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->ppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 	
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPH Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->pph,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Total + Tax :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_totalppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo "</table>";	
		 echo "</div>";
	 }
}	

public function get_recieved_modal()
   {		
     $tampungmaster = $this->M_print_fpb_po->m_mastergetid_pp();
	 $tampungdetail = $this->M_print_fpb_po->m_getid_pp();	 
	  
	 $no="1";	 	
	 echo "<div style='overflow-x:auto;'>" ;
	 echo "<table class='table table-striped table-bordered table-hover'>";
	 echo  "<thead>" ;
	 echo  "<tr style='font-weight:bold; font-size:11px'>";
	 echo  "<td >No</td>";
	 echo  "<td>Description</td>";
	 echo  "<td>Specs</td>";
	 echo  "<td>P.O Reff</td>";
	 echo  "<td>C.O.A</td>";
	 echo  "<td>No Activity</td>";
	 echo  "<td>Qty</td>";
	 echo  "<td>Prices</td>";
	 echo  "<td>Total Prices</td>";
	/* echo  "<td width='7%'>PPN</td>";
	 echo  "<td width='7%'>PPH</td>";
	 echo  "<td width='25%'>PPN Amount</td>";
	 echo  "<td width='25%'>PPH Amount</td>"; */
	 echo  "</tr> "; 	
	 echo  "</thead>" ; 	 	 				
		foreach ($tampungdetail as $row_jq) {
		    echo  "<tr style='font-size:12px'> ";
			echo '<td>'.$no++.'</td>';			
			echo '<td>'.$row_jq->desc.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			echo '<td>'.$row_jq->po_reff.'</td>';
			echo '<td>'.$row_jq->coa.'</td>';
			echo '<td>'.$row_jq->no_act.'</td>';
			echo '<td>'.$row_jq->qty.'</td>';
			echo '<td>'.number_format($row_jq->harga,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->total,2,'.',',').'</td>';
			echo '<td>'.$row_jq->tax_type.'</td>';
			echo '<td>'.$row_jq->tax_typepph.'</td>';
			echo '<td>'.number_format($row_jq->tax_detail,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->tax_detailpph,2,'.',',').'</td>';
		    echo "</tr>"; 							
			
		}
				
	 foreach ($tampungmaster as $row_jm) {	
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Sub Total :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_total,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPN Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->ppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 	
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPH Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->pph,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Total + Tax :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_totalppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo "</table>";	
		 echo "</div>";
	 }
}	

	
function cath_data_fordatatables_po(){
	
	  //$str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
	  $str_idcompany = $this->session->userdata('id_company'); 
	  $str_iddept = $this->session->userdata('id_dept'); 
	  $str_status_send = "1";
	  $str_status_approved = "1";
	  /*Menagkap semua data yang dikirimkan oleh client*/

	 
	 $draw=$_REQUEST['draw'];
	 $length=$_REQUEST['length'];
	 $start=$_REQUEST['start'];
	 $search=$_REQUEST['search']["value"];

	 //order short column
	 $column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top");
	 
	 /*Menghitung total qv didalam database*/
	  $this->db->select('id_master');
	  $this->db->from('qv_head_pp_complite');
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->like("id_master",$search);
	  $this->db->or_like("user_submission",$search);
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");				
	  $this->db->where("status","1");
	  $this->db->or_like("header_desc",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->or_like("short",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->or_like("vendor",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $total = $this->db->count_all_results();

	 $output=array();
	 $output['draw']=$draw;
	 $output['recordsTotal']=$output['recordsFiltered']=$total;
	 $output['data']=array();

	 /*Jika $search mengandung nilai, berarti user sedang telah 
	 memasukan keyword didalam filed pencarian*/
	 if($search!=""){
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->like("id_master",$search);
			  $this->db->or_like("user_submission",$search);
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);		
			  $this->db->where("aprove_bod","1");		
			  $this->db->where("status","1");
			  $this->db->or_like("header_desc",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("short",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("vendor",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
	 }

		  /*Lanjutkan pencarian ke database*/
		  $this->db->limit($length,$start);		
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  $this->db->where("id_dept",$str_iddept);			 
		  $this->db->where("aprove_bod","1");
	      /*Urutkan dari alphabet paling terkahir*/
	      $this->db->where("status","1");
	      // $this->db->order_by('date_pp','DESC');
	     
		  //order column
		  if (isset($_REQUEST["order"])){
			   $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
		  }else{
			   $this->db->order_by('aprove_head','desc');
			   $this->db->order_by('approve_purchasing','desc');
			   $this->db->order_by('aprove_fc','desc');
			   $this->db->order_by('aprove_bod','desc');
		   }
	   
		   $this->db->order_by('aprove_head','asc');
		   $this->db->order_by('approve_purchasing','asc');
		   $this->db->order_by('aprove_fc','asc');
		   $this->db->order_by('aprove_bod','asc');	
		   $this->db->order_by('date_send_aproval','DESC');  
		   $query=$this->db->get('qv_head_pp_complite');


	 /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
	 dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
	 yang mengandung keyword tertentu
	 */
	 
	 if($search!=""){
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->like("id_master",$search);
			  $this->db->or_like("user_submission",$search);
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);		
			  $this->db->where("aprove_bod","1");		
			  $this->db->where("status","1");
			  $this->db->or_like("header_desc",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("short",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("vendor",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			 
		      $jum=$this->db->get('qv_head_pp_complite');
		      $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
	 }




	 
	 foreach ($query->result_array() as $row_tbl) {
		  
		  If ($row_tbl['attach_quo'] != "")
		   {  
			$attach_quo = '<div  align="center"><a href='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
		   }else{
			 $attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
		   }
	 
		
		
		     $btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i></a></center>';//button detail
		
		 If ($row_tbl['aprove_head'] == "1")
		  {
			 $head_approval = "<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";
			 
		  }
		  
		   If ($row_tbl['approve_purchasing'] == "1")
		   {
				 //$purchase_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 $purchase_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
			 
		  }
			
			If ($row_tbl['aprove_fc'] == "1")
		   {
				 //$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 $fc_aproval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
			  }else{
				   If ($row_tbl['status_send_aprove'] == "-1")
				   {
					 $fc_aproval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
				   }else{
					 $fc_aproval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
				   }
			}
			
			 
			
			   If ($row_tbl['aprove_bod'] == "1")
			  {
				  $bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
				 
			   }
		
$strbodaprove  = $row_tbl["aprove_bod"] ;
$strpurchaseaprove = $row_tbl["approve_purchasing"] ;
$strfcaprove   = $row_tbl["aprove_fc"] ;
$strheadaprove = $row_tbl["aprove_head"] ;	

          
		  
/*check---------------------------------
 $chk_idmaster ='<div align="center"><input id="checkchild" name="msgp[]" type="checkbox" value='.$row_tbl["id_master"].' class="ace" />
 <span class="lbl"></span> ';
*/
		  
		  
		  
		  /* $str_print = '<div  align="center"><a href="#" class="btn btn-app btn-danger btn-xs radius-8 printpobutton" id="btnprintpo" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i>print </a></center>';//button detail */
		   
 
			
			/*$str_print3 = '<input id="btnprintpo" name="btnprintpo" type="submit" value="Print P.O" class="btn btn-success printpobutton" />' ; */

		 				
			

//Pembedaan Warna pada type purchase.
		if ($row_tbl['flag_purchase'] == "1") {
			$type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
		}else{
			  if ($row_tbl['flag_purchase'] == "2") {
					$type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
				}else{
					  if ($row_tbl['flag_purchase'] == "3") {
						$type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
					}else{
						  if ($row_tbl['flag_purchase'] == "4") {
								$type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
							}else{
								 $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
						  };
				  };
			  };   
		  };
//end------------------------------			 

//date--------------
		if ($row_tbl['date_aprove_head'] ==""){
			$datehead = "" ;
		}else{
			$datehead  = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_head']))."</div>";
		}
		
		if ($row_tbl['date_approve_purchasing'] ==""){
			$datepur = "" ;
		}else{
			$datepur = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_approve_purchasing']))."</div>";
		}

		if ($row_tbl['date_aprove_fc'] ==""){
			 $datefc = "" ;
		}else{
			 $datefc= "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_fc']))."</div>";
		}

		if ($row_tbl['date_aprove_bod'] ==""){
			$datebod = "" ;
		}else{
			$datebod = "<div style='color:#EB293D'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_bod']))."</div>";
		}

//end date-----------------

//label colum 1 idepp
$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP
//end label
//-----------------------------------------------------------------------------------------------
$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";
//-----------------------------------------------------------------------------------------------
 
//button print p.o--
$strprintpo_ok = '<button class="btn btn-app btn-grey btn-xs radius-4 btnpo btn-clean printpobutton" type="submit" id="btnprintpo" name="btnprintpo" value ='.$row_tbl["id_master"].' onclick="window.location.href="" ><i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i> </button>';
//end---------------------------
 
	  
		 $output['data'][]=array(
								 $idmas,
								 $btn_view_detail,
								 $attach_quo,															
								 $strprintpo_ok,
								 $row_tbl['short'],
								 $row_tbl['user_submission'],
								 $row_tbl['dept'],
								 $header_desc ,//$row_tbl['header_desc'],
								 $row_tbl['vendor'],																								
								 date('d-m-Y', strtotime($row_tbl['date_pp'])),	
								 $type_purchase, //$row_tbl['type_purchase'],
								 $row_tbl['currency'],
								 number_format($row_tbl['gran_total'],2,'.',','),
								 number_format($row_tbl['gran_totalppn'],2,'.',','),
								 $row_tbl['term_top']." "."Days",
								 $head_approval.$datehead,
								 $purchase_approval.$datepur,
								 $fc_aproval.$datefc,
								 $bod_approval.$datebod,	
																 
						   );			
	 }
	 echo json_encode($output);
 }	
 
 function cath_data_fordatatables_rieceved(){
	
	//  $str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
	  $str_idcompany = $this->session->userdata('id_company'); 
	  $str_iddept = $this->session->userdata('id_dept'); 
	  $str_status_send = "1";
	  $str_status_approved = "1";
	
	 $draw=$_REQUEST['draw'];
	 $length=$_REQUEST['length'];
	 $start=$_REQUEST['start'];

	 /*Keyword yang diketikan oleh user pada field pencarian*/
	 $search=$_REQUEST['search']["value"];

	 //order short column
	 $column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top");
	 
	 /*Menghitung total qv didalam database*/
	  $this->db->select('id_master');
	  $this->db->from('qv_head_pp_complite');
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->like("id_master",$search);
	  $this->db->or_like("user_submission",$search);
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");				
	  $this->db->where("status","1");
	  $this->db->or_like("header_desc",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->or_like("short",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->or_like("vendor",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $total = $this->db->count_all_results();

	 /*Mempersiapkan array tempat kita akan menampung semua data
	 yang nantinya akan server kirimkan ke client*/
	 $output=array();

	 /*Token yang dikrimkan client, akan dikirim balik ke client*/
	 $output['draw']=$draw;

	 /*
	 $output['recordsTotal'] adalah total data sebelum difilter
	 $output['recordsFiltered'] adalah total data ketika difilter
	 Biasanya kedua duanya bernilai sama, maka kita assignment 
	 keduaduanya dengan nilai dari $total
	 */
	 $output['recordsTotal']=$output['recordsFiltered']=$total;

	 /*disini nantinya akan memuat data yang akan kita tampilkan 
	 pada table client*/
	 $output['data']=array();


	 /*Jika $search mengandung nilai, berarti user sedang telah 
	 memasukan keyword didalam filed pencarian*/
	 if($search!=""){
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->like("id_master",$search);
			  $this->db->or_like("user_submission",$search);
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);		
			  $this->db->where("aprove_bod","1");		
			  $this->db->where("status","1");
			  $this->db->or_like("header_desc",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("short",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("vendor",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
	 }

		  /*Lanjutkan pencarian ke database*/
		  $this->db->limit($length,$start);		
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  $this->db->where("id_dept",$str_iddept);			 
		  $this->db->where("aprove_bod","1");
	      /*Urutkan dari alphabet paling terkahir*/
	      $this->db->where("status","1");
	      // $this->db->order_by('date_pp','DESC');
	     
		  //order column
		  if (isset($_REQUEST["order"])){
			   $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
		  }else{
			   $this->db->order_by('aprove_head','desc');
			   $this->db->order_by('approve_purchasing','desc');
			   $this->db->order_by('aprove_fc','desc');
			   $this->db->order_by('aprove_bod','desc');
		   }
	   
		   $this->db->order_by('aprove_head','asc');
		   $this->db->order_by('approve_purchasing','asc');
		   $this->db->order_by('aprove_fc','asc');
		   $this->db->order_by('aprove_bod','asc');	
		   $this->db->order_by('date_send_aproval','DESC');  
		   $query=$this->db->get('qv_head_pp_complite');


	 /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
	 dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
	 yang mengandung keyword tertentu
	 */
	 
	 if($search!=""){
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->like("id_master",$search);
			  $this->db->or_like("user_submission",$search);
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);		
			  $this->db->where("aprove_bod","1");		
			  $this->db->where("status","1");
			  $this->db->or_like("header_desc",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("short",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("vendor",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			 
		      $jum=$this->db->get('qv_head_pp_complite');
		      $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
	 }




	 
	 foreach ($query->result_array() as $row_tbl) {
		  
		  If ($row_tbl['attach_quo'] != "")
		   {  
			$attach_quo = '<div  align="center"><a href='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
		   }else{
			 $attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
		   }
	 
		
		
		  $btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i></a></center>';//button detail
		
		 If ($row_tbl['aprove_head'] == "1")
		  {
			 $head_approval = "<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";
			 
		  }
		  
		   If ($row_tbl['approve_purchasing'] == "1")
		   {
				 //$purchase_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 $purchase_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
			 
		  }
			
			If ($row_tbl['aprove_fc'] == "1")
		   {
				 //$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 $fc_aproval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
			  }else{
				   If ($row_tbl['status_send_aprove'] == "-1")
				   {
					 $fc_aproval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
				   }else{
					 $fc_aproval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
				   }
			}
			
			 
			
			   If ($row_tbl['aprove_bod'] == "1")
			  {
					  $bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
				 
			   }
			  
		 
$strbodaprove  = $row_tbl["aprove_bod"] ;
$strpurchaseaprove = $row_tbl["approve_purchasing"] ;
$strfcaprove   = $row_tbl["aprove_fc"] ;
$strheadaprove = $row_tbl["aprove_head"] ;	

         $str_print = '<div  align="center"><a href="#" class="btn btn-app btn-danger btn-xs radius-8 printrecived" id="printrecived" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i>print </a></center>';//button detail

		/* $chk_idmaster ='<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value='.$row_tbl["id_master"].' class="ace" req_id_del='.$row_tbl["id_master"].' />
		<span class="lbl"></span> '; */				
			

//Pembedaan Warna pada type purchase.
		if ($row_tbl['flag_purchase'] == "1") {
			$type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
		}else{
			  if ($row_tbl['flag_purchase'] == "2") {
					$type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
				}else{
					  if ($row_tbl['flag_purchase'] == "3") {
						$type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
					}else{
						  if ($row_tbl['flag_purchase'] == "4") {
								$type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
							}else{
								 $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
						  };
				  };
			  };   
		  };
//end------------------------------			 

//date--------------
		if ($row_tbl['date_aprove_head'] ==""){
			$datehead = "" ;
		}else{
			$datehead  = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_head']))."</div>";
		}
		
		if ($row_tbl['date_approve_purchasing'] ==""){
			$datepur = "" ;
		}else{
			$datepur = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_approve_purchasing']))."</div>";
		}

		if ($row_tbl['date_aprove_fc'] ==""){
			 $datefc = "" ;
		}else{
			 $datefc= "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_fc']))."</div>";
		}

		if ($row_tbl['date_aprove_bod'] ==""){
			$datebod = "" ;
		}else{
			$datebod = "<div style='color:#EB293D'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_bod']))."</div>";
		}

//end date-----------------

//label colum 1 idepp
$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP
//end label

$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";


	  
		 $output['data'][]=array(
								// $chk_idmaster,
								 $idmas,
								 $btn_view_detail,
								 $attach_quo,															
								 $str_print	,
								 $row_tbl['short'],
								 $row_tbl['user_submission'],
								 $row_tbl['dept'],
								 $header_desc ,//$row_tbl['header_desc'],
								 $row_tbl['vendor'],																								
								 date('d-m-Y', strtotime($row_tbl['date_pp'])),	
								 $row_tbl['currency'],
								 $type_purchase, //$row_tbl['type_purchase'],
								 number_format($row_tbl['gran_total'],2,'.',','),
								 number_format($row_tbl['gran_totalppn'],2,'.',','),
								 $row_tbl['term_top']." "."Days",
								/* $head_approval.$datehead,
								 $purchase_approval.$datepur,
								 $fc_aproval.$datefc,
								 $bod_approval.$datebod, */	
																 
						   );			
	 }
	 echo json_encode($output);
 }	
 
 function cath_data_fordatatables_fpb(){
	
	//  $str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
	  $str_idcompany = $this->session->userdata('id_company'); 
	  $str_iddept = $this->session->userdata('id_dept'); 
	  $str_status_send = "1";
	  $str_status_approved = "1";
	 /*Menagkap semua data yang dikirimkan oleh client*/

	 /*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
	 server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
	 sesuai dengan urutan yang sebenarnya */
	 $draw=$_REQUEST['draw'];

	 /*Jumlah baris yang akan ditampilkan pada setiap page*/
	 $length=$_REQUEST['length'];

	 /*Offset yang akan digunakan untuk memberitahu database
	 dari baris mana data yang harus ditampilkan untuk masing masing page
	 */
	 $start=$_REQUEST['start'];

	 /*Keyword yang diketikan oleh user pada field pencarian*/
	 $search=$_REQUEST['search']["value"];

	 //order short column
	 $column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top");
	 
	 /*Menghitung total qv didalam database*/
	  $this->db->select('id_master');
	  $this->db->from('qv_head_pp_complite');
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->like("id_master",$search);
	  $this->db->or_like("user_submission",$search);
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");				
	  $this->db->where("status","1");
	  $this->db->or_like("header_desc",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->or_like("short",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $this->db->or_like("vendor",$search);	
	  $this->db->where("status_send_aprove",$str_status_send);
	  $this->db->where("id_company",$str_idcompany);
	  $this->db->where("id_dept",$str_iddept);
	  $this->db->where("aprove_bod","1");
	  $this->db->where("status","1");
	  $total = $this->db->count_all_results();

	 /*Mempersiapkan array tempat kita akan menampung semua data
	 yang nantinya akan server kirimkan ke client*/
	 $output=array();

	 /*Token yang dikrimkan client, akan dikirim balik ke client*/
	 $output['draw']=$draw;

	 /*
	 $output['recordsTotal'] adalah total data sebelum difilter
	 $output['recordsFiltered'] adalah total data ketika difilter
	 Biasanya kedua duanya bernilai sama, maka kita assignment 
	 keduaduanya dengan nilai dari $total
	 */
	 $output['recordsTotal']=$output['recordsFiltered']=$total;

	 /*disini nantinya akan memuat data yang akan kita tampilkan 
	 pada table client*/
	 $output['data']=array();


	 /*Jika $search mengandung nilai, berarti user sedang telah 
	 memasukan keyword didalam filed pencarian*/
	 if($search!=""){
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->like("id_master",$search);
			  $this->db->or_like("user_submission",$search);
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);		
			  $this->db->where("aprove_bod","1");		
			  $this->db->where("status","1");
			  $this->db->or_like("header_desc",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("short",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("vendor",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
	 }

		  /*Lanjutkan pencarian ke database*/
		  $this->db->limit($length,$start);		
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  $this->db->where("id_dept",$str_iddept);			 
		  $this->db->where("aprove_bod","1");
	      /*Urutkan dari alphabet paling terkahir*/
	      $this->db->where("status","1");
	      // $this->db->order_by('date_pp','DESC');
	     
		  //order column
		  if (isset($_REQUEST["order"])){
			   $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
		  }else{
			   $this->db->order_by('aprove_head','desc');
			   $this->db->order_by('approve_purchasing','desc');
			   $this->db->order_by('aprove_fc','desc');
			   $this->db->order_by('aprove_bod','desc');
		   }
	   
		   $this->db->order_by('aprove_head','asc');
		   $this->db->order_by('approve_purchasing','asc');
		   $this->db->order_by('aprove_fc','asc');
		   $this->db->order_by('aprove_bod','asc');	
		   $this->db->order_by('date_send_aproval','DESC');  
		   $query=$this->db->get('qv_head_pp_complite');


	 /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
	 dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
	 yang mengandung keyword tertentu
	 */
	 
	 if($search!=""){
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->like("id_master",$search);
			  $this->db->or_like("user_submission",$search);
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);		
			  $this->db->where("aprove_bod","1");		
			  $this->db->where("status","1");
			  $this->db->or_like("header_desc",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("short",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			  $this->db->or_like("vendor",$search);	
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  $this->db->where("id_dept",$str_iddept);
			  $this->db->where("aprove_bod","1");
			  $this->db->where("status","1");
			 
		      $jum=$this->db->get('qv_head_pp_complite');
		      $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
	 }




	 
	 foreach ($query->result_array() as $row_tbl) {
		  
		  If ($row_tbl['attach_quo'] != "")
		   {  
			$attach_quo = '<div  align="center"><a href='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
		   }else{
			 $attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
		   }
	 
		
		
		  $btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i></a></center>';//button detail
		
		
			
			 
			
			   If ($row_tbl['aprove_bod'] == "1")
			  {
					  $bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
				 
			   }
			  
		 
$strbodaprove  = $row_tbl["aprove_bod"] ;
$strpurchaseaprove = $row_tbl["approve_purchasing"] ;
$strfcaprove   = $row_tbl["aprove_fc"] ;
$strheadaprove = $row_tbl["aprove_head"] ;			
			

//Pembedaan Warna pada type purchase.
		if ($row_tbl['flag_purchase'] == "1") {
			$type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
		}else{
			  if ($row_tbl['flag_purchase'] == "2") {
					$type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
				}else{
					  if ($row_tbl['flag_purchase'] == "3") {
						$type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
					}else{
						  if ($row_tbl['flag_purchase'] == "4") {
								$type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
							}else{
								 $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
						  };
				  };
			  };   
		  };
//end------------------------------			 


//label colum 1 idepp
$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP

//end label
$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";

//tombol fpb......
$strprintfbp_ok = '<button class="btn btn-app btn-success btn-xs radius-4 btnpo btn-clean fpbbutton" type="button" id="fpbbutton" name="fpbbutton" value ='.$row_tbl["id_master"].' onclick="window.location.href="" ><i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i> </button>';
//end---------------------------
	  
		 $output['data'][]=array(
								 $idmas,
								// $btn_view_detail,
								// $attach_quo,															
								 $strprintfbp_ok,
								 $row_tbl['short'],
								 $row_tbl['user_submission'],
								 $row_tbl['dept'],
								 $header_desc ,//$row_tbl['header_desc'],
								 $row_tbl['vendor'],																								
								 date('d-m-Y', strtotime($row_tbl['date_pp'])),
								 $row_tbl['term_top']." "."Days",									 
								 $type_purchase, //$row_tbl['type_purchase'],
								 $row_tbl['currency'],
								 number_format($row_tbl['gran_total'],2,'.',','),
								 number_format($row_tbl['gran_totalppn'],2,'.',','),
										 
						   );			
	 }
	 echo json_encode($output);
 }	
 
 

	
 

	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }
  
 
	
}

