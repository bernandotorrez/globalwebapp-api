<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<?php if ($this->session->userdata('aproval_flag') =='0'){ ?>

<!--<div class="widget-box">
    <div class="widget-header widget-header-flat widget-header-small">
            <h5 class="widget-title">
                <i class="ace-icon fa fa-tags"></i>
                Information
            </h5>                  
    	</div>
</div> -->
            
 <?php //$this->load->view('dashboard/animation') ?>

 <!-- Responsive Form for Mobile and Web Start -->

<!-- New Dashboard -->
<!-- New Dashboard -->


 <div class="container">

 <h3 class="text-center">EPP Summary</h3>
 <hr style="border-top: 5px solid #8c8b8b; ">
    
        <div class="col-sm-12 infobox-container click" >
            <div class="infobox infobox-green">                
                <div class="infobox-icon">                                       
                    <i class="ace-icon fa fa-external-link-square"></i>    
                </div>
				
                <div class="infobox-data ">
                    <span class="infobox-data-number"><?php echo $data_tampil_submission ?></span>
                    <div class="infobox-content"> EPP Submission </div>
                </div>			
            </div>
           
            <div class="infobox infobox-orange">
                <div class="infobox-icon">
                    <i class="ace-icon fa fa-refresh"></i>
                </div>

                <div class="infobox-data">
                    <span class="infobox-data-number"><?php echo $data_waiting_approval ?></span>
                    <div class="infobox-content">EPP Pending Approval </div>
                </div>                
            </div>
            
             <div class="infobox infobox-red">
                <div class="infobox-icon">
                    <i class="ace-icon fa fa-bolt"></i>
                </div>

                <div class="infobox-data">
                    <span class="infobox-data-number"><?php echo $data_reject_approval ?></span>
                    <div class="infobox-content">EPP Rejected</div>
                   
                </div>
            </div>
            
             <div class="infobox infobox-blue">
                <div class="infobox-icon">
                    <i class="ace-icon fa fa-check-square-o"></i>
                </div>

                <div class="infobox-data">
                    <span class="infobox-data-number"><?php echo $data_approve_approval ?></span>
                    <div class="infobox-content"> EPP Approved</div>
                </div>

            </div>           

            <div class="infobox infobox-orange2">      
                <div class="infobox-icon">         
                    <i class="ace-icon fa fa-bar-chart"></i>
                </div>

                <div class="infobox-data">
                    <span class="infobox-data-number"><?php echo $data_all_approval ?></span>
                    <div class="infobox-content">EPP Total Submission</div>
                </div>
            </div>
           
            <div class="space-8"></div>     

        <div class="space-24"></div>

       <!-- Demo Car Summary -->
       <h3 class="text-center">Demo Car Summary</h3>
 <hr style="border-top: 5px solid #8c8b8b; ">
    
        <div class="col-sm-12 infobox-container click" >
            <div class="infobox infobox-green">                
                <div class="infobox-icon">                                       
                    <i class="ace-icon fa fa-external-link-square"></i>    
                </div>
				
                <div class="infobox-data ">
                    <span class="infobox-data-number"><?php echo $data_tampil_democar_submission ?></span>
                    <div class="infobox-content"> Demo Car Submission </div>
                </div>			
            </div>
           
            <div class="infobox infobox-orange">
                <div class="infobox-icon">
                    <i class="ace-icon fa fa-refresh"></i>
                </div>

                <div class="infobox-data">
                    <span class="infobox-data-number"><?php echo $data_waiting_democar_approval ?></span>
                    <div class="infobox-content">Demo Car Pending Approval </div>
                </div>                
            </div>
            
             <div class="infobox infobox-red">
                <div class="infobox-icon">
                    <i class="ace-icon fa fa-bolt"></i>
                </div>

                <div class="infobox-data">
                    <span class="infobox-data-number"><?php echo $data_reject_democar_approval ?></span>
                    <div class="infobox-content">Demo Car Rejected</div>
                   
                </div>
            </div>
            
             <div class="infobox infobox-blue">
                <div class="infobox-icon">
                    <i class="ace-icon fa fa-check-square-o"></i>
                </div>

                <div class="infobox-data">
                    <span class="infobox-data-number"><?php echo $data_approve_democar_approval ?></span>
                    <div class="infobox-content"> Demo Car Approved</div>
                </div>

            </div>           

            <div class="infobox infobox-orange2">      
                <div class="infobox-icon">         
                    <i class="ace-icon fa fa-bar-chart"></i>
                </div>

                <div class="infobox-data">
                    <span class="infobox-data-number"><?php echo $data_all_democar_approval ?></span>
                    <div class="infobox-content">Demo Car Total Submission</div>
                </div>
            </div>
           
            <div class="space-8"></div>  
       <!-- Demo Car Summary -->

       <div class="space-24"></div>
       

<!-- Book Meeting Summary -->
<h3 class="text-center">Book Meeting Summary</h3>
<hr style="border-top: 5px solid #8c8b8b; ">

 <div class="col-sm-12 infobox-container click" >
     <div class="infobox infobox-orange2">      
         <div class="infobox-icon">         
             <i class="ace-icon fa fa-bar-chart"></i>
         </div>

         <div class="infobox-data">
             <span class="infobox-data-number"><?php echo $data_all_bookmeeting_approval ?></span>
             <div class="infobox-content">Book Meeting Total</div>
         </div>
     </div>
    
     <div class="space-8"></div>  

     <div class="space-24"></div>                         
 </div><!-- /.container -->
<!-- Book Meeting Summary -->   

<div class="space-8"></div>
        <div class="space-24"></div>

       <!-- SPK Summary -->
       <h3 class="text-center">SPK Summary</h3>
 <hr style="border-top: 5px solid #8c8b8b; ">
    
        <div class="col-sm-12 infobox-container click" >
            <div class="infobox infobox-green">                
                <div class="infobox-icon">                                       
                    <i class="ace-icon fa fa-external-link-square"></i>    
                </div>
				
                <div class="infobox-data ">
                    <span class="infobox-data-number"><?php echo $data_spk_mazda; ?></span>
                    <div class="infobox-content"> SPK Mazda </div>
                </div>			
            </div>
           
            <div class="infobox infobox-orange">
                <div class="infobox-icon">
                    <i class="ace-icon fa fa-external-link-square"></i>
                </div>

                <div class="infobox-data">
                    <span class="infobox-data-number"><?php echo $data_spk_bmw; ?></span>
                    <div class="infobox-content">SPK BMW </div>
                </div>                
            </div>
            
             <div class="infobox infobox-red">
                <div class="infobox-icon">
                    <i class="ace-icon fa fa-external-link-square"></i>
                </div>

                <div class="infobox-data">
                    <span class="infobox-data-number"><?php echo $data_spk_mas; ?></span>
                    <div class="infobox-content">SPK Maserati</div>
                   
                </div>
            </div>
           
            <div class="space-8"></div>  
       <!-- SPK Summary -->

       <div class="space-24"></div>

<!-- Modal -->
 <div class="modal fade" id="ViewDetailModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Purchase</h4>
                    </div>
                    
                    <div class="modal-body">
                           
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal --> 

<?php } else if ($this->session->userdata('aproval_flag') =='3'){ 
    
    redirect('approval_pr_form/c_table_aproval_pr');

    } else {
        redirect('approval_form/c_table_aproval');
    }
?>


	

<script>
//get data from control to modal popup.
$(function(){
  $(document).on('click_submission','.detail',function(e){
		var req_id = <?php echo $this->session->userdata('id_dept'); ?> //$(this).attr('req_id');  
		var url = '<?php //echo site_url("epp_form/c_create_pp/get_idtrans_modal"); ?>';
		
		$("#ViewDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_dept:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		}); 			
		
   });	
});
</script>    
    
