<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_main_menu extends MY_Controller
{
  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_main_menu','',TRUE);
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_main_menu->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}		
	}		  	
		
  public function index()
  {		

      $this->show_data(); 

  }
  
  public function show_data()
  {                          
        $buff['ses_date'] = $this->M_main_menu->tampil_date_time();							
	    $this->session->set_userdata($buff);	
		
		$aproval_flag = $this->session->userdata('aproval_flag');
		$id_company = $this->session->userdata('id_company');
		$id_dept = $this->session->userdata('id_dept');
		$name = $this->session->userdata('name');
		$email = $this->session->userdata('email');

		$data = array('aproval_flag' => $aproval_flag, 'id_company' => $id_company, 'id_dept' => $id_dept,'name' => $name, 'email' => $email);
		$parameter = http_request_get(API_URL.'/mainMenu/getSummaryDashboard', $data);
		$query = json_decode($parameter, true);

		$data['data_tampil_submission'] = $query['data']['epp_submission'];
		$data['data_waiting_approval'] = $query['data']['epp_pending_approval'];
		$data['data_reject_approval'] = $query['data']['epp_rejected'];
		$data['data_approve_approval'] = $query['data']['epp_approved'];
		$data['data_all_approval'] = $query['data']['epp_all'];

		$data['data_tampil_democar_submission'] = $query['data']['democar_submission'];
		$data['data_waiting_democar_approval'] = $query['data']['democar_pending_approval'];
		$data['data_reject_democar_approval'] = $query['data']['democar_rejected'];
		$data['data_approve_democar_approval'] = $query['data']['democar_approved'];	
		$data['data_all_democar_approval'] = $query['data']['democar_all'];

		$data['data_all_bookmeeting_approval'] = $query['data']['book_meeting_all'];

		$data['data_spk_mazda'] = $query['data']['spk_mazda'];
		$data['data_spk_bmw'] = $query['data']['spk_bmw'];
		$data['data_spk_mas'] = $query['data']['spk_mas'];
						
		$data['show_view'] = 'main_menu/Main_menu_admin_view';	   				 			
  	 	$this->load->view('dashboard/Template',$data);		
	
  }
  
  
  /* public function get_idtrans_modal()
	{				
		$striddep= $this->input->post('id_pp');					
		$this->db->select('id_master,desc,spec,qty,harga,total,po_reff');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_detail_pp');						 
		 
		 $tampungdetail = $this->M_create_pp->m_getid_pp();
		 		 		 
		 $no="1";	 
		 $cathtotal="0";
		 
		 echo "<table class='table table-striped table-bordered table-hover'>";
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='2%'>No</td>";
		 echo  "<td width='22%'>Description</td>";
		 echo  "<td width='22%' >Specs</td>";
		 echo  "<td width='10%'>P.O </td>";
		 echo  "<td width='7%'>Qty</td>";
		 echo  "<td width='25%'>Prices</td>";
		 echo  "<td width='40%'>Total Prices</td>";
		 echo  "</tr> "; 	 	 	 				
			foreach ($tampungdetail as $row_jq) {
				echo  "<tr> ";
				echo '<td>'.$no++.'</td>';			
				echo '<td>'.$row_jq->desc.'</td>';
				echo '<td>'.$row_jq->spec.'</td>';
				echo '<td>'.$row_jq->po_reff.'</td>';
				echo '<td>'.$row_jq->qty.'</td>';
				echo '<td>'. number_format($row_jq->harga,2,'.',',').'</td>';
				echo '<td>'. number_format($row_jq->total,2,'.',',').'</td>';
				echo "</tr>"; 							
				$cathtotal = $cathtotal + $row_jq->total  ;
			}
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='6' align='right'>Sub Total :</td>";
		 echo  "<td width='40%'>".number_format($cathtotal,2,'.',',')."</td>";
		 echo  "</tr> "; 								
		 echo "</table>";	
	}

	*/

		public function disconnect()
  {
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('id' => $strid_user, 'login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);
		//$this->db->where('id',$strid_user);
		//$this->db->update('tbl_user',$data); // update login terakhir user

		$update = http_request_put(API_URL.'/login/doLogout', $data);
		$update = json_decode($update, true);
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }

	 
	
}

