<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_main_menu extends CI_Model {	
				
    public $db_tabel       = 'qv_branch_company';	
   
   
	function tampil_date_time()
	{
	  //bikin fungsi hari tanggal bulan tahun dan jam untuk di simpan di sesion
	   date_default_timezone_set('Asia/Jakarta');
	   $array_hr= array(1=>"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday");
	   $hr = $array_hr[date('N')];
	   $ddate = date('d F Y');
	   $strfudate= $hr.",$ddate " ;			   
	   return $strfudate;			   	
	}
	
	// function tampil_submission()
	// {
		
	//   $buff_approvalflag = $this->session->userdata('aproval_flag');
	//   $buff_idcompany = $this->session->userdata('id_company');
	//   $buff_iddept = $this->session->userdata('id_dept');
	//   $buff_name= $this->session->userdata('name');
	//   $sendapprove ="0" ; //terkirim  
	   	
	//   if ($buff_approvalflag !=0) {
	// 	$query = $this->db->query("select * from tbl_master_pp where status_send_aprove = '".$sendapprove."' and id_company ='". $buff_idcompany."'" ) ;				
	// 	return $query;		
	//   }else{	
	//     $query = $this->db->query("select * from tbl_master_pp where status_send_aprove = '".$sendapprove."' and id_company ='". $buff_idcompany."'and id_dept ='".$buff_iddept."' and user_submission ='".$buff_name."'" ) ;				
	// 	return $query;			  
	//   }
	// }
	
	// function tampil_reject()
	// {
		
	//   $buff_approvalflag = $this->session->userdata('aproval_flag');
	//   $buff_idcompany = $this->session->userdata('id_company');
	//   $buff_iddept = $this->session->userdata('id_dept');
	//   $buff_name= $this->session->userdata('name');
	//   $sendapprove ="-1" ; //terkirim  
	   	
	//   if ($buff_approvalflag !=0) {
	// 	$query = $this->db->query("select * from tbl_master_pp where status_send_aprove = '".$sendapprove."' and id_company ='". $buff_idcompany."'" ) ;				
	// 	return $query;		
	//   }else{	
	//     $query = $this->db->query("select * from tbl_master_pp where status_send_aprove = '".$sendapprove."' and id_company ='". $buff_idcompany."'and id_dept ='".$buff_iddept."'and user_submission ='".$buff_name."'") ;				
	// 	return $query;			  
	//   }
		
			
	// }
	
	// function tampil_approve()
	// {
	//    $buff_approvalflag = $this->session->userdata('aproval_flag');
	//    $buff_idcompany = $this->session->userdata('id_company');
	//    $buff_iddept = $this->session->userdata('id_dept');
	//    $buff_name= $this->session->userdata('name');
	//    $aprove_bod ="1" ; //bod approve  
	   	
	//   if ($buff_approvalflag !=0) {
	// 	$query = $this->db->query("select * from tbl_master_pp where aprove_bod = '".$aprove_bod."' and id_company ='". $buff_idcompany."'" ) ;				
	// 	return $query;		
	//   }else{	
	//     $query = $this->db->query("select * from tbl_master_pp where aprove_bod = '".$aprove_bod."' and id_company ='". $buff_idcompany."'and id_dept ='".$buff_iddept."'and user_submission ='".$buff_name."'") ;				
	// 	return $query;			  
	//   }		
	// }
	
	// function waiting_approve()
	// {
		
	//    $buff_approvalflag = $this->session->userdata('aproval_flag');
	//    $buff_idcompany = $this->session->userdata('id_company');
	//    $buff_iddept = $this->session->userdata('id_dept');	   
	//    $buff_name= $this->session->userdata('name');
	//    $sendapprove ="1" ; //terkirim  
	   	
	//   if ($buff_approvalflag !=0) {
	// 	 $query = $this->db->query("select * from tbl_master_pp where status_send_aprove = '".$sendapprove."' and id_company ='". $buff_idcompany."'" ) ;				
	// 	 return $query;		
	//   }else{	
	//      $query = $this->db->query("select * from tbl_master_pp where status_send_aprove = '".$sendapprove."' and  id_company ='". $buff_idcompany."' and id_dept ='".$buff_iddept."'and user_submission ='".$buff_name."'") ;				
	// 	 return $query;			  
	//   }
			
	// }
	
	
	// function all_subbmission()
	// {
	//    $buff_approvalflag = $this->session->userdata('aproval_flag');
	//    $buff_idcompany = $this->session->userdata('id_company');
	//    $buff_iddept = $this->session->userdata('id_dept');
	//    $buff_name= $this->session->userdata('name');	  
	   	
	//   if ($buff_approvalflag !=0) {
	// 	 $query = $this->db->query("select * from tbl_master_pp where id_company ='". $buff_idcompany."'" ) ;				
	// 	 return $query;		
	//   }else{	
	//      $query = $this->db->query("select * from tbl_master_pp where id_company ='". $buff_idcompany."'and id_dept ='".$buff_iddept."'and user_submission ='".$buff_name."'" ) ;				
	// 	 return $query;			  
	//   }
			
	// }
	
	public function get_value($where, $val, $table){
		// $this->db->where($where, $val);
		// $_r = $this->db->get($table);
		// $_l = $_r->result();
		// return $_l;

		$parameter = array('id' => $val);	
		$query = http_request_get(API_URL.'/login/get', $parameter);
		$query = json_decode($query);
		return $query->data;
	}
	
	// public function tampil_submission_democar() {
	// 	$buff_approvalflag = $this->session->userdata('aproval_flag');
	// 	$buff_idcompany = $this->session->userdata('id_company');
	// 	$buff_iddept = $this->session->userdata('id_dept');
	// 	$buff_name= $this->session->userdata('name');
	// 	$submission_email = $this->session->userdata('email');
	// 	$sendapprove ="0" ; //terkirim  
	   	
	// 	if ($buff_approvalflag !=0) {
	// 		$query = "SELECT id_req_democar_group FROM qv_req_demo_car WHERE send_approval = 0 AND sign_1='0' AND sign_2='0' AND id_company='". $buff_idcompany."'";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;		
	// 	}else{	
	// 		$query = "SELECT id_req_democar_group FROM qv_req_demo_car WHERE send_approval = 0 AND sign_1='0' AND sign_2='0' AND id_company='". $buff_idcompany."' AND submission_email='".$submission_email."' ";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;			  
	// 	}
	// }
		
	// public function waiting_approve_democar() {
	// 	$buff_approvalflag = $this->session->userdata('aproval_flag');
	// 	$buff_idcompany = $this->session->userdata('id_company');
	// 	$buff_iddept = $this->session->userdata('id_dept');
	// 	$buff_name= $this->session->userdata('name');
	// 	$submission_email = $this->session->userdata('email');
	// 	$sendapprove ="0" ; //terkirim  
	   	
	// 	if ($buff_approvalflag !=0) {
	// 		$query = "SELECT id_req_democar_group 
	// 		FROM qv_req_demo_car 
	// 		WHERE send_approval = 1 AND sign_1='0' OR send_approval = 1 AND sign_2='0' AND id_company='". $buff_idcompany."'
	// 		";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;		
	// 	}else{	
	// 		$query = "SELECT id_req_democar_group 
	// 		FROM qv_req_demo_car 
	// 		WHERE send_approval = 1 AND sign_1='0' OR send_approval = 1 AND sign_2='0' AND id_company='". $buff_idcompany."' AND submission_email='".$submission_email."'
	// 		";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;			  
	// 	}
	// }

	// public function tampil_reject_democar() {
	// 	$buff_approvalflag = $this->session->userdata('aproval_flag');
	// 	$buff_idcompany = $this->session->userdata('id_company');
	// 	$buff_iddept = $this->session->userdata('id_dept');
	// 	$buff_name= $this->session->userdata('name');
	// 	$submission_email = $this->session->userdata('email');
	// 	$sendapprove ="0" ; //terkirim  
	   	
	// 	if ($buff_approvalflag !=0) {
	// 		$query = "SELECT id_req_democar_group FROM qv_req_demo_car WHERE send_approval = 2 AND sign_1='2' OR send_approval = 2 AND sign_2='2' AND id_company='". $buff_idcompany."'";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;		
	// 	}else{	
	// 		$query = "SELECT id_req_democar_group FROM qv_req_demo_car WHERE send_approval = 2 AND sign_1='2' OR send_approval = 2 AND sign_2='2' AND id_company='". $buff_idcompany."' AND submission_email='".$submission_email."' ";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;			  
	// 	}
	// }

	// public function tampil_approve_democar() {
	// 	$buff_approvalflag = $this->session->userdata('aproval_flag');
	// 	$buff_idcompany = $this->session->userdata('id_company');
	// 	$buff_iddept = $this->session->userdata('id_dept');
	// 	$buff_name= $this->session->userdata('name');
	// 	$submission_email = $this->session->userdata('email');
	// 	$sendapprove ="0" ; //terkirim  
	   	
	// 	if ($buff_approvalflag !=0) {
	// 		$query = "SELECT id_req_democar_group FROM qv_req_demo_car WHERE send_approval = 2 AND sign_1='1' AND sign_2='1' AND id_company='". $buff_idcompany."'";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;		
	// 	}else{	
	// 		$query = "SELECT id_req_democar_group FROM qv_req_demo_car WHERE send_approval = 2 AND sign_1='1' AND sign_2='1' AND id_company='". $buff_idcompany."' AND submission_email='".$submission_email."' ";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;			  
	// 	}
	// }

	// public function all_subbmission_democar() {
	// 	$buff_approvalflag = $this->session->userdata('aproval_flag');
	// 	$buff_idcompany = $this->session->userdata('id_company');
	// 	$buff_iddept = $this->session->userdata('id_dept');
	// 	$buff_name= $this->session->userdata('name');
	// 	$submission_email = $this->session->userdata('email');
	// 	$sendapprove ="0" ; //terkirim  
	   	
	// 	if ($buff_approvalflag !=0) {
	// 		$query = "SELECT id_req_democar_group FROM qv_req_demo_car WHERE id_company='". $buff_idcompany."'";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;		
	// 	}else{	
	// 		$query = "SELECT id_req_democar_group FROM qv_req_demo_car WHERE id_company='". $buff_idcompany."' AND submission_email='".$submission_email."' ";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;			  
	// 	}
	// }
		
	// // Book Meeting
	
	// public function all_subbmission_bookmeeting() {
	// 	$buff_approvalflag = $this->session->userdata('aproval_flag');
	// 	$buff_idcompany = $this->session->userdata('id_company');
	// 	$buff_iddept = $this->session->userdata('id_dept');
	// 	$buff_name= $this->session->userdata('name');
	// 	$submission_email = $this->session->userdata('email');
	// 	$sendapprove ="0" ; //terkirim  
	   	
	// 	if ($buff_approvalflag !=0) {
	// 		$query = "SELECT id_book FROM qv_room_book_meeting WHERE id_company='". $buff_idcompany."' AND status='1'";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;		
	// 	}else{	
	// 		$query = "SELECT id_book FROM qv_room_book_meeting WHERE id_company='". $buff_idcompany."' AND id_dept ='".$buff_iddept."' and user_submission ='".$buff_name."' AND status='1' ";
	// 		$query = $this->db->query($query) ;				
	// 		return $query;			  
	// 	}
	// }
	// // Book Meeting


	// // SPK Summary
	// public function count_spk_mazda() {
	// 	$buff_approvalflag = $this->session->userdata('aproval_flag');
	// 	$buff_idcompany = $this->session->userdata('id_company');
	// 	$buff_iddept = '3';
	// 	$buff_name= $this->session->userdata('name');
	// 	$submission_email = $this->session->userdata('email');
	// 	$sendapprove ="0" ; //terkirim  

	// 	$query = "SELECT count(id_trans_sales) as total_data FROM qv_complite_master_sell WHERE nospk IS NOT NULL AND status_flag_sell = '2' AND status = '1'";
	// 		$query = $this->db->query($query) ;				
	// 		return $query->result();

	// }

	// public function count_spk_bmw() {
	// 	$buff_approvalflag = $this->session->userdata('aproval_flag');
	// 	$buff_idcompany = $this->session->userdata('id_company');
	// 	$buff_iddept = '3';
	// 	$buff_name= $this->session->userdata('name');
	// 	$submission_email = $this->session->userdata('email');
	// 	$sendapprove ="0" ; //terkirim  

	// 	$query = "SELECT count(id_trans_sales) as total_data FROM qv_complite_master_sell_bmw WHERE nospk IS NOT NULL AND status_flag_sell = '2' AND status = '1'";
	// 		$query = $this->db->query($query) ;				
	// 		return $query->result();

	// }

	// public function count_spk_mas() {
	// 	$buff_approvalflag = $this->session->userdata('aproval_flag');
	// 	$buff_idcompany = $this->session->userdata('id_company');
	// 	$buff_iddept = '3';
	// 	$buff_name= $this->session->userdata('name');
	// 	$submission_email = $this->session->userdata('email');
	// 	$sendapprove ="0" ; //terkirim  

	// 	$query = "SELECT count(id_trans_sales) as total_data FROM qv_complite_master_sell_mas WHERE nospk IS NOT NULL AND status_flag_sell = '2' AND status = '1'";
	// 		$query = $this->db->query($query) ;				
	// 		return $query->result();

	// }
	// SPK Summary
}