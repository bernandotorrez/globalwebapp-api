<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_branch extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_branch');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_branch->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}	
	 	}
		
	public function index()
		{

		$data['branch']=$this->M_branch->get_all_branch();
		$data['company']=$this->M_branch->get_all_company();
		$data['area']=$this->M_branch->get_all_area();
		$data['total']=$this->M_branch->get_count_id();
		$data['show_view'] = 'branch_form/V_branch';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		}
	public function ajax_list()
	{
		$list = $this->M_branch->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $qv_branch) {
			$chk_idmaster ='<div align="center"><input id="checkdept" name="checkdept" type="checkbox" value='.$qv_branch->branch_id.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $qv_branch->branch_id;
			$row[] = $qv_branch->company;
			$row[] = $qv_branch->name_branch;
			$row[] = $qv_branch->address;
			$row[] = $qv_branch->city;
			$row[] = $qv_branch->phone;
			$row[] = $qv_branch->fax;
			$row[] = $qv_branch->email;
			$row[] = $qv_branch->area_name;
			$row[] = $qv_branch->branch_short;
			$row[] = $qv_branch->date_create;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_branch->count_all(),
						"recordsFiltered" => $this->M_branch->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('branch_id');
		$this->db->from('qv_branch');
		$this->db->where('status','1');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("branch_id",$search);
		$this->db->where('status','1');
		$this->db->or_like("company",$search);
		$this->db->where('status','1');
		$this->db->or_like("name_branch",$search);
		$this->db->where('status','1');
		$this->db->or_like("address",$search);
		$this->db->where('status','1');
		$this->db->or_like("city",$search);
		$this->db->where('status','1');
		$this->db->or_like("phone",$search);
		$this->db->where('status','1');
		$this->db->or_like("fax",$search);
		$this->db->where('status','1');
		$this->db->or_like("email",$search);
		$this->db->where('status','1');
		$this->db->or_like("area_name",$search);
		$this->db->where('status','1');
		$this->db->or_like("brand_name",$search);
		$this->db->where('status','1');
		$this->db->or_like("branch_short",$search);
		$this->db->where('status','1');
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		$this->db->where('status','1');
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('branch_id','ASC');
		$query=$this->db->get('qv_branch');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("branch_id",$search);
		$jum=$this->db->get('qv_branch');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $tbl_branches) {
		$chk_idmaster ='<div align="center"><input id="checkbranch" name="checkbranch" type="checkbox" value='.$tbl_branches["branch_id"].' class="editRow ace" req_id_del='.$tbl_branches["branch_id"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($tbl_branches['branch_id'],$tbl_branches['company'],
									$tbl_branches['name_branch'],
									$tbl_branches['address'],$tbl_branches['city'],
									$tbl_branches['phone'],$tbl_branches['fax'],
									$tbl_branches['email'],$tbl_branches['area_name'],
									$tbl_branches['brand_name'],$tbl_branches['branch_short'],
									$chk_idmaster
									);

		}

		echo json_encode($output);


	}
		
	public function add_branch()
		{
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_add = date('Y-m-d H:i:s');
		
		if(empty($_POST["id_company"])){
			die ("Field ID Company must be filled in!! ");
		}
		elseif(empty($_POST["name_branch"])){
			die ("Field Name Branch must be filled in!! ");
		}
		elseif(empty($_POST["address"])){
			die ("Field Address must be filled in!! ");
		}
		elseif(empty($_POST["city"])){
			die ("Field City must be filled in!! ");
		}
		elseif(empty($phone) || !preg_match('/^[+#*\(\)\[\]]*([0-9][ ext+-pw#*\(\)\[\]]*){6,45}$/', $phone)){
			die ("Field Phone must be filled in and numeric!! ");
		}
		elseif(empty($_POST["fax"])){
			die ("Field Fax must be filled in!! ");
		}
		elseif(empty($_POST["email"])){
			die ("Field Email must be filled in!! ");
		}
		elseif(!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $email)) {
			die ("Field Email isn't valid! ");
		}
		elseif(empty($_POST["area_id"])){
			die ("Field ID Area must be filled in!! ");
		}
		elseif(empty($_POST["branch_short"])){
			die ("Field Branch Short must be filled in!! ");
		}
		elseif(empty($_POST["place"])){
			die ("Field Place must be filled in!! ");
		}
		else{
			$data = array(
					'branch_id' => $this->input->post('branch_id'),
					'id_company' => $this->input->post('id_company'),
					'name_branch' => $this->input->post('name_branch'),
					'branch_short' => $this->input->post('branch_short'),
					'address' => $this->input->post('address'),
					'city' => $this->input->post('city'),
					'phone' => $this->input->post('phone'),
					'fax' => $this->input->post('fax'),
					'email' => $this->input->post('email'),
					'status' => '1',
					'area_id' => $this->input->post('area_id'),
					'place' => $this->input->post('place'),		
					'date_create' => $date_add,
					'remarks' => $remarks_add		
				);
			$insert = $this->M_branch->add_branch($data);   
		  }
		  echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_branch->get_by_id($id);
			echo json_encode($data);
		}

	public function update_branch()
		{
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_update = date('Y-m-d H:i:s');
		
		if(empty($_POST["id_company"])){
			die ("Field ID Company must be filled in!! ");
		}
		elseif(empty($_POST["name_branch"])){
			die ("Field Name Branch must be filled in!! ");
		}
		elseif(empty($_POST["address"])){
			die ("Field Address must be filled in!! ");
		}
		elseif(empty($_POST["city"])){
			die ("Field City must be filled in!! ");
		}
		elseif(empty($phone) || !preg_match('/^[+#*\(\)\[\]]*([0-9][ ext+-pw#*\(\)\[\]]*){6,45}$/', $phone)){
			die ("Field Phone must be filled in and numeric!! ");
		}
		elseif(empty($_POST["fax"])){
			die ("Field Fax must be filled in!! ");
		}
		elseif(empty($_POST["email"])){
			die ("Field Email must be filled in!! ");
		}
		elseif(!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $email)) {
			die ("Field Email isn't valid! ");
		}
		elseif(empty($_POST["area_id"])){
			die ("Field ID Area must be filled in!! ");
		}
		elseif(empty($_POST["branch_short"])){
			die ("Field Branch Short must be filled in!! ");
		}
		elseif(empty($_POST["place"])){
			die ("Field Place must be filled in!! ");
		}
		else{
			$data = array(
					'branch_id' => $this->input->post('branch_id'),
					'id_company' => $this->input->post('id_company'),
					'name_branch' => $this->input->post('name_branch'),
					'branch_short' => $this->input->post('branch_short'),
					'address' => $this->input->post('address'),
					'city' => $this->input->post('city'),
					'phone' => $this->input->post('phone'),
					'fax' => $this->input->post('fax'),
					'email' => $this->input->post('email'),
					'status' => '1',
					'area_id' => $this->input->post('area_id'),
					'place' => $this->input->post('place'),	
					'date_update' => $date_update,
					'remarks' => $remarks_update			
				);
		$this->M_branch->update_branch(array('branch_id' => $this->input->post('branch_id')), $data);
		     }
			 echo 'Insert';
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_delete = 'Action : Delete | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_delete = date('Y-m-d H:i:s');

		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
						'date_update' => $date_delete,
						'remarks' => $remarks_delete
				);
				$this->db->where('branch_id', $id);
				$this->db->update('tbl_branches', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}



}
