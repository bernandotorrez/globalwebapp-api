<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class M_change_pass extends CI_Model 
{
 	
   public $db_table = 'tbl_user';   
   
    function get_pass()
   {     
   		$strid_user =$this->session->userdata('id');	
	    $password = md5($this->input->post('password'));  		
		$data=array('password'=>$password );	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update password user			
		return true;
   }
   
   public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}
  

	
}