<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_user_group extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_user_group');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_user_group->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}	
	 	}
		
	public function index()
		{

		$data['user_group']=$this->M_user_group->get_all_user_group();
		$data['show_view'] = 'user_group_form/V_user_group';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		}
										
	public function ajax_list()
	{
		$list = $this->M_user_group->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_user_groups) {
			$chk_idmaster ='<div align="center"><input id="checkuser_group" name="checkuser_group" type="checkbox" value='.$tbl_user_groups->id_group.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_user_groups->id_group;
			$row[] = $tbl_user_groups->name_grp;
			$row[] = $tbl_user_groups->apps_access;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_user_group->count_all(),
						"recordsFiltered" => $this->M_user_group->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_group');
		$this->db->from('tbl_user_groups');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_group",$search);
		$this->db->or_like("name_grp",$search);
		$this->db->or_like("apps_access",$search);
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_group','ASC');
		$query=$this->db->get('tbl_user_groups');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("id_group",$search);
		$jum=$this->db->get('tbl_user_groups');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $tbl_user_groups) {
		$chk_idmaster ='<div align="center"><input id="checkgroup" name="checkgroup" type="checkbox" value='.$tbl_user_groups["id_group"].' class="editRow ace" req_id_del='.$tbl_user_groups["id_group"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($tbl_user_groups['id_group'],$tbl_user_groups['name_grp'],
									$tbl_user_groups['apps_access'],
									$chk_idmaster
									);
		}
		echo json_encode($output);
	}

	public function crudusergroup(){
		$this->load->view('formcrudusergroup');
	}
											
	public function add_user_group(){
		if(empty($_POST["id_group"])){
			die ("Field Id Group must be filled in!! ");
		}
		elseif(empty($_POST["name_grp"])){
			die ("Field Name Group must be filled in!! ");
		}
		else{
			$data = array(
					'id_group' => $this->input->post('id_group'),
					'name_grp' => $this->input->post('name_grp'),
					'apps_access' => $this->input->post('apps_access'),
					'status' => '1',
				);
			$insert = $this->M_user_group->add_user_group($data);
		  }
		  echo 'Insert';
	}
		
	public function update_user_group(){
		if(empty($_POST["name_grp"])){
			die ("Field Name Group must be filled in!! ");
		}
		else{
			$data = array(
					'id_group' => $this->input->post('id_group'),
					'name_grp' => $this->input->post('name_grp'),
					'apps_access' => $this->input->post('apps_access'),
					'status' => '1',
				);
		$this->M_user_group->update_user_group(array('id_group' => $this->input->post('id_group')), $data);
		  }
		  echo 'Insert';
	}
		
	public function ajax_edit($id)
		{
			$data = $this->M_user_group->get_by_id($id);
			echo json_encode($data);
		}

	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$this->db->where('id_group', $id);
				$this->db->delete('tbl_user_groups');
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}



}
