<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>

<script>	
$(function() {
  $("#txthead").focus();
});		  
</script>     
<style type="text/css">   			
	label
	{
		width:20%;
		display: block;
		margin-right:2%;
		text-align:left;
		line-height:22px;
	}	
    span.error
	{
		font: 11px Verdana, Geneva, sans-serif;
		color:red;
		margin-left:8px;
		line-height:22px;
	}
	
	/*------------------------- */
</style>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>

<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">    
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel">Change Your Signature BPK Here</div>
   <div class="panel-body">               
    <div id="tampilanform"></div>                   
      
     <form name="form" id="form"   action="<?php echo base_url('signaturebpk_form/c_signature_bpk/do_change_sign');?>" method="post">
     	 
     <div id="rounded_main_menu" class="col-lg-7 col-lg-offset-0" > 
        <h3 style="color:#900">Please logout and login back  for the result of changes</h3>                             
        <table class="table">                                                       
         <tr> 
            <td>  
                 <div class="form-group">
                   <label for="Head"  class="col-xs-4">Penerima</label>
                    <div class="col-xs-7">
                 <input type="text" id="txtpenerima" name="txtpenerima" class="form-control" value="<?php echo $this->session->userdata("sign_penerima"); ?>"  />
                    </div>
                </div>
               </td>
            <td> 
            		 <div class="form-group">
                   <label for="Head"  class="col-xs-4">Finance</label>
                    <div class="col-xs-7">
                 <input type="text" id="txtfinance" name="txtfinance" class="form-control" value="<?php echo $this->session->userdata("sign_finance"); ?>"  />
                    </div>
                </div>
            </td>
            </tr>           
              <tr> 
            <td>  
                 <div class="form-group">
                   <label for="Head"  class="col-xs-4">Accounting</label>
                    <div class="col-xs-7">
                 <input type="text" id="txtacc" name="txtacc" class="form-control" value="<?php echo $this->session->userdata("sign_accounting"); ?>"  />
                    </div>
                </div>                
               </td>               
               <td> 
               		    <div class="form-group">
                   <label for="Head"  class="col-xs-4">Pemeriksa I</label>
                    <div class="col-xs-7">
                 <input type="text" id="txtpem1" name="txtpem1" class="form-control" value="<?php echo $this->session->userdata("sign_pemeriksa1"); ?>"  />
                    </div>
                </div>                     
               </td>
            </tr>           
              <tr> 
            <td>  
           	     <div class="form-group">
                   <label for="Head"  class="col-xs-4">Pemeriksa II</label>
                   <div class="col-xs-7">
                 <input type="text" id="txtpem2" name="txtpem2" class="form-control" value="<?php echo $this->session->userdata("sign_pemeriksa2"); ?>"  />
                   </div>
                </div>                
               </td>
           	   <td> 
               		 <div class="form-group">
                   <label for="Head"  class="col-xs-4">Pimpinan</label>
                    <div class="col-xs-7">
                 <input type="text" id="txtpimpinan" name="txtpimpinan" class="form-control" value="<?php echo $this->session->userdata("sign_pimpinan"); ?>"  />
                    </div>
                </div>                   
               </td>
            </tr>          
            <tr>
              <tr> 
            <td>  
           	     <div class="form-group">
                   <label for="Head"  class="col-xs-4">Giro I</label>
                   <div class="col-xs-7">
                 <input type="text" id="txtgiro1" name="txtgiro1" class="form-control" value="<?php echo $this->session->userdata("sign_giro1"); ?>"  />
                   </div>
                </div>                
               </td>
           	   <td> 
               		 <div class="form-group"> <label for="Head"  class="col-xs-4">Giro II</label>
               		   <div class="col-xs-7">
                 <input type="text" id="txtgiro2" name="txtgiro2" class="form-control" value="<?php echo $this->session->userdata("sign_giro2"); ?>"  />
                    </div>
                </div>                   
               </td>
            </tr>          
            <tr>
            	<td>
                <div><input type="submit" name="btnchange" value="change" class="btn btn-danger" ></div>     
                </td>
            </tr>
            
         </table>      
    </div>
    
    </form>   
    
   </div>
 </div>
</div>  
    



