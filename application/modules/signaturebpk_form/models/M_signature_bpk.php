<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class M_signature_bpk extends CI_Model 
{
 	
   public $db_table = 'tbl_user';   
   
    function get_pass()
   {     
   		$strid_user =$this->session->userdata('id');	
	    $penerima = $this->input->post('txtpenerima');  		
		$finance = $this->input->post('txtfinance');  		
		$accounting = $this->input->post('txtacc');  	
		$pem1 = $this->input->post('txtpem1');  		
		$pem2 = $this->input->post('txtpem2');  	
		$pimpinan = $this->input->post('txtpimpinan');  		
		$giro1 = $this->input->post('txtgiro1');
		$giro2 = $this->input->post('txtgiro2');  		
		
		$data=array('sign_penerima'=>$penerima,
					'sign_finance'=>$finance,
					'sign_accounting'=>$accounting,
					'sign_pemeriksa1'=>$pem1,
					'sign_pemeriksa2'=>$pem2,
					'sign_pimpinan'=>$pimpinan,
					'sign_giro1'=>$giro1,
					'sign_giro2'=>$giro2
					 );	
					 
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update password user
		
		 $this->session->unset_userdata('sign_penerima');
		 $this->session->unset_userdata('sign_finance')	;				 
		 $this->session->unset_userdata('sign_accounting');
		 $this->session->unset_userdata('sign_pemeriksa1');
		 $this->session->unset_userdata('sign_pemeriksa2')	;				 
		 $this->session->unset_userdata('sign_pimpinan');
		 $this->session->unset_userdata('sign_giro1')	;				 
		 $this->session->unset_userdata('sign_giro2');
		
		$this->session->set_userdata($data);		
					
		return true;
   }
   
   public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}
  

	
}