<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_goods_category extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_goods_category');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_goods_category->get_value('id',$this->session->userdata('id'),'tbl_user');
				$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}		
	 	}
		
	public function index()
		{
		
		$data['category']=$this->M_goods_category->get_all_category();
		$data['total']=$this->M_goods_category->get_count_id();
		$data['show_view'] = 'goods_category/V_goods_category';
		$this->load->view('dashboard/Template',$data);		
		
		}
		
	public function ajax_list()
	{
		$list = $this->M_goods_category->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_goods_category) {
			$chk_idmaster ='<div align="center"><input id="checkcategory" name="checkcategory" type="checkbox" value='.$tbl_goods_category->id_category.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_goods_category->id_category;
			$row[] = $tbl_goods_category->name_category;	
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_goods_category->count_all(),
						"recordsFiltered" => $this->M_goods_category->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	//function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		//$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		//$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		//$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		//$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		/*$this->db->select('id_category');
		$this->db->from('tbl_goods_category');
		$this->db->where('status','1');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		//$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		//$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		//$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		//$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		/*if($search!=""){
		$this->db->like("id_category",$search);
		$this->db->where('status','1');
		$this->db->or_like("name_category",$search);
		$this->db->where('status','1');
		}


		/*Lanjutkan pencarian ke database*/
		/*$this->db->limit($length,$start);
		$this->db->where('status','1');
		/*Urutkan dari alphabet paling terkahir
		$this->db->order_by('id_category','ASC');
		$query=$this->db->get('tbl_goods_category');*/


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		/*if($search!=""){
		$this->db->like("id_category",$search);
		$jum=$this->db->get('tbl_goods_category');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $tbl_goods_category) {
		$chk_idmaster ='<div align="center"><input id="checkbank" name="checkbank" type="checkbox" value='.$tbl_goods_category["id_category"].' class="editRow ace" req_id_del='.$tbl_goods_category["id_category"].' />
        <span class="lbl"></span> ';

			$output['data'][]=array($tbl_goods_category['id_category'],$tbl_goods_category['name_category'],
									$chk_idmaster
									);

		}

		echo json_encode($output);


	}*/

	public function add_category()
		{
		if(empty($_POST["name_category"])){
			die ("Field Nama Category must be filled in!! ");
		}
		else{
			$data = array(
					'id_category' => $this->input->post('id_category'),
					'name_category' => $this->input->post('name_category'),
				);
			$insert = $this->M_goods_category->add_category($data);
			//return $this->db->insert_id();
		  }
		  echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_goods_category->get_by_id($id);
			echo json_encode($data);
		}

	public function update_category()
		{
		if(empty($_POST["name_category"])){
			die ("Field Nama Category must be filled in!! ");
		}
		else{
		$data = array(
				'name_category' => $this->input->post('name_category'),
			);
		$this->M_goods_category->update_category(array('id_category' => $this->input->post('id_category')), $data);
		  }
		  echo 'Insert';
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array (
						'status' => '0',
				);
				$this->db->where('id_category', $id);
				$this->db->update('tbl_goods_category', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}



}
