<!DOCTYPE html>
<?php
date_default_timezone_set("Asia/Bangkok");
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<?php
	$id = $_GET['id'];
	
	// Perintah untuk menampilkan data
$queri="select * from qv_wrs_request where id_wrs_request='".$id."'" ;  //menampikan SEMUA data dari tabel siswa

$hasil=$this->db->query($queri);    //fungsi untuk SQL
$ada = $hasil->result();
// perintah untuk membaca dan mengambil data dalam bentuk array




?>
<Style type='text/css' media='print'>
label { 
  text-align: right; 
  float: left; 
} 
a:link:after, a:visited:after {  
      display: none;
      content: "";    
    }
#printSize {width : 720px}
#checkboxlist { 
  text-align: left;
	float : left;
} 
#printLink {display : none}

</Style>
<style>
 .outer {
    width: 210px;
    color: black;
    border: 2px solid Gainsboro;
    padding: 5px;
 }
  hr{
	width: 100%;
	border-top: 2px solid lightgray;;
  }
	table { margin:10px auto; border-collapse:collapse; border:1px solid gray; }
	td,th { border:1px solid gray; text-align:left; padding:7px;}
	td.opt1 { text-align:center; vertical-align:middle; }
	td.opt2 { text-align:right; }
	 
</style> 
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mazda | WRS Request Form</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker3.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-timepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/daterangepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-colorpicker.min.css') ?>" />
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">


        <!-- Main content -->
        <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <img src="../../assets/images/logo/mazda.png" alt="Mazda"> PT. Eurokars Motor Indonesia
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Address
              <address>
                <strong>Porsche Centre Jakarta</strong><br>
                Jl. Sultan Iskandar Muda, No. 15<br>
                Arteri Pondok Indah, Jakarta Selatan<br>
                Phone	: (021) 2793 2838<br>
                Fax		: (021) 2793 2838
              </address>
            </div><!-- /.col -->
          </div><!-- /.row -->
		<center><div class="row" id="printSize">
		<h3 class="box-title" style="font-weight:bold;"><center>WRS REQUEST FORM</center></h3></br>
		 <form class="form-horizontal" id="form">
		<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="name" class="col-sm-3 control-label" style="padding-right:10px">Request Date</label>

                  <div class="col-sm-8">
                    <input name="request_date" id="request_date" class="form-control" type="text" readonly value="<?php echo $ada[0]->request_date ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="type" class="col-sm-3 control-label" style="padding-right:10px">Request Type</label>
				
			
					
                  <div class="col-sm-3 outer" id="checkboxlist" style="text-align:left">
                    <?php
						$dataarraycekbox = array(1=>'Data Deletion','Data Change/Modification','Deactivated WRS Sales','Other');
						$cek = "";
						$nb = 1;
						$value = explode(",",$ada[0]->request_type);
						error_reporting(0);
						foreach($value as $kalue){
							$cekcekcek[$kalue] = "checked='true'";
						}
						foreach($dataarraycekbox as $keyarray => $valarray){
							$no = $nb++;
							echo '<input '.$cekcekcek[$keyarray].' type="checkbox" name="type[]" id="type" value="'.$no.'">'.$valarray.'<br>';
						}
					?>
					</div>
				</div>
				 <div class="form-group" id="type_other">
                  <label for="type_other" class="col-sm-3 control-label" style="padding-right:10px">Type Other</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="type_other" id="type_other" readonly value="<?php echo $ada[0]->type_other ?>">
                  </div>
                </div>
				 <div class="box-body">
                <div class="form-group">
                  <label for="reason" class="col-sm-3 control-label" style="padding-right:10px">Reason</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="reason" id="reason" readonly value="<?php echo $ada[0]->reason ?>">
                  </div>
                </div>
							
              </div>
              </div>
		</div>
		</div>
		<div class="col-xs-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
              <div class="box-body">
                     <div class="form-group">
                  <label for="target" class="col-sm-3 control-label" style="padding-right:10px">Dealer Target</label>

                  <div class="col-sm-3 outer" id="checkboxlist" style="text-align:left">
					 <?php
						$dataarraycekbox1 = array(1=>'All Dealer','Specific Dealer');
						$cek1 = "";
						$nb1 = 1;
						$value1 = explode(",",$ada[0]->dealer_target);
						error_reporting(0);
						foreach($value1 as $kalue1){
							$cekcekcek1[$kalue1] = "checked='true'";
						}
						foreach($dataarraycekbox1 as $keyarray1 => $valarray1){
							$no1 = $nb1++;
							echo '<input '.$cekcekcek1[$keyarray1].' type="checkbox" name="type[]" id="type" value="'.$no1.'">'.$valarray1.'<br>';
						}
					?>
                  </div>
                </div>
				 <div class="form-group" id="dealer_name" style="display: none">
                  <label for="dealer_name" class="col-sm-3 control-label" style="padding-right:10px">Dealer Name</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="dealer_name" id="dealer_name" readonly value="<?php echo $ada[0]->dealer_name ?>">
                  </div>
                </div>
				<div class="form-group" id="dealer_code" style="display: none">
                  <label for="dealer_code" class="col-sm-3 control-label" style="padding-right:10px">Dealer Code</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="dealer_code" id="dealer_code" readonly value="<?php echo $ada[0]->dealer_code ?>">
                  </div>
                </div>
				<div class="form-group">
                  <label for="remarks" class="col-sm-3 control-label" style="padding-right:10px">Remarks</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="remarks" id="remarks" readonly value="<?php echo $ada[0]->remarks ?>">
                  </div>
                </div>
              </div>
          </div>
		</div>
		<hr>
		<div class="col-xs-9">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="requestor_name" class="col-sm-2 control-label" style="padding-right:10px">Requestor Name</label>

                  <div class="col-sm-8">
                    <input class="form-control" rows="3" name="requestor_name" id="requestor_name" readonly value="<?php echo $ada[0]->requestor_name ?>"></textarea>
                  </div>
                </div>
				<div class="form-group">
                  <label for="dept" class="col-sm-2 control-label" style="padding-right:10px">Department</label>

					<div class="col-sm-8">
					   <input class="form-control" name="remarks" id="remarks" readonly value="<?php echo $ada[0]->dept ?>">    
					</div>
                </div>
				<div class="form-group">
                  <label for="position" class="col-sm-2 control-label" style="padding-right:10px">Position</label>

                  <div class="col-sm-8">
                    <input class="form-control" rows="3" name="position" id="position" readonly value="<?php echo $ada[0]->position ?>"></textarea>
                  </div>
                </div>
				<table>
				  <tr>
					<th width="25px"><center>No</center></th>
					<th width="500px"><center>Module on WRS Sales</center></th>
					<th width="110px"><center>Please Thick</center></th>
				  </tr>
				  <tr>
					<td class="opt1">1</td>
					<td>
					  MMI Stock [Please attach the details]
					</td>
					<td class="opt1">
						<?php
						$value2 = $ada[0]->mmi_stock;
						error_reporting(0);
						if($value2 == 1){
							echo '<input type="checkbox" id="mmi_stock" name="mmi_stock" checked>';
						}else{
							echo '<input type="checkbox" id="mmi_stock" name="mmi_stock">';
						}
					?>
					</td>
				  </tr>
				  <tr>
					<td class="opt1">2</td>
					<td>
					  Dealer Stock  [Please attach the details]
					</td>
					<td class="opt1">
					  <?php
						$value3 = $ada[0]->dealer_stock;
						error_reporting(0);
						if($value3 == 1){
							echo '<input type="checkbox" id="dealer_stock" name="dealer_stock" checked>';
						}else{
							echo '<input type="checkbox" id="dealer_stock" name="dealer_stock">';
						}
					?>
					</td>
				  </tr>
				  <tr>
					<td class="opt1">3</td>
					<td>
					  Faktur Retail  [Please attach the details]
					</td>
					<td class="opt1">
					  <?php
						$value4 = $ada[0]->faktur_retail;
						error_reporting(0);
						if($value4 == 1){
							echo '<input type="checkbox" id="faktur_retail" name="faktur_retail" checked>';
						}else{
							echo '<input type="checkbox" id="faktur_retail" name="faktur_retail">';
						}
					?>
					</td>
				  </tr>
				  <tr>
					<td class="opt1">4</td>
					<td>
					  SPK Retail  [Please attach the details]
					</td>
					<td class="opt1">
					  <?php
						$value5 = $ada[0]->spk_retail;
						error_reporting(0);
						if($value5 == 1){
							echo '<input type="checkbox" id="spk_retail" name="spk_retail" checked>';
						}else{
							echo '<input type="checkbox" id="spk_retail" name="spk_retail">';
						}
					?>
					</td>
				  </tr>
				  <tr>
					<td class="opt1">5</td>
					<td>
					  SPK Fleet  [Please attach the details]
					</td>
					<td class="opt1">
					   <?php
						$value5 = $ada[0]->spk_fleet;
						error_reporting(0);
						if($value5 == 1){
							echo '<input type="checkbox" id="spk_fleet" name="spk_fleet" checked>';
						}else{
							echo '<input type="checkbox" id="spk_fleet" name="spk_fleet">';
						}
					?>
					</td>
				  </tr>
				  <tr>
					<td class="opt1">6</td>
					<td>
					  Other
					</td>
					<td class="opt1">
					   <?php
						$value5 = $ada[0]->other;
						error_reporting(0);
						if($value5 == 1){
							echo '<input type="checkbox" id="other" name="other" checked>';
						}else{
							echo '<input type="checkbox" id="other" name="other">';
						}
					?>
					</td>
				  </tr>
				</table>
				 <div class="form-group" id="specify" style="display: none">
                  <label for="specify" class="col-sm-3 control-label" style="padding-right:10px">Spec. Other</label>

                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="specify" id="specify" placeholder="Mazda">
                  </div>
                </div>
              </div>
			</div>
		</form>
		</div>
		</center>
          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
              <button id="printLink" onclick="window.print()">Cetak Halaman Web</button>
            </div>
          </div>
        </section><!-- /.content -->
        <div class="clearfix"></div>
      </div><!-- /.content-wrapper -->
      
    <!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
  </body>
  
</html>
