<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<style>
 .outer {
    width: 210px;
    color: black;
    border: 2px solid;
    padding: 5px;
 }
</style> 


									<div>
									<a href="<?php echo site_url('wrs_request_form/c_wrs/view_add_wrs'); ?>"><button class="btn bg-purple btn-flat"><i class="fa fa-plus-circle"></i> Add Data</button></a>
									<button onclick="delete_wrs()" id="deleteTriger" name="deleteTriger" class="btn bg-maroon btn-flat" disabled><i class="fa fa-trash"></i> Delete Data</button>
									<button onclick="edit_wrs()" id="editTriger"  name="editTriger"class="btn bg-maroon btn-flat" disabled><i class="fa fa-edit"></i> Edit Data</button>
									</div>
                                    <br/>
									<div class="row">
									<div class="col-xs-12" style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">                               
	                                <table  class="table table-striped table-bordered table-hover" id="myTable" >
	                                    <thead class="text-warning">
											<th width="5%" style="text-align:center">
												<label class="pos-rel">
                                                    <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
                                                    <span class="lbl"></span>
                                                </label>
                                            </th>
											<th>No</th>
	                                        <th>Id</th>
											<th>Name</th>
											<th>Department</th>
											<th>Position</th>
											<th>Date</th>
											<th>Req. Type</th>
											<th>Dealer Target</th>
											<th>Dealer Name</th>
											<th>Reason</th>
											<th>Remarks</th>
											<th>Submitted By</th>
											<th>Detail</th>
	                                    </thead>
                                     </table> 
      </div>                               
</div>      
  
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-80p">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">×</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    WRS Request Form
                </h4>
            </div>
	  <div class="modal-body form" style="padding-top:10px">       
	<form class="form-horizontal" id="form" >
		<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
			  <div class="form-group">
                  <label for="id" class="col-sm-3 control-label" style="padding-right:10px">Id WRS</label>

                  <div class="col-sm-8">
                    <input type="text" readonly class="form-control" name="id" id="id" placeholder="1">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-3 control-label" style="padding-right:10px">Request Date</label>

                  <div class="col-sm-8">
                    <input name="request_date" id="request_date" placeholder="1" class="form-control" type="text" readonly value="<?php echo date('l, d-m-Y')?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="type" class="col-sm-3 control-label" style="padding-right:10px">Request Type</label>

                  <div class="col-sm-3 outer" id="type">
                    <input type="checkbox" name="type[]" id="type" value="1">Data Deletion<br>
					<input type="checkbox" name="type[]" id="type" value="2">Data Change/Modification<br>
					<input type="checkbox" name="type[]" id="type" value="3">Deactivated WRS Sales<br>
					<input type="checkbox" name="type[]" id="type" onclick="ShowHideDiv(this)" value="4">Other<br>
                  </div>
                </div>
				 <div class="form-group" id="type_other">
                  <label for="type_other" class="col-sm-3 control-label" style="padding-right:10px">Type Other</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="type_other" id="type_other" placeholder="Data Deletion">
                  </div>
                </div>
				 <div class="box-body">
                <div class="form-group">
                  <label for="reason" class="col-sm-3 control-label" style="padding-right:10px">Reason</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" name="reason" id="reason" placeholder=""></textarea>
                  </div>
                </div>
							
              </div>
              </div>
		</div>
		</div>
		<div class="col-xs-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
              <div class="box-body">
                     <div class="form-group">
                  <label for="target" class="col-sm-3 control-label" style="padding-right:10px">Dealer Target</label>

                  <div class="col-sm-3 outer" id="checkboxlist">
                    <input type="checkbox" name="target[]" id="target" value="1">All Dealer<br>
					<input type="checkbox" name="target[]" id="target" onclick="ShowHideTarget(this)" value="2">Specific Dealer<br>
                  </div>
                </div>
				 <div class="form-group" id="dealer_name">
                  <label for="dealer_name" class="col-sm-3 control-label" style="padding-right:10px">Dealer Name</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="dealer_name" id="dealer_name" placeholder="Mazda">
                  </div>
                </div>
				<div class="form-group" id="dealer_code" >
                  <label for="dealer_code" class="col-sm-3 control-label" style="padding-right:10px">Dealer Code</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="dealer_code" id="dealer_code" placeholder="01">
                  </div>
                </div>
				<div class="form-group">
                  <label for="remarks" class="col-sm-3 control-label" style="padding-right:10px">Remarks</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" name="remarks" id="remarks" placeholder=""></textarea>
                  </div>
                </div>
              </div>
          </div>
		</div>
		<hr>
		<div class="col-xs-9">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="requestor_name" class="col-sm-2 control-label" style="padding-right:10px">Requestor Name</label>

                  <div class="col-sm-8">
                    <input class="form-control" rows="3" name="requestor_name" id="requestor_name" placeholder="Brian"></textarea>
                  </div>
                </div>
				<div class="form-group">
                  <label for="dept" class="col-sm-2 control-label" style="padding-right:10px">Department</label>

					<div class="col-sm-8">
					   <select class="form-control" name="dept" id="dept">
					   <option  value="">Choose Department</option>
					   <?php
						$sqlpo	  = "select * from tbl_dept where status=1";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo 	  = $qrypo->result();					
						foreach($adapo as $aaa) { ?>
						<option value="<?php echo $aaa->id_dept;?>"><?php echo $aaa->dept;?>
						</option>
						<?php } ?>			
						</select>    
					</div>
                </div>
				<div class="form-group">
                  <label for="position" class="col-sm-2 control-label" style="padding-right:10px">Position</label>

                  <div class="col-sm-8">
                    <input class="form-control" rows="3" name="position" id="position" placeholder="Assistant"></textarea>
                  </div>
                </div>
				<table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
				  <tr>
					<th><center>No</center></th>
					<th><center>Module on WRS Sales</center></th>
					<th><center>Please Thick</center></th>
				  </tr>
				  <tr>
					<td class="col-sm-2 opt1"><center>1</center></td>
					<td class="col-sm-4 opt1">
					  MMI Stock [Please attach the details]
					</td>
					<td class="col-sm-2 opt1"><center>
					  <input type="checkbox" id="mmi_stock" name="mmi_stock" value="TRUE"></center>
					</td>
				  </tr>
				  <tr>
					<td class="opt1"><center>2</center></td>
					<td class="col-sm-4 opt1">
					  Dealer Stock  [Please attach the details]
					</td>
					<td class="col-sm-2 opt1"><center>
					  <input type="checkbox" id="dealer_stock" name="dealer_stock" value="TRUE"></center>
					</td>
				  </tr>
				  <tr>
					<td class="opt1"><center>3</center></td>
					<td class="col-sm-4 opt1">
					  Faktur Retail  [Please attach the details]
					</td>
					<td class="col-sm-2 opt1"><center>
					  <input type="checkbox" id="faktur_retail" name="faktur_retail" value="TRUE"></center>
					</td>
				  </tr>
				  <tr>
					<td class="opt1"><center>4</center></td>
					<td class="col-sm-4 opt1">
					  SPK Retail  [Please attach the details]
					</td>
					<td class="col-sm-2 opt1"><center>
					  <input type="checkbox" id="spk_retail" name="spk_retail" value="TRUE"></center>
					</td>
				  </tr>
				  <tr>
					<td class="opt1"><center>5</center></td>
					<td class="col-sm-4 opt1">
					  SPK Fleet  [Please attach the details]
					</td>
					<td class="col-sm-2 opt1"><center>
					  <input type="checkbox" id="spk_fleet" name="spk_fleet" value="TRUE"></center>
					</td>
				  </tr>
				  <tr>
					<td class="opt1"><center>6</center></td>
					<td class="col-sm-4 opt1">
					  Other
					</td>
					<td class="col-sm-2 opt1"><center>
					  <input type="checkbox" id="other" name="other" onclick="ShowHideOther(this)" value="TRUE"></center>
					</td>
				  </tr>
				</table> 
				<br>
				 <div class="form-group" id="specify">
                  <label for="specify" class="col-sm-3 control-label" style="padding-right:10px">Spec. Other</label>

                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="specify" id="specify" placeholder="Mazda">
                  </div>
                </div>
              </div>
			</div>
		</form>
		</div>
			<div class="modal-header">
            <button type="button" id="btnSave" name="btnSave" onclick="save()" class="btn btn-primary col-sm-3">Save</button>
            <button type="button" class="btn btn-danger col-sm-3" data-dismiss="modal">Cancel</button>
			</div>
		</div> <!-- End modal body div -->
      </div> <!-- End modal content div -->
</div>
<script>
$('.selectall').click(function() {
    if ($(this).is(':checked')) {
        $('div input').attr('checked', true);
    } else {
        $('div input').attr('checked', false);
    }
});
</script>
<script>


//$('#myTable').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
var tableUsers = $('#myTable').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		"dom": 'lfBrtip',
		"buttons": [
            {
		   "extend":    'copy',
		   "text":      '<i class="fa fa-files-o"></i> Copy',
		   "titleAttr": 'Copy'
		   },
		   {
		   "extend":    'print',
		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
		   "titleAttr": 'Print',
		   "orientation": 'landscape',
           "pageSize": 'LEGAL'
		   },
		   {
		   "extend":    'excel',
		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
		   "titleAttr": 'Excel'
		   },
		   {
		   "extend":    'pdf',
		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
		   "titleAttr": 'PDF',
		   "orientation": 'landscape',
           "pageSize": 'LEGAL'
		   }
        ],
		"autoWidth" : false,
		"scrollY" : '250',
		"scrollX" : true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('wrs_request_form/C_wrs/ajax_list') ?>",
            "type": "POST"
        },
		'columnDefs': [
		{
        'targets': [0],
		'orderable': false,
		}
   ],

      'order': [[2, 'desc']],
        });
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------

//aktif print--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#printTriger').prop("disabled", false): $('#printTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>


<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>

<script>
        	$(document).ready(function(){
	            $("#id_company").change(function (){
	                var url = "<?php echo site_url('hrd_loan_form/C_loan/pos_branch');?>/"+$(this).val();
	                $('#branch_id').load(url);
	                return false;
	            })
	        });
    	</script>

		<script type="text/javascript">
    function ShowHideDiv(type4) {
        var type_other = document.getElementById("type_other");
        type_other.style.display = type4.checked ? "block" : "none";
    }
	
	function ShowHideTarget(target2) {
        var dealer_name = document.getElementById("dealer_name");
		var dealer_code = document.getElementById("dealer_code");
        dealer_name.style.display = target2.checked ? "block" : "none";
		dealer_code.style.display = target2.checked ? "block" : "none";
    }
	
	function ShowHideOther(other) {
        var specify = document.getElementById("specify");
        specify.style.display = other.checked ? "block" : "none";
    }
	</script>
	
	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
	
	function edit_wrs(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('wrs_request_form/c_wrs/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {
			var aaa = data.request_type.split(",");
			$.each(aaa, function(i, item) {
			if (item == "1") {
			$("#type[value='1']" ).prop("checked", true);
			} 
			if (item == "2") {
			$("#type[value='2']").prop("checked", true);
			} 
			if (item == "3") {
			$("#type[value='3']").prop("checked", true);
			} 
			if (item == "4") {
			$("#type[value='4']").prop("checked", true);
			} 
			
			});
			var bbb = data.dealer_target;
			if (bbb == "1") {
			$("#target[value='1']" ).prop("checked", true);
			} 
			if (bbb == "2") {
			$("#target[value='2']").prop("checked", true);
			} 
			
			var ccc = data.mmi_stock; 
			if (ccc == "1") {
			$("#mmi_stock[value='TRUE']" ).prop("checked", true);
			} 
			
			var ddd = data.dealer_stock;
			if (ddd == "1") {
			$("#dealer_stock[value='TRUE']" ).prop("checked", true);
			} 
			
			var iii = data.faktur_retail;
			if (iii == "1") {
			$("#faktur_retail[value='TRUE']" ).prop("checked", true);
			} 
			
			var fff = data.spk_retail;
			if (fff == "1") {
			$("#spk_retail[value='TRUE']" ).prop("checked", true);
			} 
			
			var ggg = data.spk_fleet;
			if (ggg == "1") {
			$("#spk_fleet[value='TRUE']" ).prop("checked", true);
			} 
			
			$('[name="id"]').val(data.id_wrs_request);
			$('[name="request_date"]').val(data.request_date);
			$('[name="type_other"]').val(data.type_other);
            $('[name="reason"]').val(data.reason);
			$('[name="dealer_name"]').val(data.dealer_name);
			$('[name="dealer_code"]').val(data.dealer_code);
			$('[name="remarks"]').val(data.remarks);
			$('[name="requestor_name"]').val(data.requestor_name);
			$('[name="dept"]').val(data.dept);
			$('[name="position"]').val(data.position);
			$('[name="specify"]').val(data.specify);
			$('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit WRS Sales'); // Set title to Bootstrap modal title
			
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

     function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('wrs_request_form/C_wrs/add_wrs')?>";
      }
      else
      {
		   url = "<?php echo site_url('wrs_request_form/C_wrs/update_wrs')?>";
      }
		  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }

     function delete_wrs()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('wrs_request_form/C_wrs/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
			
		

      
    }
	
  </script>

  

</html>
