<!DOCTYPE html>
<?php
date_default_timezone_set("Asia/Bangkok");
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<style>
 .outer {
    width: 210px;
    color: black;
    border: 2px solid Gainsboro;
    padding: 5px;
 }
  hr{
	width: 100%;
	border-top: 2px solid lightgray;;
  }
	table { margin:10px auto; border-collapse:collapse; border:1px solid gray; }
	td,th { border:1px solid gray; text-align:left; padding:7px;}
	td.opt1 { text-align:center; vertical-align:middle; }
	td.opt2 { text-align:right; }
	 
</style> 
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mazda | WRS Request Form</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker3.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-timepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/daterangepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-colorpicker.min.css') ?>" />
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">


        <!-- Main content -->
        <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <img src="../../assets/images/logo/mazda.png" alt="Mazda"> PT. Eurokars Motor Indonesia
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Address
              <address>
                <strong>Porsche Centre Jakarta</strong><br>
                Jl. Sultan Iskandar Muda, No. 15<br>
                Arteri Pondok Indah, Jakarta Selatan<br>
                Phone	: (021) 2793 2838<br>
                Fax		: (021) 2793 2838
              </address>
            </div><!-- /.col -->
          </div><!-- /.row -->
		<div class="row" id="modal_form">
		<h3 class="box-title" style="font-weight:bold;"><center>WRS REQUEST FORM</center></h3></br>
		 <form class="form-horizontal" id="form">
		<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="name" class="col-sm-3 control-label" style="padding-right:10px">Request Date</label>

                  <div class="col-sm-8">
                    <input name="request_date" id="request_date" placeholder="1" class="form-control" type="text" readonly value="<?php echo date('l, d-m-Y')?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="type" class="col-sm-3 control-label" style="padding-right:10px">Request Type</label>

                  <div class="col-sm-3 outer" id="checkboxlist">
                    <input type="checkbox" name="type[]" id="type1" value="1">Data Deletion<br>
					<input type="checkbox" name="type[]" id="type2" value="2">Data Change/Modification<br>
					<input type="checkbox" name="type[]" id="type3" value="3">Deactivated WRS Sales<br>
					<input type="checkbox" name="type[]" id="type4" onclick="ShowHideDiv(this)" value="4">Other<br>
                  </div>
                </div>
				 <div class="form-group" id="type_other" style="display: none">
                  <label for="type_other" class="col-sm-3 control-label" style="padding-right:10px">Type Other</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="type_other" id="type_other" placeholder="Data Deletion">
                  </div>
                </div>
				 <div class="box-body">
                <div class="form-group">
                  <label for="reason" class="col-sm-3 control-label" style="padding-right:10px">Reason</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" name="reason" id="reason" placeholder=""></textarea>
                  </div>
                </div>
							
              </div>
              </div>
		</div>
		</div>
		<div class="col-xs-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
              <div class="box-body">
                     <div class="form-group">
                  <label for="target" class="col-sm-3 control-label" style="padding-right:10px">Dealer Target</label>

                  <div class="col-sm-3 outer" id="checkboxlist">
                    <input type="checkbox" name="target[]" id="target1" value="1">All Dealer<br>
					<input type="checkbox" name="target[]" id="target2" onclick="ShowHideTarget(this)" value="2">Specific Dealer<br>
                  </div>
                </div>
				 <div class="form-group" id="dealer_name" style="display: none">
                  <label for="dealer_name" class="col-sm-3 control-label" style="padding-right:10px">Dealer Name</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="dealer_name" id="dealer_name" placeholder="Mazda">
                  </div>
                </div>
				<div class="form-group" id="dealer_code" style="display: none">
                  <label for="dealer_code" class="col-sm-3 control-label" style="padding-right:10px">Dealer Code</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="dealer_code" id="dealer_code" placeholder="01">
                  </div>
                </div>
				<div class="form-group">
                  <label for="remarks" class="col-sm-3 control-label" style="padding-right:10px">Remarks</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" name="remarks" id="remarks" placeholder=""></textarea>
                  </div>
                </div>
              </div>
          </div>
		</div>
		<hr>
		<div class="col-xs-9">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="requestor_name" class="col-sm-2 control-label" style="padding-right:10px">Requestor Name</label>

                  <div class="col-sm-8">
                    <input class="form-control" rows="3" name="requestor_name" id="requestor_name" placeholder="Brian"></textarea>
                  </div>
                </div>
				<div class="form-group">
                  <label for="dept" class="col-sm-2 control-label" style="padding-right:10px">Department</label>

					<div class="col-sm-8">
					   <select class="form-control" name="dept" id="dept">
					   <option  value="">Choose Department</option>
					   <?php
						$sqlpo	  = "select * from tbl_dept where status=1";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo 	  = $qrypo->result();					
						foreach($adapo as $aaa) { ?>
						<option value="<?php echo $aaa->id_dept;?>"><?php echo $aaa->dept;?>
						</option>
						<?php } ?>			
						</select>    
					</div>
                </div>
				<div class="form-group">
                  <label for="position" class="col-sm-2 control-label" style="padding-right:10px">Position</label>

                  <div class="col-sm-8">
                    <input class="form-control" rows="3" name="position" id="position" placeholder="Assistant"></textarea>
                  </div>
                </div>
				<table>
				  <tr>
					<th width="25px"><center>No</center></th>
					<th width="500px"><center>Module on WRS Sales</center></th>
					<th width="110px"><center>Please Thick</center></th>
				  </tr>
				  <tr>
					<td class="opt1">1</td>
					<td>
					  MMI Stock [Please attach the details]
					</td>
					<td class="opt1">
					  <input type="checkbox" id="mmi_stock" name="mmi_stock" value="TRUE">

					</td>
				  </tr>
				  <tr>
					<td class="opt1">2</td>
					<td>
					  Dealer Stock  [Please attach the details]
					</td>
					<td class="opt1">
					  <input type="checkbox" id="dealer_stock" name="dealer_stock" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">3</td>
					<td>
					  Faktur Retail  [Please attach the details]
					</td>
					<td class="opt1">
					  <input type="checkbox" id="faktur_retail" name="faktur_retail" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">4</td>
					<td>
					  SPK Retail  [Please attach the details]
					</td>
					<td class="opt1">
					  <input type="checkbox" id="spk_retail" name="spk_retail" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">5</td>
					<td>
					  SPK Fleet  [Please attach the details]
					</td>
					<td class="opt1">
					  <input type="checkbox" id="spk_fleet" name="spk_fleet" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">6</td>
					<td>
					  Other
					</td>
					<td class="opt1">
					  <input type="checkbox" id="other" name="other" onclick="ShowHideOther(this)" value="TRUE">
					</td>
				  </tr>
				</table>
				 <div class="form-group" id="specify" style="display: none">
                  <label for="specify" class="col-sm-3 control-label" style="padding-right:10px">Spec. Other</label>

                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="specify" id="specify" placeholder="Mazda">
                  </div>
                </div>
              </div>
			</div>
		</form>
		</div>

          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
              <button class="btn btn-success pull-right" onclick="add_wrs()"><i class="fa fa-save"></i> Submit</button>
            </div>
          </div>
        </section><!-- /.content -->
        <div class="clearfix"></div>
      </div><!-- /.content-wrapper -->
      
	 <script type="text/javascript">
            $(function () {
                $('#datepicker').datetimepicker();
            });
     </script>
    <!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
  </body>
  
  <script type="text/javascript"> 
        $("#id_divisi").change(function(){
                var id_divisi = {id_divisi:$("#id_divisi").val()};
                   $.ajax({
               type: "POST",
               url : "<?php echo site_url('wrs_request_form/C_wrs/ajax_divisi/') ?>",
               data: id_divisi,
               success: function(msg){
               $('#id_dept').html(msg);
               }
            });
              });
</script>
  <script type="text/javascript">
    function ShowHideDiv(type4) {
        var type_other = document.getElementById("type_other");
        type_other.style.display = type4.checked ? "block" : "none";
    }
	
	function ShowHideTarget(target2) {
        var dealer_name = document.getElementById("dealer_name");
		var dealer_code = document.getElementById("dealer_code");
        dealer_name.style.display = target2.checked ? "block" : "none";
		dealer_code.style.display = target2.checked ? "block" : "none";
    }
	
	function ShowHideOther(other) {
        var specify = document.getElementById("specify");
        specify.style.display = other.checked ? "block" : "none";
    }
	</script>
  <script type="text/javascript">	

	var save_method; //for save method string
    var table;
		
    function add_wrs()
    {
     var url;
	 url = "<?php echo site_url('wrs_request_form/C_wrs/add_wrs')?>";
	 urlbef = "<?php echo site_url('wrs_request_form/C_wrs')?>";
	 var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR) 
            {
				if(data=='Insert'){
				window.location.href = urlbef;
			   }else {
			   alert (data);
			   }
			   
            },
        });
    }
  </script>
</html>
