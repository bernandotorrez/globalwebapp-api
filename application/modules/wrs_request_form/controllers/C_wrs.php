<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_wrs extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_wrs');
			$this->load->library('pagination'); //call pagination library
			$this->load->helper('form');
			$this->load->helper('terbilang');			
			$this->load->library('fpdf');   		
			$this->load->database();
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_wrs->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}
	 	}
		
	public function index()
		{

		$data['loan']=$this->M_wrs->get_all_wrs();
		$data['total']=$this->M_wrs->get_count_id();		
		$data['divisi']=$this->M_wrs->get_all_divisi();
		$data['group']=$this->M_wrs->get_all_group();
		$data['department']=$this->M_wrs->get_all_dept();
		$data['purchase']=$this->M_wrs->get_all_purchase();
		$data['company']=$this->M_wrs->get_all_company();
		$data['company2']=$this->M_wrs->get_all_company();
		$data['show_view'] = 'wrs_request_form/V_wrs';
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function ajax_list()
	{
		$list = $this->M_wrs->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_wrs_request) {
			
			$chk_idmaster ='<div align="center"><input id="checkuser" name="checkuser" type="checkbox" value='.$tbl_wrs_request->id_wrs_request.' class="editRow ace" />
           <span class="lbl"></span> ';
		   $print_pdf ='<div align="center"><a href="C_wrs/view_wrs_pdf?id='.$tbl_wrs_request->id_wrs_request.'" value='.$tbl_wrs_request->id_wrs_request.' name="print_pdf" type="button"  class="editRow ace">Detail</a>
           </div> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_wrs_request->id_wrs_request;
			$row[] = $tbl_wrs_request->requestor_name;
			$row[] = $tbl_wrs_request->dept;
			$row[] = $tbl_wrs_request->position;
			$row[] = $tbl_wrs_request->request_date;
			$row[] = $tbl_wrs_request->request_type;
			$row[] = $tbl_wrs_request->dealer_target;
			$row[] = $tbl_wrs_request->dealer_name;
			$row[] = $tbl_wrs_request->reason;
			$row[] = $tbl_wrs_request->remarks;
			$row[] = $tbl_wrs_request->name;
			$row[] = $print_pdf;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_wrs->count_all(),
						"recordsFiltered" => $this->M_wrs->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	public function reset_pass()
	{
		$data = array(
					'id' => $this->input->post('id'),
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('username')),
					);
		$this->M_wrs->update_wrs(array('id' => $this->input->post('id')), $data);
		echo 'Insert';
	}
	
	public function add_wrs()
		{
		$str_user =$this->session->userdata('id');
		$type = $_POST['type'];
		$value = implode(",",$type);
		$target = $_POST['target'];
		$value1 = implode(",",$target);
		if(isset($_POST['mmi_stock'])) {
			$value2 = 1; }
			else {
			$value2 = 0;
		}
		if(isset($_POST['dealer_stock'])) {
			$value3 = 1; }
			else {
			$value3 = 0;
		}
		if(isset($_POST['faktur_retail'])) {
			$value4 = 1; }
			else {
			$value4 = 0;
		}
		if(isset($_POST['spk_retail'])) {
			$value5 = 1; }
			else {
			$value5 = 0;
		}
		if(isset($_POST['spk_fleet'])) {
			$value6 = 1; }
			else {
			$value6 = 0;
		}
		
		if(empty($_POST["type"])){
			die ("Field Request Type must be filled in!! ");
		}
		elseif(empty($_POST["reason"])){
			die ("Field Reason must be filled in!! ");
		}
		elseif(empty($_POST["target"])){
			die ("Field Dealer Target must be filled in!! ");
		}
		elseif(empty($_POST["requestor_name"])){
			die ("Field Requestor Name must be filled in!! ");
		}
		elseif(empty($_POST["dept"])){
			die ("Field Department must be filled in!! ");
		}
		elseif(empty($_POST["position"])){
			die ("Field Position must be filled in!! ");
		}
		else{
			 $data = array(
					'id_user' => $str_user,
					'request_type' => $value,
					'type_other' => $this->input->post('type_other'),
					'dealer_target' => $value1,
					'dealer_name' => $this->input->post('dealer_name'),
					'dealer_code' => $this->input->post('dealer_code'),
					'reason' => $this->input->post('reason'),
					'remarks' => $this->input->post('remarks'),
					'requestor_name' => $this->input->post('requestor_name'),
					'dept' => $this->input->post('dept'),
					'position' => $this->input->post('position'),
					'mmi_stock' => $value2,
					'dealer_stock' => $value3,
					'faktur_retail' => $value4,
					'spk_retail' => $value5,
					'spk_fleet' => $value6,
					'specify' => $this->input->post('specify'),
					'status' => '1',
				); 
			
			$insert = $this->M_wrs->add_wrs($data);
		 }
		 echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_wrs->get_by_id($id);
			echo json_encode($data);
		}
		
	function pos_branch($id_company)
	{
    	$query = $this->db->get_where('tbl_branches',array('id_company'=>$id_company));
    	$data = "<option value=''>- Select Branch -</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->branch_id."'>".$value->name_branch."</option>";
    	}
    	echo $data;
	}
	
	public function ajax_divisi()
    {
        $id = $this->input->post('id_divisi');
        $data['dept'] = $this->M_wrs->get_dept($id);
		$data['dept2'] = $this->M_wrs->get_all_dept($id);
        $this->load->view('dept',$data);
    }
	
	public function update_wrs()
		{
		$str_user =$this->session->userdata('id');
		$type = $_POST['type'];
		$value = implode(",",$type);
		$target = $_POST['target'];
		$value1 = implode(",",$target);
		if(isset($_POST['mmi_stock'])) {
			$value2 = 1; }
			else {
			$value2 = 0;
		}
		if(isset($_POST['dealer_stock'])) {
			$value3 = 1; }
			else {
			$value3 = 0;
		}
		if(isset($_POST['faktur_retail'])) {
			$value4 = 1; }
			else {
			$value4 = 0;
		}
		if(isset($_POST['spk_retail'])) {
			$value5 = 1; }
			else {
			$value5 = 0;
		}
		if(isset($_POST['spk_fleet'])) {
			$value6 = 1; }
			else {
			$value6 = 0;
		}
		
		if(empty($_POST["type"])){
			die ("Field Request Type must be filled in!! ");
		}
		elseif(empty($_POST["reason"])){
			die ("Field Reason must be filled in!! ");
		}
		elseif(empty($_POST["target"])){
			die ("Field Dealer Target must be filled in!! ");
		}
		elseif(empty($_POST["requestor_name"])){
			die ("Field Requestor Name must be filled in!! ");
		}
		elseif(empty($_POST["dept"])){
			die ("Field Department must be filled in!! ");
		}
		elseif(empty($_POST["position"])){
			die ("Field Position must be filled in!! ");
		}
		else{
			 $data = array(
					'request_type' => $value,
					'type_other' => $this->input->post('type_other'),
					'dealer_target' => $value1,
					'dealer_name' => $this->input->post('dealer_name'),
					'dealer_code' => $this->input->post('dealer_code'),
					'reason' => $this->input->post('reason'),
					'remarks' => $this->input->post('remarks'),
					'requestor_name' => $this->input->post('requestor_name'),
					'dept' => $this->input->post('dept'),
					'position' => $this->input->post('position'),
					'mmi_stock' => $value2,
					'dealer_stock' => $value3,
					'faktur_retail' => $value4,
					'spk_retail' => $value5,
					'spk_fleet' => $value6,
					'specify' => $this->input->post('specify'),
				); 
			$this->M_wrs->update_wrs(array('id_wrs_request' => $this->input->post('id')), $data);
		  }
		   echo ('Insert');
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);
				$this->db->where('id_wrs_request', $id);
				$this->db->update('tbl_wrs_request', $data);
			}
		}
	}
	
	 public function view_add_wrs()
	{

	  $data['show_view']='wrs_request_form/V_add_wrs';
	  $this->load->view('dashboard/Template',$data);		  	 	     		   		      						   	   	
	}
	
	
	 public function view_wrs_pdf()
	{

	  $data['show_view']='wrs_request_form/V_print_wrs';
	 $this->load->view('dashboard/Template',$data);	  
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }



}
