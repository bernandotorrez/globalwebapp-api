<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_table_aproval extends CI_Model {	

	public function __construct() {
		parent::__construct();

		$this->load->helper('api_helper');
	}
				    
    public $db_tabel = 'qv_head_pp_complite';
	 
    // public function tampil_add_pp(){ //tampil table untuk ngececk num_row	
	
	//      if ( $this->session->userdata('aproval_flag')=="3" or $this->session->userdata('aproval_flag')=="0"){ //aproval falg 1= bod, 2 = fc , 3 = head. 
		     
	// 		 $ses_id_dept = $this->session->userdata('id_dept'); 
	// 	     $ses_id_company = $this->session->userdata('id_company');  	
	// 	     $ses_id_branch = $this->session->userdata('branch_id');		 		 					 
	// 		 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject									
	// 		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	// 		 $str_flag_purchase = "1"; //jika untuk flag unit
			 			 
			  
	// 		 $this->load->database();		
	// 		 $this->db->select('*');		
	// 		 $this->db->where('flag_purchase',$str_flag_purchase);
	// 		 $this->db->where('id_company',$ses_id_company);					
	// 		 $this->db->where('branch_id',$ses_id_branch );	
	// 		 $this->db->where('id_dept',$ses_id_dept);
	// 		 $this->db->where('status',$status_delete);
	// 		 $this->db->where('status_send_aprove',$send_aprove);													
	// 		 $this->db->order_by('date_send_aproval','desc');
	// 		 $result = $this->db->get('qv_head_pp_complite');			     		 
			 
	// 		 return $result;
			 
	// 	 }else{
	// 	    if ($this->session->userdata('aproval_flag') == "4"){	 
	// 			 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
	// 		     $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	// 		     $flag_head	=  "1"; //jika  1 sudah di aprove head dept di tampilkan di view FC 
	// 		     $flag_purchase_stat	=  "0"; //jika  0 belum di aprove fc di maka tampilkan di view FC
	// 		     //  $flag_aprove_fc =  "1"; //jika 1 sudah di aprove fc maka di tampilkan di view FC
	// 		   	 $flag_bod	=  "0"; //jika  0 belum di aprove bod maka di tampilkan di view FC 
	// 			 $str_flag_purchase = "1"; //jika untuk flag unit
				 			 
	// 		     $this->load->database();		
	// 		     $this->db->select('*');					
				 
	// 			 $this->db->where('flag_purchase',$str_flag_purchase);					 
	// 		     $this->db->where('aprove_head',$flag_head); //flag aprove Head
	// 			 $this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a				
	// 		    // $this->db->where('aprove_fc',$flag_fc);   //flag aprove purchase approve_purchasing				
	// 		    // $this->db->or_where('aprove_fc',$flag_aprove_fc);   //flag aprove FC			
	// 		     $this->db->where('status_send_aprove',$send_aprove);
	// 		     $this->db->where('status',$status_delete);
	// 		     $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D		
											 
	// 			 $this->db->order_by('date_send_aproval','desc');
	// 			 $result = $this->db->get('qv_head_pp_complite');			     		 
	// 			 return $result;
				 
	// 		}else{
				
	// 			 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
	// 			 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	// 			 $flag_head	=  "1"; //jika 1 sudah di aprove head dept dan..(kebawah) 
	// 			 $flag_purchase_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
	// 			 $flag_fc_approved_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
	// 			 $flag_bod	=  "0"; //jika 0 belum di aprove B.O.D ,maka di tampilkan di view BOD 
	// 			 //$str_flag_purchase = "1"; //jika untuk flag unit
				  
	// 			 $this->load->database();		
	// 			 $this->db->select('*');								
	// 			 //$this->db->where('flag_purchase',$str_flag_purchase);
	// 			 $this->db->where('aprove_head',$flag_head); //flag aprove Head
	// 			 $this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a				
	// 		     $this->db->where('aprove_fc',$flag_fc_approved_stat);   //flag aprove purchase a		
	// 			 $this->db->where('status_send_aprove',$send_aprove);
	// 		     $this->db->where('status',$status_delete);
	// 		     $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D		
											 
	// 			 $this->db->order_by('date_send_aproval','desc');
	// 			 $result = $this->db->get('qv_head_pp_complite');			     		 
	// 			 return $result;
				
				
	// 		}			 
	// 	 }
	// }	   

	// public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 
		 
	//    if ( $this->session->userdata('aproval_flag')=="3"){    
	//          $ses_id_company = $this->session->userdata('id_company'); 				 
	// 	     $ses_id_branch = $this->session->userdata('branch_id');	
	// 	     $ses_id_dept = $this->session->userdata('id_dept'); //manggili session id dept					 			  		
	// 		 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
	// 		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
	// 		 $str_flag_purchase = "1"; //jika untuk flag inderect	 
			  
	// 		 $this->load->database();		
	// 		 $this->db->select('*');	
	// 		 $this->db->where('flag_purchase',$str_flag_purchase);			
	// 		 $this->db->where('id_company',$ses_id_company);					
	// 	     $this->db->where('branch_id',$ses_id_branch );	
	// 	     $this->db->where('id_dept',$ses_id_dept);
	// 		 $this->db->where('status',$status_delete);
	// 		 $this->db->where('status_send_aprove',$send_aprove);				 			
	// 		 $this->db->order_by('date_send_aproval','desc');
	// 		 $this->db->limit($start_row, $limit);					     	 	   		
	// 		 $result = $this->db->get('qv_head_pp_complite')->result();		 		   
	// 		 return $result ;
    //    }else{	
	//    	   if ($this->session->userdata('aproval_flag') == "4"){	 
	// 		   $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject						
	// 		   $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	// 		   $flag_head	=  "1"; //jika  1 sudah di aprove head dept di tampilkan di view FC 
	// 		   $flag_purchase_stat	=  "0"; //jika  0 belum di aprove fc di maka tampilkan di view FC				
	// 		   $str_flag_purchase = "1"; //jika untuk flag inderect tampilkan di view FC 
	// 		   $flag_bod = "0";
			   				 
	// 		   $this->load->database();		
	// 		   $this->db->select('*');					
	// 		   $this->db->where('flag_purchase',$str_flag_purchase);			
	// 		   $this->db->where('status_send_aprove',$send_aprove);						 
	// 		   $this->db->where('aprove_head',$flag_head); //flag aprove Head
	// 		   $this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a					
	// 		  // $this->db->or_where('aprove_fc',$flag_aprove_fc);   //flag aprove FC						   
	// 		   $this->db->where('status',$status_delete);
	// 		   $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D													
																								 
	// 		   $this->db->order_by('date_send_aproval','desc'); 
	// 		   $this->db->limit($start_row, $limit);					     	 	   		
	// 		   $result = $this->db->get('qv_head_pp_complite')->result();		 		   
	// 		   return $result ;
			   
	// 	   }else{
			   
	// 		    $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
	// 		    $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	// 		    $flag_head	=  "1"; //jika 1 sudah di aprove head dept dan..(kebawah) 
	// 		    $flag_purchase_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
	// 		    $flag_fc_approved_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
	// 		    $flag_bod	=  "0"; //jika  0 sudah di aprove head dept dan sudah di aprove fc tetapi belum di aprove bod
							 
	// 		    $this->load->database();		
	// 		    $this->db->select('*');					
			   			   
	// 		   // $this->db->where('flag_purchase','1');		//indirect
	// 		    $this->db->where('aprove_head',$flag_head); //flag aprove Head
	// 			$this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a				
	// 		    $this->db->where('aprove_fc',$flag_fc_approved_stat);   //flag aprove purchase a							   			   
	// 		    $this->db->where('status_send_aprove',$send_aprove);
	// 		    $this->db->where('status',$status_delete);
	// 		    $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D		
				 
																												 
	// 		    $this->db->order_by('date_send_aproval','desc'); 
	// 		    $this->db->limit($start_row, $limit);					     	 	   		
	// 		    $result = $this->db->get('qv_head_pp_complite')->result();		 		   
	// 		    return $result ;
	// 	   }
	//    }
	// }	
	
	
	
	function give_aproval()						  		
	{
	     $strid_master =$this->input->post('msg'); 			  		 	
		 $flag_aproved = "1" ; // flag 1 aproved
		 
			 if ($this->session->userdata('aproval_flag')=="3") {			 
				  $buff_aproval ="aprove_head";
				  $buff_date ="date_aprove_head";
			 }else{
				   if ($this->session->userdata('aproval_flag')=="4") {					   
					   $buff_aproval ="approve_purchasing"; 
					   $buff_date ="date_approve_purchasing";
					}else{   
						if ($this->session->userdata('aproval_flag')=="2" || $this->session->userdata('aproval_flag')=="5" || $this->session->userdata('aproval_flag')=="6"|| $this->session->userdata('aproval_flag')=="7" || $this->session->userdata('aproval_flag')=="8") {						   					   
					   		$buff_aproval = "aprove_fc"; //automatic fc field approved
					   		$buff_date    = "date_aprove_fc";
				   		}else{
						   if ($this->session->userdata('aproval_flag')=="1") {					   
							  $buff_aproval ="aprove_bod"; 
							  $buff_date ="date_aprove_bod";
						   }else{
							   $buff_aproval="";
						   }
						}
				   }
			 }
			  			  
			  if (isset($buff_aproval) && trim($buff_aproval!=''))
			  {						  				  
				  for ($i=0; $i < count($strid_master) ; $i++) { 										
					
					if ($this->session->userdata('aproval_flag')=="4") : //jika flag approval adalah purchasing
					   $data = array($buff_aproval=>$flag_aproved,
					                 // $buff_aproval_fc=> $flag_aproved,
									 // $buff_date_fc => date('Y-m-d'), 	 						  
									  $buff_date =>date('Y-m-d H:i:s'), 	
									  'date_aproval' =>date('Y-m-d H:i:s'),
									  'id_master' => $strid_master[$i]
									  );						
					
					else:
					   if ($this->session->userdata('aproval_flag')=="2" || $this->session->userdata('aproval_flag')=="5" || $this->session->userdata('aproval_flag')=="6"||$this->session->userdata('aproval_flag')=="7" || $this->session->userdata('aproval_flag')=="8") : //jika flag approval adalah fc
						   $data = array($buff_aproval=>$flag_aproved,
										  $buff_date =>date('Y-m-d H:i:s'), 	
										  'date_aproval' =>date('Y-m-d H:i:s'),	
										  'id_master' => $strid_master[$i]									  
										  );	
						
						else:
							if ($this->session->userdata('aproval_flag')=="1") : //jika flag approval adalah BOD
							   $data = array($buff_aproval=>$flag_aproved,
											  $buff_date =>date('Y-m-d H:i:s'), 	
											  'date_aproval' =>date('Y-m-d H:i:s'),
											  'id' => $strid_master[$i]
											  );	
							
							else:
								$data = array($buff_aproval=>$flag_aproved,
											  $buff_date =>date('Y-m-d H:i:s'), 	
											  'date_aproval' =>date('Y-m-d H:i:s')
											  );	
							endif;	
						endif;				  																																	
					endif;
									  
					// $this->db->where('id_master',$strid_master[$i]);
					// $this->db->update('tbl_master_pp',$data); 

					$query = http_request_put(API_URL.'/PRForm/updateMasterPP', $data);
					$query = json_decode($query);
					
					$buff_ses_idmaster = array('ses_idmaster'=>$strid_master[$i]);
				    $this->session->set_userdata($buff_ses_idmaster);				
					
					$value[] = $strid_master[$i] ;	//memasukan beberapa id master yg di select ke dalam array.				
					$this->session->set_userdata('result_id_master',$value); //simpan session array					
					//print_r($buff_ses_idmaster); echo array													
				  }				 				  				 				  				
				  return true;	
			  }else{
				  return false;
			  }
	  
		  
	}
	
	function give_rejected()						  		
	{	     		  		 	
		 $strid_master = $this->input->post('txtppno');
		 $str_name_reject = "By"." ".$this->session->userdata('name')." " . ":";
		 $strid_reason_reject =  $str_name_reject." ".$this->input->post('txtreject');
		 $flag_zero_aprove = "0" ; // menggembailkan flag approve menjadi 0 	 
		 $flag_reject = "-1" ; // flag -1 flag_reject pada field status_send_approval
		 $buff_status_reject = "status_send_aprove";
		
		 //$buff_reason_reject = "reason_reject";
		 
		   if ($this->session->userdata('aproval_flag')=="3") {			 
				 // $buff_aproval ="aprove_head";		
				  //$flag_back_aprove = "0" ;
				   $buff_field_date_reject ="date_reject_head";
				   $buff_date_reject = date('Y-m-d H:i:s');
			 }else{
				   if ($this->session->userdata('aproval_flag')=="4") {					   
					  // $buff_aproval ="approve_purchasing"; 					
					  // $flag_back_aprove = "0" ;
					     $buff_field_date_reject ="date_reject_purchasing";
				  		 $buff_date_reject = date('Y-m-d H:i:s');
				   }else{
						 if ($this->session->userdata('aproval_flag')=="2" || $this->session->userdata('aproval_flag')=="5" || $this->session->userdata('aproval_flag')=="6"||$this->session->userdata('aproval_flag')=="7" || $this->session->userdata('aproval_flag')=="8") {					   
						   //$buff_aproval ="aprove_fc"; 					
						  // $flag_back_aprove = "0" ;
						      $buff_field_date_reject ="date_reject_fc";
				              $buff_date_reject = date('Y-m-d H:i:s');
					    }else{
							   if ($this->session->userdata('aproval_flag')=="1") {					   
								 // $buff_aproval ="aprove_bod"; 						  
								 // $flag_back_aprove = "0" ;
								   $buff_field_date_reject ="date_reject_bod";
				  				   $buff_date_reject = date('Y-m-d H:i:s');
							   }else{
								    $buff_field_date_reject ="";
				  					 $buff_date_reject = "";
							   }
						}
				   }
			 } 
		 
		 			
			  if (isset($buff_status_reject) && trim($buff_status_reject!=''))
			  {		
						
					// $query = $this->db->select("id_master") //check id booking pada table_parent 								
					// 					->where('id_master',$strid_master)
					// 					->get('tbl_master_pp');

					$parameter = array('id_master' => $strid_master);
					$query = http_request_get(API_URL.'/PRForm/getPP', $parameter);
					$query = json_decode($query, true);

					  if ($query['length'] == 1)
					  {				
						/*$data = array($buff_status_reject=>$flag_reject,
						              $buff_reason_reject=>$strid_reason_reject,
									  $buff_aproval => $flag_back_aprove
									  ); */	
													
						$data = array('status_send_aprove'=>$flag_reject,
						              'reason_reject'=> $strid_reason_reject,
									  //'aprove_head' => $flag_zero_aprove,
									  'approve_purchasing' => $flag_zero_aprove,
									  'aprove_fc' => $flag_zero_aprove,
									  'aprove_bod' => $flag_zero_aprove,
									  $buff_field_date_reject => $buff_date_reject,
									  'id_master' => $strid_master
									  ); 						

						// $this->db->where('id_master',$strid_master);
						// $this->db->update('tbl_master_pp',$data); 

						$query = http_request_put(API_URL.'/PRForm/updateMasterPP', $data);
						$query = json_decode($query);
						
						$buff_ses_idmaster = array('ses_idmaster'=>$strid_master);
						$this->session->set_userdata($buff_ses_idmaster);
						
						return true;
					  }		  
			  }else{
				  return false;
			  }
	 }

	 public function getAllUserDeligate($id_company) {
		// $this->db->where('id_company', $id_company);
		// $this->db->where('flag_deligated', '1');
		// $query = $this->db->get('tbl_user');

		// return $query->result();

		$parameter = array('id_company' => $id_company);
		$query = http_request_get(API_URL.'/approvalPurchase/getAllUserDeligate', $parameter);
		$query = json_decode($query);

		return $query->data;
	 }

	 public function getUserDeligate($id, $id_company) {
		// $this->db->where('id_company', $id_company);
		// $this->db->where('flag_deligated', '1');
		// $this->db->where('status', '1');
		// $this->db->where('aproval_flag !=', '10');
		// $this->db->where('id', $id);
		// $query = $this->db->get('tbl_user');

		// return $query->result();

		$parameter = array('id_company' => $id_company, 'id' => $id);
		$query = http_request_get(API_URL.'/approvalPurchase/getUserDeligate', $parameter);
		$query = json_decode($query);

		return $query->data;
	 }

	 public function getEmailDeligate($id) {
		// $this->db->where('id', $id);
		// $query = $this->db->get('tbl_user');

		// return $query->result();

		$parameter = array('id' => $id);
		$query = http_request_get(API_URL.'/approvalPurchase/getEmailDeligate', $parameter);
		$query = json_decode($query);

		return $query->data;
	 }

	 public function updateUserDeligate($id, $data) {
		//  $this->db->where('id', $id);
		//  $this->db->where('flag_deligated', '1');
		//  $query = $this->db->update('tbl_user', $data);

		//  return $query;

		$query = http_request_put(API_URL.'/approvalPurchase/updateUserDeligate', $data);
		$query = json_decode($query);

		return $query->data;
	 }

	 public function getQuotation($id_master) {
		// $this->db->select('attach_quo, attach_quo_purchase, attach_quo_purchase2, attach_quo_purchase3');
		// $this->db->where('status', '1');
		// $this->db->where('id_master', $id_master);
		// $result = $this->db->get('qv_head_pp_complite');		
		// return $result->result();

		// API getPP
		$parameter = array('id_master' => $id_master);	
		$query = http_request_get(API_URL.'/PRForm/getPP', $parameter);
		$query = json_decode($query);

		return $query->data;
	}
}
