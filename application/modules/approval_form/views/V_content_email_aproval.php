
<h3><label>Reject Info</label></h3>    

<?php foreach ($str_pp_master as $row)  :  ?>
<h2>
	<?php
	    $strcom = $this->session->userdata('company');
		$strbranch = $this->session->userdata('name_branch');
		$strdept = $this->session->userdata('dept');
		
		if ($row->status_send_aprove =="-1" ) {		
		   $str_rejected_By ='<td >'.'Rejected By :'.'</td>';
		   $str_reject_name ='<td >'.$this->session->userdata('name').'</td>';
		   $str_reject_label ='<td >'.'Rejected Reason :'.'</td>';
		   $str_reject_reason = '<td>'.$row->reason_reject.'</td>';
		}else{	
		    if ($row->status_send_aprove =="1" and $row->aprove_bod == "1")
			{
				 $header = 'Approved Request by B.O.D';		        
				 echo "PP Has Been Approved By B.O.D : "." ".$this->session->userdata("name");
				 //echo "PP Has Been Approved By Board.Of.Director(B.O.D)";
				 $str_rejected_By ='';
				 $str_reject_name ='';
				 $str_reject_label ='';
				 $str_reject_reason = '';
			} 
		}		 
	   
	 ?>
</h2>

<table border="1" width="100%" >
<?php  if ($row->status_send_aprove =="-1" ) {	 ?>
    <tr>
      <?php echo $str_rejected_By; ?>
      <?php echo $str_reject_name; ?>
    </tr> 
     <tr>
       <?php echo $str_reject_label; ?>
       <?php echo $str_reject_reason; ?>      
    </tr> 
<?php } ?>    
    <tr >
      <td >PP Number :</td>
      <td><?php echo  $this->session->userdata("ses_idmaster");  ?></td>
    </tr>
    <tr >
      <td >Requester :</td>
      <td ><?php echo $row->user_submission ?></td>
    </tr>
    <tr >
      <td >Date Sent Approval : </td>
      <td ><?php echo date('d-m-Y', strtotime($row->date_send_aproval)) ?></td>
    </tr>    
    <tr >
      <td >Date Approval : </td>
      <td ><?php echo date('d-m-Y', strtotime($row->date_send_aproval)) ?></td>
    </tr>    
    <?php 
	   if ($this->session->userdata == "1"  ) { 
       echo'<tr>';
       echo '<td >Time Approval : </td>';
       echo '<td>'.$row->time_approved .'</td>';
       echo ' </tr>';
       } 
	?>   
   <!--  <tr>
    <td>C.O.A :</td>
       <td><?php //echo $row->nomor_coa ?></td>
    </tr> --> 
    <tr >
      <td  >T.O.P :</td>
       <td ><?php echo $row->term_top ?></td>
    </tr>
     <tr >
       <td >Remarks:</td>
       <td ><?php echo $row->remarks ?></td>
    </tr>
    <tr>
       <td ><?php echo "Grand Total : "." ".$row->currency ?></td>
       <td ><?php echo number_format($row->gran_total,2,'.',',')?></td>
    </tr>
    <tr>
       <td ><?php echo "PPN : "." ".$row->currency ?></td>
       <td ><?php echo number_format($row->ppn,2,'.',',')?></td>
    </tr>
    <tr>
       <td ><?php echo "Grand Total + PPN : "." ".$row->currency ?></td>
       <td ><?php echo number_format($row->gran_totalppn,2,'.',',')?></td>
    </tr>
 <?php endforeach; ?>   
</table>

</br>

<h2>Detail PP</h2>
<table border="1" width="100%" >    
    <tr >
        <td >No</td>
        <td >Description</td>
        <td >Spec</td>
       <!-- <td >Po Reff</td> -->
        <td >Qty</td>
        <td >Price </td>							
        <td >Total Price</td>
    </tr>
    <?php foreach ($str_pp_detail as $row_detail)  : $intno = $intno +1;  ?>
   		<tr>	
          <td><?php echo $intno ?></td>							  
          <td><?php echo $row_detail->desc ?></td>							  
          <td><?php echo $row_detail->spec ?></td>		
         <!-- <td><?php //echo $row_detail->po_reff ?></td> -->																																																				
          <td><?php echo number_format($row_detail->qty) ?></td>							
          <td><?php echo  number_format($row_detail->harga,2,'.',',') ?></td>							
          <td><?php echo number_format($row_detail->total,2,'.',',') ?></td> 
        </tr>
    <?php endforeach; ?>
</table>