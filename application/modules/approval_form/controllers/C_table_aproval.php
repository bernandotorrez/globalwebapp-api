<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_table_aproval extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_table_aproval','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		//$this->load->database();	
		$this->load->helper('api_helper');						
	}		  	
		
  public function index()
  {					    	
  		 
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	 //$tampil_table_aproval= $this->M_table_aproval->tampil_add_pp()->result();	
	// $total_rows =$this->M_table_aproval->tampil_add_pp()->num_rows();	  										  	
	 $dept  = $this->session->userdata('dept') ;	
	 $branch  = $this->session->userdata('name_branch') ; 
	 $company = $this->session->userdata('short') ;		
	 $data['intno'] = ""; //variable buat looping no table.				
	 $data['header'] ="Aproval Form"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	 //$data['ceck_row'] = $total_rows;	
	// $data['tampil_company']=$this->M_table_aproval->get_company()->result();																															
	// $data['tampil_dept']=$this->M_table_aproval->get_dept()->result();																															
	 $data['show_view'] = 'approval_form/V_table_aproval';		
	 $this->load->view('dashboard/Template',$data);					
				 
  }
  
  public function get_idtrans_modal()
  {
         // $tampungmaster = $this->M_create_pp->m_mastergetid_pp();
	     //$tampungdetail = $this->M_create_pp->m_getid_pp();
	 
	     $status_aktif_record = "1" ; // 1 = masih aktif , 0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //       
	   
	     //$tampungmaster = $this->db->query("select id_master,remarks,gran_total,currency,ppn,pph,gran_totalppn from qv_head_pp_complite where id_master ='".$nopp."'");  
         //$tampungdetail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		
		  // Call API PRForm/getPP and getDetailPP
		  $parameter = array('id_master' => $nopp);

		  $tampungmaster = http_request_get(API_URL.'/PRForm/getPP', $parameter);
		  $tampungmaster = json_decode($tampungmaster);
 
		  $tampungdetail = http_request_get(API_URL.'/PRForm/getDetailPP', $parameter);
		  $tampungdetail = json_decode($tampungdetail);

		 $no="1";
         echo "<p>";
		
		 // echo "<div style='overflow-x:auto;'>" ;
		 echo "<div class='table-responsive'>" ;
		 echo "<table class='table table-striped table-bordered table-hover' width='70%'>";
		 echo  "<tr style='font-weight:bold; font-size:11px' class='btn-info'>";
		 echo  "<td width='3%' align='center'>No</td>";
		 echo  "<td width='20%'  align='center'>Item Type</td>";
		 echo  "<td width='20%'  align='center' >Description</td>";
		 // echo  "<td width='10%'>P.O Reff</td>";
		 // echo  "<td width='10%'>C.O.A</td>";
		 // echo  "<td width='10%'>No Actifity</td>";
		 echo  "<td  align='center' >Qty</td>";
		 echo  "<td width='15%' align='center'>Prices</td>";
		 echo  "<td width='15%' align='center' >Total Prices</td>";
		 echo  "<td width='2%'>PPN Type</td>";
		 echo  "<td width='2%'>PPH Type</td>";
		 echo  "<td width='15%'>PPN Amount</td>";
		 echo  "<td width='20%'>PPH Amount</td>";
		 echo  "</tr> ";
			foreach ($tampungdetail->data as $row_jq) {
				echo  "<tr style='font-size:12px'> ";
				echo '<td >'.$no++.'</td>';
				echo '<td>'.$row_jq->desc.'</td>';
				echo '<td>'.$row_jq->spec.'</td>';
				//echo '<td>'.$row_jq->po_reff.'</td>';
				//echo '<td>'.$row_jq->coa.'</td>';
				//echo '<td>'.$row_jq->no_act.'</td>';
				echo '<td align="center" style="text-align:center;">'.$row_jq->qty.'</td>';
				echo '<td align="right" style="text-align:right;">'.number_format($row_jq->harga,2,'.',',').'</td>';
				echo '<td align="right" style="text-align:right;">'.number_format($row_jq->total,2,'.',',').'</td>';
				echo '<td>'.$row_jq->tax_type.'</td>';
				echo '<td>'.$row_jq->tax_typepph.'</td>';
				echo '<td align="right" style="text-align:right;">'.number_format($row_jq->tax_detail,2,'.',',').'</td>';
				echo '<td align="right" style="text-align:right;">'.number_format($row_jq->tax_detailpph,2,'.',',').'</td>';
				echo "</tr>";
	
			}
	
		 foreach ($tampungmaster->data as $row_jm) {
			 echo  "<tr style='font-weight:bold' class='btn-success'>";
			 echo  "<td width='27%' colspan='2'>Remarks :</td>";
			 echo  "<td width='40%' colspan='8'>".$row_jm->remarks."</td>";
			 echo  "</tr> ";
			 echo  "<tr style='font-weight:bold' class ='btn-danger' >";
			 echo  "<td width='25%' colspan='9' align='right'>Total (".$row_jm->currency.") : </td>";
			 echo  "<td width='40%' style='text-align: right'>".number_format($row_jm->gran_total,2,'.',',')."</td>";
			 echo  "</tr> ";
			 echo  "<tr style='font-weight:bold' class ='btn-light'>";
			 echo  "<td width='25%' colspan='9' align='right'>PPN Amount (".$row_jm->currency.") : </td>";
			 echo  "<td width='40%' style='text-align: right'>".number_format($row_jm->ppn,2,'.',',')."</td>";
			 echo  "</tr> ";
			 echo  "<tr style='font-weight:bold' class ='btn-light'>";
			 echo  "<td width='25%' colspan='9' align='right'>PPH Amount (".$row_jm->currency.") : </td>";
			 echo  "<td width='40%' style='text-align: right'>(".number_format($row_jm->pph,2,'.',',').")</td>";
			 echo  "</tr> ";
			 echo  "<tr style='font-weight:bold' class ='btn-danger'>";
			 echo  "<td width='25%' colspan='9' align='right'>Grand Total (".$row_jm->currency.") : </td>";
			 echo  "<td width='40%' style='text-align: right'>".number_format($row_jm->gran_totalppn,2,'.',',')."</td>";
			 echo  "</tr> ";
			 echo "</table>";
			 echo "</div>";
		  }

		  $data_attach = $this->M_table_aproval->getQuotation($nopp);
	 $data_attach = $data_attach[0];

	 if($data_attach->attach_quo != '') {
		$attach_quo = '<a id="attach_quo" onClick="PDFPopup(this)" req_id='.base_url($data_attach->attach_quo).' href="#" class="btn btn-app btn-primary btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a>';
	 } else {
		$attach_quo = '<font color="red">'."No Quotation".'</font>';
	 }

	 if($data_attach->attach_quo_purchase != '') {
		$attach_quo_purchase = '<a id="attach_quo_purchase" onClick="PDFPopup(this)" req_id='.base_url($data_attach->attach_quo_purchase).' href="#" class="btn btn-app btn-primary btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".' 1 '.'</a>';
	 } else {
		$attach_quo_purchase = '<font color="red">'."No Quotation Purchase".'</font>';
	 }

	 if($data_attach->attach_quo_purchase2 != '') {
		$attach_quo_purchase2 = '<a id="attach_quo_purchase2" onClick="PDFPopup(this)" req_id='.base_url($data_attach->attach_quo_purchase2).' href="#" class="btn btn-app btn-primary btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".' 2 '.'</a>';
	 } else {
		$attach_quo_purchase2 = '<font color="red">'."No Quotation Purchase 2".'</font>';
	 }

	 if($data_attach->attach_quo_purchase3 != '') {
		$attach_quo_purchase3 = '<a id="attach_quo_purchase3" onClick="PDFPopup(this)" req_id='.base_url($data_attach->attach_quo_purchase3).' href="#" class="btn btn-app btn-primary btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".' 3 '.'</a>';
	 } else {
		$attach_quo_purchase3 = '<font color="red">'."No Quotation Purchase 3".'</font>';
	 }

	 echo '<div class="modal-footer">
	 <div class="col-md-4 text-left">
	 Attached Quotation Requester: '.$attach_quo.'
	                        
	 </div>

	 <div class="col-md-6 text-left">
	 Attached Quotation Purchase: '.$attach_quo_purchase.'  '.$attach_quo_purchase2.'  '.$attach_quo_purchase3.'
	                      
	 </div>

	 <div class="col-md-2">
	 <button type="button" class="btn btn-app btn-primary btn-xs radius-8 btnclose" data-dismiss="modal">Close</button>                       
					
	 </div>

 </div>';
}
  
  
  
  
  
  public function do_aproval()	
  {
		if ($this->M_table_aproval->give_aproval())
		{	 	
			$this->session->set_flashdata('pesan_aproval','1');	
		
		    if ($this->session->userdata('aproval_flag')=="2" || $this->session->userdata('aproval_flag')=="5" || $this->session->userdata('aproval_flag')=="6" || $this->session->userdata('aproval_flag')=="7" || $this->session->userdata('aproval_flag')=="8") {	
			    $this->kirim_email_to_bod();
			}else{
			 	if ($this->session->userdata('aproval_flag')=="4") {	
			    	$this->kirim_email_to_fc();
				}else{
			 		//if ($this->session->userdata('aproval_flag')=="1") {	
			    	    $this->kirim_email_aprove(); 
				}
			}
		    							
			redirect('approval_form/c_table_aproval'); //riderct ke menu utama  			
			//print_r($this->session->userdata("result_id_master"));	
			
		}else{
			$this->session->set_flashdata('pesan_aproval','0');		
			redirect('approval_form/c_table_aproval');   
		}
  }
  
  public function do_rejected()	
  {
		if ($this->M_table_aproval->give_rejected())
		{	 			    
			$this->session->set_flashdata('pesan_reject','1');	
			$this->kirim_email_reject();		
			redirect('approval_form/c_table_aproval');   
		}else{			
			$this->session->set_flashdata('pesan_reject','0');		
			redirect('approval_form/c_table_aproval');     
		}
  }
  
  
   public function do_search_pp()	
  {		
  		 $tampil_table_aproval= $this->M_table_aproval->get_search()->result();	
	     $total_rows =$this->M_table_aproval->tampil_add_pp()->num_rows();
  	
		if ($tampil_table_aproval)
		{	 	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;					
			$data['intno'] = ""; //variable buat looping no table.	
			$data['header'] ="Approval Form"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				
			$data['ceck_row'] = $total_rows;	
			$data['pp_view'] =  $tampil_table_aproval;
			$data['tampil_company']=$this->M_table_aproval->get_company()->result();
			$data['tampil_dept']=$this->M_table_aproval->get_dept()->result();																																
			$data['show_view'] = 'approval_form/V_table_aproval';		
			$this->load->view('dashboard/Template',$data);					
	  }else{	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;	
			$data['intno'] = ""; //variable buat looping no table.					
			$data['header'] ="Aproval Form"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;	
			$data['pp_view'] =  $tampil_table_aproval;
			$data['tampil_company']=$this->M_table_aproval->get_company()->result();	
			$data['tampil_dept']=$this->M_table_aproval->get_dept()->result();																															
			$data['pesan'] = 'Data PP aproval table is empty';				
			$data['show_view'] = 'approval_form/V_table_aproval';
			$this->load->view('dashboard/Template',$data);					
	  } 
		
  }
  
   
  public function multiple_submit() //fungsi submit dengan banyak button dalam satu form
  {  
	
		if ($this->input->post('btnaprove')){	
			$this->do_aproval();
		}else{
			if ($this->input->post('btnreject')){	//dari modal popup
				$this->do_rejected();							
			}else{
				if ($this->input->post('btncari')){	
					$this->do_search_pp();
				}else{		
					redirect('approval_form/c_table_aproval');
				}
			}
		}
	

	}

//------------------------------------pashing result to modal popup-------------------
	public function get_nopp_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //       
	     //$query_master = $this->db->query("select id_master,remarks,gran_total,currency,ppn,pph,gran_totalppn from qv_head_pp_complite where id_master ='".$nopp."'");  
		 //$query_detail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		 
		// Call API PRForm/getPP and getDetailPP
		$parameter = array('id_master' => $nopp);

		$tampungmaster = http_request_get(API_URL.'/PRForm/getPP', $parameter);
		$query_master = json_decode($tampungmaster);

		$tampungdetail = http_request_get(API_URL.'/PRForm/getDetailPP', $parameter);
		$query_detail = json_decode($tampungdetail);
		   		  
		   if($query_master->length > 0 and $query_detail->length >0){	   
		   
		      $data['str_pp_master']=$query_master->data;
			  $data['str_pp_detail']=$query_detail->data;
			  $data['intno']="";			  			  			  
			  $data['phasing_nopp'] = $nopp ;
			  $tampil_detail = $this->load->view('approval_form/V_conten_detail',$data,true);
			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
						

public function kirim_email_reject(){          
 		   
		   $this->load->library('email');				 								   		   		  	        
		   //konfigurasi email		
		 /*  $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;		
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.gmail.com';
		   
		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';		  		  
		   $config['smtp_port']  = '5220';	
		   //$config['smtp_port']  = '465'; portgmail
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   
		   //$config['smtp_user']= "epurchaseeuro@gmail.com"; //Gmail
		   //$config['smtp_pass']= "#purchas3"; 
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5"; */
		   //--------------------------------	
		   
		   $config['charset']    = 'utf-8';
           $config['newline']    = "\r\n";
           $config['mailtype'] = 'html'; 
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!"; 
           $config['validation'] = TRUE;      
           $this->email->initialize($config);	   		  
		    		   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			$str_status = '1' ; //aktif ;
						
			if ($strponumber != '') :						  	
				//Select untuk email master PP----------------------------
				//$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,ppn,pph,gran_total,gran_totalppn,remarks,status_send_aprove,aprove_bod,reason_reject,email_pp,email_head_pp,email_cc_pp from qv_head_pp_complite where id_master ='".$strponumber."' and status ='".$str_status."'");				
				$parameter = array('id_master' => $strponumber);
				$query = http_request_get(API_URL.'/PRForm/getPP', $parameter);
				$query = json_decode($query);		
				if ($query->length > 0 ) :
			      $data_email['str_pp_master'] = $query->data;		
				  foreach ($query->data as $row_ceck) :	
				     //simpan row alamat email kedalam variable..
					 	 $struseremail = $row_ceck->email_pp ; //addrees for to email
					     $strheademail = $row_ceck->email_head_pp; //addrees for to email
						 $strccemail = $row_ceck->email_cc_pp ; //addrees for to email
					 //end-----------------------------------------	
					 		   
				     //Select untuk email deteail PP--------------------------------
				     //$query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."' and status ='".$str_status."'");		
					 $query_detail = http_request_get(API_URL.'/PRForm/getDetailPP', $parameter);
					 $query_detail = json_decode($query_detail);
					 if ($query_detail->length > 0 ) :										 
					    $data_email['str_pp_detail'] = $query_detail->data;	
					    $data_email['intno'] = ""; //untuk counter angka table																									
					    $message = $this->load->view('approval_form/V_content_email_aproval',$data_email,true);														
					  endif;
					  //end-----------------------------------------------------------					 
						  if ($row_ceck->status_send_aprove =="-1")	:					  														  							   														
								$subject = "PP REJECTED-- Eurokarsgroup Indonesia";		
								$result = $this->email ;							   
								$this->email->initialize($config);  
								$this->email->set_newline("\r\n"); 
							   
								//Exchange
								//konfigurasi pengiriman					
								
								$sender_email = "helpdesk@eurokars.co.id"; 					   
								$sender_name = "Epurchasing Notification";					   					    
								
								//GMAIL-----
								//$sender_email = "epurchaseeuro@gmail.com";
								//$sender_name = "Epurchasing Eurokarsgroup";					   					    																						   
								//$to = 'brian.yunanda@eurokars.co.id';
								
							    $to = $struseremail.",".$strheademail.",".$strccemail; //email address,user,head, and CC
								 
								$this->email->from($sender_email, $sender_name);
								$this->email->to($to);
								$this->email->subject($subject);  
								
								//$this->email->message(print_r($message, true));			   
							  
								$this->email->message($message);// tanpa array	
																		 
								if ($this->email->send()) :		  	
									$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
									//redirect(current_url());
								else:
									show_error($this->email->print_debugger());
								endif; 										   						   
						      endif;
					      endforeach;							    
			         endif;
			     endif;				 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
			 
	      
 }
 
 public function kirim_email_aprove(){          
 		   
		   $this->load->library('email');				 								   		   		  	        
		   //konfigurasi email		
		  /* $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;		
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.gmail.com';
		   
		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';		  		  
		    $config['smtp_port']  = '5220';	
		   //$config['smtp_port']  = '465'; port gmail
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   
		   //$config['smtp_user']= "epurchaseeuro@gmail.com"; //Gmail
		   //$config['smtp_pass']= "#purchas3"; 
		    $config['smtp_user']= "helpdesk@eurokars.co.id";
		    $config['smtp_pass']= "#ur0kar5"; */ 
		   //--------------------------------	
		   
		   $config['charset']    = 'utf-8';
           $config['newline']    = "\r\n";
           $config['mailtype'] = 'html'; 
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!"; 
           $config['validation'] = TRUE;      
           $this->email->initialize($config);	   		  
		    		   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			$str_status = '1' ; //aktif ;
			
			if ($strponumber != '') :						  	
				//Select untuk email master PP----------------------------
				//$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,ppn,ppn,gran_total,gran_totalppn,remarks,status_send_aprove,aprove_bod,reason_reject,email_pp,email_head_pp,email_cc_pp from qv_head_pp_complite where id_master ='".$strponumber."' and status ='".$str_status."'");				
				$parameter = array('id_master' => $strponumber);
				$query = http_request_get(API_URL.'/PRForm/getPP', $parameter);
				$query = json_decode($query);
				if ($query->length > 0 ) :
			      $data_email['str_pp_master'] = $query->data;		
				  foreach ($query->data as $row_ceck) :	
				  			   				     	
					 //simpan row alamat email kedalam variable..
					 	$struseremail = $row_ceck->email_pp ;
						$strheademail = $row_ceck->email_head_pp;
						$strccemail = $row_ceck->email_cc_pp ;
					 //end-----------------------------------------
					
				     //Select untuk email detail PP--------------------------------
				     //$query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."' and status ='".$str_status."'");		
					 $query_detail = http_request_get(API_URL.'/PRForm/getDetailPP', $parameter);
					 $query_detail = json_decode($query_detail);
					 if ($query_detail->length > 0 ) :					 																					
					    $data_email['str_pp_detail'] = $query_detail->data;	
					    $data_email['intno'] = ""; //untuk counter angka table																									
					    $message = $this->load->view('approval_form/V_content_email_aproval',$data_email,true);														
					  endif;				
					  //end---------------------------------------------------------
					  		
							 if ($row_ceck->status_send_aprove =="1" and $row_ceck->aprove_bod == "1")	:										 																									
									$subject = "PP APPROVED -- Eurokarsgroup Company";	
									$result = $this->email ;							   
									$this->email->initialize($config);  
									$this->email->set_newline("\r\n"); 
								   				
									
									$sender_email = "helpdesk@eurokars.co.id"; 					   
									$sender_name = "Epurchasing Notification";					   					    
									
									
									$to = $struseremail.",".$strheademail.",".$strccemail; //email address,user,head, and CC
									 
									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);  
									
									//$this->email->message(print_r($message, true));			   
								  
									$this->email->message($message);// tanpa array	
																			 
									if ($this->email->send()) :		  	
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
										//redirect(current_url());
									//else:
									//	show_error($this->email->print_debugger());
									endif;
								 endif;						    
					      endforeach;							    
			         endif;
			     endif;				 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
			 
	      
 }
 
 public function kirim_email_to_fc(){          
 		   
		   $this->load->library('email');				 								   		   		  	        		  
		  /* $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;				  		
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;		     		  
		   $config['smtp_port']  = '5220';		  		  		
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   		   
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5";  */
		   //--------------------------------	
		   $config['charset']    = 'utf-8';
           $config['newline']    = "\r\n";
           $config['mailtype'] = 'html'; 
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!"; 
           $config['validation'] = TRUE;      
           $this->email->initialize($config);	   		  
		    		   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			
			if ($strponumber != '')
			 {				  	
				//Select untuk email master PP----------------------------
				//$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,gran_total,ppn,pph,type_purchase,gran_totalppn,flag_purchase,remarks from qv_head_pp_complite where id_master ='".$strponumber."'and status = '1'");				
				$parameter = array('id_master' => $strponumber);
				$query = http_request_get(API_URL.'/PRForm/getPP', $parameter);
				$query = json_decode($query);
				if ($query->length > 0 ) {
					$data_email['str_pp_master'] = $query->data;		
								   
					//$query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."'and status = '1'");		
					$query_detail = http_request_get(API_URL.'/PRForm/getDetailPP', $parameter);
					 $query_detail = json_decode($query_detail);
					if ($query_detail->length > 0 ) {										 
					   $data_email['str_pp_detail'] = $query_detail->data;	
					   $data_email['intno'] = ""; //untuk counter angka table	
					 		
					   $message = $this->load->view('approval_form/V_content_email_to_bod_fc',$data_email,true);
					 						
					} 
			   }
			}
					 
			$result = $this->email ;							   
			$this->email->initialize($config);  
			$this->email->set_newline("\r\n"); 
		   
		   				
			
			$sender_email = "helpdesk@eurokars.co.id"; 					   
			$sender_name = "Epurchasing Notification";					   					    
												   					    
									$result = $this->email ;							   
									$this->email->initialize($config);  
									$this->email->set_newline("\r\n"); 
																			
									//$to = 'andri.widjaja@mazda.co.id' ;																	   
									$to = 'brian.yunanda@eurokars.co.id';
									$subject = 'submission Approval F.C';
									
									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);  																		
								  
									$this->email->message($message);// tanpa array	
																			 
									if ($this->email->send()) :		  	
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
									//	redirect(current_url());
									else:
										show_error($this->email->print_debugger());
									endif;												    						 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
			 
	      
 }

 
 public function kirim_email_to_bod(){          
 		   
		  $this->load->library('email');				 								   		   		  	        		  
		  /* $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;				  		
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;		     		  
		   $config['smtp_port']  = '5220';		  		  		
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   		   
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5"; */ 
		   //--------------------------------	
		   
		   $config['charset']    = 'utf-8';
           $config['newline']    = "\r\n";
           $config['mailtype'] = 'html'; 
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!"; 
           $config['validation'] = TRUE;      
           $this->email->initialize($config);	   		  
		    		   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			
			if ($strponumber != '')
			 {				  	
				//Select untuk email master PP----------------------------
				//$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,gran_total,ppn,pph,type_purchase,gran_totalppn,flag_purchase,remarks from qv_head_pp_complite where id_master ='".$strponumber."'and status = '1'");				
				$parameter = array('id_master' => $strponumber);
				$query = http_request_get(API_URL.'/PRForm/getPP', $parameter);
				$query = json_decode($query);
				if ($query->length > 0 ) {
					$data_email['str_pp_master'] = $query->data;		
								   
					//$query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."'and status = '1'");		
					$query_detail = http_request_get(API_URL.'/PRForm/getDetailPP', $parameter);
					 $query_detail = json_decode($query_detail);
					if ($query_detail->length > 0 ) {										 
					   $data_email['str_pp_detail'] = $query_detail->data;	
					   $data_email['intno'] = ""; //untuk counter angka table	
					 		
					   $message = $this->load->view('approval_form/V_content_email_to_bod_fc',$data_email,true);
					 						
					} 
			   }
			}
					 
			$result = $this->email ;							   
			$this->email->initialize($config);  
			$this->email->set_newline("\r\n"); 
		   
		   				
			
			$sender_email = "helpdesk@eurokars.co.id"; 					   
			$sender_name = "Epurchasing Notification";					   					    
												   					    
									$result = $this->email ;							   
									$this->email->initialize($config);  
									$this->email->set_newline("\r\n"); 
																			
									//$to = 'roy.arfandy@mazda.co.id' ;																	   											 																	   
									$to = 'brian.yunanda@eurokars.co.id';
									$subject = 'submission Approval F.C';
									
									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);  																		
								  
									$this->email->message($message);// tanpa array	
																			 
									if ($this->email->send()) :		  	
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
									//	redirect(current_url());
									else:
										show_error($this->email->print_debugger());
									endif;												    						 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
																			
									
 }




function cath_data_fordatatables(){
	
	     $str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
		 $str_idcompany = $this->session->userdata('id_company'); 
		 $str_iddept = $this->session->userdata('id_dept'); 

		 $start_date = $_POST['start_date'];
		 $end_date = $_POST['end_date'];
		 $status_approve = $_POST['status_approve'];
		
		 $str_status_send = "1";
		 $str_status_approved = "1";

		 $gran_total = '50000000';
		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];

        //order short column
		$column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top");
		
<<<<<<< HEAD
		$param_count = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'str_flag_approval' => $str_flag_approval, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => 'date_send_pr', 'dir' => 'desc',  'is_date_search' => $_POST["is_date_search"], 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'approve_purchasing' => $_POST["status_approve"]);
		$param_count = http_request_get(API_URL.'/approvalPurchasing/countAllResult', $param_count);
		$param_count = json_decode($param_count, true);
		$total = $param_count['length'];
		
=======
		/*Menghitung total desa didalam database*/

		$parameter = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'gran_total' => $gran_total, 'str_flag_approval' => $str_flag_approval);
		$query = http_request_get(API_URL.'/approvalPurchase/countAllResult', $parameter);
		$query = json_decode($query, true);
		$total = $query['length'];
		//$total=$this->db->count_all_results("qv_vendor");
		//$total=$this->db->count_all_results("qv_head_pp_complite");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
>>>>>>> 9106b17a275c73b88ce7c7fedff87a416c3e3225
		$output=array();
		$output['draw']=$draw;	
		$output['recordsTotal']=$output['recordsFiltered']=$total;
		$output['data']=array();

		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/

		if($search!=""){
<<<<<<< HEAD
			$param_search = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => 'date_send_pr', 'dir' => 'desc',  'is_date_search' => $_POST["is_date_search"], 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'approve_head' => $_POST["status_approve"]);
			if  ($str_flag_approval == "3" or $str_flag_approval == "0" ){	
			     $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->like("id_master",$search);
				 $this->db->or_like("user_submission",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);				
				 $this->db->where("status","1");
				 $this->db->or_like("header_desc",$search);	
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->or_like("short",$search);	
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->or_like("vendor",$search);	
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("id_dept",$str_iddept);
				 $this->db->where("status","1");
			}else{
					if  ($str_flag_approval == "4"){	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );								
						 $this->db->where("status","1");
						 $this->db->like("id_master",$search);
						 $this->db->or_like("user_submission",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );		
						 $this->db->where("status","1");
						 $this->db->or_like("header_desc",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );		
						 $this->db->where("status","1");
						 $this->db->or_like("short",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );		
						 $this->db->where("status","1");
						 $this->db->or_like("vendor",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("aprove_head",$str_status_approved );		
						 $this->db->where("status","1");
				 }else{
					 if  ($str_flag_approval == "2"){	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_purchasing",$str_status_approved );	
						 $this->db->where("aprove_fc","0" );							
						 $this->db->where("status","1");
						 $this->db->like("id_master",$search);
						 $this->db->or_like("user_submission",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_purchasing",$str_status_approved );
						 $this->db->where("aprove_fc","0" );			
						 $this->db->where("status","1");
						 $this->db->or_like("header_desc",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_purchasing",$str_status_approved );	
						 $this->db->where("aprove_fc","0" );		
						 $this->db->where("status","1");
						 $this->db->or_like("short",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_purchasing",$str_status_approved );	
						 $this->db->where("aprove_fc","0" );	
						 $this->db->where("status","1");
						 $this->db->or_like("vendor",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_purchasing",$str_status_approved );	
						  $this->db->where("aprove_fc","0" );	
						 $this->db->where("status","1");
					 }else{	
					        if  ($str_flag_approval == "5"){	
								 $this->db->where("status_send_aprove",$str_status_send);
								 $this->db->where("approve_purchasing",$str_status_approved );	
								 $this->db->where("aprove_fc","0" );	
								 $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
					             $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));						
								 $this->db->where("status","1");
								 $this->db->like("id_master",$search);
								 $this->db->or_like("user_submission",$search);
								 $this->db->where("status_send_aprove",$str_status_send);
								 $this->db->where("approve_purchasing",$str_status_approved );	
								 $this->db->where("aprove_fc","0" );	
								 $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
					             $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
								 $this->db->where("status","1");
								 $this->db->or_like("header_desc",$search);	
								 $this->db->where("status_send_aprove",$str_status_send);
								 $this->db->where("approve_purchasing",$str_status_approved );	
								 $this->db->where("aprove_fc","0" );	
								 $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
					             $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
								 $this->db->where("status","1");
								 $this->db->or_like("short",$search);	
								 $this->db->where("status_send_aprove",$str_status_send);
								 $this->db->where("approve_purchasing",$str_status_approved );	
								 $this->db->where("aprove_fc","0" );	
								 $this->db->where_in('id_company', array("1", "2", "7", "8", "9"));	//ARE,EAU,KK,ATI,EGU
					             $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
								 $this->db->where("status","1");
								 $this->db->or_like("vendor",$search);	
								 $this->db->where("status_send_aprove",$str_status_send);
								 $this->db->where("approve_purchasing",$str_status_approved );	
								  $this->db->where("aprove_fc","0" );		
								 $this->db->where("status","1");
								
							 }else{	
					 
										 if  ($str_flag_approval == "6"){	
											 $this->db->where("status_send_aprove",$str_status_send);
											 $this->db->where("approve_purchasing",$str_status_approved );	
											 $this->db->where("aprove_fc","0" );	
											 $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
											 $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));								
											 $this->db->where("status","1");
											 $this->db->like("id_master",$search);
											 $this->db->or_like("user_submission",$search);
											 $this->db->where("status_send_aprove",$str_status_send);
											 $this->db->where("aprove_fc","0" );	
											 $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
											 $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
											 $this->db->where("status","1");
											 $this->db->or_like("header_desc",$search);	
											 $this->db->where("status_send_aprove",$str_status_send);
											 $this->db->where("aprove_fc","0" );	
											 $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
											 $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
											 $this->db->where("status","1");
											 $this->db->or_like("short",$search);	
											 $this->db->where("status_send_aprove",$str_status_send);
											 $this->db->where("approve_purchasing",$str_status_approved );
											 $this->db->where("aprove_fc","0" );	
											 $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
											 $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
											 $this->db->where("status","1");
											 $this->db->or_like("vendor",$search);	
											 $this->db->where("status_send_aprove",$str_status_send);
											 $this->db->where("approve_purchasing",$str_status_approved );
											 $this->db->where("aprove_fc","0" );	
											 $this->db->where_in('id_company', array("4","5",));	 //ETU TEI
											 $this->db->where_not_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));			
											 $this->db->where("status","1");
											
										 }else{		
												 if  ($str_flag_approval == "7"){	
													 $this->db->where("status_send_aprove",$str_status_send);
													 $this->db->where("approve_purchasing",$str_status_approved );	
													 $this->db->where("aprove_fc","0" );	
													 $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));							
													 $this->db->where("status","1");
													 $this->db->like("id_master",$search);
													 $this->db->or_like("user_submission",$search);
													 $this->db->where("status_send_aprove",$str_status_send);
													 $this->db->where("approve_purchasing",$str_status_approved );	
													 $this->db->where("aprove_fc","0" );	
													 $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
													 $this->db->where("status","1");
													 $this->db->or_like("header_desc",$search);	
													 $this->db->where("status_send_aprove",$str_status_send);
													 $this->db->where("approve_purchasing",$str_status_approved );
													 $this->db->where("aprove_fc","0" );	
													 $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
													 $this->db->where("status","1");
													 $this->db->or_like("short",$search);	
													 $this->db->where("status_send_aprove",$str_status_send);
													 $this->db->where("approve_purchasing",$str_status_approved );
													 $this->db->where("aprove_fc","0" );	
													 $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));		
													 $this->db->where("status","1");
													 $this->db->or_like("vendor",$search);	
													 $this->db->where("status_send_aprove",$str_status_send);
													 $this->db->where("approve_purchasing",$str_status_approved );
													 $this->db->where("aprove_fc","0" );		
													 $this->db->where_in('id_dept', array("1","5","10","14","15","16","17","18","19","28","29","33","34","35","36","45"));	
													 $this->db->where("status","1");
													
												 }else{		
												 	 if  ($str_flag_approval == "8"){	
														 $this->db->where("status_send_aprove",$str_status_send);
														 $this->db->where("approve_purchasing",$str_status_approved );
														 $this->db->where("aprove_fc","0" );	
														 $this->db->where_in('id_company', array("3","6","11"));								
														 $this->db->where("status","1");
														 $this->db->like("id_master",$search);
														 $this->db->or_like("user_submission",$search);
														 $this->db->where("status_send_aprove",$str_status_send);
														 $this->db->where("approve_purchasing",$str_status_approved );
														 $this->db->where("aprove_fc","0" );	
														 $this->db->where_in('id_company', array("3","6","11"));		
														 $this->db->where("status","1");
														 $this->db->or_like("header_desc",$search);	
														 $this->db->where("status_send_aprove",$str_status_send);
														 $this->db->where("approve_purchasing",$str_status_approved );
														 $this->db->where("aprove_fc","0" );	
														 $this->db->where_in('id_company', array("3","6","11"));		
														 $this->db->where("status","1");
														 $this->db->or_like("short",$search);	
														 $this->db->where("status_send_aprove",$str_status_send);
														 $this->db->where("approve_purchasing",$str_status_approved );
														 $this->db->where("aprove_fc","0" );	
														 $this->db->where_in('id_company', array("3","6","11"));		
														 $this->db->where("status","1");
														 $this->db->or_like("vendor",$search);	
														 $this->db->where("status_send_aprove",$str_status_send);
														 $this->db->where("approve_purchasing",$str_status_approved );
														 $this->db->where("aprove_fc","0" );	
														 $this->db->where_in('id_company', array("3","6","11"));		
														 $this->db->where("status","1");
														
													 }else{	

														if  ($str_flag_approval == "9"){	
															// Query for FD
															$this->db->where("status_send_aprove",$str_status_send);
															$this->db->where("approve_purchasing",$str_status_approved );	
															$this->db->where("aprove_fc","0" );						
															$this->db->where("status","1");
															$this->db->where("gran_totalppn <=", $gran_total);
															$this->db->like("id_master",$search);
															$this->db->or_like("user_submission",$search);
															$this->db->where("status_send_aprove",$str_status_send);
															$this->db->where("approve_purchasing",$str_status_approved );	
															$this->db->where("aprove_fc","0" );
															$this->db->where("status","1");
															$this->db->where("gran_totalppn <=", $gran_total);
															$this->db->or_like("header_desc",$search);	
															$this->db->where("status_send_aprove",$str_status_send);
															$this->db->where("approve_purchasing",$str_status_approved );	
															$this->db->where("aprove_fc","0" );
															$this->db->where("status","1");
															$this->db->where("gran_totalppn <=", $gran_total);
															$this->db->or_like("short",$search);	
															$this->db->where("status_send_aprove",$str_status_send);
															$this->db->where("approve_purchasing",$str_status_approved );	
															$this->db->where("aprove_fc","0" );	
															$this->db->where("status","1");
															$this->db->where("gran_totalppn <=", $gran_total);
															$this->db->or_like("vendor",$search);	
															$this->db->where("status_send_aprove",$str_status_send);
															$this->db->where("approve_purchasing",$str_status_approved );	
															$this->db->where("aprove_fc","0" );	
															$this->db->where("status","1");
															$this->db->where("gran_totalppn <=", $gran_total);
														   
														}else{
															
															if  ($str_flag_approval == "1"){	
																// Query for FD
																$this->db->where("status_send_aprove",$str_status_send);
																$this->db->where("approve_purchasing",$str_status_approved );	
																$this->db->where("aprove_fc","0" );						
																$this->db->where("status","1");
																$this->db->where("gran_totalppn >=", $gran_total);
																$this->db->like("id_master",$search);
																$this->db->or_like("user_submission",$search);
																$this->db->where("status_send_aprove",$str_status_send);
																$this->db->where("approve_purchasing",$str_status_approved );	
																$this->db->where("aprove_fc","0" );
																$this->db->where("status","1");
																$this->db->where("gran_totalppn >=", $gran_total);
																$this->db->or_like("header_desc",$search);	
																$this->db->where("status_send_aprove",$str_status_send);
																$this->db->where("approve_purchasing",$str_status_approved );	
																$this->db->where("aprove_fc","0" );
																$this->db->where("status","1");
																$this->db->where("gran_totalppn >=", $gran_total);
																$this->db->or_like("short",$search);	
																$this->db->where("status_send_aprove",$str_status_send);
																$this->db->where("approve_purchasing",$str_status_approved );	
																$this->db->where("aprove_fc","0" );	
																$this->db->where("status","1");
																$this->db->where("gran_totalppn >=", $gran_total);
																$this->db->or_like("vendor",$search);	
																$this->db->where("status_send_aprove",$str_status_send);
																$this->db->where("approve_purchasing",$str_status_approved );	
																$this->db->where("aprove_fc","0" );	
																$this->db->where("status","1");
																$this->db->where("gran_totalppn >=", $gran_total);
															   
															}else{
																	
															 $this->db->where("status_send_aprove",$str_status_send);
															 $this->db->where("aprove_fc",$str_status_approved );								
															 $this->db->where("status","1");
															 $this->db->like("id_master",$search);
															 $this->db->or_like("user_submission",$search);
															 $this->db->where("status_send_aprove",$str_status_send);
															 $this->db->where("aprove_fc",$str_status_approved );		
															 $this->db->where("status","1");
															 $this->db->or_like("header_desc",$search);	
															 $this->db->where("status_send_aprove",$str_status_send);
															 $this->db->where("aprove_fc",$str_status_approved );		
															 $this->db->where("status","1");
															 $this->db->or_like("short",$search);	
															 $this->db->where("status_send_aprove",$str_status_send);
															 $this->db->where("aprove_fc",$str_status_approved );		
															 $this->db->where("status","1");
															 $this->db->or_like("vendor",$search);	
															 $this->db->where("status_send_aprove",$str_status_send);
															 $this->db->where("aprove_fc",$str_status_approved );		
															 $this->db->where("status","1");
															}
														}												 
													 }
												 }
										  }
							        }
					       }
				 }
			}
=======
			$param_search = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => 'date_send_pr', 'dir' => 'desc',  'is_date_search' => $_POST["is_date_search"], 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"],  'gran_total' => $gran_total, 'flag_approval' => $str_flag_approval);
			$param_search = http_request_get(API_URL.'/approvalPR/datatableSearch', $param_search);
			// balikan si API
			$param_search = json_decode($param_search, true);
			$output['recordsTotal']=$output['recordsFiltered']=$param_search['length'];
>>>>>>> 9106b17a275c73b88ce7c7fedff87a416c3e3225
		}

		if($_POST["is_date_search"] == "yes") {
			$parameter = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'is_date_search' => 'yes', 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'gran_total' => $gran_total, 'flag_approval' => $str_flag_approval);
		} else {
			$parameter = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => 'date_send_pr', 'dir' => 'desc',  'is_date_search' => $_POST["is_date_search"], 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'gran_total' => $gran_total, 'flag_approval' => $str_flag_approval);
		}
	
		// proses paling awal saat load, dan saat user ada klik pada colum di table nya
		if (isset($_REQUEST["order"])){
			$parameter = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => $column_order[$_REQUEST["order"][0]["column"]], 'dir' => $_REQUEST["order"][0]['dir'],  'is_date_search' => $_POST["is_date_search"], 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'gran_total' => $gran_total, 'flag_approval' => $str_flag_approval);
		}else{
			$parameter = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => 'date_send_pr', 'dir' => 'desc',  'is_date_search' => $_POST["is_date_search"], 'start_date' => $_POST["start_date"], 'end_date' => $_POST["end_date"], 'gran_total' => $gran_total, 'flag_approval' => $str_flag_approval);
		}
	
			$query = http_request_get(API_URL.'/approvalPurchase/datatableSearch', $parameter);
			$query = json_decode($query, true);


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		
		foreach ($query['data'] as $row_tbl) {
			
			 if ($row_tbl['status_send_aprove'] == "-1")	
			 {
				   $info_aproval = "Reject";
			 }else{				
				   $info_aproval = "New Crated";				
			 }
			 
			 If ($row_tbl['attach_quo'] != "")
			  {  
			   $attach_quo = '<div  align="center"><a onClick="PDFPopup(this)" req_id='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
			 }else{
				$attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
			 }
		
		   
		   
		     $btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i></a></center>';//button detail
		   
		    If ($row_tbl['aprove_head'] == "1")
			 {
				$head_approval = "<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";
				
			 }else{
				  If ($row_tbl['status_send_aprove'] == "-1")
				  {
					 $head_approval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
				  }else{
					 $head_approval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
				  }
			 }
			 
			  If ($row_tbl['approve_purchasing'] == "1")
			  {
					//$purchase_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				    $purchase_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
				 }else{
					  If ($row_tbl['status_send_aprove'] == "-1")
					  {
						$purchase_approval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
					  }else{
						$purchase_approval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
					  }
			   }
			   
			   If ($row_tbl['aprove_fc'] == "1")
			  {
					//$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
					$fc_aproval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
				 }else{
					  If ($row_tbl['status_send_aprove'] == "-1")
					  {
						$fc_aproval =  "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
					  }else{
						$fc_aproval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
					  }
			   }
			   
				
			   
 				 If ($row_tbl['aprove_bod'] == "1")
				 {
						//$bod_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
						 $bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
					 }else{
						  If ($row_tbl['status_send_aprove'] == "-1")
						  {
							//$bod_approval =  '<img src="'.base_url("asset/images/reject.png").'">' ;
						  	  $bod_approval = "<center><span class='btn btn-info glyphicon glyphicon-remove btn-xs' aria-hidden='true'></span></center>";
						  }else{
							$bod_approval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
						  }
				  }
				 
			
 $strbodaprove  = $row_tbl["aprove_bod"] ;
 $strpurchaseaprove = $row_tbl["approve_purchasing"] ;
 $strfcaprove   = $row_tbl["aprove_fc"] ;
 $strheadaprove = $row_tbl["aprove_head"] ;	
 

 if ($this->session->userdata("aproval_flag")== "3" and $row_tbl["aprove_head"]=="1" ) {			
	  $str_reject ='<div align="center" style="color:#2292d8"> Has been aproved head</div>' ;	
	  $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";		
 }else{
   if ($this->session->userdata("aproval_flag")== "4" and $row_tbl["approve_purchasing"]=="1" ) {	
	   $str_reject =  '<div align="center" style="color:#2292d8>Has been Aproved Purchasing</div>' ;
	    $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";				
   }else{
	 if ($this->session->userdata("aproval_flag")== "2" and $row_tbl["aprove_fc"]=="1" ) {	
		  $str_reject =  '<div align="center" style="color:#2292d8>Has been aproved fc</div>' ;	
		   $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";			
 	 }else{	
		  if ($this->session->userdata("aproval_flag")== "5" and $row_tbl["aprove_fc"]=="1" ) {	
			  $str_reject =  '<div align="center" style="color:#2292d8>Has been aproved fc</div>' ;		
			   $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";		
		 }else{	
		     if ($this->session->userdata("aproval_flag")== "6" and $row_tbl["aprove_fc"]=="1" ) {	
			     $str_reject =  '<div align="center" style="color:#2292d8>Has been aproved fc</div>' ;
				  $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";				
			 }else{	
				  if ($this->session->userdata("aproval_flag")== "7" and $row_tbl["aprove_fc"]=="1" ) {	
					  $str_reject =  '<div align="center" style="color:#2292d8>Has been aproved fc</div>' ;
					   $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";	
				 }else{	
					if ($this->session->userdata("aproval_flag") == "1" and $row_tbl["aprove_bod"] ) {
						 $str_reject =  '<div align="center" style="color:#2292d8>Has been aproved all</div>' ;
						  $chk_idmaster ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true' ></span></center>";	
					}else{ 	 
						  $str_reject =  '<div align="center"><a href="#" class="btn btn-app btn-danger btn-xs radius-8 reject " id="reject" style="text-decoration:none" req_id='.$row_tbl["id_master"].'><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span></a></div>'; //button reject 	
						  
					 //checkbox aktiff belum teraproval.
			         $chk_idmaster ='<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value='.$row_tbl["id_master"].' class="ace" req_id_del='.$row_tbl["id_master"].' />
           <span class="lbl"></span> ';				
					}
				 }
			 }
		  }
	   }
     }
  }
  
  //Pembedaan Warna pada type purchase.
		   if ($row_tbl['flag_purchase'] == "1") {
		       $type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
		   }else{
			     if ($row_tbl['flag_purchase'] == "2") {
		     		  $type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
		  		 }else{
			   		  if ($row_tbl['flag_purchase'] == "3") {
		     			  $type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
		  			 }else{
			    			 if ($row_tbl['flag_purchase'] == "4") {
		      					 $type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
		  					 }else{
		       					 $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
							 };
					 };
				 };   
		     };
//end------------------------------			 

//date--------------
if ($row_tbl['date_aprove_head'] ==""){
   $datehead = "" ;
}else{
   $datehead  = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_head']))."</div>";
}

if ($row_tbl['date_approve_purchasing'] ==""){
   $datepur = "" ;
}else{
   $datepur = "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_approve_purchasing']))."</div>";
}

if ($row_tbl['date_aprove_fc'] ==""){
    $datefc = "" ;
}else{
    $datefc= "<div style='color:#EB293D' align='center'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_fc']))."</div>";
}

if ($row_tbl['date_aprove_bod'] ==""){
   $datebod = "" ;
}else{
   $datebod = "<div style='color:#EB293D'>".date('d-m-Y H:i:s', strtotime($row_tbl['date_aprove_bod']))."</div>";
}

//end date-----------------

//label colum 1 idepp
$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP
//end label

$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";


if($row_tbl['term_top'] > 1) {
	$day = 'Days';
} else {
	$day = 'Day';
}	 
			$output['data'][]=array(
			                        $chk_idmaster,
									//$row_tbl['id_master'],
									$idmas,
									$btn_view_detail,
									$attach_quo,															
									$str_reject	,
									$row_tbl['short'],
									$row_tbl['user_submission'],
									$row_tbl['dept'],
									$header_desc ,//$row_tbl['header_desc'],
									$row_tbl['vendor'],																								
									date('d-m-Y', strtotime($row_tbl['date_pp'])),	
									$row_tbl['currency'],
									$type_purchase, //$row_tbl['type_purchase'],
									//$row_tbl['nomor_coa'],
									number_format($row_tbl['gran_total'],2,'.',','),
									//number_format($row_tbl['ppn'],2,'.',','),
									number_format($row_tbl['gran_totalppn'],2,'.',','),
									$row_tbl['term_top'].' '.$day,
									$head_approval.$datehead,
									$purchase_approval.$datepur,
									$fc_aproval.$datefc,
									$bod_approval.$datebod,	
																	
							  );			
		}
		echo json_encode($output);
	}	
	
	public function getUserDeligate() {
		$id_company = $this->session->userdata('id_company');
		$data = $this->M_table_aproval->getAllUserDeligate($id_company);

		echo '<form method="post" action="'.base_url('approval_form/c_table_aproval/updateUserDeligate').'"> 
		<div class="row">
					<div class="col-sm-5 col-sm-offset-4">
						<div class="form-group">
							
							<label class="control-label" style="color:#900">User Deligate *</label>';
							echo '<select class="form-control select2" name="user_deligate" required>';
							
							echo '<option value=""> - Choose User Deligate - </option>';

							
							foreach($data as $val) {
								if($val->status == '1') {
									$deligated = 'Deligated Active';
								} else {
									$deligated = 'Deligated Non-Active';
								}
	
								echo '<option value="'.$val->id.'">'.$val->username.' | '.$deligated.'</option>';
							}
							
							echo '
							</select>
		
						
						</div>
					</div>
		
				</div>
				<div class="row">
					<div class="col-sm-5 col-sm-offset-4">
						<div class="form-group">
							<button type="submit" class="btn btn-success" name="submit_deligate">Submit</button>
						</div> 
					</div> 
				</div>
		</form>
		
		
		';
	}
	
	public function updateUserDeligate() {
		$id = $this->input->post('user_deligate');
		$aproval_flag = $this->session->userdata('aproval_flag');
		$id_company = $this->session->userdata('id_company');

		$cek_data = $this->M_table_aproval->getUserDeligate($id, $id_company);
		
		$data_request = $this->M_table_aproval->getEmailDeligate($id);

		$kirim_email = $this->kirim_email_deligate('Deligated', $data_request[0]);

		if(count($cek_data) > 0) {
			$data = array('status' => 0,
						'aproval_flag' => '10',
						'id' => $id
					);
		} else {
			$data = array('status' => 1,
						'aproval_flag' => $aproval_flag,
						'id' => $id
					);
		}

		if($kirim_email == 'ok') {
			$update = $this->M_table_aproval->updateUserDeligate($id, $data);

			if($update) {
				$this->session->set_flashdata('msg', '<div class="alert alert-success">User Deligate Berhasil</div>');
			} else {
				$this->session->set_flashdata('msg', '<div class="alert alert-danger">User Deligate Gagal</div>');
			}

		} else {
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">'.$kirim_email.'</div>');
		}

		redirect(base_url('approval_form/c_table_aproval'));
		
	}

	public function tes_email() {
		$config = array('protocol' => 'smtp',
							'smtp_host' => 'ssl://smtp.googlemail.com',
							'smtp_port' => 465,
							'smtp_user' => '3m1mazda@gmail.com',
							'smtp_pass' => '3mim@zd4!',
							'mailtype' => 'text',
							'charset' => 'iso-8859-1',
							'wordwrap' => TRUE

				);

		$message = 'Hello World';
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('3m1mazda@gmail.com');
		$this->email->to('Bernand.Hermawan@eurokars.co.id', 'bernandotorrez4@gmail.com');
		$this->email->subject('testing');
		$this->email->message($message);

		if($this->email->send()){

            echo 'ok';

        } else {
            echo '<pre>';print_r($this->email->print_debugger());
            //print_r($this->email->print_debugger());
        }
	}


	public function kirim_email_deligate($status, $data_request){       
		// Tes

		$this->load->library('email');
		//konfigurasi email
		$config = array();
		$config['charset'] = "utf-8";
		$config['useragent'] = "I.T";
		$config['protocol']= "smtp";
		$config['mailtype']= "html";
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		//------google
		//$config['smtp_host']    = 'ssl://smtp.googlemail.com';

		//-----------exchange
		$config['smtp_host']  = 'webmail.eurokars.co.id' ;
		//$config['smtp_port']  = '587';
		$config['smtp_port']  = '5220';
		//$config['smtp_port']  = '465'; //port gmail
		$config['newline']    = "\r\n";
		$config['mailtype']   ="html" ;
		//$config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		//$config['smtp_pass']= "3mim@zd4!";
		$config['smtp_user']= "helpdesk@eurokars.co.id";
		$config['smtp_pass']= "#ur0kar5";

		$this->email->initialize($config);
			$this->email->set_newline("\r\n");

		    //Exchange
			//konfigurasi pengiriman

			$sender_email = "helpdesk@eurokars.co.id";
			$sender_name = "Deligate";

			//GMAIL-----
			//$sender_email = "epurchaseeuro@gmail.com";
			//$sender_name = "Epurchasing Eurokarsgroup";

		   $user_email = $data_request->email;
		   $user_email_fc = $this->session->userdata('email');
		   //$user_numpang = 'Bernand.Hermawan@eurokars.co.id';
		   //end-----------------------------------------
   
		   	$to = $user_email.', '.$user_email_fc;

			$subject = "Your now Deligated";
			$this->email->from($sender_email, $sender_name);
			$this->email->to($to);
			$this->email->subject($subject);

			//$this->email->message(print_r($message, true));


			$message = "Your now Deligated";
			$email_body = $this->load->view('approval_form/V_email_deligated',$data,true);
			$this->email->message($message);// tanpa array
			
			if($this->email->send()){
	
				return 'ok';
		
			} else {
				return $this->email->print_debugger();
				//print_r($this->email->print_debugger());
			}


    }
	 		
}

	
