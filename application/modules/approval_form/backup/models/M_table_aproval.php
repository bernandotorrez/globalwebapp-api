<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_table_aproval extends CI_Model {	
				    
    public $db_tabel = 'qv_head_pp_complite';
	
	
	
	function give_aproval()						  		
	{
	     $strid_master =$this->input->post('msg'); 			  		 	
		 $flag_aproved = "1" ; // flag 1 aproved
		 
			 if ($this->session->userdata('aproval_flag')=="3") {			 
				  $buff_aproval ="aprove_head";
				  $buff_date ="date_aprove_head";
			 }else{
				   if ($this->session->userdata('aproval_flag')=="4") {					   
					   $buff_aproval ="aprove_bod"; 
					   $buff_date ="date_aprove_bod";
					}else{   
					    if ($this->session->userdata('aproval_flag')=="2") {					   					   
					   	    $buff_aproval ="aprove_fc"; //automatic fc field approved
					      	$buff_date ="date_aprove_fc";
				   		}else{
						   if ($this->session->userdata('aproval_flag')=="1") {					   
							  $buff_aproval ="aprove_presdir"; 
							  $buff_date ="date_aprove_presdir";
							  $buff_time ="time_approved";
						   }else{
							   $buff_aproval="";
						   }
						}
				   }
			 }
			  			  
			  if (isset($buff_aproval) && trim($buff_aproval!=''))
			  {						  				  
				  for ($i=0; $i < count($strid_master) ; $i++) { 										
					
				 if ($this->session->userdata('aproval_flag')=="4") : //jika flag approval adalah purchasing
					   $data = array($buff_aproval=>$flag_aproved,			  
									  $buff_date =>date('Y-m-d H:i:s'), 	
									  'date_aproval' =>date('Y-m-d H:i:s')
									  );						
					
					else:
					    if ($this->session->userdata('aproval_flag')=="2") : //jika flag approval adalah fc
						   $data = array($buff_aproval=>$flag_aproved,
										  $buff_date =>date('Y-m-d H:i:s'), 	
										  'date_aproval' =>date('Y-m-d H:i:s')										  
										  );	
						
						else:
							if ($this->session->userdata('aproval_flag')=="1") : //jika flag approval adalah BOD
							   $data = array($buff_aproval=>$flag_aproved,
											  $buff_date =>date('Y-m-d H:i:s'), 	
											  'date_aproval' =>date('Y-m-d H:i:s'),
											  'time_approved'=>date('H:i:s')
											  );	
							
							else:
								$data = array($buff_aproval=>$flag_aproved,
											  $buff_date =>date('Y-m-d H:i:s'), 	
											  'date_aproval' =>date('Y-m-d H:i:s')
											  );	
							endif;	
						endif;				  																																	
					endif;
									  
					$this->db->where('id_master',$strid_master[$i]);
					$this->db->update('tbl_master_pp',$data); 
					
					$buff_ses_idmaster = array('ses_idmaster'=>$strid_master[$i]);
				    $this->session->set_userdata($buff_ses_idmaster);				
					
					$value[] = $strid_master[$i] ;	//memasukan beberapa id master yg di select ke dalam array.				
					$this->session->set_userdata('result_id_master',$value); //simpan session array					
					//print_r($buff_ses_idmaster); echo array													
				  }				 				  				 				  				
				  return true;	
			  }else{
				  return false;
			  }
	  
		  
	}
	
	function give_aproval_bod()	//melalui modal poup					  		
	{
		$idpp 	   = $this->input->post('txtppnoappro');
		$remrakapp = $this->input->post('txtappro');
		$flag_aproved = "1" ; // flag 1 aproved
		
		$data = array('aprove_presdir'=>$flag_aproved,
					  'date_aprove_presdir' =>date('Y-m-d h:i:sa'), 	
					  'date_aproval' =>date('Y-m-d h:i:sa'),
					  'time_approved'=>date("h:i:sa"),
					  'remark_aprov' =>$remrakapp
					  );	
		$this->db->where('id_master',$idpp);
		$this->db->update('tbl_master_pp',$data); 
		
		$buff_ses_idmaster = array('ses_idmaster'=>$idpp);
	    $this->session->set_userdata($buff_ses_idmaster);	
	}
	
	
	
	function give_rejected()						  		
	{	     		  		 	
		 $strid_master = $this->input->post('txtppno');
		 $str_name_reject = "By"." ".$this->session->userdata('name')." " . ":";
		 $strid_reason_reject =  $str_name_reject." ".$this->input->post('txtreject');
		 
		 		 
		 $flag_reject = "-1" ; // flag 1 flag_reject
		 $buff_status_reject = "status_send_aprove";
		 $buff_reason_reject = "reason_reject";
		 
		 			
			  if (isset($buff_status_reject) && trim($buff_status_reject!=''))
			  {		
						
					$query = $this->db->select("id_master") //check id booking pada table_parent 								
										->where('id_master',$strid_master)
										->get('tbl_master_pp');
					  if ($query->num_rows() == 1)
					  {		
					  
						 if ($this->session->userdata('aproval_flag')=="3") {			 	
							  $data = array($buff_status_reject=>$flag_reject,
						              $buff_reason_reject=>$strid_reason_reject,
									  "aprove_head" => "0"
									  );	
						 }else{
							   if ($this->session->userdata('aproval_flag')=="4") {	
							        $data = array($buff_status_reject=>$flag_reject,
										  $buff_reason_reject=>$strid_reason_reject,
										  "aprove_head" => "0",
										  "aprove_bod" => "0"				   				
						                  );
							   }else{
									if ($this->session->userdata('aproval_flag')=="2") {					   
										  $data = array($buff_status_reject=>$flag_reject,
										  $buff_reason_reject=>$strid_reason_reject,
										  "aprove_head" => "0",
										  "aprove_bod" => "0",
										  "aprove_fc" => "0"
										  );	
									}else{
										   if ($this->session->userdata('aproval_flag')=="1") {	
										   
										      $data = array($buff_status_reject=>$flag_reject,
													  $buff_reason_reject=>$strid_reason_reject,
													  "aprove_head" => "0",
													  "aprove_bod" => "0",
													  "aprove_fc" => "0",
													  "aprove_presdir" => "0"
													  );					   
										    }else{
											   $buff_aproval="";
											  // $flag_back_aprove = "" ;
										    }
									}
							   }
						 }		
						
																																													
						$this->db->where('id_master',$strid_master);
						$this->db->update('tbl_master_pp',$data); 
						
						$buff_ses_idmaster = array('ses_idmaster'=>$strid_master);
						$this->session->set_userdata($buff_ses_idmaster);
						
						return true;
					  }
				 // }				  
			  }else{
				  return false;
			  }
	  		  
	}
	
	function get_company()
	{		
	     $this->load->database();		
		 $this->db->select('short');				 
		 $result = $this->db->get('tbl_company');			     		 
		 return $result;	 	
	}
	
	function get_dept()
	{		
	     $this->load->database();		
		 $this->db->select('dept');				 
		 $result = $this->db->get('tbl_dept');			     		 
		 return $result;	 	
	}
	
	function get_search()
	{
		
	 $strdatestart =$this->input->post('txtdatestart');	
	 $strdatend =$this->input->post('txtdateend');	
	 $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	 $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
	 	 
	 $strcompany = $this->input->post('cbostatus');
	 $strdept = $this->input->post('cbodept');	 
	 $strcari = $this->input->post('txtcari');
	 
	 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
	 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		
	 
	 $flag_head_aprove ="1";
	 $flag_head	=  "0"; //jika  0 sudah di aprove head dept 
	 
	 $flag_purchase_stat_aprove	=  "1"; //jika  1 sudah di aprove head dept 
	 $flag_purchase_stat	=  "0"; //jika  0 belum di aprove fc di maka tampilkan 	
	 $flag_bod	=  "0"; //jika  0 belum di aprove bod 	 				
     $str_flag_purchase = "1"; //jika untuk flag unit	
	 $flag_fc_stat_aprove = "1";
	 			
	 $this->load->database();		
	 $this->db->select('*');
	 
	 if ($this->session->userdata('aproval_flag') =="3" or $this->session->userdata('aproval_flag') =="0" ){	   		
		  $this->db->where('flag_purchase',$str_flag_purchase);		
		  $this->db->where('status_send_aprove',$send_aprove);
		  $this->db->where('status',$status_delete); 							 										 	         }else{
		   if ($this->session->userdata('aproval_flag') =="4"){	 
		       $this->db->where('flag_purchase',$str_flag_purchase);		  	  
		       $this->db->where('aprove_head',$flag_head_aprove);   //flag aprove FC	
			   
		       $this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove FC
			   $this->db->where('aprove_bod',$flag_bod);
			   $this->db->where('status_send_aprove',$send_aprove);
			   $this->db->where('status',$status_delete);				   
			   if($strcompany !='ALL'){$this->db->where('short',$strcompany);}							 				 				
			   if($strdept !='ALL'){ $this->db->where('dept',$strdept);}	 						  				 				             
			   if($strdatestart !="" and $strdatend !="" ){	
					$this->db->where('date_send_aproval BETWEEN "'.$datestart.'" and "'.$dateend.'"');}	 	 																	
			   
			   if($strcari !=""){ $this->db->where("id_master",$strcari);} 			  						   
			   //-----------------Or Where---------------------------------			   			   
			   // $this->db->or_where('aprove_fc',$flag_aprove_fc);   //flag aprove FC			
			   $this->db->where('aprove_bod',$flag_bod);
			   $this->db->where('status_send_aprove',$send_aprove);
			   $this->db->where('status',$status_delete);
		   }else{
			  // $this->db->where('flag_purchase',$str_flag_purchase);		 
			   $this->db->where('aprove_head',$flag_head_aprove);   //flag aprove FC			     			  			
			   $this->db->where('approve_purchasing',$flag_purchase_stat_aprove);   //flag aprove FC
			   $this->db->where('aprove_fc',$flag_fc_stat_aprove);   //flag aprove FC			
			   $this->db->where('aprove_bod',$flag_bod);
			   $this->db->where('status_send_aprove',$send_aprove);
			   $this->db->where('status',$status_delete);			   
		   }
	 }
					 		  	     			  	 
	 if($strcompany !='ALL'){	
	   $this->db->where('short',$strcompany);	
	 }	 
	 
	 if($strdept !='ALL'){	
	   $this->db->where('dept',$strdept);	
	 }	 
	 
	 if($strdatestart !="" and $strdatend !="" ){	
	  	$this->db->where('date_send_aproval BETWEEN "'.$datestart.'" and "'.$dateend.'"');		 			  		   					
	 }	 	 
	 				  		   							
	 if($strcari !=""){
		 $this->db->where("id_master",$strcari);			 					 	 
	 } 
		 	
	 	 	 		 	   
	 $this->db->order_by('date_send_aproval','desc');
	 $result = $this->db->get('qv_head_pp_complite');		
	 return $result;
	 
	}
	
	public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}
				    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}