<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_coa extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_coa');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_coa->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}	
	 	}
		
	public function index()
		{
		$data['coa']=$this->M_coa->get_all_coa();
		$data['company']=$this->M_coa->get_all_company();
		$data['department']=$this->M_coa->get_all_dept();
		$data['total']=$this->M_coa->get_count_id();
		$data['show_view'] = 'coa_form/V_coa';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function ajax_list()
	{
		$list = $this->M_coa->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_coa) {
			$chk_idmaster ='<div align="center"><input id="checkcoa" name="checkcoa" type="checkbox" value='.$tbl_coa->id_coa.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_coa->id_coa;
			$row[] = $tbl_coa->nomor_coa;
			$row[] = $tbl_coa->desc_coa;
			$row[] = $tbl_coa->company;
			$row[] = $tbl_coa->dept;
			$row[] = $tbl_coa->date_create;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_coa->count_all(),
						"recordsFiltered" => $this->M_coa->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	
	public function add_coa()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_add = date('Y-m-d H:i:s');

		if(empty($_POST["nomor_coa"])){
			die ("Field Nomor COA must be filled in!! ");
		}
		elseif(empty($_POST["desc_coa"])){
			die ("Field Description must be filled in!! ");
		}
		elseif(empty($_POST["id_company"])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_POST["id_dept"])){
			die ("Field Department must be filled in!! ");
		}
		else{
			$data = array(
					'id_coa' => $this->input->post('id_coa'),
					'nomor_coa' => $this->input->post('nomor_coa'),
					'desc_coa' => $this->input->post('desc_coa'),
					'id_company' => $this->input->post('id_company'),
					'id_dept' => $this->input->post('id_dept'),
					'status' => '1',
					'date_create' => $date_add,
					'remarks' => $remarks_add
				);
			$insert = $this->M_coa->add_coa($data);
		  }
		  echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_coa->get_by_id($id);
			echo json_encode($data);
		}

	public function update_coa()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_update = date('Y-m-d H:i:s');

		if(empty($_POST["nomor_coa"])){
			die ("Field Nomor COA must be filled in!! ");
		}
		elseif(empty($_POST["desc_coa"])){
			die ("Field Description must be filled in!! ");
		}
		elseif(empty($_POST["id_company"])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_POST["id_dept"])){
			die ("Field Department must be filled in!! ");
		}
		else{
			$data = array(
					'id_coa' => $this->input->post('id_coa'),
					'nomor_coa' => $this->input->post('nomor_coa'),
					'desc_coa' => $this->input->post('desc_coa'),
					'id_company' => $this->input->post('id_company'),
					'id_dept' => $this->input->post('id_dept'),
					'status' => '1',
					'date_update' => $date_update,
					'remarks' => $remarks_update
				);
		$this->M_coa->update_coa(array('id_coa' => $this->input->post('id_coa')), $data);
		  }
		   echo 'Insert';
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_delete = 'Action : Delete | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_delete = date('Y-m-d H:i:s');

		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
						'date_update' => $date_delete,
						'remarks' => $remarks_delete
				);	
				$this->db->where('id_coa', $id);
				$this->db->update('tbl_coa', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}



}
