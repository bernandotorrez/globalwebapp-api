<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<div>
		<button onclick="add_coa()" class="btn btn-app btn-primary btn-xs radius-4 caddnew" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
        Add
		</button>
		<button onclick="delete_coa()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit" id="deleteTriger" name="deleteTriger"  value ="btndel"  disabled="disabled" >
      <i class="ace-icon fa fa-ban bigger-160"></i>
        Delete
		</button>
		<button onclick="edit_coa()" class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="editTriger"  name="editTriger"  value ="btnedit"  disabled="disabled" >
      <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
        Edit
		</button> 
</div>
<br/>
<div class="table-header btn-info"> <?php echo " ".$header ;?>   </div>  
	<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">  

  <div class="form-group col-xs-6">
		<div class="col-xs-10">
			<div class="form-inline ">
				<div class="form-group">Date start</div>

				<div class="form-group">
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>

						<input type="text" class="form-control input-daterange" id="start_date" name="start_date"
							data-date-format="dd-mm-yyyy" autocomplete="off">
					</div> <!-- /.input-group datep -->
				</div>

				<div class="form-group">
					<label for="From" class="col-xs-1 control-label">To</label>
				</div>

				<div class="form-group">
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="text" class="form-control input-daterange" id="end_date" name="end_date"
							data-date-format="dd-mm-yyyy" autocomplete="off">
					</div>
				</div>

			</div>
		</div>
	</div>

		<table id="myTable" cellpadding="0" cellspacing="0" width="100%"  class="table table-striped table-bordered table-hover">
			<thead class="text-warning" > 
				<th width="5%" style="text-align:center">
					<label class="pos-rel">
            <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
            <span class="lbl"></span>
          </label>
        </th>
				<th>No</th>
	      <th>Id</th>
	      <th>Nomor COA</th>
				<th>Description</th>
				<th>Company</th>
				<th>Department</th>
        <th>Date Create</th>
	    </thead>
     </table> 
  </div>      
                                 	                       
<!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">COA Form</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
        <div class="widget-body">
				  <div class="widget-main">
          <input type="hidden" value="" name="id_coa"/>
          <div>
						<label for="id_coa">ID COA</label>
						<?php
							$totaldata = $total[0]->totaldata+1;
						?>
            <input class="form-control" id="id_coa" name="id_coa" type="text" readonly value="<?php echo $totaldata ?>"/>
					</div>
          <div>
              <label for="nomor_coa">Nomor COA</label>
              <input name="nomor_coa" placeholder="123456" class="form-control" id="nomor_coa" type="text"/>
					</div>
          <div>
              <label for="desc_coa">Description</label>
              <input name="desc_coa" placeholder="Insurance" class="form-control" id="desc_coa" type="text"/>
					</div>
          <div>
              <label for="form-field-select-3">Company</label>
			    		<select class="myselect form-control" name="id_company" id="id_company" tabindex="2" style="width: 100%;">
                <option  value="">Choose Company</option> 
										<?php foreach($company as $aaa) { ?>
                <option value="<?php echo $aaa->id_company;?>"><?php echo $aaa->company;?>
								</option>
										<?php } ?>
							</select>			
        	</div>
          <div>
              <label for="form-field-select-3">Department</label>
			    		<select class="myselect form-control" name="id_dept" id="id_dept" tabindex="2" style="width: 100%;">
                <option  value="">Choose Department</option> 
										<?php foreach($department as $bbb) { ?>
                <option value="<?php echo $bbb->id_dept;?>"><?php echo $bbb->dept;?>
								</option>
										<?php } ?>
							</select>			
        	</div>
        </div>
      </div>              
			</form>
      </div>
          <div class="modal-footer">
            <button onclick="save()" id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
              <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                Save
            </button>
            <button class="btn btn-app btn-danger btn-xs radius-4" type="submit" data-dismiss="modal">
              <i class="ace-icon fa fa-close bigger-160"></i>
                Cancel
            </button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->


<script type="text/javascript">
  $(".myselect").select2();
</script>
<script>
//$('#myTable').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
// var tableUsers = $('#myTable').DataTable({
// 		"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
// 		"dom": 'lfBrtip',
// 		"buttons": [
//             {
// 		   "extend":    'copy',
// 		   "text":      '<i class="fa fa-files-o"></i> Copy',
// 		   "titleAttr": 'Copy'
// 		   },
// 		   {
// 		   "extend":    'print',
// 		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
// 		   "titleAttr": 'Print',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   },
// 		   {
// 		   "extend":    'excel',
// 		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
// 		   "titleAttr": 'Excel'
// 		   },
// 		   {
// 		   "extend":    'pdf',
// 		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
// 		   "titleAttr": 'PDF',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   }
//         ],
//         "autoWidth" : false,
// 		"scrollY" : '250',
// 		"scrollX" : true,
//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.
//         "order": [], //Initial no order.

//         // Load data for the table's content from an Ajax source
//         "ajax": {
//             "url": "<?php echo base_url('coa_form/C_coa/ajax_list') ?>",
//             "type": "POST"
//         },
// 		'columnDefs': [
// 		{
//         'targets': [0],
// 		'orderable': false,
// 		}
// 		],

//       'order': [[2, 'desc']],
//         });

/*var tableUsers = $('#myTable').DataTable({
            searching:true,
			width:'100%',
  		    responsive : true,
            "sPaginationType": "full_numbers",
            "bPaginate": true,			
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true,
            "bProcessing": true
			
        });	*/	
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>

<!-- Datatable -->
<script>
$(document).ready(function(){ 
		$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data('no'); //
	
		function fetch_data(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
				"ajax":{
				    url: "<?php echo base_url('coa_form/C_coa/ajax_list') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
				 
				 'columnDefs': [
							{
							'targets': [0],
							'orderable': false,
							}
					],

						'order': [[2, 'desc']],
			 });
		  }
		  	  
		  
//proses change date-----------------------------------------------
		$('#start_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
		$('#end_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);		
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	


  }); //end document on ready	
		
</script>

<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>


	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
    function add_coa()
    {
      save_method = 'add';
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
	
	function edit_coa(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('coa_form/C_coa/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_coa"]').val(data.id_coa);
            $('[name="nomor_coa"]').val(data.nomor_coa);
			$('[name="desc_coa"]').val(data.desc_coa);
			$('[name="id_company"]').val(data.id_company);
			$('[name="id_divisi"]').val(data.id_divisi);
			$('[name="id_dept"]').val(data.id_dept);
            
            $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit COA'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



     function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('coa_form/C_coa/add_coa')?>";
      }
      else
      {
		   url = "<?php echo site_url('coa_form/C_coa/update_coa')?>";
      }
		  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
              if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
           
        });
    }

     function delete_coa()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('coa_form/C_coa/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
      
    }
	
  </script>

</html>
