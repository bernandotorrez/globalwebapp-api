<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<style>
/* for autocomplitee */
.ui-autocomplete {
    max-height: 200px;
    overflow-y: auto;    
    overflow-x: hidden;   
    padding-right: 20px;
}

* html .ui-autocomplete {
    height: 200px;
	
}

* html .ui-autocomplete-loading {
background:url('<?php echo base_url('asset/images/loading.gif') ?>') no-repeat right center ;
}
</style>
<div>
		<button onclick="add_parent()" class="btn btn-app btn-primary btn-xs radius-4 caddnew" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
        Add
		</button>
		<button onclick="delete_parent()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit" id="deleteTriger" name="deleteTriger"  value ="btndel"  disabled="disabled" >
      <i class="ace-icon fa fa-ban bigger-160"></i>
        Delete
		</button>
		<button onclick="edit_parent()" class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="editTriger"  name="editTriger"  value ="btnedit"  disabled="disabled" >
      <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
        Edit
		</button> 
		<button onclick="edit_position()" class="btn btn-app btn-flat btn-xs radius-4" type="submit">
			<i class="ace-icon glyphicon glyphicon-sort bigger-120"></i>
        Switch
		</button>
</div>
<br/>
<div class="table-header btn-info"> <?php echo " ".$header ;?> </div>
<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">

<div class="form-group col-xs-6">
	<div class="col-xs-10">
		<div class="form-inline ">
			<div class="form-group">Date start</div>

			<div class="form-group">
				<div class="input-group date">
					<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</div>

					<input type="text" class="form-control input-daterange" id="start_date" name="start_date"
						data-date-format="dd-mm-yyyy" autocomplete="off">
				</div> <!-- /.input-group datep -->
			</div>

			<div class="form-group">
				<label for="From" class="col-xs-1 control-label">To</label>
			</div>

			<div class="form-group">
				<div class="input-group date">
					<div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					</div>
					<input type="text" class="form-control input-daterange" id="end_date" name="end_date"
						data-date-format="dd-mm-yyyy" autocomplete="off">
				</div>
			</div>

		</div>
	</div>
</div>

	<table id="myTable" cellpadding="0" cellspacing="0" width="100%"
		class="table table-striped table-bordered table-hover">
		<thead class="text-warning">
			<th width="5%" style="text-align:center">
				<label class="pos-rel">
					<input type="checkbox" class="ace ace-checkbox-1" id="checkAll" />
					<span class="lbl"></span>
				</label>
			</th>
			<th>No</th>
			<th>Id</th>
			<th>Position Parent</th>
			<th>Name Parent</th>
			<th>Type Parent</th>
			<th>URL Parent</th>
			<th>Icon Parent</th>
			<th>Date Create</th>
		</thead>
	</table>
</div>
</div>
                                 	                       
<!-- Bootstrap modal -->
<form action="#" id="form" class="form-horizontal">
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Parent Form</h3>
      </div>
    <div class="modal-body form" >
        <div class="widget-body">
					<div class="widget-main">
          <input type="hidden" value="" name="id_parent"/>
            <div>
              <label for="id_parent">ID Parent</label>
							<?php
							$totaldata = $total[0]->totaldata+1;
							?>
              <input class="form-control" id="id_parent" name="id_parent" placeholder="Id_parent" type="text" readonly value="<?php echo $totaldata ?>">
            </div>
            <div>
							<label for="id_parent">Position Parent</label>
							<input class="form-control" id="order_parent" name="order_parent" placeholder="1" type="text"/>
						</div>
						<div>
							<label for="id_parent">Name Parent</label>
							<input class="form-control col-sm-9" id="parent_menu_desc" name="parent_menu_desc" placeholder="Home" type="text"/>
						</div>
						<div>
							<label for="id_parent">Type Parent</label>
							<input class="form-control col-sm-9" id="parent_fix" name="parent_fix" placeholder="Parent" type="text"/>
						</div>
						<div>
							<label for="id_parent">URL Parent</label>
							<input class="form-control col-sm-9" id="anchor_url" name="anchor_url" placeholder="main_menu/C_main_menu" type="text" title="Hello Put like : main_menu/C_main_menu " data-placement="bottom"/>
						</div>
						<div>
							<label for="icon">Icon Parent</label>
							<input class="form-control col-sm-9" data-rel="tooltip" id="icon" name="icon" placeholder="menu-icon fa fa-home" type="text" title="Hello Put like : menu-icon fa fa-home " data-placement="bottom"/>
						</div>
						<br/> 
						<br/>
					</div>
			</div>
			</div>
				<div class="modal-footer">
          <button onclick="save()" id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
					  <i class="ace-icon fa fa-floppy-o bigger-160"></i>
					    Save
				  </button>
				  <button class="btn btn-app btn-danger btn-xs radius-4" type="submit" data-dismiss="modal">
					  <i class="ace-icon fa fa-close bigger-160"></i>
					    Cancel
				  </button>
        </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  
  
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form2" role="dialog">
  	<div class="modal-dialog">
  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
  						aria-hidden="true">&times;</span></button>
  				<h3 class="modal-title">Position Form</h3>
  			</div>
  			<div class="modal-body form">

  				<table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top">
  					<thead class="text-warning">
  						<th>Id</th>
  						<th>Name Parent</th>
  						<th>Change Position</th>
  					</thead>
  					<tbody>
  						<?php $totaldata = $parent3[0]->totaldata+1;
						foreach($parent2 as $a){
					?>
  						<tr>
  							<td align="center" name="id_parent"><?php echo $a->id_parent;?></td>
  							<td name="name_parent"><?php echo $a->parent_menu_desc;?></td>
  							<td>
  								<select name="pos[<?=@$a->id_parent?>]">
  									<option value=""></option>
  									<?php for($i=1;$i<$totaldata;$i++){
							$sel = "";
							if($i == $a->order_parent){
								$sel = "selected='true'";
							}
						?>
  									<option value="<?=@$i?>" <?=@$sel?>><?=@$i?></option>
  									<?php } ?>
  								</select>
  							</td>
  						</tr>
  						<?php }?>
  					</tbody>
  				</table>
  			</div>
  			<div class="modal-footer">
  				<button onclick="save2()" id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4"
  					type="submit">
  					<i class="ace-icon fa fa-floppy-o bigger-160"></i>
  					Save
  				</button>
  				<button class="btn btn-app btn-danger btn-xs radius-4" type="submit" data-dismiss="modal">
  					<i class="ace-icon fa fa-close bigger-160"></i>
  					Cancel
  				</button>
  			</div>
  		</div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
  </form>
  

	
<script>
//$('#myTable').dataTable();
//-----------------------------------------data table custome----
// var rows_selected = [];
// var tableUsers = $('#myTable').DataTable({
//         "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
// 		"dom": 'lfBrtip',
// 		"buttons": [
//             {
// 		   "extend":    'copy',
// 		   "text":      '<i class="fa fa-files-o"></i> Copy',
// 		   "titleAttr": 'Copy'
// 		   },
// 		   {
// 		   "extend":    'print',
// 		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
// 		   "titleAttr": 'Print',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   },
// 		   {
// 		   "extend":    'excel',
// 		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
// 		   "titleAttr": 'Excel'
// 		   },
// 		   {
// 		   "extend":    'pdf',
// 		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
// 		   "titleAttr": 'PDF',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   }
//         ],
// 		"autoWidth" : false,
// 		"scrollY" : '250',
// 		"scrollX" : true,
//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.

//         // Load data for the table's content from an Ajax source
//         "ajax": {
//             "url": "<?php echo base_url('parent_form/C_parent/ajax_list') ?>",
//             "type": "POST"
//         },
// 		'columnDefs': [
// 		{
//         'targets': [0],
// 		'orderable': false,
// 		}
//    ],

//       'order': [[2, 'desc']],
//         });
/*var tableUsers = $('#myTable').DataTable({
            searching:true,
			//width:'100%',
			autoWidth : false,
  		    responsive : true,
			"scrollY" : '250',
			"scrollX" : true,
			ordering: false,
			/*oLanguage: {
				sProcessing: "<img src='' />",
			},
			processing: true,
			serverSide: true,
			ajax: {
			  url: "<?php echo base_url('parent_form/C_parent/ambil_data') ?>",
			  type:'POST',
	}
			
        });	*/		
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $(cells ).find(':checkbox').prop('checked', $(this).is(':checked'));
	$('#editTriger').prop("disabled", true);	
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>

<!-- Datatable -->
<script>
$(document).ready(function(){ 
		$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data('no'); //
	
		function fetch_data(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
				"ajax":{
				    url: "<?php echo base_url('parent_form/C_parent/ajax_list') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
				 
				 'columnDefs': [
							{
							'targets': [0],
							'orderable': false,
							}
					],

						'order': [[2, 'desc']],
			 });
		  }
		  	  
		  
//proses change date-----------------------------------------------
		$('#start_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
		$('#end_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);		
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	
 
 
 
 //onblur date-------------------------------------------------------------------------
 
 		// $('#start_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
		
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		  fetch_data('no', start_date, end_date);
		// 	} 
			
		// });
 
 
 	
		// $('#end_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
			
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		 fetch_data('no', start_date, end_date);
		// 	} 
		// });
 //end onblur-------------------------------------	

  }); //end document on ready	
		
</script>

<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>

	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
    function add_parent()
    {
      save_method = 'add';
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
	
	function edit_position()
    {
		$.ajax({
        url:"<?php echo site_url('parent_form/C_parent/ajax_parent/')?>/", //the page containing php script
        type: "POST", //request type
        success:function(result){
			$('#id_position').html(result);
            $('#modal_form2').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Position'); // Set title to Bootstrap modal title
       },
	    error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
     });
	}
	
	function edit_parent(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('parent_form/C_parent/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_parent"]').val(data.id_parent);
            $('[name="order_parent"]').val(data.order_parent);
			$('[name="parent_menu_desc"]').val(data.parent_menu_desc);
			$('[name="parent_fix"]').val(data.parent_fix);
			$('[name="anchor_url"]').val(data.anchor_url);
			$('[name="icon"]').val(data.icon);
            
            $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Parent'); // Set title to Bootstrap modal title
			

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



     function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('parent_form/C_parent/add_parent')?>";
      }
      else
      {
		   url = "<?php echo site_url('parent_form/C_parent/update_parent')?>";
      }
          var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
              if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }
	
	
	function save2()
    {
      var url;
      url = "<?php echo site_url('parent_form/C_parent/update_position')?>";
      var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
              if(data=='Insert'){
				$('#modal_form2').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
           
        });
    }
	
	
     function delete_parent()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('parent_form/C_parent/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}					
      
    }
	
  </script>

<script>

//autocomplete----		
	
$(document).ready(function () {
	$(function () {
		$("#icon").autocomplete({
			minLength:0,
			delay:0,
			source: function(request, response) {	
			var str_url = '<?php echo site_url('parent_form/C_parent/suggest_icon'); ?>';					
			var str_icon =$("#icon").val();																																		
				$.ajax({ 
					url: str_url ,												
					data: {icon:str_icon},																																		
					dataType: "json",
					type: "POST",										  					
					success: function(data){						
						response(data);	
						$(".ui-autocomplete").css("z-index", "2147483647");																																																						
					}						
				});
			},	
				select:function(event, ui){					  
				   $('#icon').val(ui.item.pashing_icon);					  
				}						                  				                     
		});		
			
	});

});	  	
 </script> 

</html>