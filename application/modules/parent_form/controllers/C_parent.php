<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_parent extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_parent');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_parent->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}	
	 	}
		
	public function index()
		{

		$data['parent']=$this->M_parent->get_all_parent();
		$data['parent2']=$this->M_parent->get_all_parent();
		$data['parent3']=$this->M_parent->get_all_count();
		$data['total']=$this->M_parent->get_count_id();
		$data['position']=$this->M_parent->get_parent();
		$data['show_view'] = 'parent_form/V_parent';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		}
										
	public function ajax_list()
	{
		$list = $this->M_parent->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_parent_menu) {
			$chk_idmaster ='<div align="center"><input id="checkdept" name="checkdept" type="checkbox" value='.$tbl_parent_menu->id_parent.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_parent_menu->id_parent;
			$row[] = $tbl_parent_menu->order_parent;
			$row[] = $tbl_parent_menu->parent_menu_desc;
			$row[] = $tbl_parent_menu->parent_fix;
			$row[] = $tbl_parent_menu->anchor_url;
			$row[] = $tbl_parent_menu->icon;
			$row[] = $tbl_parent_menu->date_create;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_parent->count_all(),
						"recordsFiltered" => $this->M_parent->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		@$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		@$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		@$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		@$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$param_count = http_request_get(API_URL.'/masterParent/countAllResult','');
		$param_count = json_decode($param_count, true);
		$total = $param_count['length'];

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, jika user ada melakukan search */
		if($search!=""){
		$param_search = array('search' => $search, 'limit' => $length, 'start' => $start);
		$param_search = http_request_get(API_URL.'/masterParent/datatableSearch', $param_search);
		// balikan si API
		$param_search = json_decode($param_search, true);
		$output['recordsTotal']=$output['recordsFiltered']=$param_search['length'];
		}

		// proses paling awal saat load, dan saat user ada klik pada colum di table nya
		if (isset($_REQUEST["order"])){
		$parameter = array('search' => $search, 'limit' => $length, 'start' => $start, 'order' => $column_order[$_REQUEST["order"][0]["column"]], 'dir' => $_REQUEST["order"][0]['dir']);	
		}else{
		$parameter = array('search' => $search, 'limit' => $length, 'start' => $start, 'order' => 'id_parent', 'dir' => 'asc');
		}

		$query = http_request_get(API_URL.'/masterParent/datatableSearch', $parameter);
		$query = json_decode($query, true);


		
		foreach ($query->result_array() as $tbl_parent_menu) {
		$chk_idmaster ='<div align="center"><input id="checkvendor" name="checkvendor" type="checkbox" value='.$tbl_parent_menu["id_parent"].' class="editRow ace" req_id_del='.$tbl_parent_menu["id_parent"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($tbl_parent_menu['id_parent'],$tbl_parent_menu['order_parent'],
									$tbl_parent_menu['parent_menu_desc'],$tbl_parent_menu['parent_fix'],
									$tbl_parent_menu['anchor_url'],$tbl_parent_menu['icon'],
									$chk_idmaster
									);

		}

		echo json_encode($output);


	}
		
	public function add_parent()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_add = date('Y-m-d H:i:s');

		if(empty($_POST["order_parent"])){
			die ("Field Position Parent must be filled in!! ");
		}
		elseif(empty($_POST["parent_menu_desc"])){
			die ("Field Name Parent must be filled in!! ");
		}
		elseif(empty($_POST["parent_fix"])){
			die ("Field Type Parent must be filled in!! ");
		}
		elseif(empty($_POST["anchor_url"])){
			die ("Field URL Parent must be filled in!! ");
		}
		elseif(empty($_POST["icon"])){
			die ("Field Icon Parent must be filled in!! ");
		}
		else{
			$data = array(
					'order_parent' => $this->input->post('order_parent'),
					'parent_menu_desc' => $this->input->post('parent_menu_desc'),
					'parent_fix' => $this->input->post('parent_fix'),
					'anchor_url' => $this->input->post('anchor_url'),
					'icon' => $this->input->post('icon'),
					'date_create' => $date_add,
					'remarks' => $remarks
				);
			$insert = $this->M_parent->add_parent($data);
		  }
		  echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_parent->get_by_id($id);
			echo json_encode($data);
		}
	
	public function ajax_parent()
		{
			$data = $this->M_parent->get_by_id();
			echo json_encode($data);
		}
		
	public function ajax_parentnew()
		{
		$data['position']=$this->M_parent->get_parent();
        $this->load->view('position', $data);
		}
	
	public function update_parent()
		{

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_update = date('Y-m-d H:i:s');

		if(empty($_POST["order_parent"])){
			die ("Field Position Parent must be filled in!! ");
		}
		elseif(empty($_POST["parent_menu_desc"])){
			die ("Field Name Parent must be filled in!! ");
		}
		elseif(empty($_POST["parent_fix"])){
			die ("Field Type Parent must be filled in!! ");
		}
		elseif(empty($_POST["anchor_url"])){
			die ("Field URL Parent must be filled in!! ");
		}
		elseif(empty($_POST["icon"])){
			die ("Field Icon Parent must be filled in!! ");
		}
		else{
		$data = array(
					'id_parent' => $this->input->post('id_parent'),
					'order_parent' => $this->input->post('order_parent'),
					'parent_menu_desc' => $this->input->post('parent_menu_desc'),
					'parent_fix' => $this->input->post('parent_fix'),
					'anchor_url' => $this->input->post('anchor_url'),
					'icon' => $this->input->post('icon'),
					'date_update' => $date_update,
					'remarks' => $remarks_update
				);
		$this->M_parent->update_parent(array('id_parent' => $this->input->post('id_parent')), $data);
		  }
		  echo 'Insert';
		}
	
	
	public function update_position()
		{
				$count = 0;
				$displayed_city = array();
				$city = array();

				foreach ($_POST['pos'] as $pos_key => $pos_value){
				  $jumlah = count($_POST['pos']); 
				  $count	++;
				  $city = '';
				  if( isset($pos_value)){
					$city = $pos_value;
					if(!in_array($city, $displayed_city)){
					  $displayed_city[] = $city;
					  $total = count($displayed_city);
					  
					}
				  }
		}
		 if($total!=$jumlah){
			die('Duplicate Data !');
			
		  }else{
			echo "Insert";
			foreach ($_POST['pos'] as $pos_key => $pos_value){
			 $dede[$pos_key] = $pos_value;
			 $parentnya['order_parent'] = $pos_value;
			 $this->db->where("id_parent",$pos_key);
			 $this->db->update("tbl_parent_menu",$parentnya);
			 
		   }
}
		}
		
		
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$this->db->where('id_parent', $id);
				$this->db->delete('tbl_parent_menu');
			}
		}
	}

	public function suggest_icon()
	{
												
	    $icon = $this->input->post('icon',TRUE);						       		     			
		//$query = $this->db->query("select desc_fafaicon from tbl_faicon where desc_fafaicon like '".'%'.$icon.'%'."'");						
		//$hasil = $query->result();
		
	    $this->load->database();
	    $this->db->select('*');		 	   	 
	    $this->db->like("desc_fafaicon",$icon);	  
	    $hasil = $this->db->get('tbl_faicon')->result();								
										
		$json_array = array();
		foreach ($hasil as $row)				
		$json_array[]= array("value" =>'menu-icon'.' '.$row->desc_fafaicon, 
                      "label" =>'menu-icon'.' '. $row->desc_fafaicon,	
					  "pashing_icon"=> 'menu-icon'.' '.$row->desc_fafaicon);
		echo json_encode($json_array);	
						
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }
	
	
}
