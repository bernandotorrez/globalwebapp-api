<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_demo_car_approval extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_demo_car_approval');
    }

    public function index() {
        //echo '<pre>';print_r($this->session->userdata());die;
        $data['show_view'] = 'demo_car_approval/V_demo_car_approval';

        $data['data_user'] = $this->M_demo_car_approval->getDataUser();
       
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function add_user(){
        $id_user_sign1 = $this->input->post('sign1');
        $id_user_sign2 = $this->input->post('sign2');

        $status = '1';

        // Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_add = date('Y-m-d H:i:s');

        // looping update sign 1
        if(count($id_user_sign1) > 0 ) {
            foreach($id_user_sign1 as $sign1) {
                $data = array(  'flag_approval_democar' => '1',
                                'date_create_app_democar' => $date_add,
                                'remarks_app_democar' => $remarks_add
                            ); 
                $update = $this->M_demo_car_approval->update($sign1, $data);
            }
        }

        // looping update sign 2
        if(count($id_user_sign2) > 0 ) {
            foreach($id_user_sign2 as $sign2) {
                $data = array(  'flag_approval_democar' => '2',
                                'date_create_app_democar' => $date_add,
                                'remarks_app_democar' => $remarks_add
                            ); 
                $update = $this->M_demo_car_approval->update($sign2, $data);
            }
        }

        echo 'Insert';

    }

    public function update_user(){
        $id_user = $this->input->post('id_user');
        $flag_approval_democar = $this->input->post('flag');

        // Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_update = date('Y-m-d H:i:s');

        $data = array(  'flag_approval_democar' => $flag_approval_democar,
                        'date_update_app_democar' => $date_update,
                        'remarks_app_democar' => $remarks_update
                    );

        $update = $this->M_demo_car_approval->update($id_user, $data);
        
        if($update) {
            echo 'Update';
        } else {
            echo 'Update Gagal';
        }

    }

    public function delete_data() {
       
        $id_user = $this->input->post('ID');

        // Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_delete = 'Action : Delete | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_delete = date('Y-m-d H:i:s');

        $data_id_array = explode(",", $id_user);
        if(!empty($data_id_array)) {
            foreach($data_id_array as $id) {
				$data = array(
                        'flag_approval_democar' => '0',
                        'date_update_app_democar' => $date_delete,
                        'remarks_app_democar' => $remarks_delete
				);
				$delete = $this->M_demo_car_approval->delete($id, $data);
			}
        } 

        echo json_encode('Delete');
    }

    public function ajax_edit($id) {
			$data = $this->M_demo_car_approval->get_by_id($id);
			echo json_encode($data);
	}

    public function ajax_list() {
            $list = $this->M_demo_car_approval->get_datatables();
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $qv_just_user_view) {

                if($qv_just_user_view->flag_approval_democar == '1') {
                    $flag_approval_democar = 'Sign 1';
                } elseif($qv_just_user_view->flag_approval_democar == '2') {
                    $flag_approval_democar = 'Sign 2';
                } else {
                    $flag_approval_democar = 'Sign Req';
                }

                $chk_idmaster ='<div align="center"><input id="checkuser" name="checkuser" type="checkbox" value='.$qv_just_user_view->id.' class="editRow ace" />
               <span class="lbl"></span> ';
                $no++;
                $row = array();
                $row[] = $chk_idmaster;
                $row[] = $no;
                $row[] = $qv_just_user_view->id;
                $row[] = $qv_just_user_view->name;
                $row[] = $qv_just_user_view->username;
                $row[] = $qv_just_user_view->name_grp;
                $row[] = $qv_just_user_view->company;
                $row[] = $qv_just_user_view->name_branch;
                $row[] = $qv_just_user_view->dept;
                $row[] = $qv_just_user_view->email;
                $row[] = $flag_approval_democar;
                $row[] = $qv_just_user_view->date_create_app_democar;
                $data[] = $row;
    }
    
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->M_demo_car_approval->count_all(),
                            "recordsFiltered" => $this->M_demo_car_approval->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }

}