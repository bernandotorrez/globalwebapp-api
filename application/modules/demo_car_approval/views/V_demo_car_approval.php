<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<div>
		<button onclick="add_user()" class="btn btn-app btn-primary btn-xs radius-4 caddnew" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
        Add
		</button>
		<button onclick="delete_user()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit" id="deleteTriger" name="deleteTriger"  value ="btndel"  disabled="disabled" >
      <i class="ace-icon fa fa-ban bigger-160"></i>
        Delete
		</button>
		<button onclick="edit_user()" class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="editTriger"  name="editTriger"  value ="btnedit"  disabled="disabled" >
      <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
        Edit
		</button> 
</div>
<br/>
<div class="table-header btn-info"> <?php echo " ".$header ;?>   </div>  
	<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">     

  <div class="form-group col-xs-6">
		<div class="col-xs-10">
			<div class="form-inline ">
				<div class="form-group">Date start</div>

				<div class="form-group">
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>

						<input type="text" class="form-control input-daterange" id="start_date" name="start_date"
							data-date-format="dd-mm-yyyy" autocomplete="off">
					</div> <!-- /.input-group datep -->
				</div>

				<div class="form-group">
					<label for="From" class="col-xs-1 control-label">To</label>
				</div>

				<div class="form-group">
					<div class="input-group date">
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
						<input type="text" class="form-control input-daterange" id="end_date" name="end_date"
							data-date-format="dd-mm-yyyy" autocomplete="off">
					</div>
				</div>

			</div>
		</div>
	</div>

		<table id="myTable" cellpadding="0" cellspacing="0" width="100%"  class="table table-striped table-bordered table-hover">
			<thead class="text-warning" > 
				<th width="5%" style="text-align:center">
					<label class="pos-rel">
            <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
            <span class="lbl"></span>
          </label>
        </th>
				<th>No</th>
					<th>Id</th>
					<th>Name</th>
					<th>Username</th>
					<th>Group</th>
					<th>Company</th>
					<th>Branch</th>
					<th>Dept</th>
					<th>Email</th>
          <th>Flag Approval Demo Car</th>
          <th>Date Create</th>
	    </thead>
    </table>
  </div>	                       
<!-- Bootstrap modal -->
  <div class="modal fade " id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content col-md-9">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Demo Car Approval</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">

       

        <div class="widget-body">
				  <div class="widget-main">
          <input type="hidden" value="" name="id_user"/>
          
          <div>
						<label for="dept">Sign 1</label>
            <select class="chosen-select form-control" id="sign1" name="sign1[]" data-placeholder="Pilih User Sign 1" multiple required>
           
                
                <?php
                    foreach($data_user as $value_user) {
                ?>
                <option value="<?=$value_user->id;?>"> <?=$value_user->username;?> </option>
                <?php
                    }
                ?>
            </select>
					</div>

          <div>
						<label for="dept">Sign 2</label>
            <select class="chosen-select form-control" id="sign2" name="sign2[]" data-placeholder="Pilih User Sign 2" multiple required>
           
                
                <?php
                    foreach($data_user as $value_user) {
                ?>
                <option value="<?=$value_user->id;?>"> <?=$value_user->username;?> </option>
                <?php
                    }
                ?>
            </select>
					</div>

        </div>
      </div>
      </form>
      </div>
          <div class="modal-footer">
            <button onclick="save()" id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
              <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                Save
            </button>
            <button class="btn btn-app btn-danger btn-xs radius-4" type="submit" data-dismiss="modal">
              <i class="ace-icon fa fa-close bigger-160"></i>
                Cancel
            </button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->

  

  <!-- Bootstrap modal -->
  <div class="modal fade " id="modal_form_edit" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content col-md-9">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Edit Demo Car Approval</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form-edit" class="form-horizontal">

       

        <div class="widget-body">
				  <div class="widget-main">
          <input type="hidden" value="" name="id_user"/>
          <div>
						<label for="id_model">ID User</label>
						

            <input class="form-control" id="id_user" name="id_user" placeholder="ID User" type="text" readonly />
					</div>

          <div>
						<label for="dept">Username</label>
            <input class="form-control" id="username" name="username" placeholder="Username" type="text" readonly />
					</div>

          <div>
						<label for="model_name">Name</label>
            <input class="form-control" id="nama" name="nama" placeholder="Model Name" type="text" readonly/>
					</div>

          <div>
						<label for="model_name">Flag Approval Demo Car</label>
            <select class="form-control" id="flag" name="flag">
              <option value="1"> Sign 1 </option>
              <option value="2"> Sign 2 </option>
            </select>
					</div>

        </div>
      </div>
      </form>
      </div>
          <div class="modal-footer">
            <button onclick="save()" id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
              <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                Save
            </button>
            <button class="btn btn-app btn-danger btn-xs radius-4" type="submit" data-dismiss="modal">
              <i class="ace-icon fa fa-close bigger-160"></i>
                Cancel
            </button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
    
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-chained/1.0.1/jquery.chained.min.js" type="text/javascript"></script>

  <script type="text/javascript">

            // $('#company').change(function() {
            //   $("#branch").chained("#company");
            // })
            
          
        </script>


   <script>


//$('#myTable').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
// var tableUsers = $('#myTable').DataTable({
// 		"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
// 		"dom": 'lfBrtip',
// 		"buttons": [
//             {
// 		   "extend":    'copy',
// 		   "text":      '<i class="fa fa-files-o"></i> Copy',
// 		   "titleAttr": 'Copy'
// 		   },
// 		   {
// 		   "extend":    'print',
// 		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
// 		   "titleAttr": 'Print',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   },
// 		   {
// 		   "extend":    'excel',
// 		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
// 		   "titleAttr": 'Excel'
// 		   },
// 		   {
// 		   "extend":    'pdf',
// 		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
// 		   "titleAttr": 'PDF',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   }
//         ],
//         "autoWidth" : false,
// 		"scrollY" : '250',
// 		"scrollX" : true,
//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.
//         "order": [], //Initial no order.

//         // Load data for the table's content from an Ajax source
//         "ajax": {
//             "url": "<?php echo base_url('demo_car_approval/C_demo_car_approval/ajax_list') ?>",
//             "type": "POST"
//         },
// 		'columnDefs': [
// 		{
//         'targets': [0],
// 		'orderable': false,
// 		}
// 		],

// 		'order': [[2, 'desc']],
//         });
				
//end--------------------------------------------------------------

	
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>

<!-- Datatable -->
<script>
$(document).ready(function(){ 
		$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data('no'); //
	
		function fetch_data(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
				"ajax":{
				    url: "<?php echo base_url('demo_car_approval/C_demo_car_approval/ajax_list') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
				 
				 'columnDefs': [
							{
							'targets': [0],
							'orderable': false,
							}
					],

						'order': [[2, 'desc']],
			 });
		  }
		  	  
		  
//proses change date-----------------------------------------------
		$('#start_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
		$('#end_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);		
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	


  }); //end document on ready	
		
</script>

<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>


	<script type="text/javascript">


	var save_method; //for save method string
    var table;
		
    function add_user()
    {
      save_method = 'add';
      $('#modal_form').modal('show'); // show bootstrap modal
      
      $('#modal_form').on('shown.bs.modal', function () {
        $('.chosen-select', this).chosen();
      });

      $('#modal_form').on('shown.bs.modal', function () {
        $('.chosen-select', this).chosen('destroy').chosen();
      });
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title

      // Hilangkan semua value input ketika tombol edit di tekan pertama kali sebelum add
      
      $('#sign1').val('');
      //$('#sign2').val('');
     

      
    }

    function edit_user(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('demo_car_approval/C_demo_car_approval/ajax_edit')?>/" + eee,
		    type: "GET",
        dataType: "JSON",
        success: function(data)
        {

          $('#id_user').val(data[0].id);
          $('#username').val(data[0].username);
          $('#nama').val(data[0].name);
          $('#flag').val(data[0].flag_approval_democar);
            
          $('#modal_form_edit').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
          //$('.modal-title').text('Edit Model Demo Car Form'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

	
    function save()
    {
      var url, action;
      
      if(save_method == 'add')
      {
        url = "<?php echo site_url('demo_car_approval/C_demo_car_approval/add_user')?>";
        action = 'Tambah';

        var sign1 = $('#sign1').val();
        var sign2 = $('#sign2').val();
        // Client Side Validation
        if(sign1 == '' && sign2 == '') {
          alert('Silahkan Pilih User Sign 1 atau Sign 2');
          $('#sign1').focus();
        } else {
          var formData = new FormData(document.getElementById('form')) 	    
            $.ajax({
              url : url,
              type: "POST",
              data: formData,										
              processData: false,															
              async: false,
              processData: false,
              contentType: false,		
              cache : false,	
              beforeSend: function()
              { 
                $("#btnSave").prop('disabled', true);
              },								
              success: function (data, textStatus, jqXHR)            
              {

                if(data=='Insert' || data=='Update'){
                  $('#modal_form').modal('hide');
                  location.reload();// for reload a page
                } else if(data=='Insert Gagal' || data=='Update Gagal') {
                  alert('Data Gagal di '+action);
                } else {
                  alert (data);
                }

                $("#btnSave").prop('disabled', false);
              },
              });
        }

      }
      else
      {
		    url = "<?php echo site_url('demo_car_approval/C_demo_car_approval/update_user')?>";
        action = 'Ubah';

        var flag = $('#flag').val();
        // Client Side Validation
        if(flag == '') {
          alert('Silahkan Pilih Flag Approval Demo Car');
          $('#sign1').focus();
        } else {
          var formData = new FormData(document.getElementById('form-edit')) 	    
            $.ajax({
              url : url,
              type: "POST",
              data: formData,										
              processData: false,															
              async: false,
              processData: false,
              contentType: false,		
              cache : false,	
              beforeSend: function()
              { 
                $("#btnSave").prop('disabled', true);
              },								
              success: function (data, textStatus, jqXHR)            
              {
                if(data=='Insert' || data=='Update'){
                  $('#modal_form').modal('hide');
                  location.reload();// for reload a page
                } else if(data=='Insert Gagal' || data=='Update Gagal') {
                  alert('Data Gagal di '+action);
                } else {
                  alert (data);
                }

                $("#btnSave").prop('disabled', false);
              },
              });
        }

      }

		  
    }

    function delete_user()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('demo_car_approval/C_demo_car_approval/delete_data')?>',{ID:ids_string},function(result){ 
              
              var json = JSON.parse(result)

              if(json == 'Delete') {
                $('#modal_form').modal('hide');
						    location.reload();// for reload a page
              } else {
                alert('Delete Data Gagal!');
              }

						  
					}); 
				}
			}
			
		

      
    }

  </script>

  

</html>
