<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_epp_history extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_epp_history','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('terbilang');			
		$this->load->library('fpdf');   		
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_epp_history->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}		
	}		  	
		
  public function index()
  {					    																
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_pp= $this->M_epp_history->tampil_add_pp()->result();	
	  $total_rows =$this->M_epp_history->tampil_add_pp()->num_rows();
	  									
	  if ($tampil_table_pp)
		{			
		
		
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;		
		$data['intno'] = ""; //variable buat looping no table.				
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		$data['ceck_row'] = $total_rows;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.
		$data['tampil_company']=$this->M_epp_history->get_company()->result();																															
		$data['tampil_dept']=$this->M_epp_history->get_dept()->result();																
		$data['show_view'] = 'epp_history/V_epp_history';		
		$this->load->view('dashboard/Template',$data);				
	  }else{	
	    $dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data PP table is empty';		
		$data['tampil_company']=$this->M_epp_history->get_company()->result();																															
		$data['tampil_dept']=$this->M_epp_history->get_dept()->result();		
		$data['show_view'] = 'epp_history/V_epp_history';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }
   
    public function do_search_data()  // fungsi cari data controler
    {				  			    	
		$tampung_cari = $this->M_epp_history->search_data(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;	
		   $data['intno'] = ""; //variable buat looping no table.					
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				   		
		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['pp_view'] =$tampung_cari;
		   $data['pesan'] = 'Data PP Not a found'; 		   		   
		   $data['show_view'] = 'epp_history/V_epp_history';		
		   $this->load->view('dashboard/Template',$data);					
		}else{			
		    $dept  = $this->session->userdata('dept') ;	
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;	
		    $data['intno'] = ""; //variable buat looping no table.				
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		    $data['tampil_company']=$this->M_epp_history->get_company()->result();																															
		    $data['tampil_dept']=$this->M_epp_history->get_dept()->result();		
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['pp_view'] =$tampung_cari;			   			
			$data['show_view'] ='epp_history/V_epp_history';		
		    $this->load->view('dashboard/Template',$data);		
		}	   
    }	
	
	 
    public function do_search_date()  // fungsi cari data controler
    {				  			    	
		$tampung_cari = $this->M_epp_history->search_date(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;	
		   $data['intno'] = ""; //variable buat looping no table.					
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				   		
		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['pp_view'] =$tampung_cari;
		   $data['pesan'] = 'Data PP Not a found'; 		   		   
		   $data['show_view'] = 'epp_history/V_epp_history';		
		   $this->load->view('dashboard/Template',$data);					
		}else{			
		    $dept  = $this->session->userdata('dept') ;	
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;					
			$data['intno'] = ""; //variable buat looping no table.
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		    $data['tampil_company']=$this->M_epp_history->get_company()->result();																															
		    $data['tampil_dept']=$this->M_epp_history->get_dept()->result();		
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['pp_view'] =$tampung_cari;			   			
			$data['show_view'] ='epp_history/V_epp_history';		
		    $this->load->view('dashboard/Template',$data);		
		}	   
    }		     		 
	 
	  public function do_give_right_print_fpb()
      {		
		if($this->M_epp_history->give_right_fpb_print())
		 {								    
		    $strnopp = $this->session->userdata('ses_nopp');
			$this->session->set_flashdata('pesan_give_right','PP No.'.$strnopp." ".'has been allow for reprint FPB!!'); 	
			$this->show_table(); //manggil fungsi show_table		
		  }else{     		
			 $this->session->set_flashdata('pesan_rpt','Give right Update Failed!!!!!! ....'); 					
			 $url=  base_url('c_epp_history/c_epp_history');		         
			 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
		  } 		 				 
  	  }	  
	
	
	//------------------------------------pashing result to modal popup-------------------
	public function get_nopp_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //       
	     $query_master = $this->db->query("select id_master,remarks,gran_total,ppn,pph,gran_totalppn,gran_total_adjust,currency from qv_head_pp_complite where id_master ='".$nopp."'");  
         $query_detail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   
		   
		      $data['str_pp_master']=$query_master->result();
			  $data['str_pp_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_nopp'] = $nopp ;
			  $tampil_detail = $this->load->view('v_epp_history/v_conten_detail',$data,true);
			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end phasing--------------------  
	
	function toExcelAll() {	 
		$tampil_to_excel =  $this->M_epp_history->ToExcelAll();									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
		$data['data_excel'] = $tampil_to_excel ; 		
		$this->load->view('epp_history/Excel_view',$data);
    }
		 	 
    public function epp_history_submit() //fungsi submit dengan banyak button dalam satu form
	{  	
		
		if ($this->input->post('btncari')){	
			$this->do_search_data();
		}else{	
		   if ($this->input->post('btncaridate')){	
			  $this->do_search_date();
		   }else{
				if ($this->input->post('btngiveright')){	
				   $this->do_give_right_print_fpb();
				}else{
					if ($this->input->post('btnreportexcel')){	
				       $this->toExcelAll();
				    }else{			
				       redirect('epp_history/c_epp_history');  //direct ke url add_menu_booking/add_menu_booking
					}
				}
		   }
		}				
		
	}
	
	
public function get_idtrans_modal()
{		
     $tampungmaster = $this->M_epp_history->m_mastergetid_pp();
	 $tampungdetail = $this->M_epp_history->m_getid_pp();	 
	  
	 $no="1";	 	
	 
	 echo "<table class='table table-striped table-bordered table-hover'>";
	 echo  "<tr style='font-weight:bold'>";
	 echo  "<td width='2%'>No</td>";
	 echo  "<td width='22%'>Description</td>";
	 echo  "<td width='22%'>Specs</td>";
	 echo  "<td width='10%'>P.O Ref</td>";
	 echo  "<td width='10%'>C.O.A</td>";
	 echo  "<td width='10%'>No Actifity</td>";
	 echo  "<td width='7%'>Qty</td>";
	 echo  "<td width='15%'>Prices</td>";
	 echo  "<td width='40%'>Total Prices</td>";
	 echo  "<td width='7%'>PPN</td>";
	 echo  "<td width='7%'>PPH</td>";
	 echo  "<td width='25%'>PPN Amount</td>";
	 echo  "<td width='25%'>PPH Amount</td>";
	 echo  "</tr> "; 	 	 	 				
		foreach ($tampungdetail as $row_jq) {
		    echo  "<tr> ";
			echo '<td>'.$no++.'</td>';			
			echo '<td>'.$row_jq->desc.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			echo '<td>'.$row_jq->po_reff.'</td>';
			echo '<td>'.$row_jq->coa.'</td>';
			echo '<td>'.$row_jq->no_act.'</td>';
			echo '<td>'.$row_jq->qty.'</td>';
			echo '<td>'. number_format($row_jq->harga,2,'.',',').'</td>';
			echo '<td>'. number_format($row_jq->total,2,'.',',').'</td>';
			echo '<td>'.$row_jq->tax_type.'</td>';
			echo '<td>'.$row_jq->tax_typepph.'</td>';
			echo '<td>'.number_format($row_jq->tax_detail,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->tax_detailpph,2,'.',',').'</td>';
		    echo "</tr>"; 							
			
		}
				
	 foreach ($tampungmaster as $row_jm) {	
	     echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='2'>Remarks :</td>";
		 echo  "<td width='40%' colspan='11'>".$row_jm->remarks."</td>";	
		 echo  "</tr> "; 
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Sub Total :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_total,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPN Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->ppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 		
		  echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>PPH Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->pph,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='12' align='right'>Total + PPN :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_totalppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo "</table>";	
	 }
}	

	
	function cath_data_fordatatables(){
						
		
		$str_submission = $this->session->userdata("name");
		$str_status_approved ="1";
			
		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];
		
		$search_date_approved = date('Y-m-d', strtotime($search)); //date
		$search_date_pp = date('Y-m-d', strtotime($search)); //date


		/*Menghitung total desa didalam database*/
		$this->db->select('id_master');
		$this->db->from('qv_head_pp_complite');					
		$this->db->where('status','1');
		$this->db->where('user_submission',$str_submission);
		$total = $this->db->count_all_results();
				
		//$total=$this->db->count_all_results("qv_vendor");
		//$total=$this->db->count_all_results("qv_head_pp_complite");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		
		
		if($search!=""){										   
			$this->db->where("status","1");
			$this->db->where('user_submission',$str_submission);				
			$this->db->like("id_master",$search);	
			$this->db->or_like("user_submission",$search);								
			$this->db->where("status","1");
			$this->db->where('user_submission',$str_submission);			
			$this->db->or_like("vendor",$search);								
			$this->db->where("status","1");
			$this->db->where('user_submission',$str_submission);
			$this->db->or_like("date_aprove_bod",$search_date_approved);								
			$this->db->where("status","1");
			$this->db->where('user_submission',$str_submission);			
			$this->db->or_like("date_pp",$search_date_pp);								
			$this->db->where("status","1");
			$this->db->where('user_submission',$str_submission);
			$this->db->or_like("dept",$search);								
			$this->db->where("status","1");
			$this->db->where('user_submission',$str_submission);			
		}

		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);																							
		$this->db->where("status","1");	
		$this->db->where("user_submission",$str_submission);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('date_pp','DESC');
		$query=$this->db->get('qv_head_pp_complite');


		/*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		
		if($search!=""){
		    $this->db->where("status","1");			
			$this->db->like("id_master",$search);	
			$this->db->or_like("user_submission",$search);								
			$this->db->where("status","1");	
			$this->db->or_like("vendor",$search);								
			$this->db->where("status","1");	
			$this->db->or_like("date_aprove_bod",$search_date_approved);								
			$this->db->where("status","1");	
			$this->db->or_like("date_pp",$search_date_pp);								
			$this->db->where("status","1");					
			$this->db->or_like("dept",$search);								
			$this->db->where("status","1");					
		  $jum=$this->db->get('qv_head_pp_complite');
		  $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $row_tbl) {
			
			 if ($row_tbl['status_send_aprove'] == "-1")	
			 {
				   $info_aproval = '<label style="color:red">'."Reject".'</label>';
			 }else{				
				   $info_aproval = "New Request";				
			 }
			 
			 If ($row_tbl['attach_quo'] != "")
			  {  
			   $attach_quo = '<a href='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-primary">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".'View'.'</a>' ;
			 }else{
				$attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
			 }
			 
			 $chk_idmaster ='<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value='.$row_tbl["id_master"].' class="ace" req_id_del='.$row_tbl["id_master"].' />
           <span class="lbl"></span> ';
		   
		   
		   $btn_view_detail = '<a href="#" class="btn btn-primary detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <span class="glyphicon glyphicon-list" aria-hidden="true"></span>Detail</a>';
						
			
			
			if ($row_tbl['date_aprove_bod'] != "") :
			   $strdateapproved = date('d-m-Y', strtotime($row_tbl['date_aprove_bod'])) ; 
			else:
				$strdateapproved = "-";
			endif ;   
			
			
			 if ($row_tbl['aprove_head'] == "1")
			 {					
				$strapprovedhead = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
			 }else{
				$strapprovedhead = '<label style="color:#EB293D">wait<label>';	
			 }
			 
			  if ($row_tbl['approve_purchasing'] == "1")
			 {					
				 $strapprovedpur = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
			 }else{
				 $strapprovedpur = '<label style="color:#EB293D">wait<label>';	
			 }
			 
			  if ($row_tbl['aprove_fc'] == "1")
			 {					
				 $strapprovedfc = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
			 }else{
				$strapprovedfc = '<label style="color:#EB293D">wait<label>';	
			 }
			 
			  if ($row_tbl['aprove_bod'] == "1")
			 {					
				 $strapprovedbod = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
			 }else{
				 $strapprovedbod = '<label style="color:#EB293D">wait<label>';	
			 }
			if ($row_tbl['aprove_presdir'] == "1")
			 {					
				 $strapprovedpresdir = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
			 }else{
				 $strapprovedpresdir = '<label style="color:#EB293D">wait<label>';	
			 }
				 
			$output['data'][]=array($row_tbl['id_master'],
									$row_tbl['short'],
									$row_tbl['dept'],
									$row_tbl['vendor'],		
									$row_tbl['user_submission'],
									$row_tbl['header_desc'],
									substr($row_tbl['remarks'],0,25),	
									date('d-m-Y', strtotime($row_tbl['date_pp'])),
									$strdateapproved,										
									$row_tbl['time_approved'],
									//$row_tbl['nomor_coa'],
									$row_tbl['type_purchase'],	
									$row_tbl['currency'],		
									number_format($row_tbl['gran_total'],2,'.',','),
									number_format($row_tbl['ppn'],2,'.',','),
									number_format($row_tbl['gran_totalppn'],2,'.',','),		
									$row_tbl['term_top'],																																																									
									$strapprovedhead,	
									$strapprovedbod ,
									$strapprovedfc,
								    $strapprovedpresdir,
									$btn_view_detail,
								    $attach_quo,																																																																																			
									$row_tbl['counter_reprint']
																																				
							  );			
		}
		echo json_encode($output);
	}	
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}
	
}

