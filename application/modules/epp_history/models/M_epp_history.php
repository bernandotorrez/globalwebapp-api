<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_epp_history extends CI_Model {	
				    
    public $db_tabel = 'qv_head_pp_complite';
	 
    public function tampil_add_pp(){ //tampil table untuk ngececk num_row	
	
	     $ses_id_company = $this->session->userdata('id_company'); 
		 $str_submission = $this->session->userdata('name'); 		 
		 					     	     		  
	     $status_delete = "1"; //jika 0 terdelete jika 1 aktif		 		 
		 $this->load->database();		
		 $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,aprove_head,aprove_fc,aprove_bod,date_aproval,term_top,user_submission,flag_fpb,flag_bpk,flag_print_bpk,status,status_send_aprove,short,currency,gran_total,attach_quo'); 	
		 			
		 $this->db->where('status',$status_delete);
		 $this->db->where('user_submission',$str_submission);		 
		 
		 if ($ses_id_company =="10"){ //emi
			 $this->db->where('id_company',$ses_id_company);	//emi			 								 				 
		 }
		 
		 $this->db->order_by('date_pp','desc');
		 $result = $this->db->get('qv_head_pp_complite');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	   
	     
		 $ses_id_company = $this->session->userdata('id_company'); 
		 $str_submission = $this->session->userdata('name');		 
		 			
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		 		  	     		 
	     $this->load->database();		
		 $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,aprove_head,aprove_fc,aprove_bod,date_aproval,term_top,user_submission,flag_fpb,flag_bpk,flag_print_bpk,status,status_send_aprove,short,currency,gran_total,attach_quo,counter_reprint'); 			
		 $this->db->where('status',$status_delete);
		 $this->db->where('user_submission',$str_submission);		 
		 
		  if ($ses_id_company =="10"){ //emi
			  $this->db->where('id_company',$ses_id_company);	//emi			 								 				 
		  }
		 	 
		 $this->db->order_by('date_pp','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_head_pp_complite')->result();		 		   
		 return $result ;
	}
	
	function get_company()
	{		
	     $this->load->database();		
		 $this->db->select('id_company,company,short');				 
		 $result = $this->db->get('tbl_company');			     		 
		 return $result;	 	
	}
	
	function get_dept()
	{		
	  
	  $data_approvalflag_head = $this->session->userdata('aproval_flag'); 
	  $data_iddept = $this->session->userdata('id_dept');   
	  
	  if ($data_approvalflag_head == "3") {
			 $this->load->database();		
			 $this->db->select('id_dept,dept');	
			 $this->db->where('id_dept',$data_iddept);			 
			 $result = $this->db->get('tbl_dept');			     		 
			 return $result;	 	
	  }else{	  
			 $this->load->database();		
			 $this->db->select('id_dept,dept');				 
			 $result = $this->db->get('tbl_dept');			     		 
			 return $result;	 	
	  }
	}
	
	function search_data() 	// fungsi cari data pp model
	{
	   $ses_id_company = $this->session->userdata('id_company');
	   $str_submission = $this->session->userdata('name');	   
	   $cari = $this->input->post('txtcari');
	   $kategory =  $this->input->post('cbostatus');	   	 
	   
	   $status_delete = "1"; //jika 0 terdelete jika 1 aktif	
	   
	         $this->load->database();	
	   	     $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,aprove_head,aprove_fc,aprove_bod,date_aproval,term_top,user_submission,flag_fpb,flag_bpk,flag_print_bpk,status,status_send_aprove,short,currency,ppn,pph,gran_totalppn,gran_total,attach_quo,counter_reprint');  
	   		
			 if ($cari != null) {
				   $this->load->database();		   				  
				   $this->db->where('status',$status_delete);
				   $this->db->where('user_submission',$str_submission);				   
				    
					if ($ses_id_company =="10"){ //emi
						 $this->db->where('id_company',$ses_id_company);	//emi			 								 				
		 			}
					
				   $this->db->like($kategory,$cari);
				   $this->db->order_by('date_aproval','desc');		   
				   $result = $this->db->get('qv_head_pp_complite');		 
				   return $result->result(); 	   		 
			 }else{
				   redirect('c_epp_history/c_epp_history'); 
			 }		  		  	 
	 } 		
	
	function search_date() 	// fungsi cari data pp model
	{
	   $ses_id_company = $this->session->userdata('id_company');
	   $str_submission = $this->session->userdata('name');
	   $strdatestart = $this->input->post('txtdatestart'); 
	   $strdatend = $this->input->post('txtdateend');
	   	  
	   $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	   $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
	   $status_delete = "1"; //jika 0 terdelete jika 1 aktif		  
	   
	         $this->load->database();	
	   	     $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,aprove_head,aprove_fc,aprove_bod,date_aproval,term_top,user_submission,flag_fpb,flag_bpk,flag_print_bpk,status,status_send_aprove,short,currency,ppn,pph,gran_totalppn,gran_total,attach_quo,counter_reprint');  
	   	
		   if($strdatestart !="" and $strdatend !="" ){	
		       $this->db->where('date_aproval BETWEEN "'.$datestart.'" and "'.$dateend.'"');	
	  		   $this->db->where('status',$status_delete);
			   $this->db->where('user_submission',$str_submission);
			   $this->db->order_by('date_aproval','desc');		   
		       $result = $this->db->get('qv_head_pp_complite');		 
		       return $result->result(); 
		  
		    }else{
			   redirect('c_epp_history/c_epp_history'); 
		    }		  	 
	 } 		
	
	
	 function ToExcelAll(){ // buat narik table to excel						    
	 	 $ses_id_company = $this->session->userdata('id_company');
		 $str_submission = $this->session->userdata('name');
		 $strcom = $this->input->post("cbocomp"); 		  		 	  				
		 $strdept =$this->input->post("cbodept");
		 $strbpk = $this->input->post("cbobpk");
		 $tglstart = $this->input->post('txtrptdtstart');
		 $tglend =  $this->input->post('txtrptdtend'); 
		 
		 $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	     $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal
	   
	  	 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
		
		 
		     $hasil = array();	       		    			 
			 $this->load->database();				 	
			 $this->db->select('id_master,id_company,id_dept,company,dept,name_branch,vendor,header_desc,date_send_aproval,date_aproval,term_top,user_submission,flag_fpb,flag_bpk,flag_print_bpk,status,short,remarks,currency,gran_total,ppn,pph,gran_totalppn,time_approved,type_purchase');								 		 
			 
			 if ($strcom != "all") :			 		  
			 	$this->db->where('id_company',$strcom);
			 endif;
			 	
			 if ($strdept != "all") :			 		  
			 	 $this->db->where('id_dept',$strdept);
			 endif;	
			 
			 if ($strbpk != "all") :			 		  
			 	 $this->db->where('flag_bpk','1');
			 endif;	
			 
			 $this->db->where('status',$status_delete );
			 $this->db->where('user_submission',$str_submission);			 
			 $this->db->where('date_send_aproval BETWEEN "'. $datestart. '" and "'.$dateend.'"');			
			 $this->db->order_by('date_send_aproval','asc');
			 $result = $this->db->get('qv_head_pp_complite');	
			 
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
		 }else{
				 return null ;
		 } 		 				 								 
	}	 		
		
	
	function give_right_fpb_print()
	{
					
	  $strid_master =$this->input->post('msg'); 	 	  						  					    						   	 			   	   					  				   			  			   			
	 	for ($i=0; $i < count($strid_master) ; $i++) { 				
			$query = $this->db->select("id_master,desc,spec,qty,harga,total,vendor,currency,gran_total,ppn,pph,gran_totalppn,counter_reprint")
								->where('id_master',$strid_master[$i])
								->get('qv_master_det_pp');
			  if ($query->num_rows() >= 1)
			  {				
			    
				  foreach ($query->result() as $row)
				  {
			  		$data_right = array('ses_nopp' => $strid_master[$i]); 																							  					$this->session->set_userdata($data_right); //simpan di session
					
					$str_buff_counter_reprint = $row->counter_reprint ;
				  }	  								  
			     							  
				 //Update flag fpb yg sedang di cetak ------------------------------------
				  $str_give_flag_fpb= "0"; //0 jika di berikan izin untuk print ulang		
				  $data_flag =array("flag_print_fpb" => $str_give_flag_fpb, //give flag_print_fpb					  													
									);	 
				  $this->db->update('tbl_master_pp',$data_flag);					
				  
				  $this->db->set('counter_reprint','counter_reprint+1', FALSE);					
				  $this->db->where('id_master',$strid_master[$i]); 				
				  $this->db->update('tbl_master_pp'); 				
				 //---------------------------------------------------------------------								 
					 
					 return $query->result();										 
			  }else{
					 return false;
			  }
	   	}	
	   
	 }		
	 
	  function m_mastergetid_pp()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('id_master,gran_total,ppn,pph,gran_totalppn,remarks');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_master_pp');		
		return $result->result();
		
	}	   	
	
	function m_getid_pp()
	{
		$stridpp= $this->input->post('id_pp');					
		$this->db->select('id_master,desc,spec,coa,no_act,qty,harga,total,po_reff,tax_type,tax_typepph,tax_detail,tax_detailpph');
		$this->db->where('id_master',$stridpp);		
		$this->db->where('status','1');	//aktiff	
		$result = $this->db->get('tbl_detail_pp');		
		return $result->result();
		
	}

	public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}
						  						    	   			  	   			 	 	 	   				
				 
}