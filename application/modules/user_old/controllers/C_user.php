<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_user extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_user');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_user->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}
	 	}
		
	public function index()
		{

		$data['user']=$this->M_user->get_all_user();
		$data['total']=$this->M_user->get_count_id();
		$data['group']=$this->M_user->get_all_group();
		$data['department']=$this->M_user->get_all_dept();
		//$data['divisi']=$this->M_user->get_all_divisi();
		//$data['divisi2']=$this->M_user->get_all_divisi();
		$data['purchase']=$this->M_user->get_all_purchase();
		$data['company']=$this->M_user->get_all_company();
		$data['company2']=$this->M_user->get_all_company();
		$data['show_view'] = 'user/V_user';
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function ajax_list()
	{
		$list = $this->M_user->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $qv_just_user_view) {
			$chk_idmaster ='<div align="center"><input id="checkuser" name="checkuser" type="checkbox" value='.$qv_just_user_view->id.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $qv_just_user_view->id;
			$row[] = $qv_just_user_view->name;
			$row[] = $qv_just_user_view->username;
			$row[] = $qv_just_user_view->name_grp;
			$row[] = $qv_just_user_view->company;
			$row[] = $qv_just_user_view->name_branch;
			//$row[] = $qv_just_user_view->divisi;
			$row[] = $qv_just_user_view->dept;
			$row[] = $qv_just_user_view->email;
			$row[] = $qv_just_user_view->email_head;
			$row[] = $qv_just_user_view->phone;
			$row[] = $qv_just_user_view->sign_head;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_user->count_all(),
						"recordsFiltered" => $this->M_user->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	public function reset_pass()
	{
		$data = array(
					'id' => $this->input->post('id'),
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('username')),
					);
		$this->M_user->update_user(array('id' => $this->input->post('id')), $data);
		echo 'Insert';
	}
	
	public function add_user()
		{
		 $email = $this->input->post('email');
		 $emailhead = $this->input->post('email_head');
		 $emailcc = $this->input->post('email_cc');
		 $phone = $this->input->post('phone');
		 $username = $this->input->post('username');
		 $this->db->where('username', $username);
		 $query = $this->db->get('tbl_user');
		 $count_row = $query->num_rows();
		
		if(empty($_POST["name"])){
			die ("Field Name must be filled in!! ");
		}
		elseif(empty($_POST["first_name"])){
			die ("Field First Name must be filled in!! ");
		}
		elseif(empty($_POST["last_name"])){
			die ("Field Last Name must be filled in!! ");
		}
		elseif(empty($_POST["nick_name"])){
			die ("Field Nick Name must be filled in!! ");
		}
		elseif(empty($_POST["username"])){
			die ("Field Username must be filled in!! ");
		}
		elseif($count_row > 0){
			die ("Field Username already exist! ");
		}
		elseif (preg_match('/\s/',$username)){
			die ("Field Username not use space! ");
		}
		elseif(empty($phone) || !preg_match('/^[+#*\(\)\[\]]*([0-9][ ext+-pw#*\(\)\[\]]*){6,45}$/', $phone)){
			die ("Field Phone must be filled in and numeric!! ");
		}
		elseif(empty($_POST["email"])){
			die ("Field Email must be filled in!! ");
		}
		elseif(!preg_match('/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/', $email)) {
			die ("Field Email isn't valid! ");
		}
		elseif(empty($_POST["sign_head"])){
			die ("Field Head must be filled in!! ");
		}
		elseif(empty($_POST["email_head"])){
			die ("Field Email Head must be filled in!! ");
		}
		elseif(!preg_match('/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/', $emailhead)) {
			die ("Field Email Head isn't valid! ");
		}
		elseif(empty($_POST["id_group"])){
			die ("Field Group must be filled in!! ");
		}
		elseif(empty($_POST["id_company"])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_POST["branch_id"])){
			die ("Field Branch must be filled in!! ");
		}
		//elseif(empty($_POST["id_divisi"])){
		//	die ("Field Divisi must be filled in!! ");
		//}
		elseif(empty($_POST["id_dept"])){
			die ("Field Department must be filled in!! ");
		}
		elseif(99 == ($_POST["aproval_flag"])){
			die ("Field Status App must be filled in!! ");
		}
		elseif(99 == ($_POST["give_righ_flag"])){
			die ("Field Status Level must be filled in!! ");
		}
		elseif(empty($_POST["purchase"])){
			die ("Field Status Purchase must be filled in!! ");
		}
		elseif (empty($emailcc)){
			$emailcc == null ;
		}
		elseif (preg_match("/^[-]*$/",$emailcc)){
			die ("Field Email CC not valid!! ");
		}
		else{
			 $purchase = $_POST['purchase'];
			 $value = implode(",",$purchase);
			 $data = array(
					'id' => $this->input->post('id'),
					'name' => $this->input->post('name'),
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'nick_name' => $this->input->post('nick_name'),
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('username')),
					'phone' => $this->input->post('phone'),
					'email' => $this->input->post('email'),
					'sign_head' => $this->input->post('sign_head'),
					'email_head' => $this->input->post('email_head'),
					'email_cc' => $this->input->post('email_cc'),
					'id_group' => $this->input->post('id_group'),
					'id_company' => $this->input->post('id_company'),
					'branch_id' => $this->input->post('branch_id'),
					//'id_divisi' => $this->input->post('id_divisi'),
					'id_dept' => $this->input->post('id_dept'),
					'aproval_flag' => $this->input->post('aproval_flag'),
					'give_righ_flag' => $this->input->post('give_righ_flag'),
					'status_pur' => $value,
					'status' => '1',
				);
			$insert = $this->M_user->add_user($data);
		  }
		 echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_user->get_by_id($id);
			echo json_encode($data);
		}
		
	/*public function ajax_divisi()
    {
        $id = $this->input->post('id_divisi');
        $data['dept'] = $this->M_user->get_dept($id);
        $this->load->view('dept',$data);
		}
	*/
	function pos_branch($id_company)
	{
    	$query = $this->db->get_where('tbl_branches',array('id_company'=>$id_company));
    	$data = "<option value=''>- Select Branch -</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->branch_id."'>".$value->name_branch."</option>";
    	}
    	echo $data;
	}

	public function update_user()
		{
		 $email = $this->input->post('email');
		 $emailhead = $this->input->post('email_head');
		 $emailcc = $this->input->post('email_cc');
		 $phone = $this->input->post('phone');
		 $username = $this->input->post('username');
		 $this->db->where('username', $username);
		 $query = $this->db->get('tbl_user');
		 $count_row = $query->num_rows();
		 

		if(empty($_POST["name"])){
			die ("Field Name must be filled in!! ");
		}
		elseif(empty($_POST["first_name"])){
			die ("Field First Name must be filled in!! ");
		}
		elseif(empty($_POST["last_name"])){
			die ("Field Last Name must be filled in!! ");
		}
		elseif(empty($_POST["nick_name"])){
			die ("Field Nick Name must be filled in!! ");
		}
		elseif(empty($phone) || !preg_match('/^[+#*\(\)\[\]]*([0-9][ ext+-pw#*\(\)\[\]]*){6,45}$/', $phone)){
			die ("Field Phone must be filled in and numeric!! ");
		}
		elseif(empty($_POST["email"])){
			die ("Field Email must be filled in!! ");
		}
		elseif(!preg_match('/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/', $email)) {
			die ("Field Email isn't valid! ");
		}
		elseif(empty($_POST["sign_head"])){
			die ("Field Head must be filled in!! ");
		}
		elseif(empty($_POST["email_head"])){
			die ("Field Email Head must be filled in!! ");
		}
		elseif(!preg_match('/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/', $emailhead)) {
			die ("Field Email Head isn't valid! ");
		}
		elseif(empty($_POST["email_cc"])){
			die ("Field Email CC must be filled in!! ");
		}
		elseif(empty($_POST["id_group"])){
			die ("Field Group must be filled in!! ");
		}
		elseif(empty($_POST["id_company"])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_POST["branch_id"])){
			die ("Field Branch must be filled in!! ");
		}
		//elseif(empty($_POST["id_divisi"])){
		//	die ("Field Divisi must be filled in!! ");
		//}
		elseif(empty($_POST["id_dept"])){
			die ("Field Department must be filled in!! ");
		}
		elseif(99 == ($_POST["aproval_flag"])){
			die ("Field Status App must be filled in!! ");
		}
		elseif(99 == ($_POST["give_righ_flag"])){
			die ("Field Status Level must be filled in!! ");
		}
		elseif(empty($_POST["purchase"])){
			die ("Field Status Purchase must be filled in!! ");
		}
		else{
			$purchase = $_POST['purchase'];
			$value = implode(",",$purchase);
			$data = array(
					'id' => $this->input->post('id'),
					'name' => $this->input->post('name'),
					'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					'nick_name' => $this->input->post('nick_name'),
					'phone' => $this->input->post('phone'),
					'email' => $this->input->post('email'),
					'sign_head' => $this->input->post('sign_head'),
					'email_head' => $this->input->post('email_head'),
					'email_cc' => $this->input->post('email_cc'),
					'id_group' => $this->input->post('id_group'),
					'id_company' => $this->input->post('id_company'),
					'branch_id' => $this->input->post('branch_id'),
					//'id_divisi' => $this->input->post('id_divisi'),
					'id_dept' => $this->input->post('id_dept'),
					'aproval_flag' => $this->input->post('aproval_flag'),
					'give_righ_flag' => $this->input->post('give_righ_flag'),
					'status_pur' => $value,
					'status' => '1',
				);
			$this->M_user->update_user(array('id' => $this->input->post('id')), $data);
		  }
		   echo 'Insert';
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);
				$this->db->where('id', $id);
				$this->db->update('tbl_user', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }



}
