<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<style>
 .outer {
    width: 210px;
    color: black;
    border: 2px solid Gainsboro;
    padding: 5px;
 }
</style> 


									<div>
									<button class="btn bg-purple btn-flat" onclick="add_user()"><i class="fa fa-plus-circle"></i> Add Data</button>
									<button onclick="delete_user()" id="deleteTriger" name="deleteTriger" class="btn bg-maroon btn-flat" disabled><i class="fa fa-trash"></i> Delete Data</button>
									<button onclick="edit_user()" id="editTriger"  name="editTriger"class="btn bg-maroon btn-flat" disabled><i class="fa fa-edit"></i> Edit Data</button>
									</div>
                                    <br/>
									<div class="row">
									<div class="col-xs-12" style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">                               
	                                <table  class="table table-striped table-bordered table-hover" id="myTable" >
	                                    <thead class="text-warning">
											<th width="5%" style="text-align:center">
												<label class="pos-rel">
                                                    <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
                                                    <span class="lbl"></span>
                                                </label>
                                            </th>
											<th>No</th>
	                                        <th>Id</th>
											<th>Name</th>
											<th>Username</th>
											<th>Group</th>
											<th>Company</th>
											<th>Branch</th>
											<th>Divisi</th>
											<th>Dept</th>
											<th>Email</th>
											<th>Email Head</th>
											<th>Phone</th>
											<th>Sign Head</th>
	                                    </thead>
                                     </table> 
      </div>                               
</div>      
  
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-80p">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">×</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    User Form
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body" style="padding-top:10px">       
			 <form action="#" id="form" class="form-horizontal">

        <div class="form-group form-group-sm">
            <!-- left column -->
            <div class="col-sm-6">
			
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">ID</label>
					<div class="col-sm-8">
					<?php
					$totaldata = $total[0]->totaldata+1;
					?>
					<input name="id" placeholder="Id" class="form-control" type="text" readonly value="<?php echo $totaldata ?>">
					</div>
				</div>
				
                <div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Full Name</label>
				  <div class="col-sm-8">
					<input name="name" placeholder="BRIAN" class="form-control" type="text">
				  </div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">First</label>
				  <div class="col-sm-3">
					<input name="first_name" placeholder="BRIAN" class="form-control" type="text">
				  </div>
				  <label class="control-label col-sm-1" style="padding-right:10px">Last</label>
				  <div class="col-sm-4">
					<input name="last_name" placeholder="BRIAN" class="form-control" type="text">
				  </div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Nick Name</label>
				  <div class="col-sm-8">
					<input name="nick_name" placeholder="BRIAN" class="form-control" type="text">
				  </div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Username</label>
				  <div class="col-sm-8">
					<input name="username" id="username" placeholder="brian123" class="form-control" type="text">
				  </div>
				</div>

				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Phone</label>
				  <div class="col-sm-8">
					<input name="phone" placeholder="08119955599" class="form-control" type="text">
				  </div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Email User</label>
				  <div class="col-sm-8">
					<input name="email" placeholder="brian.yunanda@eurokars.co.id" class="form-control" type="text">
				  </div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Head</label>
				  <div class="col-sm-8">
					<input name="sign_head" placeholder="Ahmad Zulfikar" class="form-control" type="text">
				  </div>
				</div>
				
            </div>
		
            <!-- right column -->
            <div class="col-sm-6">
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Email Head</label>
				  <div class="col-sm-8">
					<input name="email_head" placeholder="ahmad.zulfikar@eurokars.co.id" class="form-control" type="text">
				  </div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Email CC</label>
				  <div class="col-sm-8">
					<input name="email_cc" placeholder="itteam@eurokars.co.id" class="form-control" type="text">
				  </div>
				</div>
				
                <div class="form-group">
					<label class="col-sm-3 control-label" style="padding-right:10px">User Group</label>
					<div class="col-sm-8">
                        <select class="form-control" name="id_group" id="id_group">
						<option  value="">Choose Group</option> 
						<?php foreach($group as $row) { ?>
						<option value="<?php echo $row->id_group;?>"><?php echo $row->name_grp;?>
						</option>
						<?php } ?>				
						</select>    
                    </div>
                </div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Company</label>
					<div class="col-sm-8">
						<select class="form-control" name="id_company" id="id_company">
						<?php foreach($company as $row) { ?>
						<option value="<?php echo $row->id_company;?>"><?php echo $row->company;?>
						</option>
						<?php } ?>				
						</select>    
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Branch</label>
					<div class="col-sm-8">
					   <select class="form-control" name="branch_id" id="branch_id">
						<option  value="">Choose Branch</option>
						<?php foreach($company2 as $bbb) { 
						$sqlpo	  = "select * from tbl_branches where id_company='".$bbb->id_company."'";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo 	  = $qrypo->result();					
						foreach($adapo as $aaa) { ?>
						<option value="<?php echo $aaa->branch_id;?>"><?php echo $aaa->name_branch;?>
						</option>
						<?php } }?>	
						</select>    
					</div>
				</div>
                
                 <div class="form-group">
                    <label class="col-sm-3 control-label" style="padding-right:10px">Status App</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="aproval_flag" id="aproval_flag"  onchange="aproval()">
							<option  value="99">Choose Status</option>
                            <option value="1" style="color:#F53D6B">Presdir--ALL DIV</option>
							<option value="2" style="color:#F53D6B">F.C--ALL DIV</option>
                            <option value="4" style="color:#F53D6B">B.O.D--PER DIV</option>
							<option value="3">Head</option>
                            <option value="0">User</option>
                        </select>
                    </div>
                </div>
			
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Group Divisi</label>
					<div class="col-sm-8">
					   <select class="form-control" name="id_divisi" id="id_divisi" onchange="div()">
						<option  value="">Select Divisi</option> 
						<?php foreach($divisi as $abc) { ?>
						<option value="<?php echo $abc->id_divisi;?>"><?php echo $abc->divisi;?>
						</option>
						<?php } ?>				
						</select>    
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Department</label>
					<div class="col-sm-8">
					   <select class="form-control" name="id_dept" id="id_dept">
						<option value="">Select Department</option>
						<?php foreach($divisi2 as $bbb) { 
						$sqlpo	  = "select * from tbl_dept where id_divisi='".$bbb->id_divisi."'";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo 	  = $qrypo->result();					
						foreach($adapo as $aaal) { ?>
						<option value="<?php echo $aaal->id_dept;?>"><?php echo $aaal->dept;?>
						</option>
						<?php } }?>		
						</select>    
					</div>
				</div>
			
               

				 <div class="form-group">
                    <label class="col-sm-3 control-label" style="padding-right:10px">Status Level</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="give_righ_flag" id="give_righ_flag">
							<option  value="99">Choose Level</option>
                            <option value="1">User</option>
                            <option value="2">Manager</option>
							<option value="3">Administrator</option>
                        </select>
                    </div>
                </div>
				
                </div>
		
				<div class="col-sm-10">
				<div class="form-group">
				<label class="col-sm-2 control-label" style="padding-right:10px">Status Purchase</label>
					<div class="col-sm-3 outer" id="checkboxlist">
						<input type="checkbox" id="purchase" name="sample" class="selectall"> All
						<?php 
						$arrSelVedTask = array();
						foreach ($purchase as $row) { 
						if(isset($arrSelVedTask[$row->id_flagpur])) {
                      ?>  <input type="checkbox" id="purchase" name="purchase[]" value="<?php echo $row->id_flagpur ;?>" >  <?php echo $row->type_purchase; ?><?php
                  }
                  else{
                      ?>  <input type="checkbox" id="purchase" name="purchase[]" value="<?php echo $row->id_flagpur ;?>" >  <?php echo $row->type_purchase; ?><?php
                  }
               }  
                  ?> 
					</div>
				</div>
				</div>
				</fieldset>
                <!-- End contact information -->
            </div>
        
      <!-- Modal Footer -->
            <div class="modal-footer">
			<button type="button" class="btn btn-warning" onclick="reset_pass()"><i></i>Reset Password</button>
            <button type="button" id="btnSave" name="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div><!-- End Modal Footer -->
    </form>
		</div> <!-- End modal body div -->
      </div> <!-- End modal content div -->
  </div> <!-- End modal dialog div -->
</div> <!-- End modal div -->
<script>
$('.selectall').click(function() {
    if ($(this).is(':checked')) {
        $('div input').attr('checked', true);
    } else {
        $('div input').attr('checked', false);
    }
});
</script>

<script type="text/javascript"> 
        $("#id_divisi").on('change', function(){
			   var app = document.getElementById("aproval_flag").value;
               var id_divisi = {id_divisi:$("#id_divisi").val()};
                   $.ajax({
               type: "POST",
               url : "<?php echo site_url('user/C_user/ajax_divisi/') ?>",
               data: id_divisi,
               success: function(msg){
               //$('#id_dept').html(msg);
               }
            });
         });
</script>
<script type="text/javascript">
   function aproval() {
	var app = document.getElementById("aproval_flag").value;
	if (app == 1){
		val_divisi = 7;
		val_dept = 17;
	}else if (app == 2){
		val_divisi = 8 ;
		val_dept = 18;
	}
        document.getElementById("id_divisi").value=val_divisi;
		document.getElementById("id_dept").value=val_dept;
}
</script>

<script type="text/javascript">
  function div() {
	var id_divisi = document.getElementById("id_divisi").value;
	var app = document.getElementById("aproval_flag").value;
	if (app == 4){
		if (id_divisi == 1){
		  val_dept = 19;
		}else if (id_divisi == 2){
			val_dept = 20;
		}else if (id_divisi == 4){
			val_dept = 21;
		}else if (id_divisi == 5){
			val_dept = 22;
		}
		document.getElementById("id_divisi").value=id_divisi;
		document.getElementById("id_dept").value=val_dept;
	}else if (app != 4) {
		var id_divisi = {id_divisi:$("#id_divisi").val()};
		$.ajax({
            url : "<?php echo site_url('user/C_user/ajax_divisi/') ?>",
            type: "POST",
            data: id_divisi,									
			success: function (msg)            
            {
               $('#id_dept').html(msg);
			}
		});
	}
		

}
</script>

<script>


//$('#myTable').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
var tableUsers = $('#myTable').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		"dom": 'lfBrtip',
		"buttons": [
            {
		   "extend":    'copy',
		   "text":      '<i class="fa fa-files-o"></i> Copy',
		   "titleAttr": 'Copy'
		   },
		   {
		   "extend":    'print',
		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
		   "titleAttr": 'Print',
		   "orientation": 'landscape',
           "pageSize": 'LEGAL'
		   },
		   {
		   "extend":    'excel',
		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
		   "titleAttr": 'Excel'
		   },
		   {
		   "extend":    'pdf',
		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
		   "titleAttr": 'PDF',
		   "orientation": 'landscape',
           "pageSize": 'LEGAL'
		   }
        ],
		"autoWidth" : false,
		"scrollY" : '250',
		"scrollX" : true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('user/C_user/ajax_list') ?>",
            "type": "POST"
        },
		'columnDefs': [
		{
        'targets': [0],
		'orderable': false,
		}
   ],

      'order': [[2, 'desc']],
        });
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>


<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>

<script>
        	$(document).ready(function(){
	            $("#id_company").change(function (){
	                var url = "<?php echo site_url('user/C_user/pos_branch');?>/"+$(this).val();
	                $('#branch_id').load(url);
	                return false;
	            })
	        });
    	</script>

	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
    function add_user()
    {
      save_method = 'add';
      $('#modal_form').modal('show');; // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
	
	
	function reset_pass()
	{
		var url;
		if(save_method == 'update')
		{
          url = "<?php echo site_url('user/C_user/reset_pass')?>";
		}
		else
		{
		  alert("Just One Allowed Data!!!");
		}
		  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
		
	}
	
	function edit_user()
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('user/C_user/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {
			var aaa = data.status_pur.split(",");
			$.each(aaa, function(i, item) {
			if (item == "1") {
			$("#purchase[value='1']" ).prop("checked", true);
			} 
			if (item == "2") {
			$("#purchase[value='2']").prop("checked", true);
			} 
			if (item == "3") {
			$("#purchase[value='3']").prop("checked", true);
			} 
			if (item == "4") {
			$("#purchase[value='4']").prop("checked", true);
			} 
			if (item == "5") {
			$("#purchase[value='5']").prop("checked", true);
			}
			
			});
            $('[name="id"]').val(data.id);
            $('[name="name"]').val(data.name);
			$('[name="first_name"]').val(data.first_name);
			$('[name="last_name"]').val(data.last_name);
			$('[name="nick_name"]').val(data.nick_name);
			$('[name="username"]').val(data.username);
			$('[name="phone"]').val(data.phone);
			$('[name="email"]').val(data.email);
			$('[name="sign_head"]').val(data.sign_head);
			$('[name="email_head"]').val(data.email_head);
			$('[name="email_cc"]').val(data.email_cc)
			$('[name="id_group"]').val(data.id_group);
			$('[name="id_company"]').val(data.id_company);
			$('[name="branch_id"]').val(data.branch_id);
			$('[name="aproval_flag"]').val(data.aproval_flag);
			$('[name="id_divisi"]').val(data.id_divisi);
			$('[name="id_dept"]').val(data.id_dept);
			$('[name="give_righ_flag"]').val(data.give_righ_flag);
			$('[name="purchase"]').val(data.purchase);
			
            $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

     function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('user/C_user/add_user')?>";
      }
      else
      {
		   url = "<?php echo site_url('user/C_user/update_user')?>";
      }
		  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }

     function delete_user()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('user/C_user/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
			
		

      
    }
	
  </script>

  

</html>
