<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_table_aproval extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('m_table_aproval/M_table_aproval','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();							
	}		  	
		
  public function index()
  {					    	
  		 
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_aproval= $this->M_table_aproval->tampil_add_pp()->result();	
	  $total_rows =$this->M_table_aproval->tampil_add_pp()->num_rows();
	  									
	  if ($tampil_table_aproval)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(4);
		$per_page =10;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'c_table_aproval/c_table_aproval/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 4;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['pp_view'] =$this->M_table_aproval->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
		
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;		
		$data['intno'] = ""; //variable buat looping no table.				
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		$data['ceck_row'] = $total_rows;	
		$data['tampil_company']=$this->M_table_aproval->get_company()->result();																															
		$data['tampil_dept']=$this->M_table_aproval->get_dept()->result();																															
		$data['show_view'] = 'v_table_aproval/v_table_aproval';		
		$this->load->view('template_admin',$data);				
	  }else{	
	    $dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;				
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	    $data['ceck_row'] = $total_rows;	
		$data['tampil_company']=$this->M_table_aproval->get_company()->result();																															
		$data['tampil_dept']=$this->M_table_aproval->get_dept()->result();																														
		$data['pesan'] = 'Data PP aproval table is empty';				
		$data['show_view'] = 'v_table_aproval/v_table_aproval';
		$this->load->view('template_admin',$data);				 
	  } 
  }
  
  public function do_aproval()	
  {
		if ($this->M_table_aproval->give_aproval())
		{	 	
			$this->session->set_flashdata('pesan_aproval','1');	
		
		    if ($this->session->userdata('aproval_flag')=="2") {	
			    $this->kirim_email_to_bod();
			}
		
			$this->kirim_email_aprove(); // jika di aprove bod						
			redirect('c_table_aproval/c_table_aproval'); //riderct ke menu utama  
			
			//print_r($this->session->userdata("result_id_master"));	
			
		}else{
			$this->session->set_flashdata('pesan_aproval','0');		
			redirect('c_table_aproval/c_table_aproval');   
		}
  }
  
  public function do_rejected()	
  {
		if ($this->M_table_aproval->give_rejected())
		{	 	
			$this->session->set_flashdata('pesan_reject','1');	
			$this->kirim_email_reject();		
			redirect('c_table_aproval/c_table_aproval');   
		}else{
			$this->session->set_flashdata('pesan_reject','0');		
			redirect('c_table_aproval/c_table_aproval');     
		}
  }
  
  
   public function do_search_pp()	
  {		
  		 $tampil_table_aproval= $this->M_table_aproval->get_search()->result();	
	     $total_rows =$this->M_table_aproval->tampil_add_pp()->num_rows();
  	
		if ($tampil_table_aproval)
		{	 	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;					
			$data['intno'] = ""; //variable buat looping no table.	
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				
			$data['ceck_row'] = $total_rows;	
			$data['pp_view'] =  $tampil_table_aproval;
			$data['tampil_company']=$this->M_table_aproval->get_company()->result();
			$data['tampil_dept']=$this->M_table_aproval->get_dept()->result();																																
			$data['show_view'] = 'v_table_aproval/v_table_aproval';		
			$this->load->view('template_admin',$data);				
	  }else{	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;	
			$data['intno'] = ""; //variable buat looping no table.					
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;	
			$data['pp_view'] =  $tampil_table_aproval;
			$data['tampil_company']=$this->M_table_aproval->get_company()->result();	
			$data['tampil_dept']=$this->M_table_aproval->get_dept()->result();																															
			$data['pesan'] = 'Data PP aproval table is empty';				
			$data['show_view'] = 'v_table_aproval/v_table_aproval';
			$this->load->view('template_admin',$data);				
	  } 
		
  }
  
   
  public function multiple_submit() //fungsi submit dengan banyak button dalam satu form
  {  
	
		if ($this->input->post('btnaprove')){	
			$this->do_aproval();
		}else{
			if ($this->input->post('btnreject')){	//dari modal popup
				$this->do_rejected();							
			}else{
				if ($this->input->post('btncari')){	
					$this->do_search_pp();
				}else{		
					redirect('c_table_aproval/c_table_aproval');
				}
			}
		}
	

	}

//------------------------------------pashing result to modal popup-------------------
	public function get_nopp_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //       
	     $query_master = $this->db->query("select id_master,remarks,gran_total,currency from qv_head_pp_complite where id_master ='".$nopp."'");  
         $query_detail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   
		   
		      $data['str_pp_master']=$query_master->result();
			  $data['str_pp_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_nopp'] = $nopp ;
			  $tampil_detail = $this->load->view('v_table_aproval/v_conten_detail',$data,true);
			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
						

public function kirim_email_reject(){          
 		   
		   $this->load->library('email');				 								   		   		  	        
		   //konfigurasi email		
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;		
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.gmail.com';
		   
		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   $config['smtp_port']  = '587';		  		  
		   //$config['smtp_port']  = '465'; portgmail
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   
		   //$config['smtp_user']= "epurchaseeuro@gmail.com"; //Gmail
		   //$config['smtp_pass']= "#purchas3"; 
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5"; 
		   //--------------------------------		   		  
		    		   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			$str_status = '1' ; //aktif ;
						
			if ($strponumber != '') :						  	
				//Select untuk email master PP----------------------------
				$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,gran_total,remarks,status_send_aprove,aprove_bod,reason_reject,email_pp,email_head_pp,email_cc_pp from qv_head_pp_complite where id_master ='".$strponumber."' and status ='".$str_status."'");				
			  if ($query->num_rows() > 0 ) :
			      $data_email['str_pp_master'] = $query->result();		
				  foreach ($query->result() as $row_ceck) :	
				     //simpan row alamat email kedalam variable..
					 	$struseremail = $row_ceck->email_pp ; //addrees for to email
						$strheademail = $row_ceck->email_head_pp; //addrees for to email
						$strccemail = $row_ceck->email_cc_pp ; //addrees for to email
					 //end-----------------------------------------	
					 		   
				     //Select untuk email deteail PP--------------------------------
				     $query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."' and status ='".$str_status."'");		
				     if ($query_detail->num_rows() > 0 ) :										 
					    $data_email['str_pp_detail'] = $query_detail->result();	
					    $data_email['intno'] = ""; //untuk counter angka table																									
					    $message = $this->load->view('v_table_aproval/v_content_email_aproval',$data_email,true);														
					  endif;
					  //end-----------------------------------------------------------
						  if ($row_ceck->status_send_aprove =="-1")	:					  														  							 															
								$subject = "PP REJECTED-- Eurokargroup Company";		
								$result = $this->email ;							   
								$this->email->initialize($config);  
								$this->email->set_newline("\r\n"); 
							   
								//Exchange
								//konfigurasi pengiriman					
								
								$sender_email = "helpdesk@eurokars.co.id"; 					   
								$sender_name = "Epurchasing Notification";					   					    
								
								//GMAIL-----
								//$sender_email = "epurchaseeuro@gmail.com";
								//$sender_name = "Epurchasing Eurokarsgroup";					   					    																						   
								//$to = 'brian.yunanda@eurokars.co.id';
								
							$to = $struseremail.",".$strheademail.",".$strccemail; //email address,user,head, and CC
								 
								$this->email->from($sender_email, $sender_name);
								$this->email->to($to);
								$this->email->subject($subject);  
								
								//$this->email->message(print_r($message, true));			   
							  
								$this->email->message($message);// tanpa array	
																		 
								if ($this->email->send()) :		  	
									$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
									//redirect(current_url());
								//else:
								//	show_error($this->email->print_debugger());
								endif; 										   						   
						      endif;
					      endforeach;							    
			         endif;
			     endif;				 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
			 
	      
 }
 
 public function kirim_email_aprove(){          
 		   
		   $this->load->library('email');				 								   		   		  	        
		   //konfigurasi email		
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;		
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.gmail.com';
		   
		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   $config['smtp_port']  = '587';		  		  
		   //$config['smtp_port']  = '465'; port gmail
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   
		   //$config['smtp_user']= "epurchaseeuro@gmail.com"; //Gmail
		   //$config['smtp_pass']= "#purchas3"; 
		    $config['smtp_user']= "helpdesk@eurokars.co.id";
		    $config['smtp_pass']= "#ur0kar5"; 
		   //--------------------------------		   		  
		    		   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			$str_status = '1' ; //aktif ;
			
			if ($strponumber != '') :						  	
				//Select untuk email master PP----------------------------
				$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,gran_total,remarks,status_send_aprove,aprove_bod,reason_reject,email_pp,email_head_pp,email_cc_pp from qv_head_pp_complite where id_master ='".$strponumber."' and status ='".$str_status."'");				
			  if ($query->num_rows() > 0 ) :
			      $data_email['str_pp_master'] = $query->result();		
				  foreach ($query->result() as $row_ceck) :	
				  			   				     	
					 //simpan row alamat email kedalam variable..
					 	$struseremail = $row_ceck->email_pp ;
						$strheademail = $row_ceck->email_head_pp;
						$strccemail = $row_ceck->email_cc_pp ;
					 //end-----------------------------------------
					
				     //Select untuk email detail PP--------------------------------
				     $query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."' and status ='".$str_status."'");		
				     if ($query_detail->num_rows() > 0 ) :					 																					
					    $data_email['str_pp_detail'] = $query_detail->result();	
					    $data_email['intno'] = ""; //untuk counter angka table																									
					    $message = $this->load->view('v_table_aproval/v_content_email_aproval',$data_email,true);														
					  endif;				
					  //end---------------------------------------------------------
					  		
							 if ($row_ceck->status_send_aprove =="1" and $row_ceck->aprove_bod == "1")	:										 																									
									$subject = "PP APPROVED -- Eurokargroup Company";	
									$result = $this->email ;							   
									$this->email->initialize($config);  
									$this->email->set_newline("\r\n"); 
								   
									//Exchange
									//konfigurasi pengiriman					
									
									$sender_email = "helpdesk@eurokars.co.id"; 					   
									$sender_name = "Epurchasing Notification";					   					    
									
									//GMAIL-----
									//$sender_email = "epurchaseeuro@gmail.com";
									//$sender_name = "Epurchasing Eurokarsgroup";					   					    								//end--------------------------
									
									//simpan session alamat email kedalam variable..
									
								   //end-----------------------------------------
								   
									//$to = 'brian.yunanda@eurokars.co.id';
									
									$to = $struseremail.",".$strheademail.",".$strccemail; //email address,user,head, and CC
									 
									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);  
									
									//$this->email->message(print_r($message, true));			   
								  
									$this->email->message($message);// tanpa array	
																			 
									if ($this->email->send()) :		  	
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
										//redirect(current_url());
									//else:
									//	show_error($this->email->print_debugger());
									endif;
								 endif;						    
					      endforeach;							    
			         endif;
			     endif;				 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
			 
	      
 }
 
 public function kirim_email_to_bod(){          
 		   
		   $this->load->library('email');				 								   		   		  	        
		   //konfigurasi email		
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;		
		   //------google
		  // $config['smtp_host'] = 'ssl://smtp.gmail.com';
		   
		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   $config['smtp_port']  = '587';		  		  
		   //$config['smtp_port']  = '465';
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   
		   //$config['smtp_user']= "epurchaseeuro@gmail.com"; //Gmail
		   //$config['smtp_pass']= "#purchas3"; 
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5"; 
		   //--------------------------------		   		  
		    		   		 		          				   				     		   				  									
		  
		  $strppnumber =$this->session->userdata("result_id_master");//session array 
		  	
		  for ($intppno=0; $intppno < count($strppnumber) ; $intppno++) :
		  
		      $query =$this->db->query("select id_master,company,short,dept,user_submission,header_desc,vendor,term_top,currency,gran_total,date_send_aproval from qv_head_pp_complite where id_master ='".$strppnumber[$intppno]."'");				
			  
			  $buffresult_email_to_bod[] = $query->result();
			  $data_email['data_email_to_bod'] = $buffresult_email_to_bod;			  
			 			
		   endfor;
		   
		  // print_r($data_email); //tampilin hasil array di page.		   
		   $message = $this->load->view('v_table_aproval/v_content_email_to_bod',$data_email,true);														
					 			
					  //end---------------------------------------------------------
					  									
									//Exchange
									//konfigurasi pengiriman					
									
									$sender_email = "helpdesk@eurokars.co.id"; 					   
									$sender_name = "Epurchasing Notification";		
									//----------
												   					    
									$result = $this->email ;							   
									$this->email->initialize($config);  
									$this->email->set_newline("\r\n"); 
									
									//GMAIL-----
									//$sender_email = "epurchaseeuro@gmail.com";
									//$sender_name = "Epurchasing Eurokarsgroup";					   					    								//end--------------------------
																			
									$to = 'Tasya@eurokars.co.id,Yeni@eurokars.co.id' ;																	   
									//$to = 'brian.yunanda@eurokars.co.id';
									$subject = 'Submission Final Approval To B.O.D';
									//$to = $struseremail.",".$strheademail.",".$strccemail; //email address,user,head, and CC
									 
									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);  
									
									//$this->email->message(print_r($message, true));			   
								  
									$this->email->message($message);// tanpa array	
																			 
									if ($this->email->send()) :		  	
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
										//redirect(current_url());
									//else:
									//	show_error($this->email->print_debugger());
									endif;												    						 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
			 
	      
 }

	 		
}

	
