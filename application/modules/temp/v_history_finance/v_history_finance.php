<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>
        
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);;color:#B66F03; font-size:16px;" ><?php echo "History F.A & Print BPK"." | ".$header  ; ?> </div>
   <div class="panel-body">               
   
          
 <?php  echo form_open('c_history_finance/c_history_finance/multiple_submit');  ?>     
 
 <table>
     <tr>             	              
       <td colspan="3"><label >Date Aproval: &nbsp; </label></td>
       <td>&nbsp;</td>
       <td>&nbsp;</td>       	       
       <td>                  
          <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
            <input id="txtdatestart" name="txtdatestart" readonly="readonly" type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />          
           </div>                            
       </td>                                       
       <td><label >&nbsp; S/D &nbsp; </label></td>                                       	
       <td>
        
         <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
          <div class="input-group">  
            <input id="txtdateend" name="txtdateend" readonly="readonly" type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />  
             <div class="input-group-btn">
              <input id="btncaridate" name="btncaridate" type="submit" value="Search date"  class="btn btn-primary" />  
              </div>
          </div>                
       </div>  
       </td>
       <td>&nbsp;</td>             
       <td>&nbsp;</td>
       <td><label >&nbsp; Select : &nbsp; </label></td>                                       
       <td>
       <select name="cbostatus" id="cbostatus" class="form-control">             
             <option value="id_master">PP NO</option>
             <option value="header_desc">Description</option>
             <option value="vendor">vendor</option>   
             <option value="gran_total">Grand Total</option>            
       </select> 
       </td>
       <td>&nbsp;</td>
       <td>&nbsp;</td>  
       <td>
       	 <div class="input-group">        
             <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari" > 
            <div class="input-group-btn">                                                     
            <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-primary" />              
            </div>
          </div> 
       </td>             
       <td>&nbsp;</td>	  
      <td>
      <input id="btnclearadjust" name="btnclearadjust" type="submit" value="Clear Adjustmen" class="btn btn-danger btnclear" disabled="disabled" />
      </td>	  
       <td>&nbsp;</td>	     
      </tr> 
    </table>   
 <br />
 
<div class="row">
 <div class="col-md-12 col-md-offset-0">   
	<?php 
	  echo '<table width="100%" class="table-striped table-bordered table-hover table-condensed"  >';		
	  echo '<tr style="background:url('.base_url("asset/images/child-panel-bg.png").');font-weight:bold;font-size: 12px">';		
	  echo '<td width="10%">PP No</td>';
	  echo '<td width="1%">Company</td>';	
	  echo '<td width="2%">Dept</td>';						
	  echo '<td width="10%">Vendor</td>';			
	  echo '<td width="15%">Description</td>' ;		
	  echo '<td width="7%">Date Approval</td>' ;
	  echo '<td width="8%">Date Subbmision</td>' ;	 
	  echo '<td width="3%">Cur</td>' ;
	  echo '<td width="9%">Total Cost</td>' ;
	  echo '<td width="3%">Term</td>' ;
	  echo '<td width="8%">Subbmision</td>' ;					
	  echo '<td width="7%" align="center">Quo</td>';
	  echo '<td width="7%" align="center">View Detail</td>';	
	  echo '<td width="7%" align="center">Print</td>';		
	  echo '<td width="7%" align="center">Action</td>';		
	  echo '</tr>' ;		    		
		
		if ($ceck_row >= 1) {		  
			  foreach($pp_view as $row){ $intno++			   			 
     ?>            
		 <?php if ($row->flag_bpk == 1) : ?>                          
                 <tr style="background:#d14743;font-size:12px;color:#FFF;" >
         <?php else:	?>
                 <tr style="font-size: 12px" >
         <?php endif?>                               
                <td width="10%"><?php echo $row->id_master ; ?></td>
                <td><?php echo $row->short ; ?></td>									
                <td><?php echo $row->dept ; ?></td>									
                <td><?php echo $row->vendor ; ?></td>                       
                <td><?php echo $row->header_desc ; ?></td>             
				<td><?php echo date('d-m-Y', strtotime($row->date_aproval)) ; ?></td>	
                <td><?php echo date('d-m-Y', strtotime($row->date_send_aproval)) ; ?></td>                
                <td><?php echo $row->currency ;?></td>
                <?php if ($row->flag_bpk == 1) : ?>
                     <td align="right" style="color:#FFF"><?php echo number_format($row->gran_total,2,'.',',') ;?></td>
                <?php else: ?>	 
                  <td align="right" style="color:#EB293D"><?php echo number_format($row->gran_total,2,'.',',') ;?></td>
                <?php endif; ?>	       
                <td align="center"><?php echo $row->term_top ;?></td>
                <td><?php echo $row->user_submission ;?></td>  
                <td align="center">
				   <?php 
				    If ($row->attach_quo != "")
					  {  
					   echo'<a href='.base_url($row->attach_quo).' class="btn btn-success"  style="text-decoration:none">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".'View'.'</a>' ;
					  }else{
						echo '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
					  }
					?>
                 </td>                                  
                <td align="center">
                	<a href="#" class="btn btn-warning btn-setting print" id="print" style="text-decoration:none" req_id="<?php echo $row->id_master ; ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>Detail</a>
                </td> 
       		   <td>          
      <?php if ($row->flag_bpk == 1) : ?>
               <div style=" font-family:Arial;color:#FFF;" align="center">From Create BPK</div>
      <?php else: ?>	            
                <a href="#" class="btn btn-primary btn-setting bpkprint" id="reject" style="text-decoration:none" req_id="<?php echo $row->id_master ; ?>" req_gtotal="<?php echo $row->gran_total ; ?>" req_gadjust="<?php echo $row->gran_total_adjust ; ?>" req_note="<?php echo $row->note_bpk ; ?>"><span class="glyphicon glyphicon-print" aria-hidden="true"></span>BPK Print</a>
       <?php endif; ?>	  
            </td>             
            <td>
             <?php if($row->gran_total_adjust != "" ) : ?>                
                <div align="center"><input  name="msg[]" type="checkbox" value="<?php echo $row->id_master; ?>"/>
             <?php else: ?>	  
               <?php if ($row->flag_bpk == 1) : ?>              
                 <div style=" font-family:Arial;color:#FFF;" align="center">BPK Input</div>               
               <?php else: ?>
                 <div style=" font-family:Arial;color:#F00;" align="center">Nothing Adjustment</div>               
               <?php endif; ?>	                      
             <?php endif; ?>	                     
            </td>                         
		 </tr>           
		 <?php 
		  		}
		 	}
   echo'</table>';	  				
		 ?> 
		 
		<?php		   			
		     if (! empty($pagination))			
			 { 
		  	   echo '<table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table>';				 
			 }			  	  						
		?>
        
 </div>  
</div> 
                
   </div>
  </div>
 		 
</div>

<!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Info & Remarks</h4>
                    </div>
                    
                    <div class="modal-body">
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->            
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal -->  	

<!-- Modal Buat Note BPK -->

  <div class="modal fade" id="myModal-bpkprint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Note And Adjustment BPK</h4>
                    </div>
                    
               <div class="modal-body-bpkprint">
                      <table width="90%" class="table-condensed" align="center">
                        <tbody > 
                          
                            <tr> 
                                <td> <label for="PP Number"  class="col-xs-10">PP Number</label></td> 
                             
                                  <td>
                                   <div class="col-xs-10">
                                    <input type="text" id="txtppno" name="txtppno" class="form-control btn-success" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                             <tr> 
                                <td> <label for="Grand Total"  class="col-xs-11">Grand Total</label></td> 
                             
                                  <td>
                                   <div class="col-xs-10" >
                                    <input type="text" id="txtgtotal" name="txtgtotal" class="form-control btn-danger gtotal_static" onkeypress="return isNumberKey(event)" readonly="readonly" >
                                   </div> 
                                  </td>                                  
                             </tr>                                                       
                            
                                  <tr>                              
                     <td><label for="Grand Total"  class="col-xs-11">Grand Total Adjust</label></td>
                       <td>
                            <div class="col-xs-10">
                                    <input type="text" id="txtcathadjust" name="txtcathadjust" placeholder="0.00" class="form-control btn-warning cadjust" onkeypress="return isNumberKey(event)" readonly="readonly" >
                                  
                           </div> 
                           
                           <div class="col-xs-1" id="imgsucces">
                           	  <img src="<?php echo base_url("asset/images/success_ico.png") ?>">
                           </div>
                       </td>                   
                   </tr>                  
                       
                     <tr id="note_row"> 
                        <td ><label for="bpk note"  class="col-xs-9">Note</label></td> 
                        <td>
                          <div class="col-xs-12">
                            <textarea id="txtnote" name="txtnote"  class="form-control"></textarea>
                          </div>  
                        </td>    
                     </tr>  
            <tr id ="tdinfo">                        
              <td  style="color:#F00" ><label class="col-xs-9">Information :</label></td>
              <td  style="color:#F00" ><label class="col-xs-9">Grand Total Adjustment Already Exsist</label></td>
            </tr>            
                  </tbody>
                 </table>                                  
           
           <div class="panel-body">                               
                <table class="table-bordered"  align="center" id="table_detail_adjust">                                 
                     <tr align="center" class="btn-default" >
                            <td width="20%">Description</td>
                            <td width="27%">Value Adjust </td>
                     </tr>                            
                  <tbody class="detail">                         
                      <tr>
                        <td>                    
                        <input type="text" id="flagstat" name="flagstat[]"  value="1"  hidden="true"  />
                        <input type="text" maxlength="70" id="txtdesc1"  name="txtdesc[]"  placeholder="Description" size="24" class="form-control txtdesc" />
                        </td>                
                        <td>
                         <input maxlength="14" id="txtprice1"  name="txtprice[]" type="text" placeholder="0.00" size="20" onkeypress="return isNumberKey(event)" class="form-control priceadjst1"  /> 
                        </td>                     
                     </tr>
                      <tr>
                        <td>                        
                        <input type="text" id="flagstat" name="flagstat[]"  value="1"  hidden="true"  />
                        <input type="text" maxlength="70" id="txtdesc2"  name="txtdesc[]"  placeholder="Description" size="24" class="form-control txtdesc" />
                        </td>                
                        <td>
                         <input maxlength="14" id="txtprice2"  name="txtprice[]" type="text" placeholder="0.00" size="20" onkeypress="return isNumberKey(event)" class="form-control priceadjst2"  /> 
                         </td>                              
                        </tr>
                        
                  <tr>                              
                   <td style="color:#F00"><label for="Grand Total"  class="col-xs-11">Result Adjustment :</label></td>
                       <td>   
                         <input maxlength="14" id="txtgadjust" name="txtgadjust" placeholder="0.00" class="form-control  hasil" onkeypress="return isNumberKey(event)" readonly="readonly"  >

                       </td>                   
                   </tr>   
                          
                    </tbody> 
                 </table>
               </div>         
           </div>          
                  <div class="modal-footer">
                
                   <input id="btnprintbpk" type="submit" name="btnprintbpk" value="Print BPK" class="btn btn-danger btn_dobpkprint"/>   
                   <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                  </div>                  
                    
                </div>
            </div>           
        </div>
    
<!-- Modal Buat Note BPK -->



<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numerala buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 

<script>
$(function() {
  $("#txtcari").focus();
});		  
//set focus ----------------------------  
</script>

<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.print',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("c_history_finance/c_history_finance/get_nopp_modal"); ?>';
		
		$("#myModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});
	
</script>

<script>
$(function(){ // Show modal pop up send noted BPK print
  $(document).on('click','.bpkprint',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref php echo $row->id_master ke variable req_id  ;																	
		var req_gtotal = $(this).attr('req_gtotal'); 
		var req_gtotal_adjust = $(this).attr('req_gadjust'); 
		var req_note = $(this).attr('req_note');
		
		if (req_gtotal_adjust == "") { // jika grand total adjustment ada maka tampilkan information already exsist
		   $('#tdinfo').hide('fast');	
		   $('#imgsucces').hide('fast');		  
		   $('#note_row').show('fast');
		   $('#table_detail_adjust').show('fast');	
		   $('#btnprintbpk').show('fast');
		}else{
		   $('#tdinfo').show('fast');		
		   $('#imgsucces').show('fast');
		   $('#note_row').hide('fast');
		   $('#table_detail_adjust').hide('fast');
		   $('#btnprintbpk').hide('fast');			
		}
		
		$("#myModal-bpkprint").modal('show');
		$('#txtppno').val(req_id); //menampilkan isi dari  idmaster kedalam inputbox 			
		$('#txtgtotal').val(numeral(req_gtotal).format('0,0.00')); //menampilkan isi dari  grantotal kedalam inputbox 							
	    $('#txtcathadjust').val(numeral(req_gtotal_adjust).format('0,0.00'));//  textbox txtcathadjust				
		$('#txtdesc1').val(''); //mengkosongkan description 1		
		$('#txtprice1').val(''); //mengkosongkan adjust 1		  				
		$('#txtdesc2').val(''); //mengkosongkan description 2					  				
		$('#txtprice2').val(''); //mengkosongkan adjust 2				  				
		$('#txtnote').val(req_note); //mengkosongkan note		  				
		$('#txtgadjust').val(''); //mengkosongkan total adjustmen				
	   });																					
});		
</script>



<script>
//setelah di print adjustment cecklist total adjust and hide text box adjust
$(document).on('click','.btn_dobpkprint',function(e){	
  var strgadjust = $('#txtgadjust').val();
  
  if (strgadjust !=''){
	  $('#tdinfo').show('fast');
	  $('#imgsucces').show('fast');
	  $('#note_row').hide('fast');
	  $('#table_detail_adjust').hide('fast');
	  $('#btnprintbpk').hide('fast');	
   }	
	     
});			  

/*$(function(){ // hide modal popup  BPK print
  $(document).on('click','.btn_sub_form',function(e){	      			      	 
		$("#myModal-bpkprint").modal('hide');				 	 					
   });																					
});	*/	

//setelah di hide lalu di refresh page parent
$('#myModal-bpkprint').on('hidden.bs.modal', function () { //refresh page on close modal
  document.location.reload();
});
</script>

<script>
//--------------------function disbaled enabled button with check book
$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btnclear').removeAttr('disabled');			
        }
        else{
            $('.btnclear').attr('disabled', 'disabled');								
        }
    });
});
</script>

<script>

$(function(){
    $('#myform').on('submit', function(e){ //id #myform di buat untuk diferentsiasi dengan form open code igniter
        e.preventDefault();		
		var url = '<?php echo base_url("c_history_finance/c_history_finance/multiple_submit"); ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), // myform di buat untuk diferentsiasi dengan form open code igniter 
            success: function(data){
                // alert($('#txtppno').val() + ',' + $('#txtreject').val())					 				
            }
        });
    });
}); 
</script>

<script>
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	
</script>

<script script type="text/javascript">

    $(document).on('change','#txtprice1','#txtprice2',function(e){   
		   var tr =  $(this).parent().parent();			  
		   var price=numeral().unformat($('#txtprice1').val());						 
		   var price2=numeral().unformat($('#txtprice2').val());		   
		   total(); 
    });
   
    $(document).on('blur','#txtprice1',function(e){   
		   var tr =  $(this).parent().parent();		  
		   
		   var price=$('#txtprice1').val();		  		   		  
		   var price2=$('#txtprice2').val();	
		   				   		  		  
		   if (price != ""){
			   $('#txtprice1').val(numeral(price).format('0,0.00')) ;			  		   		   	  		   			
		   }
		   total(); //masuk ke fungsi total		
    });  	 	
	
	  $(document).on('blur','#txtprice2',function(e){   
		   var tr =  $(this).parent().parent();		  
		   
		   var price=$('#txtprice1').val();		  		   		  
		   var price2=$('#txtprice2').val();	
		 		   		   		  
		   if (price2 != ""){		
			   $('#txtprice2').val(numeral(price2).format('0,0.00')) ;			   		   	  		   			
		   }
		   total(); //masuk ke fungsi total		
    });  	 	


function total()
{
	  
	var hasilprice  = numeral().unformat($('#txtprice1').val());		  		   		  
    var hasilprice2 = numeral().unformat($('#txtprice2').val());			
	var gtstatic = numeral().unformat($('#txtgtotal').val()); //textboxt grandtotal	
	var cathgadjust = numeral().unformat($('#txtcathadjust').val()); //textboxt grandtotal adjust		
	    
	//---------------------------------------------
		
		
	   if (hasilprice =="0" && hasilprice2 =="0"){//jika text boxt price satu dan dua kosong
		   $('.hasil').val('');	  
		   $('#txtprice1').val('');
	       $('#txtprice2').val('');	 
	   }else{
			if	(cathgadjust != "0") {				
				 var amount = hasilprice + hasilprice2 ;	
				 var result_t= cathgadjust + amount ;  									
			}else{ 			  
				 var amount = hasilprice + hasilprice2 ;	
				 var result_t=gtstatic + amount ;  				
			}
			$('.hasil').val(numeral(result_t).format('0,0.00')) ;
	   }
	 
							
}

</script>

