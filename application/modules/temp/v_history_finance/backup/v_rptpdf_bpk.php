<?php
			
	require('mc_table.php');
	$pdf=new PDF_MC_Table('P','cm',"A4");
	$pdf->Open();
	$pdf->AddPage();	
	$pdf->AliasNbPages();
	$pdf->SetMargins(1,1,1);
	$pdf->SetFont('times','B',12);

	$pdf->Image('asset/images/euro.jpg',1,1,4); //tampilan yg pertama di tampilkan report : P,L,B
			//-----------------------------------------------------				
	$pdf->Ln(1); // baris berikut nya pada report denga lebar (line)garis ke bawah sebanyak 15(line) 
	$pdf->setFont('Arial','',10); //Type huruf(font beserta besaran huruf)
	$pdf->setFillColor(255,255,255); //warna pada baris tulisan                								
	$pdf->cell(90,0,"PT.".$this->session->userdata('company'),0,0,'L',1); //tampil session company 				
	$pdf->cell(0,0,"NO : ".$this->session->userdata('sesidmaster'),0,1,'R',1); 				
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',10);
	$pdf->setFillColor(255,255,255);                													   					
	$pdf->cell(0,0,"Finance - Accounting" ,0,0,'L',1); 
	$pdf->cell(0,0,"Date Submission : " .date('d-M-Y',strtotime($this->session->userdata('ses_datesend'))),0,1,'R',1);
	
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','B'.'U',12);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"BUKTI PENGELUARAN KAS / BANK",0,0,'C',1);                
   	
	
	$pdf->Ln(0.7);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"Payment To : ".$this->session->userdata('ses_vendor'),0,0,'L',1);               
	$pdf->cell(0,0,"Date Approval : " .date('d-M-Y',strtotime($this->session->userdata('ses_dateapproval'))),0,1,'R',1);
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"Amount Of Money :"." ". $this->session->userdata('ses_curr')." ".number_format($this->session->userdata('ses_grand')),0,0,'L',1); 
	$pdf->cell(0,0,"Date issuet : " .date('d-M-Y',strtotime($this->session->userdata('ses_datecair'))),0,1,'R',1);
	
	
	$pdf->Ln(0.5);				
	$no = 1;// intuk index petama	
	$pdf->SetFont('Arial','B',9);
	$pdf->SetWidths(array(1, 5, 5, 1,3.7,3.7));
	$pdf->SetHeight(0.1);
	$pdf->Row(array("No", "Description", "Specs", "Qty","Price","Total"));
	
	$pdf->SetFont('Arial','',8);
	foreach ($data as $key) {
		$pdf->Row(array($no,
						$key->desc,
						$key->spec,
						number_format($key->qty),
						number_format($key->harga),
						number_format($key->total)						
				        )
			      );
	     $no++;	 			
	}
	
	$pdf->setFont('Arial','B',10);
	$pdf->setFillColor(255,255,255);               
	$pdf->cell(15.7,0.6,"Sub Total :"." ".$key->currency." ",1,0,'R',1);
	$pdf->cell(3.7,0.6,number_format($key->gran_total),1,1,'R',1);	
	$pdf->cell(19.4,0.6,"Mention:"." "."(".terbilang($key->gran_total).")",1,1,'L',1);
	$pdf->cell(15.7,0.6,"Grand Total Adjustment :"." ".$key->currency." ",1,0,'R',1);
	$pdf->cell(3.7,0.6,number_format($key->gran_total_adjust),1,1,'R',1);
	$pdf->cell(19.4,0.6,"Mention:"." "."(".terbilang($key->gran_total_adjust).")",1,1,'L',1);
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','B'.'I',8);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"Note:"." ".$this->session->userdata('ses_notebpk'),0,1,'L',1); 
	
	$pdf->Ln(0.5);				
	$pdf->setFont('Arial','B',7);
	$pdf->setFillColor(255,255,255);    	
	//$pdf->cell(13.4,0.5,"",0,0,'C',1);  	// buat header jabatan yg menandatangani	
	$pdf->cell(4.1,0.4,"TTD Giro/Cheque"    ,1,0,'C',1);
	$pdf->cell(2.18,0.8,"Pimpinan"         ,1,0,'C',1);
	$pdf->cell(2.18,0.8,"Pemeriksa II "     ,1,0,'C',1);
	$pdf->cell(2.18,0.8,"Pemeriksa I "     ,1,0,'C',1);	    				
	$pdf->cell(2.18,0.8,"Accounting "      ,1,0,'C',1);
	$pdf->cell(2.18,0.8,"Finance "         ,1,0,'C',1);   
	//$pdf->cell(2.18,0.8,"Pemohon "         ,1,0,'C',1);    
	$pdf->cell(2.18,0.8,"Penerima"         ,1,0,'C',1); 
	
	$pdf->Ln(0.4);				
	$pdf->setFont('Arial','B',7);
	$pdf->setFillColor(255,255,255);  		
	$pdf->cell(2.0,0.4,"I",1,0,'C',1);
	$pdf->cell(2.09,0.4,"II",1,1,'C',1);  
	
	
	$pdf->Ln(0.01);				
	$pdf->setFont('Arial','B',7);
	$pdf->setFillColor(255,255,255);    		
	$pdf->cell(2.0,1.5,""   ,1,0,'C',1);
	$pdf->cell(2.1,1.5,""   ,1,0,'C',1);
	$pdf->cell(2.18,1.5,""  ,1,0,'C',1);
	$pdf->cell(2.18,1.5,""  ,1,0,'C',1);
	$pdf->cell(2.18,1.5,""  ,1,0,'C',1);	    				
	$pdf->cell(2.18,1.5,""  ,1,0,'C',1);
	$pdf->cell(2.18,1.5,""  ,1,0,'C',1);   
	//$pdf->cell(2.18,1.5,""  ,1,0,'C',1);    
	$pdf->cell(2.18,1.5,""  ,1,1,'C',1);
	
	$pdf->Ln(0.01);				
	$pdf->setFont('Arial','',6);
	$pdf->setFillColor(255,255,255);  		
	$pdf->cell(2.0,0.5,"",1,0,'C',1);
	$pdf->cell(2.1,0.5,"",1,0,'C',1);
	$pdf->cell(2.18,0.5,$this->session->userdata('sign_bod'),1,0,'C',1);
	$pdf->cell(2.18,0.5,"",1,0,'C',1);
	$pdf->cell(2.18,0.5,"",1,0,'C',1);
	$pdf->cell(2.18,0.5,"",1,0,'C',1);
	$pdf->cell(2.18,0.5,"",1,0,'C',1);
	//$pdf->cell(2.18,0.5,$this->session->userdata('ses_user_sub'),1,0,'C',1);
	$pdf->cell(2.18,0.5,"",1,0,'C',1);
	  
	
 									                              			   						      				 
	$pdf->Output(); //hasil out put ke browser
	
?>