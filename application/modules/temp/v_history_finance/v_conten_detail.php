<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.css');?>" />  
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" />        
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />      

<table class="table-striped table-hover table-condensed">
    <tr>
       <td style="font-weight:bold; font-size:13px;">
	   <?php echo "PP Number"." : ".$phasing_nopp ?>
       </td>
    </tr>
</table>
    
<table class="table-condensed table-bordered"  width="100%">        
    <tr style="background:#EFEFEF;font-weight:bold; font-size:13px;">
        <td width="5%">No</td>
        <td width="28%">Description</td>
        <td width="28%">Spec</td>
        <td width="5%">Qty</td>
        <td width="20%">Price</td>							
        <td width="20%">Total Price</td>
    </tr>
    <?php foreach ($str_pp_detail as $row_detail)  : $intno = $intno +1;  ?>
   		<tr style="font-size:13px;">	
          <td><?php echo $intno ?></td>							  
          <td><?php echo $row_detail->desc ?></td>							  
          <td><?php echo $row_detail->spec ?></td>																																																				
          <td align="center"><?php echo  number_format($row_detail->qty) ?></td>							
          <td><?php echo  number_format($row_detail->harga,2,'.',',') ?></td>							
          <td style="color:#FF4246" ><?php echo number_format($row_detail->total,2,'.',',') ?></td> 
        </tr>
    <?php endforeach; ?>
</table>

<br>

<table width="100%" class="table-striped  table-hover table-condensed table-bordered">
  <?php foreach ($str_pp_master as $row) : ?>
     <tr style="background:#EFEFEF; font-size:13px;" >
           <td  style="font-weight:bold" width="30%">Grand Total Actual:</td>
           <td  style="font-weight:bold; color:#FF4246" width="50%"><?php echo $row->currency." ".number_format($row->gran_total,2,'.',',') ?></td>
    </tr>
    
    <tr style="background:#EFEFEF; font-size:13px;" >
           <td  style="font-weight:bold" width="30%">Grand Total Adjustmen :</td>
           <td  style="font-weight:bold; color:#FF4246" width="50%"><?php echo $row->currency." ".number_format($row->gran_total_adjust,2,'.',',') ?></td>
    </tr>
    <tr style="background:#FAFECB; font-size:13px;" >
           <td style="font-weight:bold" width="30%">Remarks :</td>
           <td width="50%"><?php echo $row->remarks ?></td>
    </tr>
  <?php endforeach  ?>
</table> 