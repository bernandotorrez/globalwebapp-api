<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_print_fpb_po extends CI_Model {	
				    
    public $db_tabel = 'qv_head_pp_complite';
	 
    public function tampil_add_pp(){ //tampil table untuk ngececk num_row						     	     		  
	     $ses_id_dept = $this->session->userdata('id_dept'); //manggili session id 
		 $ses_id_company = $this->session->userdata('id_company');  	
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 
		 $short_company = $this->session->userdata('short');	 		 		
		 $ses_branch_short = $this->session->userdata('branch_short'); 
		 $ses_dept = $this->session->userdata('dept');	 		 
		 
		 $send_aprove ="1";		  		 		 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $flag_status_aprove = "1"; // sudah di aprove
		 
		 $this->load->database();		
		 $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,aprove_head,aprove_fc,aprove_bod,date_aproval,term_top,user_submission,flag_fpb,status,status_send_aprove,short,currency,gran_total,attach_quo,counter_reprint'); 		
		 $this->db->where('id_company',$ses_id_company);	
		 $this->db->where('branch_id',$ses_id_branch );		
		 $this->db->where('id_dept',$ses_id_dept);		 
		 $this->db->where('status',$status_delete);
		 $this->db->where('status_send_aprove',$send_aprove);				
		 $this->db->where('short',$short_company);						 
		 $this->db->where('branch_short',$ses_branch_short);	
		 $this->db->where('dept',$ses_dept);	
		 
		 $this->db->where('aprove_head',$flag_status_aprove); //flag aprove Head
		 $this->db->where('aprove_fc',$flag_status_aprove);   //flag aprove FC	
		 $this->db->where('aprove_bod',$flag_status_aprove);  //flag aprove B.O.D					
		 	
		 $this->db->order_by('date_pp','desc');
		 $result = $this->db->get('qv_head_pp_complite');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	     $ses_id_dept = $this->session->userdata('id_dept'); 
		 $ses_id_company = $this->session->userdata('id_company');  	
		 $ses_id_branch = $this->session->userdata('branch_id');
		 $short_company = $this->session->userdata('short');	 		 		
		 $ses_branch_short = $this->session->userdata('branch_short'); 
		 $ses_dept = $this->session->userdata('dept');	 		 		
		 
		 $send_aprove ="1";				
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $flag_status_aprove = "1"; // sudah di aprove
		  	     		 
	     $this->load->database();		
		 $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,aprove_head,aprove_fc,aprove_bod,date_aproval,term_top,user_submission,flag_fpb,status,status_send_aprove,short,currency,gran_total,attach_quo,counter_reprint'); 	
		 $this->db->where('id_company',$ses_id_company);					
		 $this->db->where('branch_id',$ses_id_branch );	
		 $this->db->where('id_dept',$ses_id_dept);
		 $this->db->where('status',$status_delete);
		 $this->db->where('status_send_aprove',$send_aprove);			
		 $this->db->where('short',$short_company);						 
		 $this->db->where('branch_short',$ses_branch_short);	
		 $this->db->where('dept',$ses_dept);		
		 
		 
		 $this->db->where('aprove_head',$flag_status_aprove); //flag aprove Head
		 $this->db->where('aprove_fc',$flag_status_aprove);   //flag aprove FC	
		 $this->db->where('aprove_bod',$flag_status_aprove);  //flag aprove B.O.D							
		  		  		 
		 $this->db->order_by('date_pp','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_head_pp_complite')->result();		 		   
		 return $result ;
	}
	
	function search_data() 	// fungsi cari data pp model
	{
	   $cari = $this->input->post('txtcari');
	   $kategory =  $this->input->post('cbostatus');	   	 
	   
	   $ses_company = $this->session->userdata('id_company');
	   $ses_id_dept = $this->session->userdata('id_dept');
	   $ses_branch_id = $this->session->userdata('branch_id');
	   
	   $flag_status_aprove = "1"; // sudah di aprove
	   $flag_bpk_form = "0"; //flag bukan dari form bpk manual
	   
	         $this->load->database();	
	   	     $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,aprove_head,aprove_fc,aprove_bod,date_aproval,term_top,user_submission,flag_fpb,status,status_send_aprove,counter_reprint,short,currency,gran_total,attach_quo');  
	   		
			 if ($cari != null) {
				   $this->load->database();		   				  
				   $this->db->where('id_company',$ses_company);
				   $this->db->where('id_dept',$ses_id_dept);
				   $this->db->where('branch_id',$ses_branch_id);	
				   $this->db->where('aprove_head',$flag_status_aprove); //flag aprove Head
		           $this->db->where('aprove_fc',$flag_status_aprove);   //flag aprove FC	
		           $this->db->where('aprove_bod',$flag_status_aprove);  //flag aprove B.O.D	
				   $this->db->where('flag_bpk',$flag_bpk_form);  //flag bukan dari form bpk manual	
				   $this->db->like($kategory,$cari);
				   $this->db->order_by('date_aproval','desc');		   
				   $result = $this->db->get('qv_head_pp_complite');		 
					return $result->result(); 	   		 
			 }else{
				   redirect('c_print_fpb_po/c_print_fpb_po'); 
			 }		  		  	 
	 } 		
	
	function search_date() 	// fungsi cari data pp model
	{
	 
	   $strdatestart = $this->input->post('txtdatestart'); 
	   $strdatend = $this->input->post('txtdateend');
	   	  
	   $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	   $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
	   
	   $ses_company = $this->session->userdata('id_company');
	   $ses_id_dept = $this->session->userdata('id_dept');
	   $ses_branch_id = $this->session->userdata('branch_id');
	   
	   $flag_status_aprove = "1"; // sudah di aprove
	   $flag_bpk_form = "0"; //flag bukan dari form bpk manual
	   
	         $this->load->database();	
	   	     $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,aprove_head,aprove_fc,aprove_bod,date_aproval,term_top,user_submission,flag_fpb,status,status_send_aprove,short,currency,gran_total,attach_quo');  
	   	
		   if($strdatestart !="" and $strdatend !="" ){	
		       $this->db->where('date_aproval BETWEEN "'.$datestart.'" and "'.$dateend.'"');	
	  		   $this->db->where('id_company',$ses_company);
			   $this->db->where('branch_id',$ses_branch_id); 
			   $this->db->where('id_dept',$ses_id_dept);
			   $this->db->where('aprove_head',$flag_status_aprove); //flag aprove Head
		       $this->db->where('aprove_fc',$flag_status_aprove);   //flag aprove FC	
		       $this->db->where('aprove_bod',$flag_status_aprove);  //flag aprove B.O.D	
			   $this->db->where('flag_bpk',$flag_bpk_form);  //flag bukan dari form bpk manual				   		
			   $this->db->order_by('date_aproval','desc');		   
		       $result = $this->db->get('qv_head_pp_complite');		 
		       return $result->result(); 
		  
		    }else{
			   redirect('c_print_fpb_po/c_print_fpb_po'); 
		    }		  	 
	 } 		
	
	
	
	//-------------------function untuk cari dan narik table report fpb
	function select_data_rpt_fpb()
	{
	  $str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete		
	  $strflag_adjust = "0";		
	  $strid_master =$this->input->post('msg'); 	 	  						  					    						   	 			   	   					  				   			  			   			
	 	for ($i=0; $i < count($strid_master) ; $i++) { 				
			  $query = $this->db->select("desc,spec,qty,harga,total,vendor,currency,gran_total")
								->where('id_master',$strid_master[$i])
								->where('flagadjust',$strflag_adjust)
								->where('status',$str_status_terdelete)
								->get('qv_master_det_pp');
			  if ($query->num_rows() >= 1)
			  {		
			  	 foreach ($query->result() as $row)
				 {
			  		$data = array('sesidmaster' => $strid_master[$i],							
								  'ses_vendor' =>  $row->vendor,
								  'ses_grand'   => $row->gran_total,	
								  ); //buat array di session		
					$this->session->set_userdata($data); //simpan di session
				 }
					
					//Update flag fpb yg sedang di cetak ------------------------------------
				   /* if($row->flag_print_fpb != "1"){				  
						  $data_flag =array("flag_fpb" => '1', //give_flag_printfpb
											"flag_print_fpb" => '1', //give_flag_printfpb										
											"date_print_fpb" => date('Y-m-d')				
											);							  				
					}else{
						  $data_flag =array(
										   "flag_print_fpb" => '1', //give_flag_printfpb										
										   "date_print_fpb" => date('Y-m-d')				
										   );		
					}
					 $this->db->where('id_master',$strid_master[$i]);
				     $this->db->update('tbl_master_pp',$data_flag);  */					 	  					  
					 //end---------------------------------------------------------------------								 
					 
					 return $query->result();										 
			  }else{
					 return false;
			  }
	   	}	
	   
	 }
	 
	 
	//-------------------function untuk cari dan narik table lalucetak PO 
	function select_data_rpt_po() //model untuk print po
	{
					
	  $strid_master =$this->input->post('msg'); 	 	  						  					    						   	  $str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete
	  $strflag_adjust = "0";
	   			   	   					  				   			  			   			
	 	for ($i=0; $i < count($strid_master) ; $i++) { 				
			  $query = $this->db->select("desc,spec,qty,harga,total,vendor,alamat,notelp,fax,currency,gran_total")
								->where('id_master',$strid_master[$i])
								->where('status',$str_status_terdelete)
								->where('flagadjust',$strflag_adjust)
								->get('qv_master_det_pp');
								
			  if ($query->num_rows() >= 1)
			  {		
			  	 foreach ($query->result() as $row)
				 {
			  		$data = array('sesidmaster' => $strid_master[$i],							
								  'ses_vendor' =>  $row->vendor,
								  'ses_alamat_ven' =>  $row->alamat,
								  'ses_notelp_ven' =>  $row->notelp,
								  'ses_fax_ven' =>  $row->fax,
								  'ses_grand'   => $row->gran_total,	
								  ); //buat array di session		
					$this->session->set_userdata($data); //simpan di session
				 }
																	 					 
					 return $query->result();										 
			  }else{
					 return false;
			  }
	   	}	
	   
	 } 

    function check_flag_fpb()// validasi untuk ceatk fpb jika nilai flag = 1 sudah tidak bisa di cetak lagi 
	{
		 $strid_master =$this->input->post('msg'); 		
		 $str_give_flag_fpb= "1";	  						  					    						   	 			   	   					  				   			  			   			
			for ($i=0; $i < count($strid_master) ; $i++) { 				
				  $query = $this->db->select("id_master") 
									->where('id_master',$strid_master[$i])
									->where('flag_print_fpb',$str_give_flag_fpb)
									->get('qv_master_det_pp');
				  if ($query->num_rows() >= 1)
				  {					  
					return true;				
				  }
				  
			}
	}	
	
	function give_right_fpb_print()
	{
					
	  $strid_master =$this->input->post('msg'); 	 	  						  					    						   	 			   	   					  				   			  			   			
	 	for ($i=0; $i < count($strid_master) ; $i++) { 				
			  $query = $this->db->select("id_master,desc,spec,qty,harga,total,vendor,currency,gran_total")
								->where('id_master',$strid_master[$i])
								->get('qv_master_det_pp');
			  if ($query->num_rows() >= 1)
			  {				
			    
				  foreach ($query->result() as $row)
				  {
			  		$data_right = array('ses_nopp' => $strid_master[$i]); 																							  					$this->session->set_userdata($data_right); //simpan di session
				  }	  								  
			     							  
				 //Update flag fpb yg sedang di cetak ------------------------------------
				  $str_give_flag_fpb= "0"; //0 jika di berikan izin untuk print ulang		
				  $data_flag =array("flag_print_fpb" => $str_give_flag_fpb, //give flag_print_fpb										
									);	
				  $this->db->where('id_master',$strid_master[$i]);
				  $this->db->update('tbl_master_pp',$data_flag); 				
				 //---------------------------------------------------------------------								 
					 
					 return $query->result();										 
			  }else{
					 return false;
			  }
	   	}	
	   
	 }		
						  						    	   			  	   			 	 	 	   				
				 
}