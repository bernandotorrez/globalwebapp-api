<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_history_finance extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('m_history_finance/M_history_finance','',TRUE);	
		$this->load->library('fpdf');   		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('terbilang');
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();							
	}		  	
		
  public function index()
  {					    																
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_pp= $this->M_history_finance->tampil_table()->result();	
	  $total_rows =$this->M_history_finance->tampil_table()->num_rows();
	  									
	  if ($tampil_table_pp)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(4);
		$per_page =5;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'c_history_finance/c_history_finance/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 4;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['pp_view'] =$this->M_history_finance->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
		
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;	
		$data['intno'] = ""; //variable buat looping no table.					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		$data['ceck_row'] = $total_rows;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'v_history_finance/v_history_finance';		
		$this->load->view('template_admin',$data);				
	  }else{	
	    $dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;	
		$data['intno'] = ""; //variable buat looping no table.					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data PP table is empty';		
		$data['show_view'] = 'v_history_finance/v_history_finance';
		$this->load->view('template_admin',$data);				
	  } 
  }
      						 			
	
	public function do_search_date()  	
	{
	  $tampil_table_pp= $this->M_history_finance->get_search_date()->result();	
	  $total_rows =$this->M_history_finance->get_search_date()->num_rows();
	  
		  if ($tampil_table_pp)
		  {
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('company') ;		
			$data['pp_view'] =	 $tampil_table_pp ;		
			$data['intno'] = ""; //variable buat looping no table.	
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;														
			$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
			$data['show_view'] = 'v_history_finance/v_history_finance';		
			$this->load->view('template_admin',$data);				
		  }else{	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('company') ;	
			$data['intno'] = ""; //variable buat looping no table.					
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;	
			$data['pesan'] = 'Data PP table is empty';		
			$data['show_view'] = 'v_history_finance/v_history_finance';
			$this->load->view('template_admin',$data);				
		  } 
	}
	
	public function do_search()  	
	{
	   $tampil_table_pp= $this->M_history_finance->get_search()->result();	
	   $total_rows =$this->M_history_finance->get_search()->num_rows();
	    if ($tampil_table_pp)
		  {
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;		
			$data['pp_view'] =	 $tampil_table_pp ;		
			$data['intno'] = ""; //variable buat looping no table.	
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;														
			$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
			$data['show_view'] = 'v_history_finance/v_history_finance';		
			$this->load->view('template_admin',$data);				
		  }else{	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;	
			$data['intno'] = ""; //variable buat looping no table.					
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;	
			$data['pesan'] = 'Data PP table is empty';		
			$data['show_view'] = 'v_history_finance/v_history_finance';
			$this->load->view('template_admin',$data);				
		  } 
	  
	}
	
	//------------------------------------pashing result to modal popup-------------------
	public function get_nopp_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //       
	     $query_master = $this->db->query("select id_master,remarks,gran_total,gran_total_adjust,currency from qv_head_pp_complite where id_master ='".$nopp."'");  
         $query_detail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   
		   
		      $data['str_pp_master']=$query_master->result();
			  $data['str_pp_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_nopp'] = $nopp ;
			  $tampil_detail = $this->load->view('v_history_finance/v_conten_detail',$data,true);
			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end phasing--------------------
	
	 public function do_print_bpk()
  	{		
		if($this->M_history_finance->select_data_rpt_bpk())
		 {	
															
			 $res['currency'] = $this->M_history_finance->select_query_rpt_bpk();				  
			 $res['data'] = $this->M_history_finance->select_query_rpt_bpk();	
			 
			 //array[data_detail_adjust] buat nampilin detail adjustment finance yang di input di form.			
			 $res['data_detail_adjust']=$this->M_history_finance->select_query_rpt_detail_adjustment();			
			 //end--------------------------------------------------------------------------------------
			 $this->load->view('v_history_finance/v_rptpdf_bpk',$res);				 				  
		  }else{     		
			 $this->session->set_flashdata('pesan_rpt','Print BPK Failed!!!!!! ....'); 					
			 $url=  base_url('c_history_finance/c_history_finance');		         
			 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
		  } 
		  
	}
	
	public function do_clear_adjust()
	{
		if($this->M_history_finance->clear_adjustment()) {
		    $this->session->set_flashdata('pesan_succes',"Clear Adjustmen Succesfully");
			 redirect('c_history_finance/c_history_finance');	  
		}else{
	         $this->session->set_flashdata('pesan',"Clear Adjustmen Failed"); 	
			  redirect('c_history_finance/c_history_finance');	  
		}
	}
	
	
	public function multiple_submit()
	{
		if ($this->input->post('btncaridate'))
		{
			$this->do_search_date();
		}else{
			if ($this->input->post('btncari'))
			{
			    $this->do_search();	
			}else{
				if ($this->input->post('btnprintbpk'))
			    {
			        $this->do_print_bpk();										
				}else{
					if ($this->input->post('btnclearadjust'))
			    	{
			        	$this->do_clear_adjust();	
					}else{
				    	redirect('c_history_finance/c_history_finance');
					}
				}
			}
		}
	}
}

