<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.css');?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />

<?php foreach ($str_pp_master as $row)  :  ?>
<h2>
	<?php
	    $strcom = $this->session->userdata('company');
		$strbranch = $this->session->userdata('name_branch');
		$strdept = $this->session->userdata('dept');

		if ($row->status_send_aprove =="-1" ) {
		   $header = 'Rejected Request';
		 //  echo '<div style="color:#FE3439;">'.$header." ".'Company : '.$strcom. " | " ." Cabang : " .$strbranch. " | " ."Dept : ".$strdept.'</div>' ;
		   $str_rejected_By ='<td width="40%" style="font-weight:bold;color:#FE3439;">Rejected By : </td>';
		   $str_reject_name ='<td width="30%" style="font-weight:bold;color:#FE3439;">'.$this->session->userdata('name').'</td>';
		   $str_reject_label ='<td width="40%" style="font-weight:bold; color:#FE3439;">Rejected Reason :</td>';
		   $str_reject_reason = '<td width="30%" style="font-weight:bold;color:#FE3439;">'.$row->reason_reject.'</td>';
		}else{
		    if ($row->status_send_aprove =="1" and $row->aprove_bod == "1")
			{
				 $header = 'Approved Request by B.O.D';
				 echo "PP Has been Approved By B.O.D : "." ".$this->session->userdata("name");
				 $str_rejected_By ='';
				 $str_reject_name ='';
				 $str_reject_label ='';
				 $str_reject_reason = '';
			}
		}

	 ?>
</h2>

<table width="100%" class="table-striped table-bordered table-hover table-condensed" >

    <tr style="background:#FBF5B7">
      <?php echo $str_rejected_By; ?>
      <?php echo $str_reject_name; ?>
    </tr>
     <tr>
       <?php echo $str_reject_label; ?>
       <?php echo $str_reject_reason; ?>

    </tr>
    <tr style="background:#FBF5B7">
      <td width="30%" style="font-weight:bold;">PP Number :</td>
      <td><?php echo  $this->session->userdata("ses_idmaster");  ?></td>
    </tr>
    <tr >
      <td width="30%" style="font-weight:bold;">User Submission :</td>
      <td width="30%"><?php echo $row->user_submission ?></td>
    </tr>
    <tr style="background:#FBF5B7">
      <td width="30%" style="font-weight:bold;">Date Sent Approval : </td>
      <td width="30%"><?php echo date('d-m-Y', strtotime($row->date_send_aproval)) ?></td>
    </tr>
    <tr >
      <td width="30%" style="font-weight:bold;" >T.O.P :</td>
       <td width="30%"><?php echo $row->term_top ?></td>
    </tr>
     <tr style="background:#FBF5B7">
       <td width="30%" style="font-weight:bold;">Remarks:</td>
       <td width="30%"><?php echo $row->remarks ?></td>
    </tr>
    <tr>
       <td width="20%" style="font-weight:bold;"><?php echo "Sub Total : "." ".$row->currency ?></td>
       <td width="20%" style="color:#FC2E33; font-weight:bold;"><?php echo number_format($row->gran_total)?></td>
    </tr>
 <?php endforeach; ?>
</table>

</br>

<h2>Detail PP</h2>
<table class="table-striped table-bordered table-hover table-condensed" width="70%">
    <tr style="background:#CCC;">
        <td width="2%" style="font-weight:bold;">No</td>
        <td width="28%" style="font-weight:bold;">Description</td>
        <td width="28%" style="font-weight:bold;">Spec</td>
        <td width="5%" style="font-weight:bold;">Qty</td>
        <td width="20%" style="font-weight:bold;">Price </td>
        <td width="20%" style="font-weight:bold;">Total Price</td>
    </tr>
    <?php foreach ($str_pp_detail as $row_detail)  : $intno = $intno +1;  ?>
   		<tr>
          <td><?php echo $intno ?></td>
          <td><?php echo $row_detail->desc ?></td>
          <td><?php echo $row_detail->spec ?></td>
          <td align="center"><?php echo  number_format($row_detail->qty) ?></td>
          <td><?php echo  number_format($row_detail->harga) ?></td>
          <td ><?php echo number_format($row_detail->total) ?></td>
        </tr>
    <?php endforeach; ?>
</table>
