<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">

<?php
	If ( $this->session->flashdata('pesan') != ""){ ?>
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>
            <?php  echo $this->session->flashdata('pesan');	?>
        </div>
<?php } ?>

<?php
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>
                <?php  echo "Successful Grant Approval"	?>
		  </div>
<?php } ?>

<?php
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>
			<?php  echo "Send Approval failed"	?>
        </div>
<?php } ?>

<?php
	If ($this->session->flashdata('pesan_reject') == "1"){ ?>
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>
			<?php  echo "Approval Rejected Succesful"	?>
        </div>
<?php } ?>



<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" >
   <?php if ( $this->session->userdata('aproval_flag')=="1" or  $this->session->userdata('aproval_flag') =="2" ){
	  		 echo "EUROKARSGROUP OF COMPANY";
		  }else{
		     echo "Approval Form"." | ".$header ;
		  }
    ?>
   </div>
 <div class="panel-body">


 <?php
     //$attributes = array('id' => 'form');
    // echo form_open('c_table_aproval/c_table_aproval/multiple_submit',$attributes);
	 echo form_open('c_table_aproval/c_table_aproval/multiple_submit');
  ?>

 <table>
     <tr>
      <td colspan="3">
       <label >Company : &nbsp; </label>
       </td>
       <td>
         <?php if($this->session->userdata("aproval_flag") != "3") { ?>
           <select name="cbostatus" id="cbostatus" class="form-control">
                <option>ALL</option>
                 <?php foreach($tampil_company as $row){ ?>
                     <option><?php echo $row->short ;?></option>
                 <?php } ?>
           </select>
         <?php }else{ ?>
            <select name="cbostatus" id="cbostatus" class="form-control">
                <option><?php echo $this->session->userdata("short"); ?></option>
 		    </select>
         <?php } ?>
       </td>
       <td>&nbsp;&nbsp;</td>
       <td><label >Dept : &nbsp; </label></td>
       <td>
        <?php if($this->session->userdata("aproval_flag") != "3") { ?>
           <select name="cbodept" id="cbodept" class="form-control">
                <option>ALL</option>
                 <?php foreach($tampil_dept as $row2){ ?>
                     <option><?php echo $row2->dept ;?></option>
                 <?php } ?>
           </select>
         <?php }else{ ?>
            <select name="cbodept" id="cbodept" class="form-control">
                <option><?php echo $this->session->userdata("dept"); ?></option>
 		    </select>
         <?php } ?>
       </td>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
       <td>
          <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
            <input id="txtdatestart" name="txtdatestart" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
           </div>
       </td>
       <td><label >&nbsp; S/D &nbsp; </label></td>
       <td>
       <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
            <input id="txtdateend" name="txtdateend" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
           </div>
       </td>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
       <td>
       	 <div class="input-group">
              <input type="text" name="txtcari" class="form-control" placeholder="Search PP Number" id="txtcari" >
            <div class="input-group-btn">
            <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-success"  />
            </div>
          </div>
       </td>
       <td colspan="3">&nbsp;</td>
       <td>
       <input id="btnref" name="btnref" type="submit" value="Refresh Table" class="btn btn-primary btn-warning" />
       </td>
       <td colspan="3">&nbsp;</td>
       <td colspan="3">
       <input id="btnaprove" name="btnaprove" type="submit" value="Approved"  class="btn btn-primary btn-aprove"  disabled="disabled"/>
         </td>
      </tr>
    </table>
 </br>


   <div class="row">
    <div class="col-md-12 col-md-offset-0">
	<?php
		echo '<table width="100%" class="table-striped table-bordered table-hover table-condensed"  >';
		echo '<tr style="background:url('.base_url('asset/images/child-panel-bg.png').');font-weight:bold;font-size: 12px">';
		echo '<td width="8%" >PP No / Id master</td>';
		echo '<td width="1%">Company</td>';
		echo '<td width="10%">Vendor</td>';
		echo '<td width="12%">Description</td>' ;
		echo '<td width="7%">Date Sent</td>' ;
		echo '<td width="3%" align="center">Currency</td>' ;
		echo '<td width="9%">Total Cost</td>' ;
		echo '<td width="8% ">Subbmision</td>' ;
		echo '<td width="8%">Head Dept</td>' ;
		echo '<td width="8%" align="center">Purchasing</td>' ;
		echo '<td width="8%" align="center">B.O.D</td>' ;
		echo '<td width="5%" align="center">Quotation</td>' ;
		echo '<td width="5%" align="center">Detail</td>' ;
		echo '<td width="5%" align="center">Reject</td>' ;
		echo '<td width="7%"><a href="javascript:void(0)" onclick="CheckAll()" style="text-decoration:none"> All</a>';
		echo ' / ';
		echo '<a href="javascript:void(0)" onclick="UncheckAll()" style="text-decoration:none">Uncheck</a></td>';
		echo '</tr>' ;

		if ($ceck_row >= 1) {
			  foreach($pp_view as $row){ $intno++ ;
        ?>
		<?php if ($intno % 2 == 0) : ?>
              <tr style="background:#F0F0F0;font-size:12px;" >
        <?php else:	?>
              <tr style="font-size: 12px" >
        <?php endif?>
                <td width="10%"><?php echo $row->id_master ; ?></td>
                <td width="1%" align="center" ><?php echo $row->short ; ?></td>
                <td><?php echo $row->vendor ; ?></td>
                <td><?php echo $row->header_desc ; ?></td>
                <td><?php echo date('d-m-Y', strtotime($row->date_send_aproval)) ; ?></td>
				<td align="center"><?php echo $row->currency ; ?></td>
                <td style="color:#EB293D;font-weight:bold"><?php echo number_format($row->gran_total,2,'.',',') ;?></td>
                <td><?php echo $row->user_submission ;?></td>
                <td align="center">
					<?php
						 If ($row->aprove_head == "1")
						 {
						    echo '<img src="'.base_url("asset/images/success_ico.png").'">' ;
						 }else{
							  If ($row->status_send_aprove == "-1")
							  {
								  echo '<div style=" color:#EB293D">'."Rejected".'</div>' ;
							  }else{
								  echo '<div style=" color:#EB293D">'."Pending Approval".'</div>' ;
							  }
						 }
				 	 ?>
                </td>
                 <td align="center">
				 	<?php
						 If ($row->approve_purchasing == "1")
						 {
						    echo '<img src="'.base_url("asset/images/success_ico.png").'">' ;
						 }else{
							  If ($row->status_send_aprove == "-1")
							  {
								 echo '<img src="'.base_url("asset/images/reject.png").'">' ;
							  }else{
								  echo '<div style=" color:#EB293D">'."Pending Approval".'</div>' ;
							  }
						 }
				 	 ?>
                </td>
                 <td align="center">
				 	 <?php
						 If ($row->aprove_bod == "1")
						 {
						     echo '<img src="'.base_url("asset/images/success_ico.png").'">' ;
						 }else{
							  If ($row->status_send_aprove == "-1")
							  {
								  echo '<img src="'.base_url("asset/images/reject.png").'">' ;
							  }else{
								  echo '<div style=" color:#EB293D">'."Pending Approval".'</div>' ;
							  }
						 }
				 	 ?>
                </td>
                <td>
				   <?php
				    If ($row->attach_quo != "")
					  {
					   echo'<a href='.base_url($row->attach_quo).' class="btn btn-warning btn-setting"  style="text-decoration:none">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".'View'.'</a>' ;
					  }else{
						echo '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';
					  }
					?>
                </td>

<td><a href="#" class="btn btn-primary btn-setting print" id="print" style="text-decoration:none" req_id="<?php echo $row->id_master ; ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>Detail</a></td>

<?php
      $strbodaprove  = $row->aprove_bod ;
	  $strpurchaseaprove   = $row->approve_purchasing ;
	  $strheadaprove = $row->aprove_head ;

if ($this->session->userdata("aproval_flag") == "1") :
  echo '<td><a href="#" class="btn btn-danger btn-setting reject" id="reject" style="text-decoration:none" req_id='. $row->id_master.'><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>Reject</a>
</td>';
else :
    if ($this->session->userdata("aproval_flag")== "3" or $this->session->userdata("aproval_flag")== "4" or $this->session->userdata("aproval_flag")== "0") :
       if ($strbodaprove == "1" and $strpurchaseaprove == "1" and $strheadaprove == "1" ) :
            echo '<td style="color:#EB293D"; align="center">'.'Has been Approved All'.'</td>' ;
       else:
          if ( $strpurchaseaprove == "1" and $strheadaprove == "1" ) :
              echo  '<td style="color:#EB293D"; align="center">'.'Has been Approved Purchase'.'</td>'  ;    
 	      else :
echo '<td><a href="#" class="btn btn-danger btn-setting reject" id="reject" style="text-decoration:none" req_id='. $row->id_master.'><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>Reject</a>
</td>';
           endif;
       endif;
     endif;
endif;
?>

<td><div align="center"><input id="checkchild" name="msg[]" type="checkbox" value="<?php echo $row->id_master ; ?>"onclick="checklist_child()"/>
  <div class='error_msg'></div>
    <?php  form_close(); ?>
</div></td>
</tr>
		 <?php
		  		}
		 	}
   echo'</table>';
		 ?>

		<?php
		     if (! empty($pagination))
			 {
		  	   echo '<table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table>';
			 }
		?>
      </div>
    </div>

  </div>
 </div>
</div>
 <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Info & Remarks</h4>
                    </div>

                    <div class="modal-body">
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->
                    </div>

                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
<!-- Modal -->

<!-- Modal Buat Reject -->

  <div class="modal fade" id="myModal-reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Remarks For Reject Status</h4>
                    </div>

                    <div class="modal-body-reject">
                       <table width="90%" class="table-condensed" align="center">
                            <tr>
                                <td> <label for="PP Number"  class="col-xs-8">PP Number</label></td>

                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtppno" name="txtppno" class="form-control" readonly>
                                   </div>
                                  </td>
                             </tr>
                             <tr>
                                <td ><label for="Reject Reason"  class="col-xs-9">Reject Reason</label></td>
                                <td>
                                  <div class="col-xs-12">
                                    <textarea id="txtreject" name="txtreject"  class="form-control"></textarea>
                                  </div>
                                </td>
                             </tr>
                             <tr>
                                <td></td>
                                <td>
                                   <div class="col-xs-12">

                                   </div>
                                </td>
                             </tr>
                            </table>

                    </div>

                     <div class="modal-footer">
                     <input type="submit" name="btnreject" value="Submit Reject" class="btn btn-danger"/>
                     <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

<!-- Modal Buat Reject -->


<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>


<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>


<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script>

<script>

// fungsi untik checklist semua otomatis
function CheckAll()
{
    l =document.forms[0].length;
		for(i=0;i<l;i++)
		{
		 document.forms[0].elements[i].checked = true;
		}
		document.getElementById('btnaprove').disabled = false;
}

function UncheckAll()
{
   l =document.forms[0].length;
		for(i=0;i<l;i++)
		{
		 document.forms[0].elements[i].checked = false;
		}
		document.getElementById('btnaprove').disabled = true;

}

//--------------------function disbaled enabled button with check book
<?php if($this->session->userdata('aproval_flag') != '1') : ?>
$(function() {  //.btn hnya untuk tombol aproval yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btn-aprove').removeAttr('disabled');
        }
        else{
            $('.btn-aprove').attr('disabled', 'disabled');
        }
    });
});
<?php else: ?>
$(function() {  //.btn hnya untuk tombol aproval yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn-aprove').removeAttr('disabled');
        }
        else{
            $('.btn-aprove').attr('disabled', 'disabled');
        }
    });
});
<?php endif ?>
//-----------------------------------------------------

//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});


</script>

<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.print',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;
		var url = '<?php echo site_url("c_table_aproval/c_table_aproval/get_nopp_modal"); ?>';

		$("#myModal").modal('show');

		$.ajax({
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}

		});

   });
});

</script>

<script>
 //get data table remarks to modal popup reject.
/*
$(function(){
  $(document).on('click','.reject',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;
		var url = '<?php //echo site_url("c_table_aproval/c_table_aproval/get_nopp_modal_reject"); ?>';

		$("#myModal-reject").modal('show');

		$.ajax({
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (html) {
				$('.modal-body').html(html);

			}
		});
   });
});

*/

$(function(){ // Show modal pop up send REJECT
  $(document).on('click','.reject',function(e){
		var req_id = $(this).attr('req_id'); //simpan atribut from <a ref 	php echo $row->id_master ke variable req_id  ;
		$("#myModal-reject").modal('show');
		$('#txtppno').val(req_id); //menampilkan isi dari  idmaster kedalam inputbox
		$('#txtreject').val(''); //mengkosongkan txtremark reject
	   });
});
</script>


<script>

$(function(){
    $('#myform').on('submit', function(e){ // id #myform di buat untuk diferentsiasi dengan form open code igniter
        e.preventDefault();

		var url = '<?php echo base_url("c_table_aproval/c_table_aproval/multiple_submit"); ?>';	 // url
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), // myform di buat untuk diferentsiasi dengan form open code igniter
            success: function(data){
                 alert($('#txtppno').val() + ',' + $('#txtreject').val())
            }
        });
    });
});
</script>
