<?php 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=report_excel_epurchasing.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
?>
<table border='1' width="70%">
    <tr bgcolor="#C5C5C5" style="font-weight:bold; alignment-adjust:middle">
    <td>PP Number</td>
    <td>Company</td>
    <td>Branch</td>		
    <td>Dept</td>		
    <td>Vendor</td>		
    <td>Description</td>
    <td>Term</td>
    <td>Date sub</td>		    			
    <td>Date Approved</td>
    <td>User Submission</td> 
    <td>PP Status</td> 
    <td>BPK Status</td> 
    <td>Remarks</td>    
    <td>Currency</td>    
    <td>Total Cost</td>    
    </tr>
	
	<?php if(!empty($data_excel)) : ?>
	  <?php foreach($data_excel as $item) {	?>
            <tr>
                <td><?=$item['id_master']?></td>
                <td><?=$item['company']?></td>
                <td><?=$item['name_branch']?></td>
                <td><?=$item['dept']?></td>
                <td><?=$item['vendor']?></td>                                        
                <td><?=$item['header_desc']?></td>  
                <td><?=$item['term_top']?></td>               
                <td><?=$item['date_send_aproval']?></td>
                <td>
				    <?php
					   if ($item['date_aproval'] != "" ) :
					       echo $item['date_aproval'];
					   else:  
						   echo '-';  
					   endif;  
					?>
                </td>
                <td><?=$item['user_submission']?></td>
                <td><?php					 
						 if ($item['flag_fpb'] != "1") :
							 echo '<label style="color:#EFE903">Open<label>';
						 else :	 
							 echo '<label style="color:blue">FPB<label>';
						 endif ;					
					?>
                 </td>   
                <td>
                   <?php
				   		 if ($item['flag_print_bpk'] == "1") :						  
					   			  echo '<label style="color:red">BPK<label>';
					        else :	
							   if ($item['flag_bpk'] == "1") :						  
								  echo '<label style="color:red">BPK<label>';
							   else :	  
							      echo "Waiting BPK";  
							   endif;
						 endif;  	
                   	?>
                </td> 	
                <td><?=$item['remarks']?></td>        
                <td><?=$item['currency']?></td>        
                <td style="color:#900;font-weight:bold"><?php echo number_format($item['gran_total'],2,'.',',') ;?></td>    
                </td>              
            </tr>
           
		<?php } ?>
        
     <?php endif ?>   
</table>