<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.css');?>" />  
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" />        
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />      


<br>
        
	<?php 
	   foreach ($str_ro_master as $row_head) :	   
	  ?>    
      <table class="table-condensed table-bordered"   >   
        <tr style="background:#CDC51F;font-weight:bold; font-size:12px;">
            <td  align="center" >No R.O</td>
            <td  align="center" >No P.O </td>
            <td  align="center" >Date R.O</td>
            <td  align="center" >No GRN</td>
            <td  align="center" >Grand Total R.O</td>
            <td  align="center" >Grand Total P.O</td>
            <td  align="center" >Grand Total Discrepancy</td>							
        </tr>    
   		<tr style="font-size:12px;font-weight:bold; color:#900;">	
          <td width="30%"><?php echo $row_head->no_ro ?></td>							  
          <td width="30%"><?php echo $row_head->id_master ?></td>							  
          <td align="center"  width="10%">
		  <?php
		  	if ($row_head->date_ro  !=""):
		  		echo  date('d-m-Y', strtotime($row_head->date_ro)); 
		    endif;		  	 
		  ?>
          </td>										
          <td align="center" width="20%"><?php echo $row_head->no_grn ?></td>																																									
          <td align="center" width="40%"><?php echo  number_format($row_head->grand_total_ro,2,'.',',') ?></td>
          <td align="center" width="40%"><?php echo  number_format($row_head->grand_total_po,2,'.',',') ?></td>
          <td align="center" width="40%"><?php echo  number_format($row_head->grand_total_discrep,2,'.',',') ?></td>
         </tr> 
       </table>               
     
            	                                
	  	 
       <?php  
	        $str_ro_detail = $this->db->query("select * from tbl_detail_ro where no_ro ='".$row_head->no_ro."'and status ='".'1'."'");
	        foreach ($str_ro_detail->result() as $row_detail) : 
	   ?>
       <table class="table-condensed table-bordered table-striped"   >
          <tr style="background:#EFEFEF;font-weight:bold; font-size:12px;">
            <td align="center">Desc</td>
            <td align="center">Qty Input</td>
            <td align="center"> Qty  R.O</td>
            <td align="center" >Qty P.O</td>
            <td align="center">Qty Discrepancy</td>
             <td align="center">Price</td>
            <td align="center">Total R.O</td>
            <td align="center">Total P.O</td>
            <td align="center">Total Discrepancy</td>							
          </tr>
          
	      <tr style="font-size:12px;">         
          	<td width="30%"><?php echo $row_detail->desc ?> </td>
            <td align="center" width="10%"><?php echo $row_detail->qty_input ?> </td>
            <td align="center" width="10%"><?php echo $row_detail->qty_ro_grn ?> </td>
            <td align="center" width="10%"><?php echo $row_detail->qty_po ?> </td>
            <td align="center" width="10%"><?php echo $row_detail->qty_disc_ro ?> </td>
            <td align="center" width="35%"><?php echo number_format($row_detail->harga_ro,2,'.',',') ?> </td>
            <td align="center" width="35%"><?php echo number_format($row_detail->total_ro,2,'.',',') ?> </td>
            <td align="center" width="45%"><?php echo number_format($row_detail->total_po,2,'.',',') ?> </td>
            <td align="center" width="45%"><?php echo number_format($row_detail->total_dis_ro,2,'.',',') ?> </td>
          </tr>   
         </table>  	                        		         
         <?php endforeach; ?>          
     <?php endforeach; ?>                    
<br>

<table width="100%" class="table-striped  table-hover table-condensed table-bordered" style="font-size:12px">



  