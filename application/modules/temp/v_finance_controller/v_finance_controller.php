<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">


 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">


<?php
	If ( $this->session->flashdata('pesan_give_right') != ""){ ?>
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>
            <?php  echo $this->session->flashdata('pesan_give_right');	?>
        </div>
<?php } ?>

<?php
	If ( $this->session->flashdata('pesan') != ""){ ?>
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>
            <?php  echo $this->session->flashdata('pesan');	?>
        </div>
<?php } ?>

<?php
	If ($this->session->flashdata('pesan_ok') != ""){ ?>
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>
               <?php  echo $this->session->flashdata('pesan_ok');	?>
		  </div>
<?php } ?>

<?php
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>
			<?php  echo "Send Approval failed"	?>
        </div>
<?php } ?>

<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" ><?php echo "Finance Controller View" ; ?> </div>

   <div class="panel-body">


 <?php  echo form_open('c_finance_controller/c_finance_controller/finance_controller_submit');  ?>

 <table>
     <tr>
       <td colspan="3">&nbsp;</td>
       <td><label >Date : &nbsp; </label></td>
       <td>
       		 <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
            <input id="txtdatestart" name="txtdatestart" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
       </td>
       <td><label >&nbsp; S/D &nbsp; </label></td>
       <td>

       	 <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
          <div class="input-group">
            <input id="txtdateend" name="txtdateend" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
            <div class="input-group-btn">
               <input id="btncaridate" name="btncaridate" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF" />
            </div>
          </div>
         </div>
       </td>
       <td>&nbsp;&nbsp;</td>
       <td colspan="3">
            <label >Selected : &nbsp; </label>
       </td>
       <td>
        <select name="cbostatus" id="cbostatus" class="form-control">
             <option value="id_master">PP NO</option>
             <option value="short">Company</option>
             <option value="dept">Dept</option>
             <option value="user_submission">Submission</option>
             <option value="header_desc">Description</option>
             <option value="vendor">vendor</option>
             <option value="gran_total">Grand Total</option>
       </select>
       </td>
       <td>&nbsp;&nbsp;</td>
       <td>
       	<div class="input-group">
              <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari" >
            <div class="input-group-btn">
            <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF" />
            </div>
          </div>
       </td>

       <td>&nbsp;</td>
       <td>
        <input id="btngiveright" name="btngiveright" type="submit" value="Give Right Re-Print FPB"  class="btn btn-warning" disabled="disabled" />
         </td>
          <td>&nbsp;</td>
        <td><a href="#" class="btn btn-danger btn-setting report" id="report" style="text-decoration:none"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>Report</a></td>
      </tr>
    </table>
 <br />

<div class="row">
 <div class="col-md-12 col-md-offset-0">
	<?php
		echo '<table width="100%" class="table-bordered  table-condensed"  >';
		echo '<tr style="background:url('.base_url("asset/images/child-panel-bg.png").');font-weight:bold;font-size:12px;">';
		echo '<td align="center width="9%" >PP No </td>';
		echo '<td align="center width="1%">Company</td>';
		echo '<td align="center width="1%">Dept</td>';
		echo '<td align="center width="10%">Vendor</td>';
		echo '<td align="center width="7%">Description</td>' ;
		echo '<td align="center width="7%">Date Approved</td>' ;
		echo '<td align="center width="9%">Total Cost</td>' ;
		echo '<td align="center width="8%">Subbmision</td>' ;
		echo '<td align="center width="8%">Head Dept</td>' ;
		echo '<td align="center width="8%">Fin Controll</td>' ;
		echo '<td align="center width="8%">B.O.D</td>' ;
		echo '<td width="7%" align="center">PP Status</td>';
		echo '<td width="4%" align="center">Re++</td>';
		echo '<td width="7%" align="center">Quo</td>';
	    echo '<td width="7%" align="center">View Detail</td>';
		echo '<td width="7%" align="center">Close GRN</td>';
		echo '<td align="center width="7%">Selected</a>';
		echo '</tr>' ;

		if ($ceck_row >= 1) {
			  foreach($pp_view as $row){ $intno++ ;
      ?>
       <?php if ($intno % 2 == 0) : ?>
                <tr style="background:#F0F0F0;font-size:12px;" >
	   <?php else:	?>
                <tr style="font-size: 12px" >
       <?php endif?>

        <?php if($row->flag_close_grn=="1"):  ?>
                <tr style="font-size:12px;" class="btn-warning" >
	   <?php else:	?>
                <tr style="font-size: 12px" >
       <?php endif?>


                <td width="8%"><?php echo $row->id_master ; ?></td>
                <td align="center"><?php echo $row->short ; ?></td>
                <td><?php echo $row->dept ; ?></td>
                <td><?php echo $row->vendor ; ?></td>
                <td><?php echo $row->header_desc ; ?></td>
                <td>
					 <?php
					    if ($row->date_aproval != "") :
					       echo date('d-m-Y', strtotime($row->date_aproval)) ;
						else:
							echo "-";
						endif ;
					 ?>
                </td>
                <td style="color:#EB293D;"><?php echo number_format($row->gran_total,2,'.',',') ;?></td>
                <td><?php echo $row->user_submission ;?></td>
                <td align="center">
					<?php
						 If ($row->aprove_head == "1")
						 {
						    echo '<img src="'.base_url("asset/images/success_ico.png").'">' ;
						 }else{
							 echo '<label style="color:#EB293D">wait<label>';
						 }
					 ?>
                </td>
                <td align="center">
					<?php
						 If ($row->aprove_fc == "1")
						 {
						   echo '<img src="'.base_url("asset/images/success_ico.png").'">' ;
						 }else{
						   echo '<label style="color:#EB293D">wait<label>';
						 }
					 ?>
                </td>
                 <td align="center">
					<?php
						 If ($row->aprove_bod == "1")
						 {
						    echo '<img src="'.base_url("asset/images/success_ico.png").'">' ;
						 }else{
							 echo '<label style="color:#EB293D">wait<label>';
						 }
					 ?>
                </td>
                <td align="center">
                   <?php
						 if  ($row->flag_bpk == "1" ){
							 echo '<label style="color:red">BPK<label>';
					  	 }else{
						   if  ($row->flag_print_bpk == "1" ){
							    echo '<label style="color:red">BPK<label>';
					  	   }else{
								 if ($row->flag_fpb == "0") {
									 echo '<label style="color:#EFE903">Open<label>';											   								}else{
									 echo '<label style="color:blue">FPB<label>';

								}
						   }
			  			 }
					?>
                </td>
                <td align="center"><?php echo $row->counter_reprint ; ?></td>
                 <td align="center">
				   <?php
				    If ($row->attach_quo != "")
					  {
					   echo'<a href='.base_url($row->attach_quo).' class="btn btn-success"  style="text-decoration:none">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".'View'.'</a>' ;
					  }else{
						echo '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';
					  }
					?>
                 </td>
                  <td align="center">
                	<a href="#" class="btn btn-primary btn-setting print" id="print" style="text-decoration:none" req_id="<?php echo $row->id_master ; ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>Detail</a>
                </td>
                 <td align="center">
                <?php if($row->flag_purchase=="2" or $row->flag_purchase=="3"):?>
                  <a href="#" class="btn btn-danger btn-close closegrn" id="closegrn" style="text-decoration:none" req_id="<?php echo $row->id_master ; ?>" flag_close="<?php echo $row->flag_close_grn;?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>GRN Detail</a>
                <?php else: ?>
                    <div style="color:#000;font-size:12px;font-weight:bold;color:#900" >
                        Not Direct Purchase
                    </div>
                <?php endif?>

                </td>
                <td><div align="center"><input id="checkchild" name="msg[]" type="checkbox" value="<?php echo $row->id_master ; ?>"onclick="checklist_child()"/>
    <?php form_close(); ?>
                </div>
            </td>
		  </tr>
		 <?php
		  		}
		 	}
   echo'</table>';
		 ?>

		<?php
		     if (! empty($pagination))
			 {
		  	   echo '<table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table>';
			 }
		?>

 </div>
</div>




  </div>
 </div>

</div>

 <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog   modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Info & Remarks</h4>
                    </div>

                    <div class="modal-body">
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->
                    </div>

                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
<!-- Modal -->

<!-- Modal For Report -->

        <div class="modal fade" id="rptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Report convert to excel</h4>
                    </div>

                    <div class="modal-body-report">
                       <table class="table-condensed" align="center"  >
                       <tr>
                          <td style="color:#900"><label for="Date start"  class="col-xs-11">Date Start * :</label></td>
                          <td>
                           <div class="control-group">
                            <div class="col-xs-11">
                          	<input id="txtrptdtstart" name="txtrptdtstart" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
                            </div>
                           </div>
                          </td>
                       </tr>
                        <tr>
                          <td style="color:#900"><label for="Date End"  class="col-xs-11" >Date End *  :</label></td>
                          <td>
                          <div class="control-group">
                            <div class="col-xs-11">
                          	<input id="txtrptdtend" name="txtrptdtend" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
                            </div>
                           </div>
                          </td>
                       </tr>
                       <tr>
                       	  <td><label for="Company"  class="col-xs-12">Company  : </label></td>
                          <td>
                         <div class="control-group">
                           <div class="col-xs-10">
                            <select id="cbocomp" name="cbocomp" class="form-control" >
                              <option value="all">- ALL -</option>
                               <?php foreach($tampil_company as $rowcom){ ?>
                             	 <option value="<?php echo $rowcom->id_company; ?>"><?php echo $rowcom->company ;?></option>
                               <?php }?>
                            </select>
                            </div>
                         </div>
                          </td>
                       </tr>
                        <tr>
                       	  <td><label for="Departement"  class="col-xs-12">Departement :</label></td>
                          <td>
                         <div class="control-group">
                           <div class="col-xs-10">
                            <select id="cbodept" name="cbodept"  class="form-control" >
                              <option value="all">- ALL -</option>
                               <?php foreach($tampil_dept as $rowdept){ ?>
                             	 <option value="<?php echo $rowdept->id_dept; ?>"><?php echo $rowdept->dept ;?></option>
                               <?php }?>
                            </select>
                            </div>
                           </div>
                          </td>
                       </tr>
                       <tr>
                       <td><label for="Status BPK"  class="col-xs-12">Status BPK :</label></td>
                         <td>
                         <div class="control-group">
                           <div class="col-xs-10">
                            <select id="cbobpk" name="cbobpk"  class="form-control" >
                              <option value="all">- ALL -</option>
                              <option value="1">BPK ONLY</option>
                            </select>
                            </div>
                           </div>
                          </td>
                       </tr>
                       </table>
                    </div>

                     <div class="modal-footer">

                        <input id="btnreportexcel" type="submit" name="btnreportexcel" value="Report To Excel" class="btn btn-danger btn_dobpkprint"/>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

<!-- Modal -->

<!-- Modal For GRN Detail -->
      <div class="modal fade" id="ViewDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header" >
                        <h4 class="modal-title modalgrnlabel" id="modalgrnlabel" style="margin-left:15px;"></h4>
                    <div>
                    </div>
                    <p>

                    <div class="col-xs-5">
                     <div class="input-group">
                       <input id="idpp" name="idpp"  class="form-control" readonly="readonly"/>
                      <div class="input-group-btn">
                             <input id="Close" type="submit" name="btn_close_grn" value="Close GRN" class="btn btn-danger btn_close_grn" onclick="return confirm('are you sure want to Close this GRN data!')"/>
                       </div>
                     </div>
                    </div>

                    <p>
                    <div class="modal-body" style="margin-top:20px;">
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->
                    </div>

                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

<!-- Modal -->


<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>


<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>


<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>


<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script>

<script>


$(function() {  //.btn hnya untuk tombol give right yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn-warning').removeAttr('disabled');
        }
        else{
            $('.btn-warning').attr('disabled', 'disabled');
        }
    });
});

//-------------------------------------------------------------------------


//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});
//set focus ----------------------------
</script>

<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.print',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;
		var url = '<?php echo site_url("c_finance_controller/c_finance_controller/get_nopp_modal"); ?>';

		$("#rptModal").modal('hide');
		$("#myModal").modal('show');

		$.ajax({
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);

			}

		});

   });
});
</script>

<script>
//post dari modal popup
$(function(){
    $('#myform').on('submit', function(e){ //id #myform di buat untuk diferentsiasi dengan form open code igniter
        e.preventDefault();
		var url = '<?php echo base_url("c_finance_controller/c_finance_controller/multiple_submit"); ?>';	 // url
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), // myform di buat untuk diferentsiasi dengan form open code igniter
            success: function(data){
            }
        });
    });
});
</script>

<script>
 //modala\ popup report.
$(function(){
  $(document).on('click','.report',function(e){
      $("#myModal").modal('hide');
	  $("#rptModal").modal('show');
	  $("#txtrptdtstart").val('');
	  $("#txtrptdtend").val('');

	  document.getElementById('cbocomp').selectedIndex= '0';
	  document.getElementById('cbodept').selectedIndex = '0';
	  document.getElementById('cbobpk').selectedIndex = '0';
   });
});
</script>

<script>
//get data table remarks to modal popup.
$(function(){
  $(document).on('click','.closegrn',function(e){
	   //-----------------------------
		var req_id = $(this).attr('req_id');
		var flag_close = $(this).attr('flag_close');
	   //ambil atribut from <a ref php echo $row->id_master ;

		var url = '<?php echo site_url("c_finance_controller/C_finance_controller/get_idporo_modal"); ?>';
		$('#ViewDetailModal').modal({backdrop: 'static', keyboard: false})
		$.ajax({
			type: 'POST',
			url: url,
			data: {id_master:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
				$('#idpp').val(req_id);
			    $("#idpp").focus();
				if (flag_close == "1")
				{
					$('#modalgrnlabel').html('Detail GRN / R.O History Already Close');
					$('#modalgrnlabel').css('color','#F00',);
					$('.btn_close_grn').attr('disabled', 'disabled');
				}else{
					$('#modalgrnlabel').html('Detail GRN / R.O History');
					$('.btn_close_grn').removeAttr('disabled');
				}

			}

		});

   });
});
</script>


<?php if ($this->session->userdata('give_righ_flag') == '0'): // jika status session user nya 7?>
   <script>
	$(document).ready(function() {
		$('#btngiveright').hide();
	});
	</script>
<?php endif ?>
