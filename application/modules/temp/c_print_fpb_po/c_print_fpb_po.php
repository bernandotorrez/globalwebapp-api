<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_print_fpb_po extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('m_print_fpb_po/M_print_fpb_po','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('terbilang');			
		$this->load->library('fpdf');   		
		$this->load->database();							
	}		  	
		
  public function index()
  {					    																
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_pp= $this->M_print_fpb_po->tampil_add_pp()->result();	
	  $total_rows =$this->M_print_fpb_po->tampil_add_pp()->num_rows();
	  									
	  if ($tampil_table_pp)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(4);
		$per_page =5;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'c_print_fpb_po/c_print_fpb_po/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 4;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['pp_view'] =$this->M_print_fpb_po->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
		
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;		
		$data['intno'] = ""; //variable buat looping no table.				
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		$data['ceck_row'] = $total_rows;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'v_print_fpb_po/v_print_fpb_po';		
		$this->load->view('template_admin',$data);				
	  }else{	
	    $dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data PP table is empty';		
		$data['show_view'] = 'v_print_fpb_po/v_print_fpb_po';
		$this->load->view('template_admin',$data);				
	  } 
  }
   
    public function do_search_data()  // fungsi cari data controler
    {				  			    	
		$tampung_cari = $this->M_print_fpb_po->search_data(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;	
		   $data['intno'] = ""; //variable buat looping no table.					
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				   		
		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['pp_view'] =$tampung_cari;
		   $data['pesan'] = 'Data PP Not a found'; 		   		   
		   $data['show_view'] = 'v_print_fpb_po/v_print_fpb_po';		
		   $this->load->view('template_admin',$data);					
		}else{			
		    $dept  = $this->session->userdata('dept') ;	
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;	
		    $data['intno'] = ""; //variable buat looping no table.				
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['pp_view'] =$tampung_cari;			   			
			$data['show_view'] ='v_print_fpb_po/v_print_fpb_po';		
		    $this->load->view('template_admin',$data);		
		}	   
    }	
	
	 
    public function do_search_date()  // fungsi cari data controler
    {				  			    	
		$tampung_cari = $this->M_print_fpb_po->search_date(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;	
		   $data['intno'] = ""; //variable buat looping no table.					
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				   		
		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['pp_view'] =$tampung_cari;
		   $data['pesan'] = 'Data PP Not a found'; 		   		   
		   $data['show_view'] = 'v_print_fpb_po/v_print_fpb_po';		
		   $this->load->view('template_admin',$data);					
		}else{			
		    $dept  = $this->session->userdata('dept') ;	
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;					
			$data['intno'] = ""; //variable buat looping no table.
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['pp_view'] =$tampung_cari;			   			
			$data['show_view'] ='v_print_fpb_po/v_print_fpb_po';		
		    $this->load->view('template_admin',$data);		
		}	   
    }	
	 
     public function do_print_fpb()
  	{
		if ($this->M_print_fpb_po->check_flag_fpb())
		{   
		   $this->session->set_flashdata('pesan',"Data has been printed, Please contact your finance to give access for reprint FPB!!"); 		          
		   redirect('c_print_fpb_po/c_print_fpb_po');	   
		}else{
			if($this->M_print_fpb_po->select_data_rpt_fpb())
			 {
				 //------give flag already print FPB--------------------
				 $data_flag =array("flag_fpb" => '1', //give_flag_printfpb
									"flag_print_fpb" => '1', //give_flag_printfpb										
									"date_print_fpb" => date('Y-m-d')				
									);	 
				 $this->db->where('id_master',$this->session->userdata("sesidmaster"));
				 $this->db->update('tbl_master_pp',$data_flag);
				 //end---------------------------------------------------------------
				 
				 $hasil_fpb = $this->M_print_fpb_po->select_data_rpt_fpb();						
				 $res['currency'] = $hasil_fpb;				  
				 $res['data'] = $hasil_fpb;				  
				 $this->load->view('v_print_fpb_po/v_rptpdf_fpb',$res);
			  }else{     		
				 $this->session->set_flashdata('pesan_rpt','Data Permintaan Pembayaran Update Failed!!!!!! ....'); 					
				 $url=  base_url('v_print_fpb_po/v_rptpdf_fpb');		         
				 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
			 } 
		}
	}
	
	 public function do_print_po()	 
  	 {	
		 if($this->M_print_fpb_po->select_data_rpt_po())
		 {
			// $this->M_print_fpb_po->give_flag_printfpb();//memberikan flag untuk yg sudah di print fpb
			 $hasil_fpb = $this->M_print_fpb_po->select_data_rpt_po();			
			 $res['data'] = $hasil_fpb;			  
			 $this->load->view('v_print_fpb_po/v_rptpdf_po',$res);
		  }else{     		
			 $this->session->set_flashdata('pesan_rpt','Data Permintaan Pembayaran Update Failed!!!!!! ....'); 					
			 $url=  base_url('c_print_fpb_po/c_print_fpb_po');		         
			 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
		 }
	 }
	 
	 
	  public function do_give_right_print_fpb()
      {		
		if($this->M_print_fpb_po->give_right_fpb_print())
		 {								    
		    $strnopp = $this->session->userdata('ses_nopp');
			$this->session->set_flashdata('pesan_give_right','PP No.'.$strnopp." ".'has been allow for reprint FPB!!'); 	
			$this->show_table(); //manggil fungsi show_table		
		  }else{     		
			 $this->session->set_flashdata('pesan_rpt','Give right Update Failed!!!!!! ....'); 					
			 $url=  base_url('c_print_fpb_po/c_print_fpb_po');		         
			 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
		  } 		 				 
  	  }	  
		 	 
    public function print_fpb_submit() //fungsi submit dengan banyak button dalam satu form
	{  	
		if ($this->input->post('btnprintfpb')){	
			$this->do_print_fpb();
		}else{
			if ($this->input->post('btnprintpo')){	
				 $this->do_print_po();
			}else{
				if ($this->input->post('btncari')){	
			 	    $this->do_search_data();
				}else{	
				   if ($this->input->post('btncaridate')){	
			 	      $this->do_search_date();
				   }else{
						if ($this->input->post('btngiveright')){	
						   $this->do_give_right_print_fpb();
						}else{			
						   redirect('c_print_fpb_po/c_print_fpb_po');  //direct ke url add_menu_booking/add_menu_booking
						}
				   }
				}				
			}
		} 
	}
	
}

