<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_history_finance extends CI_Model {	
				    
    public $db_tabel = 'qv_head_pp_complite';
	 
    public function tampil_table(){ //tampil table untuk ngececk num_row						     	     		  	   
		 $ses_id_company = $this->session->userdata('id_company');  				 		 		  		 		 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $status_fpb_flag = "1"; //flag FPB
		 
		 $this->load->database();		
		 $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,date_aproval,term_top,user_submission,flag_fpb,status,status_send_aprove,short,currency,gran_total,gran_total_adjust,flag_bpk,note_bpk,attach_quo');		
		 $this->db->where('id_company',$ses_id_company);							 
		 $this->db->where('flag_fpb',$status_fpb_flag);														 					
		 $this->db->where('status',$status_delete);	
		 $this->db->order_by('date_aproval','desc');
		 $result = $this->db->get('qv_head_pp_complite');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    	     
		 $ses_id_company = $this->session->userdata('id_company');  								
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $status_fpb_flag = "1"; //flag sudah di print FPB
		  	     		 
	     $this->load->database();		
		 $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,date_aproval,term_top,user_submission,flag_fpb,status,status_send_aprove,short,currency,gran_total,gran_total_adjust,flag_bpk,note_bpk,attach_quo');		
		 $this->db->where('id_company',$ses_id_company);								
		 $this->db->where('flag_fpb',$status_fpb_flag);				 	 									 							
	     $this->db->where('status',$status_delete);	  		 
		 $this->db->order_by('date_aproval','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_head_pp_complite')->result();		 		   
		 return $result ;
	}							  		
			
	public function get_search_date()
	{
	    $strdatestart =$this->input->post('txtdatestart');	
	    $strdatend =$this->input->post('txtdateend');	
	    $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	    $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		$status_fpb_flag = "1";  //flag sudah di print FPB
		$str_flag_del ="1" ; //flag sudah di print delete record
		//$str_flag_bpkform ="0" ; //flag bukan dari form naual bpk
	    $ses_id_company = $this->session->userdata('id_company');		
		 
	    if($strdatestart !="" and $strdatend !="" ){	
		   $this->load->database();		
	       $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,date_aproval,term_top,user_submission,flag_fpb,status,status_send_aprove,short,currency,flag_bpk,gran_total,gran_total_adjust,note_bpk,attach_quo');
		   $this->db->where('id_company',$ses_id_company);
		   $this->db->where('flag_fpb',$status_fpb_flag);
		  // $this->db->where('flag_bpk',$str_flag_bpkform); ////flag bukan dari form naual bpk
		   $this->db->where('status',$str_flag_del);
		   $this->db->where('date_aproval BETWEEN "'.$datestart.'" and "'.$dateend.'"');
		   $this->db->order_by('date_aproval','desc');
	 	   $result = $this->db->get('qv_head_pp_complite');		
	       return $result;		 			  		 	   	 
	    }else{
		   redirect('c_history_finance/c_history_finance');	
		}
		  
	}			
	
	public function get_search()
	{   	    
	    $strstatus =$this->input->post('cbostatus');	
	    $strcari =$this->input->post('txtcari');
		$status_fpb_flag = "1";  //flag sudah di print FPB
		$str_flag_del ="1" ; //flag sudah di print delete record
		//$str_flag_bpkform ="0" ; //flag bukan dari form naual bpk	
	    $ses_id_company = $this->session->userdata('id_company');				   
	   
	    if($strstatus !="" and $strcari !="" ){	
		   $this->load->database();		
	       $this->db->select('id_master,id_company,id_dept,company,dept,vendor,header_desc,date_send_aproval,date_aproval,term_top,user_submission,flag_fpb,status,status_send_aprove,short,currency,gran_total,gran_total_adjust,note_bpk,flag_bpk,attach_quo');	
		   $this->db->where('id_company',$ses_id_company);
		   $this->db->where('flag_fpb',$status_fpb_flag);	
		  // $this->db->where('flag_bpk',$str_flag_bpkform);  ////flag bukan dari form naual bpk 
		   $this->db->where('status',$str_flag_del);   
		   $this->db->like($strstatus,$strcari);
		   $this->db->order_by('date_aproval','desc');
	 	   $result = $this->db->get('qv_head_pp_complite');		
	       return $result;		 			  		 	   	 
	    }else{
		   redirect('c_history_finance/c_history_finance');
		}
		  
	}
    public function check_flag_bpk()// validasi untuk bpk jika nilai flag = 1 sudah tidak bisa di cetak lagi 
	{
		 $strid_master =$this->input->post('txtppno'); 		
		 $str_give_flag_bpk= "1";	  						  					    						   	 			   	   					  				   			  			   			
						
				  $query = $this->db->select("id_master") 
									->where('id_master',$strid_master)
									->where('flag_print_bpk',$str_give_flag_bpk)
									->get('qv_master_det_pp');
				  if ($query->num_rows() >= 1)
				  {					  
					return true;				
				  }
				  			
	} 		
	
    public function select_data_rpt_bpk()
	{
	
	  $str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete				
	  $strid_master =$this->input->post('txtppno'); 	  
	  $buffstrgadjust = $this->input->post('txtgadjust'); //nilai total adjustment	   		  	   	
	  $strnote_bpk = $this->input->post('txtnote');	 	  	
	  
		  if ($buffstrgadjust == "" or $buffstrgadjust == "0" ) {
			  $strgadjust = null ;			  		  
		  }else{
			  $strgadjust = str_replace(',','',$buffstrgadjust);
		  }
		  	  
	         //Update flag pada table master_PP bpk yg sedang di cetak ------------------------------------
			  $str_give_flag_bpk= "1";		
			  $data_flag =array(//"flag_bpk" => $str_give_flag_bpk, //give_flag_printbpk
								"flag_print_bpk" => $str_give_flag_bpk, //give_flag_printbpk	
								"note_bpk" => $strnote_bpk,		
								//"gran_total_adjust" =>str_replace(',','',$strgadjust),						
								"gran_total_adjust" =>$strgadjust,						
								"date_print_bpk" => date('Y-m-d')				
								);	
			  $this->db->where('id_master',$strid_master);
			  $this->db->update('tbl_master_pp',$data_flag); 
			  //--------------------------------------------------------------------------------------
			  
			  //detail insert adjustment--------------------------------------------------------------			  
				$strdesc = $this->input->post('txtdesc'); //array desc	 							
				$stradjust = $this->input->post('txtprice'); //array ajust	
													
			 if (implode($stradjust) != null) : //di conversi dari array ke string untuk validasi									
					  for ($i=0; $i < count($stradjust); $i++) :					      
					       if ($stradjust[$i] == "") {
						       $strbuffdesc = "-";
							   $strbuffspec = "-";
							   $strbuffqty = "0";
							   $strbuffket = "Clear Adjustment";
						   }else{	   	  
						       $strbuffdesc = $strdesc[$i];
							   $strbuffspec = "Adjustment";
							   $strbuffqty = "1"; 	
							   $strbuffket = "Adjustment";				
						   }
						 	 $insert_data = array(
										"id_master"  => $strid_master,				
										"desc"       => $strbuffdesc,	
										"spec"       => $strbuffspec,	
										"qty"        => $strbuffqty ,	
										"harga"      =>  str_replace(',','',$stradjust[$i]),	
										"total"      =>  str_replace(',','',$stradjust[$i]),																								
										"status"	 => "1",	
										"flagadjust" => "1",		
										"keterangan" =>  $strbuffket										
										);						
								$this->db->insert('tbl_detail_pp',$insert_data); 
						  endfor;					
			 endif;												  
		  //end-------------------------------------------------------------------------------------
		  return true;						   	   
	 }				    	   			  	   		
	 
	 public function select_query_rpt_bpk()
	 {		 
		
		$strid_master =$this->input->post('txtppno'); 	 
		$str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete	
		$str_flag_adjustment = "0";// jika "1" detail merupakan adjustmen		
		  //select field buat di cetak ------------------------------------	
			  $query = $this->db->select("desc,spec,qty,harga,total,vendor,currency,gran_total,gran_total_adjust,user_submission,head_user,note_bpk,date_send_aproval,date_aproval,date_print_bpk,flagadjust")
								->where('id_master',$strid_master)
								->where('status',$str_status_terdelete)
								->where('flagadjust',$str_flag_adjustment)
								->get('qv_master_det_pp');
			  if ($query->num_rows() >= 1)
			  {		
			  	 foreach ($query->result() as $row)
				 {
			  		$data = array('sesidmaster'      => $strid_master,							
								  'ses_vendor'       => $row->vendor,								  	
								  'ses_user_sub'     => $row->user_submission,	
								  'ses_head'         => $row->head_user,
								  'ses_notebpk'      => $row->note_bpk,									  
								  'ses_datesend'     => $row->date_send_aproval,	
								  'ses_dateapproval' => $row->date_aproval,
								  'ses_datecair'     => $row->date_print_bpk,	
								  'ses_curr'	     => $row->currency,								  									
								  'ses_grand'        => $row->gran_total,
								  'ses_grand_adjust' => $row->gran_total_adjust
								  ); //buat array di session		
					$this->session->set_userdata($data); //simpan di session
				 }													 										  
					 return $query->result();						 													
			  }else{				  
					 return false;
			  }
	 }
	 
	  public function select_query_rpt_detail_adjustment()
	  {		 		
		 $strid_master =$this->input->post('txtppno'); 	 
		 $str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete	
		 $str_flag_adjustment = "1";// jika "1" detail merupakan adjustmen
		
		 $query_adjust = $this->db->select("desc,spec,qty,harga,total,flagadjust")
								->where('id_master',$strid_master)
								->where('status',$str_status_terdelete)
								->where('flagadjust',$str_flag_adjustment)
								->get('tbl_detail_pp');
			  if ($query_adjust->num_rows() >= 1)
			  {		
			  	  return $query_adjust->result();	
			  }
		
	  }
	  
	   public function clear_adjustment()
	   {
		  $strget_idmaster = $this->input->post('msg');
		  $strflag_adjust = "1" ;//flag adjust
		  
		  if (isset($strget_idmaster) && trim($strget_idmaster!='')) :
				  for ($intloop = 0 ; $intloop < count($strget_idmaster);$intloop++) :	
				       //delete detail adjust		  	
					   $this->db->where('id_master', $strget_idmaster[$intloop]);
					   $this->db->where('flagadjust', $strflag_adjust);
					   $this->db->delete('tbl_detail_pp');	
					   
					   //clear field gran_total_adjust on master pp
					   $str_clear_totaladjust = array('gran_total_adjust' => null,
					    							   'note_bpk'  => null	
					   								  ) ;				   
					   $this->db->where('id_master', $strget_idmaster[$intloop]); 
					   $this->db->update('tbl_master_pp',$str_clear_totaladjust);			   		  	     
				  endfor;			  			       		   		  	 
			 return true; 
		 else :
		 	 return false; 
		 endif;	   
		  
		  
	   }
//------------------------------------------------------------------------------------------------------------------			
				 
}