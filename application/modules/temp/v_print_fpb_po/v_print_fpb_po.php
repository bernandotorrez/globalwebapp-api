<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">


 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">


<?php
	If ( $this->session->flashdata('pesan_give_right') != ""){ ?>
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>
            <?php  echo $this->session->flashdata('pesan_give_right');	?>
        </div>
<?php } ?>

<?php
	If ( $this->session->flashdata('pesan') != ""){ ?>
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>
            <?php  echo $this->session->flashdata('pesan');	?>
        </div>
<?php } ?>

<?php
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>
                <?php  echo "Send Approval Successfully"	?>
		  </div>
<?php } ?>

<?php
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>
			<?php  echo "Send Approval failed"	?>
        </div>
<?php } ?>

<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" ><?php echo "FPB Print". " | " .$header ; ?> </div>

   <div class="panel-body">


 <?php  echo form_open('c_print_fpb_po/c_print_fpb_po/print_fpb_submit');  ?>

 <table>
     <tr>
       <td colspan="3">&nbsp;</td>
       <td><label >Date : &nbsp; </label></td>
       <td>
       		 <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
            <input id="txtdatestart" name="txtdatestart" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
       </td>
       <td><label >&nbsp; S/D &nbsp; </label></td>
       <td>

       	 <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
          <div class="input-group">
            <input id="txtdateend" name="txtdateend" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
            <div class="input-group-btn">
               <input id="btncaridate" name="btncaridate" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF" />
            </div>
          </div>
         </div>
       </td>
       <td>&nbsp;&nbsp;</td>
       <td colspan="3">
            <label >Selected : &nbsp; </label>
       </td>
       <td>
        <select name="cbostatus" id="cbostatus" class="form-control">
             <option value="id_master">PP NO</option>
             <option value="header_desc">Description</option>
             <option value="vendor">vendor</option>
             <option value="gran_total">Grand Total</option>
       </select>
       </td>
       <td>&nbsp;&nbsp;</td>
       <td>
       	<div class="input-group">
              <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari" >
            <div class="input-group-btn">
            <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF" />
            </div>
          </div>
       </td>
         <td>&nbsp;</td>
       <td>
       <input id="btnprintpo" name="btnprintpo" type="submit" value="Print P.O"   class="btn btn-success" disabled="disabled"/>
  	   </td>
       <td>&nbsp;</td>
       <td>
       <input id="btnprintfpb" name="btnprintfpb" type="submit" value="Print FPB"  class="btn btn-primary " disabled="disabled" />
       </td>
       <td>&nbsp;</td>
      </tr>
    </table>
 <br />

<div class="row">
 <div class="col-md-12 col-md-offset-0">
	<?php
		echo '<table width="100%" class="table-striped table-bordered table-hover table-condensed"  >';
		echo '<tr style="background:url('.base_url("asset/images/child-panel-bg.png").');font-weight:bold;font-size:12px;">';
		echo '<td align="center" width="9%" >PP No </td>';
		echo '<td align="center" width="1%">Company</td>';
		echo '<td align="center" width="10%">Vendor</td>';
		echo '<td align="center" width="7%">Description</td>' ;
		echo '<td align="center" width="7%">Date Approved</td>' ;
		echo '<td align="center" width="9%">Total Cost</td>' ;
		echo '<td align="center" width="8%">Subbmision</td>' ;
		//echo '<td align="center width="8%">Head Dept</td>' ;
		//echo '<td align="center width="8%">Fin Controll</td>' ;
		//echo '<td align="center width="8%">B.O.D</td>' ;
		echo '<td align="center" width="8%">PP Approved</td>' ;
		echo '<td align="center" width="8%">Status FPB Printed</td>' ;
		echo '<td align="center" width="8%">Re-Print Counted</td>' ;
		echo '<td align="center" width="7%">Selected</a>';
		echo '</tr>' ;

		if ($ceck_row >= 1) {
			  foreach($pp_view as $row){ $intno++ ;
      ?>
       <?php if ($intno % 2 == 0) : ?>
                <tr style="background:#F0F0F0;font-size:12px;" >
	   <?php else:	?>
                <tr style="font-size: 12px" >
       <?php endif?>
                <td width="8%"><?php echo $row->id_master ; ?></td>
                <td><?php echo $row->short ; ?></td>
                <td><?php echo $row->vendor ; ?></td>
                <td><?php echo $row->header_desc ; ?></td>
                <td align="center"><?php echo date('d-m-Y', strtotime($row->date_aproval)) ; ?></td>
                <td  align="center" style="color:#EB293D; font-weight:bold"><?php echo number_format($row->gran_total,2,'.',',') ;?></td>
                <td align="center"><?php echo $row->user_submission ;?></td>
              <!--  <td align="center"> -->
					<?php
					/*	 If ($row->aprove_head == "1")
						 {
						    echo '<img src="'.base_url("asset/images/success_ico.png").'">' ;
						 }
						 */
					 ?>
                </td>
               <!-- <td align="center"> -->
					<?php
					  /*
						 If ($row->aprove_fc == "1")
						 {
						   echo '<img src="'.base_url("asset/images/success_ico.png").'">' ;
						 }
						 */
					 ?>
                </td>
                <td align="center">
					<?php

						 If ($row->aprove_bod == "1")
						 {
						  echo '<img src="'.base_url("asset/images/success_ico.png").'">' ;
						 }

					 ?>
                </td>
                <td align="center">
                 <?php
				 	if ($row->flag_fpb == 0 ) :
						echo '<label  style="color:#EB293D;">'.'Not Printed'. '</label>';
					else :
						echo '<label  style="color:#EB293D;">'.'Printed'. '</label>';
					endif;
				   ?>
				</td>
                <td align="center"><?php echo $row->counter_reprint ?></td>
                <td><div align="center"><input id="checkchild" name="msg[]" type="checkbox" value="<?php echo $row->id_master ; ?>"onclick="checklist_child()"/>
    <?php form_close(); ?>
                </div>
            </td>
		  </tr>
		 <?php
		  		}
		 	}
   echo'</table>';
		 ?>

		<?php
		     if (! empty($pagination))
			 {
		  	   echo '<table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table>';
			 }
		?>

 </div>
</div>




  </div>
 </div>

</div>



<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>


<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>


<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script>

<script>

//--------------------function disbaled enabled button with check book

$(function() {  //.btn hnya untuk tombol po yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn-success').removeAttr('disabled');
        }
        else{
            $('.btn-success').attr('disabled', 'disabled');
        }
    });
});


$(function() {  //.btn hnya untuk tombol fpb yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn-primary').removeAttr('disabled');
        }
        else{
            $('.btn-primary').attr('disabled', 'disabled');
        }
    });
});


$(function() {  //.btn hnya untuk tombol give right yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn-warning').removeAttr('disabled');
        }
        else{
            $('.btn-warning').attr('disabled', 'disabled');
        }
    });
});

//-------------------------------------------------------------------------


//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});
//set focus ----------------------------
</script>

<?php if ($this->session->userdata('give_righ_flag') == '0'): // jika status session user nya 7?>
   <script>
	$(document).ready(function() {
		$('#btngiveright').hide();
	});
	</script>
<?php endif ?>
