<?php
			
	require('mc_table.php');
	$pdf=new PDF_MC_Table('P','cm',"A4");
	$pdf->Open();
	$pdf->AddPage();	
	$pdf->AliasNbPages();
	$pdf->SetMargins(1,1,1);
	$pdf->SetFont('times','B',12);

	$pdf->Image('asset/images/euro.jpg',1,1,4); //tampilan yg pertama di tampilkan report : P,L,B
	
	//-----------------------------------------------------				
	$pdf->Ln(1);
	$pdf->setFont('Arial','B'.'U',12);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"PURCHASE ORDER",0,0,'C',1);  		
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial');
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"NO PO : ".'PO/'.$this->session->userdata('sesidmaster'),0,0,'C',1); 		           
	
	$pdf->Ln(0.7);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                													   						
	$pdf->cell(0,0,"Printed date : " . date('d/m/Y'),0,0,'R',1);   
   	
	$pdf->Ln(0.5); // baris berikut nya pada report denga lebar (line)garis ke bawah sebanyak 15(line) 
	$pdf->setFont('Arial','B',10); //Type huruf(font beserta besaran huruf)
	$pdf->setFillColor(255,255,255); //warna pada baris tulisan                								
	$pdf->cell(0,0,"PT.".$this->session->userdata('company'),0,0,'L',1); //tampil session company 				
	$pdf->cell(0,0,"Pay To : ".$this->session->userdata('ses_vendor'),0,1,'R',1);    		
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                													   					
	$pdf->cell(0,0,"Departement : " .$this->session->userdata('dept'),0,0,'L',1); 
	$pdf->cell(0,0,"Address : " . $this->session->userdata('ses_alamat_ven'),0,1,'R',1);
	
		                       	           	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"Phone :".$this->session->userdata('phone_branch'),0,0,'L',1); 
	$pdf->cell(0,0,"Phone :".$this->session->userdata('ses_notelp_ven'),0,1,'R',1); 
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"Fax :".$this->session->userdata('fax_branch'),0,0,'L',1); 
	$pdf->cell(0,0,"Fax :".$this->session->userdata('ses_fax_ven'),0,1,'R',1); 
	
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"Address :".$this->session->userdata('address_branch'),0,0,'L',1); 
	//$pdf->cell(0,0,"Address :".$this->session->userdata('ses_alamat_ven'),0,1,'R',1); 
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(100,0,"City :".$this->session->userdata('city'),0,1,'L',1); 
	
	
	$pdf->Ln(0.5);				
	$no = 1;// intuk index petama	
	$pdf->SetFont('Arial','B',9);
	$pdf->SetWidths(array(1, 5, 5, 1,3.7,3.7));
	$pdf->SetHeight(0.1);
	$pdf->Row(array("No", "Description", "Specs", "Qty","Price","Total"));
	
	$pdf->SetFont('Arial','',9);
	foreach ($data as $key) {
		$pdf->Row(array($no,
						$key->desc,
						$key->spec,
						number_format($key->qty),
						number_format($key->harga,2,'.',','),
						number_format($key->total,2,'.',',')						
				        )
			      );
	     $no++;	 			
	}
	
	$pdf->setFont('Arial','B',10);
	$pdf->setFillColor(255,255,255);               
	$pdf->cell(15.7,0.6,"Sub Total :"." ".$key->currency." ",1,0,'R',1);
	$pdf->cell(3.7,0.6,number_format($key->gran_total,2,'.',','),1,1,'R',1);
	$pdf->cell(19.4,0.6,"Mention:"." "."(".terbilang($key->gran_total).")",1,1,'L',1);
	
	$pdf->Ln(0.6);				
	$pdf->setFont('Arial','B',8);
	$pdf->setFillColor(255,255,255);  
	
	
	$pdf->cell(13.4,0.5,"",0,0,'C',1); // buat sepasi ke kanan
	$pdf->cell(3,0.5,"Purchasing",1,0,'C',1); 
	$pdf->cell(3,0.5,"Applicant",1,1,'C',1); 	
	
	$pdf->cell(13.4,0.5,"",0,0,'C',1);// buat sepasi ke kanan
    $pdf->cell(3,1.5,"",1,0,'C',1); //buat space tanda tangan 
	$pdf->cell(3,1.5,"",1,1,'C',1); //buat space tanda tangan                           		
	
	$pdf->cell(13.4,0.5,"",0,0,'C',1);  	// buat sepasi ke kanan
	$pdf->cell(3,0.5,"FASTWIN A.SUJOTO",1,0,'C',1); 
	$pdf->cell(3,0.5,$this->session->userdata('sign_head'),1,1,'C',1);  				    									   		 
	$pdf->Output(); //hasil out put ke browser
	
?>