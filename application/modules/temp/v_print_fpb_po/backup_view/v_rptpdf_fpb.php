<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class FPDF_AutoWrapTable extends FPDF
{		
	//Page header
	
	var $CI; //mendefinisikan variable yada ada di CI ke FPDF
	
    private $data = array();  	
	private $options = array(
  		'filename' => '',
  		'destinationfile' => '',
  		'paper_size'=>'F4',
  		'orientation'=>'P'
  	);
	
    function __construct($data = array(), $options = array()){ 
        parent::__construct();
        $this->CI =& get_instance();//membuat function construct agar variable yg ada di CI Dapat terhubung ke fpdf
		$this->data = $data;
    	$this->options = $options;
		
    }
	
	
	public function rptDetailData () {
		
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;
		
		//header
		$this->Image('asset/images/euro.jpg',30,20,90); //tampilan yg pertama di tampilkan report : P,L,B		
		
		$this->Ln(14);
		$this->SetFont("Arial", "B", 9);
		$this->cell(0,0, "PT.".$this->CI->session->userdata('company'));
		$this->cell(0,0,"NO PP : ".implode($this->CI->session->userdata('sesidmaster')),0,1,'R',1);
				
		$this->Ln(15);		
		$this->setFont('Arial','',9);
		$this->setFillColor(255,255,255);                													   					
		$this->cell(0,0,"Departement : " .$this->CI->session->userdata('dept'),0,0,'L',1); 		
		$this->cell(0,0,"Printed date : " . date('d/m/Y'),0,1,'R',1);
					
		$this->Ln(10);
		$this->setFont('Arial','B'.'U',12);
		$this->setFillColor(255,255,255);                               
		$this->cell(0,0,"FORM PERMINTAAN BIAYA",0,0,'C',1);                
							
		$this->Ln(20);
		$this->setFont('Arial','',9);
		$this->setFillColor(255,255,255);                               
		$this->cell(100,6,"Payment To : ".$this->CI->session->userdata('ses_vendor'),0,0,'L',1);   
				
		$this->Ln(15);
		$this->setFont('Arial','',9);
		$this->setFillColor(255,255,255);   
		$this->cell(100,6,"Amount Of Money : ".number_format($this->CI->session->userdata('ses_grand')),0,0,'L',1); 
		
		$this->Ln(18);
		$h = 13;
		$left = 40;
		$top = 80;	
		#tableheader
		$this->SetFont("", "B", 9);
		$this->SetFillColor(255,255,255);	
		$left = $this->GetX();
		$this->Cell(20,$h,'NO',1,0,'L',true);				
		$this->SetX($left += 20); $this->Cell(140, $h, 'Description', 1, 0, 'C',true);
		$this->SetX($left += 140); $this->Cell(120, $h, 'Specs', 1, 0, 'C',true);
		$this->SetX($left += 120); $this->Cell(40, $h, 'Qty', 1, 0, 'C',true);
		$this->SetX($left += 40); $this->Cell(105, $h, 'Price', 1, 0, 'C',true);
		$this->SetX($left += 105); $this->Cell(115, $h, 'Total', 1, 1, 'C',true);		
		//--------------------------------------------header--------------------
		
		#detail
		$this->SetFont('Arial','',9);
		$this->SetWidths(array(20,140,120,40,105,115));
		$this->SetAligns(array('C','L','L','L','L','L'));
		$no = 1; $this->SetFillColor(255);
		foreach ($this->data as $baris) {
			$this->Row(
				array($no++, 				
				$baris['Description'], 
				$baris['Specs'], 
				$baris['Qty'], 
				$baris['Price'], 
				$baris['Total']
			));
		}
			
	}
	
	public function printPDF () {
				
		if ($this->options['paper_size'] == "F4") {
			$a = 8.3 * 72; //1 inch = 72 pt
			$b = 13.0 * 72;
			$this->FPDF($this->options['orientation'], "pt", array($a,$b));
		} else {
			$this->FPDF($this->options['orientation'], "pt", $this->options['paper_size']);
		}
		
	    $this->SetAutoPageBreak(false);
	    $this->AliasNbPages();
	    $this->SetFont("Arial", "B", 10);
	
	    $this->rptDetailData();
			    
	    $this->Output($this->options['filename'],$this->options['destinationfile']);
  	}
	
	private $widths;
	private $aligns;

	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}

	function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=10*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,10,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
	
	
		
	
	
}//end function

//contoh penggunaan
  


$data = array();
foreach ($data_detail as $key){	   
	$data = $data_detail ;	
}  	

//pilihan
$options = array(
	'filename' => '', //nama file penyimpanan, kosongkan jika output ke browser
	'destinationfile' => '', //I=inline browser (default), F=local file, D=download
	'paper_size'=>'F4',	//paper size: F4, A3, A4, A5, Letter, Legal
	'orientation'=>'P' //orientation: P=portrait, L=landscape
);

$tabel = new FPDF_AutoWrapTable($data, $options);
$tabel->printPDF();

 
/*$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
$pdf->Content($data);
$pdf->Output(); */