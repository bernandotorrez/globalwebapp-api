<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class PDF extends FPDF
{		
	//Page header
	
	var $CI; //mendefinisikan variable yada ada di CI ke FPDF

    function __construct(){ //membuat function construct agar variable yg ada di CI Dapat terhubung ke fpdf
        parent::__construct();
        $this->CI =& get_instance();
    }
	
		
	function Header()
	{			
				

			$this->Image('asset/images/euro.jpg',10,15,40); //tampilan yg pertama di tampilkan report : P,L,B
			//-----------------------------------------------------				
			$this->Ln(15); // baris berikut nya pada report denga lebar (line)garis ke bawah sebanyak 15(line) 
			$this->setFont('Arial','',10); //Type huruf(font beserta besaran huruf)
			$this->setFillColor(255,255,255); //warna pada baris tulisan                								
			$this->cell(90,0,"PT.".$this->CI->session->userdata('company'),0,0,'L',1); //tampil session company 				
			$this->cell(0,0,"NO : ".implode($this->CI->session->userdata('sesidmaster')),0,1,'R',1); 				
			
			$this->Ln(5);
			$this->setFont('Arial','',10);
			$this->setFillColor(255,255,255);                													   					
			$this->cell(0,0,"Departement : " .$this->CI->session->userdata('dept'),0,0,'L',1); 
			$this->cell(0,0,"Printed date : " . date('d/m/Y'),0,1,'R',1);
			
			
			$this->Ln(10);
			$this->setFont('Arial','B'.'U',12);
			$this->setFillColor(255,255,255);                               
			$this->cell(0,0,"FORM PERMINTAAN BIAYA",0,0,'C',1);                
		   
			
			
			$this->Ln(5);
			$this->setFont('Arial','',11);
			$this->setFillColor(255,255,255);                               
			$this->cell(100,6,"Payment To : ".$this->CI->session->userdata('ses_vendor'),0,1,'L',1);                	            $this->cell(100,6,"Amount Of Money :".number_format($this->CI->session->userdata('ses_grand')),0,1,'L',1); 
			
			
			$this->Ln(7);
			$this->setFont('Arial','',9);
			$this->setFillColor(255,255,255,255,255,255);
			$this->cell(10,6,'No.',1,0,'C',1);
			$this->cell(55,6,'Description',1,0,'C',1);
			$this->cell(45,6,'Specs',1,0,'C',1);
			$this->cell(10,6,'Qty',1,0,'C',1);
			$this->cell(35,6,'Price',1,0,'C',1);
			$this->cell(35,6,'Total',1,1,'C',1);			
                
	}
 
	function Content($data) //Deatil rpt yg datimapilkan dari table detail_pp
	{
            $ya = 46;
            $rw = 6;
            $no = 1;
				
                foreach ($data as $key) {
                        $this->setFont('Arial','',9);
                        $this->setFillColor(255,255,255,255,255,255);	
                        $this->cell(10,9,$no,1,0,'C',1);
                        $this->cell(55,9,$key->desc,1,0,'L',1);
                        $this->cell(45,9,$key->spec,1,0,'L',1);
                        $this->cell(10,9,number_format($key->qty),1,0,'C',1);
						$this->cell(35,9,number_format($key->harga),1,0,'R',1);
						$this->cell(35,9,number_format($key->total),1,1,'R',1);
                        $ya = $ya + $rw;
                        $no++;
                }
				
				$this->setFont('Arial','B',10);
                $this->setFillColor(255,255,255);               
				$this->cell(155,6,"Sub Total :"." ".$key->currency." ",1,0,'R',1);
				$this->cell(35,6,number_format($key->gran_total),1,1,'R',1);
				$this->cell(190,6,"Mention:"." "."(".terbilang($key->gran_total).")",1,1,'L',1);
				
				$this->Ln(10);				
			    $this->setFont('Arial','B',8);
		   	    $this->setFillColor(255,255,255);    
				
				$this->cell(70,6,"",0,0,'C',1);  	// buat header jabatan yg menandatangani		    				
				$this->cell(30,6,"B.O.D",1,0,'C',1);  
				$this->cell(30,6,"F.C",1,0,'C',1);
				$this->cell(30,6,"Manag.Dept / Head",1,0,'C',1);    
				$this->cell(30,6,"Applicant",1,1,'C',1);    								                              			   				
				$this->cell(70,11,"",0,0,'C',1); //buat space tanda tangan 			    				
				$this->cell(30,11,"",1,0,'C',1);  
				$this->cell(30,11,"",1,0,'C',1);
				$this->cell(30,11,"",1,0,'C',1);    
				$this->cell(30,11,"",1,1,'C',1);    								                              			
				
				$this->setFont('Arial','B'.'I'.'U',8);
		   	    $this->setFillColor(255,255,255); 
				$this->cell(70,0,"",0,0,'C',1);  // buat nama yg menandatangani			    
				$this->cell(30,6,$this->CI->session->userdata('sign_bod'),1,0,'C',1);  				
				$this->cell(30,6,$this->CI->session->userdata('sign_fc'),1,0,'C',1);  
				$this->cell(30,6,$this->CI->session->userdata('sign_head'),1,0,'C',1);  
				$this->cell(30,6,$this->CI->session->userdata('name'),1,1,'C',1);  
				  
 
	}
	function Footer()
	{
		//atur posisi 1.5 cm dari bawah
		$this->SetY(-15);
		//buat garis horizontal
		//$this->Line(10,$this->GetY(),210,$this->GetY());
		//Arial italic 9
		//$this->SetFont('Arial','I',9);
          //      $this->Cell(0,10,'copyright gubugkoding.com Semarang ' . date('Y'),0,0,'L');
		//nomor halaman
		//$this->Cell(0,10,'Halaman '.$this->PageNo().' dari {nb}',0,0,'R');
	}
}
 
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
$pdf->Content($data);
$pdf->Output();