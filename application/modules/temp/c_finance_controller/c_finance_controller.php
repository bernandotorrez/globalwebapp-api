<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_finance_controller extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('m_finance_controller/M_finance_controller','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('terbilang');			
		$this->load->library('fpdf');   		
		$this->load->database();							
	}		  	
		
  public function index()
  {					    																
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_pp= $this->M_finance_controller->tampil_add_pp()->result();	
	  $total_rows =$this->M_finance_controller->tampil_add_pp()->num_rows();
	  									
	  if ($tampil_table_pp)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(4);
		$per_page =5;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'c_finance_controller/c_finance_controller/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 4;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['pp_view'] =$this->M_finance_controller->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
		
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;		
		$data['intno'] = ""; //variable buat looping no table.				
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		$data['ceck_row'] = $total_rows;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.
		$data['tampil_company']=$this->M_finance_controller->get_company()->result();																															
		$data['tampil_dept']=$this->M_finance_controller->get_dept()->result();																
		$data['show_view'] = 'v_finance_controller/v_finance_controller';		
		$this->load->view('template_admin',$data);				
	  }else{	
	    $dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data PP table is empty';		
		$data['tampil_company']=$this->M_finance_controller->get_company()->result();																															
		$data['tampil_dept']=$this->M_finance_controller->get_dept()->result();		
		$data['show_view'] = 'v_finance_controller/v_finance_controller';
		$this->load->view('template_admin',$data);				
	  } 
  }
   
    public function do_search_data()  // fungsi cari data controler
    {				  			    	
		$tampung_cari = $this->M_finance_controller->search_data(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;	
		   $data['intno'] = ""; //variable buat looping no table.					
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				   		
		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['pp_view'] =$tampung_cari;
		   $data['pesan'] = 'Data PP Not a found'; 		   		   
		   $data['show_view'] = 'v_finance_controller/v_finance_controller';		
		   $this->load->view('template_admin',$data);					
		}else{			
		    $dept  = $this->session->userdata('dept') ;	
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;	
		    $data['intno'] = ""; //variable buat looping no table.				
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		    $data['tampil_company']=$this->M_finance_controller->get_company()->result();																															
		    $data['tampil_dept']=$this->M_finance_controller->get_dept()->result();		
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['pp_view'] =$tampung_cari;			   			
			$data['show_view'] ='v_finance_controller/v_finance_controller';		
		    $this->load->view('template_admin',$data);		
		}	   
    }	
	
	 
    public function do_search_date()  // fungsi cari data controler
    {				  			    	
		$tampung_cari = $this->M_finance_controller->search_date(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;	
		   $data['intno'] = ""; //variable buat looping no table.					
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				   		
		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['pp_view'] =$tampung_cari;
		   $data['pesan'] = 'Data PP Not a found'; 		   		   
		   $data['show_view'] = 'v_finance_controller/v_finance_controller';		
		   $this->load->view('template_admin',$data);					
		}else{			
		    $dept  = $this->session->userdata('dept') ;	
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;					
			$data['intno'] = ""; //variable buat looping no table.
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		    $data['tampil_company']=$this->M_finance_controller->get_company()->result();																															
		    $data['tampil_dept']=$this->M_finance_controller->get_dept()->result();		
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['pp_view'] =$tampung_cari;			   			
			$data['show_view'] ='v_finance_controller/v_finance_controller';		
		    $this->load->view('template_admin',$data);		
		}	   
    }		     		 
	 
	  public function do_give_right_print_fpb()
      {		
		if($this->M_finance_controller->give_right_fpb_print())
		 {								    
		    $strnopp = $this->session->userdata('ses_nopp');
			$this->session->set_flashdata('pesan_give_right','PP No.'.$strnopp." ".'has been allow for reprint FPB!!'); 	
			$this->show_table(); //manggil fungsi show_table		
		  }else{     		
			 $this->session->set_flashdata('pesan_rpt','Give right Update Failed!!!!!! ....'); 					
			 $url=  base_url('c_finance_controller/c_finance_controller');		         
			 echo "<script type='text/javascript'> window.location='" . $url . "'; </script>";  
		  } 		 				 
  	  }	  
	
	
	//------------------------------------pashing result to modal popup-------------------
	public function get_nopp_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //       
	     $query_master = $this->db->query("select id_master,remarks,gran_total,gran_total_adjust,currency from qv_head_pp_complite where id_master ='".$nopp."'");  
         $query_detail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   
		   
		      $data['str_pp_master']=$query_master->result();
			  $data['str_pp_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_nopp'] = $nopp ;
			  $tampil_detail = $this->load->view('v_finance_controller/v_conten_detail',$data,true);
			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end phasing--------------------  
	
	function toExcelAll() {	 
		$tampil_to_excel =  $this->M_finance_controller->ToExcelAll(); 
		$data['data_excel'] = $tampil_to_excel ; 		
		$this->load->view('v_finance_controller/excel_view',$data);
    }
	
	   //------------------------------------VIEW pashing result to modal popup-------------------
	public function get_idporo_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $id_master= $this->input->post('id_master',TRUE); //       
	     $query_master = $this->db->query("select * from tbl_ro where id_master ='".$id_master."'");  		 
        // $query_detail = $this->db->query("select * from qv_ro_complite where id_master ='".$id_master."'and status ='".$status_aktif_record."'");  
		   		  		   		   
		   if($query_master->num_rows()>0) {	   		   
		      $data['str_ro_master']=$query_master->result();			
			  $data['intno']="";			  			  			  
			  $data['phasing_id_master'] = $id_master ;
			  $tampil_detail = $this->load->view('v_finance_controller/v_conten_detail_grn',$data,true);			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end phasing------------------------------------------------------------------------------		
	
	public function do_update_close_grn()
	{
		if($this->M_finance_controller->update_close_grn())
		{			
			$strnopp =$this->session->userdata('ses_idpp'); 		    
			$this->session->set_flashdata('pesan_ok','PP No.'.$strnopp." ".'Close GRN Is Done!'); 	
			$this->show_table(); 
	
		}
			
    }

		 	 
    public function finance_controller_submit() //fungsi submit dengan banyak button dalam satu form
	{  			
		if ($this->input->post('btncari')){	
			$this->do_search_data();
		}else{	
		   if ($this->input->post('btncaridate')){	
			  $this->do_search_date();
		   }else{
				if ($this->input->post('btngiveright')){	
				   $this->do_give_right_print_fpb();
				}else{
					if($this->input->post('btn_close_grn')){
					   $this->do_update_close_grn();	
					}else{
						if ($this->input->post('btnreportexcel')){	
						   $this->toExcelAll();
						}else{			
						   redirect('c_finance_controller/c_finance_controller');  //direct ke url add_menu_booking/add_menu_booking
						}
					 }
				}
		   }
		}				
		
	}
	
}

