<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_report_grn extends CI_Model
{

    var $table = 'tbl_master_grn';
    var $column_order = array(null, null,'id_master','number_grn','date_grn', 'user_submission', 'gran_total', 'total_ppn', 'total_amount'); //set column field database for datatable orderable
	var $column_search = array('id_master','number_grn','date_grn', 'user_submission', 'gran_total', 'total_ppn', 'total_amount'); //set column field database for datatable searchable 
    var $order = array('number_grn' => 'desc'); // default order 
    //var $view_table = 'qv_head_pp_complite';
    var $table_detail = 'tbl_detail_grn';

    public function getDataMasterGRNByID($id) {
       
        $query = $this->db->get_where($this->table, array('number_grn' => $id));
        return $query->result();
    }  

    public function getDataDetailGRNByID($id) {
       
        $query = $this->db->get_where($this->table_detail, array('number_grn' => $id));
        return $query->result();
    }

    public function getDataMasterPOByID($id) {
       
        $query = $this->db->get_where('qv_head_pp_complite', array('id_master' => $id));
        return $query->result();
    } 

    public function get_data($start, $length, $order, $dir) 
    {

        if($order !=null) {
            $this->db->order_by($order, $dir);
        }

        return $this->db
            ->limit($length,$start)
            ->get($this->table);
    }

    function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
        $this->db->where('status = 0');
		$query = $this->db->get();
		return $query->result();
    }
    
    private function _get_datatables_query()
	{
        
        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        //$access = '0';
        // if($access == '1') {
        //     $this->db->from($this->table);
        // } else {
        //     $this->db->from($this->table);
        //     $this->db->where('id_company', $id_company);
        //     $this->db->where('id_branch', $id_branch);
        // }

        $this->db->from($this->table);
		$this->db->where('status = 0');

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
        }
        
        if($_POST['start_date'] != '' && $_POST['end_date'] != '') // here order processing
		{
			$start_date = $_POST['start_date'];
			$end_date = $_POST['end_date'];

			$start_date = date("Y-m-d", strtotime($start_date));
			$end_date = date("Y-m-d", strtotime($end_date));
			$this->db->where('date_grn BETWEEN "'.$start_date. '" and "'.$end_date.'"');
		}
    }
    
    function count_filtered()
	{
		$this->_get_datatables_query();
        $this->db->where('status = 0');
		$query = $this->db->get();
		return $query->num_rows();
    }

    public function count_all()
	{
		$this->db->from($this->table);
        $this->db->where('status = 0');
		return $this->db->count_all_results();
	}

}