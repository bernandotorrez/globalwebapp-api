<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_report_excel extends CI_Model {	

    public $view_table = 'qv_head_pp_complite';

    public function reportExcelTotal($date_start, $date_end, $id_company, $id_dept, $status_bpk) {
        $hasil = array();	       						 	
		$this->db->select('id_master,id_company,id_dept,company,dept,name_branch,vendor,header_desc,date_send_aproval,date_aproval,term_top,user_submission,flag_fpb,flag_bpk,flag_print_bpk,status,short,remarks,currency,gran_total,ppn,pph,gran_totalppn,type_purchase');								 		 
			 
		if ($id_company != "all") :			 		  
			$this->db->where('id_company',$id_company);
		endif;
			 	
		if ($id_dept != "all") :			 		  
			$this->db->where('id_dept',$id_dept);
		endif;	
			 
		if ($status_bpk != "all") :			 		  
			$this->db->where('flag_bpk','1');
		endif;	
			 
		$this->db->where('status','1');			 			 			
        $this->db->where('date_send_aproval BETWEEN "'. $date_start. '" and "'.$date_end.'"');	
        $this->db->where('po_no_created', null);		
		$this->db->order_by('date_send_aproval','asc');
		$result = $this->db->get('qv_head_pp_complite');	
			 
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
		 }else{
				 return null ;
		 } 		 				
    }

    public function reportExcelTotalUnder50($date_start, $date_end, $id_company, $id_dept, $status_bpk) {
        $hasil = array();	       						 	
		$this->db->select('id_master,id_company,id_dept,company,dept,name_branch,vendor,header_desc,date_send_aproval,date_aproval,term_top,user_submission,flag_fpb,flag_bpk,flag_print_bpk,status,short,remarks,currency,gran_total,ppn,pph,gran_totalppn,type_purchase');								 		 
			 
		if ($id_company != "all") :			 		  
			$this->db->where('id_company',$id_company);
		endif;
			 	
		if ($id_dept != "all") :			 		  
			$this->db->where('id_dept',$id_dept);
		endif;	
			 
		if ($status_bpk != "all") :			 		  
			$this->db->where('flag_bpk','1');
		endif;	
			 
		$this->db->where('status','1');			 			 			
        $this->db->where('date_send_aproval BETWEEN "'. $date_start. '" and "'.$date_end.'"');	
        $this->db->where('gran_totalppn <=', '50000000');	
        $this->db->where('po_no_created', null);		
        $this->db->order_by('date_send_aproval','asc');	
		$result = $this->db->get('qv_head_pp_complite');	
			 
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
		 }else{
				 return null ;
		 } 		 				
    }

    public function reportExcelTotalUpper50($date_start, $date_end, $id_company, $id_dept, $status_bpk) {
        $hasil = array();	       						 	
		$this->db->select('id_master,id_company,id_dept,company,dept,name_branch,vendor,header_desc,date_send_aproval,date_aproval,term_top,user_submission,flag_fpb,flag_bpk,flag_print_bpk,status,short,remarks,currency,gran_total,ppn,pph,gran_totalppn,type_purchase');								 		 
			 
		if ($id_company != "all") :			 		  
			$this->db->where('id_company',$id_company);
		endif;
			 	
		if ($id_dept != "all") :			 		  
			$this->db->where('id_dept',$id_dept);
		endif;	
			 
		if ($status_bpk != "all") :			 		  
			$this->db->where('flag_bpk','1');
		endif;	
			 
		$this->db->where('status','1');			 			 			
        $this->db->where('date_send_aproval BETWEEN "'. $date_start. '" and "'.$date_end.'"');	
        $this->db->where('gran_totalppn >=', '50000001');	
        $this->db->where('po_no_created', null);		
        $this->db->order_by('date_send_aproval','asc');	
		$result = $this->db->get('qv_head_pp_complite');	
			 
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
		 }else{
				 return null ;
		 } 		 				
    }

    // PO
    public function reportExcelTotalPO($date_start, $date_end, $id_company, $id_dept, $status_bpk) {
        $hasil = array();	       						 	
		$this->db->select('id_master,id_company,id_dept,company,dept,name_branch,vendor,header_desc,date_send_aproval,date_aproval,term_top,user_submission,flag_fpb,flag_bpk,flag_print_bpk,status,short,remarks,currency,gran_total,ppn,pph,gran_totalppn,type_purchase,po_no_created');								 		 
			 
		if ($id_company != "all") :			 		  
			$this->db->where('id_company',$id_company);
		endif;
			 	
		if ($id_dept != "all") :			 		  
			$this->db->where('id_dept',$id_dept);
		endif;	
			 
		if ($status_bpk != "all") :			 		  
			$this->db->where('flag_bpk','1');
		endif;	
			 
		$this->db->where('status','1');			 			 			
        $this->db->where('date_send_aproval BETWEEN "'. $date_start. '" and "'.$date_end.'"');
        $this->db->where('po_no_created != ', null);			
		$this->db->order_by('date_send_aproval','asc');
		$result = $this->db->get('qv_head_pp_complite');	
			 
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
		 }else{
				 return null ;
		 } 		 				
    }

    public function reportExcelTotalUnder50PO($date_start, $date_end, $id_company, $id_dept, $status_bpk) {
        $hasil = array();	       						 	
		$this->db->select('id_master,id_company,id_dept,company,dept,name_branch,vendor,header_desc,date_send_aproval,date_aproval,term_top,user_submission,flag_fpb,flag_bpk,flag_print_bpk,status,short,remarks,currency,gran_total,ppn,pph,gran_totalppn,type_purchase,po_no_created');								 		 
			 
		if ($id_company != "all") :			 		  
			$this->db->where('id_company',$id_company);
		endif;
			 	
		if ($id_dept != "all") :			 		  
			$this->db->where('id_dept',$id_dept);
		endif;	
			 
		if ($status_bpk != "all") :			 		  
			$this->db->where('flag_bpk','1');
		endif;	
			 
		$this->db->where('status','1');			 			 			
        $this->db->where('date_send_aproval BETWEEN "'. $date_start. '" and "'.$date_end.'"');	
        $this->db->where('gran_totalppn <=', '50000000');
        $this->db->where('po_no_created != ', null);		
        $this->db->order_by('date_send_aproval','asc');		
		$result = $this->db->get('qv_head_pp_complite');	
			 
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
		 }else{
				 return null ;
		 } 		 				
    }

    public function reportExcelTotalUpper50PO($date_start, $date_end, $id_company, $id_dept, $status_bpk) {
        $hasil = array();	       						 	
		$this->db->select('id_master,id_company,id_dept,company,dept,name_branch,vendor,header_desc,date_send_aproval,date_aproval,term_top,user_submission,flag_fpb,flag_bpk,flag_print_bpk,status,short,remarks,currency,gran_total,ppn,pph,gran_totalppn,type_purchase,po_no_created');								 		 
			 
		if ($id_company != "all") :			 		  
			$this->db->where('id_company',$id_company);
		endif;
			 	
		if ($id_dept != "all") :			 		  
			$this->db->where('id_dept',$id_dept);
		endif;	
			 
		if ($status_bpk != "all") :			 		  
			$this->db->where('flag_bpk','1');
		endif;	
			 
		$this->db->where('status','1');			 			 			
        $this->db->where('date_send_aproval BETWEEN "'. $date_start. '" and "'.$date_end.'"');	
        $this->db->where('gran_totalppn >=', '50000000');
        $this->db->where('po_no_created != ', null);		
        $this->db->order_by('date_send_aproval','asc');		
		$result = $this->db->get('qv_head_pp_complite');	
			 
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
		 }else{
				 return null ;
		 } 		 				
    }

    // GRN
    public function reportExcelTotalGRN($date_start, $date_end, $id_company, $id_dept, $status_bpk) {
        $hasil = array();	       						 	
        $this->db->select('tmg.*, tmp.id_company, tmp.id_dept, tmp.company, tmp.dept, tmp.vendor, tmp.header_desc, tmp.term_top,
        tmp.type_purchase, tmp.currency, tmp.remarks, tmp.name_branch');
        $this->db->from('tbl_master_grn tmg');	
        $this->db->join('qv_head_pp_complite tmp', 'tmp.id_master = tmg.id_master', 'inner');							 		 
			 
		if ($id_company != "all") :			 		  
			$this->db->where('tmp.id_company',$id_company);
		endif;
			 	
		if ($id_dept != "all") :			 		  
			$this->db->where('tmp.id_dept',$id_dept);
		endif;	
			 
		if ($status_bpk != "all") :			 		  
			$this->db->where('tmp.flag_bpk','1');
		endif;		
			 
		$this->db->where('tmp.status','1');			 			 			
        $this->db->where('tmg.date_grn BETWEEN "'. $date_start. '" and "'.$date_end.'"');			
		$this->db->order_by('tmg.date_grn','asc');
		$result = $this->db->get();	
			 
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
		 }else{
				 return null ;
		 } 		 				
    }

    public function reportExcelTotalUnder50GRN($date_start, $date_end, $id_company, $id_dept, $status_bpk) {
        $hasil = array();	       						 	
		$this->db->select('tmg.*, tmp.id_company, tmp.id_dept, tmp.company, tmp.dept, tmp.vendor, tmp.header_desc, tmp.term_top,
        tmp.type_purchase, tmp.currency, tmp.remarks, tmp.name_branch');
        $this->db->from('tbl_master_grn tmg');	
        $this->db->join('qv_head_pp_complite tmp', 'tmp.id_master = tmg.id_master', 'inner');							 		 
			 
		if ($id_company != "all") :			 		  
			$this->db->where('tmp.id_company',$id_company);
		endif;
			 	
		if ($id_dept != "all") :			 		  
			$this->db->where('tmp.id_dept',$id_dept);
		endif;	
			 
		if ($status_bpk != "all") :			 		  
			$this->db->where('tmp.flag_bpk','1');
		endif;		
			 
		$this->db->where('tmp.status','1');			 			 			
        $this->db->where('tmg.date_grn BETWEEN "'. $date_start. '" and "'.$date_end.'"');
        $this->db->where('tmg.gran_total <=', '50000000');			
		$this->db->order_by('tmg.date_grn','asc');
		$result = $this->db->get();	
			 
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
		 }else{
				 return null ;
		 } 	 				
    }

    public function reportExcelTotalUpper50GRN($date_start, $date_end, $id_company, $id_dept, $status_bpk) {
        $hasil = array();	       						 	
		$this->db->select('tmg.*, tmp.id_company, tmp.id_dept, tmp.company, tmp.dept, tmp.vendor, tmp.header_desc, tmp.term_top,
        tmp.type_purchase, tmp.currency, tmp.remarks, tmp.name_branch');
        $this->db->from('tbl_master_grn tmg');	
        $this->db->join('qv_head_pp_complite tmp', 'tmp.id_master = tmg.id_master', 'inner');							 		 
			 
		if ($id_company != "all") :			 		  
			$this->db->where('tmp.id_company',$id_company);
		endif;
			 	
		if ($id_dept != "all") :			 		  
			$this->db->where('tmp.id_dept',$id_dept);
		endif;	
			 
		if ($status_bpk != "all") :			 		  
			$this->db->where('tmp.flag_bpk','1');
		endif;	
			 
		$this->db->where('tmp.status','1');			 			 			
        $this->db->where('tmg.date_grn BETWEEN "'. $date_start. '" and "'.$date_end.'"');
        $this->db->where('tmg.gran_total >=', '50000001');				
		$this->db->order_by('tmg.date_grn','asc');
		$result = $this->db->get();	
			 
		 if ($result->num_rows() > 0){
				 return  $result->result_array();
		 }else{
				 return null ;
		 } 			 				
    }

}