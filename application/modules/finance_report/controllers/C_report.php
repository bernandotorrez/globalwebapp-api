<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_report extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_report_pr');
        $this->load->model('M_report_po');
        $this->load->model('M_report_grn');
        $this->load->model('fincon_form/M_finance_controller');
        $this->load->model('M_report_excel');
    }

    public function index() {
        //echo '<pre>';print_r($this->session->userdata());die;
        $data['show_view'] = 'finance_report/V_report';

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        //$access = '0'
        $data['access'] = $access;
        $model  = 'Data PR';	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Report ". " : ".$model ;
        
        $model  = 'Data PO';						
        $data['header_po'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Report ". " : ".$model ;
        
        $model  = 'Data GRN';						
        $data['header_grn'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Report ". " : ".$model ;
        
        $data['tampil_company'] = $this->M_finance_controller->get_company()->result();
        $data['tampil_dept']=$this->M_finance_controller->get_dept()->result();	

        // Handle Report Action
            
        // Handle Report Action

		$this->load->view('dashboard/Template',$data);		
    }

    public function ajax_data_pr()
	{
		@$list = $this->M_report_pr->get_datatables();
		$data = array();
        @$no = $_POST['start'];
        $i = 0;
		foreach ($list as $data_pr) {
            
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$data_pr->id_master.' class="editRow ace" />
           <span class="lbl"></span> ';

            $gran_total = number_format($data_pr->gran_total, '0', '', ',');
            $gran_totalppn = number_format($data_pr->gran_totalppn, '0', '', ',');
            $date_send_aproval = date('d-m-Y', strtotime($data_pr->date_send_aproval));
            $id_master = '<span class="label label-primary arrowed-in-right arrowed">'.$data_pr->id_master.'</span>';

            if ($data_pr->flag_purchase == "1") {
                $type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
            }else{
                  if ($data_pr->flag_purchase == "2") {
                        $type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
                    }else{
                          if ($data_pr->flag_purchase == "3") {
                            $type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
                        }else{
                              if ($data_pr->flag_purchase == "4") {
                                    $type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
                                }else{
                                     $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
                              };
                      };
                  };   
              };

			$no++;
            $data[$i]['no'] = $no;
            $data[$i]['header_desc'] = $data_pr->header_desc;
            $data[$i]['id_master'] = $id_master;
            //$data[$i]['id_master'] = $data_pr->id_master;
            $data[$i]['user_submission'] = $data_pr->user_submission;
            $data[$i]['company'] = $data_pr->company;
            $data[$i]['dept'] = $data_pr->dept;
            $data[$i]['header_desc'] = $data_pr->header_desc;
            $data[$i]['vendor'] = $data_pr->vendor;
            $data[$i]['date_send_aproval'] = $date_send_aproval;
            $data[$i]['currency'] = $data_pr->currency;
            $data[$i]['type_purchase'] = $type_purchase;
            $data[$i]['gran_total'] = $gran_total;
            $data[$i]['gran_totalppn'] = $gran_totalppn;
            $data[$i]['term_top'] = $data_pr->term_top;
            $i++;
		}

		// $output = array(
		// 				"draw" => $_POST['draw'],
		// 				"recordsTotal" => $this->M_report_pr->count_all(),
		// 				"recordsFiltered" => $this->M_report_pr->count_filtered(),
		// 				"data" => $data,
        // 		);
        
        @$output = array('data' => @$data);

		echo json_encode($output);
    }
    
    public function getDataMasterPRByID() {
        $id = $this->input->get('id');
        $id = substr($id, 59, -7);
        @$data_master = $this->M_report_pr->getDataMasterPRByID($id)[0];
        @$data_detail = $this->M_report_pr->getDataDetailPRByID($id);

        $html  = '<div class="alert alert-info"><table cellpadding="0" cellspacing="0" width="50%"  class="table table-striped table-bordered table-hover">';
        
        $html .= '<tr>
            <th width="2%"> </th>
            <th style="text-align: center;" width="5%"> No </th>
            <th style="text-align: center;"> Item Type </th>
            <th style="text-align: center;"> Description </th>
            <th style="text-align: center;" width="5%"> Qty </th>
            <th style="text-align: center;"> Unit Price </th>
            <th style="text-align: center;"> Total </th>
        </tr>';
        $i = 1;
        foreach($data_detail as $row) { 

        $harga = number_format($row->harga, '0', '', ',');
        $total = number_format($row->total, '0', '', ',');

        $html .= '<tr>
            <td> </td>
            <td style="text-align: center;"> '.$i++.' </td>
            <td> '.$row->desc.' </td>
            <td> '.$row->spec.' </td>
            <td style="text-align: center;"> '.$row->qty.' </td>
            <td style="text-align: right;"> '.$harga.' </td>
            <td style="text-align: right;"> '.$total.' </td>
        </tr>';

        }  

        $total = number_format($data_master->gran_total, '0', '', ',');
        $ppn = number_format($data_master->ppn, '0', '', ',');
        $pph = number_format($data_master->pph, '0', '', ',');
        $gran_total = number_format($data_master->gran_totalppn, '0', '', ',');

        $html .= '<tr>
        <td colspan="6" style="text-align: right;"> Total : '.$data_master->currency.' </td>
        <td colspan="1" style="text-align: right;"> '.$total.' </td>
        </tr>
        <tr>
        <td colspan="6" style="text-align: right;"> Grand Total : '.$data_master->currency.' </td>
        <td colspan="1" style="text-align: right;"> '.$gran_total.' </td>
        </tr>
        ';

        $html .= '</table></div>';

        echo json_encode(@$html);
    }
    
    public function ajax_data_po()
	{
		@$list = $this->M_report_po->get_datatables();
		$data = array();
        @$no = $_POST['start'];
        $i = 0;
		foreach ($list as $data_po) {
            
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$data_po->id_master.' class="editRow ace" />
           <span class="lbl"></span> ';

            $gran_total = number_format($data_po->gran_total, '0', '', ',');
            $gran_totalppn = number_format($data_po->gran_totalppn, '0', '', ',');
            $date_send_aproval = date('d-m-Y', strtotime($data_po->date_print_po));
            $id_master = '<span class="label label-primary arrowed-in-right arrowed">'.$data_po->id_master.'</span>';
            $po_no_created = '<span class="label label-success arrowed-in-right arrowed">'.$data_po->po_no_created.'</span>';

            if ($data_po->flag_purchase == "1") {
                $type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
            }else{
                  if ($data_po->flag_purchase == "2") {
                        $type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
                    }else{
                          if ($data_po->flag_purchase == "3") {
                            $type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
                        }else{
                              if ($data_po->flag_purchase == "4") {
                                    $type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
                                }else{
                                     $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
                              };
                      };
                  };   
              };

			$no++;
            $data[$i]['no'] = $no;
            $data[$i]['header_desc'] = $data_po->header_desc;
            $data[$i]['id_master'] = $id_master;
            $data[$i]['po_no_created'] = $po_no_created;
            //$data[$i]['id_master'] = $data_pr->id_master;
            $data[$i]['user_submission'] = $data_po->user_submission;
            $data[$i]['company'] = $data_po->company;
            $data[$i]['dept'] = $data_po->dept;
            $data[$i]['header_desc'] = $data_po->header_desc;
            $data[$i]['vendor'] = $data_po->vendor;
            $data[$i]['date_send_aproval'] = $date_send_aproval;
            $data[$i]['currency'] = $data_po->currency;
            $data[$i]['type_purchase'] = $type_purchase;
            $data[$i]['gran_total'] = $gran_total;
            $data[$i]['gran_totalppn'] = $gran_totalppn;
            $data[$i]['term_top'] = $data_po->term_top;
            $i++;
		}

		// $output = array(
		// 				"draw" => $_POST['draw'],
		// 				"recordsTotal" => $this->M_report_pr->count_all(),
		// 				"recordsFiltered" => $this->M_report_pr->count_filtered(),
		// 				"data" => $data,
        // 		);
        
        @$output = array('data' => @$data);

		echo json_encode($output);
    }

    public function getDataMasterPOByID() {
        $id = $this->input->get('id');
        $id = substr($id, 59, -7);
        @$data_master = $this->M_report_po->getDataMasterPOByID($id)[0];
        @$data_detail = $this->M_report_po->getDataDetailPOByID($id);

        $html  = '<div class="alert alert-info"><table cellpadding="0" cellspacing="0" width="100%"  class="table table-striped table-bordered table-hover">';
        
        $html .= '<tr>
            <th width="2%"> </th>
            <th style="text-align: center;" width="5%"> No </th>
            <th style="text-align: center;" width="18%"> Item Type </th>
            <th style="text-align: center;" width="18%"> Description </th>
            <th style="text-align: center;" width="5%"> Qty </th>
            <th style="text-align: center;" width="18%"> Unit Price </th>
            <th style="text-align: center;" width="18%"> Total </th>
            <th style="text-align: center;" width="18%"> PPN </th>
        </tr>';
        $i = 1;
        foreach($data_detail as $row) { 

        $harga = number_format($row->harga, '0', '', ',');
        $total = number_format($row->total, '0', '', ',');
        $ppn = number_format($row->tax_detail, '0', '', ',');

        $html .= '<tr>
            <td> </td>
            <td style="text-align: center;"> '.$i++.' </td>
            <td> '.$row->desc.' </td>
            <td> '.$row->spec.' </td>
            <td style="text-align: center;"> '.$row->qty.' </td>
            <td style="text-align: right;"> '.$harga.' </td>
            <td style="text-align: right;"> '.$total.' </td>
            <td style="text-align: right;"> '.$ppn.' </td>
        </tr>';

        }

        $total = number_format($data_master->gran_total, '0', '', ',');
        $ppn = number_format($data_master->ppn, '0', '', ',');
        $pph = number_format($data_master->pph, '0', '', ',');
        $gran_total = number_format($data_master->gran_totalppn, '0', '', ',');

        $html .= '<tr>
        <td colspan="7" style="text-align: right;"> Total : '.$data_master->currency.' </td>
        <td colspan="1" style="text-align: right;"> '.$total.' </td>
        </tr>
        <tr>
        <td colspan="7" style="text-align: right;"> PPN : '.$data_master->currency.' </td>
        <td colspan="1" style="text-align: right;"> '.$ppn.' </td>
        </tr>
        <tr>
        <td colspan="7" style="text-align: right;"> Grand Total : '.$data_master->currency.' </td>
        <td colspan="1" style="text-align: right;"> '.$gran_total.' </td>
        </tr>
        ';

        $html .= '</table></div>';

        echo json_encode(@$html);
    }

    public function ajax_data_grn()
	{
		@$list = $this->M_report_grn->get_datatables();
		$data = array();
        @$no = $_POST['start'];
        $i = 0;
		foreach ($list as $data_grn) {
            
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$data_grn->number_grn.' class="editRow ace" />
           <span class="lbl"></span> ';

            $gran_total = number_format($data_grn->total_amount, '0', '', ',');
            $gran_totalppn = $data_grn->total_amount + $data_grn->total_ppn;
            $gran_totalppn = number_format($gran_totalppn, '0', '', ',');
            $total_ppn = number_format($data_grn->total_ppn, '0', '', ',');
            $date_grn = date('d-m-Y', strtotime($data_grn->date_grn));
            $id_master = '<span class="label label-primary arrowed-in-right arrowed">'.$data_grn->id_master.'</span>';
            $number_grn = '<span class="label label-success arrowed-in-right arrowed">'.$data_grn->number_grn.'</span>';
            
			$no++;
            $data[$i]['no'] = $no;
            $data[$i]['id_master'] = $id_master;
            $data[$i]['number_grn'] = $number_grn;
            $data[$i]['date_grn'] = $date_grn;
            $data[$i]['user_submission'] = $data_grn->user_submission;
            $data[$i]['gran_total'] = $gran_totalppn;
            $data[$i]['total_ppn'] = $total_ppn;
            $data[$i]['total_amount'] = $gran_total;
            $i++;
		}

		// @$output = array(
		// 				"draw" => $_POST['draw'],
		// 				"recordsTotal" => $this->M_report_grn->count_all(),
		// 				"recordsFiltered" => $this->M_report_grn->count_filtered(),
		// 				"data" => @$data,
        // 		);
        
        @$output = array('data' => @$data);

		echo json_encode($output);
    }

    public function getDataMasterGRNByID() {
        $id = $this->input->get('id');
        $id = substr($id, 59, -7);
        @$data_master = $this->M_report_grn->getDataMasterGRNByID($id)[0];
        @$data_master_po = $this->M_report_grn->getDataMasterPOByID($data_master->id_master)[0];
        @$data_detail = $this->M_report_grn->getDataDetailGRNByID($id);

        $html  = '<div class="alert alert-info"><table cellpadding="0" cellspacing="0" width="100%"  class="table table-striped table-bordered table-hover">';
        
        $html .= '<tr>
            <th width="2%"> </th>
            <th style="text-align: center;" width="3%"> No </th>
            <th style="text-align: center;" width="11%"> Item Type </th>
            <th style="text-align: center;" width="11%"> Description </th>
            <th style="text-align: center;" width="5%"> Qty </th>
            <th style="text-align: center;" width="11%"> Unit Price </th>
            <th style="text-align: center;" width="11%"> Total </th>
            <th style="text-align: center;" width="5%"> Qty Receipt </th>
            <th style="text-align: center;" width="11%"> Total </th>
            <th style="text-align: center;" width="5%"> Qty Outstd </th>
            <th style="text-align: center;" width="11%"> Remaining Payment </th>
            <th style="text-align: center;" width="11%"> PPN </th>
        </tr>';
        $i = 1;
        foreach($data_detail as $row) { 
        
        $amount_in = number_format($row->amount_in, '0', '', ',');
        $price = number_format($row->price, '0', '', ',');
        $amount = number_format($row->amount, '0', '', ',');
        $amount_received = number_format($row->amount_received, '0', '', ',');
        $remaining_price = number_format($row->remaining_price, '0', '', ',');
        //$ppn = $row->amount_received + $row->tax_ppn;
        $ppn = number_format($row->tax_ppn, '0', '', ',');

        $html .= '<tr>
            <td> </td>
            <td style="text-align: center;"> '.$i++.' </td>
            <td> '.$row->description.' </td>
            <td> '.$row->spec.' </td>
            <td style="text-align: center;"> '.$row->qty_begin.' </td>
            <td style="text-align: right;"> '.$price.' </td>
            <td style="text-align: right;"> '.$amount.' </td>
            <td style="text-align: center;"> '.$row->qty_received.' </td>
            <td style="text-align: right;"> '.$amount_in.' </td>
            <td style="text-align: center;"> '.$row->qty_outstanding.' </td>
            <td style="text-align: right;"> '.$remaining_price.' </td>
            <td style="text-align: right;"> '.$ppn.' </td>
        </tr>';

        }

        $total = number_format($data_master->total_amount, '0', '', ',');
        $ppn = number_format($data_master->total_ppn, '0', '', ',');
        $pph = number_format($data_master->total_pph, '0', '', ',');
        $gran_total = $data_master->total_amount + $data_master->total_ppn;
        $gran_total = number_format($gran_total, '0', '', ',');

        $html .= '<tr>
        <td colspan="11" style="text-align: right;"> Total : '.$data_master_po->currency.' </td>
        <td colspan="1" style="text-align: right;"> '.$total.' </td>
        </tr>
        <tr>
        <td colspan="11" style="text-align: right;"> PPN : '.$data_master_po->currency.' </td>
        <td colspan="1" style="text-align: right;"> '.$ppn.' </td>
        </tr>
        <tr>
        <td colspan="11" style="text-align: right;"> Grand Total : '.$data_master_po->currency.' </td>
        <td colspan="1" style="text-align: right;"> '.$gran_total.' </td>
        </tr>
        ';

        $html .= '</table></div>';

        echo json_encode(@$html);
    }

    function reportExcelTotal() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotal($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_header_pr.xls";		
		$this->load->view('finance_report/Excel_header_pr',$data);
    }

    function reportExcelDetail() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotal($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 	
        $data['file_name'] = "report_excel_detail_pr.xls";	
		$this->load->view('finance_report/Excel_detail_pr',$data);
    }

    function reportExcelUnder50() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUnder50($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_header_pr_under50.xls";		
		$this->load->view('finance_report/Excel_header_pr',$data);
    }

    function reportExcelDetailUnder50() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUnder50($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_detail_pr_under50.xls";		
		$this->load->view('finance_report/Excel_detail_pr',$data);
    }

    function reportExcelUpper50() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUpper50($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_header_pr_upper50.xls";		
		$this->load->view('finance_report/Excel_header_pr',$data);
    }

    function reportExcelDetailUpper50() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUpper50($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_detail_pr_upper50.xls";		
		$this->load->view('finance_report/Excel_detail_pr',$data);
    }

    // PO
    function reportExcelTotalPO() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalPO($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_header_po.xls";		
		$this->load->view('finance_report/Excel_header_po',$data);
    }

    function reportExcelDetailPO() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalPO($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 	
        $data['file_name'] = "report_excel_detail_po.xls";	
		$this->load->view('finance_report/Excel_detail_po',$data);
    }

    function reportExcelUnder50PO() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUnder50PO($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_header_po_under50.xls";		
		$this->load->view('finance_report/Excel_header_po',$data);
    }

    function reportExcelDetailUnder50PO() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUnder50PO($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_detail_po_under50.xls";		
		$this->load->view('finance_report/Excel_detail_po',$data);
    }

    function reportExcelUpper50PO() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUpper50PO($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_header_po_upper50.xls";		
		$this->load->view('finance_report/Excel_header_po',$data);
    }

    function reportExcelDetailUpper50PO() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUpper50PO($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_detail_po_upper50.xls";		
		$this->load->view('finance_report/Excel_detail_po',$data);
    }

    // GRN
    function reportExcelTotalGRN() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalGRN($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_header_grn.xls";		
		$this->load->view('finance_report/Excel_header_grn',$data);
    }

    function reportExcelDetailGRN() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalGRN($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 	
        $data['file_name'] = "report_excel_detail_grn.xls";	
		$this->load->view('finance_report/Excel_detail_grn',$data);
    }

    function reportExcelUnder50GRN() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUnder50GRN($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_header_grn_under50.xls";		
		$this->load->view('finance_report/Excel_header_grn',$data);
    }

    function reportExcelDetailUnder50GRN() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUnder50GRN($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_detail_grn_under50.xls";		
		$this->load->view('finance_report/Excel_detail_grn',$data);
    }

    function reportExcelUpper50GRN() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUpper50GRN($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_header_grn_upper50.xls";		
		$this->load->view('finance_report/Excel_header_grn',$data);
    }

    function reportExcelDetailUpper50GRN() {	 
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));

        $id_company = $this->input->get('id_company');
        $id_dept = $this->input->get('id_dept');
        $status_bpk = $this->input->get('status_bpk');

		$data_excel =  $this->M_report_excel->reportExcelTotalUpper50GRN($date_start, $date_end, $id_company, $id_dept, $status_bpk);									  			
		$data['intno'] = "" ; 
		$data['intbuff1'] = "" ; 		
        $data['data_excel'] = $data_excel ; 
        $data['file_name'] = "report_excel_detail_grn_upper50.xls";		
		$this->load->view('finance_report/Excel_detail_grn',$data);
    }

}