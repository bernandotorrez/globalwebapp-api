<?php 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=".$file_name);
	header("Pragma: no-cache");
	header("Expires: 0");
?>
<table border='1' width="70%">
    <tr bgcolor="#C5C5C5" style="font-weight:bold; alignment-adjust:middle">
    <td>PR Number</td>
    <td>GRN Number</td>
    <td>Company</td>
    <td>Branch</td>		
    <td>Dept</td>		
    <td>Vendor</td>		
    <td>Item Type</td>
    <td>Term</td>   
    <td>Purchase Type </td>
    <td>Currency</td> 
    <td>Total</td> 
    <td>PPN</td>  
    <td>PPH</td> 
    <td>Grand Total </td>
    <td>Date GRN</td>		
    <td>User Submission</td> 
    <td>PR Status</td> 
    <td>BPK Status</td> 
    <td>Ref No</td> 
    <td>Remarks</td>   
    </tr>
	
	<?php if(!empty($data_excel)) : ?>
	  <?php foreach($data_excel as $item) {
            $idmaster = $item['id_master'] ;
            $number_grn = $item['number_grn'] ;
    	?>
            <tr style="background:#FF9">
            <td><?=$item['id_master']?></td>
                <td><?=$item['number_grn']?></td>
                <td><?=$item['company']?></td>
                <td><?=$item['name_branch']?></td>
                <td><?=$item['dept']?></td>
                <td><?=$item['vendor']?></td>                                        
                <td><?=$item['header_desc']?></td>  
                <td><?=$item['term_top']?></td>                
                <td><?=$item['type_purchase']?></td>
                <td><?=$item['currency']?></td> 
                <td style="color:#900;font-weight:bold"><?=number_format($item['total_amount'],2,'.',',')?></td>	
                <td style="color:#900;font-weight:bold"><?=number_format($item['total_ppn'],2,'.',',')?></td>	
                <td style="color:#900;font-weight:bold"><?=number_format($item['total_pph'],2,'.',',')?></td>	
                <td style="color:#900;font-weight:bold"><?=number_format($item['gran_total'],2,'.',',')?></td>						                              
                <td><?=$item['date_grn']?></td>
                <td><?=$item['user_submission']?></td>
                <td><?php					 
						 if ($item['flag_fpb'] != "1") :
							 echo '<label style="color:#EFE903">Open<label>';
						 else :	 
							 echo '<label style="color:blue">FPB<label>';
						 endif ;					
					?>
                 </td>   
                <td>
                   <?php
				   		 if ($item['flag_print_bpk'] == "1") :						  
					   			  echo '<label style="color:red">BPK<label>';
					        else :	
							   if ($item['flag_bpk'] == "1") :						  
								  echo '<label style="color:red">BPK<label>';
							   else :	  
							      echo "Waiting BPK";  
							   endif;
						 endif;  	
                   	?>
                </td> 	
                <td></td>
                <td><?=$item['remarks']?></td>                                               
            </tr>
              <tr style="background:#EFEFEF;font-weight:bold; font-size:13px;">
                <td width="5%">No</td>
                <td width="28%">Item Type</td>
                <td width="28%">Description</td>
                <td width="5%">Qty</td>
                <td width="20%">Unit Price</td>							
                <td width="20%">Total Price</td>
                <td width="5%">Qty Receipt</td>
                <td width="20%">Total Receipt</td>
                <td width="5%">Qty Ourstd</td>
                <td width="20%">Remaining Payment</td>
                <td width='7%'>PPN</td>
             	  <td width='25%'>PPN Amount</td>
                <td width='25%'>Subtotal</td>
            </tr>
            <?php 
		     $str_detail = $this->db->query("select * from tbl_detail_grn where number_grn ='".$number_grn."' ");	  					 			 			
			 
			 $intbuff1 = 1;						
			 foreach ($str_detail->result_array()  as $item_detail)  { 			   					
			  
			   $intno = $intbuff1 ;		   
               $intbuff1 = $intno+1;
               $amount_ppn = $item_detail['amount_in'] + $item_detail['tax_ppn'];		
               $amount_pph = $item_detail['amount_in'] + $item_detail['tax_pph'];  
               $subtotal = $item_detail['amount_in'] + ($item_detail['tax_ppn'] - $item_detail['tax_pph']);
			 ?>
                <tr style="font-size:13px;">	
                  <td><?=$intno?></td>						                                          
                  <td><?=$item_detail['description']?></td>							  
                  <td><?=$item_detail['spec']?></td>																																																					
                  <td align="center"><?= number_format($item_detail['qty_begin'])?></td>							
                  <td><?= number_format($item_detail['price'],2,'.',',')?></td>							
                  <td style="color:#FF4246" ><?= number_format($item_detail['amount'],2,'.',',')?></td> 
                  <td align="center"><?= number_format($item_detail['qty_received'])?></td>	
                  <td style="color:#FF4246" ><?= number_format($item_detail['amount_in'],2,'.',',')?></td> 	
                  <td align="center"><?= number_format($item_detail['qty_outstanding'])?></td>	
                  <td style="color:#FF4246" ><?= number_format($item_detail['remaining_price'],2,'.',',')?></td>
                  <td ><?= number_format($item_detail['tax_ppn'],2,'.',',')?></td> 	 	
                  <td style="color:#FF4246"><?= number_format($amount_ppn,2,'.',',')?></td> 
                  <td style="color:#FF4246"><?= number_format($subtotal,2,'.',',')?></td>	
                </tr>                               
    		<?php 			   
			   } 			   			   				  
			 ?>
                 <tr>
                    <td colspan="12" style="text-align: right;">Total : <?=$item['currency'];?> &nbsp;</td>
                    <td style="color:#900;font-weight:bold"><?=number_format($item['total_amount'],2,'.',',')?></td>							
                </tr>
                <tr>
                    <td colspan="12" style="text-align: right;"> PPN : <?=$item['currency'];?> &nbsp; </td> 
                    <td style="color:#900;font-weight:bold"><?=number_format($item['total_ppn'],2,'.',',')?></td>							
                </tr>
                <tr>
                    <td colspan="12" style="text-align: right;"> PPH : <?=$item['currency'];?> &nbsp; </td> 
                    <td style="color:#900;font-weight:bold"><?=number_format($item['total_pph'],2,'.',',')?></td>							
                </tr>
                <tr>
                     <td colspan="12" style="text-align: right;">Grand Total : <?=$item['currency'];?> &nbsp; </td>
              		 <td style="color:#900;font-weight:bold"><?=number_format($item['gran_total'],2,'.',',')?></td>							
                </tr>
                <tr border="0" style="border-style: none;">
                  <td border="0" style="border-style: none;"></td>
                </tr>
		<?php } ?>
        
     <?php endif ?>   
</table>