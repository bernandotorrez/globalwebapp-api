<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>

<style>
  td.details-control {
    background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
}
</style>



<div>
		<button onclick="ReportPR()" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
        PR
		</button>
		<button onclick="ReportPO()" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
        PO
		</button>
    <button onclick="ReportGRN()" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
        GRN
		</button>
</div>
<br/>

<div class="table-header btn-info text-center"> REPORT PR </div>
<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">

<div class="form-group col-xs-6">
        <div class="col-xs-10">
            <div class="form-inline ">
                <div class="form-group">Date start</div>
                
                <div class="form-group">
                    <div class="input-group date">
                       <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                       </div>
                       
                       <input type="text" class="form-control input-daterange" id="start_date_pr" name="start_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                    </div> <!-- /.input-group datep -->       
                </div>
               
               <div class="form-group">
                       <label for="From" class="col-xs-1 control-label">To</label>
               </div>
              
               <div class="form-group">
                      <div class="input-group date">
                               <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                               </div>
                                     <input type="text" class="form-control input-daterange" id="end_date_pr" name="end_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                       </div>  
               </div>
                
            </div>
        </div>
      </div>

  <table id="myTable" cellpadding="0" cellspacing="0" width="100%"
    class="table table-striped table-bordered table-hover table-responsive">
    <thead class="text-warning">
      <!-- <th width="5%" style="text-align:center">
					<label class="pos-rel">
            <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
            <span class="lbl"></span>
          </label>
        </th> -->
      <!-- <th>No</th> -->
      <th width="5%"></th>
      <th>No</th>
      <th>PR No</th>
      <th>Requester</th>
      <th>Company</th>
      <th>Dept</th>
      <th>Item Type</th>
      <th>Vendor</th>
      <th>Date Sent</th>
      <th>Curr</th>
      <th>Type</th>
      <th>Total</th>
      <th>Total + PPN</th>
      <th>T.O.P</th>
    </thead>

  </table>

</div>


    <div class="space-30"></div>
  
    <!-- Table PO -->

    <div class="table-header btn-info text-center"> REPORT PO </div>
    <div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">

    <div class="form-group col-xs-6">
        <div class="col-xs-10">
            <div class="form-inline ">
                <div class="form-group">Date start</div>
                
                <div class="form-group">
                    <div class="input-group date">
                       <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                       </div>
                       
                       <input type="text" class="form-control input-daterange" id="start_date_po" name="start_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                    </div> <!-- /.input-group datep -->       
                </div>
               
               <div class="form-group">
                       <label for="From" class="col-xs-1 control-label">To</label>
               </div>
              
               <div class="form-group">
                      <div class="input-group date">
                               <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                               </div>
                                     <input type="text" class="form-control input-daterange" id="end_date_po" name="end_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                       </div>  
               </div>
                
            </div>
        </div>
      </div>

      <table id="myTablePO" cellpadding="0" cellspacing="0" width="100%"
        class="table table-striped table-bordered table-hover table-responsive">
        <thead class="text-warning">
          <!-- <th width="5%" style="text-align:center">
					<label class="pos-rel">
            <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
            <span class="lbl"></span>
          </label>
        </th> -->
          <!-- <th>No</th> -->
          <th width="5%"></th>
          <th>No</th>
          <th>PR No</th>
          <th>PO No</th>
          <th>Requester</th>
          <th>Company</th>
          <th>Dept</th>
          <th>Item Type</th>
          <th>Vendor</th>
          <th>Date Print PO</th>
          <th>Curr</th>
          <th>Type</th>
          <th>Total</th>
          <th>Total + PPN</th>
          <th>T.O.P</th>
        </thead>

      </table>

    </div>

    <!-- Table PO -->

    <div class="space-30"></div>
  
  <!-- Table GRN -->

  <div class="table-header btn-info text-center"> REPORT GRN </div>
  <div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">

  <div class="form-group col-xs-6">
        <div class="col-xs-10">
            <div class="form-inline ">
                <div class="form-group">Date start</div>
                
                <div class="form-group">
                    <div class="input-group date">
                       <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                       </div>
                       
                       <input type="text" class="form-control input-daterange" id="start_date_grn" name="start_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                    </div> <!-- /.input-group datep -->       
                </div>
               
               <div class="form-group">
                       <label for="From" class="col-xs-1 control-label">To</label>
               </div>
              
               <div class="form-group">
                      <div class="input-group date">
                               <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                               </div>
                                     <input type="text" class="form-control input-daterange" id="end_date_grn" name="end_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                       </div>  
               </div>
                
            </div>
        </div>
      </div>

    <table id="myTableGRN" cellpadding="0" cellspacing="0" width="100%"
      class="table table-striped table-bordered table-hover table-responsive">
      <thead class="text-warning">
        <!-- <th width="5%" style="text-align:center">
        <label class="pos-rel">
          <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
          <span class="lbl"></span>
        </label>
      </th> -->
        <!-- <th>No</th> -->
        <th width="5%"></th>
        <th>No</th>
        <th>PR No</th>
        <th>GRN No</th>
        <th>Requester</th>
        <th>Date GRN</th>
        <th>Total</th>
        <th>PPN</th>
        <th>Total + PPN</th>
      </thead>

    </table>

  </div>

  <!-- Table GRN -->

  <!-- Bootstrap modal PR -->
<div class="modal fade " id="report-pr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content modal-dialog-centered" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"> Report PR </h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <div class="widget-body">
                        <div class="widget-main">
                            <div>
                                <label for="date_start" style="color:#900">Date Start</label>
                            

                                <input id="date_start" name="date_start" readonly="" type="text" data-date-format="dd-mm-yyyy" class="form-control datepicker">
                            </div>
                            
                            <div>
                                <label for="date_end" style="color:#900">Date End</label>
                            

                                <input id="date_end" name="date_end" readonly="" type="text" data-date-format="dd-mm-yyyy" class="form-control datepicker">
                            </div>

                            <div>
                                <label for="company">Company</label>
                                <select id="id_company" name="id_company" class="form-control" >
                                <!-- <option value="all">- ALL -</option> --->
                                  <?php foreach($tampil_company as $rowcom){ ?>
                                  <option value="<?php echo $rowcom->id_company; ?>"><?php echo $rowcom->company ;?></option>
                                  <?php }?>
                                </select>
                            </div>
                              
                            <div>
                                <label for="company">Dept</label>
                                <select id="id_dept" name="id_dept"  class="form-control" >
                                  <?php  if($this->session->userdata('aproval_flag') =="3"){ ?>
                                    <option value="<?php echo $this->session->userdata('id_dept'); ?>"><?php echo $this->session->userdata('dept') ;?></option>
                                  <?php }else{ ?>
                                      <?php  if($this->session->userdata('aproval_flag') =="4"){ ?>		
                                            <?php foreach($tampil_dept as $rowdept){ ?>
                                          <option value="<?php echo $rowdept->id_dept; ?>"><?php echo $rowdept->dept ;?></option>  
                                            <?php }?> 	
                                      <?php }else{ ?> 
                                              <option value="all">- ALL -</option>
                                              <?php foreach($tampil_dept as $rowdept){ ?>
                                            <option value="<?php echo $rowdept->id_dept; ?>"><?php echo $rowdept->dept ;?></option> 
                                              <?php }?> 	
                                      <?php }?>
                                  <?php }?>
                                    </select>
                            </div>

                            <div>
                                <label for="date_end">Status BPK</label>
                            

                                <select id="status_bpk" name="status_bpk"  class="form-control" >
                                  <option value="all">- ALL -</option>
                                  <option value="1">BPK ONLY</option>
                                </select>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <div class="col-md-5 text-center">
                <a class="btn btn-white btn-info btn-bold" id="reportExcelTotal"
                  name="reportExcelTotal" value="reportExcelTotal" onClick="exportExcelTotal()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Excel Total
                </a>
              </div>

              <div class="col-md-5 col-md-offset-2 text-center">
                <a class="btn btn-white btn-danger btn-bold" id="reportExcelDetail"
                  name="reportExcelDetail" value="reportExcelDetail" onClick="exportExcelDetail()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Excel Detail
                </a>
              </div>
              
              <div class="space-24"></div>

              <div class="col-md-5 text-center">
                <a class="btn btn-white btn-warning btn-bold" id="reportExcelUnder50"
                  name="reportExcelUnder50" value="reportExcelUnder50" onClick="exportExcelUnder50()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total <= 50jt
                </a>
              </div>

              <div class="col-md-5 col-md-offset-2 text-center">

                <a class="btn btn-white btn-success btn-bold" id="reportExcelDetailUnder50" name="reportExcelDetailUnder50"
                  value="reportExcelDetailUnder50" onClick="exportExcelDetailUnder50()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total Detail <= 50jt
                </a>
              </div>

              <div class="space-24"></div>
              <div class="space-24"></div>
              
              <div class="col-md-5 text-center">
                <a class="btn btn-white btn-warning btn-bold" id="reportExcelUpper50"
                  name="reportExcelUpper50" value="reportExcelUpper50" onClick="exportExcelUpper50()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total >= 50jt
                </a>
              </div>

              <div class="col-md-5 col-md-offset-2 text-center">

                <a class="btn btn-white btn-success btn-bold" id="reportExcelDetailUpper50" name="reportExcelDetailUpper50"
                  value="reportExcelDetailUpper50" onClick="exportExcelDetailUpper50()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total Detail >= 50jt
                </a>
              </div>
              

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal PR -->

 <!-- Bootstrap modal PO -->
 <div class="modal fade " id="report-po" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content modal-dialog-centered" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"> Report PO </h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <div class="widget-body">
                        <div class="widget-main">
                            <div>
                                <label for="date_start" style="color:#900">Date Start</label>
                            

                                <input id="date_start_po" name="date_start_po" readonly="" type="text" data-date-format="dd-mm-yyyy" class="form-control datepicker">
                            </div>
                            
                            <div>
                                <label for="date_end" style="color:#900">Date End</label>
                            

                                <input id="date_end_po" name="date_end_po" readonly="" type="text" data-date-format="dd-mm-yyyy" class="form-control datepicker">
                            </div>

                            <div>
                                <label for="company">Company</label>
                                <select id="id_company_po" name="id_company_po" class="form-control" >
                                <!-- <option value="all">- ALL -</option> --->
                                  <?php foreach($tampil_company as $rowcom){ ?>
                                  <option value="<?php echo $rowcom->id_company; ?>"><?php echo $rowcom->company ;?></option>
                                  <?php }?>
                                </select>
                            </div>
                              
                            <div>
                                <label for="company">Dept</label>
                                <select id="id_dept_po" name="id_dept_po"  class="form-control" >
                                  <?php  if($this->session->userdata('aproval_flag') =="3"){ ?>
                                    <option value="<?php echo $this->session->userdata('id_dept'); ?>"><?php echo $this->session->userdata('dept') ;?></option>
                                  <?php }else{ ?>
                                      <?php  if($this->session->userdata('aproval_flag') =="4"){ ?>		
                                            <?php foreach($tampil_dept as $rowdept){ ?>
                                          <option value="<?php echo $rowdept->id_dept; ?>"><?php echo $rowdept->dept ;?></option>  
                                            <?php }?> 	
                                      <?php }else{ ?> 
                                              <option value="all">- ALL -</option>
                                              <?php foreach($tampil_dept as $rowdept){ ?>
                                            <option value="<?php echo $rowdept->id_dept; ?>"><?php echo $rowdept->dept ;?></option> 
                                              <?php }?> 	
                                      <?php }?>
                                  <?php }?>
                                    </select>
                            </div>

                            <div>
                                <label for="date_end">Status BPK</label>
                            

                                <select id="status_bpk_po" name="status_bpk_po"  class="form-control" >
                                  <option value="all">- ALL -</option>
                                  <option value="1">BPK ONLY</option>
                                </select>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <div class="col-md-5 text-center">
                <a class="btn btn-white btn-info btn-bold" id="reportExcelTotal"
                  name="reportExcelTotal" value="reportExcelTotal" onClick="exportExcelTotalPO()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Excel Total
                </a>
              </div>

              <div class="col-md-5 col-md-offset-2 text-center">
                <a class="btn btn-white btn-danger btn-bold" id="reportExcelDetail"
                  name="reportExcelDetail" value="reportExcelDetail" onClick="exportExcelDetailPO()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Excel Detail
                </a>
              </div>
              
              <div class="space-24"></div>

              <div class="col-md-5 text-center">
                <a class="btn btn-white btn-warning btn-bold" id="reportExcelUnder50"
                  name="reportExcelUnder50" value="reportExcelUnder50" onClick="exportExcelUnder50PO()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total <= 50jt
                </a>
              </div>

              <div class="col-md-5 col-md-offset-2 text-center">

                <a class="btn btn-white btn-success btn-bold" id="reportExcelDetailUnder50" name="reportExcelDetailUnder50"
                  value="reportExcelDetailUnder50" onClick="exportExcelDetailUnder50PO()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total Detail <= 50jt
                </a>
              </div>

              <div class="space-24"></div>
              <div class="space-24"></div>
              
              <div class="col-md-5 text-center">
                <a class="btn btn-white btn-warning btn-bold" id="reportExcelUpper50"
                  name="reportExcelUpper50" value="reportExcelUpper50" onClick="exportExcelUpper50PO()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total >= 50jt
                </a>
              </div>

              <div class="col-md-5 col-md-offset-2 text-center">

                <a class="btn btn-white btn-success btn-bold" id="reportExcelDetailUpper50" name="reportExcelDetailUpper50"
                  value="reportExcelDetailUpper50" onClick="exportExcelDetailUpper50PO()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total Detail >= 50jt
                </a>
              </div>
              

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal PO -->

<!-- Bootstrap modal GRN -->
<div class="modal fade " id="report-grn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content modal-dialog-centered" role="document">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"> Report GRN </h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <div class="widget-body">
                        <div class="widget-main">
                            <div>
                                <label for="date_start" style="color:#900">Date Start</label>
                            

                                <input id="date_start_grn" name="date_start_grn" readonly="" type="text" data-date-format="dd-mm-yyyy" class="form-control datepicker">
                            </div>
                            
                            <div>
                                <label for="date_end" style="color:#900">Date End</label>
                            

                                <input id="date_end_grn" name="date_end_grn" readonly="" type="text" data-date-format="dd-mm-yyyy" class="form-control datepicker">
                            </div>

                            <div>
                                <label for="company">Company</label>
                                <select id="id_company_grn" name="id_company_grn" class="form-control" >
                                <!-- <option value="all">- ALL -</option> --->
                                  <?php foreach($tampil_company as $rowcom){ ?>
                                  <option value="<?php echo $rowcom->id_company; ?>"><?php echo $rowcom->company ;?></option>
                                  <?php }?>
                                </select>
                            </div>
                              
                            <div>
                                <label for="company">Dept</label>
                                <select id="id_dept_grn" name="id_dept_grn"  class="form-control" >
                                  <?php  if($this->session->userdata('aproval_flag') =="3"){ ?>
                                    <option value="<?php echo $this->session->userdata('id_dept'); ?>"><?php echo $this->session->userdata('dept') ;?></option>
                                  <?php }else{ ?>
                                      <?php  if($this->session->userdata('aproval_flag') =="4"){ ?>		
                                            <?php foreach($tampil_dept as $rowdept){ ?>
                                          <option value="<?php echo $rowdept->id_dept; ?>"><?php echo $rowdept->dept ;?></option>  
                                            <?php }?> 	
                                      <?php }else{ ?> 
                                              <option value="all">- ALL -</option>
                                              <?php foreach($tampil_dept as $rowdept){ ?>
                                            <option value="<?php echo $rowdept->id_dept; ?>"><?php echo $rowdept->dept ;?></option> 
                                              <?php }?> 	
                                      <?php }?>
                                  <?php }?>
                                    </select>
                            </div>

                            <div>
                                <label for="date_end">Status BPK</label>
                            

                                <select id="status_bpk_grn" name="status_bpk_grn"  class="form-control" >
                                  <option value="all">- ALL -</option>
                                  <option value="1">BPK ONLY</option>
                                </select>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
              <div class="col-md-5 text-center">
                <a class="btn btn-white btn-info btn-bold" id="reportExcelTotal"
                  name="reportExcelTotal" value="reportExcelTotal" onClick="exportExcelTotalGRN()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Excel Total
                </a>
              </div>

              <div class="col-md-5 col-md-offset-2 text-center">
                <a class="btn btn-white btn-danger btn-bold" id="reportExcelDetail"
                  name="reportExcelDetail" value="reportExcelDetail" onClick="exportExcelDetailGRN()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Excel Detail
                </a>
              </div>
              
              <div class="space-24"></div>

              <div class="col-md-5 text-center">
                <a class="btn btn-white btn-warning btn-bold" id="reportExcelUnder50"
                  name="reportExcelUnder50" value="reportExcelUnder50" onClick="exportExcelUnder50GRN()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total <= 50jt
                </a>
              </div>

              <div class="col-md-5 col-md-offset-2 text-center">

                <a class="btn btn-white btn-success btn-bold" id="reportExcelDetailUnder50" name="reportExcelDetailUnder50"
                  value="reportExcelDetailUnder50" onClick="exportExcelDetailUnder50GRN()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total Detail <= 50jt
                </a>
              </div>

              <div class="space-24"></div>
              <div class="space-24"></div>
              
              <div class="col-md-5 text-center">
                <a class="btn btn-white btn-warning btn-bold" id="reportExcelUpper50"
                  name="reportExcelUpper50" value="reportExcelUpper50" onClick="exportExcelUpper50GRN()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total >= 50jt
                </a>
              </div>

              <div class="col-md-5 col-md-offset-2 text-center">

                <a class="btn btn-white btn-success btn-bold" id="reportExcelDetailUpper50" name="reportExcelDetailUpper50"
                  value="reportExcelDetailUpper50" onClick="exportExcelDetailUpper50GRN()">
                  <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>Report Total Detail >= 50jt
                </a>
              </div>  
              

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal GRN -->

    <script>

    $('.datepicker').datetimepicker({
      language: 'id',
      weekStart: 1,
      todayBtn: 1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0
    });
    // PR

    function exportExcelTotal() {
      var date_start = $('#date_start').val();
      var date_end = $('#date_end').val();
      var id_company = $('#id_company').val();
      var id_dept = $('#id_dept').val();
      var status_bpk = $('#status_bpk').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelTotal?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelDetail() {
      var date_start = $('#date_start').val();
      var date_end = $('#date_end').val();
      var id_company = $('#id_company').val();
      var id_dept = $('#id_dept').val();
      var status_bpk = $('#status_bpk').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelDetail?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelUnder50() {
      var date_start = $('#date_start').val();
      var date_end = $('#date_end').val();
      var id_company = $('#id_company').val();
      var id_dept = $('#id_dept').val();
      var status_bpk = $('#status_bpk').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelUnder50?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelDetailUnder50() {
      var date_start = $('#date_start').val();
      var date_end = $('#date_end').val();
      var id_company = $('#id_company').val();
      var id_dept = $('#id_dept').val();
      var status_bpk = $('#status_bpk').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelDetailUnder50?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelUpper50() {
      var date_start = $('#date_start').val();
      var date_end = $('#date_end').val();
      var id_company = $('#id_company').val();
      var id_dept = $('#id_dept').val();
      var status_bpk = $('#status_bpk').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelUpper50?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelDetailUpper50() {
      var date_start = $('#date_start').val();
      var date_end = $('#date_end').val();
      var id_company = $('#id_company').val();
      var id_dept = $('#id_dept').val();
      var status_bpk = $('#status_bpk').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelDetailUpper50?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }



    // PO
    function exportExcelTotalPO() {
      var date_start = $('#date_start_po').val();
      var date_end = $('#date_end_po').val();
      var id_company = $('#id_company_po').val();
      var id_dept = $('#id_dept_po').val();
      var status_bpk = $('#status_bpk_po').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelTotalPO?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelDetailPO() {
      var date_start = $('#date_start_po').val();
      var date_end = $('#date_end_po').val();
      var id_company = $('#id_company_po').val();
      var id_dept = $('#id_dept_po').val();
      var status_bpk = $('#status_bpk_po').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelDetailPO?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelUnder50PO() {
      var date_start = $('#date_start_po').val();
      var date_end = $('#date_end_po').val();
      var id_company = $('#id_company_po').val();
      var id_dept = $('#id_dept_po').val();
      var status_bpk = $('#status_bpk_po').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelUnder50PO?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelDetailUnder50PO() {
      var date_start = $('#date_start_po').val();
      var date_end = $('#date_end_po').val();
      var id_company = $('#id_company_po').val();
      var id_dept = $('#id_dept_po').val();
      var status_bpk = $('#status_bpk_po').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelDetailUnder50PO?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelUpper50PO() {
      var date_start = $('#date_start_po').val();
      var date_end = $('#date_end_po').val();
      var id_company = $('#id_company_po').val();
      var id_dept = $('#id_dept_po').val();
      var status_bpk = $('#status_bpk_po').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelUpper50PO?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelDetailUpper50PO() {
      var date_start = $('#date_start_po').val();
      var date_end = $('#date_end_po').val();
      var id_company = $('#id_company_po').val();
      var id_dept = $('#id_dept_po').val();
      var status_bpk = $('#status_bpk_po').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelDetailUpper50PO?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    // GRN
    function exportExcelTotalGRN() {
      var date_start = $('#date_start_grn').val();
      var date_end = $('#date_end_grn').val();
      var id_company = $('#id_company_grn').val();
      var id_dept = $('#id_dept_grn').val();
      var status_bpk = $('#status_bpk_grn').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelTotalGRN?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelDetailGRN() {
      var date_start = $('#date_start_grn').val();
      var date_end = $('#date_end_grn').val();
      var id_company = $('#id_company_grn').val();
      var id_dept = $('#id_dept_grn').val();
      var status_bpk = $('#status_bpk_grn').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelDetailGRN?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelUnder50GRN() {
      var date_start = $('#date_start_grn').val();
      var date_end = $('#date_end_grn').val();
      var id_company = $('#id_company_grn').val();
      var id_dept = $('#id_dept_grn').val();
      var status_bpk = $('#status_bpk_grn').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelUnder50GRN?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelDetailUnder50GRN() {
      var date_start = $('#date_start_grn').val();
      var date_end = $('#date_end_grn').val();
      var id_company = $('#id_company_grn').val();
      var id_dept = $('#id_dept_grn').val();
      var status_bpk = $('#status_bpk_grn').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelDetailUnder50GRN?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelUpper50GRN() {
      var date_start = $('#date_start_grn').val();
      var date_end = $('#date_end_grn').val();
      var id_company = $('#id_company_grn').val();
      var id_dept = $('#id_dept_grn').val();
      var status_bpk = $('#status_bpk_grn').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelUpper50GRN?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

    function exportExcelDetailUpper50GRN() {
      var date_start = $('#date_start_grn').val();
      var date_end = $('#date_end_grn').val();
      var id_company = $('#id_company_grn').val();
      var id_dept = $('#id_dept_grn').val();
      var status_bpk = $('#status_bpk_grn').val();

      window.open("<?=base_url('finance_report/C_report/reportExcelDetailUpper50GRN?date_start=');?>"+date_start+"&date_end="+date_end+"&id_company="+id_company+"&id_dept="+id_dept+"&status_bpk="+status_bpk);
    }

      var save_method; //for save method string
      var table;

      function ReportPR() {
          save_method = 'add';
          $('#report-pr').modal('show'); // show bootstrap modal
          
          //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title

          // Hilangkan semua value input ketika tombol edit di tekan pertama kali sebelum add
          // var id_increment = $('#id_increment').val();
          // $('#id_colour').val(id_increment);
          // $('#brand').val('');
          // $('#model').val('');
          // $('#type').val('');
          // $('#colour').val('');

      }

      function ReportPO() {
          save_method = 'add';
          $('#report-po').modal('show'); // show bootstrap modal
          
          //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title

          // Hilangkan semua value input ketika tombol edit di tekan pertama kali sebelum add
          // var id_increment = $('#id_increment').val();
          // $('#id_colour').val(id_increment);
          // $('#brand').val('');
          // $('#model').val('');
          // $('#type').val('');
          // $('#colour').val('');

      }

      function ReportGRN() {
          save_method = 'add';
          $('#report-grn').modal('show'); // show bootstrap modal
          
          //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title

          // Hilangkan semua value input ketika tombol edit di tekan pertama kali sebelum add
          // var id_increment = $('#id_increment').val();
          // $('#id_colour').val(id_increment);
          // $('#brand').val('');
          // $('#model').val('');
          // $('#type').val('');
          // $('#colour').val('');

      }
      
    </script>

   <script>


//$('#myTable').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
$(document).ready(function(){ 

  $('.input-daterange').datetimepicker({
      language: 'id',
      weekStart: 1,
      todayBtn: 1,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      minView: 2,
      forceParse: 0
    });

  fetch_data('no'); //

function fetch_data(is_date_search,start_date='', end_date=''){
var table = $('#myTable').DataTable({
  
   "autoWidth": true,
   "scrollY": '250',
  "scrollX": true,
   "processing": true, //Feature control the processing indicator.
   "lengthChange": false,
  "ajax":{
				    url: "<?php echo base_url('finance_report/C_report/ajax_data_pr') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },

        "columns": [
            {
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            { "data": "no" },
            { "data": "id_master" },
            { "data": "user_submission" },
            { "data": "company" },
            { "data": "dept" },
            { "data": "header_desc" },
            { "data": "vendor" },
            { "data": "date_send_aproval" },
            { "data": "currency" },
            { "data": "type_purchase" },
            { "data": "gran_total" },
            { "data": "gran_totalppn" },
            { "data": "term_top" },
        ],

  'order': [
    [2, 'ASC']
  ],
});
}

//proses change date-----------------------------------------------
$('#start_date_pr').change(function(){
			var start_date_pr =    $('#start_date_pr').val();
			var end_date_pr   =    $('#end_date_pr').val();
		
			if(start_date_pr != '' && end_date_pr != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date_pr_pr, end_date);
			} else if(start_date_pr == '' && end_date_pr == ''){ 
				$('#myTable').DataTable().destroy();
				fetch_data('no');
			}
		});
		
		$('#end_date_pr').change(function(){
			var start_date_pr =    $('#start_date_pr').val();
			var end_date_pr   =    $('#end_date_pr').val();
		
			if(start_date_pr != '' && end_date_pr != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date_pr, end_date_pr);		
			} else if(start_date_pr == '' && end_date_pr == ''){ 
				$('#myTable').DataTable().destroy();
				fetch_data('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	
 
 
 
 //onblur date-------------------------------------------------------------------------
 
 		// $('#start_date_pr').blur(function(){
		// 	var start_date_pr =    $('#start_date_pr').val();
		// 	var end_date_pr   =    $('#end_date_pr').val();
		
		// 	if(start_date_pr == '' || end_date_pr == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		  fetch_data('no', start_date_pr, end_date_pr);
		// 	} 
			
		// });
 
 
 	
		// $('#end_date_pr').blur(function(){
		// 	var start_date_pr =    $('#start_date_pr').val();
		// 	var end_date_pr   =    $('#end_date_pr').val();
			
		// 	if(start_date_pr == '' || end_date_pr == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		 fetch_data('no', start_date_pr, end_date_pr);
		// 	} 
		// });
 //end onblur-------------------------------------	

})

function format ( rowData ) {
    var div = $('<div/>')
        .addClass( 'loading' )
        .html( '<div class="alert alert-info text-center"> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <span class="sr-only">Loading...</span></div>');
 
    $.ajax( {
        url: "<?php echo site_url('finance_report/C_report/getDataMasterPRByID?id=')?>" + rowData.id_master,
        dataType: 'json',
        success: function ( json ) {
            div
                .html( json )
                .removeClass( 'loading' );
        }
    } );
 
    return div;
}

$('#myTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
        
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
				
//end--------------------------------------------------------------


// Table PO
$(document).ready(function(){ 

  
  fetch_data_po('no'); //

  function fetch_data_po(is_date_search,start_date='', end_date=''){
var tablePO = $('#myTablePO').DataTable({

  "autoWidth": true,
  "scrollY": '250',
  "scrollX": true,
  "processing": true, //Feature control the processing indicator.
  "lengthChange": false,
  //"ajax": "<?php echo base_url('finance_report/C_report/ajax_data_po') ?>",
  "ajax":{
				    url: "<?php echo base_url('finance_report/C_report/ajax_data_po') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
  "columns": [{
      "className": 'details-control',
      "orderable": false,
      "data": null,
      "defaultContent": ''
    },
    {
      "data": "no"
    },
    {
      "data": "id_master"
    },
    {
      "data": "po_no_created"
    },
    {
      "data": "user_submission"
    },
    {
      "data": "company"
    },
    {
      "data": "dept"
    },
    {
      "data": "header_desc"
    },
    {
      "data": "vendor"
    },
    {
      "data": "date_send_aproval"
    },
    {
      "data": "currency"
    },
    {
      "data": "type_purchase"
    },
    {
      "data": "gran_total"
    },
    {
      "data": "gran_totalppn"
    },
    {
      "data": "term_top"
    },
  ],

  'order': [
    [2, 'ASC']
  ],
});

//proses change date-----------------------------------------------
$('#start_date_po').change(function(){
			var start_date_po =    $('#start_date_po').val();
			var end_date_po   =    $('#end_date_po').val();
		
			if(start_date_po != '' && end_date_po != ''){
				 $('#myTablePO').DataTable().destroy();
				 fetch_data_po('yes', start_date_po_po, end_date);
			} else if(start_date_po == '' && start_date_po == ''){ 
				$('#myTablePO').DataTable().destroy();
				fetch_data_po('no');
			}
		});
		
		$('#end_date_po').change(function(){
			var start_date_po =    $('#start_date_po').val();
			var end_date_po   =    $('#end_date_po').val();
		
			if(start_date_po != '' && end_date_po != ''){
				$('#myTablePO').DataTable().destroy();
        fetch_data_po('yes', start_date_po, end_date_po);		
			} else if(start_date_po == '' && start_date_po == ''){ 
				$('#myTablePO').DataTable().destroy();
				fetch_data_po('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	
 
 
 
 //onblur date-------------------------------------------------------------------------
 
 		// $('#start_date_po').blur(function(){
		// 	var start_date_po =    $('#start_date_po').val();
		// 	var end_date_po   =    $('#end_date_po').val();
		
		// 	if(start_date_po == '' || end_date_po == ''){
		// 		$('#myTablePO').DataTable().destroy();
    //     fetch_data_po('no', start_date_po, end_date_po);
		// 	} 
			
		// });
 
 
 	
		// $('#end_date_po').blur(function(){
		// 	var start_date_po =    $('#start_date_po').val();
		// 	var end_date_po   =    $('#end_date_po').val();
			
		// 	if(start_date_po == '' || end_date_po == ''){
		// 		$('#myTablePO').DataTable().destroy();
    //     fetch_data_po('no', start_date_po, end_date_po);
		// 	} 
		// });
 //end onblur-------------------------------------	


function formatPO(rowData) {
  var div = $('<div/>')
        .addClass( 'loading' )
        .html( '<div class="alert alert-info text-center"> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <span class="sr-only">Loading...</span></div>');

  $.ajax({
    url: "<?php echo site_url('finance_report/C_report/getDataMasterPOByID?id=')?>" + rowData.id_master,
    dataType: 'json',
    success: function (json) {
      div
        .html(json)
        .removeClass('loading');
    }
  });

  return div;
}

$('#myTablePO tbody').on('click', 'td.details-control', function () {
  var tr = $(this).closest('tr');
  var row = tablePO.row(tr);


  if (row.child.isShown()) {
    // This row is already open - close it
    row.child.hide();
    tr.removeClass('shown');
  } else {
    // Open this row
    row.child(formatPO(row.data())).show();
    tr.addClass('shown');
  }
});
  }
});

// Table PO

// Table GRN
$(document).ready(function(){ 

  
  fetch_data_grn('no'); //

function fetch_data_grn(is_date_search,start_date='', end_date=''){
var tableGRN = $('#myTableGRN').DataTable({

"autoWidth": true,
"scrollY": '250',
"scrollX": true,
"processing": true, //Feature control the processing indicator.

//"ajax": "<?php echo base_url('finance_report/C_report/ajax_data_grn') ?>",
"lengthChange": false,
  "ajax":{
				    url: "<?php echo base_url('finance_report/C_report/ajax_data_grn') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
"columns": [{
    "className": 'details-control',
    "orderable": false,
    "data": null,
    "defaultContent": ''
  },
  {
    "data": "no"
  },
  {
    "data": "id_master"
  },
  {
    "data": "number_grn"
  },
  {
    "data": "user_submission"
  },
  {
    "data": "date_grn"
  },
  {
    "data": "total_amount"
  },
  {
    "data": "total_ppn"
  },
  {
    "data": "gran_total"
  },
],

'order': [
  [2, 'ASC']
],
});

//proses change date-----------------------------------------------
$('#start_date_grn').change(function(){
			var start_date_grn =    $('#start_date_grn').val();
			var end_date_grn   =    $('#end_date_grn').val();
		
			if(start_date_grn != '' && end_date_grn != ''){
				 $('#myTableGRN').DataTable().destroy();
				 fetch_data_grn('yes', start_date_grn_grn, end_date);
			} else if(start_date_grn == '' && start_date_grn == ''){ 
				$('#myTableGRN').DataTable().destroy();
				fetch_data_grn('no');
			}
		});
		
		$('#end_date_grn').change(function(){
			var start_date_grn =    $('#start_date_grn').val();
			var end_date_grn   =    $('#end_date_grn').val();
		
			if(start_date_grn != '' && end_date_grn != ''){
				$('#myTableGRN').DataTable().destroy();
        fetch_data_grn('yes', start_date_grn, end_date_grn);		
			} else if(start_date_grn == '' && start_date_grn == ''){ 
				$('#myTableGRN').DataTable().destroy();
				fetch_data_grn('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	
 
 
 
 //onblur date-------------------------------------------------------------------------
 
 		// $('#start_date_grn').blur(function(){
		// 	var start_date_grn =    $('#start_date_grn').val();
		// 	var end_date_grn   =    $('#end_date_grn').val();
		
		// 	if(start_date_grn == '' || end_date_grn == ''){
		// 		$('#myTableGRN').DataTable().destroy();
    //     fetch_data_grn('no', start_date_grn, end_date_grn);
		// 	} 
			
		// });
 
 
 	
		// $('#end_date_grn').blur(function(){
		// 	var start_date_grn =    $('#start_date_grn').val();
		// 	var end_date_grn   =    $('#end_date_grn').val();
			
		// 	if(start_date_grn == '' || end_date_grn == ''){
		// 		$('#myTableGRN').DataTable().destroy();
    //     fetch_data_grn('no', start_date_grn, end_date_grn);
		// 	} 
		// });
 //end onblur-------------------------------------	

function formatGRN(rowData) {
  var div = $('<div/>')
        .addClass( 'loading' )
        .html( '<div class="alert alert-info text-center"> <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> <span class="sr-only">Loading...</span></div>');

$.ajax({
  url: "<?php echo site_url('finance_report/C_report/getDataMasterGRNByID?id=')?>" + rowData.number_grn,
  dataType: 'json',
  success: function (json) {
    div
      .html(json)
      .removeClass('loading');
  }
});

return div;
}

$('#myTableGRN tbody').on('click', 'td.details-control', function () {
var tr = $(this).closest('tr');
var row = tableGRN.row(tr);


if (row.child.isShown()) {
  // This row is already open - close it
  row.child.hide();
  tr.removeClass('shown');
} else {
  // Open this row
  row.child(formatGRN(row.data())).show();
  tr.addClass('shown');
}
});
  }
});

// Table GRN

	  
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>


<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>


	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
    function add_model()
    {
      save_method = 'add';
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title

      // Hilangkan semua value input ketika tombol edit di tekan pertama kali sebelum add
      var id_increment = $('#id_increment').val();
      $('#id_model').val(id_increment);
      $('#brand_name').val('');
      $('#model_name').val('');

      var access = $('#access').val();

      if(access == '1') {
        $('#company').val('');
        $('#branch').val('');
      }

      
    }

    function edit_model(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('demo_car/C_demo_car/ajax_edit/')?>/" + eee,
		    type: "GET",
        dataType: "JSON",
        success: function(data)
        {

          $('#branch-input').html(data.html_branch);

          $('#id_model').val(data.data[0].id_model);
          $('#brand_name').val(data.data[0].id_brand);
          $('#model_name').val(data.data[0].model_name);
          $('#company').val(data.data[0].id_company);
          $('#branch').val(data.data[0].id_branch);
            
          $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
          $('.modal-title').text('Edit Model Demo Car Form'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

	
    function save()
    {
      var url, action;
      if(save_method == 'add')
      {
        url = "<?php echo site_url('demo_car/C_demo_car/add_model')?>";
        action = 'Tambah';
      }
      else
      {
		    url = "<?php echo site_url('demo_car/C_demo_car/update_model')?>";
        action = 'Ubah';
      }

      var brand_name = $('#brand_name').val();
      var model_name = $('#model_name').val();

      var access = $('#access').val();

      if(access == '0') {
        // Client Side Validation
      if(brand_name == '') {
        alert('Silahkan Pilih Brand Name');
        $('#brand_name').focus();
      } else if(model_name == '' || model_name.trim() == '') {
        alert('Silahkan Isi Model Name');
        $('#model_name').focus();
      
      } else {
        var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
            processData: false,															
            async: false,
            processData: false,
            contentType: false,		
            cache : false,	
            beforeSend: function()
            { 
              $("#btnSave").prop('disabled', true);
            },								
            success: function (data, textStatus, jqXHR)            
            {

              if(data=='Insert' || data=='Update'){
                $('#modal_form').modal('hide');
                location.reload();// for reload a page
              } else if(data=='Insert Gagal' || data=='Update Gagal') {
                alert('Data Gagal di '+action);
              } else {
                alert (data);
              }

              $("#btnSave").prop('disabled', false);
            },
            });
      }
      } else {
        // Client Side Validation
        var branch = $('#branch').val();
        var company = $('#company').val();
      if(brand_name == '') {
        alert('Silahkan Pilih Brand Name');
        $('#brand_name').focus();
      } else if(model_name == '' || model_name.trim() == '') {
        alert('Silahkan Isi Model Name');
        $('#model_name').focus();
      
      } else if(company == '') {
        alert('Silahkan Pilih Company');
        $('#company').focus();
      
      } else if(branch == '') {
        alert('Silahkan Pilih Branch');
        $('#branch').focus();
      
      } else {
        var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
            processData: false,															
            async: false,
            processData: false,
            contentType: false,		
            cache : false,	
            beforeSend: function()
            { 
              $("#btnSave").prop('disabled', true);
            },								
            success: function (data, textStatus, jqXHR)            
            {

              if(data=='Insert' || data=='Update'){
                $('#modal_form').modal('hide');
                location.reload();// for reload a page
              } else if(data=='Insert Gagal' || data=='Update Gagal') {
                alert('Data Gagal di '+action);
              } else {
                alert (data);
              }

              $("#btnSave").prop('disabled', false);
            },
            });
      }
      }

      

		  
    }

    function delete_model()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('demo_car/C_demo_car/delete_data')?>',{ID:ids_string},function(result){ 
              
              var json = JSON.parse(result)

              if(json == 'Delete') {
                $('#modal_form').modal('hide');
						    location.reload();// for reload a page
              } else {
                alert('Delete Data Gagal!');
              }

						  
					}); 
				}
			}
			
		

      
    }

    function getDataBranch(e) {
        var value = e;
        var url = "<?=base_url('demo_car/C_demo_car/ajax_branch?company=');?>"+value;

        $.ajax({
            url: url,
            type: "GET",
                beforeSend: function () {
                    $("#btnSave").prop('disabled', true);
                },
                success: function (data, textStatus, jqXHR) {
                    //console.log(data)
                    //$('#model').html($data);
                    
                    $('#branch-input').html(data);

                    $("#btnSave").prop('disabled', false);
                }
            });
    }

  </script>

  

</html>
