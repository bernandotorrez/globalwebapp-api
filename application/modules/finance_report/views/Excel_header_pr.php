<?php 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=".$file_name);
	header("Pragma: no-cache");
	header("Expires: 0");
?>
<table border='1' width="70%">
    <tr bgcolor="#C5C5C5" style="font-weight:bold; alignment-adjust:middle">
    <td>PR Number</td>
    <td>Company</td>
    <td>Branch</td>		
    <td>Dept</td>		
    <td>Vendor</td>		
    <td>Item Type</td>
    <td>Term</td>   
    <td>Purchase Type </td>
    <td>Currency</td> 
    <td>Total</td> 
    <td>PPN</td>  
    <td>PPH</td> 
    <td>Grand Total</td>
    <td>Date sub</td>		    			
    <td>Date Approved</td>
    <td>User Submission</td> 
    <td>PP Status</td> 
    <td>BPK Status</td> 
    <td>Ref No</td> 
    <td>Remarks</td>            
    </tr>
	
	<?php if(!empty($data_excel)) : ?>
	  <?php foreach($data_excel as $item) {
		    $idmaster = $item['id_master'] ;
    	?>
            <tr style="background:#FF9">
                <td><?=$item['id_master']?></td>
                <td><?=$item['company']?></td>
                <td><?=$item['name_branch']?></td>
                <td><?=$item['dept']?></td>
                <td><?=$item['vendor']?></td>                                        
                <td><?=$item['header_desc']?></td>  
                <td><?=$item['term_top']?></td>                
                <td><?=$item['type_purchase']?></td>
                <td><?=$item['currency']?></td> 
                <td style="color:#900;font-weight:bold;text-align: right;"><?=number_format($item['gran_total'],2,'.',',')?></td>	
                <td style="color:#900;font-weight:bold;text-align: right;"><?=number_format($item['ppn'],2,'.',',')?></td>	
                <td style="color:#900;font-weight:bold;text-align: right;"><?=number_format($item['pph'],2,'.',',')?></td>	
                <td style="color:#900;font-weight:bold;text-align: right;"><?=number_format($item['gran_totalppn'],2,'.',',')?></td>						                              
                <td><?=$item['date_send_aproval']?></td>
              
				    <?php
					   if ($item['date_aproval'] != "" ) :
					       echo '<td>'.$item['date_aproval'].'</td>';
					   else:  
					       echo '<td style="background:red">'."Not Approved Yet".'</td>' ; 						  
					   endif;  
					?>
                </td>
                <td><?=$item['user_submission']?></td>
                <td><?php					 
						 if ($item['flag_fpb'] != "1") :
							 echo '<label style="color:#EFE903">Open<label>';
						 else :	 
							 echo '<label style="color:blue">FPB<label>';
						 endif ;					
					?>
                 </td>   
                <td>
                   <?php
				   		 if ($item['flag_print_bpk'] == "1") :						  
					   			  echo '<label style="color:red">BPK<label>';
					        else :	
							   if ($item['flag_bpk'] == "1") :						  
								  echo '<label style="color:red">BPK<label>';
							   else :	  
							      echo "Waiting BPK";  
							   endif;
						 endif;  	
                   	?>
                </td> 	
                <td></td>
                <td><?=$item['remarks']?></td>                                                  
           
		<?php } ?>
        
     <?php endif ?>   
</table>