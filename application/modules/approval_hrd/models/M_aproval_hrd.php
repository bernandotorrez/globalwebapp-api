<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_aproval_hrd extends CI_Model {	
				    
    public $db_tabel = 'qv_head_pp_complite';
	 
    public function tampil_add_pp(){ //tampil table untuk ngececk num_row	
	
	     if ( $this->session->userdata('aproval_flag')=="3" or $this->session->userdata('aproval_flag')=="0"){ //aproval falg 1= bod, 2 = fc , 3 = head. 
		     
			 $ses_id_dept = $this->session->userdata('id_dept'); 
		     $ses_id_company = $this->session->userdata('id_company');  	
		     $ses_id_branch = $this->session->userdata('branch_id');		 		 					 
			 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject									
			 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
			 $str_flag_purchase = "1"; //jika untuk flag unit
			 			 
			  
			 $this->load->database();		
			 $this->db->select('*');		
			 $this->db->where('flag_purchase',$str_flag_purchase);
			 $this->db->where('id_company',$ses_id_company);					
			 $this->db->where('branch_id',$ses_id_branch );	
			 $this->db->where('id_dept',$ses_id_dept);
			 $this->db->where('status',$status_delete);
			 $this->db->where('status_send_aprove',$send_aprove);													
			 $this->db->order_by('date_send_aproval','desc');
			 $result = $this->db->get('qv_head_pp_complite');			     		 
			 
			 return $result;
			 
		 }else{
		    if ($this->session->userdata('aproval_flag') == "4"){	 
				 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
			     $status_delete = "1"; //jika 0 terdelete jika 1 aktif
			     $flag_head	=  "1"; //jika  1 sudah di aprove head dept di tampilkan di view FC 
			     $flag_purchase_stat	=  "0"; //jika  0 belum di aprove fc di maka tampilkan di view FC
			     //  $flag_aprove_fc =  "1"; //jika 1 sudah di aprove fc maka di tampilkan di view FC
			   	 $flag_bod	=  "0"; //jika  0 belum di aprove bod maka di tampilkan di view FC 
				 $str_flag_purchase = "1"; //jika untuk flag unit
				 			 
			     $this->load->database();		
			     $this->db->select('*');					
				 
				 $this->db->where('flag_purchase',$str_flag_purchase);					 
			     $this->db->where('aprove_head',$flag_head); //flag aprove Head
				 $this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a				
			    // $this->db->where('aprove_fc',$flag_fc);   //flag aprove purchase approve_purchasing				
			    // $this->db->or_where('aprove_fc',$flag_aprove_fc);   //flag aprove FC			
			     $this->db->where('status_send_aprove',$send_aprove);
			     $this->db->where('status',$status_delete);
			     $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D		
											 
				 $this->db->order_by('date_send_aproval','desc');
				 $result = $this->db->get('qv_head_pp_complite');			     		 
				 return $result;
				 
			}else{
				
				 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
				 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
				 $flag_head	=  "1"; //jika 1 sudah di aprove head dept dan..(kebawah) 
				 $flag_purchase_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
				 $flag_fc_approved_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
				 $flag_bod	=  "0"; //jika 0 belum di aprove B.O.D ,maka di tampilkan di view BOD 
				 //$str_flag_purchase = "1"; //jika untuk flag unit
				  
				 $this->load->database();		
				 $this->db->select('*');								
				 //$this->db->where('flag_purchase',$str_flag_purchase);
				 $this->db->where('aprove_head',$flag_head); //flag aprove Head
				 $this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a				
			     $this->db->where('aprove_fc',$flag_fc_approved_stat);   //flag aprove purchase a		
				 $this->db->where('status_send_aprove',$send_aprove);
			     $this->db->where('status',$status_delete);
			     $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D		
											 
				 $this->db->order_by('date_send_aproval','desc');
				 $result = $this->db->get('qv_head_pp_complite');			     		 
				 return $result;
				
				
			}			 
		 }
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 
		 
	   if ( $this->session->userdata('aproval_flag')=="3"){    
	         $ses_id_company = $this->session->userdata('id_company'); 				 
		     $ses_id_branch = $this->session->userdata('branch_id');	
		     $ses_id_dept = $this->session->userdata('id_dept'); //manggili session id dept					 			  		
			 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
			 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
			 $str_flag_purchase = "1"; //jika untuk flag inderect	 
			  
			 $this->load->database();		
			 $this->db->select('*');	
			 $this->db->where('flag_purchase',$str_flag_purchase);			
			 $this->db->where('id_company',$ses_id_company);					
		     $this->db->where('branch_id',$ses_id_branch );	
		     $this->db->where('id_dept',$ses_id_dept);
			 $this->db->where('status',$status_delete);
			 $this->db->where('status_send_aprove',$send_aprove);				 			
			 $this->db->order_by('date_send_aproval','desc');
			 $this->db->limit($start_row, $limit);					     	 	   		
			 $result = $this->db->get('qv_head_pp_complite')->result();		 		   
			 return $result ;
       }else{	
	   	   if ($this->session->userdata('aproval_flag') == "4"){	 
			   $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject						
			   $status_delete = "1"; //jika 0 terdelete jika 1 aktif
			   $flag_head	=  "1"; //jika  1 sudah di aprove head dept di tampilkan di view FC 
			   $flag_purchase_stat	=  "0"; //jika  0 belum di aprove fc di maka tampilkan di view FC				
			   $str_flag_purchase = "1"; //jika untuk flag inderect tampilkan di view FC 
			   $flag_bod = "0";
			   				 
			   $this->load->database();		
			   $this->db->select('*');					
			   $this->db->where('flag_purchase',$str_flag_purchase);			
			   $this->db->where('status_send_aprove',$send_aprove);						 
			   $this->db->where('aprove_head',$flag_head); //flag aprove Head
			   $this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a					
			  // $this->db->or_where('aprove_fc',$flag_aprove_fc);   //flag aprove FC						   
			   $this->db->where('status',$status_delete);
			   $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D													
																								 
			   $this->db->order_by('date_send_aproval','desc'); 
			   $this->db->limit($start_row, $limit);					     	 	   		
			   $result = $this->db->get('qv_head_pp_complite')->result();		 		   
			   return $result ;
			   
		   }else{
			   
			    $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
			    $status_delete = "1"; //jika 0 terdelete jika 1 aktif
			    $flag_head	=  "1"; //jika 1 sudah di aprove head dept dan..(kebawah) 
			    $flag_purchase_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
			    $flag_fc_approved_stat	=  "1"; //jika  0 belum di aprove fc di maka tampilkan di view FC
			    $flag_bod	=  "0"; //jika  0 sudah di aprove head dept dan sudah di aprove fc tetapi belum di aprove bod
							 
			    $this->load->database();		
			    $this->db->select('*');					
			   			   
			   // $this->db->where('flag_purchase','1');		//indirect
			    $this->db->where('aprove_head',$flag_head); //flag aprove Head
				$this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove purchase a				
			    $this->db->where('aprove_fc',$flag_fc_approved_stat);   //flag aprove purchase a							   			   
			    $this->db->where('status_send_aprove',$send_aprove);
			    $this->db->where('status',$status_delete);
			    $this->db->where('aprove_bod',$flag_bod);  //flag aprove B.O.D		
				 
																												 
			    $this->db->order_by('date_send_aproval','desc'); 
			    $this->db->limit($start_row, $limit);					     	 	   		
			    $result = $this->db->get('qv_head_pp_complite')->result();		 		   
			    return $result ;
		   }
	   }
	}	
	
	
	
	function give_aproval()						  		
	{
	     $strid_master =$this->input->post('msg'); 			  		 	
		 $flag_aproved = "1" ; // flag 1 aproved
		 
			 if ($this->session->userdata('aproval_flag')=="3") {			 
				  $buff_aproval ="approve_head";
				  $buff_date ="date_app_head";
			 }else{
				  if ($this->session->userdata('aproval_flag')=="2") {					   					   
					   		$buff_aproval ="approve_fc"; //automatic fc field approved
					   		$buff_date ="date_app_fc";
				   		}else{
							   $buff_aproval="";
						   }
						}
				   
			  			  
			  if (isset($buff_aproval) && trim($buff_aproval!=''))
			  {						  				  
				  for ($i=0; $i < count($strid_master) ; $i++) { 										
					
					
					    if ($this->session->userdata('aproval_flag')=="2") : //jika flag approval adalah fc
						   $data = array($buff_aproval=>$flag_aproved,
										  $buff_date =>date('Y-m-d'), 	
										  'date_aproval' =>date('Y-m-d'),										  
										  );	
						
						else:
								$data = array($buff_aproval=>$flag_aproved,
											  $buff_date =>date('Y-m-d'), 	
											  'date_aproval' =>date('Y-m-d')
											  );	
							endif;	
						
									  
					$this->db->where('id_loan',$strid_master[$i]);
					$this->db->update('tbl_hrd_loan',$data); 
					
					$buff_ses_idmaster = array('ses_idmaster'=>$strid_master[$i]);
				    $this->session->set_userdata($buff_ses_idmaster);				
					
					$value[] = $strid_master[$i] ;	//memasukan beberapa id master yg di select ke dalam array.				
					$this->session->set_userdata('result_id_master',$value); //simpan session array					
					//print_r($buff_ses_idmaster); echo array													
				  }				 				  				 				  				
				  return true;	
			  }else{
				  return false;
			  }
	  
		  
	}
	
	function give_rejected()						  		
	{	     		  		 	
		 $strid_master = $this->input->post('txtppno');
		 $str_name_reject = "By"." ".$this->session->userdata('name')." " . ":";
		 $strid_reason_reject =  $str_name_reject." ".$this->input->post('txtreject');
		 
		 		 
		 $flag_reject = "-1" ; // flag 1 flag_reject
		 $buff_status_reject = "status_send_aprove";
		 $buff_reason_reject = "reason_reject";
		 
		  if ($this->session->userdata('aproval_flag')=="3") {			 
				  $buff_aproval ="approve_head";		
				  $flag_back_aprove = "0" ;
			 }else{
				   if ($this->session->userdata('aproval_flag')=="4") {					   
					   $buff_aproval ="approve_bod"; 					
					   $flag_back_aprove = "0" ;
				   }else{
						if ($this->session->userdata('aproval_flag')=="2") {					   
						   $buff_aproval ="approve_fc"; 					
						   $flag_back_aprove = "0" ;
					    }else{
							   if ($this->session->userdata('aproval_flag')=="1") {					   
								  $buff_aproval ="approve_presdir"; 						  
								  $flag_back_aprove = "0" ;
							   }else{
								   $buff_aproval="";
								   $flag_back_aprove = "" ;
							   }
						}
				   }
			 }
		 
		 			
			  if (isset($buff_status_reject) && trim($buff_status_reject!=''))
			  {		
						
					$query = $this->db->select("id_loan") //check id booking pada table_parent 								
										->where('id_loan',$strid_master)
										->get('tbl_hrd_loan');
					  if ($query->num_rows() == 1)
					  {				
						$data = array($buff_status_reject=>$flag_reject,
						              $buff_reason_reject=>$strid_reason_reject,
									  $buff_aproval => $flag_back_aprove
									  );	
																																													
						$this->db->where('id_loan',$strid_master);
						$this->db->update('tbl_hrd_loan',$data); 
						
						$buff_ses_idmaster = array('ses_idmaster'=>$strid_master);
						$this->session->set_userdata($buff_ses_idmaster);
						
						return true;
					  }
				 // }				  
			  }else{
				  return false;
			  }
	  		  
	}
	
	function get_company()
	{		
	     $this->load->database();		
		 $this->db->select('short');				 
		 $result = $this->db->get('tbl_company');			     		 
		 return $result;	 	
	}
	
	function get_dept()
	{		
	     $this->load->database();		
		 $this->db->select('dept');				 
		 $result = $this->db->get('tbl_dept');			     		 
		 return $result;	 	
	}
	
	function get_search()
	{
		
	 $strdatestart =$this->input->post('txtdatestart');	
	 $strdatend =$this->input->post('txtdateend');	
	 $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	 $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
	 	 
	 $strcompany = $this->input->post('cbostatus');
	 $strdept = $this->input->post('cbodept');	 
	 $strcari = $this->input->post('txtcari');
	 
	 $send_aprove = "1" ; //flag send_aproval jika 0 belum di send jika 1 sudah di send jika -1 =reject			
	 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		
	 
	 $flag_head_aprove ="1";
	 $flag_head	=  "0"; //jika  0 sudah di aprove head dept 
	 
	 $flag_purchase_stat_aprove	=  "1"; //jika  1 sudah di aprove head dept 
	 $flag_purchase_stat	=  "0"; //jika  0 belum di aprove fc di maka tampilkan 	
	 $flag_bod	=  "0"; //jika  0 belum di aprove bod 	 				
     $str_flag_purchase = "1"; //jika untuk flag unit	
	 $flag_fc_stat_aprove = "1";
	 			
	 $this->load->database();		
	 $this->db->select('*');
	 
	 if ($this->session->userdata('aproval_flag') =="3" or $this->session->userdata('aproval_flag') =="0" ){	   		
		  $this->db->where('flag_purchase',$str_flag_purchase);		
		  $this->db->where('status_send_aprove',$send_aprove);
		  $this->db->where('status',$status_delete); 							 										 	         }else{
		   if ($this->session->userdata('aproval_flag') =="4"){	 
		       $this->db->where('flag_purchase',$str_flag_purchase);		  	  
		       $this->db->where('aprove_head',$flag_head_aprove);   //flag aprove FC	
			   
		       $this->db->where('approve_purchasing',$flag_purchase_stat);   //flag aprove FC
			   $this->db->where('aprove_bod',$flag_bod);
			   $this->db->where('status_send_aprove',$send_aprove);
			   $this->db->where('status',$status_delete);				   
			   if($strcompany !='ALL'){$this->db->where('short',$strcompany);}							 				 				
			   if($strdept !='ALL'){ $this->db->where('dept',$strdept);}	 						  				 				             
			   if($strdatestart !="" and $strdatend !="" ){	
					$this->db->where('date_send_aproval BETWEEN "'.$datestart.'" and "'.$dateend.'"');}	 	 																	
			   
			   if($strcari !=""){ $this->db->where("id_master",$strcari);} 			  						   
			   //-----------------Or Where---------------------------------			   			   
			   // $this->db->or_where('aprove_fc',$flag_aprove_fc);   //flag aprove FC			
			   $this->db->where('aprove_bod',$flag_bod);
			   $this->db->where('status_send_aprove',$send_aprove);
			   $this->db->where('status',$status_delete);
		   }else{
			  // $this->db->where('flag_purchase',$str_flag_purchase);		 
			   $this->db->where('aprove_head',$flag_head_aprove);   //flag aprove FC			     			  			
			   $this->db->where('approve_purchasing',$flag_purchase_stat_aprove);   //flag aprove FC
			   $this->db->where('aprove_fc',$flag_fc_stat_aprove);   //flag aprove FC			
			   $this->db->where('aprove_bod',$flag_bod);
			   $this->db->where('status_send_aprove',$send_aprove);
			   $this->db->where('status',$status_delete);			   
		   }
	 }
					 		  	     			  	 
	 if($strcompany !='ALL'){	
	   $this->db->where('short',$strcompany);	
	 }	 
	 
	 if($strdept !='ALL'){	
	   $this->db->where('dept',$strdept);	
	 }	 
	 
	 if($strdatestart !="" and $strdatend !="" ){	
	  	$this->db->where('date_send_aproval BETWEEN "'.$datestart.'" and "'.$dateend.'"');		 			  		   					
	 }	 	 
	 				  		   							
	 if($strcari !=""){
		 $this->db->where("id_master",$strcari);			 					 	 
	 } 
		 	
	 	 	 		 	   
	 $this->db->order_by('date_send_aproval','desc');
	 $result = $this->db->get('qv_head_pp_complite');		
	 return $result;
	 
	}
	
	public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}
				    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}