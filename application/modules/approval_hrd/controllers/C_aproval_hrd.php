<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_aproval_hrd extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_aproval_hrd','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_aproval_hrd->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}			
	}		  	
		
  public function index()
  {					    	
  		 
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	 $tampil_table_aproval= $this->M_aproval_hrd->tampil_add_pp()->result();	
	 $total_rows =$this->M_aproval_hrd->tampil_add_pp()->num_rows();	  										  	
	 $dept  = $this->session->userdata('dept') ;	
	 $branch  = $this->session->userdata('name_branch') ; 
	 $company = $this->session->userdata('short') ;		
	 $data['intno'] = ""; //variable buat looping no table.				
	 $data['header'] ="Aproval Form"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	 $data['ceck_row'] = $total_rows;	
	 $data['tampil_company']=$this->M_aproval_hrd->get_company()->result();																															
	 $data['tampil_dept']=$this->M_aproval_hrd->get_dept()->result();																															
	 $data['show_view'] = 'approval_hrd/V_aproval_hrd';		
	 $this->load->view('dashboard/Template',$data);					
				 
	
  }
  
  public function do_aproval()	
  {
		if ($this->M_aproval_hrd->give_aproval())
		{	 	
			$this->session->set_flashdata('pesan_aproval','1');	
		
			 if ($this->session->userdata('aproval_flag')=="4") {	
			    $this->kirim_email_to_fc();
			}
			 if ($this->session->userdata('aproval_flag')=="2") {	
			     $this->kirim_email_aprove(); 
			}
		    							
			redirect('approval_hrd/C_aproval_hrd'); //riderct ke menu utama  			
			//print_r($this->session->userdata("result_id_master"));	
			
		}else{
			$this->session->set_flashdata('pesan_aproval','0');		
			redirect('approval_hrd/C_aproval_hrd');   
		}
  }
  
  public function do_rejected()	
  {
		if ($this->M_aproval_hrd->give_rejected())
		{	 			    
			$this->session->set_flashdata('pesan_reject','1');	
			$this->kirim_email_reject();		
			redirect('approval_hrd/C_aproval_hrd');   
		}else{			
			$this->session->set_flashdata('pesan_reject','0');		
			redirect('approval_hrd/C_aproval_hrd');     
		}
  }
  
  
   public function do_search_pp()	
  {		
  		 $tampil_table_aproval= $this->M_aproval_hrd->get_search()->result();	
	     $total_rows =$this->M_aproval_hrd->tampil_add_pp()->num_rows();
  	
		if ($tampil_table_aproval)
		{	 	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;					
			$data['intno'] = ""; //variable buat looping no table.	
			$data['header'] ="Approval Form"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;				
			$data['ceck_row'] = $total_rows;	
			$data['pp_view'] =  $tampil_table_aproval;
			$data['tampil_company']=$this->M_aproval_hrd->get_company()->result();
			$data['tampil_dept']=$this->M_aproval_hrd->get_dept()->result();																																
			$data['show_view'] = 'approval_hrd/V_aproval_hrd';		
			$this->load->view('dashboard/Template',$data);					
	  }else{	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;	
			$data['intno'] = ""; //variable buat looping no table.					
			$data['header'] ="Aproval Form"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;	
			$data['pp_view'] =  $tampil_table_aproval;
			$data['tampil_company']=$this->M_aproval_hrd->get_company()->result();	
			$data['tampil_dept']=$this->M_aproval_hrd->get_dept()->result();																															
			$data['pesan'] = 'Data PP aproval table is empty';				
			$data['show_view'] = 'approval_hrd/V_aproval_hrd';
			$this->load->view('dashboard/Template',$data);					
	  } 
		
  }
  
   
  public function multiple_submit() //fungsi submit dengan banyak button dalam satu form
  {  
	
		if ($this->input->post('btnaprove')){	
			$this->do_aproval();
		}else{
			if ($this->input->post('btnreject')){	//dari modal popup
				$this->do_rejected();							
			}else{
				if ($this->input->post('btncari')){	
					$this->do_search_pp();
				}else{		
					redirect('approval_hrd/C_aproval_hrd');
				}
			}
		}
	

	}

//------------------------------------pashing result to modal popup-------------------
	public function get_nopp_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //       
	     $query_master = $this->db->query("select id_loan, no_loan, name_staff, dept, nik, no_inventory, date_loan, description, additional, purpose from tbl_hrd_loan where id_loan ='".$nopp."'");  
         $query_detail = $this->db->query("select * from tbl_hrd_loan where id_loan ='".$nopp."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   
		   
		      $data['str_pp_master']=$query_master->result();
			  $data['str_pp_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_nopp'] = $nopp ;
			  $tampil_detail = $this->load->view('approval_hrd/V_conten_detail',$data,true);
			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
						

public function kirim_email_reject(){          
 		   
		   $this->load->library('email');
		   //konfigurasi email
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T";
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.googlemail.com';
		  $config['smtp_host']    = 'ssl://googlemail-smtp.l.google.com';

		   //-----------exchange
		   //$config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';
		   //$config['smtp_port']  = '5220';
		   $config['smtp_port']  = '465'; //port gmail
		   $config['newline']    = "\r\n";
		   $config['mailtype']   ="html" ;
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!";
		   //$config['smtp_user']= "helpdesk@eurokars.co.id";
		   //$config['smtp_pass']= "";
		   //--------------------------------	   		  
		    		   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			$str_status = '1' ; //aktif ;
						
			if ($strponumber != '') :						  	
				//Select untuk email master PP----------------------------
				$query =$this->db->query("select email, email_head, email_cc, date_aproval, status_send_aprove, reason_reject, approve_fc, name_staff from tbl_hrd_loan where id_loan ='".$strponumber."' and status ='".$str_status."'");				
			  if ($query->num_rows() > 0 ) :
			      $data_email['str_pp_master'] = $query->result();		
				  foreach ($query->result() as $row_ceck) :	
				     //simpan row alamat email kedalam variable..
					 	 $struseremail = $row_ceck->email ; //addrees for to email
					     $strheademail = $row_ceck->email_head; //addrees for to email
						 $strccemail = $row_ceck->email_cc ; //addrees for to email
					 //end-----------------------------------------	
					 		   
				     //Select untuk email deteail PP--------------------------------
				    $query_detail =$this->db->query("select * from tbl_hrd_loan where id_loan ='".$strponumber."' and status ='".$str_status."'");		
				     if ($query_detail->num_rows() > 0 ) :										 
					    $data_email['str_pp_detail'] = $query_detail->result();	
					    $data_email['intno'] = ""; //untuk counter angka table																									
					    $message = $this->load->view('approval_hrd/V_content_email_aproval',$data_email,true);														
					  endif;
					  //end-----------------------------------------------------------					 
						  if ($row_ceck->status_send_aprove =="-1")	:					  														  							   														
								$subject = "PP REJECTED-- Eurokars Motor Indonesia";		
								$result = $this->email ;							   
								$this->email->initialize($config);  
								$this->email->set_newline("\r\n"); 
							   
								//Exchange
								//konfigurasi pengiriman					
								
								//$sender_email = "helpdesk@eurokars.co.id";
                				$sender_email = "3m1mazda@gmail.com"; 					   
								$sender_name = "HRD Loan Notification";					   					    
								
								//GMAIL-----
								//$sender_email = "epurchaseeuro@gmail.com";
								//$sender_name = "Epurchasing Eurokarsgroup";					   					    																						   
								//$to = 'brian.yunanda@eurokars.co.id';
								
							    $to = $struseremail.",".$strheademail.",".$strccemail; //email address,user,head, and CC
								 
								$this->email->from($sender_email, $sender_name);
								$this->email->to($to);
								$this->email->subject($subject);  
								
								//$this->email->message(print_r($message, true));			   
							  
								$this->email->message($message);// tanpa array	
																		 
								if ($this->email->send()) :		  	
									$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
									//redirect(current_url());
								else:
									show_error($this->email->print_debugger());
								endif; 										   						   
						      endif;
					      endforeach;							    
			         endif;
			     endif;				 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
			 
	      
 }
 
 public function kirim_email_aprove(){          
 		   
		  $this->load->library('email');
		   //konfigurasi email
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T";
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.googlemail.com';
		  $config['smtp_host']    = 'ssl://googlemail-smtp.l.google.com';

		   //-----------exchange
		   //$config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';
		   //$config['smtp_port']  = '5220';
		   $config['smtp_port']  = '465'; //port gmail
		   $config['newline']    = "\r\n";
		   $config['mailtype']   ="html" ;
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!";
		   //$config['smtp_user']= "helpdesk@eurokars.co.id";
		   //$config['smtp_pass']= "";
		   //--------------------------------	     		  
		    		   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			$str_status = '1' ; //aktif ;
			
			if ($strponumber != '') :						  	
				//Select untuk email master PP----------------------------
				$query =$this->db->query("select email, email_head, email_cc, date_aproval, status_send_aprove, reason_reject, approve_fc, name_staff from tbl_hrd_loan where id_loan ='".$strponumber."' and status ='".$str_status."'");				
			  if ($query->num_rows() > 0 ) :
			      $data_email['str_pp_master'] = $query->result();		
				  foreach ($query->result() as $row_ceck) :	
				  			   				     	
					 //simpan row alamat email kedalam variable..
					 	$struseremail = $row_ceck->email ;
						$strheademail = $row_ceck->email_head;
						$strccemail = $row_ceck->email_cc ;
					 //end-----------------------------------------
					
				     //Select untuk email detail PP--------------------------------
				    $query_detail =$this->db->query("select * from tbl_hrd_loan where id_loan ='".$strponumber."' and status ='".$str_status."'");			
				     if ($query_detail->num_rows() > 0 ) :					 																					
					    $data_email['str_pp_detail'] = $query_detail->result();	
					    $data_email['intno'] = ""; //untuk counter angka table																									
					    $message = $this->load->view('approval_hrd/V_content_email_aproval',$data_email,true);														
					  endif;				
					  //end---------------------------------------------------------
					  		
							 if ($row_ceck->status_send_aprove =="1" and $row_ceck->approve_head == "1")	:										 																									
									$subject = "PP APPROVED -- Eurokargroup Company";	
									$result = $this->email ;							   
									$this->email->initialize($config);  
									$this->email->set_newline("\r\n"); 
								   
									//Exchange
									//konfigurasi pengiriman					
									
									//$sender_email = "helpdesk@eurokars.co.id";
									$sender_email = "3m1mazda@gmail.com";				   
									$sender_name = "HRD Loan Notification";					   					    
									
									//GMAIL-----
									//$sender_email = "epurchaseeuro@gmail.com";
									//$sender_name = "Epurchasing Eurokarsgroup";					   					    								//end--------------------------
									
									//simpan session alamat email kedalam variable..
									
								   //end-----------------------------------------
								   
									//$to = 'brian.yunanda@eurokars.co.id';
									
									$to = $struseremail.",".$strheademail.",".$strccemail; //email address,user,head, and CC
									 
									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);  
									
									//$this->email->message(print_r($message, true));			   
								  
									$this->email->message($message);// tanpa array	
																			 
									if ($this->email->send()) :		  	
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
										//redirect(current_url());
									//else:
									//	show_error($this->email->print_debugger());
									endif;
								 endif;						    
					      endforeach;							    
			         endif;
			     endif;				 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
			 
	      
 }
 
 public function kirim_email_to_fc(){          
 		   
		   $this->load->library('email');
		   //konfigurasi email
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T";
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.googlemail.com';
		   $config['smtp_host']    = 'ssl://googlemail-smtp.l.google.com';

		   //-----------exchange
		   //$config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';
		   //$config['smtp_port']  = '5220';
		   $config['smtp_port']  = '465'; //port gmail
		   $config['newline']    = "\r\n";
		   $config['mailtype']   ="html" ;
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!";
		   //$config['smtp_user']= "helpdesk@eurokars.co.id";
		   //$config['smtp_pass']= "";
		   //--------------------------------	   		  
		    		   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			
			if ($strponumber != '')
			 {				  	
				//Select untuk email master PP----------------------------
				$query =$this->db->query("select email, email_head, email_cc, date_aproval, status_send_aprove, reason_reject, approve_fc, name_staff from tbl_hrd_loan where id_loan ='".$strponumber."' and status ='".$str_status."'");			
			    if ($query->num_rows() > 0 ) {
					$data_email['str_pp_master'] = $query->result();		
								   
					 $query_detail =$this->db->query("select * from tbl_hrd_loan where id_loan ='".$strponumber."' and status ='".$str_status."'");		
					if ($query_detail->num_rows() > 0 ) {										 
					   $data_email['str_pp_detail'] = $query_detail->result();	
					   $data_email['intno'] = ""; //untuk counter angka table	
					 		
					   $message = $this->load->view('approval_hrd/V_content_email_to_bod_fc',$data_email,true);
					 						
					} 
			   }
			}
					 
			$result = $this->email ;							   
			$this->email->initialize($config);  
			$this->email->set_newline("\r\n"); 
		   
		   				
			
			//$sender_email = "helpdesk@eurokars.co.id";
			$sender_email = "3m1mazda@gmail.com";					   
			$sender_name = "HRD Loan Notification";					   					    
												   					    
									$result = $this->email ;							   
									$this->email->initialize($config);  
									$this->email->set_newline("\r\n"); 
																			
									$to = 'andri.widjaja@mazda.co.id' ;
									//$to = 'brian.yunanda@eurokars.co.id';
									$subject = 'submission Approval F.C';
									
									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);  																		
								  
									$this->email->message($message);// tanpa array	
																			 
									if ($this->email->send()) :		  	
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
									//	redirect(current_url());
									else:
										show_error($this->email->print_debugger());
									endif;												    						 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
			 
	      
 }

 
 public function kirim_email_to_bod(){          
 		   
		  $this->load->library('email');
		   //konfigurasi email
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T";
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;
		   //------google
		  // $config['smtp_host']    = 'ssl://smtp.googlemail.com';
		  $config['smtp_host']    = 'ssl://googlemail-smtp.l.google.com';

		   //-----------exchange
		   //$config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';
		   //$config['smtp_port']  = '5220';
		   $config['smtp_port']  = '465'; //port gmail
		   $config['newline']    = "\r\n";
		   $config['mailtype']   ="html" ;
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!";
		   //$config['smtp_user']= "helpdesk@eurokars.co.id";
		   //$config['smtp_pass']= "";
		   //--------------------------------		   		  
		    		   		 		          				   				     		   				  									
			$strponumber =$this->session->userdata("ses_idmaster");
			
			if ($strponumber != '')
			 {				  	
				//Select untuk email master PP----------------------------
				$query =$this->db->query("select email, email_head, email_cc, date_aproval, status_send_aprove, reason_reject, approve_fc, name_staff from tbl_hrd_loan where id_loan ='".$strponumber."' and status ='".$str_status."'");			
			    if ($query->num_rows() > 0 ) {
					$data_email['str_pp_master'] = $query->result();		
								   
					 $query_detail =$this->db->query("select * from tbl_hrd_loan where id_loan ='".$strponumber."' and status ='".$str_status."'");		
					if ($query_detail->num_rows() > 0 ) {										 
					   $data_email['str_pp_detail'] = $query_detail->result();	
					   $data_email['intno'] = ""; //untuk counter angka table	
					 		
					   $message = $this->load->view('approval_hrd/V_content_email_to_bod_fc',$data_email,true);
					 						
					} 
			   }
			}
					 
			$result = $this->email ;							   
			$this->email->initialize($config);  
			$this->email->set_newline("\r\n"); 
		   
		   				
			
			//$sender_email = "helpdesk@eurokars.co.id";
			$sender_email = "3m1mazda@gmail.com";				   
			$sender_name = "Epurchasing Notification";					   					    
												   					    
									$result = $this->email ;							   
									$this->email->initialize($config);  
									$this->email->set_newline("\r\n"); 
																			
									$to = 'roy.arfandy@mazda.co.id' ;
									//$to = 'brian.yunanda@eurokars.co.id';
									$subject = 'submission Approval B.O.D';
									
									$this->email->from($sender_email, $sender_name);
									$this->email->to($to);
									$this->email->subject($subject);  																		
								  
									$this->email->message($message);// tanpa array	
																			 
									if ($this->email->send()) :		  	
										$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
									//	redirect(current_url());
									else:
										show_error($this->email->print_debugger());
									endif;												    						 											
		
			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;				 
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent'); 
			 $this->session->unset_userdata('ses_grandtotal'); 
			 $this->session->unset_userdata('ses_term'); 
			 $this->session->unset_userdata('ses_remarks'); 
			 $this->session->unset_userdata('ses_idmaster'); 
																			
									
 }




function cath_data_fordatatables(){
	
	  
		 $str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
		 $str_idcompany = $this->session->userdata('id_company'); 
		 $str_iddept = $this->session->userdata('id_dept'); 
		 $str_iddivisi = $this->session->userdata('id_divisi'); 
		 $str_status_send = "1";
		 $str_status_approved = "1";
		 
		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_loan');
		$this->db->from('tbl_hrd_loan');
		if  ($str_flag_approval == "3" or $str_flag_approval == "0" ){	
			     $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->like("id_loan",$search);
				 $this->db->or_like("no_loan",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);				
				 $this->db->where("status","1");
				 $this->db->or_like("name_staff",$search);	
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->or_like("date_loan",$search);	
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);
				 $this->db->where("status","1");
				
			}else{
					if  ($str_flag_approval == "4"){	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);						 
						 $this->db->where("status","1");
						 $this->db->like("id_loan",$search);
						 $this->db->or_like("no_loan",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);						 
						 $this->db->where("status","1");
						 $this->db->or_like("name_staff",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);						 
						 $this->db->where("status","1");
						 $this->db->or_like("date_loan",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						
				 }else{		
						if  ($str_flag_approval == "2"){	
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_head",$str_status_approved );
							 $this->db->where("approve_fc",$str_status_approved );								
							 $this->db->where("status","1");
							 $this->db->like("id_loan",$search);
							 $this->db->or_like("no_loan",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_head",$str_status_approved );
							 $this->db->where("approve_fc",$str_status_approved );		
							 $this->db->where("status","1");
							 $this->db->or_like("name_staff",$search);	
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_head",$str_status_approved );
							 $this->db->where("approve_fc",$str_status_approved );		
							 $this->db->where("status","1");
							 $this->db->or_like("date_loan",$search);	
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_head",$str_status_approved );
							 $this->db->where("approve_fc",$str_status_approved );		
							 $this->db->where("status","1");
						}							 
					 
				 }
			}
		$total = $this->db->count_all_results();
		//$total=$this->db->count_all_results("qv_vendor");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		if  ($str_flag_approval == "3" or $str_flag_approval == "0" ){	
			     $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->like("id_loan",$search);
				 $this->db->or_like("no_loan",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);				
				 $this->db->where("status","1");
				 $this->db->or_like("name_staff",$search);	
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->or_like("date_loan",$search);	
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);
				 $this->db->where("status","1");
				
			}else{
					if  ($str_flag_approval == "4"){	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);						 
						 $this->db->where("status","1");
						 $this->db->like("id_loan",$search);
						 $this->db->or_like("no_loan",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->or_like("name_staff",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);						 
						 $this->db->where("status","1");
						 $this->db->or_like("date_loan",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						
				 }else{		
						if  ($str_flag_approval == "2"){	
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_fc",$str_status_approved );								
							 $this->db->where("status","1");
							 $this->db->like("id_loan",$search);
							 $this->db->or_like("no_loan",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_fc",$str_status_approved );		
							 $this->db->where("status","1");
							 $this->db->or_like("name_staff",$search);	
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_fc",$str_status_approved );		
							 $this->db->where("status","1");
							 $this->db->or_like("date_loan",$search);	
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_fc",$str_status_approved );		
							 $this->db->where("status","1");	
						}
					 
				 }
			}
		}

		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		 if  ($str_flag_approval == "3" or $str_flag_approval == "0"){						
			 $this->db->where("status_send_aprove",$str_status_send);
			 $this->db->where("id_company",$str_idcompany);
			 $this->db->where("dept",$str_iddept);			 
		 }else{
			  if  ($str_flag_approval == "4"){
				   $this->db->where("status_send_aprove",$str_status_send);	
				   $this->db->where("approve_head",$str_status_approved );
				   $this->db->where("id_divisi",$str_id_divisi);
			  }else{	
					  if  ($str_flag_approval == "2"){
					    $this->db->where("status_send_aprove",$str_status_send);						
						$this->db->where("approve_head",$str_status_approved );
					  }
					
			  }
		 }
		/*Urutkan dari alphabet paling terkahir*/
		  $this->db->where("status","1");
		 $this->db->order_by('id_loan','DESC');
		 $query=$this->db->get('tbl_hrd_loan');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		if  ($str_flag_approval == "3" or $str_flag_approval == "0" ){	
			     $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->like("id_loan",$search);
				 $this->db->or_like("no_loan",$search);
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);				
				 $this->db->where("status","1");
				 $this->db->or_like("name_staff",$search);	
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);
				 $this->db->where("status","1");
				 $this->db->or_like("date_loan",$search);	
				 $this->db->where("status_send_aprove",$str_status_send);
				 $this->db->where("id_company",$str_idcompany);
				 $this->db->where("dept",$str_iddept);
				 $this->db->where("status","1");
				
			}else{
					if  ($str_flag_approval == "4"){	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->like("id_loan",$search);
						 $this->db->or_like("no_loan",$search);
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->or_like("name_staff",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);
						 $this->db->where("status","1");
						 $this->db->or_like("date_loan",$search);	
						 $this->db->where("status_send_aprove",$str_status_send);
						 $this->db->where("approve_head",$str_status_approved );
						 $this->db->where("id_divisi",$str_id_divisi);						 
						 $this->db->where("status","1");
						
				 }else{		
						if  ($str_flag_approval == "2"){	
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_fc",$str_status_approved );								
							 $this->db->where("status","1");
							 $this->db->like("id_loan",$search);
							 $this->db->or_like("no_loan",$search);
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_fc",$str_status_approved );		
							 $this->db->where("status","1");
							 $this->db->or_like("name_staff",$search);	
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_fc",$str_status_approved );		
							 $this->db->where("status","1");
							 $this->db->or_like("date_loan",$search);	
							 $this->db->where("status_send_aprove",$str_status_send);
							 $this->db->where("approve_fc",$str_status_approved );		
							 $this->db->where("status","1");	
						}
					 
				 }
			}
		
		
		$jum=$this->db->get('tbl_hrd_loan');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		foreach ($query->result_array() as $row_tbl) {
			
			 if ($row_tbl['status_send_aprove'] == "-1")	
			 {
				   $info_aproval = "Reject";
			 }else{				
				   $info_aproval = "New Crated";				
			 }
			 
			 $chk_idmaster ='<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value='.$row_tbl["id_loan"].' class="ace" req_id_del='.$row_tbl["id_loan"].' />
           <span class="lbl"></span> ';
		   
		   
		   $btn_view_detail = '<a href="#" class="btn btn-primary detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_loan"].'> <span class="glyphicon glyphicon-list" aria-hidden="true"></span>Detail</a>';
		   
		    If ($row_tbl['approve_head'] == "1")
			 {
				$head_approval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
			 }else{
				  If ($row_tbl['status_send_aprove'] == "-1")
				  {
					 $head_approval = '<div style=" color:#EB293D">'."Rejected".'</div>' ; 
				  }else{
					 $head_approval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
				  }
			 }
			
			   If ($row_tbl['approve_fc'] == "1")
			  {
					$fc_aproval = '<img src="'.base_url("asset/images/success_ico.png").'">' ;
				 }else{
					  If ($row_tbl['status_send_aprove'] == "-1")
					  {
						$fc_aproval = '<img src="'.base_url("asset/images/reject.png").'">' ;
					  }else{
						$fc_aproval = '<div style=" color:#EB293D">'."Waiting Approval".'</div>' ; 
					  }
			   }
			  		 
			

 $strfcaprove   = $row_tbl["approve_fc"] ;
 $strheadaprove = $row_tbl["approve_head"] ;	
 

 if ($this->session->userdata("aproval_flag")== "3" and $row_tbl["approve_head"]=="1" ) {	
	 $str_reject =  'Has been Aproved head' ;			
 }else{
	 if ($this->session->userdata("aproval_flag")== "2" and $row_tbl["approve_fc"]=="1" ) {	
		  $str_reject =  'Has been Aproved fc' ;			
 	 }else{ 	 
			  $str_reject =  '<a href="#" class="btn btn-danger btn-setting reject" id="reject" style="text-decoration:none" req_id='.$row_tbl["id_loan"].'><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>Reject</a>
								';  	 
				/*if ($this->session->userdata("aproval_flag")== "3" or $this->session->userdata("aproval_flag")== "4"  or $this->session->userdata("aproval_flag")== "2" or $this->session->userdata("aproval_flag")== "0") {				
				   if ($strbodaprove == "1" and $strfcaprove == "1" and $strpurchaseaprove == "1" and $strheadaprove == "1" ) {	 
					   $str_reject =  'Has been Aproved All' ;
				   }else{ 			    
					  $str_reject =  '<a href="#" class="btn btn-danger btn-setting reject" id="reject" style="text-decoration:none" req_id='.$row_tbl["id_master"].'><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>Reject</a>
								';  
					   
				   }
			   }	*/  				
		   }
	   }
     
  
		
		//foreach ($query->result_array() as $tbl_vendor) {
		//$chk_idmaster ='<div align="center"><input id="checkvendor" name="checkvendor" type="checkbox" value='.$row_tbl["id_loan"].' class="editRow ace" req_id_del='.$row_tbl["id_loan"].' />
          //<span class="lbl"></span> ';

			$output['data'][]=array($chk_idmaster,
									$row_tbl['id_loan'],
									$row_tbl['no_loan'],
									$row_tbl['name_staff'],
									$row_tbl['dept'],
									$row_tbl['nik'],
									$row_tbl['no_inventory'],
									$row_tbl['date_loan'],
									$row_tbl['description'],
									$row_tbl['additional'],
									$row_tbl['purpose'],
									$head_approval,
									$fc_aproval,
									$str_reject
									);

		}

		echo json_encode($output);
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }
	 		
}

	
