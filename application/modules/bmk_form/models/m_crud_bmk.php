<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_crud_bmk extends CI_Model {	
				
    public $db_tabel_qv = 'qv_complite_master_bmk';	    
	
	 /*function getppnumber(){	old counter
	 	 
		$strshort = $this->session->userdata('short');//short company
        $strdept = substr(($this->session->userdata('dept')),0,3);
		$strbranchshort = $this->session->userdata('branch_short');
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");				
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	    $q = $this->db->query("select MAX(RIGHT(id_master,4)) as code_max from tbl_master_bmk where left(id_master,2)='".substr($strshort,0,2)."'");
									  	   
	   $qdate = $this->db->query("select * from tbl_date where date_add ='".$strdates."'"); 	//check tanggal
	   
	     $code = ""; //empty code
		 if ($qdate->num_rows()>0) {			
					if($q->num_rows()>0){
						foreach($q->result() as $cd){
							$tmp = ((int)$cd->code_max)+1;
							$code = sprintf("%04s", $tmp);;
						}
					}else{					
						 $code = "0001";					 
					}		
		 }else{		
		       $code = "0001";					 
		 }
		 //insert tanggal ketable--------------
		 $data = array( 'date_add'=>$strdates );																	  									
	     $this->db->update('tbl_date',$data); 		 
		 //end--------------------
		 
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	    return substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$code ;			        //end------------------------------------------
	   
    } */
	
	function getppnumber(){	// view counter number when add new
	 	 
		$strshort = $this->session->userdata('short');//short company
		$stridcom = $this->session->userdata('id_company');//id company
        $strdept = substr(($this->session->userdata('dept')),0,3);
		$strbranchshort = $this->session->userdata('branch_short');
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");			    
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	    $qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_bmkno where id_company='".$stridcom."'");
									  	   
	    $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); 	//check tanggal
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
				if($qcounter->num_rows()== 1):
					foreach($qcounter->result() as $cd):
						$tmp = ((int)$cd->code_max);
						$code = sprintf("%04s", $tmp);;
					endforeach;
					
					 if ($code== '0000') :						
						 $code = "0001";					 
					 endif;
					 
				 else:					  
					$code = "0001";								
				 endif;		
		 else:		
		       $code = "0001";					 
		 endif;
		 
	 //update tanggal ketable date--------------
	 $data = array('date_add'=>$strdates );																			
	 $this->db->update('tbl_date_compare',$data); 		 
	 //end--------------------
		 
	 //update tanggal ketable counter--------------
	  $data = array('counter'=> $code );
		
	  // ada perubahan
	  $this->db->where('id_company',$stridcom);												  									
	  $this->db->update('tbl_counter_bmkno',$data); 		 
	 //end--------------------
		 
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	  return substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$strdates."/".$code ;			        //end------------------------------------------			 
    }
	
	function counterbmknumber(){	//counter number when save data
	 	 
		$strshort = $this->session->userdata('short');//short company
		$stridcom = $this->session->userdata('id_company');//id company
        $strdept = substr(($this->session->userdata('dept')),0,3);
		$strbranchshort = $this->session->userdata('branch_short');
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");			    
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	    $qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_bmkno where id_company='".$stridcom."'");
									  	   
	    $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); //check tanggal
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
					if($qcounter->num_rows()>0):
						foreach($qcounter->result() as $cd):
							$tmp = ((int)$cd->code_max)+1;
						    $code = sprintf("%04s", $tmp);;
						endforeach;
						 if ($code== '0000') :						
						     $code = "0001";					 
					 	 endif;
					 else:					
						 $code = "0001";					 
					 endif;		
		 else:	
		       $code = "0001";					 
		 endif;
		 
		 //update tanggal ketable date--------------
			 $data = array('date_add'=>$strdates );																			
			 $this->db->update('tbl_date_compare',$data); 		 
		 //end--------------------
		 
		 //update tanggal ketable counter--------------
			 $data = array('counter'=> $code );	

			 // ada perubahan
			 $this->db->where('id_company',$stridcom);														  									
			 $this->db->update('tbl_counter_bmkno',$data); 		 
		 //end--------------------
		 
		//ambil short company 2 digit dari kiri esu=>es, gabungkan string dengan kode yang telah dibuat tadi			
	    //end------------------------------------------					 
	      return true;
    }	
	
	function get_bank()
	{		
	     $this->load->database();		
		 $this->db->select('name_bank');				
		 $result = $this->db->get('tbl_bank');			     		 
		 return $result->result();	 	
	}	
	
	function get_currency()
	{		
	     $this->load->database();		
		 $this->db->select('id_curr,currency');				
		 $result = $this->db->get('tbl_curr');			     		 
		 return $result->result();	 	
	}	
	 
		
	function insert_master_and_detail()	
	{ 
	     //-------------------Checck Duplicate NOPP / Idmaster		 
		 $ponumber = $this->input->post('txtnopp'); 		 		 
		 $query2=   $this->db->select('*')
						 ->where('id_master',$ponumber)	 //Checck Duplicate NOpo / Idmaster					
						 ->get('tbl_master_bmk');		
		$id_user = $this->session->userdata('id');
		 if ($query2->num_rows() == 1) // Jika ketemu buat NOPP baru.
		 {					 			  	
			$strshort = $this->session->userdata('short');//short company
			$stridcom = $this->session->userdata('id_company');//id company
			$strdept = substr(($this->session->userdata('dept')),0,3);
			$strbranchshort = $this->session->userdata('branch_short');
			$stryear = substr(date("Y"),-3,3);
			$strmonth = date("m");			    
			$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	        $qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_bmkno where id_company='".$stridcom."'");
									  	   
	        $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); //check tanggal
	   
			$code = ""; //empty code
			 if ($qdate->num_rows()== 1) :			
						if($qcounter->num_rows()>0):
							foreach($qcounter->result() as $cd):
								$tmp = ((int)$cd->code_max)+1;
								$code = sprintf("%04s", $tmp);;
							endforeach;
							 if ($code== '0000') :						
								 $code = "0001";					 
							 endif;
						 else:					
							 $code = "0001";					 
						 endif;		
			 else:	
				   $code = "0001";					 
			 endif;
		 
		    //update tanggal ketable date--------------
			  $data = array('date_add'=>$strdates );																			
			  $this->db->update('tbl_date_compare',$data); 		 
		   //end--------------------
		 
		     //update tanggal ketable counter--------------
			   $data = array('counter'=> $code );
			   // ada perubahan	
			  $this->db->where('id_company',$stridcom);														  									
			  $this->db->update('tbl_counter_bmkno',$data); 
			  //buat NOPP Baru jika duplicate						  
		    return  substr($strshort,0,2)."/".$strbranchshort."/".$strdept."/".$stryear."/".$strmonth."/".$code;				
		 }else{			
		      //Ambil NOPP yang ada di inputbox posting	 jika tidak duplicate			 
			  $txtmasterid = $this->input->post('txtnopp'); 	  	   	  
	     }
		      //----mengambil data dari inputbox/textbox lalu di simpan didalam variable
			  $txtnovoucher =$this->input->post('txtvoucher');
			  $ses_idcom = $this->session->userdata('id_company');
			  $ses_dept = $this->session->userdata('id_dept');
			  $ses_branch = $this->session->userdata('branch_id');	
			  $txtpayfrom = $this->input->post('txtpayfrom');  			 
			  $cbobank = $this->input->post('cbobank');			
			  $txtsub	= $this->input->post('txtsub');	
			  $txt_desc_detail = $this->input->post('txtdesc[0]'); //*from description detail	  		  		  	  		   	  			  
			  $txtdate_reciep = date('Y-m-d',strtotime($this->input->post('txtdatericived')));
			  $txtdate = date("Y-m-d");			  			 
			  $idcurr = $this->input->post('cbocur');
			  
			  //---------------------unformat grandtotal---------
			  $txt_grand_total =$this->input->post('result'); 
			  if ( strstr( $txt_grand_total, ',' ) ) $txt_grand_total = str_replace( ',','', $txt_grand_total );  
			  //-------------------------------------
			  $strgiro = $this->input->post('txtgirono');			 
			  $status_aktif = '1';			  			 			  	  	  	  	  	  	  			  			
			  $strnote = $this->input->post('txtnote');
			  $strpayefrom = $this->input->post('txtpayfrom'); 					
									 
			//---------------------------------------------- insert tbl_master_pp		      
			
			$data=array('id_master'=>$txtmasterid,
						'no_voucher'=>$txtnovoucher,	
						'id_company'=>$ses_idcom, 
						'id_dept'=>$ses_dept,
						'branch_id'=>$ses_branch,
						'paye_from'=>$txtpayfrom,
						'user_reciep'=>$txtsub,																		
						'header_desc'=>$txt_desc_detail, //*from description detail
						'date_reciep'=>$txtdate_reciep,							
						'date_create'=> date('Y-m-d'),		
						'giro_no' =>$strgiro,	
						'bank' => $cbobank,																											
						'gran_total'=>$txt_grand_total,						
						'id_curr' =>$idcurr,																												
						'status'=>$status_aktif,						
						'note_bmk'=> $strnote						
						);					
										
			$this->db->insert('tbl_master_bmk',$data);	
								

			//------------------detail------------------------------------------------													 				
			$desc = $this->input->post('txtdesc');										
			$result = array();					
				foreach($desc AS $key => $val)
				{					 											  
					 $result[1] = array(			
					  "id_master"   => $_POST['txtidmasdet'][$key],			  
					  "desc"  		=> $_POST['txtdesc'][$key],						  
					  "spec"  		=> $_POST['txtspecs'][$key],
					  "qty"  		=> str_replace(',','',$_POST["txtqty"][$key]),
					  "harga"  		=> str_replace(',','',$_POST["txtprice"][$key]),
					  "total"  		=> str_replace(',','',$_POST["txtamount"][$key]),					
					  "status"		=> $_POST['flagstat'][$key] 					  			
					 );
					 
				 $test= $this->db->insert_batch('tbl_detail_bmk', $result);
				} 					
				return true;			     					
		 
	}
	
	
	function edit_bmk()	
	{
		//master---------------------------------------post		  
	 	  $txtmasterid = $this->input->post('txtnopp'); 	
		  $txtnovoucher =$this->input->post('txtvoucher');
		  $txtsub	= $this->input->post('txtsub');		
		  $txtpayfrom = $this->input->post('txtpayfrom'); 
		  $cbobank = $this->input->post('cbobank');	
		  $cbocur = $this->input->post('cbocur'); 	
		  $txtgiro = $this->input->post('txtgirono');			
		  $txtnotebmk = $this->input->post('txtnote'); 
		  $txtdate_reciep = date('Y-m-d',strtotime($this->input->post('txtdatericived')));			 		 
		  $txtdesc_detail = $this->input->post('txtdesc[0]');				  
		  
		  //---remove row detail pp from fom_edit pp array format ------------		  
		   $get_iddet_remove_row = $this->input->post('txtiddet_buff') ;
		
	       if(isset($get_iddet_remove_row) && trim($get_iddet_remove_row!='')){					 			   
			   $stsrconvert_iddet = explode(",",$get_iddet_remove_row) ;				   			  		   
			   $flag_remove = "0";		   			   
			   for ($intiddet=0; $intiddet < count($stsrconvert_iddet) ; $intiddet++)
			   {				   									   				   	   
					//table detail 								
					$data = array('status'=>$flag_remove);	
										
					$this->db->where('id_detail', $stsrconvert_iddet[$intiddet]);
					$this->db->update('tbl_detail_bmk',$data);						   	 				  
			   }  			
		   }		  
		  //end---------------------------------------------------------------------------
		  
		  		  
		  //---------------------unformat grandtotal---------
			 $txt_grand_total =$this->input->post('result'); 
			  if ( strstr( $txt_grand_total, ',' ) ) $txt_grand_total = str_replace( ',','', $txt_grand_total );  
		  //end------------------------------------- 
		  		  
		 
		
		  $txtmasterid = $this->input->post('txtnopp'); 			 
		  if (isset($txtmasterid) && trim($txtmasterid!=''))
		  {
			    $query = $this->db->select("id_master") //check id booking pada table_parent 
									->where('id_master',$txtmasterid)
									->get('tbl_master_bmk');
				  if ($query->num_rows() == 1)
				  {				
				    //master------------------------------
				  	$data = array( 'user_reciep'=>$txtsub,
									'no_voucher'=>$txtnovoucher,
								   'header_desc'=>$txtdesc_detail, 								  
								   'paye_from'=>$txtpayfrom ,
								   'id_curr'=>$cbocur,
								   'giro_no'=>$txtgiro,
								   'bank' => $cbobank,								  
								   'note_bmk'=>$txtnotebmk,								 								
								   'gran_total'=>$txt_grand_total,								  
								   'date_reciep'=>$txtdate_reciep
								   );					
					$this->db->where('id_master',$txtmasterid);
					$this->db->update('tbl_master_bmk',$data); 
																				
					
					//detail---------------------------					
					  $id_det = $this->input->post('txtiddet');											
					  if ($id_det != "") {
							foreach($id_det AS $key => $val)
							{
								if ($_POST['txtiddet'][$key]!=""){ //jika hanya edit row detail 
								    // echo "masuk edit" ;				 											
									 $result[1] = array(								   
									  "id_detail"   => $_POST['txtiddet'][$key],										
									  "desc"  		=> $_POST['txtdesc'][$key],						  
									  "spec"  		=> $_POST['txtspecs'][$key],
									  "qty"  		=> str_replace(',','',$_POST["txtqty"][$key]),
									  "harga"  		=> str_replace(',','',$_POST["txtprice"][$key]),
									  "total"  		=> str_replace(',','',$_POST["txtamount"][$key])							
									  );												
									  $this->db->update_batch('tbl_detail_bmk',$result,'id_detail');
								}else{  //jika edit row detail dan juga addnew rowdetail	
								    //  echo "masuk save" ;				 												
								      $result[1] = array(			
									  "id_master"   => $_POST['txtidmasdet'][$key],			  
									  "desc"  		=> $_POST['txtdesc'][$key],						  
									  "spec"  		=> $_POST['txtspecs'][$key],
									  "qty"  		=> str_replace(',','',$_POST["txtqty"][$key]),
									  "harga"  		=> str_replace(',','',$_POST["txtprice"][$key]),
									  "total"  		=> str_replace(',','',$_POST["txtamount"][$key]),									
									  "status"		=> "1" 					  			
									 );									 
								    $test= $this->db->insert_batch('tbl_detail_bmk', $result);
								}
									
							} 
					     }
				  	  return true;
				  }
				  
				  
		  }else{
			  return false;
		  }
		  
	}
	
	
	function getedit_master_view_bmk()
	{
	  $get_edit_nopp =$this->input->post('msg');	  		 				    							   				      $flag_status_master = "1" ;
	 
	  if (isset($get_edit_nopp) && trim($get_edit_nopp!=''))
	   {		
	           for ($i=0; $i < count($get_edit_nopp) ; $i++) { 		
					 $this->db->select("id_master,no_voucher,company,name_branch,paye_from,date_create,date_reciep,bank,id_curr,currency,giro_no,note_bmk,gran_total")	;
					 $this->db->where('id_master',$get_edit_nopp[$i]);
					 $this->db->where('status',$flag_status_master);											  	
					 $result_master = $this->db->get('qv_complite_master_bmk');							    				
			   }						
		      return $result_master->result() ;							  		  
		}else{
			  return false;
	    }   
	}
	
	function get_edit_detail_bmk()
	{		
		 
      $get_edit_nopo =$this->input->post('msg');	  		 				    							   				      $flag_status_det = "1" ;
	    	   					  				   			  			   	 		  
	  if (isset($get_edit_nopo) && trim($get_edit_nopo!=''))
	  {		
		  for ($i=0; $i < count($get_edit_nopo) ; $i++) { 	
			 $this->db->select("id_detail,id_master,desc,spec,qty,harga,total")	;
			 $this->db->where('id_master', $get_edit_nopo[$i]);	
			 $this->db->where('status', $flag_status_det);										  	
			 $result = $this->db->get('tbl_detail_bmk');							    				
		   }						
		  return $result->result() ;							  		  
	  }else{
		  return false;
	  }
		
	}
	
	
					
	
	
	function delete_with_edit_flag() {				
		  $delete = $this->input->post('msg');		  				  		 
		  $flag_del = '0';
		  
		  if(isset($delete) && trim($delete!=''))
		  {	   
			  // --------delete table master dan detail pp menggunakan flag status
			    
				for ($i=0; $i < count($delete) ; $i++) { 				    
				    //array variable flag del					
				    $data = array('status'=>$flag_del);						
					//master table 													
					$this->db->where('id_master',$delete[$i]);
					$this->db->update('tbl_master_bmk',$data); 						   				   	   
				   
				    //master detail 													
				    $this->db->where('id_master', $delete[$i]);
				    $this->db->update('tbl_detail_bmk',$data);						   				   	   					  				  
			    }
			   					  	   
			   return true;
		  }else{
			   return false;	  			  		  
		  }
	}
	
	
	/*function delete_checked_bmk() {			// true delete	
		  $delete = $this->input->post('msg');		  				  		 
		  
		  if(isset($delete) && trim($delete!=''))
		  {	   
			 // --------delete table master dan detail pp langsung ke table
			    
				for ($i=0; $i < count($delete) ; $i++) { 		
				   //ubah status delete menjadi 0 pada table master		    
				   $this->db->where('id_master', $delete[$i]);
				   $this->db->delete('tbl_master_bmk');						   				   	   
				   
				   //ubah status delete menjadi 0 pada table detail
				   $this->db->where('id_master', $delete[$i]);
				   $this->db->delete('tbl_detail_bmk');						   				   	   			  				 			    }			   					  	   
			   return true;
		  }else{
			   return false;	  			  		  
		  }
	} */
	public function get_search_date()
	{
	    $strdatestart =$this->input->post('txtdatestart');	
	    $strdatend =$this->input->post('txtdateend');	
	    $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	    $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		$status_fpb_flag = "1";  //flag sudah di print FPB
	    $ses_id_company = $this->session->userdata('id_company');		
		 
	    if($strdatestart !="" and $strdatend !="" ){	
		   $this->load->database();		
	       $this->db->select('id_master,no_voucher,id_company,id_dept,company,dept,paye_from,header_desc,date_create,date_reciep,user_reciep,status,currency,gran_total,short');
		   $this->db->where('id_company',$ses_id_company);		  
		   $this->db->where('date_reciep BETWEEN "'.$datestart.'" and "'.$dateend.'"');
		   $this->db->order_by('date_reciep','desc');
	 	   $result = $this->db->get('qv_complite_master_bmk');		
	       return $result;		 			  		 	   	 
	    }else{
		    redirect('bmk_form/C_create_bmk');	
		}
		  
	}			
	
	
	function search_data() 	// fungsi cari data pp model
	{
	   $cari = $this->input->post('txtcari');
	   $kategory =  $this->input->post('cbostatus');	  
	   
	   $ses_company = $this->session->userdata('id_company');
	   $ses_id_dept = $this->session->userdata('id_dept');
	   $ses_branch_id = $this->session->userdata('branch_id');
	   	   
	   
	   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
	   {
		   redirect('bmk_form/C_create_bmk');  //direct ke url add_menu_booking/add_menu_booking
	   }else{	
		   $this->load->database();		   		  
		   $this->db->where('id_company',$ses_company);
		   //$this->db->where('id_dept',$ses_id_dept);
		   $this->db->where('branch_id',$ses_branch_id);
		   $this->db->like($kategory,$cari);
		   $result = $this->db->get('qv_complite_master_bmk');		 
		   return $result->result(); 
	   }
	} 
	  
 public function select_query_rpt_bmk()
	 {		 
		
		$strid_master =$this->input->post('msg'); 	 
	    for ($i=0; $i < count($strid_master) ; $i++) { 	
		     $str_hasil_idamaster = $strid_master[$i];			 
		}
				
		//--------------------------------------------
		
		$str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete				
		  //select field buat di cetak ------------------------------------	
			  $query = $this->db->select("no_voucher,desc,spec,qty,harga,total,paye_from,currency,gran_total,user_reciep,note_bmk,giro_no,date_reciep,date_create,bank")
								->where('id_master',$str_hasil_idamaster)
								->where('status',$str_status_terdelete)								
								->get('qv_detail_master_bmk');
			  if ($query->num_rows() >= 1)
			  {		
			  	 foreach ($query->result() as $row)
				 {
			  		$data = array('sesidmaster'      => $str_hasil_idamaster,							
					              'ses_no_voucher'    => $row->no_voucher,								  	 
								  'ses_paye_from'    => $row->paye_from,								  	
								  'ses_user_sub'     => $row->user_reciep,									
								  'ses_notebmk'      => $row->note_bmk,									  								
								  'ses_date_reciep'  => $row->date_reciep,								 	
								  'ses_curr'	     => $row->currency,
								  'ses_giro'		 =>	$row->giro_no,
								  'ses_bank'		 =>	$row->bank,							  									
								  'ses_grand'        => $row->gran_total								
								  ); //buat array di session		
					$this->session->set_userdata($data); //simpan di session
				 }													 										  
					 return $query->result();						 													
			  }else{				  
					 return false;
			  }
	 }							    					 	 	 	    

  
//------------------------------------------------------------------------------------------------------------------		
		   
				 
}