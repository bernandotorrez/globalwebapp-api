<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_create_bmk extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_master_bmk';
	 
    public function tampil_add_pp(){ //tampil table untuk ngececk num_row						     	     		  
	     $ses_id_dept = $this->session->userdata('id_dept'); //manggili session id 
		 $ses_id_company = $this->session->userdata('id_company');  	
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 
		 $short_company = $this->session->userdata('short');	 		 		
		 $ses_branch_short = $this->session->userdata('branch_short'); 
		 $ses_dept = $this->session->userdata('dept');	 		 
		 $status_delete = "1" ;		  		 		 				 
		 $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('id_company',$ses_id_company);	
		 $this->db->where('branch_id',$ses_id_branch );						 
		 $this->db->where('status',$status_delete);		
		 $this->db->order_by('date_reciep','desc');
		 $result = $this->db->get('qv_complite_master_bmk');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	     $ses_id_dept = $this->session->userdata('id_dept'); 
		 $ses_id_company = $this->session->userdata('id_company');  	
		 $ses_id_branch = $this->session->userdata('branch_id');
		 $short_company = $this->session->userdata('short');	 		 		
		 $ses_branch_short = $this->session->userdata('branch_short'); 
		 $ses_dept = $this->session->userdata('dept');	 		 				
		 $status_delete = "1" ; 	    
				 		 
	     $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('id_company',$ses_id_company);					
		 $this->db->where('branch_id',$ses_id_branch );			
		 $this->db->where('status',$status_delete);			  		 
		 $this->db->order_by('date_reciep','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_master_bmk')->result();		 		   
		 return $result ;
	}	
		
     public function select_query_rpt_bmk()
	 {		 
		
		$strid_master =$this->input->post('msg'); 	 
		$str_status_terdelete = "1"; // jika "1" record masih aktif dan belum terdelete			
		  //select field buat di cetak ------------------------------------	
			  $query = $this->db->select("desc,spec,qty,harga,total,paye_from,currency,gran_total,user_reciep,note_bmk,date_create,date_reciep,giro_no,bank")
								->where('id_master',$strid_master)
								->where('status',$str_status_terdelete)							
								->get('qv_detail_master_bmk');
			  if ($query->num_rows() >= 1)
			  {		
			  	 foreach ($query->result() as $row)
				 {
			  		$data = array('sesidmaster'      => $strid_master,							
								  'ses_paye_from'    => $row->paye_from,								  	
								  'ses_user_reciep'  => $row->user_reciep,									 
								  'ses_note_bmk'     => $row->note_bmk,									  
								  'ses_date_create'  => $row->date_create,	
								  'ses_date_reciep'  => $row->date_reciep,								
								  'ses_curr'	     => $row->currency,								  									
								  'ses_grand'        => $row->gran_total							
								  ); //buat array di session		
					$this->session->set_userdata($data); //simpan di session
				 }													 										  
					 return $query->result();						 													
			  }else{				  
					 return false;
			  }
	 }										  		
				    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}