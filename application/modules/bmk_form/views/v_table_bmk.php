<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">



 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>
      
<?php   	
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>      
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>                        
                <?php  echo "Send Approval Successfully"	?>  
		  </div>		
<?php } ?>

<?php   	
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>                
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>                        
			<?php  echo "Send Approval failed"	?>  
        </div>		      
<?php } ?>	

 
  
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" ><?php echo "Create BMK"." | ".$header ; ?> </div>
   <div class="panel-body">               
   
          
 <?php  echo form_open('bmk_form/c_crud_bmk/multiple_submit');  ?>     
 
 <!-- Menu Button Start -->
			
 <div class="row text-right">
        <button id="btnadd" name="btnadd" class="btn btn-app btn-primary btn-xs radius-4" value="Add New" type="submit">
          <i class="ace-icon fa fa-book bigger-160"></i>
            Add New
        </button>

        <button id="btnedit" name="btnedit" class="btn btn-app btn-success btn-xs radius-4 btn_edit" value="Edit" type="submit" disabled>
          <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
            Edit
        </button>

        <button id="btndel" name="btndel" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" value="Delete" type="submit" onclick="return confirm('are you sure want to delete this data!')" disabled>
          <i class="ace-icon fa fa-refresh bigger-160"></i>
            Delete
        </button>

        <button id="btnprintbmk" name="btnprintbmk" class="btn btn-app btn-warning btn-xs radius-4 btnprintbpk" value="Print Bmk" type="submit" disabled>
          <i class="ace-icon fa fa-print bigger-160"></i>
            Print BMK
        </button>  
				
        <!-- Search Menu Start -->
    

    <table align="left">
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="text-center"><label>Select: </label></td>
            <td>&nbsp;

            </td>
            <td>
                <select name="cbostatus" id="cbostatus" class="form-control">
                    <option value="id_master">BMK NO</option>
                    <option value="no_voucher">Voucher  No </option>
                    <option value="header_desc">Description</option>
                    <option value="paye_from">Pay from</option>                          
                    <option value="gran_total">Total Ricieve</option>       
                </select>
            </td>
            <td>&nbsp;

            </td>

            <td>
                <input type="text" name="txtcari" size="15" class="form-control" placeholder="Search" id="txtcari"
                    value="<?php echo $this->session->flashdata('cari'); ?>">

            </td>

            <td>
                <button id="btncari" name="btncari" class="btn btn-app btn-primary btn-xs radius-4" value="Search"
                    type="submit">
                    <i class="ace-icon fa fa-search bigger-160"></i>
                    Search
                </button>
            </td>

        </tr>
    </table>

    <table align="left">
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="text-center"><label>Date: </label></td>
            <td>&nbsp;

            </td>
            <td>
                <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2"
                    data-link-format="dd-mm-yyyy">
                    <input id="txtdatestart" size="15" name="txtdatestart" readonly type='text'
                        data-date-format="dd-mm-yyyy" class="form-control datepicker" />
            </td>
            <td>&nbsp;

            </td>
            <td class="text-center"><label>S / D </label></td>
            <td>&nbsp;

            </td>
            <td>
                <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2"
                    data-link-format="dd-mm-yyyy">
                    <div class="input-group">
                        <input id="txtdateend" size="15" name="txtdateend" readonly type='text'
                            data-date-format="dd-mm-yyyy" class="form-control datepicker" />

                    </div>
                </div>
            </td>
            <td>&nbsp;

            </td>
            <td>
                <button id="btncaridate" name="btncaridate" class="btn btn-app btn-primary btn-xs radius-4"
                    value="Search" type="submit">
                    <i class="ace-icon fa fa-search bigger-160"></i>
                    Search
                </button>
            </td>
        </tr>
    </table>
    </div>

		
			<!-- Menu Button End -->
    <div class="space-12"></div>
    <!-- Search Menu End --> 

    <!-- Table Responsive -->
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th> BMK No </th>
                    <th> Voucher No </th>
                    <th> Company </th>
                    <th> Payee Form </th>
                    <th> Description </th>
                    <th> Date Receive </th>
                    <th> Cur </th>
                    <th> Total Receive </th>
                    <th> BMK Create </th>
                    <th> View Detail </th>
                    <th> <a href="javascript:void(0)" onclick="CheckAll()" style="text-decoration:none"> All</a> /
                        <a href="javascript:void(0)" onclick="UncheckAll()" style="text-decoration:none">Uncheck</a>
                    </th>
                </tr>


            </thead>

            <tbody>

                <?php 	
                    
                    if ($ceck_row >= 1) {		  
                        foreach($bmk_view as $row){	   			 					   
                                
                    ?>

                <tr style="font-size: 12px;">

                    <td width="8%"><?php echo $row->id_master ; ?></td>
                    <td align="center"><?php echo $row->no_voucher ; ?></td>
                    <td><?php echo $row->short ; ?></td>
                    <td><?php echo $row->paye_from ; ?></td>
                    <td><?php echo $row->header_desc ; ?></td>
                    <td align="center"><?php echo date('d-m-Y', strtotime($row->date_reciep)) ; ?></td>
                    <td align="center"><?php echo $row->currency ;?></td>
                    <td align="right"><?php echo number_format($row->gran_total,2,'.',',') ;?></td>
                    <td align="center"><?php echo $row->user_reciep ;?></td>
                    <td align="center">
                        <a href="#" class="btn btn-danger btn-setting print" id="print" style="text-decoration:none"
                            req_id="<?php echo $row->id_master ; ?>"><span class="glyphicon glyphicon-eye-open"
                                aria-hidden="true"></span> Detail</a>
                    </td>
                    <td>
                        <div align="center"><input id="checkchild" name="msg[]" type="checkbox"
                                value="<?php echo $row->id_master ; ?>" />
                            <?php form_close(); ?>
                        </div>
                    </td>
                </tr>
                <?php } } ?>
            </tbody>

        </table>


    </div>
    </div>

    <div class="table-responsive">
        <?php		   			
		     if (! empty($pagination))			
			 { 
		  	   echo '<center><table align="center" class="table">';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table></center>';				 
			 }			  	  						
		?>

    </div>

    </div>

    <!-- Table Responsive -->
          
  </div>
 </div>
   
</div>

<!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Info & Note BMK</h4>
                    </div>
                    
                    <div class="modal-body">
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->            
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal -->  	

<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numerala buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 

<script>
$(function() {
  $("#txtcari").focus();
});		  
//set focus ----------------------------  
</script>


<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.print',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("bmk_form/c_create_bmk/get_nopp_modal"); ?>';
		
		$("#myModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});
	
</script>


<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.reject',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("c_create_pp/c_create_pp/get_info_reject"); ?>';
		
		$("#myModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});
</script>

<script>

// fungsi untik checklist semua otomatis
function CheckAll()
{
    l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = true;		
		}	
		document.getElementById('btnedit').disabled = true;	
		document.getElementById('btndel').disabled = false;	
		document.getElementById('btnprintbpk').disabled = true;	
}

function UncheckAll()
{
   l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = false;			 
		}
		document.getElementById('btnedit').disabled = true;
		document.getElementById('btndel').disabled = true;
		document.getElementById('btnprintbpk').disabled = true;
}


//--------------------function disbaled enabled button with check book

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_edit').removeAttr('disabled');			
        }
        else{
            $('.btn_edit').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btnprintbpk').removeAttr('disabled');			
        }
        else{
            $('.btnprintbpk').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btn_delete').removeAttr('disabled');			
        }
        else{
            $('.btn_delete').attr('disabled', 'disabled');								
        }
    });
});

</script>


<?php if ($this->session->userdata('id_group') != 'admin001'): // jika status session user nya 7?>
   <script>
	$(document).ready(function() { 	
		$('#btndel').hide();		
	});
	</script>
<?php endif ?>