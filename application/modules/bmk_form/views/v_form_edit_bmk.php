<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_fail') ;?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_insert_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_insert_fail');?>  
        </div>	
<?php } ?>

          <div class="table-header btn-info">
            Edit BMK
          </div>

          <?php  echo form_open_multipart('bmk_form/c_crud_bmk/multiple_submit',array('class'=>'form-multi'));  ?>

          <!-- Responsive Form for Mobile and Web Start -->
          <div class="container">

            <?php
            if ($check_row >= 1) :  
            foreach($tampil_edit_master_bmk as $row1) :
          ?>

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">BMK Number</label>

                  <input type="text" id="txtnopp" name="txtnopp" class="form-control" readonly="readonly" value="<?php echo  $row1->id_master ?>"  />
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">Date Receive *</label>

                  <input type="text" id="txtdatericived" name="txtdatericived"  data-date-format="dd-mm-yyyy" class="form-control datepicker" readonly="readonly" value="<?php echo date('d-m-Y',strtotime($row1->date_reciep)) ?>">
                </div>
              </div>

            </div>
            <!-- Row End -->

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">Date Submission</label>

                  <input type="text" id="txtdate" name="txtdate" class="form-control" readonly value="<?php echo date("d-m-Y") ?>" />
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">Payee Form *</label>

                  <input type="text" id="txtpayfrom" name="txtpayfrom" class="form-control" maxlength="60" value="<?php echo $row1->paye_from ?>"/>
                </div>
              </div>

            </div>
            <!-- Row End -->

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">Company</label>

                  <input type="text" id="txtcompany" name="txtcompany" class="form-control" value="<?php echo $this->session->userdata('company');  ?>"  readonly />
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">Bank *</label>

                  <select name="cbobank" class="form-control">
                    <option><?php  echo $row1->bank ?></option>                      
                                <?php
                                    foreach ($tampil_bank as $row_bank) :						
                                        echo '<option>'.$row_bank->name_bank.'</option>';
                                    endforeach;
                                ?>
                        <option> - </option>
                     </select>
                </div>
              </div>

            </div>
            <!-- Row End -->

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">Branch</label>

                  <input type="text" id="txtbranch" name="txtbranch" class="form-control" value="<?php echo $this->session->userdata('name_branch'); ?>" readonly />
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">Cheque / Giro No *</label>

                  <input name="txtgirono" type="text" class="form-control" id="txtgirono" placeholder="Cheque / Giro Number" onkeypress="return isNumberKey(event)"  value="<?php echo $row1->giro_no ?>"  maxlength="40"  />
                </div>
              </div>

            </div>
            <!-- Row End -->

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">Department</label>

                  <input type="text" id="txtdept" name="txtdept" class="form-control" value="<?php echo $this->session->userdata('dept'); ?>" readonly />
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">Currency</label>

                  <select id="cbocur" name="cbocur"  class="form-control">
                    <option value="<?php echo $row1->id_curr ;?>"><?php echo $row1->currency ;?></option>
	             <?php foreach($tampil_curr as $row_curr){ ?>               
               		 <option value="<?php echo $row_curr->id_curr; ?>"><?php echo $row_curr->currency ;?></option>
                 <?php } ?>  
               </select>
                </div>
              </div>

            </div>
            <!-- Row End -->

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">BMK Create By *</label>

                  <input type="text" id="txtsub" name="txtsub" class="form-control" value="<?php echo $this->session->userdata('name'); ?>"  readonly="readonly"/>
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label" style="color:#900">Note BMK *</label>

                  <textarea name="txtnote" class="form-control" id="txtnote"><?php echo $row1->note_bmk ?></textarea>
                </div>
              </div>

            </div>
            <!-- Row End -->

            <!-- Row Start -->
            <div class="row">
              <div class="col-sm-5">
                <div class="form-group">
                  <label class="control-label">No Voucher</label>

                  <input type="text" id="txtvoucher" name="txtvoucher" class="form-control"  maxlength="60" value="<?php echo $row1->no_voucher ?>"/>
                </div>
              </div>

              <div class="col-sm-1">


              </div>

              <div class="col-sm-5">
                <div class="form-group">
                 
                </div>
              </div>

            </div>
            <!-- Row End -->

            <?php endforeach ; ?>
            <?php endif ;?>

            <div class="table-responsive">
                <div class="panel-heading" id="label_head_panel">

                  <span style="color:#A6090D; font-weight:bold; ">
                    Grand Total :
                  </span>
                  <input  maxlength="70" id="result" name="result" class=" total" readonly="readonly" style="color:#A6090D"  value="<?php echo number_format($row1->gran_total ,2) ?>" />
       
                  <input  type="text" name="txtiddet_buff" hidden="true"  readonly="readonly" class ="buffarray_iddet" width="70" />
                </div>

            <table class="table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Description</th>
                  <th>Specs</th>
                  <th>Qty</th>
                  <th>Price</th>
                  <th>Total Price</th>

                  <th align="center"><strong>
                      <input type="button" id="add" Class="btn-sm radius-6 btn-info" value="+" />
                  </th>
                </tr>
              </thead>

              <tbody class="detail">

              <?php  foreach($tampil_det_bmk as $row2  ){ $ino++ ; // buat  perulangan tampilan detail ?> 		
                <tr>
                        <td class="no" align="center"><?php echo $ino ;?></td>
                        <td>        
                         <input type="text" name="txtidmasdet[]" maxlength="25"  value="<?php echo $ppnumber  ?>"  hidden="true"  />                                     
                        <input type="text" name="txtiddet[]" value="<?php echo $row2->id_detail ?>" class="txtid"  hidden="true" />       
                                       
                        <input type="text" maxlength="70" id="txtdesc"  name="txtdesc[]"  placeholder="Description" size="24" class="form-control txtdesc"  value="<?php echo $row2->desc ?>"/>
                        </td>
                        <td>
                          <input type="text" maxlength="70"  name="txtspecs[]"  placeholder="Specs" size="30" class="form-control txtspecs" value="<?php echo $row2->spec ?>" />
                        </td>                     
                        <td><input maxlength="6" style="text-align: center;" name="txtqty[]" type="text" placeholder="0" size="12" onkeypress="return isNumberKey(event)" class="form-control quantity" value="<?php echo number_format($row2->qty) ?>" />  
                        </td>                                                		                                  
                        <td>
                         <input maxlength="14" style="text-align: right;" name="txtprice[]" type="text" placeholder="0.00" size="20" onkeypress="return isNumberKey(event)" class="form-control price" value="<?php echo number_format($row2->harga,2) ?>"  /> 
                        </td>
                        
                       <td>
                  			<input  maxlength="70" style="text-align: right;" name="txtamount[]"  placeholder="0.00"  size="30"  class="form-control amount" readonly="readonly" value="<?php echo number_format($row2->total,2) ?>" />                                                                                                                   
                       </td>
                        <td >                        
                        <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button>
                        </td>
                       
                     </tr>

              <?php } ?>

              </tbody>

            </table>
				 		

            <table>
              <tr>
                <td>
                  <button class="btn btn-app btn-info btn-xs radius-4 btnsave" type="submit" id="btnsave_edit"
                    name="btnsave_edit" value="save">
                    <i class="ace-icon fa fa-book sm-160"></i>
                    Submit
                  </button>


                </td>

                <td hidden>
                  <input id="btnsend" name="btnsend" type="submit" value="Save & Send Approval"
                    class="btn btn-default btn-clean" />
                </td>

                <td>
                  <a href="<?php echo base_url('bpk_form/c_create_bpk');?>" style="text-decoration:none;"
                    class="btn btn-app btn-success btn-xs radius-4 btnback"> <i
                      class="ace-icon fa fa-exchange bigger-160"></i>Back</a>
                </td>
              </tr>
            </table>

          </div>
          <!-- Responsive Form for Mobile and Web End -->

          <?php form_close();?>
      
    </div> <!-- /panel -->             
</div>  <!-- /div class="col-lg-12 -->   

 <div class="space-32"></div>
 <?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>    
 
<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numerala buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 



<script type="text/javascript">
//--------------function number only
 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	

//----------------set focus----
 $(function() {
  $("#txtsub").focus();    
});	

</script>


<script script type="text/javascript">

//calculate and add field--------------
$(function(){		
	$('#add').click(function(){
	  addnewrow();	
	 }); 	
	 
	
	 $('body').delegate('.remove','click',function(){		 					 				 		 
	 	 $(this).parent().parent().remove();
		   
		   //Untuk menghitung pengurangan Grand total di kurang total amount. ketika di remove
		   var tr        =  $(this).parent().parent();  
		   var amtprice  =  tr.find('.amount').val();
		   var get_iddet =  tr.find('.txtid').val();//getiddetailpp from inputboxarray name=txtiddet class=txtid
		   var gt 	     = 	$('.total').val() ;
		   var hasil     =  numeral().unformat(gt) - numeral().unformat(amtprice);
		   		   
		   $('.total').val(numeral(hasil).format('0,0.00')) ;		   		   		 
		   //end----------------------------------------------------
		   
		   //simpan pada array	inputbox name = iddet_buff class =buffarray_iddet --- 											
		    var  buff_iddet =$('.buffarray_iddet').val()			
		    if  (buff_iddet == "") { 	
			    $('.buffarray_iddet').val( get_iddet);
			}else{
		        $('.buffarray_iddet').val($('.buffarray_iddet').val() + "," + get_iddet );
			}
		   
		   /* var tr = '<input type="text" name="txtiddet[]" value = "' + get_iddet + '" class="txtiddet" />' ;		           $('.remove').append(tr);  */
		  //end----------------------------------------------------------------------------
				 						   			  		  			 		 
	 });
	 	
	 
	  $('body').delegate('.quantity,.price','change',function(){  
		  var tr =  $(this).parent().parent();
		   var qty=numeral().unformat(tr.find('.quantity').val());
		   var price=numeral().unformat(tr.find('.price').val());
		   var amt=numeral().unformat(tr.find('.amount').val());
		   var amt=qty*price;             
		   tr.find('.amount').val(amt);
		   total(); 
 	 });
	 
	$('body').delegate('.quantity,.price','blur',function(){  
		   var tr =  $(this).parent().parent();
		   var qty=tr.find('.quantity').val();
		   var price=tr.find('.price').val();
		   var amt=tr.find('.amount').val();
		   
		   tr.find('.quantity').val(numeral(qty).format('0,0')) ;
		   tr.find('.price').val(numeral(price).format('0,0.00')) ;
		   tr.find('.amount').val(numeral(amt).format('0,0.00')) ;
		   total();
 	 });  	 	
	 	  	
});


function total()
{
	var t=0;
	var jmlbaris=parseInt($('.remove').length);
	for(var i=0;i<jmlbaris;++i){
	   t+=parseFloat(numeral().unformat($('.amount').eq(i).val()));
	}
	$('.total').val(numeral(t).format('0,0.00')) ;	
			
}

function addnewrow()
{

var n = ($('.detail tr').length-0)+1;	
var tr = '<tr>'+
'<td class="no" align="center">'+ n +'</td>'+                                            
'<td><input type="text" name="txtidmasdet[]" value="<?php echo $ppnumber ?>" hidden="true" /><input type="text" name="txtiddet[]" class="txtid" hidden="true" /><input type="text" maxlength="70"  name="txtdesc[]"  placeholder="Description" size="24" class="form-control txtdesc" /></td>'+'<td><input type="text" maxlength="70"  name="txtspecs[]"  placeholder="Specs" size="30" class="form-control txtspecs" /></td>'+'<td><input maxlength="6" id="txtqty" style="text-align: center;" name="txtqty[]" type="text" placeholder="0" size="7" onkeypress="return isNumberKey(event)" class="form-control quantity" /></td>'+                                                		                                  
'<td><input maxlength="14" style="text-align: right;" name="txtprice[]" type="text" placeholder="0.00" size="30" onkeypress="return isNumberKey(event)" class="form-control price"/> </td>'+	
'<td><input  maxlength="70" style="text-align: right;"  name="txtamount[]"  placeholder="0.00"  size="35"  class="form-control amount" readonly="readonly"/></td>'+'<td>  <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button></td>'+
'</tr>';
 
 $('.detail').append(tr);
}

</script>