

	 	
<script>
$(document).ready(function() {
	<?php
	 if ($this->session->userdata('level_access') == "1") { ?>
		$('#cbocomp').prop('readonly', false); 
		$('#cbodept').prop('readonly', false);
	<?php }?>	
}); 
</script>

<script>

//email validation------------------------------------------------------------
function validate_email() {
    var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
	var div = $("#emailcc_book").closest("div");
    if ($('#emailcc_book').val()!="" && !testEmail.test($('#emailcc_book').val())){
	  //alert('contact email not allowed');
	  $('[data-toggle="tooltip"]').tooltip('show');//tooltips
	  div.removeClass("has-success");
	  $("#glypcn"+"emailcc_book").remove();
	  div.addClass("has-error has-feedback");
	  div.append('<span id="glypcn'+'emailcc_book'+'" class="glyphicon glyphicon-remove form-control-feedback"></span>');
	  $("#emailcc_book").val("");
	}else{
	  div.removeClass("has-error");
	  $("#glypcn"+"emailcc_book").remove();
	  div.addClass("has-success has-feedback");
	  div.append('<span id="glypcn'+'emailcc_book'+'" class="glyphicon glyphicon-ok form-control-feedback"></span>');		
	  $('[data-toggle="tooltip"]').tooltip('hide');
	}
}
//end email validation--------------------------------------------------

//submit to table--------------------------------------------
$(document).ready(function(){ 
  $('.btnsaveadd').click(function(){
	    var date_phas =  $("#datebook").val();
	    var url = "<?php echo site_url('book_meeting_room/C_book_meeting_room/do_save'); ?>";
		var urlcektime = '<?php echo site_url("book_meeting_room/C_book_meeting_room/do_chek_time"); ?>'
	    var formData = new FormData(document.getElementById('form-calender'));
		var timephasing =$('#jamstart_add').val()
	    var m =  $('#ViewloadinglModal');
		var madd = $('#ModalAdd');
		 // alert(date_phas);
			   if ($('#cbocomp').val()==""){
					  alert("company Must  be Required");
			   }else{
					 if ($('#cbodept').val()==""){
						alert("Departemen Must be Required");
				     }else{
					   	 if ($('#roomname').val()==""){
				   			alert("Meeting Room name Must be Required");
					   	 }else{
							 if ($('#username').val()==""){
								alert("Submmission User Must be Required");
					  		 }else{
								 if ($('#participan').val()==""){
									alert("Participant Must be Required");
								 }else{
									 if ($('#meet').val()==""){
										alert("Meeting Info Must be Required");
									 }else{							    
											 //check time(jam)----------------------------------
											 var timefrom = new Date();
											 temp = $('#jamstart_add').val().split(":");
											 timefrom.setHours((parseInt(temp[0]) - 1 + 24) % 24);
											 timefrom.setMinutes(parseInt(temp[1]));
											
											 var timeto = new Date();
											 temp = $('#jamend_add').val().split(":");
											 timeto.setHours((parseInt(temp[0]) - 1 + 24) % 24);
											 timeto.setMinutes(parseInt(temp[1]));
										
										     if (timeto < timefrom){ //jika waktu start lebih besar dibandingkan waktu selesai
												  alert('start time should be smaller than end time!');
											 }else{
												  function callBeforeAjax(){
												 	 m.modal('show');
												  }		
												
												//after validation--------------------------------------
												   $.ajax({
													   beforeSend: callBeforeAjax,
													   url:url,
													   method:"POST",
													   data: formData,
													   async: false,
													   processData: false,
													   contentType: false,
													   cache : false,
													   success:function(data){   
													 		setTimeout(function(){ 
														    m.modal('hide'); }, 3000);
															location.reload();
											       	   },  
													   complete:function(data) {   
														    alert("Insert Successfull!!");
															//alert(data);
															location.reload();
													   },  
													});
											     }
											} 
							           }
								 }
							 }
						 }
				   } //endif last
 	 });
});
</script>

<script>
$(document).ready(function(){ //fungsi check file excel jika exist.
  $('.btnsaveedit').click(function(){
	  var url = "<?php echo site_url('book_meeting_room/c_book_meeting_room/do_save'); ?>";
	     // var form = $('form')[0];
	     // var formData = form ;  
	     var formData = new FormData(document.getElementById('form-calenderedit'));
		
			   if ($('#cbocomp_edit').val()==""){
					  alert("company Must  be Required");
			   }else{
					 if ($('#cbodept_edit').val()==""){
						alert("Departemen Must be Required");
				     }else{
					   	 if ($('#roomname_edit').val()==""){
				   			alert("Meeting Room name Must be Required");
					   	 }else{
							 if ($('#username_edit').val()==""){
								alert("Submmission User Must be Required");
					  		 }else{
								 if ($('#participan_edit').val()==""){
									alert("Participant Must be Required");
								 }else{
									 if ($('#meet_edit').val()==""){
										alert("Meeting Info Must be Required");
									 }else{
										   
											//after validation--------------------------------------
											   $.ajax({
												    url:url,
												    method:"POST",
												    data: formData,
												    async: false,
												    processData: false,
												    contentType: false,
												    cache : false,
												    success:function(data){  
												    
													  alert("Update Successfull!!");
													  location.reload();
													},  
												});
									     }
									 }
								 }
							 }
						 }
				   } //endif last
 	 });
});
</script>

<link rel="stylesheet" href="assets/css/chosen.min.css" />

     <div class="page-header btn-light" align="center">
        <h1>
            Calendar Date Booking Of Meeting Room 
        </h1>
      </div><!-- /.page-header -->
    <div class="row">
   		 <div class="col-xs-10 col-md-offset-2">
        	  <div id="calendar" class="col-xs-10" ></div>   
         </div>     
     </div>    
     <p> 
		
		<!-- Modal -->
		<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
			<form class="form-horizontal" id="form-calender"  method="POST" action="">
			
			  <div class="modal-header">
				<button type="submit" class="close refresh" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Booking Meeting Room Apoitment</h4>
			  </div>
              <p>
             
			  <div class="modal-body">
              
			<div class="form-group">
                <label for="company" class="col-sm-3 control-label">Company : </label>
               <div class= "col-sm-4 col-md-offset-1">
                    <select data-placeholder="Company " id="cbocomp" name="cbocomp" class="myselect" tabindex="2" style="width: 100%;" readonly>
		         <option value="<?php echo $this->session->userdata("id_company");?>"><?php echo $this->session->userdata("company") ;?></option>  
	             <?php foreach($book_company as $row){ ?>
               		 <option value="<?php echo $row->id_company ;?>"><?php echo $row->company ;?></option>
                 <?php } ?>
                     </select>
                </div>
              </div>
              
              <div class="form-group">
                <label for="dept" class="col-sm-3 control-label">Dept : </label>
               <div class= "col-sm-5 col-md-offset-1">
                  
                    <select data-placeholder="Departemen" id="cbodept" name="cbodept" class="myselect" tabindex="2" style="width: 100%;" readonly>
		          <option value="<?php echo $this->session->userdata("id_dept") ;?>"><?php echo $this->session->userdata("dept") ;?></option>  
	             <?php foreach($book_dept as $row2){ ?>
               		 <option value="<?php echo $row2->id_dept ;?>"><?php echo $row2->dept ;?></option>
                 <?php } ?>
                     </select>
                </div>
              </div>
              <div class="form-group">
                <label for="roomname" class="col-sm-3 control-label">Room Name : </label>
               <div class= "col-sm-4 col-md-offset-1">
                    <select data-placeholder="Room Name " id="roomname" name="roomname" class="myselect" tabindex="2" style="width: 100%;">
		         <option value="">- Choose -</option> 
	             <?php foreach($book_roomname as $row3){ ?>
               		 <option value="<?php echo $row3->id_room_meeting ;?>"><?php echo $row3->room_name ;?></option>
                 <?php } ?>
                
                 </select>
                      
                </div>
              </div>
               <div class="form-group">
					<label for="jam Start" class="col-sm-3 control-label" style="color:#06F"> Start Time Available:</label>
					<div class= "col-sm-2 col-md-offset-1">
                     
					 <!-- <input type="text" name="jamstart_add" class="form-control" id="jamstart_add"  readonly="readonly" > -->
                     <select id="jamstart_add" name="jamstart_add" class="form-control" disabled="disabled" >
   
                      
                       <?php
					      /*
					        $begin = new DateTime("08:30");
							$end   = new DateTime("18:00");
							
							$interval = DateInterval::createFromDateString('30 min');
							
							$times    = new DatePeriod($begin, $interval, $end);
							
							foreach ($times as $time) {
								echo "<option>".$time->format('H:i')."</option>";
								//echo $time->format('H:i'), '-', 
									// $time->add($interval)->format('H:i'), "\n"
									 //;
							}   
						  */
						?>	
                     </select>
                     
					</div>
				  </div>
                  <div class="form-group">
					<label for="jam End" class="col-sm-3 control-label">Time End :</label>
					<div class= "col-sm-2 col-md-offset-1">
                    
					 <!-- <input type="text" name="jamend_add" class="form-control " id="jamend_add"  readonly="readonly" value="0:00"> -->
                      <select class="form-control" id="jamend_add" name="jamend_add" disabled="disabled" >
                       <?php
					     
					      /*  $begin = new DateTime("08:30");
							$end   = new DateTime("18:00");
							
							$interval = DateInterval::createFromDateString('30 min');
							
							$times    = new DatePeriod($begin, $interval, $end);
							
							foreach ($times as $time) {
								echo "<option>".$time->format('H:i')."</option>";
							} 	   
						  
						*/?>	
                     </select>
                      
					</div>
				  </div>
                   <div class="form-group">
					<label for="jam Start" class="col-sm-3 control-label" style="color:#F36">Info Time Booked :</label>
					<div class= "col-sm-7 col-md-offset-1">
					  <select class="form-control " id="listbookedtime" name="listbookedtime" multiple="multiple">
                      </select>
					</div>
				  </div>
                   
				  <div class="form-group">
					<label for="datebook" class="col-sm-3 control-label">Date Book:</label>
					<div class= "col-sm-5 col-md-offset-1">
					  <input type="text" name="datebook" class="form-control" id="datebook" readonly="readonly">
					</div>
				  </div>
              
               <div class="form-group">
					<label for="usersubmision" class="col-sm-3 control-label">User submission : </label>
					<div class= "col-sm-5 col-md-offset-1">
					  <input type="text" name="username" class="form-control" id="username" placeholder="User submission" value="<?php echo $this->session->userdata("name") ;?>" readonly>
					</div>
				  </div>
                   <div class="form-group">
					<label for="participan" class="col-sm-3 control-label">Participan : </label>
					<div class= "col-sm-1 col-md-offset-1">
					  <input type="text" name="participan" class="form-control" id="participan" placeholder="1" >
					</div>
				  </div>
                
				 
				  <div class="form-group">
					<label for="color" class="col-sm-3 control-label">Color : </label>
					<div class= "col-sm-5 col-md-offset-1">
					  <select name="color" class="form-control" id="color">
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquoise</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; Dark blue</option>
						  <option style="color:#008000;" value="#008000">&#9724; Green</option>												  						  <option style="color:#FFD700;" value="#FFD700">&#9724; Yellow</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; Orange</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; Red</option>
						  <option style="color:#000;" value="#000">&#9724; Black</option>
						  
						</select>
					</div>
				  </div>
                  
                   <div class="form-group">
					<label for="title" class="col-sm-3 control-label">Email CC : </label>
					<div class= "col-sm-7 col-md-offset-1">
					  <input type="text" name="emailcc_book" class="form-control" id="emailcc_book" placeholder="admin1@eurokars.co.id" onblur="validate_email()" data-toggle="tooltip" data-placement="bottom" title="contact email not allowed" data-trigger="manual" >
					</div>
                 </div> 
				 
					 <div class="form-group">
					<label for="title" class="col-sm-3 control-label">Meeting info : </label>
					<div class= "col-sm-5 col-md-offset-1">
					  <textarea name="meet" class="form-control" id="meet" placeholder="Meeting info"></textarea>
					</div>
                    
                    
				  </div>
			  </div>
			  <div class="modal-footer">
             	
				<button type="button" class="btn btn-app btn-info btn-xs radius-4 btnsaveadd" id="btnsaveadd"><i class="ace-icon fa fa-pencil-square-o bigger-160"></i>Save</button>
             
               <button type="submit" class="btn btn-app btn-danger btn-xs radius-4 refresh" data-dismiss="modal"><i class="ace-icon fa 	fa-arrow-circle-o-down  bigger-160"></i>Close</button>

			  </div>
			</form>
			</div>
		  </div>
		</div>
		
		
		
		<!-- Modal -->
		<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal"  method="POST" id="form-calenderedit" action="">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Edit Booking Meeting Room Apoitment</h4>
			  </div>
              <p>
			  <div class="modal-body">
              
			<div class="form-group">
                <label for="company" class="col-sm-3 control-label">Company : </label>
               <div class= "col-sm-4 col-md-offset-1">
                    <select data-placeholder="Company " id="cbocomp_edit" name="cbocomp_edit" class="myselect" tabindex="2" style="width: 100%;">
		         <option value="<?php echo $this->session->userdata("id_company");?>"><?php echo $this->session->userdata("company") ;?></option>  
	             <?php foreach($book_company as $row){ ?>
               		 <option value="<?php echo $row->id_company ;?>"><?php echo $row->company ;?></option>
                 <?php } ?>
                     </select>
                </div>
              </div>
              <div class="form-group">
                <label for="dept" class="col-sm-3 control-label">Dept : </label>
               <div class= "col-sm-5 col-md-offset-1">
                  
                    <select data-placeholder="Departemen" id="cbodept_edit" name="cbodept_edit" class="myselect" tabindex="2" style="width: 100%;">
		          <option value="<?php echo $this->session->userdata("id_dept") ;?>"><?php echo $this->session->userdata("dept") ;?></option>  
	             <?php foreach($book_dept as $row2){ ?>
               		 <option value="<?php echo $row2->id_dept ;?>"><?php echo $row2->dept ;?></option>
                 <?php } ?>
                     </select>
                </div>
              </div>
              <div class="form-group">
                <label for="roomname" class="col-sm-3 control-label">Room Name : </label>
               <div class= "col-sm-4 col-md-offset-1">
                    <select data-placeholder="Room Name " id="roomname_edit" name="roomname_edit" class="myselect" tabindex="2" style="width: 100%;" disabled="disabled" > 
	             <?php foreach($book_roomname as $row3){ ?>
               		 <option value="<?php echo $row3->id_room_meeting ;?>" readonly><?php echo $row3->room_name ;?></option>
                 <?php } ?>
                     </select>
                </div>
              </div>
              
               <div class="form-group">
					<label for="usersubmision" class="col-sm-3 control-label">User submission : </label>
					<div class= "col-sm-5 col-md-offset-1">
					  <input type="text" name="username_edit" class="form-control" id="username_edit" placeholder="User submission" disabled="disabled">
					</div>
				  </div>
                   <div class="form-group">
					<label for="participan" class="col-sm-3 control-label">Participan : </label>
					<div class= "col-sm-1 col-md-offset-1">
					  <input type="text" name="participan_edit" class="form-control" id="participan_edit" placeholder="1" >
					</div>
				  </div>
                
				 
				  <div class="form-group">
					<label for="color" class="col-sm-3 control-label">Color : </label>
					<div class= "col-sm-5 col-md-offset-1">
					  <select name="color_edit" class="form-control" id="color_edit">
						  <option value="">Choose</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; Dark blue</option>
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquoise</option>
						  <option style="color:#008000;" value="#008000">&#9724; Green</option>						  
						  <option style="color:#FFD700;" value="#FFD700">&#9724; Yellow</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; Orange</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; Red</option>
						  <option style="color:#000;" value="#000">&#9724; Black</option>
						  
						</select>
					</div>
				  </div>
				  <div class="form-group">
					<label for="datebook" class="col-sm-3 control-label">Date Book:</label>
					<div class= "col-sm-5 col-md-offset-1">
					  <input type="text" name="datebook_edit" class="form-control" id="datebook_edit" readonly>
					</div>
				  </div>
                  <div class="form-group">
					<label for="jam start" class="col-sm-3 control-label" style="color:#39F">Time Start Available :</label>
					<div class= "col-sm-5 col-md-offset-1">
					  <input type="text" name="jamstart_edit" class="form-control timepicker" id="jamstart_edit" readonly="readonly" >
					</div>
				  </div>
                  <div class="form-group">
					<label for="jam End" class="col-sm-3 control-label">Time End :</label>
					<div class= "col-sm-5 col-md-offset-1">
					  <input type="text" name="jamend_edit" class="form-control timepicker" id="jamend_edit" readonly="readonly" >
					</div>
				  </div>
                  
                    <div class="form-group">
					<label for="title" class="col-sm-3 control-label">Email CC : </label>
					<div class= "col-sm-7 col-md-offset-1">
					  <input type="text" name="emailcc_book_edd" class="form-control" id="emailcc_book_edd" >
					</div>
                 </div> 
				
				  <div class="form-group">
					<label for="title" class="col-sm-3 control-label">Meeting info : </label>
					<div class= "col-sm-5 col-md-offset-1">
					  <textarea name="meet_edit" class="form-control" id="meet_edit" placeholder="Meeting info"> </textarea>
					</div>
				  </div>
			  
				    <div class="form-group"> 
                    <label for="title" class="col-sm-3 control-label">Delete Action : </label>
						<div class="col-sm-offset-4">
						  <div class="checkbox">
							<label class="text-danger"><input type="checkbox"  name="delete" value="0"> Delete Booking Meeting Room</label>
						  </div>
						</div>
					</div>
				  
				 <input type="hidden" name="id" class="form-control" id="id" >
			 </div>
             
			  <div class="modal-footer">
              <!--<button type="submit" class="btn btn-primary btn btn-app btn-info btn-xs radius-4" id="btnsaveedit" name="btnsaveedit"><i class="ace-icon fa fa-pencil-square-o bigger-160"></i>Save</button> -->
              	
				<button type="button" class="btn btn-app btn-info btn-xs radius-4 btnsaveedit" id="btnsaveedit" name="btnsaveedit"><i class="ace-icon fa fa-pencil-square-o bigger-160"></i>Save</button>
              
				 <button type="submit" class="btn btn-app btn-danger btn-xs radius-4" data-dismiss="modal"><i class="ace-icon fa 	fa-arrow-circle-o-down  bigger-160 refresh"></i>Close</button>
			  </div>
			</form>
			</div>
		  </div>
		</div>

    </div>
    
    <!-- Modal -->
</p>
<div style="position: absolute">
        <div class="modal fade" id="ViewloadinglModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">                                        
                    <div class="modal-body" align="center">
                            <img src="<?php echo base_url('assets/img/loading_spinner.gif') ; ?>" />
                    </div> 
                    
                </div>
            </div>
        </div>
 </div>       
<!-- Modal -->
    
    <?php 
	
    ?>
    <!-- /.container -->

    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-2.1.4.min.js');?>" charset="UTF-8"  ></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script  src="<?php echo base_url('assets/js/jquery-ui.custom.min.js'); ?>"></script>
		<script  src="<?php echo base_url('assets/js/jquery.ui.touch-punch.min.js'); ?>"></script>
		<script  src="<?php echo base_url('assets/js/moment.min.js'); ?>"></script>
		<script  src="<?php echo base_url('assets/js/fullcalendar.min.js'); ?>"></script>
		<script  src="<?php echo base_url('assets/js/bootbox.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-timepicker.min.js'); ?>"></script>
       
	
	<!-- Function timepicker 
	<script type="text/javascript">
	    $('#jamstart_add').timepicker({showMeridian:false,
				maxHours:'18', ////waktu 24 hours
		});//waktu 24 hours
		
		$('#jamend_add').timepicker({showMeridian:false,
			    maxHours:'18', //waktu 24 hours
		}); ////waktu 24 hours
	</script>-->

	<script>
	$(document).ready(function() {
		
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				//right: 'month,basicWeek,basicDay'
			},
			//defaultDate: '2016-01-12',
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			selectable: true,
			selectHelper: true,
			select: function(start,end, id_dept) { //addnew
				//alert(moment(start).format('YYYY-MM-DD'));
				var urlbook = '<?php echo site_url("book_meeting_room/C_book_meeting_room/list_time_book"); ?>';
				var date_pahasing =  moment(start).format('DD-MM-YYYY');
				
				$.ajax({
					type: "POST",
					//dataType:"json",//using jason					
					url: urlbook,		
					data:{date_post:date_pahasing},	
					cache: false,       
					success: function(timebook){           
					   $("#listbookedtime").html(timebook);		  					
					}
				}); 
				
				   $('#ModalAdd #datebook').val(moment(start).format('DD-MM-YYYY'));
				   $("#roomname").val('');
				   $('#ModalAdd').modal('show');
			},
			
			eventRender: function(event, element) {
				element.bind('dblclick', function() { //edit
					if (event.id_dept == <?php echo $this->session->userdata("id_dept") ?>) {	
						$('#ModalEdit #id').val(event.id);
						$('#ModalEdit #cbocomp_edit').val(event.id_company);
						$('#ModalEdit #cbodept_edit').val(event.id_dept);
						$('#ModalEdit #roomname_edit').val(event.id_room);
						$('#ModalEdit #username_edit').val(event.user_submission);
						$('#ModalEdit #datebook_edit').val(event.date_book);
						$('#ModalEdit #jamstart_edit').val(event.jamstart);
						$('#ModalEdit #jamend_edit').val(event.jamend);
						$('#ModalEdit #participan_edit').val(event.participan);
						$('#ModalEdit #emailcc_book_edd').val(event.email_cc_book);
						$('#ModalEdit #meet_edit').val(event.info_meet);
						$('#ModalEdit #color_edit').val(event.color); 
						$('#ModalEdit').modal('show');
					}else{
						alert('not alowed to edit booked other departement ');
					}
					
					
				});
			},
			
			eventDrop: function(event, delta, revertFunc) { // si changement de position

				edit(event);

			},
			eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur

				edit(event);

			},
			events: [
			<?php foreach($book_meet_room as $event): 
			
				$start =  $event['jam_book'];
				$end =  $event['jam_end'];
				
				if( $start == '00:00:00'){
				    $start = $event['date_book'];
				}else{
					$start = $event['date_book']." ".$event['jam_book'];
				}
				
				if( $end == '00:00:00'){
					$end =  $event['date_book'];
				}else{
					$end = $event['date_book']." ".$event['jam_end'] ;
				}
				
			?>
				{
					id: '<?php echo $event['id_book']; ?>',
					title:'<?php echo $event['dept']." ".$event['user_submission'] ?>', //tite nama pada tgl calender
					id_company : '<?php echo $event['id_company']; ?>',
					id_dept : '<?php echo $event['id_dept']; ?>',	
					id_room: '<?php echo $event['id_room_meeting']; ?>',				
					participan : '<?php echo $event['total_participan'] ;?>', 
					email_cc_book:'<?php echo $event['email_cc_book'] ;?>',
					info_meet:'<?php echo $event['info_meet'] ;?>',
					user_submission:'<?php echo $event['user_submission'] ;?>', 
					date_book : '<?php echo date('d-m-Y', strtotime($event['date_book']));?>',
					jam_start :'<?php echo $event['jam_book'] ;?>', 
					jam_end   :'<?php echo $event['jam_end'] ;?>', 
					
					//buat calender---------------------------------------------- 
					   start: '<?php echo $start ; ?>',
					   end  : '<?php echo $end ; ?>',
					//------------------------------------------------
					
					jamstart :'<?php echo date("H:i",strtotime($event['jam_book']));//format24H 5 dig?>',
					jamend   :'<?php echo date("H:i",strtotime($event['jam_end']))//format24H 5 dig ;?>',
					color: '<?php echo $event['color']; ?>',
				},
			<?php endforeach; ?>
			]
			
		});
		
	});

</script>

<script>
//autocomplete jam available pada saaat pilih room meeting
$(document).ready(function() {
	  $("#roomname").change(function(){
		var cboroom_val   = $("#roomname").val();
		var str_date_book = $("#datebook").val();
		var url = '<?php echo site_url("book_meeting_room/C_book_meeting_room/list_time_free"); ?>';
		$.ajax({
			type:'POST',
			url: url,		
			data:{id_room_post:cboroom_val,book_date:str_date_book},	
			cache: false,          
			success: function(msg){            
			    $("#jamstart_add").removeAttr("disabled");
			    $("#jamend_add").removeAttr("disabled");
				$("#jamstart_add").html(msg);
				$("#jamend_add").html(msg);	
				//alert(msg);						
			}
		});
	  });	
});
</script>

<script>
$('.refresh').click(function() {
    location.reload();
});
</script>



