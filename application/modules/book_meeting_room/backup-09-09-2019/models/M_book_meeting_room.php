<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_book_meeting_room extends CI_Model {	
				    
    public $db_tabel = 'qv_room_book_meeting';
	 
    public function tampil_book(){ //tampil table untuk ngececk num_row						     	     		  
	 /*  if ($this->session->userdata("level_access")=="1") {  
		   $status_delete = "1"; //jika 0 terdelete jika 1 aktif		  		 		 				 
		   $this->load->database();		
		   $this->db->select('*');				
		   $this->db->where('status',$status_delete);				
		   $this->db->order_by('date_book','desc');
		   $result = $this->db->get('qv_room_book_meeting');			     		 
	   }else{
		   $status_delete = "1"; //jika 0 terdelete jika 1 aktif		  		 		 				 
		   $this->load->database();		
		   $this->db->where('id_dept',$this->session->userdata('id_dept')) ;			
		   $this->db->where('status',$status_delete);				
		   $this->db->order_by('date_book','desc');
		   $result = $this->db->get('qv_room_book_meeting');		  
	   } */
	       $this->load->database();		
		   $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		   $this->db->select('*');				
		   $this->db->where('status',$status_delete);				
		   $this->db->order_by('date_book','desc');
		   $result = $this->db->get('qv_room_book_meeting');
	   	return $result;
		   
	}	   
	
	public function tampil_date(){
		 $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('date_book',$start_phasing_jquery);		
		 $this->db->where('status',"1");	
		 $this->db->limit(1);
		 $this->db->order_by('id_book','desc');
		 $query_time = $this->db->get('qv_room_book_meeting');
		 $query_time->result_array();	
	}
	
	 	
	
	public function tampil_company(){ //tampil table untuk ngececk num_row						     	     		  
	     
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		  		 		 				 
		 $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('status',$status_delete);				
		 $this->db->order_by('id_company','asc');
		 $result = $this->db->get('tbl_company');			     		 
		 return $result;
	}	    	
	
	public function tampil_dept(){ //tampil table untuk ngececk num_row						     	     		  
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		  		 		 				 
		 $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('status',$status_delete);				
		 $this->db->order_by('id_dept','asc');
		 $result = $this->db->get('tbl_dept');			     		 
		 return $result;
	}	    	
	
	public function tampil_table_roomname(){ //tampil table untuk ngececk num_row						     	     		  
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		  		 		 				 
		 $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('status',$status_delete);				
		 $this->db->order_by('id_room_meeting','asc');
		 $result = $this->db->get('tbl_room_meeting_name');			     		 
		 return $result;
	}	    	
	
 
   	
  public function save_data()
  {			
    $roomname = $this->input->post('roomname');    
	$cbocomp = $this->input->post('cbocomp');  
	$dept = $this->input->post('cbodept');
	$name = $this->input->post('username');  
    $color = $this->input->post('color');  
	$participan = $this->input->post('participan');  
	$datebook = date('Y-m-d', strtotime($this->input->post('datebook')));  			
	$jamstart = $this->input->post('jamstart_add'); 	
	$jamend = $this->input->post('jamend_add'); 	
	$email_cc_bookroom = $this->input->post('emailcc_book');
	$info_meet = $this->input->post('meet');
	$str_status = "1";  	
	
	

				$insert_data = array(
								"id_room_meeting" => $roomname,
								"id_company" => $cbocomp,
								"id_dept" => $dept,
								"user_submission" => $name ,														
								"color" =>$color,					
								"total_participan" =>$participan,		
								"date_book " =>$datebook,	
								"jam_book" =>$jamstart,		
								"jam_end" =>$jamend,
								"email_cc_book" =>$email_cc_bookroom,
								"info_meet"=>$info_meet,	
								"status"=>$str_status			
								
								);
				$this->db->insert('tbl_book_roommeeting',$insert_data);	
				return true;	
		
							
  }		
  
 
  
    public function edit_data()
	{		 	
	    $checkbox_delete = $this->input->post('delete');
		
	    $id_book = $this->input->post('id');   
		$cbocomp = $this->input->post('cbocomp_edit');  
		$dept = $this->input->post('cbodept_edit');
		$roomname = $this->input->post('roomname_edit');  
		$name = $this->input->post('username_edit');  
		$color = $this->input->post('color_edit');  
		$participan = $this->input->post('participan_edit');  
		$info_meet = $this->input->post('meet_edit'); 		
		
		if ($checkbox_delete == "0" ){
			    $str_status = $checkbox_delete;
				$update_data_book = array(
							"status" => $str_status													
						);
						$this->db->where('id_book', $id_book);
						$this->db->update('tbl_book_roommeeting',$update_data_book); 
						return true;	
		}else{
			    $str_status = "1";
				$update_data_book = array(
							//"id_room_meeting" => $roomname,
							"id_company" => $cbocomp,
							//"id_dept" => $dept,
							//"user_submission" => $name ,														
							"color" =>$color,					
							"total_participan" =>$participan,		
							"info_meet"=>$info_meet,
							"status" => $str_status													
						);
						$this->db->where('id_book', $id_book);
					    $this->db->update('tbl_book_roommeeting',$update_data_book); 
						return false;	
		}
			
						
																	
			
	
								
	  }		
	  
	    public function delete_data()
	    {		 		
			$delete = $this->input->post('msg');  		
								
			   if(isset($delete) && trim($delete!=''))
			   {	   				
					for ($i=0; $i < count($delete) ; $i++) {																																																									
						$this->db->where('id',$delete[$i]);
						$this->db->delete('tbl_user'); 
					}		
					return true;		
				}else{
					return false;		
				}								
	  }		
				
		public function search_data() 	// fungsi cari data vendor
		{
		   $cari = $this->input->post('txtcari');
		   $status_del = "1";	   
		   
		   if ($cari == null) // jika textbox cari kosong
		   {
			   redirect('user_form/C_user');  //direct ke url c_vendor/c_vendor
		   }else{	
			   $result = $this->db->query("select * from qv_complite_user where status ='".$status_del."' and name like '".'%'.$cari.'%'."' or username like '".'%'.$cari.'%'."' or email like '".'%'.$cari.'%'."'"); 		   		   	 
			   return $result->result(); 
		   }
		} 		    	
		
		public function reset_data() 	// fungsi cari data vendor
		{
		   $str_iduser = $this->input->post('txtid');
		   $str_txtuser = $this->input->post('txtuser');
		   $status_del = "1";	
		   
		   if ($str_iduser == null) // jika textbox cari kosong
		   {
			   redirect('user_form/C_user');  //direct ke url c_vendor/c_vendor
		   }else{				 
			   $update_pass = array("pass" => md5($str_txtuser));																																										
			   $this->db->where('id',$str_iduser);
			   $this->db->update('tbl_user',$update_pass); 	
			   return true ;
			 
		   }
		} 		    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
		public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}		 
}