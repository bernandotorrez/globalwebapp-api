 

<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>


<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>
      
<?php   	
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>      
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>                        
                <?php  echo "Send Approval Successfully"	?>  
		  </div>		
<?php } ?>

<?php   	
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>                
        <div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>                        
			<?php  echo "Send Approval failed"	?>  
        </div>		      
<?php } ?>	
 
 <div class="page-header btn-light" >
        <h1>
            List Table Date Booking Of Meeting Room 
        </h1>
  </div><!-- /.page-header --> 
  
                
 <?php  echo form_open('book_meeting_room/c_book_meeting_room_table/cath_data_fordatatables',array('id' => 'form-tablebookroom'));  ?> 
 
 
 
     <div class="form-group col-xs-6">
        <div class="col-xs-10">
            <div class="form-inline ">
                <div class="form-group">Date start</div>
                
                <div class="form-group">
                    <div class="input-group date">
                       <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                       </div>
                       
                       <input type="text" class="form-control input-daterange" id="start_date" name="start_date" data-date-format="dd-mm-yyyy">
                    </div> <!-- /.input-group datep -->       
                </div>
               
               <div class="form-group">
                       <label for="From" class="col-xs-1 control-label">To</label>
               </div>
              
               <div class="form-group">
                      <div class="input-group date">
                               <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                               </div>
                                     <input type="text" class="form-control input-daterange" id="end_date" name="end_date" data-date-format="dd-mm-yyyy">
                       </div>  
               </div>
                
            </div>
        </div>
      </div>
   
   
    
 

		<table id="myTable" cellpadding="0" cellspacing="0" width="100%"  class="table table-striped table-bordered table-hover">
	      <thead align="center" class="text-warning" > 
            <tr>  		          
                <th>ID BOOK </th>			
                <th>ROOM NAME</th>			                	
                <th>COMPANY</th>      
                <th>DEPT</th> 
                <th>USER SUBMISSION</th>                
                <th>DATE BOOK</th>
                <th>TIME BOOK</th>
                <th>TIME END</th>		
                <th>MEETNG INFO</th>		
                	
               </tr>
             </thead>    
         </table>
    </table>
     
<br />

     

<!-- Modal -->
        <div class="modal fade" id="ViewDetailModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Purchase</h4>
                    </div>
                    
                    <div class="modal-body">                      
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary btnclose" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal -->  	





<script>
	
//check all--------------------------
$('#checkAll').change(function(){	
		var table = $('#myTable').DataTable();
		var cells = table.cells( ).nodes();
		$( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));			
		
});  

//aktif edit--------------------------------------------------------  
var counterCheckededit = 0;
	$('body').on('change', 'input[type="checkbox"]', function() { 
		   this.checked ? counterCheckededit++ : counterCheckededit--;
		   if($('#checkAll').not(':checked').length){ 
		       counterCheckededit == 1 ? $('#btnedit').prop("disabled", false): $('#btnedit').prop("disabled", true);
		   }
	 });	


//end---------------------------------------------------------------


//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterChecked++ : counterChecked--;
	counterChecked > 0 ? $('#btndel').prop("disabled", false): $('#btndel').prop("disabled", true);

});
//--------------------------------------------------------------------


//aktif button send email--------------------------------------------------------
var counterCheckesendeamail = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckesendeamail++ : counterCheckesendeamail--;
	if($('#checkAll').not(':checked').length){ 
	   counterCheckesendeamail == 1 ? $('#btnsendtab').prop("disabled", false): $('#btnsendtab').prop("disabled", true);
	}
});
//end---------------------------------------------------------------



</script>

<script>
//get data from control to modal popup.
$(function(){
  $(document).on('click','.detail',function(e){
		var req_id = $(this).attr('req_id');  
		var url = '<?php echo site_url("book_meeting_room/C_book_meeting_room_table/get_idtrans_modal"); ?>';
		
		$("#ViewDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_pp:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		}); 			
		
   });	
});
</script>





<script>
//table view----------------------------------------------------------
 $(document).ready(function(){ 
		$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data('no'); //
	
		function fetch_data(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
				"ajax":{
				    url: "<?php echo base_url('book_meeting_room/c_book_meeting_room_table/cath_data_fordatatables') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
					
					
					
					"columnDefs": [ {
						 //"targets": [0,1,2,3,4,5], /* column index */
						 "orderable": false, /* true or false */		
					} ],
				 }
			 });
		  }
		  	  
		  
//proses change date-----------------------------------------------
		$('#start_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);
			}
		});
		
		$('#end_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);		
			}
		});
		
 //end change date----------------------------------------------------------------------	
 
 
 
 //onblur date-------------------------------------------------------------------------
 
 		$('#start_date').blur(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date == '' || end_date == ''){
				$('#myTable').DataTable().destroy();
				  fetch_data('no', start_date, end_date);
			} 
			
		});
 
 
 	
		$('#end_date').blur(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
			
			if(start_date == '' || end_date == ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('no', start_date, end_date);
			} 
		});
 //end onblur-------------------------------------		
		
 	
 }); //end document on ready
 
//--------------------------------------------------------------------------

</script>


