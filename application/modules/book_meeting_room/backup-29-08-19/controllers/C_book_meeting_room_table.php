<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_book_meeting_room_table extends MY_Controller
{
  public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('M_book_meeting_room_table','',TRUE);
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_book_meeting_room_table->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}
	}

  public function index()
  {
		$this->show_table(); //manggil fungsi show_table
  }

  public function show_table()
  {
		
		$data['show_view'] = 'book_meeting_room/V_table_book_meeting_room';
		$this->load->view('dashboard/Template',$data);
	 
  }



function cath_data_fordatatables(){
        
		$str_idcompany = $this->session->userdata("id_company"); 
		$str_iddept = $this->session->userdata("id_dept");
		$str_submission = $this->session->userdata("name");
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
	
		$draw=$_REQUEST['draw'];
		$length=$_REQUEST['length'];
		$start=$_REQUEST['start'];
		$search=$_REQUEST['search']["value"];
		
		//order short column
		$column_order =array("id_book","room_name","company","short","dept","user_submission","date_book","jam_book","jam_end","info_meet");
        //end------------------------------------------

		/*Menghitung total data didalam database*/
		$this->db->select('id_book');
		$this->db->from('qv_room_book_meeting');
		//$this->db->where('id_company', $str_idcompany);
		//$this->db->where('id_dept',$str_iddept);
		//$this->db->where('user_submission',$str_submission );
		$this->db->where('status','1');
		$total = $this->db->count_all_results();
        //end----------------------------------------
	
		$output=array();
		$output['draw']=$draw;
		$output['recordsTotal']=$output['recordsFiltered']=$total;
		$output['data']=array();
		//end----------------------------------------

		/*Jika $search mengandung nilai */
		if($search!=""){
			$this->db->like("room_name",$search);
			$this->db->where("status","1"); 
			$this->db->or_like("company",$search);
			$this->db->where("status","1"); 
			$this->db->or_like("dept",$search);
			$this->db->where("status","1");
			$this->db->or_like("user_submission",$search);
			$this->db->where("status","1");
			$this->db->or_like("date_book",$search);
			$this->db->where("status","1");
			$this->db->or_like("jam_book",$search);
			$this->db->where("status","1"); 
			$this->db->or_like("jam_end",$search);
			$this->db->where("status","1");
			$this->db->or_like("info_meet",$search);
			$this->db->where("status","1");
		  }
		  
		  if($_POST["is_date_search"] == "yes") { // filter range date
		  	  $this->db->where('date_book BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
			  $this->db->group_by("date_book");
			  $this->db->group_by("jam_book");
		  }

			/*Lanjutkan pencarian ke database*/
			$this->db->limit($length,$start);
			$this->db->where("status","1");
		
			
			//fungsi order percolumn datatable
		    if (isset($_REQUEST["order"])){
		          $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
			}else{
				  $this->db->order_by('date_book','DESC');
			}
			$this->db->order_by('date_book','DESC');
			$query = $this->db->get('qv_room_book_meeting');


	    /* menapilkan total record saat pencarian */
		if($search!=""){
			$this->db->like("room_name",$search);
			$this->db->where("status","1"); 
			$this->db->or_like("company",$search);
			$this->db->where("status","1"); 
			$this->db->or_like("dept",$search);
			$this->db->where("status","1");
			$this->db->or_like("user_submission",$search);
			$this->db->where("status","1");
			$this->db->or_like("date_book",$search);
			$this->db->where("status","1");
			$this->db->or_like("jam_book",$search);
			$this->db->where("status","1"); 
			$this->db->or_like("jam_end",$search);
			$this->db->where("status","1");
			$this->db->or_like("info_meet",$search);
			$this->db->where("status","1");
			$jumlahlahrecordcari=$this->db->get('qv_room_book_meeting');
			$output['recordsTotal']=$output['recordsFiltered']=$jumlahlahrecordcari->num_rows();
		 }
		 
		  if($_POST["is_date_search"] == "yes") { // hitung total record filter range date
		  	  $this->db->where('date_book BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
			  $this->db->group_by("date_book");
			  $this->db->group_by("jam_book");
			  $jumlahlahrecordfilterdate=$this->db->get('qv_room_book_meeting');
			  $output['recordsTotal']=$output['recordsFiltered']=$jumlahlahrecordfilterdate->num_rows();
			  
		  }
		  
		foreach ($query->result_array() as $row_tbl) {
		  //Pembedaan Warna pada type purchase.
		   if ($row_tbl['id_company'] == "1") {
		       $company = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>'".$row_tbl['company']."'</span></div>";   
		   }else{
			     if ($row_tbl['id_company'] == "2") {
		     		  $company = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>'".$row_tbl['company']."'t</span></div>";   
		  		 }else{
			   		  if ($row_tbl['id_company'] == "3") {
		     			  $company = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>'".$row_tbl['company']."'</span></div>";   
		  			 }else{
			    			 if ($row_tbl['id_company'] == "4") {
		      					 $company = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>'".$row_tbl['company']."'</span></div>";
		  					 }else{
		       					 $company = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>'".$row_tbl['company']."'</span></div>";
							 };
					 };
				 };   
		     };
//end------------------------------	

//submision---
  $submision = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>".$row_tbl['user_submission']."</span></div>"; 
//end

//date book---
  $strdate_book = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>".date('d-m-Y', strtotime($row_tbl['date_book']))."</span></div>"; 
//end

//submision---
  $jam_start = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>".$row_tbl['jam_book']."</span></div>"; 
  
  //submision---
  $jam_end = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>".$row_tbl['jam_end']."</span></div>"; 
//end
//end
	

		$output['data'][]=array(
									$row_tbl['id_book'],
									$row_tbl['room_name'],
									$company,//$row_tbl['company'],
									$row_tbl['dept'],
									$submision,//$row_tbl['user_submission'],
									$strdate_book,
									$jam_start, //$row_tbl['jam_book'],
									$jam_end,//$row_tbl['jam_end'],
									$row_tbl['info_meet'],
									
							  );
		}
		echo json_encode($output);
	}
	
	public function rangeDates(){
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
	
		$return = $this->M_book_meeting_room_table->rangeDate($start_date,$end_date);
		
		echo json_encode($return);
	}

 
	public function disconnect()
	{
		$ddate = date('d F Y');
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user

		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');

  }

}
