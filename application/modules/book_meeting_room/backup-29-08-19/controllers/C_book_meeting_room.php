<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_book_meeting_room extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_book_meeting_room','C_book_meeting_room',TRUE);	
		$this->load->library('form_validation');	   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->C_book_meeting_room->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}			
	}		  	
		
  public function index()
  {					    																
	$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	    $tampil_table_book_room = $this->C_book_meeting_room->tampil_book()->result_array();
		$tampil_table_company = $this->C_book_meeting_room->tampil_company()->result();
		$tampil_table_dept  = $this->C_book_meeting_room->tampil_dept()->result();	
		$tampil_table_roomname  = $this->C_book_meeting_room->tampil_table_roomname()->result();	
	 			
		//$data['header'] ="REGISTER USER";		
		$data['intstart']="";
		$data['$intend1']="";
		$data['$intend2']="";
		$data['start_phasing_jquery']="";
		$data['query_time']="";
		$data['book_meet_room'] = $tampil_table_book_room;	
		$data['book_company'] = $tampil_table_company;	
		$data['book_dept'] = $tampil_table_dept;
		$data['book_roomname'] = $tampil_table_roomname;										
		$data['intno'] = ""; //variable buat looping no table.														
		$data['show_view'] = 'book_meeting_room/V_book_meeting_room';				
		$this->load->view('dashboard/Template',$data);				
		
  }
 
  public  function do_save()
 {
	$id_book = $this->input->post('id'); 
	   	
	  if ($id_book == "" ){ 	
	  		 if ($this->C_book_meeting_room->save_data()){
				  echo "Insert Success";	//save data
				  $this->kirim_email() ;
			 }else{
				  echo "time alrerady book";
			 }
		
			 
	  }else{
		 	 $this->C_book_meeting_room->edit_data();	//edit data
	  }		
 } 
 
 /* public  function do_update()
  {
	 $this->C_book_meeting_room->edit_data();	
	 redirect('book_meeting_room/C_book_meeting_room');   
  }*/ 
 
 public function do_delete()
 {
	 if ($this->C_book_meeting_room->delete_data())
	{	 	
		$this->session->set_flashdata('pesan_succes','Delete user Successfully');			
		redirect('book_meeting_room/C_book_meeting_room');   
	}else{
		$this->session->set_flashdata('pesan','Delete user Failed');		
		redirect('book_meeting_room/C_book_meeting_room');   
	}
 }
 
  public  function do_chek_time()
  {
	     $phasing_date = date('Y-m-d',strtotime($this->input->post('date_post')));
	     $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('date_book',$phasing_date);		
		 $this->db->where('status',"1");	
		 $this->db->limit(1);
		 $this->db->order_by('jam_end','desc');
		 $query= $this->db->get('qv_room_book_meeting');
		 
		 if($query->num_rows() == 0){
			  $data['phase_jam_start'] = "8:30";
		 }else{
			  foreach ($query->result() as $row_time):								
			     $data['phase_jam_start'] = date("H:i",strtotime($row_time->jam_end)); 								
			  endforeach; 
		 
		 }
		 
		 echo json_encode($data);	 
  } 
    public  function list_time_book()
   {
			 $phasing_date = date('Y-m-d',strtotime($this->input->post('date_post')));
			 $this->load->database();		
			 $this->db->select('*');		
			 $this->db->where('date_book',$phasing_date);		
			 $this->db->where('status',"1");	
			 $this->db->order_by('jam_book','asc');
			 $query= $this->db->get('qv_room_book_meeting');
			 
			 if($query->num_rows() < 1){
					echo '<option  style="color:#F36">'."Time Not Booking yet".'</option>';
			 }else{
				  foreach ($query->result() as $row_time):			
				   echo '<option   class="btn-danger">'.date("H:i",strtotime($row_time->jam_book))." "."To"." ".date("H:i",strtotime($row_time->jam_end))." | ".$row_time->user_submission." | ".$row_time->dept." | ".$row_time->room_name.'</option>';										
				  endforeach; 
			 
			 }
	  } 
	  
	  
	   public  function list_time_free()
       {
			 $phasing_date = date('Y-m-d',strtotime($this->input->post('book_date')));
			 $phasing_id_room = $this->input->post('id_room_post');
			 
			 $this->load->database();		
			 $this->db->select('*');		
			 $this->db->where('date_book',$phasing_date);
			 $this->db->where('id_room_meeting',$phasing_id_room);		
			 $this->db->where('status',"1");	
			 $this->db->order_by('jam_book','asc');
			 $query= $this->db->get('qv_room_book_meeting');
			 
			
				   foreach ($query->result() as $row_time) {
						$begin = new DateTime($row_time->jam_book);
						$end   = new DateTime($row_time->jam_end);
						
						$interval = DateInterval::createFromDateString('30 min');
						$times    = new DatePeriod($begin, $interval, $end);
						
						foreach ($times as $time) {
							//echo '<option  style="color:#06F">'.$time->format('H:i').'</option>';
							$data_time_table[] = $time->format('H:i');
						}
				    }
					  
					$begin_timecurr = new DateTime("09:00");
					$end_timecurr   = new DateTime("18:00");
					
					$interval_curr = DateInterval::createFromDateString('30 min');
					$times_curr    = new DatePeriod($begin_timecurr, $interval_curr, $end_timecurr);
					
					foreach ($times_curr as $time_curr) {
						$data_time_curr[] = $time_curr->format('H:i');
					} 
					
					
					if (empty($data_time_table)) {
						
						foreach ($times_curr as $time_curr) {
							echo '<option  class="btn-info">'.$time_curr->format('H:i').'</option>';
						} 
					
					}else{
						
						$time_free_taken = array_diff($data_time_curr,$data_time_table); //compare array notmatch time 
						foreach($time_free_taken as $value_time){
							echo '<option class="btn-info">'.$value_time.'</option>';
					    }
					}
					
					
					//print_r($time_free_taken);
					//print_r($data_time_table);
					//print_r($data_time_curr);
				  
			
	  } 
	 

 
 
 public function kirim_email(){          
 		   
		   $this->load->library('email');	
		   
		   $config['charset']    = 'utf-8';
           $config['newline']    = "\r\n";
           $config['mailtype'] = 'html'; 
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!"; 
           $config['validation'] = TRUE;      
           $this->email->initialize($config);
           
		  // $sender_email = "helpdesk@eurokars.co.id"; 					   
		   $sender_name =  "Booking_Rooom_Meeting_Notification";	;			
		   
		   $this->email->from($sender_name); 
		   
		   
		   
		    //simpan session alamat email kedalam variable..
			    //$ccuser = $this->session->userdata('email');
			    $strheaduser = $this->session->userdata('email_head');
				$struseremail = $this->session->userdata('email');
				$useradmin = "Lisa.Aprianty@eurokars.co.id";
				$user_pe1 ="Tasya@eurokars.co.id" ;
				$user_Pe2 = "Eti.Widhiari@eurokars.co.id" ;
			//end-----------------------------------------
		  
		   	if( $this->input->post('roomname') == "1") {
			      $roommettingname = "B.O.D Room Meeting" ;
				 // $to_email = $useradmin.",".$user_pe1.",".$user_Pe2.",".$struseremail ; 
			}else{
				 if( $this->input->post('roomname') == "2") {
					  $roommettingname = "Finance Room Meeting";
					  //$to_email = $useradmin.",".$user_Pe2.",".$struseremail  ; 
				 }else{
					 $roommettingname = "Porsche Room Meeting";
					 //$to_email = $useradmin.",".$user_Pe2.",".$struseremail  ; 
				 }
			}           
		  
		   // $to_email = 'brian.yunanda@eurokars.co.id';
		    $to_email = $useradmin.",".$struseremail.",".$strheaduser; 
            $this->email->to($to_email);
           
		   $this->email->subject('Request Book Meeting Room'.$roommettingname);
           $first_name=$this->input->post('fname');
           $email=$this->input->post('email');
           $email_subject = 'Request Booking Meeting Room';   
		   
		             
         
		   $data_email['roomname'] = $roommettingname; 
		   $data_email['cbocomp'] = $this->session->userdata('company');
		   $data_email['dept'] =  $this->session->userdata('dept');
		   $data_email['name'] =  $this->input->post('username');
		   $data_email['participan'] =   $this->input->post('participan');  
		   $data_email['datebook'] =  date('d-m-Y', strtotime($this->input->post('datebook')));  		
		   $data_email['jamstart'] =  $this->input->post('jamstart_add');
		   $data_email['jamend'] =    $this->input->post('jamend_add'); 
		   $data_email['info_meet'] =   $this->input->post('meet');
		   $email_body = $this->load->view('book_meeting_room/V_content_email_book',$data_email,true);
           $this->email->message($email_body); 
           
		   $this->email->send();
								

	 }
 
 
      						 		
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }
  
  
	
}

