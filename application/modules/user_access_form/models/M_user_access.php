<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user_access extends CI_Model
{

	var $table = 'tbl_user_access';
	var $tablenew = 'qv_user_access';
	var $column_order = array(null, null, 'id_user_access','request_date','full_name','id_employee','dept','job_title','reported_to','name'); //set column field database for datatable orderable
	var $column_search = array('id_user_access','request_date','full_name','id_employee','dept','job_title','reported_to','name'); //set column field database for datatable searchable 
	var $order = array('id_user_access' => 'desc'); // default order 
	
    
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	private function _get_datatables_query()
	{
		
		$this->db->from($this->tablenew);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$id_user  = $this->session->userdata('id') ;
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->where('status = 1');
		$this->db->where('id_user',$id_user);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$id_user  = $this->session->userdata('id') ;
		$this->_get_datatables_query();
		$this->db->where('status = 1');
		$this->db->where('id_user',$id_user);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$id_user  = $this->session->userdata('id') ;
		$this->db->from($this->tablenew);
		$this->db->where('status = 1');
		$this->db->where('id_user',$id_user);
		return $this->db->count_all_results();
	}
	
	public function get_all_loan()
		{
			$this->db->from('tbl_hrd_loan');
			$this->db->where('status = 1');
			$query=$this->db->get();
			return $query->result();
		}

	public function get_by_id($id)
		{
			$this->db->from('tbl_user_access');
			$this->db->where('id_user_access',$id);
			$query = $this->db->get();
			return $query->row();
		}
	
	public function get_count_id()
		{
			$this->db->select('max(id) as totaldata');
			$this->db->from('tbl_user');
			$query=$this->db->get();
			return $query->result();
		}
		
	public function get_all_company()
		{
			$this->db->from('tbl_company');
			$this->db->where('status = 1');
			$query=$this->db->get();
			return $query->result();

		}
		
	public function get_all_group()
		{
			$this->db->from('tbl_user_groups');
			$this->db->where('status = 1');
			$query=$this->db->get();
			return $query->result();

		}
		
	public function get_all_dept()
		{
			$this->db->from('tbl_dept');
			$this->db->where('status = 1');
			$query=$this->db->get();
			return $query->result();

		}
	
	public function get_all_purchase()
		{
			$this->db->from('tbl_type_purchase');
			$query=$this->db->get();
			return $query->result();

		}
	
	public function get_child($id)
		{
        $q = $this->db->query("select * from tbl_child_menu left join tbl_parent_menu on tbl_child_menu.id_parent=tbl_parent_menu.id_parent where tbl_child_menu.id_parent='$id'");
        return $q;
		}
	
	public function get_subchild($id)
		{
        $q = $this->db->query("select * from tbl_sub_child_menu left join tbl_child_menu on tbl_sub_child_menu.id_child=tbl_child_menu.id_child where tbl_sub_child_menu.id_child='$id'");
        return $q;
		}
		
	public function add_user_access($data)
		{
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}

	public function update_user($where, $data)
		{
			$this->db->update($this->table, $data, $where);
			return $this->db->affected_rows();
		}

	public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}


}
