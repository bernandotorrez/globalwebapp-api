<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<style>
 .outer {
    width: 210px;
    color: black;
    border: 2px solid Gainsboro;
    padding: 5px;
 }
 hr{
	width: 100%;
	border-top: 2px solid lightgray;;
  }
	table { margin:10px auto; border-collapse:collapse; border:1px solid gray; }
	td,th { border:1px solid gray; text-align:left; padding:7px;}
	td.opt1 { text-align:center; vertical-align:middle; }
	td.opt2 { text-align:right; }
</style> 


									<div>
									<a href="<?php echo site_url('user_access_form/c_user_access/view_add_user'); ?>"><button class="btn bg-purple btn-flat"><i class="fa fa-plus-circle"></i> Add Data</button></a>
									<button onclick="delete_user()" id="deleteTriger" name="deleteTriger" class="btn bg-maroon btn-flat" disabled><i class="fa fa-trash"></i> Delete Data</button>
									<button onclick="edit_user()" id="editTriger"  name="editTriger"class="btn bg-maroon btn-flat" disabled><i class="fa fa-edit"></i> Edit Data</button>
									</div>
                                    <br/>
									<div class="row">
									<div class="col-xs-12" style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">                               
	                                <table  class="table table-striped table-bordered table-hover" id="myTable" >
	                                    <thead class="text-warning">
											<th width="5%" style="text-align:center">
												<label class="pos-rel">
                                                    <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
                                                    <span class="lbl"></span>
                                                </label>
                                            </th>
											<th>No</th>
	                                        <th>Id</th>
											<th>Req. Date</th>
											<th>Name</th>
											<th>NIK</th>
											<th>Department</th>
											<th>Position</th>
											<th>Reported To</th>
											<th>Submitted</th>
											<th>Detail</th>
	                                    </thead>
                                     </table> 
      </div>                               
</div>      
  
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-80p">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">×</span>
                       <span class="sr-only">Close</span>
                </button>
                 <h4 class="modal-title" id="myModalLabel">
                    User Access Form
                </h4>
            </div>
	  <div class="modal-body form" style="padding-top:10px">       
		 <form class="form-horizontal" id="form">
		<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
			  <div class="form-group">
                  <label for="id" class="col-sm-3 control-label" style="padding-right:10px">Id User</label>

                  <div class="col-sm-8">
                    <input type="text" readonly class="form-control" name="id" id="id" placeholder="">
                  </div>
                </div>
               <div class="form-group">
                  <label for="request_date" class="col-sm-3 control-label" style="padding-right:10px">Request Date</label>

                  <div class="col-sm-8">
                    <input name="request_date" id="request_date" placeholder="1" class="form-control" type="text" readonly value="<?php echo date('l, d-m-Y')?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="type" class="col-sm-3 control-label" style="padding-right:10px">Request Type</label>

                  <div class="col-sm-3 outer" id="type">
                    <input type="checkbox" name="type[]" id="type" value="1">New User<br>
					<input type="checkbox" name="type[]" id="type" value="2">Modification<br>
					<input type="checkbox" name="type[]" id="type" value="3">Internal Transfer<br>
					<input type="checkbox" name="type[]" id="type" value="4">Retention<br>
					<input type="checkbox" name="type[]" id="type" value="5">Termination<br>
                  </div>
                </div>
				<div class="form-group">
				  <div class="col-sm-8">
                    <label for="date" class="col-sm-7 control-label" style="padding-right:10px">Until Date</label>

                  <div class="input-group date col-sm-5">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="datepicker" id="datepicker">
                </div>
                  </div>
                </div>
              </div>
		</div>
		</div>
		<div class="col-xs-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
              <div class="box-body">
                  <div class="form-group">
                  <label for="target" class="col-sm-3 control-label" style="padding-right:10px">Employee Type</label>

                   <div class="col-sm-3 outer" id="type">
						<input type="checkbox" name="employee[]" id="employee" value="1">EMI Employee<br>
						<input type="checkbox" name="employee[]" id="employee" onclick="ShowHideEmployee(this)" value="2">Temporary Employee<br>
					</div>
                </div>
				 <div class="form-group" id="temporary_name">
                  <label for="temporary_name" class="col-sm-3 control-label" style="padding-right:10px">Temporary Name</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="temporary_name" id="temporary_name" placeholder="Brian">
                  </div>
                </div>
				<div class="form-group" id="sponsor_by" >
                  <label for="sponsor_by" class="col-sm-3 control-label" style="padding-right:10px">Sponsor By</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="sponsor_by" id="sponsor_by" placeholder="Brian">
                  </div>
                </div>
              </div>
          </div>
		</div>
		<div class="col-xs-9">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="additional_reason" class="col-sm-2 control-label" style="padding-right:10px">Additional Reason</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" rows="3" name="additional_reason" id="additional_reason" placeholder=""></textarea>
                  </div>
                </div>				
              </div>
		</div>
		</div>
		<hr>
		<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="id_employee" class="col-sm-3 control-label" style="padding-right:10px">ID Employee</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="id_employee" id="id_employee" placeholder="">
                  </div>
                </div>
				<div class="form-group">
                  <label for="full_name" class="col-sm-3 control-label" style="padding-right:10px">Full Name</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="full_name" id="full_name" placeholder="">
                  </div>
                </div>	
				<div class="form-group">
                  <label for="job_title" class="col-sm-3 control-label" style="padding-right:10px">Job Title</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="job_title" id="job_title" placeholder="">
                  </div>
                </div>
              </div>
		</div>
		</div>
			<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
				<div class="form-group">
                  <label for="department" class="col-sm-3 control-label" style="padding-right:10px">Department</label>

					<div class="col-sm-8">
					   <select class="form-control" name="department" id="department">
					   <option  value="">Choose Department</option>
					   <?php
						$sqlpo	  = "select * from tbl_dept where status=1";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo 	  = $qrypo->result();					
						foreach($adapo as $aaa) { ?>
						<option value="<?php echo $aaa->id_dept;?>"><?php echo $aaa->dept;?>
						</option>
						<?php } ?>			
						</select>    
					</div>
                </div>
				<div class="form-group">
                  <label for="reported_to" class="col-sm-3 control-label" style="padding-right:10px">Reported To</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="reported_to" id="reported_to" placeholder="">
                  </div>
                </div>
              </div>
		</div>
		</div>
		<hr>
		<div class="col-xs-12">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <table>
				  <tr>
					<th width="25px"><center>No</center></th>
					<th width="500px"><center>Application Needs</center></th>
					<th width="110px"><center>Please Thick</center></th>
				  </tr>
				  <tr>
					<td class="opt1">1</td>
					<td>
					 User ID
					  <div class="form-group" id="name_userid">
						  <label for="name_userid" class="col-sm-3 control-label" style="padding-right:10px">Name UserId</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="name_userid" id="name_userid" placeholder="">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="userid" name="userid" onclick="ShowHideUser(this)" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">2</td>
					<td>
					 Email Account
					</td>
					<td class="opt1">
					  <input type="checkbox" id="email_account" name="email_account" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">3</td>
					<td>
					  Shared Drives
					  <div class="form-group" id="detail_drive">
						  <label for="detail_drive" class="col-sm-3 control-label" style="padding-right:10px">Detail Drive</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_drive" id="detail_drive" placeholder="">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="shared_drives" name="shared_drives" onclick="ShowHideDrives(this)" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">4</td>
					<td>
					  Telephone
					  <div class="form-group" id="nmr_ext">
						  <label for="nmr_ext" class="col-sm-3 control-label" style="padding-right:10px">No. EXT.</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="nmr_ext" id="nmr_ext" placeholder="1008">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="telephone" name="telephone" onclick="ShowHideTelephone(this)" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">5</td>
					<td>
					  Intranet ID
					  <div class="form-group" id="detail_intranetid" >
						  <label for="detail_intranetid" class="col-sm-3 control-label" style="padding-right:10px">Detail Intranet</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_intranetid" id="detail_intranetid" placeholder="">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="intranetid" name="intranetid" onclick="ShowHideIntranet(this)" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">6</td>
					<td>
					  WRS ID
					  <div class="form-group" id="detail_wrs">
						  <label for="detail_wrs" class="col-sm-3 control-label" style="padding-right:10px">Detail WRS</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_wrs" id="detail_wrs" placeholder="">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="wrsid" name="wrsid" onclick="ShowHideWrs(this)" value="TRUE">
					</td>
				  </tr>
				   <tr>
					<td class="opt1">7</td>
					<td>
					  Pentana ID
					  <div class="form-group" id="detail_pentana" >
						  <label for="detail_pentana" class="col-sm-3 control-label" style="padding-right:10px">Detail Pentana</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_pentana" id="detail_pentana" placeholder="Mazda">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="mds_pentana" name="mds_pentana" onclick="ShowHidePentana(this)" value="TRUE">
					</td>
				  </tr>
				</table>
                </div>				
              </div>
		</div>
		</div>
		</form>
		</div>
			<div class="modal-header">
            <button type="button" id="btnSave" name="btnSave" onclick="save()" class="btn btn-primary col-sm-3">Save</button>
            <button type="button" class="btn btn-danger col-sm-3" data-dismiss="modal">Cancel</button>
			</div>
		
		</div> <!-- End modal body div -->
      </div> <!-- End modal content div -->
</div>
 <script type="text/javascript">
	
	
    function ShowHideEmployee(employee2) {
        var temporary_name = document.getElementById("temporary_name");
        temporary_name.style.display = employee2.checked ? "block" : "none";
    }
	
	function ShowHideUser(userid) {
        var name_userid = document.getElementById("name_userid");
        name_userid.style.display = userid.checked ? "block" : "none";
    }
	
	function ShowHideDrives(shared_drives) {
        var detail_drive = document.getElementById("detail_drive");
        detail_drive.style.display = shared_drives.checked ? "block" : "none";
    }
	
	function ShowHideTelephone(telephone) {
        var nmr_ext = document.getElementById("nmr_ext");
        nmr_ext.style.display = telephone.checked ? "block" : "none";
    }
	
	function ShowHideIntranet(intranetid) {
        var detail_intranetid = document.getElementById("detail_intranetid");
        detail_intranetid.style.display = intranetid.checked ? "block" : "none";
    }
	
	function ShowHideWrs(wrsid) {
        var detail_wrs = document.getElementById("detail_wrs");
        detail_wrs.style.display = wrsid.checked ? "block" : "none";
    }
	
	function ShowHidePentana(mds_pentana) {
        var detail_pentana = document.getElementById("detail_pentana");
        detail_pentana.style.display = mds_pentana.checked ? "block" : "none";
    }
	</script>	
<script>
$('.selectall').click(function() {
    if ($(this).is(':checked')) {
        $('div input').attr('checked', true);
    } else {
        $('div input').attr('checked', false);
    }
});
</script>
<script>


//$('#myTable').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
var tableUsers = $('#myTable').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		"dom": 'lfBrtip',
		"buttons": [
            {
		   "extend":    'copy',
		   "text":      '<i class="fa fa-files-o"></i> Copy',
		   "titleAttr": 'Copy'
		   },
		   {
		   "extend":    'print',
		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
		   "titleAttr": 'Print',
		   "orientation": 'landscape',
           "pageSize": 'LEGAL'
		   },
		   {
		   "extend":    'excel',
		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
		   "titleAttr": 'Excel'
		   },
		   {
		   "extend":    'pdf',
		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
		   "titleAttr": 'PDF',
		   "orientation": 'landscape',
           "pageSize": 'LEGAL'
		   }
        ],
		"autoWidth" : false,
		"scrollY" : '250',
		"scrollX" : true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('user_access_form/c_user_access/ajax_list') ?>",
            "type": "POST"
        },
		'columnDefs': [
		{
        'targets': [0],
		'orderable': false,
		}
   ],

      'order': [[2, 'desc']],
        });
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>


<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>

<script>
        	$(document).ready(function(){
	            $("#id_company").change(function (){
	                var url = "<?php echo site_url('user_access_form/c_user_access/pos_branch');?>/"+$(this).val();
	                $('#branch_id').load(url);
	                return false;
	            })
	        });
    	</script>

	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
	
	function edit_user(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('user_access_form/c_user_access/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {
			var aaa = data.request_type.split(",");
			$.each(aaa, function(i, item) {
			if (item == "1") {
			$("#type[value='1']" ).prop("checked", true);
			} 
			if (item == "2") {
			$("#type[value='2']").prop("checked", true);
			} 
			if (item == "3") {
			$("#type[value='3']").prop("checked", true);
			} 
			if (item == "4") {
			$("#type[value='4']").prop("checked", true);
			} 
			if (item == "5") {
			$("#type[value='5']").prop("checked", true);
			} 
			
			});
			var bbb = data.employee_type;
			if (bbb == "1") {
			$("#employee[value='1']" ).prop("checked", true);
			} 
			if (bbb == "2") {
			$("#employee[value='2']").prop("checked", true);
			} 
			
			var ccc = data.userid; 
			if (ccc == "1") {
			$("#userid[value='TRUE']" ).prop("checked", true);
			} 
			
			var ddd = data.email_account;
			if (ddd == "1") {
			$("#email_account[value='TRUE']" ).prop("checked", true);
			} 
			
			var eee = data.shared_drives;
			if (eee == "1") {
			$("#shared_drives[value='TRUE']" ).prop("checked", true);
			} 
			
			var fff = data.telephone;
			if (fff == "1") {
			$("#telephone[value='TRUE']" ).prop("checked", true);
			} 
			
			var ggg = data.intranetid;
			if (ggg == "1") {
			$("#intranetid[value='TRUE']" ).prop("checked", true);
			} 
			
			var hhh = data.wrsid;
			if (hhh == "1") {
			$("#wrsid[value='TRUE']" ).prop("checked", true);
			} 
			
			var iii = data.mds_pentana;
			if (iii == "1") {
			$("#mds_pentana[value='TRUE']" ).prop("checked", true);
			} 
			
			$('[name="id"]').val(data.id_user_access);
			$('[name="datepicker"]').val(data.until_date);
            $('[name="additional_reason"]').val(data.additional_reason);
			$('[name="temporary_name"]').val(data.temporary_name);
			$('[name="sponsor_by"]').val(data.sponsor_by);
			$('[name="id_employee"]').val(data.id_employee);
			$('[name="full_name"]').val(data.full_name);
			$('[name="job_title"]').val(data.job_title);
			$('[name="department"]').val(data.department);
			$('[name="reported_to"]').val(data.reported_to);
			$('[name="name_userid"]').val(data.name_userid);
			$('[name="detail_drive"]').val(data.detail_drive);
			$('[name="nmr_ext"]').val(data.nmr_ext);
			$('[name="detail_intranetid"]').val(data.detail_intranetid);
			$('[name="detail_wrs"]').val(data.detail_wrs);
			$('[name="detail_pentana"]').val(data.detail_pentana);
			$('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit User Access'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

     function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('user_access_form/c_user_access/add_user')?>";
      }
      else
      {
		   url = "<?php echo site_url('user_access_form/c_user_access/update_user')?>";
      }
		  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }

     function delete_user()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('user_access_form/c_user_access/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
			
		

      
    }
	
  </script>

  

</html>
