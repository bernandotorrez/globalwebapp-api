job_title<!DOCTYPE html>
<?php
date_default_timezone_set("Asia/Bangkok");
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<style>
  hr{
	width: 100%;
	border-top: 2px solid lightgray;;
  }
	table { margin:10px auto; border-collapse:collapse; border:1px solid gray; }
	td,th { border:1px solid gray; text-align:left; padding:7px;}
	td.opt1 { text-align:center; vertical-align:middle; }
	td.opt2 { text-align:right; }
	 
</style> 
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mazda | Loan Form</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker3.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-timepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/daterangepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-colorpicker.min.css') ?>" />
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">


        <!-- Main content -->
        <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <img src="../../assets/images/logo/mazda.png" alt="Mazda"> PT. Eurokars Motor Indonesia
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Address
              <address>
                <strong>Porsche Centre Jakarta</strong><br>
                Jl. Sultan Iskandar Muda, No. 15<br>
                Arteri Pondok Indah, Jakarta Selatan<br>
                Phone	: (021) 2793 2838<br>
                Fax		: (021) 2793 2838
              </address>
            </div><!-- /.col -->
          </div><!-- /.row -->
		<div class="row" id="modal_form">
		<h3 class="box-title" style="font-weight:bold;"><center>USER ACCESS FORM</center></h3></br>
		 <form class="form-horizontal" id="form">
		<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
               <div class="form-group">
                  <label for="date" class="col-sm-3 control-label" style="padding-right:10px">Request Date</label>

                  <div class="col-sm-8">
                    <input name="request_date" id="request_date" placeholder="1" class="form-control" type="text" readonly value="<?php echo date('l, d-m-Y')?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="request_type" class="col-sm-3 control-label" style="padding-right:10px">Request Type</label>
				   <div class="col-sm-7 outer" id="checkboxlist">
					  <div class="col-sm-4">
						<label><input type="checkbox" name="type[]" id="type" value="1">New User</label>
					  </div>
					  <div class="col-sm-4">
						<label><input type="checkbox" name="type[]" id="type" value="2">Modification</label>
					  </div>
					  <div class="col-sm-4">
						<label><input type="checkbox" name="type[]" id="type" value="3">Internal Transfer</label>
					  </div>
					  <div class="col-sm-4">
						<label><input type="checkbox" name="type[]" id="type" value="4">Retention</label>
					  </div>
					  <div class="col-sm-4">
						<label><input type="checkbox" name="type[]" id="type" value="5">Termination</label>
					  </div>
					</div>
				</div>
				<div class="form-group">
				  <div class="col-sm-8">
                    <label for="description" class="col-sm-7 control-label" style="padding-right:10px">Until Date</label>

                  <div class="input-group date col-sm-5">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="datepicker" id="datepicker">
                </div>
                  </div>
                </div>
              </div>
		</div>
		</div>
		<div class="col-xs-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
              <div class="box-body">
                     <div class="form-group">
                  <label for="target" class="col-sm-3 control-label" style="padding-right:10px">Employee Type</label>

                   <div class="col-sm-7 outer" id="checkboxlist">
					  <div class="col-sm-5">
						<label><input type="checkbox" name="employee[]" id="employee" value="1">EMI Employee</label>
						<label><input type="checkbox" name="employee[]" id="employee2" onclick="ShowHideEmployee(this)" value="2">Temporary Employee</label>
					  </div>
					</div>
                </div>
				 <div class="form-group" id="temporary_name" style="display: none">
                  <label for="temporary_name" class="col-sm-3 control-label" style="padding-right:10px">Temporary Name</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="temporary_name" id="temporary_name" placeholder="Brian">
                  </div>
                </div>
				<div class="form-group" id="sponsor_by" >
                  <label for="sponsor_by" class="col-sm-3 control-label" style="padding-right:10px">Sponsor By</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="sponsor_by" id="sponsor_by" placeholder="Brian">
                  </div>
                </div>
              </div>
          </div>
		</div>
		<div class="col-xs-9">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="additional_reason" class="col-sm-2 control-label" style="padding-right:10px">Additional Reason</label>

                  <div class="col-sm-8">
                    <textarea class="form-control" rows="3" name="additional_reason" id="additional_reason" placeholder=""></textarea>
                  </div>
                </div>				
              </div>
		</div>
		</div>
		<hr>
		<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="id_employee" class="col-sm-3 control-label" style="padding-right:10px">ID Employee</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="id_employee" id="id_employee" placeholder="">
                  </div>
                </div>
				<div class="form-group">
                  <label for="full_name" class="col-sm-3 control-label" style="padding-right:10px">Full Name</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="full_name" id="full_name" placeholder="">
                  </div>
                </div>	
				<div class="form-group">
                  <label for="job_title" class="col-sm-3 control-label" style="padding-right:10px">Job Title</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="job_title" id="job_title" placeholder="">
                  </div>
                </div>
              </div>
		</div>
		</div>
			<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
				<div class="form-group">
                  <label for="department" class="col-sm-3 control-label" style="padding-right:10px">Department</label>

					<div class="col-sm-8">
					   <select class="form-control" name="department" id="department">
					   <option  value="">Choose Department</option>
					   <?php
						$sqlpo	  = "select * from tbl_dept where status=1";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo 	  = $qrypo->result();					
						foreach($adapo as $aaa) { ?>
						<option value="<?php echo $aaa->id_dept;?>"><?php echo $aaa->dept;?>
						</option>
						<?php } ?>			
						</select>    
					</div>
                </div>
				<div class="form-group">
                  <label for="reported_to" class="col-sm-3 control-label" style="padding-right:10px">Reported To</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="reported_to" id="reported_to" placeholder="">
                  </div>
                </div>
              </div>
		</div>
		</div>
		<hr>
		<div class="col-xs-12">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <table>
				  <tr>
					<th width="25px"><center>No</center></th>
					<th width="500px"><center>Application Needs</center></th>
					<th width="110px"><center>Please Thick</center></th>
				  </tr>
				  <tr>
					<td class="opt1">1</td>
					<td>
					 User ID
					  <div class="form-group" id="name_userid" style="display: none">
						  <label for="name_userid" class="col-sm-3 control-label" style="padding-right:10px">Name UserId</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="name_userid" id="name_userid" placeholder="">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="userid" name="userid" onclick="ShowHideUser(this)" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">2</td>
					<td>
					 Email Account
					</td>
					<td class="opt1">
					  <input type="checkbox" id="email_account" name="email_account" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">3</td>
					<td>
					  Shared Drives
					  <div class="form-group" id="detail_drive" style="display: none">
						  <label for="detail_drive" class="col-sm-3 control-label" style="padding-right:10px">Detail Drive</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_drive" id="detail_drive" placeholder="">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="shared_drives" name="shared_drives" onclick="ShowHideDrives(this)" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">4</td>
					<td>
					  Telephone
					  <div class="form-group" id="nmr_ext" style="display: none">
						  <label for="nmr_ext" class="col-sm-3 control-label" style="padding-right:10px">No. EXT.</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="nmr_ext" id="nmr_ext" placeholder="1008">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="telephone" name="telephone" onclick="ShowHideTelephone(this)" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">5</td>
					<td>
					  Intranet ID
					  <div class="form-group" id="detail_intranetid" style="display: none">
						  <label for="detail_intranetid" class="col-sm-3 control-label" style="padding-right:10px">Detail Intranet</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_intranetid" id="detail_intranetid" placeholder="">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="intranetid" name="intranetid" onclick="ShowHideIntranet(this)" value="TRUE">
					</td>
				  </tr>
				  <tr>
					<td class="opt1">6</td>
					<td>
					  WRS ID
					  <div class="form-group" id="detail_wrs" style="display: none">
						  <label for="detail_wrs" class="col-sm-3 control-label" style="padding-right:10px">Detail WRS</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_wrs" id="detail_wrs" placeholder="">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="wrsid" name="wrsid" onclick="ShowHideWrs(this)" value="TRUE">
					</td>
				  </tr>
				   <tr>
					<td class="opt1">7</td>
					<td>
					  Pentana ID
					  <div class="form-group" id="detail_pentana" style="display: none">
						  <label for="detail_pentana" class="col-sm-3 control-label" style="padding-right:10px">Detail Pentana</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_pentana" id="detail_pentana" placeholder="Mazda">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <input type="checkbox" id="mds_pentana" name="mds_pentana" onclick="ShowHidePentana(this)" value="TRUE">
					</td>
				  </tr>
				</table>
                </div>				
              </div>
		</div>
		</div>
		</form>
		</div>

          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
              <center><button class="btn btn-success" onclick="add_user()"><i class="fa fa-save"></i> Submit</button></center>
             </div>
          </div>
        </section><!-- /.content -->
        <div class="clearfix"></div>
      </div><!-- /.content-wrapper -->
      
	 <script type="text/javascript">
            $(function () {
                $('#datepicker').datepicker();
                $('#datepicker1').datetimepicker();
            });
     </script>
    <!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
  </body>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
   <script type="text/javascript">
    function ShowHideEmployee(employee2) {
        var temporary_name = document.getElementById("temporary_name");
        temporary_name.style.display = employee2.checked ? "block" : "none";
    }
	
	function ShowHideUser(userid) {
        var name_userid = document.getElementById("name_userid");
        name_userid.style.display = userid.checked ? "block" : "none";
    }
	
	function ShowHideDrives(shared_drives) {
        var detail_drive = document.getElementById("detail_drive");
        detail_drive.style.display = shared_drives.checked ? "block" : "none";
    }
	
	function ShowHideTelephone(telephone) {
        var nmr_ext = document.getElementById("nmr_ext");
        nmr_ext.style.display = telephone.checked ? "block" : "none";
    }
	
	function ShowHideIntranet(intranetid) {
        var detail_intranetid = document.getElementById("detail_intranetid");
        detail_intranetid.style.display = intranetid.checked ? "block" : "none";
    }
	
	function ShowHideWrs(wrsid) {
        var detail_wrs = document.getElementById("detail_wrs");
        detail_wrs.style.display = wrsid.checked ? "block" : "none";
    }
	
	function ShowHidePentana(mds_pentana) {
        var detail_pentana = document.getElementById("detail_pentana");
        detail_pentana.style.display = mds_pentana.checked ? "block" : "none";
    }
	</script>	
  
  <script type="text/javascript">	

	var save_method; //for save method string
    var table;
		
    function add_user()
    {
     var url;
	 url = "<?php echo site_url('user_access_form/C_user_access/add_user')?>";
	 urlbef = "<?php echo site_url('user_access_form/C_user_access')?>";
	 var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR) 
            {
				if(data=='Insert'){
				window.location.href = urlbef;
			   }else {
			   alert (data);
			   }
			   
            },
        });
    }

    function edit_loan()
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
			
      //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('bank_form/C_bank/ajax_edit/')?>/" + eee,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_bank"]').val(data.id_bank);
            $('[name="name_bank"]').val(data.name_bank);
            
            $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Bank'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('hrd_loan_form/C_loan/add_loan')?>";
      }
      else
      {
		   url = "<?php echo site_url('bank_form/C_bank/update_bank')?>";
      }
		  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR) 
            {
				if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
			   
            },
        });
    }
  </script>
</html>
