<!DOCTYPE html>
<?php
date_default_timezone_set("Asia/Bangkok");
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<?php
	$id = $_GET['id'];
	
	// Perintah untuk menampilkan data
$queri="select * from qv_user_access where id_user_access='".$id."'" ;  //menampikan SEMUA data dari tabel siswa

$hasil=$this->db->query($queri);    //fungsi untuk SQL
$ada = $hasil->result();
// perintah untuk membaca dan mengambil data dalam bentuk array




?>
<Style type='text/css' media='print'>
label { 
  text-align: right; 
  float: left; 
} 
.test {
	text-align : left;
	width : 100px;
}
input {
    border: none;
    background: transparent;
}
a:link:after, a:visited:after {  
      display: none;
      content: "";    
    }
#printSize {width : 720px}
#checkboxlist { 
  text-align: left;
	float : left;
} 
#printLink {display : none}

</Style>
<style>
  hr{
	width: 100%;
	border-top: 2px solid lightgray;;
  }
	table { margin:10px auto; border-collapse:collapse; border:1px solid gray; }
	td,th { border:1px solid gray; text-align:left; padding:7px;}
	td.opt1 { text-align:center; vertical-align:middle; }
	td.opt2 { text-align:right; }
	 
</style> 
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Mazda | Loan Form</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datepicker3.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-timepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/daterangepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-colorpicker.min.css') ?>" />
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">


        <!-- Main content -->
        <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <img src="../../assets/images/logo/mazda.png" alt="Mazda"> PT. Eurokars Motor Indonesia
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              Address
              <address>
                <strong>Porsche Centre Jakarta</strong><br>
                Jl. Sultan Iskandar Muda, No. 15<br>
                Arteri Pondok Indah, Jakarta Selatan<br>
                Phone	: (021) 2793 2838<br>
                Fax		: (021) 2793 2838
              </address>
            </div><!-- /.col -->
          </div><!-- /.row -->
		<center><div class="row" id="printSize">
		<h3 class="box-title" style="font-weight:bold;"><center>USER ACCESS FORM</center></h3></br>
		 <form class="form-horizontal" id="form">
		<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
               <div class="form-group">
                  <label for="date" class="col-sm-3 control-label" style="padding-right:10px">Request Date</label>

                  <div class="col-sm-8">
                    <input name="request_date" id="request_date" placeholder="1" class="form-control" type="text" readonly value="<?php echo $ada[0]->request_date ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="request_type" class="col-sm-3 control-label" style="padding-right:10px">Request Type</label>
				   <div class="col-sm-7 outer" id="checkboxlist" style="text-align:left">
					<?php
					  	$dataarraycekbox = array(1=>'New User','Modification','Internal Transfer','Retention','Termination');
						$cek = "";
						$nb = 1;
						$value = explode(",",$ada[0]->request_type);
						error_reporting(0);
						foreach($value as $kalue){
							$cekcekcek[$kalue] = "checked='true'";
						}
						foreach($dataarraycekbox as $keyarray => $valarray){
							$no = $nb++;
							echo '<input '.$cekcekcek[$keyarray].' type="checkbox" name="type[]" id="type" value="'.$no.'">'.$valarray.'<br>';
						}
					?>
					</div>
				</div>
				<div class="form-group">
				  <div class="col-sm-8">
                    <label for="description" class="col-sm-7 control-label" style="padding-right:10px">Until Date</label>

                  <div class="input-group date col-sm-5">
                  <input name="until_date" id="until_date"  class="form-control" type="text" readonly value="<?php echo $ada[0]->until_date ?>">
                </div>
                  </div>
                </div>
              </div>
		</div>
		</div>
		<div class="col-xs-6">
          <!-- Horizontal Form -->
          <div class="box box-info">
              <div class="box-body">
                     <div class="form-group">
                  <label for="target" class="col-sm-3 control-label" style="padding-right:10px">Employee Type</label>

                   <div class="col-sm-7 outer" id="checkboxlist" style="text-align:left">
					  <?php
					  	$dataarraycekbox1 = array(1=>'EMI Employee','Temporary Employee');
						$cek1 = "";
						$nb1 = 1;
						$value1 = explode(",",$ada[0]->employee_type);
						error_reporting(0);
						foreach($value1 as $kalue1){
							$cekcekcek1[$kalue1] = "checked='true'";
						}
						foreach($dataarraycekbox1 as $keyarray1 => $valarray1){
							$no1 = $nb1++;
							echo '<input '.$cekcekcek1[$keyarray1].' type="checkbox" name="employee[]" id="employee" value="'.$no1.'">'.$valarray1.'<br>';
						}
					?>
					</div>
                </div>
				 <div class="form-group" id="temporary_name" >
                  <label for="temporary_name" class="col-sm-3 control-label" style="padding-right:10px">Temporary Name</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="temporary_name" id="temporary_name" readonly value="<?php echo $ada[0]->temporary_name ?>">
                  </div>
                </div>
				<div class="form-group" id="sponsor_by" >
                  <label for="sponsor_by" class="col-sm-3 control-label" style="padding-right:10px">Sponsor By</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="sponsor_by" id="sponsor_by" readonly value="<?php echo $ada[0]->sponsor_by ?>">
                  </div>
                </div>
              </div>
          </div>
		</div>
		<div class="col-xs-9">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="additional_reason" class="col-sm-2 control-label" style="padding-right:10px">Additional Reason</label>

                  <div class="col-sm-8">
                    <input class="form-control" rows="3" name="additional_reason" id="additional_reason" readonly value="<?php echo $ada[0]->additional_reason ?>">
                  </div>
                </div>				
              </div>
		</div>
		</div>
		<hr>
		<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <label for="id_employee" class="col-sm-3 control-label" style="padding-right:10px">ID Employee</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="id_employee" id="id_employee" readonly value="<?php echo $ada[0]->id_employee ?>">
                  </div>
                </div>
				<div class="form-group">
                  <label for="full_name" class="col-sm-3 control-label" style="padding-right:10px">Full Name</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="full_name" id="full_name" readonly value="<?php echo $ada[0]->full_name ?>">
                  </div>
                </div>	
				<div class="form-group">
                  <label for="job_title" class="col-sm-3 control-label" style="padding-right:10px">Job Title</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="job_title" id="job_title" readonly value="<?php echo $ada[0]->job_title ?>">
                  </div>
                </div>
              </div>
		</div>
		</div>
			<div class="col-xs-6">
         <div class="box box-info">
              <div class="box-body">
				<div class="form-group">
                  <label for="department" class="col-sm-3 control-label" style="padding-right:10px">Department</label>

					<div class="col-sm-8">
					  <input class="form-control" name="department" id="department" readonly value="<?php echo $ada[0]->department ?>"> 
					</div>
                </div>
				<div class="form-group">
                  <label for="reported_to" class="col-sm-3 control-label" style="padding-right:10px">Reported To</label>

                  <div class="col-sm-8">
                    <input class="form-control" name="reported_to" id="reported_to" readonly value="<?php echo $ada[0]->reported_to ?>">
                  </div>
                </div>
              </div>
		</div>
		</div>
		<hr>
		<div class="col-xs-12">
         <div class="box box-info">
              <div class="box-body">
                <div class="form-group">
                  <table>
				  <tr>
					<th width="25px"><center>No</center></th>
					<th width="500px"><center>Application Needs</center></th>
					<th width="110px"><center>Please Thick</center></th>
				  </tr>
				  <tr>
					<td class="opt1">1</td>
					<td>
					 User ID
					  <div class="form-group" id="name_userid" >
						  <label for="name_userid" class="col-sm-3 control-label" style="padding-right:10px;text-align:center">Name UserId</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control test" name="name_userid" id="name_userid" readonly value="<?php echo $ada[0]->name_userid ?>">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <?php
						$value = $ada[0]->userid;
						error_reporting(0);
						if($value == 1){
							echo '<input type="checkbox" id="userid" name="userid" checked>';
						}else{
							echo '<input type="checkbox" id="userid" name="userid">';
						}
					?>
					</td>
				  </tr>
				  <tr>
					<td class="opt1">2</td>
					<td>
					 Email Account
					</td>
					<td class="opt1">
					   <?php
						$value1 = $ada[0]->email_account;
						error_reporting(0);
						if($value1 == 1){
							echo '<input type="checkbox" id="email_account" name="email_account" checked>';
						}else{
							echo '<input type="checkbox" id="email_account" name="email_account">';
						}
					?>
					</td>
				  </tr>
				  <tr>
					<td class="opt1">3</td>
					<td>
					  Shared Drives
					  <div class="form-group" id="detail_drive">
						  <label for="detail_drive" class="col-sm-3 control-label" style="padding-right:10px">Detail Drive</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_drive" id="detail_drive" readonly value="<?php echo $ada[0]->detail_drive ?>">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <?php
						$value2 = $ada[0]->shared_drives;
						error_reporting(0);
						if($value2 == 1){
							echo '<input type="checkbox" id="shared_drives" name="shared_drives" checked>';
						}else{
							echo '<input type="checkbox" id="shared_drives" name="shared_drives">';
						}
					?>
					</td>
				  </tr>
				  <tr>
					<td class="opt1">4</td>
					<td>
					  Telephone
					  <div class="form-group" id="nmr_ext" >
						  <label for="nmr_ext" class="col-sm-3 control-label" style="padding-right:10px">No. EXT.</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="nmr_ext" id="nmr_ext" readonly value="<?php echo $ada[0]->nmr_ext ?>">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <?php
						$value3 = $ada[0]->telephone;
						error_reporting(0);
						if($value3 == 1){
							echo '<input type="checkbox" id="telephone" name="telephone" checked>';
						}else{
							echo '<input type="checkbox" id="telephone" name="telephone">';
						}
					?>
					</td>
				  </tr>
				  <tr>
					<td class="opt1">5</td>
					<td>
					  Intranet ID
					  <div class="form-group" id="detail_intranetid">
						  <label for="detail_intranetid" class="col-sm-3 control-label" style="padding-right:10px">Detail Intranet</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_intranetid" id="detail_intranetid" readonly value="<?php echo $ada[0]->detail_intranetid ?>">
						  </div>
						</div>
					</td>
					<td class="opt1">
					   <?php
						$value4 = $ada[0]->intranetid;
						error_reporting(0);
						if($value4 == 1){
							echo '<input type="checkbox" id="intranetid" name="intranetid" checked>';
						}else{
							echo '<input type="checkbox" id="intranetid" name="intranetid">';
						}
					?>
					</td>
				  </tr>
				  <tr>
					<td class="opt1">6</td>
					<td>
					  WRS ID
					  <div class="form-group" id="detail_wrs">
						  <label for="detail_wrs" class="col-sm-3 control-label" style="padding-right:10px">Detail WRS</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_wrs" id="detail_wrs" readonly value="<?php echo $ada[0]->detail_wrs ?>">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <?php
						$value5 = $ada[0]->wrsid;
						error_reporting(0);
						if($value5 == 1){
							echo '<input type="checkbox" id="wrsid" name="wrsid" checked>';
						}else{
							echo '<input type="checkbox" id="wrsid" name="wrsid">';
						}
					?>
					</td>
				  </tr>
				   <tr>
					<td class="opt1">7</td>
					<td>
					  Pentana ID
					  <div class="form-group" id="detail_pentana" >
						  <label for="detail_pentana" class="col-sm-3 control-label" style="padding-right:10px" >Detail Pentana</label>

						  <div class="col-sm-8">
							<input type="text" class="form-control" name="detail_pentana" id="detail_pentana" readonly value="<?php echo $ada[0]->detail_pentana ?>">
						  </div>
						</div>
					</td>
					<td class="opt1">
					  <?php
						$value6 = $ada[0]->mds_pentana;
						error_reporting(0);
						if($value6 == 1){
							echo '<input type="checkbox" id="mds_pentana" name="mds_pentana" checked>';
						}else{
							echo '<input type="checkbox" id="mds_pentana" name="mds_pentana">';
						}
					?>
					</td>
				  </tr>
				</table>
                </div>				
              </div>
		</div>
		</div>
		</form>
		</div>
</center>
          <!-- this row will not appear when printing -->
          <div class="row no-print">
            <div class="col-xs-12">
              <button id="printLink" onclick="window.print()">Cetak Halaman Web</button>
             </div>
          </div>
        </section><!-- /.content -->
        <div class="clearfix"></div>
      </div><!-- /.content-wrapper -->
    
    <!-- jQuery 3 -->
	<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- FastClick -->
	<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="../../dist/js/adminlte.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="../../dist/js/demo.js"></script>
	<!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../../plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../../dist/js/demo.js"></script>
  </body>
  
</html>
