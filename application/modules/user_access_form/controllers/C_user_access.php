<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_user_access extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_user_access');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_user_access->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}
	 	}
		
	public function index()
		{

		$data['loan']=$this->M_user_access->get_all_loan();
		$data['total']=$this->M_user_access->get_count_id();
		$data['group']=$this->M_user_access->get_all_group();
		$data['department']=$this->M_user_access->get_all_dept();
		$data['purchase']=$this->M_user_access->get_all_purchase();
		$data['company']=$this->M_user_access->get_all_company();
		$data['company2']=$this->M_user_access->get_all_company();
		$data['show_view'] = 'user_access_form/V_user_access';
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function ajax_list()
	{
		$list = $this->M_user_access->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_user_access) {
			$chk_idmaster ='<div align="center"><input id="checkuser" name="checkuser" type="checkbox" value='.$tbl_user_access->id_user_access.' class="editRow ace" />
           <span class="lbl"></span> ';
		   $print_pdf ='<div align="center"><a href="C_user_access/view_user_pdf?id='.$tbl_user_access->id_user_access.'" value='.$tbl_user_access->id_user_access.' name="print_pdf" type="button"  class="editRow ace">Detail</a>
           </div> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_user_access->id_user_access;
			$row[] = $tbl_user_access->request_date;
			$row[] = $tbl_user_access->full_name;
			$row[] = $tbl_user_access->id_employee;
			$row[] = $tbl_user_access->dept;
			$row[] = $tbl_user_access->job_title;
			$row[] = $tbl_user_access->reported_to;
			$row[] = $tbl_user_access->name;
			$row[] = $print_pdf;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_user_access->count_all(),
						"recordsFiltered" => $this->M_user_access->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	public function add_user()
		{
		$str_user =$this->session->userdata('id');
		$type = $_POST['type'];
		$value = implode(",",$type);
		$employee = $_POST['employee'];
		$value1 = implode(",",$employee);
		if(isset($_POST['userid'])) {
			$value2 = 1; }
			else {
			$value2 = 0;
		}
		if(isset($_POST['email_account'])) {
			$value3 = 1; }
			else {
			$value3 = 0;
		}
		if(isset($_POST['shared_drives'])) {
			$value4 = 1; }
			else {
			$value4 = 0;
		}
		if(isset($_POST['telephone'])) {
			$value5 = 1; }
			else {
			$value5 = 0;
		}
		if(isset($_POST['intranetid'])) {
			$value6 = 1; }
			else {
			$value6 = 0;
		}
		if(isset($_POST['wrsid'])) {
			$value7 = 1; }
			else {
			$value7 = 0;
		}
		if(isset($_POST['mds_pentana'])) {
			$value8 = 1; }
			else {
			$value8 = 0;
		}
		
		if(empty($_POST["type"])){
			die ("Field Request Type must be filled in!! ");
		}
		elseif(empty($_POST["employee"])){
			die ("Field Employee Type must be filled in!! ");
		}
		elseif(empty($_POST["id_employee"])){
			die ("Field ID Employee must be filled in!! ");
		}
		elseif(empty($_POST["full_name"])){
			die ("Field Name must be filled in!! ");
		}
		elseif(empty($_POST["department"])){
			die ("Field Department must be filled in!! ");
		}
		elseif(empty($_POST["reported_to"])){
			die ("Field Reported To must be filled in!! ");
		}
		else{
			 $data = array(
					'id_user' => $str_user,
					'request_type' => $value,
					'until_date' =>  $this->input->post('datepicker'),
					'additional_reason' => $this->input->post('additional_reason'),
					'employee_type' => $value1,
					'temporary_name' => $this->input->post('temporary_name'),
					'sponsor_by' => $this->input->post('sponsor_by'),
					'id_employee' => $this->input->post('id_employee'),
					'full_name' => $this->input->post('full_name'),
					'job_title' => $this->input->post('job_title'),
					'department' => $this->input->post('department'),
					'reported_to' => $this->input->post('reported_to'),
					'userid' => $value2,
					'name_userid' => $this->input->post('name_userid'),
					'email_account' => $value3,
					'shared_drives' => $value4,
					'detail_drive' => $this->input->post('detail_drive'),
					'telephone' => $value5,
					'nmr_ext' => $this->input->post('nmr_ext'),
					'intranetid' => $value6,
					'detail_intranetid' => $this->input->post('detail_intranetid'),
					'wrsid' => $value7,
					'detail_wrs' => $this->input->post('detail_wrs'),
					'mds_pentana' => $value8,
					'detail_pentana' => $this->input->post('detail_pentana'),
					'status' => '1',
				); 
			
			$insert = $this->M_user_access->add_user_access($data);
		 }
		 echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_user_access->get_by_id($id);
			echo json_encode($data);
		}
		
	function pos_branch($id_company)
	{
    	$query = $this->db->get_where('tbl_branches',array('id_company'=>$id_company));
    	$data = "<option value=''>- Select Branch -</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->branch_id."'>".$value->name_branch."</option>";
    	}
    	echo $data;
	}

	public function update_user()
		{
		$str_user =$this->session->userdata('id');
		$type = $_POST['type'];
		$value = implode(",",$type);
		$employee = $_POST['employee'];
		$value1 = implode(",",$employee);
		if(isset($_POST['userid'])) {
			$value2 = 1; }
			else {
			$value2 = 0;
		}
		if(isset($_POST['email_account'])) {
			$value3 = 1; }
			else {
			$value3 = 0;
		}
		if(isset($_POST['shared_drives'])) {
			$value4 = 1; }
			else {
			$value4 = 0;
		}
		if(isset($_POST['telephone'])) {
			$value5 = 1; }
			else {
			$value5 = 0;
		}
		if(isset($_POST['intranetid'])) {
			$value6 = 1; }
			else {
			$value6 = 0;
		}
		if(isset($_POST['wrsid'])) {
			$value7 = 1; }
			else {
			$value7 = 0;
		}
		if(isset($_POST['mds_pentana'])) {
			$value8 = 1; }
			else {
			$value8 = 0;
		}
		
		if(empty($_POST["type"])){
			die ("Field Request Type must be filled in!! ");
		}
		elseif(empty($_POST["employee"])){
			die ("Field Employee Type must be filled in!! ");
		}
		elseif(empty($_POST["id_employee"])){
			die ("Field ID Employee must be filled in!! ");
		}
		elseif(empty($_POST["full_name"])){
			die ("Field Name must be filled in!! ");
		}
		elseif(empty($_POST["department"])){
			die ("Field Department must be filled in!! ");
		}
		elseif(empty($_POST["reported_to"])){
			die ("Field Reported To must be filled in!! ");
		}
		else{
			 $data = array(
					'request_type' => $value,
					'until_date' =>  $this->input->post('datepicker1'),
					'additional_reason' => $this->input->post('additional_reason'),
					'employee_type' => $value1,
					'temporary_name' => $this->input->post('temporary_name'),
					'sponsor_by' => $this->input->post('sponsor_by'),
					'id_employee' => $this->input->post('id_employee'),
					'full_name' => $this->input->post('full_name'),
					'job_title' => $this->input->post('job_title'),
					'department' => $this->input->post('department'),
					'reported_to' => $this->input->post('reported_to'),
					'userid' => $value2,
					'name_userid' => $this->input->post('name_userid'),
					'email_account' => $value3,
					'shared_drives' => $value4,
					'detail_drive' => $this->input->post('detail_drive'),
					'telephone' => $value5,
					'nmr_ext' => $this->input->post('nmr_ext'),
					'intranetid' => $value6,
					'detail_intranetid' => $this->input->post('detail_intranetid'),
					'wrsid' => $value7,
					'detail_wrs' => $this->input->post('detail_wrs'),
					'mds_pentana' => $value8,
					'detail_pentana' => $this->input->post('detail_pentana'),
				); 
				
			$this->M_user_access->update_user(array('id_user_access' => $this->input->post('id')), $data);
		  }
		   echo ('Insert');
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);
				$this->db->where('id_user_access', $id);
				$this->db->update('tbl_user_access', $data);
			}
		}
	}
	
	 public function view_add_user()
	{

	  $data['show_view']='user_access_form/V_add_user';
	  $this->load->view('dashboard/Template',$data);		  	 	     		   		      						   	   	
	}
	
	 public function view_user_pdf()
	{

	  $data['show_view']='user_access_form/V_print_user';
	 $this->load->view('dashboard/Template',$data);	  
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }



}
