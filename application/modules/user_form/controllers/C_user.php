<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_user extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_user','C_user',TRUE);	
		$this->load->library('form_validation');	   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_user->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}			
	}		  	
		
  public function index()
  {					    																
	$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_user= $this->C_user->tampil_user()->result();	
	  $total_rows =$this->C_user->tampil_user()->num_rows();
	  
	  $tampillevel= $this->C_user->show_level()->result();	
	  $tampilposition= $this->C_user->show_position()->result();		  
	  									
	  if ($tampil_table_user)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(4);
		$per_page =5;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'user_form/C_user/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 4;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['user_view'] =$this->C_user->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
		
		$data['level_view'] =$tampillevel;
		$data['posisi_view'] =$tampilposition;				
		$data['header'] ="REGISTER USER";
		$data['ceck_row'] = $total_rows;														
		$data['intno'] = ""; //variable buat looping no table.														
		$data['show_view'] = 'user_form/V_table_user';				
		$this->load->view('dashboard/Template',$data);				
	  }else{		   				
		$data['header'] ="REGISTER USER ";
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data User table is empty';		
		$data['show_view'] = 'user_form/V_table_user';		
		$this->load->view('dashboard/Template',$data);				
	  } 
  }
 
 public  function do_save()
 {
        $this->form_validation->set_rules('txtname','txtname','required');
		$this->form_validation->set_rules('txtuser','txtuser','required');	    
		$this->form_validation->set_rules('txtemailuser','Email','required');						
		
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan','Insert Failed  some data required!!');
		   redirect('user_form/C_user'); 	
		}else{   		 
			if ($this->C_user->save_data())
			{	 	
				$this->session->set_flashdata('pesan_succes','Insert User Successfully');			
				redirect('user_form/C_user');   
			}else{
				$this->session->set_flashdata('pesan','Insert User Failed');		
				redirect('user_form/C_user');   
			}
		}
 } 
 
  public  function do_update()
 {
	    $this->form_validation->set_rules('txtid','txtid','required');
		$this->form_validation->set_rules('txtname','txtname','required');
		$this->form_validation->set_rules('txtuser','txtuser','required');		
		$this->form_validation->set_rules('txtemailuser','Email','required');				
		
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan','Update Failed some data required!!');
		   redirect('user_form/C_user'); 	
		}else{   		 
			if ($this->C_user->edit_data())
			{	 	
				$this->session->set_flashdata('pesan_succes','Update User Successfully');			
				redirect('user_form/C_user');   
			}else{
				$this->session->set_flashdata('pesan','Update User Failed');		
				redirect('user_form/C_user');   
			}
		}
 } 
 
 public function do_delete()
 {
	 if ($this->C_user->delete_data())
	{	 	
		$this->session->set_flashdata('pesan_succes','Delete user Successfully');			
		redirect('user_form/c_user');   
	}else{
		$this->session->set_flashdata('pesan','Delete user Failed');		
		redirect('user_form/c_user');   
	}
 }
 
 public function do_search()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->C_user->search_data(); // manggil hasil cari di model 				 
	        
		
		if($tampung_cari == null){		  			
		   $data['header'] ="REGISTER USER  ";
		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['user_view'] =$tampung_cari;
		   $data['pesan'] = 'User Data Not a found'; 		 		  
		   $data['show_view'] = 'user_form/V_table_user';		
		   $this->load->view('dashboard/Template',$data);					
		}else{					   			
		    $data['header'] ="REGISTER USER  ";
		    $data['intno'] = ""; //variable buat looping no table.
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['user_view'] =$tampung_cari;			   			
			$data['show_view'] = 'user_form/V_table_user';		
		    $this->load->view('dashboard/Template',$data);		
		}	   
    }
 
 
  public function do_reset_pass()  // fungsi cari data controler
  {				  			    	
		$tampung_reset = $this->C_user->reset_data(); 
		
		if ($tampung_reset) :
		   $this->session->set_flashdata('pesan_succes','Reset Password Successfully');
		   redirect('user_form/C_user');
		endif;
  }
 
 public function multiple_submit()
 {
	 if ($this->input->post('btnsave')) {
		 $this->do_save();
	 }else{
		 if ($this->input->post('btncari')) {
			 $this->do_search();
	 	 }else{
			 if ($this->input->post('btndel')) {
			     $this->do_delete();
	 	     }else{
				  if ($this->input->post('btnupdate')) {
			          $this->do_update();
	 	     	  }else{
					   if ($this->input->post('btnreset')) {
			              $this->do_reset_pass();
					  }else{
						  redirect('user_form/C_user');
					  }
				  }			
			 }
		 }
	 }
		 
 }
 
 //------------------------------------pashing result to modal popup View Edit vendor-------------------
	
	public function get_iduser_modal_update() {    	       			
	     $id_user = $this->input->post('id_user',TRUE); //       
	     $query = $this->db->query("select * from qv_complite_user where id ='".$id_user."'");  	    
		
		 foreach ($query->result() as $row)		
		 {
			$data['id_user']        = $row->id;
			$data['name']           = $row->name;
			$data['username']       = $row->username;			
			$data['email']          = $row->email;			
			$data['id_level']       = $row->id_level;			
			$data['id_position']     = $row->id_position; 						 
		 }
	    
		 echo json_encode($data); //masukan kedalam jasson jquery untuk menampilkan data	   		  			   		      		
    }      					
      						 		
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }
}

