<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_user extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_user';
	 
    public function tampil_user(){ //tampil table untuk ngececk num_row						     	     		  
	     
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		  		 		 				 
		 $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('status',$status_delete);				
		 $this->db->order_by('id','desc');
		 $result = $this->db->get('qv_complite_user');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    	     
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  	     		 
	     $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('status',$status_delete);				  		 
		 $this->db->order_by('id','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_user')->result();		 		   
		 return $result ;
	}							  		

   public function show_level()
   {
	  $this->load->database();
	  $this->db->select('id_level,level');
	  $this->db->where('status','1');
	  $result = $this->db->get('tbl_level');	  
	  return $result;	  
   }	
   	
   public function show_position()
   {
	  $this->load->database();
	  $this->db->select('id_position,position');
	  $this->db->where('status','1');
	  $result = $this->db->get('tbl_posisi');	  
	  return $result;	  
   }
      
  public function save_data()
  {			
	$str_user = $this->input->post('txtuser');  
	$str_pass = md5($this->input->post('txtuser'));
	$str_name= strtoupper($this->input->post('txtname'));  
    $str_level = $this->input->post('cbolevel');  
	$str_posisi = $this->input->post('cboposisi');  			
	$str_email_user = $this->input->post('txtemailuser'); 	
	$str_status = "1";  	
					
	$insert_data = array(
	  				"username" => $str_user,
					"pass" => $str_pass,
					"name" => $str_name,
					"id_level" => $str_level ,
					"id_position " => $str_posisi,										
					"email" =>$str_email_user,					
					"status" =>$str_status,					
					
					);
	$this->db->insert('tbl_user',$insert_data);		
	return true;							
  }		
  
    public function edit_data()
	{		 	
	   	$str_iduser = $this->input->post('txtid');
		$str_user = $this->input->post('txtuser');  		
		$str_name= strtoupper($this->input->post('txtname'));  
		$str_level = $this->input->post('cbolevel');  
	    $str_posisi = $this->input->post('cboposisi');  			
	    $str_email_user = $this->input->post('txtemailuser'); 			
		
		if ($str_iduser !='') {
			$edit_data = array(
							"username" => $str_user,							
							"name" => $str_name,
							"id_position" => $str_posisi  ,
					        "id_level" =>  $str_level,													
							"email" =>$str_email_user
							//"status" => $str_status													
							);
							
			$this->db->where('id',$str_iduser);
			$this->db->update('tbl_user',$edit_data); 															
			return true;		
		}else{
			return false;		
		}
								
	  }		
	  
	    public function delete_data()
	    {		 		
			$delete = $this->input->post('msg');  		
								
			   if(isset($delete) && trim($delete!=''))
			   {	   				
					for ($i=0; $i < count($delete) ; $i++) {																																																									
						$this->db->where('id',$delete[$i]);
						$this->db->delete('tbl_user'); 
					}		
					return true;		
				}else{
					return false;		
				}								
	  }		
				
		public function search_data() 	// fungsi cari data vendor
		{
		   $cari = $this->input->post('txtcari');
		   $status_del = "1";	   
		   
		   if ($cari == null) // jika textbox cari kosong
		   {
			   redirect('user_form/C_user');  //direct ke url c_vendor/c_vendor
		   }else{	
			   $result = $this->db->query("select * from qv_complite_user where status ='".$status_del."' and name like '".'%'.$cari.'%'."' or username like '".'%'.$cari.'%'."' or email like '".'%'.$cari.'%'."'"); 		   		   	 
			   return $result->result(); 
		   }
		} 		    	
		
		public function reset_data() 	// fungsi cari data vendor
		{
		   $str_iduser = $this->input->post('txtid');
		   $str_txtuser = $this->input->post('txtuser');
		   $status_del = "1";	
		   
		   if ($str_iduser == null) // jika textbox cari kosong
		   {
			   redirect('user_form/C_user');  //direct ke url c_vendor/c_vendor
		   }else{				 
			   $update_pass = array("pass" => md5($str_txtuser));																																										
			   $this->db->where('id',$str_iduser);
			   $this->db->update('tbl_user',$update_pass); 	
			   return true ;
			 
		   }
		} 		    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
		public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}		 
}