<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>

 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>
      

 
   
          
 <?php  echo form_open('user_form/C_user/multiple_submit','id = form_my');  ?>      
 <table>
     <tr>
      
       <td>
       	 <div class="input-group">        
              <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari" > 
            <div class="input-group-btn">                                                     
            <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF;height:34px;padding:1px;"  />              
            </div>
          </div> 
       </td>
       <td colspan="3">&nbsp;  
             
       </td> 
       <td colspan="3">                        
       
       <input id="btnadd" name="btnadd" type="button" value="Add New"  class="btn btn-info addnew " style="height:34px;padding:1px;"  />        
       <input id="btnedit" name="btnedit" type="button" value="Edit"   class="btn btn-success edit_user" disabled="disabled" style="height:34px;padding:1px;"/>
       <input id="btndel" name="btndel" type="submit" value="Delete"  class="btn btn-warning" onclick="return confirm('are you sure want to delete this data!')" disabled="disabled" style="height:34px;padding:1px;"/>	  	     
         </td>
      </tr> 
    </table>   
 </br>
 

<div style="overflow-x:auto;">  
<table id="myTable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead>
      <tr>
        <th>ID User</th>      
        <th>Name</th>
        <th>Username</th>
        <th>Level</th>
        <th>Position</th>
        <th>Email</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php  
	      if ($ceck_row >= 1)  :	  
			  foreach($user_view as $row): ;				   			
	  ?>
              <tr>
                 <td><?php echo $row->id ; ?></td>
                 <td><?php echo $row->name ; ?></td>
                 <td><?php echo $row->username ; ?></td>
                 <td><?php echo $row->email ; ?></td>
                 <td><?php echo $row->level ; ?></td>
                 <td><?php echo $row->position ; ?></td>                  
     <td><div align="center"><input id="checkchild" name="msg[]" type="checkbox" value="<?php echo $row->id; ?>" />
                 </td>      
              </tr>           
    		 <?php endforeach ?>
     <?php endif ?> 
     
    </tbody>
  </table> 
  <?php form_close(); ?>                    	    		
     <?php	 if (! empty($pagination)) :?>	   					    					 
             
                 <?php echo $pagination ; ?>
                						
      <?php  endif	?> 
</div>		
	
     
    
		 

<style>
/*label berwarna merah ketika validasi fom modal popup. */
	label
	{
		width:50%;
		display: block;
		margin-right:2%;
		text-align:left;
		line-height:22px;
	}	
    span.error
	{
		font: 12px ;
		color:red;
		margin-left:8px;
		line-height:22px;
	}	
/*------------------------- */
</style>
	
 <!-- Modal Buat save and edit -->
    <div id="tampilanform"></div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    
                    <div class="modal-body">
                          <table width="100%" class="table-striped table-condensed" align="center">
						
                         <tr hidden="true"> 
                        <td id="row_label"> <label for="id_user"  class="col-xs-10">id_user</label></td>
                          <td id="row_input">
                          <div class="control-group">
                               <div class="col-xs-12">
                               <input name="txtid"  type="text" class="form-control" id="txtid" readonly >    
                               </div> 
                           </div>
                          </td>  
                                               
                        </tr>    
                        <tr> 
                        <td> <label for="Name" style="color:#900" class="col-xs-10">Name *</label></td>
                          <td>
                          <div class="control-group">
                              <div class="col-xs-12">   
                              <input name="txtname"  type="text" class="form-control " id="txtname" maxlength="49">
                              </div> 
                           </div>    
                          </td>  
                                       
                          
                                                                 
                        </tr>  
                         <tr> 
                        <td> <label for="User Name" style="color:#900" class="col-xs-10">User Name*</label></td>
                          <td>
                          <div class="control-group">
                              <div class="col-xs-11">   
                              <input name="txtuser"  type="text" class="form-control " id="txtuser" maxlength="49">
                              </div> 
                           </div>    
                          </td>  
                        <tr>
                         <tr> 
                        <td> <label for="email" style="color:#900" class="col-xs-10">Email*</label></td>
                          <td>
                          <div class="control-group">
                              <div class="col-xs-11">   
                              <input name="txtemailuser"  type="text" class="form-control " id="txtemailuser" maxlength="70">
                              </div> 
                           </div>    
                          </td>  
                        <tr>
                           <td ><label for="User Group" style="color:#900"  class="col-xs-10">User Level *</label></td> 
                        <td>
                        <div class="control-group">
                          <div class="col-xs-10">
                            <select name="cbolevel" id="cbolevel" class="form-control">             
                                  <option>- Choose -</option>     
                                 <?php 								     
								    foreach ($level_view as $row_level )
								    {                             
									echo '<option value="'.$row_level->id_level.'">'.$row_level->level.'</option>';
								    } 
							 	  ?>	                                    
                           </select> 
                          </div>  
                         </div> 
                        </td>
                          </tr>
                          <td> <label for="company" style="color:#900" class="col-xs-10">Position *</label></td>                         
                          <td>
                          <div class="control-group">
                           <div class="col-xs-10">
                                 <select name="cboposisi" id="cboposisi" class="form-control">                                  <option>- Choose -</option>                                  
                                   <?php 								     
								    foreach ($posisi_view as $row_pos )
								    {                             
									 echo '<option value="'.$row_pos->id_position.'">'.$row_pos->position.'</option>';
								    } 
							 	  ?>	 
                                </select> 
                            </div> 
                           </div>
                        
                          </td>           
                                                    
                        </tr>                                                                           
                        </table>                 
                    </div>                     
                     <div class="modal-footer">
                       <input type="submit" name="btnsave" id="btnsave" value="Submit " class="btn btn-primary"/>
                       <input type="submit" name="btnupdate" id="btnupdate" value="Submit Update" class="btn btn-primary"/>                       <input type="submit" name="btnreset" id="btnreset" value="Reset Password" class="btn btn-success"/>
                       <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal Buat edit -->  	



<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.validate.js" charset="UTF-8"></script>

  
<script>
// fungsi untik checklist semua otomatis
function CheckAll()
{
    l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = true;		
		}	
		document.getElementById('btnedit').disabled = true;	
		document.getElementById('btndel').disabled = false;	
		document.getElementById('btnsendtab').disabled = true;	
}

function UncheckAll()
{
   l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = false;			 
		}
		document.getElementById('btnedit').disabled = true;
		document.getElementById('btndel').disabled = true;
		document.getElementById('btnsendtab').disabled = true;
}


//--------------------function disbaled enabled button with check book

$(function() {  //.btn hnya untuk tombol edit yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn-success').removeAttr('disabled');			
        }
        else{
            $('.btn-success').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btn-warning').removeAttr('disabled');			
        }
        else{
            $('.btn-warning').attr('disabled', 'disabled');								
        }
    });
});


  
//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});		  


//--------------function number only
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 45 && charCode != 41 && charCode != 40 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	
</script>

<script>
// Show modal pop up for addnew----------
$(function(){ 
	  $(document).on('click','.addnew',function(e){		       		  
			$("#myModal").modal('show');						  
			$("#myModalLabel").html("Create New User");							
			$("#txtid").val("")	;	
			$("#txtname").val("")	;
			$("#txtuser").val("")	;			
			$("#txtemailuser").val("")	;				
			$("#cbolevel").val("- Choose -"); 
			$("#cboposisi").val("- Choose -"); 					
		    $("#row_label").hide();	
			$("#row_input").hide();		
			$("#btnupdate").hide();	
			$("#btnreset").hide();				
		    $("#btnsave").show();		       												
	  });																						   	 	   
});		
</script>

<script>
 
$(function(){
  $(document).on('click','.edit_user',function(e){
	
		var req_id = $('input[type="checkbox"]:checked').val()
		var url = '<?php echo base_url("user_form/C_user/get_iduser_modal_update"); ?>';
					
		$("#myModal").modal('show');  	
		$("#myModalLabel").html("Edit User");			
	    					
		$.ajax({			
			type:'POST',
			url: url,
			dataType: "json",
			data: {id_user:req_id}, 
			success: function(data, textStatus, jqXHR ) {				    			
			    $("#row_label").show();	
			    $("#row_input").show();	
				$("#btnsave").hide();
				$("#btnreset").show();
				$("#btnupdate").show();				
				$('[name="txtid"]').val(req_id);
				$('[name="txtname"]').val(data.name);
				$('[name="txtuser"]').val(data.username);				
				$('[name="txtemailuser"]').val(data.email);								 				
				$('[name="cbolevel"]').val(data.id_level); 
				$('[name="cboposisi"]').val(data.id_position);
			}
						
		});				
		
   });	
});	
</script>

<script>
$(function(){
	//jquery untuk post submit save / edit
    $('#myform').on('submit', function(e){ //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
        e.preventDefault();		
		var url = '<?php  echo base_url('user_form/C_user/multiple_submit');  ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
            success: function(data){              
            }
        });
    });
});
</script>


