

<script type="text/javascript">
//--------------function number only
 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	


</script>


<script script type="text/javascript">

//calculate and add field--------------
$(function(){		
	$('#add').click(function(){
	  addnewrow();	
	 }); 	
	 
	
	 $('body').delegate('.remove','click',function(){				 				 
	 	 $(this).parent().parent().remove();
		  		
		    var tr =  $(this).parent().parent();  
		    var get_iddet =  tr.find('.txtid').val();//getiddetailpp from 									
		    var buff_iddet =$('.buffarray_iddet').val()			
			
		    if  (buff_iddet == "") { 	
			    $('.buffarray_iddet').val(get_iddet);
			}else{
		        $('.buffarray_iddet').val($('.buffarray_iddet').val() + "," + get_iddet );
			} 
			//end--------------------------------------------------------------------------
		     total();
		   			  		  			 		 
	 });
	 	
	 
	  $('body').delegate('.quantity,.price','change',function(){  
		   var tr =  $(this).parent().parent();
		   var qty=numeral().unformat(tr.find('.quantity').val());
		   var price=numeral().unformat(tr.find('.price').val());
		   var amt=numeral().unformat(tr.find('.amount').val());
		   var stattaxppn =  tr.find(".txtstat").val();
		   var stattaxpph =  tr.find(".txtstatpph").val();
		   var amt=qty*price;  
		   
		   tr.find('.amount').val(amt);
		   
		  
		   			
		   if (stattaxppn == "PPN") { //ppn 10%			   		       		   	
		       var ttlppn= amt * (10/100);
			   tr.find('.taxcharge').val(numeral(ttlppn).format('0,0.00'));								
		   }		   
		  
		    if (stattaxpph =="PPH2%") { //pph 2%			   
		       var ttlpph = amt * (2/100);	   
			   tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));								
		   }else{
			    if (stattaxpph =="PPH4%") { //pph 2%			   
		      	    var ttlpph = amt * (4/100);
				    tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
				}else{	
					if (stattaxpph =="PPH15%") { //pph 2%			   
		      	    	var ttlpph = amt * (15/100);
						 tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
					}else{	
			   			 tr.find('.taxchargepph').val('0,0.00');
					}
				}
		   }		 
		   total(); 
 	 });
	 	 
	 
	$('body').delegate('.quantity,.price','blur',function(){  
		  var tr =  $(this).parent().parent();
		   var qty=tr.find('.quantity').val();
		   var price=tr.find('.price').val();
		   amt=numeral().unformat(tr.find('.amount').val());		
		   
		   tr.find('.quantity').val(numeral(qty).format('0,0')) ;
		   tr.find('.price').val(numeral(price).format('0,0.00')) ;
		   tr.find('.amount').val(numeral(amt).format('0,0.00')) ;
		   total();
 	 });  	
	 
	  $('body').delegate('.txtstat','change',function(){  		
		  var tr =  $(this).parent().parent();
		   var amt = numeral().unformat(tr.find('.amount').val());
		   var stattaxppn =  tr.find(".txtstat").val()	 ;
			
		   if (stattaxppn == "PPN") { //ppn 10%			   		       		   	
		       var ttlppn= amt * (10/100)
			   tr.find('.taxcharge').val(numeral(ttlppn).format('0,0.00'));								
		   }		   		  
		   
		   if (stattaxppn == "NONE") { //none			   
				tr.find('.taxcharge').val('0,0.00');
		   }	
		 
		    //function total----------------
		      total() ;
		    //end---------------------------		   		  	   	   		   
 	 }); 	
	 
	  $('body').delegate('.txtstatpph','click',function(){  		
		   var tr =  $(this).parent().parent();
		   var amt = numeral().unformat(tr.find('.amount').val())	;			 
		   var stattaxpph =  tr.find(".txtstatpph").val();
					 
		   if (stattaxpph =="PPH2%") { //pph 2%			   
		       var ttlpph = amt * (2/100);	   
			   tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));								
		   }else{
			    if (stattaxpph =="PPH4%") { //pph 2%			   
		      	    var ttlpph = amt * (4/100);
					 tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
				}else{	
					if (stattaxpph =="PPH15%") { //pph 2%			   
		      	    	var ttlpph = amt * (15/100);
						 tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
					}else{	
			   			 tr.find('.taxchargepph').val('0,0.00');
						 tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
					}
				}
		   }
		 
		    //function total----------------
		      total() ;
		    //end---------------------------	   		  	   	   		   
 	 }); 	
	 	
	 	  	
});


function total()
{
	var t=0;
	var tppn=0;
	var tpph=0;	
	var jmlbaris=parseInt($('.remove').length);
	
	for(var i=0;i<jmlbaris;++i){
	   t+=parseFloat(numeral().unformat($('.amount').eq(i).val()));
	}	
	
	for(var i=0;i<jmlbaris;++i){	
	
	      tppn+=parseFloat(numeral().unformat($('.taxcharge').eq(i).val()));			  
	  	  tpph+=parseFloat(numeral().unformat($('.taxchargepph').eq(i).val()));	   		 		  
	}		
	
			
	var hasilgranppn = (t+tppn) - tpph;
	$('.total').val(numeral(t).format('0,0.00')) ;	
	$('.ppncalc').val(numeral(tppn).format('0,0.00'));
	$('.pphcalc').val(numeral(tpph).format('0,0.00'))	;
	$('.grandppn').val(numeral(hasilgranppn).format('0,0.00'));
	
}


function addnewrow()
{

var strnopp =$('#txtnopp').val()

var n = ($('.detail tr').length-0)+1;	
var tr = '<tr>'+
'<td class="no" align="center">'+ n +'</td>'+                                            
'<td><input type="text" id="txtidmasdetid" name="txtidmasdet[]"  value="'+strnopp+'"  hidden="hide" /><input type="text" name="txtiddet[]" class="txtid" value="" hidden="hide"  /> <input type="text" name="flagstat[]"  value="1" hidden="hide" /><input type="text" maxlength="70"  name="txtdesc[]"  placeholder="Description" size="24" class="form-control txtdesc" /></td>'+'<td><input type="text" maxlength="70"  name="txtspecs[]"  placeholder="Specs" size="30" class="form-control txtspecs" /></td>'+'<td><input maxlength="6" id="txtqty"  name="txtqty[]" type="text" placeholder="0" size="7" onkeypress="return isNumberKey(event)" class="form-control quantity" /></td>'+                                                		                                  
'<td><input maxlength="14"  name="txtprice[]" type="text" placeholder="0.00" size="30" onkeypress="return isNumberKey(event)" class="form-control price"/> </td>'+	
'<td><input  maxlength="70"   name="txtamount[]"  placeholder="0.00"  size="35"  class="form-control amount" readonly="readonly"/></td>'+'<td width="7%"><select  name="cbotax[]" class="form-control txtstat"><option value="NONE">NONE</option><option value="PPN">PPN</option></select></td>'+' <td width="7%"><select  name="cbotaxpph[]" class="form-control txtstatpph"><option value="NONE">NONE</option><option value="PPH2%">PPH2%</option><option value="PPH4%">PPH4% </option><option value="PPH15%">PPH15% </option>  </select></td>'+'<td><input  maxlength="70" name="txttaxcharge[]" placeholder="0.00"  size="30"  class="form-control taxcharge" readonly="readonly" /></td>'+'<td><input  maxlength="70" name="txttaxchargepph[]"  placeholder="0.00"  size="30"  class="form-control taxchargepph" readonly="readonly" /></td>'+'<td> <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button></td>'+
'</tr>';
 
 $('.detail').append(tr);
}


</script>

<script>
$(function(){		
	$('.ppnnot').change(function(){		 		
		 if($(this).is(':checked')){
				  $('.ppncalc').val("0.00");	
				  $('.grandppn').val($('.total').val());						 		 
		 }else{					
			total()
		 }		 		
	});
});
</script>

<script type="text/javascript">
//upload menggunakan userfile type file
$(document).ready(function(){	
	 $('#fileSelect').bind('change',function(event){     	 
	  event.preventDefault(); 	  
	  var formData = new FormData(document.getElementById('form-upload'));		 										
		   $.ajax({					          					
					url:"<?php echo base_url('epp_form/c_create_pp/do_upload_excel');  ?>", 										
					type: 'POST',						
					//enctype: "multipart/form-data", 					
					data: formData,										
					processData: false,															
		    		//async: false,
					processData: false,
					contentType: false,		
					cache : false,	
					beforeSend:function()
					 {
						$("#prog").show();
						$("#prog").attr('value','0');
						   
				     },
					   uploadProgress:function(event,position,total,percentCompelete)
					   {
						  $("#prog").attr('value',percentCompelete); 
						  $("#percent").html(percentCompelete+'%');
					   },		            
									
				    success:function (data, textStatus, jqXHR) {			 											
					  alert(data);
					  $('#hexcel').show();
					  $('#hdel').show();																	
				    }
		  });	 
    });
});

</script>
<script>
//check file exsist 
	$(document).ready(function(e) {		
	   var checkfile = "<?php 
		                 $company_cut =substr($this->session->userdata('short'),0,-1);
				         $id_dept =$this->session->userdata('id_dept');
						 echo base_url('./asset/uploads/'.$company_cut.'/'. $id_dept .'/data_part.csv'); ?>" ;			  
	  
	   $.ajax({
		url:checkfile ,
		type:'post',
		error: function()
		{
			 $('#hexcel').hide();
			 $('#hdel').hide();
		},
		success: function()
		{
			 $('#hexcel').show();
			 $('#hdel').show();
		}
	});	   					
 });
</script>

<script>
//delete excel.....file

$(function(){
    $('a.deleteexcel').click(function(){
      $.ajax({
       url:"<?php echo base_url('epp_form/c_create_pp/clear_excel'); ?>",      
       method:'POST',
       success:function(data){       
           alert("Excel Removed");    
		   location.reload();		
		   
       }
      });
    });
});
</script>


<script>
$(document).ready(function(){ //fungsi check file excel jika exist.
 $('#load_data').click(function(){     	
	var n = $('.detail tr').length ;	// banyak nya detail untuk dihapus seblm upload.					
		$.ajax({
		   url:"<?php 
				    $company_cut =substr($this->session->userdata('short'),0,-1);
				    $id_dept =$this->session->userdata('id_dept');
				    echo base_url('asset/uploads/'.$company_cut."/".$id_dept.'/data_part.csv'); 
				?>",
			type:'HEAD',
			error: function()
			{
				alert('Nothing csv File');
			},
			success: function()
			{
				for(var row = 0; row < n   ; row++)
				{				  				   
				   var tr =  $(this).parent().parent();  		          
				   var get_iddet =$('.txtid').eq(row).val();						
		           var buff_iddet =$('.buffarray_iddet').val()					  				   				   				  
				   if (buff_iddet == ""  ) { 	
					$('.buffarray_iddet').val(get_iddet);
				   }else{							       
			        $('.buffarray_iddet').val($('.buffarray_iddet').val() + "," + get_iddet );								
				   } 				   				   
				}
				
				for(var row = 0; row < n +1  ; row++)
				{					
				   $('.detail tr').remove(row); //remove lengt of tr detail
				}
							
				load_fromexcel_data_part(); // function uploa dexcel 											
			}
		});
 	 });	
});
</script>

<script>
function load_fromexcel_data_part() //fungsi load
{   
  $.ajax({
   url:"<?php 
          //$company_cut =substr($this->session->userdata('short'),0,-1);
		  $company_cut =substr($this->session->userdata('short'),0,-1);
		  $id_dept =$this->session->userdata('id_dept');
          echo base_url('asset/uploads/'.$company_cut."/".$id_dept.'/data_part.csv'); 
		?>",
   dataType:"text",
   success:function(data)
   {		
	//---------------------------------------------------     
    var part_data = data.split(/\r?\n|\r/);	  
	//---------------------------------------------------
	var strnopp =$('#txtnopp').val()
    for(var count = 0;count< part_data.length - 1; count++)
    {
	     	 
     var cell_data = part_data[count].split(",");	 	
		 
	 var tr = '<tr id="rowdetail">'+
'<td class="no" align="center">'+ cell_data[0] +'</td>'+'<td><input type="text" id="txtidmasdetid" name="txtidmasdet[]"  value="'+strnopp+'" hidden="hide" /><input type="text" name="txtiddet[]" class="txtid" value="" hidden="hide" /> <input type="text" name="flagstat[]"  value="1" hidden="hide" /><input type="text" maxlength="70"  name="txtdesc[]" value='+ cell_data[1] +' placeholder="Description" size="24" class="form-control txtdesc" /></td>'+'<td><input type="text" maxlength="70"  name="txtspecs[]" value='+ cell_data[2] +'  placeholder="Specs" size="30" class="form-control txtspecs" /></td>'+'<td><input maxlength="6" id="txtqty"  name="txtqty[]" value='+ cell_data[6] +' type="text" placeholder="0" size="7" onkeypress="return isNumberKey(event)" class="form-control quantity"  /></td>'+                                                		                                  
'<td><input maxlength="14" name="txtprice[]" value='+ numeral(cell_data[7]).format('0,0.00')  +' type="text" placeholder="0.00" size="30" onkeypress="return isNumberKey(event)" class="form-control price"/> </td>'+	
'<td><input  maxlength="70"   name="txtamount[]" value='+ numeral(cell_data[8]).format('0,0.00') +' placeholder="0.00"  size="35"  class="form-control amount" readonly="readonly"/></td>'+' <td width="7%"><select  name="cbotax[]" class="form-control txtstat"><option value='+ cell_data[9] +'>'+ cell_data[9] +'</option><option value="NONE">NONE</option><option value="PPN">PPN</option></select></td> '+'<td width="10%"><select  name="cbotaxpph[]" class="form-control txtstatpph" ><option value='+ cell_data[10] +'>'+ cell_data[10] +'</option><option value="NONE">NONE</option><option value="PPH2%">PPH 23 2%</option><option value="PPH4%">PPH 23 4%</option><option value="PPH15%">PPH 23 15%</option></select></td> '+'<td><input maxlength="70" name="txttaxcharge[]"  placeholder="0.00"  size="30"  class="form-control taxcharge" readonly="readonly" /></td>'+'<td><input  maxlength="70" name="txttaxchargepph[]"  placeholder="0.00"  size="30"  class="form-control taxchargepph" readonly="readonly" /></td>'+'<td>  <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button></td>'+
'</tr>';    
	$('.detail').append(tr);   	 	
	
	//------call total detail....	
	       var tr =  $(this).parent().parent();	
		  // numeral().unformat($('.taxcharge').eq(i).val())	  
		   var amt =  numeral().unformat(cell_data[8]);		   
		   var stattaxppn =  tr.find(".txtstat").val();
		   var stattaxpph =  tr.find(".txtstatpph").val();
				  
		   if ( cell_data[9] == "PPN") { //ppn 10%			   		       		   	
		       var ttlppn= amt * (10/100)	;		  			  
			   $('.taxcharge').eq(count).val(numeral(ttlppn).format('0,0.00'))	;							
		   }		   		  		  
		   
		    if ( cell_data[9] == "NONE") { //none			   				
				$('.taxcharge').eq(count).val('0,0.00')	;
		    }		
		   
		  
					 
		   if (cell_data[10] =="PPH2%") { //pph 2%			   
		       var ttlpph = amt * (2/100);	   
			   $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));								
		   }else{
			    if (cell_data[10] =="PPH4%") { //pph 2%			   
		      	    var ttlpph = amt * (4/100);
					 $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));
				}else{	
					if (cell_data[10] =="PPH15%") { //pph 2%			   
		      	    	var ttlpph = amt * (15/100);
						 $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));
					}else{	
			   			 $('.taxchargepph').eq(count).val('0,0.00');
						 $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));
					}
				}
		   }	 			
	      
		   total(); //call total
	  //--------------------------	
	
    }//endfor			
   }   
  });
};

</script>

<script>
$(document).ready(function(){ //fungsi check file excel jika exist.
 $('#btnsavebtn').click(function sendedit(){     	
	  var url = "<?php echo base_url('epp_form/c_create_pp/do_edit_pp'); ?>";		 
	  var formData = new FormData(document.getElementById('form-upload'));	
	  var m =  $('#ViewloadinglModal');	  		       
	  
			  if ($('#cbotype_pur').val()==""){
				   alert("Purchase Type  Must be Required");
			  }else{
					 if ($('#cbovendor').val()==""){	
						alert("Vendor Must be Required");	
				   }else{
					   	 if ($('#txtterm').val()==""){	
				   			alert("Term Of Payment Must be Required");				   					
					   	}else{
							if ($('#cbocur').val()==""){	
								alert("curency Must be Required");				   
					  		 }else{
								 if ($('#cbocoa').val()==""){	
									alert("coa Must be Required");				   
								 }else{		
								 	 if ($('#txtremarks').val()==""){	
										alert("Remark Must be Required");	
										$('#txtremarks').focus();			   
								 	}else{		
								 		 //cek validation detail....
										var jmlbaris=parseInt($('.remove').length);	
										for(var i=0;i<jmlbaris;++i){
										   var txtdesc =$('.txtdesc').eq(i).val();
										   var txtspecs =$('.txtspecs').eq(i).val();
										   var txtporeff =$('.txtporeff').eq(i).val();
										   var cbocoa =$('.cbocoa_search_data').eq(i).val();
										   var cboact =$('.cboact_search_data').eq(i).val();
										   var qty =$('.quantity').eq(i).val();
										   var price =$('.price').eq(i).val();
										  
										   if(txtdesc == ""){
											  $('.txtdesc').eq(i).focus();  
											  alert("Desc Data Must be Required");
											  return;
										   }else{
											 if(txtspecs == ""){ 
												$('.txtspecs').eq(i).focus();
												alert("Specs Data Must be Required");
												return;
											 }else{												  
												 if(cbocoa == ""){ 
													$('.cbocoa_search_data').eq(i).focus();
													alert("COA Data Must be Required");
													return;
												 }else{
													if(cboact == ""){ 
													  $('.cboact_search_data').eq(i).focus();
													  alert("Actifity No Data Must  Required");
													  return;
													}else{
													   if(qty == ""){ 
														 $('.quantity').eq(i).focus();
														 alert("Qty Data Must be Required");
														 return;
													   }else{
														  if(price == ""){ 
															 $('.price').eq(i).focus();
															 alert("Price Data Must  Required");
															 return;														 														 														  }
														}
													 }
												  }													
											   }
											}
										}//endfor	
										   
											 											
									//after validation--------------------------------------		
																																								                                           function callBeforeAjax(){
												 m.modal('show');
											 }											
										   
										   $.ajax({
											   beforeSend: callBeforeAjax,	
											   url:url,      
											   method:'POST',
											   enctype: "multipart/form-data", 					
											   data: formData,										
											   processData: true,															
											   async: false,
											   processData: false,
											   contentType: false,		
											   cache : false,	
											   success:function(data){
												  setTimeout(function(){  m.modal('hide'); }, 3000);
											   }, 
											   																
											   complete:function(data) { 
												  alert("update Successfully"); 
												  location.href = "<?php echo base_url('epp_form/c_create_pp'); ?>"  	               	
											   }	
											    
											});  
																				
									 }
								  }
							 }
						}
				     }
			    }//endif last						 					
					
 	 });	
});
</script>

<script>

$(document).ready(function () {
	$(function () {
		$("#cbovendor_search").autocomplete({
			minLength:0,
			delay:0,
			source: function(request, response) {	
			var str_url = '<?php echo site_url('epp_form/C_create_pp/suggest_vendor'); ?>';					
			var str_vendor =$("#cbovendor_search").val();																																		
				$.ajax({ 
					url: str_url ,												
					data: {vendor:str_vendor},																																		
					dataType: "json",
					type: "POST",										  					
					success: function(data){						
						response(data);	
						$(".ui-autocomplete").css("z-index", "2147483647");																																																						
					}						
				});
			},	
				select:function(event, ui){						   		  
				   $('#cbovendor').val(ui.item.idvendor);					  
				}							                  				                     
		});		
			
	});

});	  



 </script> 
 
 
 <script>

/*$('body').delegate('.cbocoa_search_data','focusin',function() {	   
	        var tr =  $(this).parent().parent();	
		    tr.find(".cbocoa_search_data").autocomplete({			 
			
			
			minLength:0,
			delay:0,			
			source: function(request, response) {				
			var str_url = '<?php //echo site_url('epp_form/C_create_pp/suggest_coa'); ?>';					
			var str_coa = tr.find('.cbocoa_search_data').val()					
				$.ajax({ 
					url: str_url ,												
					data: {coa:str_coa},																																		
					dataType: "json",
					type: "POST",										  					
					success: function(data){						
						response(data);	
						//alert(str_coa);
						$(".ui-autocomplete").css("z-index", "2147483647");																																																						
					}						
				});
			},	
				select:function(event, ui){					   				  				 
				   tr.find('.cbocoa').val(ui.item.idcoa);					  
				}						                  				                     
		});		
			
}); */


$('body').delegate('.cbocoa_search','blur',function(){  	
  var coa = $('#cbocoa').val() ;
  var coasearch = $('#cbocoa_search').val() ;
  
	if(coa == "" ) {
	  $('#cbocoa_search').val("") ;
	  $('#cbocoa_search').focus();
	}
	
	if (coasearch==""){
		$('#cbocoa').val("");
		$('#cbocoa_search').focus();
	}
	  
		   		
}); 	

 </script> 
 
 <script>
  $('body').delegate('.cboact_search_data','focusin','change',function() { //using focusin
	        var tr =  $(this).parent().parent();
			var str_coa = tr.find('.cbocoa_search_data').val();
			
		    tr.find(".cboact_search_data").autocomplete({
			
			minLength:0,
			delay:0,
			source: function(request, response) {
			var str_url = '<?php echo site_url('epp_form/C_create_pp/suggest_act'); ?>';
			var str_act = tr.find('.cboact_search_data').val();
			//var totalrow=parseInt($('.remove').length);
		        /* for(var i=0;i<totalrow;++i){
		            var str_coa =$('.cbocoa_search_data').eq(i).val();
						if (str_coa=="") {
							$('.cboact_search_data').eq(i).val("")
							alert("coa must be required");
							$('.cbocoa_search_data').eq(i).focus()
							return;	
						}
				   }*/	
			
				$.ajax({
					url: str_url ,
					data: {act:str_act},
					dataType: "json",
					type: "POST",
					success: function(data){
						response(data);
						$(".ui-autocomplete").css("z-index", "2147483647");
						
					}
				});
			},
				select:function(event, ui){
					var thisRow = $(this).parents("tr");
					thisRow.find('.cbocoa_search_data').val(ui.item.nomorcoa);
				}
		});

    }); //bodydelagite



/*$('body').delegate('.cbocoa_search_data','blur',function(){
   var tr =  $(this).parent().parent();
   var coa = tr.find('.cbocoa').val() ;
   var coasearch = tr.find('cbocoa_search_data').val() ;

	if(coa=="") {
	  tr.find('.cbocoa_search_data').val("") ;
	  tr.find('.cbocoa_search_data').focus();
	}

	if (coasearch==""){
		tr.find('.cbocoa').val("");
		tr.find('.cbocoa_search_data').focus();
	}
}); */
 /*$(function() {
    $(".chosenselect").chosen();
  }); */
 </script>

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_fail') ;?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_insert_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_insert_fail');?>  
        </div>	
<?php } ?>


  <?php  echo form_open_multipart('epp_form/c_create_pp/multiple_submit',array('class'=>'form-multi','id' => 'form-upload',));  ?> 
  
    <div class="table-header btn-info">
                <?php echo " ".$header ;?> 
</div>
          

<table class="table borderless ">
             
          <?php	 foreach($tampil_pp as $row) {	 ?>                 
             <tr> 
             <td>              
                 <div class="form-group">
                   <label for="PP Number"  class="col-xs-3">PP Number</label>
                    <div class="col-xs-6">
                    <input type="text" id="txtnopp" name="txtnopp" class="form-control" readonly="readonly" ="true" value="<?php echo $row->id_master ?>" on />
                    </div>
                </div>
               </td> 
               
               <td>  
                 <div class="form-group">
                   <label for="Submission By"  class="col-xs-3" style="color:#900">Submission By *</label>
                   <div class="col-xs-5">
                     <input type="text" id="txtsub" name="txtsub" class="form-control" value="<?php echo $this->session->userdata('name'); ?>" readonly="readonly" />
                   </div>
                </div>
               </td>                		                                                        
           </tr>
           <tr>
           <td>   
              <div class="form-group">
               <label for="Date Submission"  class="col-xs-3">Date Submission</label>
                <div class="col-xs-7">
                 <input type="text" id="txtdate" name="txtdate" class="form-control" readonly value="<?php echo date("d-m-Y") ?>" />
               
                </div>
              </div>                
             </td>   
             <td>                                
               <div class="form-group">
                 <label for="attach"  class="col-xs-3" style="color:#900">Upload Quo(*.PDF Only)</label>                
                   <div class="col-xs-5">                         
                     <input type="file" id="txtupload" name="userfile" class="btn btn-success" value="<?php echo $row->attach_quo ?>" accept=".pdf" />  
                 </div>
               </div>      
            
         </td>
           </tr>             
             <tr>   
               <td>
				<div class="form-group">
                  <label for="Company"  class="col-xs-3">Company</label>
                    <div class="col-xs-8">
                     <input type="text" id="txtcompany" name="txtcompany" class="form-control" value="<?php echo $this->session->userdata('company');  ?>"  readonly />
                    </div>
                </div>    
               </td>               
               <td>   
               	 <div class="form-group">
                 <label for="Attachment Quo"  class="col-xs-3" style="color:#900">Attachment Quo * </label> 
                                				                    
                 <div class="col-xs-8">                          
                        <?php      				
						  If ($row->attach_quo != "")
						  {  						 						  
					    ?>	  		                       
						 <a href="<?php echo site_url($row->attach_quo); ?>"  >
                         <i id="imgexcel" class="fa fa-file-pdf-o" style="font-size:30px">
                           <label>Quotation Vendor</label>
                         </i>                           
                 		 </a>                  			       						 
                        <?php    				
						  }else{
							echo'No Attachment' ;	  
						  }
						?>                                             					
                   </div>      
                 </div>                                           
               </td>                
             </tr>             
         <tr>   
         	<td>
            <div class="form-group">
                <label for="Branch"  class="col-xs-3">Branch</label>
                <div class="col-xs-6">
                 <input type="text" id="txtbranch" name="txtbranch" class="form-control" value="<?php echo $this->session->userdata('name_branch'); ?>" readonly />
                </div>
            </div>    
           </td>
           
            <td>
                <div class="form-group">
                   <label for="Vendor Name"  class="col-xs-3" style="color:#900">Vendor Name *</label>
                   <div class="col-xs-8">
                   <!-- <input data-rel="tooltip" type="text"id="cbovendor_search" name="cbovendor_search" placeholder="ID / Vendor Name" title="Hello Put like : ID / Vendor Name  " data-placement="bottom" class="col-md-12 cbovendor_search" value="<?php //echo $row->vendor ?>"  /> -->
                    
                    <!-- <input id="cbovendor" name="cbovendor" value="<?php // echo $row->id_vendor ?>" hidden="hide" /> -->
                                         <select data-placeholder="Dealer Code | Vendor Name | ID" id="cbovendor" name="cbovendor" class="myselect" tabindex="2" style="width: 100%;">
		         <option value="<?php echo $row->id_vendor ;?>"><?php echo $row->vendor ?></option>  
	             <?php foreach($tampil_vendor as $row11){ ?>
               		 <option value="<?php echo $row11->id_vendor ;?>"><?php echo $row11->vendor ;?></option>
                 <?php } ?>
                     </select>

                  </div>
                </div>
                                
           </td> 
         </tr> 
          
           <tr>   
         	<td>
            <div class="form-group">
                <label for="Branch"  class="col-xs-3">Departement</label>
                <div class="col-xs-6">
                 <input type="text" id="txtdept" name="txtdept" class="form-control" value="<?php echo $this->session->userdata('dept'); ?>" readonly />
                </div>
            </div>    
           </td>
           
            <td>
              <div class="form-group">
                        <label for="Term Of Payment"  class="col-xs-3" style="color:#900">Term Of Payment *</label>
                        <div class="col-xs-5">
                         <input name="txtterm" type="text" class="form-control" id="txtterm" placeholder="T.O.P" onkeypress="return isNumberKey(event)" style="width:30%;" maxlength="3"  value="<?php echo $row->term_top ?>" />                                                
                        </div>
              </div>                                       
            </td>              
         </tr>                    
    
	<tr>   
       <td>    
        <div class="form-group">
            <label for="E-mail"  class="col-xs-3">E-mail</label>
            <div class="col-xs-7">
                 <input type="text" id="txtmail" name="txtmail" class="form-control" readonly="readonly" value="<?php echo $this->session->userdata('email'); ?>">
            </div>
        </div>
       </td>        
        <td>
         <div class="form-group">
            <label for="Currency"  class="col-xs-3" style="color:#900">Currency *</label>
            <div class="col-xs-6">
             <select id="cbocur" name="cbocur"  class="myselect" style="width: 100%;">
                     <option value="<?php echo $row->id_curr ;?>"><?php echo $row->currency ;?></option>
	             <?php foreach($tampil_curr as $row_curr){ ?>               
               		 <option value="<?php echo $row_curr->id_curr; ?>"><?php echo $row_curr->currency ;?></option>
                 <?php } ?>  
              </select>
            </div>
        </div>
            
      </td>                                                                  
    </tr>    
    	<tr>
   		<td>
          <div class="form-group">
            <label for="Currency"  class="col-xs-3" style="color:#900">Purchase Type</label>
            <div class="col-xs-6">
             <select id="cbotype_pur" name="cbotype_pur" class="select2" style="width: 100%;"
>
				 <option value="<?php echo $row->flag_purchase ;?>"><?php echo $row->type_purchase ;?></option>
	             <?php foreach($tampil_type_purchase as $row_pur){ ?>             
                     <?php if ( $row_pur->flag=="1") { ?>
   		       		      <option value="<?php echo $row_pur->flag ;?>" class="btn-info"><?php echo $row_pur->type_purchase ;?></option>
               		 <?php }else{ ?>
                    	   <?php if ( $row_pur->flag=="2") { ?>
                     	 	      <option value="<?php echo $row_pur->flag ;?>" class="btn-success"><?php echo $row_pur->type_purchase ;?></option>
                            <?php }else{ ?>
                  			       <?php if ( $row_pur->flag=="3") { ?>
                     					  <option value="<?php echo $row_pur->flag ;?>" class="btn-yellow"><?php echo $row_pur->type_purchase ;?></option>
                         	       <?php }else{ ?>
                     						<?php if ( $row_pur->flag=="4") { ?>
                     							 <option value="<?php echo $row_pur->flag ;?>" class="btn-purple"><?php echo $row_pur->type_purchase ;?></option>
                                             <?php }else{ ?>  
                                             	 <option value="<?php echo $row_pur->flag ;?>" class="btn-danger"><?php echo $row_pur->type_purchase ;?></option>
                                              <?php } ?>   
								     <?php } ?>   
   		       		   	     <?php } ?>
                       <?php } ?>   
                 <?php } ?>  
              </select>
            </div>
        </div>    
        </td>         
         <td rowspan="2" >
        	<label for="Reject Reason"  class="col-xs-3">Reject Reason </label>
          <div class="col-xs-7">
          <textarea name="txtreject" class="form-control" id="txtreject" readonly="readonly"></textarea>
          </div>
          </td>                         
    </tr>    
    <tr>
     <?php if($row->reason_reject != "") { ?>
        <td style="background:#F96064">
        <label for="Reject Reason"  class="col-xs-3" style="color:#900;">Reject Reason </label>
          <div class="col-xs-7">
          <textarea name="txtreject" class="form-control" id="txtreject" readonly="readonly"><?php echo $row->reason_reject ?></textarea>
          </div>
        </td>
     <?php }else{ ?>  
        <?php } ?>                    
        <td>
          <div class="form-group">
              <label for="Remarks"  class="col-xs-3" style="color:#900">Remarks *</label>
              <div class="col-xs-5">
              <textarea name="txtremarks" class="form-control" id="txtremarks"><?php echo $row->remarks ?></textarea>
              </div>
          </div>
        </td>   
    </tr> 
      <?php } ?> 				 			 				 			 
</table>
   
<div class="table-header btn-info">
       Purchase detail
</div>
 	<br />
    
         <table class="">
       	   <tr>            
            	<td>
                 <input id="fileSelect" name="fileSelect" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" class="btn btn-info" />
                </td>
                <td>&nbsp;</td>
              <td hidden id="hexcel">
              <div style="margin-left:5px; border:thin; border-color: #666;" >
                                                                 
                  <a href="<?php    
				    $company_cut =substr($this->session->userdata('short'),0,-1);
				    $id_dept =$this->session->userdata('id_dept');
				    echo site_url('asset/uploads/'.$company_cut."/".$id_dept.'/data_part.csv');                                  
                    ?>"  class="btn btn-success" >
                      <i id="imgexcel" class="fa fa-file-excel-o" style="font-size:20px" ></i>
                  </a>
                </div>
             </td> 
                 <td hidden id="hdel">                   
                  <div style="margin-left:5px; border:thin; border-color: #666;" >
                   <a href="#" class="deleteexcel btn btn-success"  >
                	  <i id="imgdel" class="fa fa-trash-o" style="font-size:20px" ></i>
                   </a>                   
               </div>     
             </td>
             <td>&nbsp;</td>
                <td>
                	   <div align="center">
                                                  <button type="button" name="load_data" id="load_data" class="btn btn-app btn-info btn-sm radius-6 "> <i class="fa fa-pencil-square-o bigger-160"></i>Load</button>

                       </div>
                </td>
           </tr>
            <tr>           	             
            <div id="prog" class="progress" style="display:none;">
                          <div class="progress-bar progress-bar-striped active" role="progressbar"aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:100%"  >
                100% upload Successfully
                         </div>
            </div>            
           </tr>                                   
         </table>
           
       <input  type="text" name="txtiddet_buff"  readonly="readonly" class ="buffarray_iddet" width="70"  hidden="hide"  />
       
        <br />
                       <table class="table  table-bordered " width="100%">
                      <thead align="center" >      
                <tr align="center" style="font-weight:bold">                                            
           		    <td >No</td>
                    <td >Description</td>
                    <td >Specs</td>
                   <!--  <td hidden="hide" >Invoice No</td>-->
                   <!--  <td hidden="hide">Act No</td>-->
                   <!--  <td hidden="hide" >COA </td>-->
                    <td >Qty</td>                   
                    <td >Prices</td>                    
                    <td >Total Prices</td>                    
                    <td >Tax PPN</td>
                    <td >Tax PPH</td>
                    <td >Charge PPN</td>
                    <td >Charge PPH</td>      
                    <td align="center"><strong><input type="button" id="add" Class="btn-sm radius-6 btn-info" value="+" /></td>
                 </tr>   
          </thead>    
                        
                  <tbody class="detail">    
                   <?php  foreach($tampil_det_pp as $row2  ){ $ino++ ; // buat  perulangan tampilan detail ?> 				
                      <tr>
                        <td class="no" align="center"><?php echo $ino ;?></td>
                        <td>        
                         <input type="text" id="txtidmasdetid" name="txtidmasdet[]" maxlength="25"  value="<?php echo $ppnumber  ?>"  hidden="hide"    />                                     
                        <input type="text" name="txtiddet[]" value="<?php echo $row2->id_detail ?>" class="txtid" hidden="hide"     />       
                        <input type="text" name="flagstat[]"  value="1" hidden="hide" />                 
                        <input type="text" maxlength="70" id="txtdesc"  name="txtdesc[]"  placeholder="Description" size="24" class="form-control txtdesc"  value="<?php echo $row2->desc ?>"/>
                        </td>
                        
                        <td>
                          <input type="text" maxlength="70"  name="txtspecs[]"  placeholder="Specs" size="30" class="form-control txtspecs" value="<?php echo $row2->spec ?>" />
                        </td>    
                         <!-- <td hidden="hide">
                          <input type="text" maxlength="70"  name="txtporeff[]"  placeholder="Invoice No" size="30" class="form-control txtporeff" value="<?php //echo $row2->po_reff ?>"  />
                        </td> -->
                        <!--  <td hidden="hide">
                         <div class="form-group">           
                           <div class="col-xs-11">
                              <input data-rel="tooltip" type="text" name="cboact_search[]" placeholder="Actifity No" title="<script>Hello Put like : Actifity number </script>" data-placement="bottom" class="col-md-12 cboact_search_data" size="30" maxlength="40" value="<?php //echo $row2->no_act ?>"  />
                           
                            </div>
                        </div>       
                        </td>-->
                         <!--<td>
                         <div class="form-group">           
                           <div class="col-xs-11">
                                 <input data-rel="tooltip" type="text" name="cbocoa_search[]" placeholder="C.O.A Number /C.O.A Desc" title="Hello Put like : Coa Number /C.O.A Desc " data-placement="bottom" class="col-md-12 cbocoa_search_data" size="40" maxlength="40" value="<?php //echo $row2->coa ?>"  />
                            </div>
                        </div>   
                       </td>  -->                   
                        <td>
                        <input maxlength="6"  name="txtqty[]" type="text" placeholder="0" size="12" onkeypress="return isNumberKey(event)" class="form-control quantity" id="txtqty" value="<?php echo number_format($row2->qty) ?>" />                          
                        </td>                                                		                                  
                        <td>
                           <input maxlength="14"  name="txtprice[]" type="text" placeholder="0.00" size="20" onkeypress="return isNumberKey(event)" class="form-control price" id="txtprice" value="<?php echo number_format($row2->harga,2) ?>" /> 
                        </td>
                        
                       <td>
                  			<input  maxlength="70" name="txtamount[]"  placeholder="0.00"  size="30"  class="form-control amount" readonly="readonly" value="<?php echo number_format($row2->total,2) ?>" />                       </td>                       
                        <td width="7%">
                         <select  name="cbotax[]" class="form-control txtstat">
                          <option value =" <?php echo $row2->tax_type ?>"> <?php echo $row2->tax_type ?></option>
                            <option value="NONE">NONE</option>    
                            <option value="PPN">PPN</option>                        
                         </select>
                        </td> 
                         <td width="9%">
                         <select  name="cbotaxpph[]" class="form-control txtstatpph" >
                            <option value =" <?php echo $row2->tax_typepph ?>"> <?php echo $row2->tax_typepph ?></option>
                            <option value="NONE">NONE</option>                              
                            <option value="PPH2%">PPH2%</option>   
                            <option value="PPH4%">PPH4%</option>   
                            <option value="PPH15%">PPH15%</option>      
                         </select>
                        </td> 
                                                            	
                        </td>
                         <td>
                         <input  maxlength="70" name="txttaxcharge[]"  placeholder="0.00"  size="30"  class="form-control taxcharge" readonly="readonly" value="<?php echo number_format($row2->tax_detail,2) ?>" />                                                
                        </td>                        
                        <td>
                  		<input  maxlength="70" name="txttaxchargepph[]"  placeholder="0.00"  size="30"  class="form-control taxchargepph" readonly="readonly" value="<?php echo number_format($row2->tax_detailpph,2) ?>" />
                       </td>  
                        <td>
                        <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button>
                        </td>
                       
                     </tr>
                    <?php } ?>
                  </tbody> 
              </table>                                                                                                                                
          </div>   
            <table class="btn-warning" hidden="hide">
           	  <tr>   
                <td width="72%"><label>No Total Tax :</label></td>                                            
                  <td align="center"  style="padding:10px"  >
                       <input type="checkbox" id="chkppn" name="chkppn" class="ppnnot ace ace-checkbox-1 col-xs-3"/>
                       <span class="lbl"></span>      
               	  </td>
              </tr>     
            </table>     
            <table class="btn-danger" width="40%" style=" color:#FFF;"  >
            <tr>              
               <td colspan="0" width="" >       
                   <label >  Sub Total :   </label>
               </td>
               <td>   
                 <input  maxlength="70" id="result" name="result" class="form-control total" readonly="readonly" style="color:#900" value="<?php echo number_format($row->gran_total,2 ) ?>" />     
               </td>
               
            </tr>
             <tr>
               <td colspan="0" width="" >       
                    <label>  Total Tax PPN:   </label>
               </td>
               <td>   
                 <input  maxlength="70" id="txtppn" name="txtppn" class="form-control ppncalc" readonly="readonly" style="color:#900" value="<?php echo number_format($row->ppn,2) ?>"  />     
               </td>
             
            </tr>
            
            <tr>
               <td colspan="0" width="" >       
                    <label>  Total Tax PPH:   </label>
               </td>
               <td>   
                 <input  maxlength="70" id="txtpph" name="txtpph" class="form-control pphcalc" readonly="readonly" style="color:#900" value="<?php echo number_format($row->pph,2 ) ?>"  />     
               </td>
             
            </tr>
            
             <tr>
               <td colspan="0" width="31%" >       
                  <label> Grand Total + TAX  </label>
               </td>
               <td>   
                 <input  maxlength="70" id="gpppn" name="gpppn" class="form-control grandppn" readonly="readonly" style="color:#900" value="<?php echo number_format($row->gran_totalppn,2 ) ?>" />     
               </td>
            </tr>
         </table>   
           
           
                    <br />
<table>
                     <tr>
                       <td>
                           <button class="btn btn-app btn-info btn-xs radius-4 cbtnsavebtn" type="button" id="btnsavebtn" name="btnsavebtn"  value ="btnsavebtn"  >
            <i class="ace-icon fa fa-book sm-160"></i>
            Submit
		</button>    
                    </td>                 
                   
                    <td>&nbsp;  </td>
                    <td>                                      			
                     <a href="<?php echo base_url('epp_form/c_create_pp');?>" style="text-decoration:none;" class="btn btn-app btn-success btn-xs radius-4 btnback"> <i class="ace-icon fa fa-exchange bigger-160"></i>Back</a>                          </td>
                  </tr>
               </table>                                                	
  
  
<p>
  <?php form_close(); ?> 
                  
                  
   <?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>    
                  
  <!-- Modal -->
</p>
        <div class="modal fade" id="ViewloadinglModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">                                        
                    <div class="modal-body" align="center">
                            <img src="<?php echo base_url('assets/img/loading_spinner.gif') ; ?>" />
                    </div> 
                    
                </div>
            </div>
        </div>
<!-- Modal -->

<!-- combobox autocomplete -->
<script type="text/javascript">
      $(".myselect").select2();
</script>   
<!-- combobox autocomplete end --> 