<style>
   th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
</style>	

<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>


      <div class="modal fade" id="ViewloadinglModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">                
                    <div class="modal-body-load-spiner" align="center">
                            <img src="<?php echo base_url('assets/img/loading_spinner.gif') ; ?>" />
                    </div> 
                    
                </div>
            </div>
        </div>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>
      
<?php   	
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>      
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>                        
                <?php  echo "Send Approval Successfully"	?>  
		  </div>		
<?php } ?>

<?php   	
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>                
        <div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>                        
			<?php  echo "Send Approval failed"	?>  
        </div>		      
<?php } ?>	
  
  
                
 <?php  echo form_open('epp_form/c_create_pp/multiple_submit',array('id' => 'form-tablepp'));  ?>     
 
    <div>     
    <table>
       <tr class="col-md-9">
        <!-- <td>         
        <a href="<?php echo site_url('epp_form/c_create_pp/view_form_pp'); ?>" class="btn btn-app btn-info btn-xs radius-4 caddnew"> <i class="ace-icon fa fa-book bigger-160"></i>AddNew</a>
        </td> -->
        <td>
        
        <button class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="btnedit" name="btnedit"  value ="btnedit"  disabled="disabled" >
            <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
            Amend
		</button> 
		</td>
        <!-- <td>
         <button class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="button" id="btndel" name="btndel"  value ="btndel"  disabled="disabled" >
            <i class="ace-icon fa fa-ban bigger-160"></i>
            Cancel
		</button> 
        
       </td> -->
         
       <td>    
       
        <button class="btn btn-app btn-yellow btn-xs radius-4 btn_send" type="button" id="btnsendtab" name="btnsendtab"  value ="btnsendtab"  disabled="disabled" >
            <i class="ace-icon fa fa-paper-plane bigger-160"></i>
            Send 
		</button> 
        </td>
       </tr>
     </table>         
 </div>     
<br />
 
					
	 <div class="table-header btn-info"> <?php echo " ".$header ;?>   </div>    
		
		<h3>Legend : <span class="badge badge-danger"> &nbsp;  &nbsp;  &nbsp;  &nbsp; </span> = Rejected </h3> 
		
		<div class="form-group col-xs-6">
			<p>
				<div class="col-xs-10">
					<div class="form-inline ">
						<div class="form-group">Date start</div>

						<div class="form-group">
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>

								<input type="text" class="form-control input-daterange" id="start_date" name="start_date"
									data-date-format="dd-mm-yyyy" autocomplete="off">
							</div> <!-- /.input-group datep -->
						</div>

						<div class="form-group ">
							<label for="From" class="col-xs-1 control-label">To</label>
						</div>

						<div class="form-group">
							<div class="input-group date">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="text" class="form-control input-daterange" id="end_date" name="end_date"
									data-date-format="dd-mm-yyyy" autocomplete="off">
							</div>
						</div>

					</div>
				</div>
		</div>
		<table id="myTable" cellpadding="0" cellspacing="0" width="100%"  class="table table-striped table-bordered table-hover">
	      <thead align="center" class="text-warning" > 
            <tr>  
                <th style="text-align:center" class="col-name">           
                  <label class="pos-rel">
                    <input type="checkbox" id="checkAll" class="ace" />
                    <span class="lbl"></span>
                  </label>
				</th>	       		          
                <th >PR No </th>
				<th >Reject </th>
                <th >Company</th>			
                <th >Vendor</th>			                	
                <th >Date Created</th>      
                <th >Type</th> 
                <th >Curr</th>                
                <th >Total Price</th>
                <th >Total + TAX</th>
                <th >T.O.P</th>
               <!-- <th >Submission</th>	-->			
                <th>Detail</th>		
				<!-- <th>Attach Quo</th>	
				<th>Attach Quo Purchase</th>	 -->
               </tr>
             </thead>    
         </table> 
    
</div>
   
  

<!-- Modal -->
        <div class="modal fade" id="ViewDetailModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Purchase</h4>
                    </div>
                    
                    <div class="modal-body">                      
                    </div> 
                    
                                     
                </div>
            </div>
        </div>
<!-- Modal -->  	

<!-- Content Popup -->
<div id="dialog" style="display: none;">
    <div>
        <iframe id="frame" width="750px" height="550px"></iframe>
    </div>
</div>
<!-- Content Popup -->

<!-- Modal Buat Reject -->

<div class="modal fade" id="myModal-reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Remarks For Reject Status</h4>
                    </div>
                    
                    <div class="modal-body-reject">
                       <table width="90%" class="table-condensed" align="center">
                            <tr> 
                                <td> <label for="PR Number"  class="col-xs-8">PP Number</label></td> 
                             
                                  <td>
                                   <div class="col-xs-12">
                                    <input type="text" id="txtppno" name="txtppno" class="form-control" readonly>
                                   </div> 
                                  </td>                                  
                             </tr>
                             <tr> 
                                <td ><label for="Reject Reason"  class="col-xs-9">Reject Reason</label></td> 
                                <td>
                                  <div class="col-xs-12">
                                    <textarea id="txtreject" name="txtreject"  class="form-control"></textarea>
                                  </div>  
                                </td>    
                             </tr>
                             <tr> 
                                <td></td> 
                                <td>
                                   <div class="col-xs-12">
                                   
                                   </div>        
                                </td>    
                             </tr>
                            </table> 
           
                    </div> 
                    
                     <div class="modal-footer">
                        <button class="btn btn-app btn-danger btn-xs radius-4 btnref btn-clean btnreject" type="button" 
						id="btnreject" name="btnreject"  value ="btnreject" onClick="prosesReject(this)">
            <i class="ace-icon fa fa-check-square-o bigger-160"></i>
            Reject
		</button> 
                  <!---   <input type="submit" name="btnreject" value=" Reject" class="btn btn-app btn-danger btn-xs radius-8"/> -->
                     <button type="button" class="btn btn-app btn-primary btn-xs radius-8 btn-xs radius-8" data-dismiss="modal">
                     <i class="ace-icon glyphicon glyphicon-remove  bigger-160"></i>
                     Close</button>                       
                    </div>                  
                </div>
            </div>           
        </div>
    
<!-- Modal Buat Reject -->  

<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.reject',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("epp_form/c_create_pp/get_info_reject"); ?>';
		
		$("#myModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
				$('#txtreject').val(msg)
			}
						
		});				
		
   });	
});


</script>

<script>
	
//check all--------------------------
$('#checkAll').change(function(){	
		var table = $('#myTable').DataTable();
		var cells = table.cells( ).nodes();
		$( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));			
		
});  


		

//aktif edit--------------------------------------------------------  
var counterCheckededit = 0;
	$('body').on('change', 'input[type="checkbox"]', function() { 
		   this.checked ? counterCheckededit++ : counterCheckededit--;
		   if($('#checkAll').not(':checked').length){ 
		       counterCheckededit == 1 ? $('#btnedit').prop("disabled", false): $('#btnedit').prop("disabled", true);
		   }
	 });	


//end---------------------------------------------------------------


//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterChecked++ : counterChecked--;
	counterChecked > 0 ? $('#btndel').prop("disabled", false): $('#btndel').prop("disabled", true);

});
//--------------------------------------------------------------------


//aktif button send email--------------------------------------------------------
var counterCheckesendeamail = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckesendeamail++ : counterCheckesendeamail--;
	if($('#checkAll').not(':checked').length){ 
	   counterCheckesendeamail == 1 ? $('#btnsendtab').prop("disabled", false): $('#btnsendtab').prop("disabled", true);
	}
});
//end---------------------------------------------------------------



</script>

<script>
//get data from control to modal popup.
$(function(){
  $(document).on('click','.detail',function(e){
		var req_id = $(this).attr('req_id');  
		var url = '<?php echo site_url("epp_form/c_create_pp/get_idtrans_modal"); ?>';
		
		$("#ViewDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_pp:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		}); 			
		
   });	
});
</script>

<script>
$(function(){
    $(document).on('click','.btn_delete',function(e){
	  
	  if (confirm('Are you sure you want to cancel?'))
	  {
		  var url = "<?php echo base_url('epp_form/c_create_pp/do_delete_pp'); ?>";		 
		  var formData = new FormData(document.getElementById('form-tablepp'));	  		       
			  
		  $.ajax({
		   url:url,      
		   method:'POST',
		   //enctype: "multipart/form-data", 					
		   data: formData,										
		   processData: false,															
		   async: false,
		   processData: false,
		   contentType: false,		
		   cache : false,	
		   success:function(){ 	    
			   alert('Delete Successfully !!');   
			   location.reload();		  	               		  
		   }
		  }); 	
	
	  }
	  
	});  	 
});
</script>

<script>
$(function(){
    $(document).on('click','.btn_send',function(e){
	     var m =  $('#ViewloadinglModal');	
		 var url = "<?php echo base_url('epp_form/c_create_pp/send_flag_aproval'); ?>";		 
		 var formData = new FormData(document.getElementById('form-tablepp'));	  	
		 var conf = confirm("Are you sure Send this PR?");	       
		
																																													      
		  
		  if(conf == true) {

			function callBeforeAjax(){
			 m.modal('show');
	     	}

			$.ajax({
			   beforeSend: callBeforeAjax,	
			   url:url,      
			   method:'POST',				
			   data: formData,										
			   processData: false,															
			   async: false,
			   processData: false,
			   contentType: false,		
			   cache : false,	
			   success:function(data){
					   m.modal('hide');
			   }, 	    
		   
			   complete:function(data) {  
				   alert('Send approval Successfully !!');   
				   location.reload();		  	               		  
			   }
			 
		  }); 
		  }
		  	
	  
	});  	 
});
</script>



<script type="text/javascript">

$(document).ready(function(){ 
	$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data('no'); //
	
		function fetch_data(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			"lengthChange": false, //hide  Entries dropdown
			
				"ajax":{
				    url:  "<?php echo base_url('epp_form/c_create_pp/cath_data_fordatatables') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
					
					
				 },

				 "columnDefs": [ {
						 "targets": [0,2,9,10,11], /* column index */
				          "orderable": false, /* true or false */		
					} ],
			 });
		  }
	
	//proses change date-----------------------------------------------
		$('#start_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
		$('#end_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);		
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	
 
 
 
 //onblur date-------------------------------------------------------------------------
 
 		// $('#start_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
		
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		  fetch_data('no', start_date, end_date);
		// 	} 
			
		// });
 
 
 	
		// $('#end_date').blur(function(){
		// 	var start_date =    $('#start_date').val();
		// 	var end_date   =    $('#end_date').val();
			
		// 	if(start_date == '' || end_date == ''){
		// 		$('#myTable').DataTable().destroy();
		// 		 fetch_data('no', start_date, end_date);
		// 	} 
		// });
 //end onblur-------------------------------------		
	
	
	
	
	
});

// $(document).ready(function(){ 
// 	$("#myTable").dataTable({
// 		"lengthMenu": [[10, 25, 50, 100],[10, 25, 50, 100]],
// 		autoWidth : true,
// 		processing: true,
// 		serverSide: true,
// 		"scrollY": 250,
// 		"scrollX": true,
		
		
// 		ajax: {
// 		  url: "<?php echo base_url('epp_form/c_create_pp/cath_data_fordatatables') ?>",
// 		  type:'POST',
// 		},
		
// 		"columnDefs": [ {
// 			 "targets": [0,9,10], /* column index */
//              "orderable": false, /* true or false */
// 			 // "targets": [1,2], /* column index */
// 		} ],
		
        
// 		//----------------------------------end
		
// 		//incrase Width Column ----------------------------------
// 		/* "columnDefs": [{
// 						 "width": "20%", "targets": 1,
// 						 "width": "20%", "targets": 2,
// 						 "width": "15%", "targets": 3
// 					  }],
// 		  */					
// 		//----------------------------------end
		
// 	});
// });	

</script>




<script type="text/javascript">

	function reject(e) {
		var val = $(e).attr('req_id');  

		$('#myModal-reject').modal('show');
		$('#txtppno').val(val);
		$('#txtreject').val("");
	}

	function prosesReject(e) {
		var id_master = $('#txtppno').val();
		var reject = $('#txtreject').val();

		var url = '<?php echo site_url("epp_form/c_create_pp/reject"); ?>'; 
		var conf = confirm("Are you sure to Reject this?");


		if(conf == true) {
			$.ajax({			
				type: 'POST',
				url: url,
				dataType: "json",
				data: {id_master: id_master, reject: reject},
				beforeSend: function() {
					$(e).prop('disabled', true);	
				},
				success: function (msg) {
					
					console.log(msg)
					
					if(msg == "Success") {
						alert("Success Reject!");
						$(e).prop('disabled', false);
						$('#myModal-reject').modal('hide');
						location.reload()
					} else {
						alert("Failed Reject!"+ " "+msg);
						$(e).prop('disabled', false);
					}
				}
						
			});
		}
		 	
	}

	function PDFPopup(e) {
		var url = $(e).attr('req_id');
		$("#dialog").dialog({
			width: 'auto',
			height: 'auto',
			resize: 'auto',
			autoResize: true
		});
		$("#frame").attr("src", url + "#toolbar=0");
        
    };

/*$(function(){
    $(document).on('click','.btnclose',function(e){
	 location.reload();	
	});
}) ; */	
</script>