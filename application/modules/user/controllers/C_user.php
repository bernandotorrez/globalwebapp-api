<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_user extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			error_reporting(0);
			$this->load->helper('url');
			$this->load->model('M_user');
			$this->load->library('pagination'); //call pagination library
			$this->load->helper('form');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_user->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}
	 	}
		
	public function index()
		{

		$data['user']=$this->M_user->get_all_user();
		$data['total']=$this->M_user->get_count_id();
		$data['group']=$this->M_user->get_all_group();
		$data['department']=$this->M_user->get_all_dept();
		$data['purchase']=$this->M_user->get_all_purchase();
		$data['company']=$this->M_user->get_all_company();
		$data['company2']=$this->M_user->get_all_company();
		$data['show_view'] = 'user/V_user';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function ajax_list()
	{
		$list = $this->M_user->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $qv_just_user_view) {
			$chk_idmaster ='<div align="center"><input id="checkuser" name="checkuser" type="checkbox" value='.$qv_just_user_view->id.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $qv_just_user_view->id;
			$row[] = $qv_just_user_view->name;
			$row[] = $qv_just_user_view->username;
			$row[] = $qv_just_user_view->name_grp;
			$row[] = $qv_just_user_view->company;
			$row[] = $qv_just_user_view->name_branch;
			$row[] = $qv_just_user_view->dept;
			$row[] = $qv_just_user_view->email;
			$row[] = $qv_just_user_view->date_create;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_user->count_all(),
						"recordsFiltered" => $this->M_user->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	public function reset_pass()
	{
		$data = array(
					'id' => $this->input->post('id'),
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('username')),
					);
		$this->M_user->update_user(array('id' => $this->input->post('id')), $data);
		echo 'Insert';
	}
	
	public function cruduser(){
		$this->load->view('formcruduser');
	}
	
	public function ajax_edit($id)
		{
			$data = $this->M_user->get_by_id($id);
			echo json_encode($data);
		}
		
	public function ajax_divisi()
    {
        $id = $this->input->post('id_divisi');
        $data['dept'] = $this->M_user->get_dept($id);
        $this->load->view('dept',$data);
    }
	function pos_branch($id_company)
	{
    	$query = $this->db->get_where('tbl_branches',array('id_company'=>$id_company));
    	$data = "<option value=''>- Select Branch -</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->branch_id."'>".$value->name_branch."</option>";
    	}
    	echo $data;
	}
	
	public function simpanupdatedatauser(){
		$sqlcekiduser = "select id from tbl_user where id ='".$_GET['id']."'";
		$qrycekiduser = $this->db->query($sqlcekiduser);
		$adacekiduser = $qrycekiduser->result();
	
		 $email = $_GET['email'];
		 $emailhead = $_GET['email_head'];
		 $emailcc = $_GET['email_cc'];
		 $phone = $_GET['phone'];
		 $password = md5($_GET['username']);
		 $username = $_GET['username'];
		 $this->db->where('username', $username);
		 $query = $this->db->get('tbl_user');
		 $count_row = $query->num_rows();
		 $email_purchase = $this->input->get('email_purchase');
		 $email_fc = $this->input->get('email_fc');
		 $email_bod = $this->input->get('email_bod');
		
		 // Log
		 $id = $this->session->userdata('id');
		 $username = $this->session->userdata('username');
		 $nama = $this->session->userdata('name');
		 $remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		 $remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		 $date_create = date('Y-m-d H:i:s');
		 $date_update = date('Y-m-d H:i:s');
		
		if(empty($_GET['name'])){
			die ("Field Name must be filled in!! ");
		}
		elseif(empty($_GET['first_name'])){
			die ("Field First Name must be filled in!! ");
		}
		elseif(empty($_GET['last_name'])){
			die ("Field Last Name must be filled in!! ");
		}
		elseif(empty($_GET['nick_name'])){
			die ("Field Nick Name must be filled in!! ");
		}
		elseif(empty($phone) || !preg_match('/^[+#*\(\)\[\]]*([0-9][ ext+-pw#*\(\)\[\]]*){6,45}$/', $phone)){
			die ("Field Phone must be filled in and numeric!! ");
		}
		elseif(empty($_GET['email'])){
			die ("Field Email must be filled in!! ");
		}
		elseif(!preg_match('/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/', $email)) {
			die ("Field Email isn't valid! ");
		}
		elseif(empty($_GET['sign_head'])){
			die ("Field Head must be filled in!! ");
		}
		elseif(empty($_GET['email_head'])){
			die ("Field Email Head must be filled in!! ");
		}
		elseif(!preg_match('/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/', $emailhead)) {
			die ("Field Email Head isn't valid! ");
		}
		elseif(empty($_GET['email_cc'])){
			die ("Field Email CC must be filled in!! ");
		}
		elseif(empty($_GET['email_purchase'])){
			die ("Field Email Purchase must be filled in!! ");
		}
		elseif(empty($_GET['email_fc'])){
			die ("Field Email FC must be filled in!! ");
		}
		elseif(empty($_GET['email_bod'])){
			die ("Field Email BOD must be filled in!! ");
		}
		elseif(empty($_GET['id_group'])){
			die ("Field Group must be filled in!! ");
		}
		elseif(empty($_GET['id_company'])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_GET['branch_id'])){
			die ("Field Branch must be filled in!! ");
		}
		elseif(99 == ($_GET['aproval_flag'])){
			die ("Field Status App must be filled in!! ");
		}
		elseif(99 == ($_GET['give_righ_flag'])){
			die ("Field Give Reprint must be filled in!! ");
		}
		elseif(empty($_GET['id_dept'])){
			die ("Field Department must be filled in!! ");
		}
		elseif(empty($_GET['purchase'])){
			die ("Field Status Purchase must be filled in!! ");
		}
		else{
			$purchase = $_GET['purchase'];
			$value = implode(",",$purchase);
			if(count($adacekiduser) < 1){
			$data = array(
					'id' => $_GET['id'],
					'name' => $_GET['name'],
					'first_name' => $_GET['first_name'],
					'last_name' => $_GET['last_name'],
					'nick_name' => $_GET['nick_name'],
					'username' => $_GET['username'],
					'password' => $password,
					'phone' => $_GET['phone'],
					'email' => $_GET['email'],
					'sign_head' => $_GET['sign_head'],
					'email_head' => $_GET['email_head'],
					'email_cc' => $_GET['email_cc'],
					'id_group' => $_GET['id_group'],
					'id_company' => $_GET['id_company'],
					'branch_id' => $_GET['branch_id'],
					'id_dept' => $_GET['id_dept'],
					'aproval_flag' => $_GET['aproval_flag'],
					'give_righ_flag' => $_GET['give_righ_flag'],
					'level_access' => $_GET['level_access'],
					'status_pur' => $value,
					'status' => '1',
					'email_purchase' => $email_purchase,
					'email_fc' => $email_fc,
					'email_bod' => $email_bod,
					'date_create' => $date_create,
					'remarks' => $remarks_add
				);
				$this->db->insert('tbl_user', $data);
			}if($adacekiduser){
				$data = array(
					'name' => $_GET['name'],
					'first_name' => $_GET['first_name'],
					'last_name' => $_GET['last_name'],
					'nick_name' => $_GET['nick_name'],
					'username' => $_GET['username'],
					'password' => $password,
					'phone' => $_GET['phone'],
					'email' => $_GET['email'],
					'sign_head' => $_GET['sign_head'],
					'email_head' => $_GET['email_head'],
					'email_cc' => $_GET['email_cc'],
					'id_group' => $_GET['id_group'],
					'id_company' => $_GET['id_company'],
					'branch_id' => $_GET['branch_id'],
					'id_dept' => $_GET['id_dept'],
					'aproval_flag' => $_GET['aproval_flag'],
					'give_righ_flag' => $_GET['give_righ_flag'],
					'level_access' => $_GET['level_access'],
					'status_pur' => $value,
					'status' => '1',
					'email_purchase' => $email_purchase,
					'email_fc' => $email_fc,
					'email_bod' => $email_bod,
					'date_update' => $date_update,
					'remarks' => $remarks_update
				);
				$this->db->where('id', $_GET['id']);
				$this->db->update('tbl_user', $data);
			}
	  echo 'Insert';
	}

}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_delete = 'Action : Delete | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_delete= date('Y-m-d H:i:s');

		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
						'date_update' => $date_delete,
						'remarks' => $remarks_delete
				);
				$this->db->where('id', $id);
				$this->db->update('tbl_user', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
  }



}
