<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<style>
 .outer {
    width: 200px;
    color: black;
	font-family:Rockwell, Calibri, Monospace;
    border: 2px solid Gainsboro;
    padding: 5px;
 }
 /* for autocomplitee */
.ui-autocomplete {
    max-height: 200px;
    overflow-y: auto;    
    overflow-x: hidden;   
    padding-right: 20px;
}

* html .ui-autocomplete {
    height: 200px;
	
}

* html .ui-autocomplete-loading {
background:url('<?php echo base_url('asset/images/loading.gif') ?>') no-repeat right center ;
}
</style>
<body>  
	<div>
		<button onclick="tambahdata()" class="btn btn-app btn-primary btn-xs radius-4 caddnew" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
            Add
		</button>
		<button onclick="delete_user()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit" id="deleteTriger" name="deleteTriger"  value ="btndel"  disabled="disabled" >
            <i class="ace-icon fa fa-ban bigger-160"></i>
            Delete
		</button>
		<button onclick="editdata()" class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="editTriger"  name="editTriger"  value ="btnedit"  disabled="disabled" >
            <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
            Edit
		</button> 
	</div>
	<br/>
<div class="table-header btn-info"> Create User   </div>  
	<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">       

	<div class="form-group col-xs-6">
        <div class="col-xs-10">
            <div class="form-inline ">
                <div class="form-group">Date start</div>
                
                <div class="form-group">
                    <div class="input-group date">
                       <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                       </div>
                       
                       <input type="text" class="form-control input-daterange" id="start_date" name="start_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                    </div> <!-- /.input-group datep -->       
                </div>
               
               <div class="form-group">
                       <label for="From" class="col-xs-1 control-label">To</label>
               </div>
              
               <div class="form-group">
                      <div class="input-group date">
                               <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                               </div>
                                     <input type="text" class="form-control input-daterange" id="end_date" name="end_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                       </div>  
               </div>
                
            </div>
        </div>
      </div>

		<table id="myTable" cellpadding="0" cellspacing="0" width="100%"  class="table table-striped table-bordered table-hover">
			<thead class="text-warning" > 
				<tr>  
					<th style="text-align:center">           
					<label class="pos-rel">
						<input type="checkbox" id="checkAll" class="ace" />
						<span class="lbl"></span>
					</label>
					</th>
					<th>No</th>
					<th>Id</th>
					<th>Name</th>
					<th>Username</th>
					<th>Group</th>
					<th>Company</th>
					<th>Branch</th>
					<th>Dept</th>
					<th>Email</th>
					<th>Date Create</th>
				</tr>
			</thead>
		</table> 	 
	</div>
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-80p">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">×</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    User Form
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body" style="padding-top:10px">
				<div class="form-group form-group-sm">
					<form action="javascript:void(0)" id="form" method="POST" class="form-horizontal">
						<div id="formcruduser"></div>
					</form>
				</div>
			</div>
				<!-- Modal Footer -->
				<div class="modal-footer">
					<button onclick="reset_pass()" class="btn btn-app btn-warning btn-xs radius-4" type="submit">
						<i class="ace-icon fa fa-refresh bigger-160"></i>
						Reset
					</button>
					<button onclick="save()" id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
						<i class="ace-icon fa fa-floppy-o bigger-160"></i>
						Save
					</button>
					<button class="btn btn-app btn-danger btn-xs radius-4" type="submit" data-dismiss="modal">
						<i class="ace-icon fa fa-close bigger-160"></i>
						Cancel
					</button>
			</div> <!-- End modal body div -->
		</div> <!-- End modal content div -->
	</div> <!-- End modal dialog div -->
</div>
</body>
  <!-- End modal div -->
<script>
//aktif edit--------------------------------------------------------
var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
	if($('#checkAll').not(':checked').length){
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
	}
});
//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//end----------------------------------------------------------------
</script>

<script type="text/javascript"> 
        $("#id_divisi").on('change', function(){
			   var app = document.getElementById("aproval_flag").value;
               var id_divisi = {id_divisi:$("#id_divisi").val()};
                   $.ajax({
               type: "POST",
               url : "<?php echo site_url('user/C_user/ajax_divisi/') ?>",
               data: id_divisi,
               success: function(msg){
               //$('#id_dept').html(msg);
               }
            });
         });
</script>
<script type="text/javascript">
   function aproval() {
	var app = document.getElementById("aproval_flag").value;
	if (app == 1){
		val_divisi = 7;
		val_dept = 17;
	}else if (app == 2){
		val_divisi = 8 ;
		val_dept = 18;
	}
        document.getElementById("id_divisi").value=val_divisi;
		document.getElementById("id_dept").value=val_dept;
}
</script>

<script type="text/javascript">
  function div() {
	var id_divisi = document.getElementById("id_divisi").value;
	var app = document.getElementById("aproval_flag").value;
	if (app == 4){
		if (id_divisi == 1){
		  val_dept = 19;
		}else if (id_divisi == 2){
			val_dept = 20;
		}else if (id_divisi == 4){
			val_dept = 21;
		}else if (id_divisi == 5){
			val_dept = 22;
		}
		document.getElementById("id_divisi").value=id_divisi;
		document.getElementById("id_dept").value=val_dept;
	}else if (app != 4) {
		var id_divisi = {id_divisi:$("#id_divisi").val()};
		$.ajax({
            url : "<?php echo site_url('user/C_user/ajax_divisi/') ?>",
            type: "POST",
            data: id_divisi,									
			success: function (msg)            
            {
               $('#id_dept').html(msg);
			}
		});
	}
		

}
</script>

<script>


//$('#myTable').dataTable();
//-----------------------------------------data table custome----
// $(document).ready(function () {
// var rows_selected = [];
// var tableUsers = $('#myTable').DataTable({
// 		"destroy" : 'true',
//         "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
// 		"dom": 'lfBrtip',
// 		"buttons": [
//             {
// 		   "extend":    'copy',
// 		   "text":      '<i class="fa fa-files-o"></i> Copy',
// 		   "titleAttr": 'Copy'
// 		   },
// 		   {
// 		   "extend":    'print',
// 		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
// 		   "titleAttr": 'Print',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   },
// 		   {
// 		   "extend":    'excel',
// 		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
// 		   "titleAttr": 'Excel'
// 		   },
// 		   {
// 		   "extend":    'pdf',
// 		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
// 		   "titleAttr": 'PDF',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   }
//         ],
// 		"autoWidth" : false,
// 		"scrollY" : '250',
// 		"scrollX" : true,
//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.

//         // Load data for the table's content from an Ajax source
//         "ajax": {
//             "url": "<?php echo base_url('user/C_user/ajax_list') ?>",
//             "type": "POST"
//         },
// 		'columnDefs': [
// 		{
//         'targets': [0],
// 		'orderable': false,
// 		}
//    ],

//       'order': [[2, 'desc']],
//         });	
// });

$("#checkAll").on('click',function(){
	$('#editTriger').prop("disabled", true);
	var status = this.checked;
	$(".editRow").each( function() {
		$(this).prop("checked",status);
	});
});
//end---------------------------------
</script>

<!-- Datatable -->
<script>
$(document).ready(function(){ 
		$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data('no'); //
	
		function fetch_data(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
				"ajax":{
				    url: "<?php echo base_url('user/C_user/ajax_list') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
				 
				 'columnDefs': [
							{
							'targets': [0],
							'orderable': false,
							}
					],

						'order': [[2, 'desc']],
			 });
		  }
		  	  
		  
//proses change date-----------------------------------------------
		$('#start_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
		$('#end_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);		
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	


  }); //end document on ready	
		
</script>


<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>


	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
	function reset_pass()
	{
		var url;
		if(save_method == 'update')
		{
          url = "<?php echo site_url('user/C_user/reset_pass')?>";
		}
		else
		{
		  alert("Just One Allowed Data!!!");
		}
		  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
		
	}
	
     function delete_user()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('user/C_user/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
			
		

      
    }
	
	function tambahdata(){
		$('#modal_form').modal('show');
		$('.modal-title').html('<b>Add User Data</b>');
		$.post('<?=@base_url('user/C_user/cruduser')?>',{ID:''},function(result){ 
			$('#formcruduser').html(result);
		}); 
	}
	
	function save(){
		var a = $('#form').serialize();
		$.post('<?=base_url('user/C_user/simpanupdatedatauser')?>/?'+a,{},function(result){ 
			if(result == 'Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			} else {
				alert(result);
			}
		});
	}
	function editdata(id){
	 var eee = $('.editRow:checked').val();
		$('#modal_form').modal('show');
		$.post('<?=@base_url('user/C_user/cruduser')?>',{ID:eee},function(result){ 
			$('#formcruduser').html(result);
		});
	}
  </script>

  

</html>
