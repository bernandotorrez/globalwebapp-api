<?php
if(!empty($_POST['ID'])){
	$sql = "select * from tbl_user where id='".$_POST['ID']."'";
	$qry = $this->db->query($sql);
	$ada = $qry->result();
}
?>

<div class="widget-body">
	<div class="widget-main">
		<div class="col-sm-6">
			<div class="form-group">
				<label for="id_menu_user_group" class="control-label col-sm-4" style="padding-right:10px">ID</label>
					<?php
						$this->db->select('max(id) as totaldata');
						$query = $this->db->get('tbl_user');
						$result = $query->result();
						$totaldata = $result[0]->totaldata+1;
					?>
					<div class="col-sm-6">			
        	<input class="form-control" id="id" name="id" placeholder="Id" type="text" readonly value="<?php echo $ada ? $ada[0]->id : $totaldata ?>"/>
					</div>
			</div>
			<div class="form-group">
	  		<label class="control-label col-sm-4" style="padding-right:10px">Full Name</label>
	  			<div class="col-sm-6">
						<input name="name" placeholder="BRIAN" class="form-control" type="text" value="<?=@$ada[0]->name?>">
	  			</div>
			</div>
			<div class="form-group">
	  		<label class="control-label col-sm-4" style="padding-right:10px">First</label>
	  			<div class="col-sm-2">
						<input name="first_name" placeholder="BRIAN" class="form-control" type="text" value="<?=@$ada[0]->first_name?>">
	  			</div>
	  		<label class="control-label col-sm-2" style="padding-right:5px">Last</label>
	  			<div class="col-sm-2">
						<input name="last_name" placeholder="BRIAN" class="form-control" type="text" value="<?=@$ada[0]->last_name?>">
	  			</div>
			</div>
			<div class="form-group">
	  		<label class="control-label col-sm-4" style="padding-right:10px">Nick Name</label>
	  			<div class="col-sm-6">
						<input name="nick_name" placeholder="BRIAN" class="form-control" type="text" value="<?=@$ada[0]->nick_name?>">
	  			</div>
			</div>	
			<div class="form-group">
	  		<label class="control-label col-sm-4" style="padding-right:10px">Username</label>
	  			<div class="col-sm-6">
						<input name="username" id="username" placeholder="brian123" class="form-control" type="text" value="<?=@$ada[0]->username?>">
	  			</div>
			</div>
			<div class="form-group">
	  		<label class="control-label col-sm-4" style="padding-right:10px">Phone</label>
	  			<div class="col-sm-6">
						<input name="phone" placeholder="081199555959" class="form-control" type="text" value="<?=@$ada[0]->phone?>">
	  			</div>
			</div>
			<div class="form-group">
	  		<label class="control-label col-sm-4" style="padding-right:10px">Email User</label>
	  			<div class="col-sm-6">
						<input name="email" placeholder="brian.yunanda@eurokars.co.id" class="form-control" type="text" value="<?=@$ada[0]->email?>">
	  			</div>
			</div>
			<div class="form-group">
	  		<label class="control-label col-sm-4" style="padding-right:10px">Head</label>
	  			<div class="col-sm-6">
						<input name="sign_head" placeholder="Ahmad Zulfikar" class="form-control" type="text" value="<?=@$ada[0]->sign_head?>">
	  			</div>
			</div>
		</div>
<!-- right column -->
		<div class="col-sm-6">
			<div class="form-group">
	  		<label class="control-label col-sm-4" style="padding-right:10px">Email Head</label>
	  			<div class="col-sm-6">
						<input name="email_head" placeholder="ahmad.zulfikar@eurokars.co.id" class="form-control" type="text" value="<?=@$ada[0]->email_head?>">
	  			</div>
			</div>	
			<div class="form-group">
	  		<label class="control-label col-sm-4" style="padding-right:10px">Email CC</label>
	  			<div class="col-sm-6">
						<input name="email_cc" placeholder="itteam@eurokars.co.id" class="form-control" type="text" value="<?=@$ada[0]->email_cc?>">
	  			</div>
			</div>

			<!-- Fitur Email Baru -->
			
			<div class="form-group">
				<label class="control-label col-sm-4" style="padding-right:10px">Email Purchase</label>
				<div class="col-sm-6">
					<input name="email_purchase" placeholder="purchase@eurokars.co.id" class="form-control" type="text"
						value="<?=@$ada[0]->email_purchase?>">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4" style="padding-right:10px">Email FC</label>
				<div class="col-sm-6">
					<input name="email_fc" placeholder="fc@eurokars.co.id" class="form-control" type="text"
						value="<?=@$ada[0]->email_fc?>">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-4" style="padding-right:10px">Email BOD</label>
				<div class="col-sm-6">
					<input name="email_bod" placeholder="bod@eurokars.co.id" class="form-control" type="text"
						value="<?=@$ada[0]->email_bod?>">
				</div>
			</div>

			<!-- Fitur Email Baru -->

			<div class="form-group">
				<label class="col-sm-4 control-label" style="padding-right:10px">User Group</label>
					<div class="col-sm-6">
						<select class="form-control select2" name="id_group" id="id_group" style="width:100%">
						<option value="">Choose Group</option>
							<?php
								$sqlgrp = "select id_group, name_grp from tbl_user_groups where status = 1";
								$qrygrp = $this->db->query($sqlgrp);
								$adagrp = $qrygrp->result();
								foreach($adagrp as $vall){
								$sel = "";
								if($ada){
									if($ada[0]->id_group == $vall->id_group){
										$sel = 'selected="true"';
									}
								}
							?>
						<option value="<?=@$vall->id_group?>" <?=@$sel?>><?=@$vall->name_grp?></option>
							<?php } ?>				
						</select>    
					</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4" style="padding-right:10px">Company</label>
					<div class="col-sm-6">
						<select class="form-control select2" name="id_company" id="id_company" style="width:100%">
						<option value="">Choose Company</option>
							<?php
								$sqlcompany = "select id_company, company from tbl_company where status = 1";
								$qrycompany = $this->db->query($sqlcompany);
								$adacompany = $qrycompany->result();
								foreach($adacompany as $vall){
								$sel = "";
								if($ada){
									if($ada[0]->id_company == $vall->id_company){
										$sel = 'selected="true"';
									}
								}
							?>
						<option value="<?=@$vall->id_company?>" <?=@$sel?>><?=@$vall->company?></option>
							<?php } ?>
						</select>    
					</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4" style="padding-right:10px">Branch</label>
					<div class="col-sm-6">
						<select class="form-control select2" name="branch_id" id="branch_id" style="width:100%">
						<option  value="">Choose Branch</option>
							<?php 
								$sqlbranches	  = "select * from tbl_branches where id_company='".$ada[0]->id_company."'";
								$qrybranches	  = $this->db->query($sqlbranches);
								$adabranches 	  = $qrybranches->result();					
								foreach($adabranches as $aaa){
									$sel = "";
									if($aaa->branch_id==$ada[0]->branch_id){
										$sel = "selected='true'";
									}
							?>
						<option value="<?php echo $aaa->branch_id;?>" <?=@$sel?>><?php echo $aaa->name_branch;?></option>
							<?php } ?>	
						</select>
					</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label" style="padding-right:10px">Status App</label>
					<div class="col-sm-6">
						<select class="form-control select2" name="aproval_flag" id="aproval_flag" style="width:100%">
			<option value="">Choose Company</option>
							<?php
								$sqlstatusapp = "select * from tbl_status_app where status = 1";
								$qrystatusapp = $this->db->query($sqlstatusapp);
								$adastatusapp = $qrystatusapp->result();
								foreach($adastatusapp as $valapp){
								$sel = "";
								if($ada){
									if($ada[0]->aproval_flag == $valapp->flag_app){
										$sel = 'selected="true"';
									}
								}
							?>
							<option value="<?=@$valapp->flag_app?>" <?=@$sel?>><?=@$valapp->name_status_app?></option>
							<?php } ?>
			</select> 
		</div>
	</div>
	
	<div class="form-group">
		<label class="control-label col-sm-4" style="padding-right:10px">Department</label>
		<div class="col-sm-6">
		   <select class="form-control select2" name="id_dept" id="id_dept" style="width:100%">
			<option value="">Choose Department</option>
				<?php
					$sqldept = "select id_dept, dept from tbl_dept where status = 1";
					$qrydept = $this->db->query($sqldept);
					$adadept = $qrydept->result();
					foreach($adadept as $vall){
					$sel = "";
					if($ada){
						if($ada[0]->id_dept == $vall->id_dept){
							$sel = 'selected="true"';
						}
					}
				?>
				<option value="<?=@$vall->id_dept?>" <?=@$sel?>><?=@$vall->dept?></option>
				<?php } ?>			
			</select>    
		</div>
	</div>
		  
	 <!-- <div class="form-group">
		<label class="col-sm-4 control-label" style="padding-right:10px">Give Reprint*</label>
		<div class="col-sm-6">
			<select class="form-control select2" name="give_righ_flag" id="give_righ_flag" style="width:100%">
				<option value="99">Choose Reprint</option>                                            		
				<?php
					$arraygiveflag = array('0' => 'No', '1' => 'Yes');
					foreach($arraygiveflag as $key => $val){
						$sel = "";
						if(!empty($_POST['ID'])){
							$sel = "";
							if($key != 0){
								if($key == $ada[0]->give_righ_flag){
									$sel = "selected='true'";
								}
							}else{
								$sel = "selected='true'";
							}
						}
				?>
				<option value="<?=@$key?>" <?=@$sel?>><?=@$val?></option>
				<?php } ?>
			</select>
		</div>
	</div> -->

	<div class="form-group">
		<label class="col-sm-4 control-label" style="padding-right:10px">Level Access</label>
		<div class="col-sm-6">
			<select class="form-control select2" name="level_access" id="level_access" style="width:100%" required>
				<option value="">Choose Level Access</option>                                            		
				
				<option value="1" <?php if($ada[0]->level_access == '1')  { echo 'selected'; } ?> >1 - Administrator</option>
				<option value="2" <?php if($ada[0]->level_access == '2')  { echo 'selected'; } ?> >2 - Super User</option>
				<option value="3" <?php if($ada[0]->level_access == '3')  { echo 'selected'; } ?> >3 - Manager</option>
				<option value="4" <?php if($ada[0]->level_access == '4')  { echo 'selected'; } ?> >4 - User</option>
			
			</select>
		</div>
	</div>
	
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label" style="padding-right:10px">Status Purchase</label>
			<div class="col-sm-6 outer" id="checkboxlist">
			<input type="checkbox" class="ace" onchange="selectallpurchase(this.checked)" name="sample"> All
			<?php 
				$querypurchase = $this->db->get('tbl_type_purchase');
				$purchase = $querypurchase->result();
				foreach($purchase as $row){
					$status_pur = $ada[0]->status_pur;
					$pecahstatus = explode(",",$status_pur);
					$cek = "";
					foreach($pecahstatus as $key => $val){
						if($row->id_flagpur==$val){
							$cek = "checked='true'";
						}
					}
			?>
				<input type="checkbox" class="" id="selectpurchase<?=@$row->id_flagpur;?>" name="purchase[]" value="<?=@$row->id_flagpur;?>" <?=@$cek?>><?=@$row->type_purchase;?>
			<?php } ?> 
		</div>
	</div>
<script>
$("#id_group").select2();
$("#id_company").select2();
$("#branch_id").select2();
$("#aproval_flag").select2();
$("#id_dept").select2();
$("#give_righ_flag").select2();
$('#level_access').select2();
	function selectallpurchase(cek){
		if(cek == true){
			<?php foreach($purchase as $row){ ?>
				$("#selectpurchase"+<?=@$row->id_flagpur?>+"").prop("checked",true);
			<?php } ?>
		}else{
			<?php foreach($purchase as $row){ ?>
				$("#selectpurchase"+<?=@$row->id_flagpur?>+"").prop("checked",false);
			<?php } ?>
		}
	}
	 $("#id_company").change(function (){
		var url = "<?php echo site_url('user/C_user/pos_branch');?>/"+$(this).val();
		$('#branch_id').load(url);
		return false;
	});
</script>