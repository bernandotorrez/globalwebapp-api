<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_scrap extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_scrap');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_scrap->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}
	 	}
		
	public function index()
		{

		$data['goods']=$this->M_scrap->get_all_goods();
		$data['total']=$this->M_scrap->get_count_id();
		$data['merk']=$this->M_scrap->get_all_merk();
		$data['category']=$this->M_scrap->get_all_category();
		$data['cate1']=$this->M_scrap->get_all_cate();
		$data['dept']=$this->M_scrap->get_all_dept();
		$data['branch']=$this->M_scrap->get_all_branch();
		$data['show_view'] = 'goods_form/V_scrap';
		$this->load->view('dashboard/Template',$data);		
		}

	
	public function ajax_list()
	{
		$list = $this->M_scrap->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $qv_complite_goods) {
			$chk_idmaster ='<div align="center"><input id="checkgoods" name="checkgoods" type="checkbox" value='.$qv_complite_goods->id_goods.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $qv_complite_goods->id_goods;
			$row[] = $qv_complite_goods->code_goods;
			$row[] = $qv_complite_goods->name_goods;
			$row[] = $qv_complite_goods->name_category;
			$row[] = $qv_complite_goods->name_merk;
			$row[] = $qv_complite_goods->description;
			$row[] = $qv_complite_goods->name_user;
			$row[] = $qv_complite_goods->dept;
			$row[] = $qv_complite_goods->name_branch;
			$row[] = $qv_complite_goods->date_loan;
			$row[] = $qv_complite_goods->date_scrap;
			$row[] = $qv_complite_goods->remark_scrap;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_scrap->count_all(),
						"recordsFiltered" => $this->M_scrap->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_goods');
		$this->db->from('qv_complite_goods');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_goods",$search);
		$this->db->or_like("id_category",$search);
		$this->db->or_like("name_category",$search);
		$this->db->or_like("id_merk",$search);
		$this->db->or_like("name_merk",$search);
		$this->db->or_like("name_goods",$search);
		$this->db->or_like("description",$search);
		$this->db->or_like("name_user",$search);
		$this->db->or_like("name_branch",$search);
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_goods','ASC');
		$query=$this->db->get('qv_complite_goods');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("id_goods",$search);
		$jum=$this->db->get('qv_complite_goods');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $qv_complite_goods) {
		$chk_idmaster ='<div align="center"><input id="checkgoods" name="checkgoods" type="checkbox" value='.$qv_complite_goods["id_goods"].' class="editRow ace" req_id_del='.$qv_complite_goods["id_goods"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($qv_complite_goods['id_goods'],$qv_complite_goods['code_goods'],
									$qv_complite_goods['name_goods'],$qv_complite_goods['name_category'],
									$qv_complite_goods['name_merk'],$qv_complite_goods['description'],
									$qv_complite_goods['name_user'],$qv_complite_goods['dept'],
									$qv_complite_goods['name_branch'],$qv_complite_goods['date_loan'],
									$qv_complite_goods['remarks'],$chk_idmaster
									);

		}

		echo json_encode($output);


	}
		
	public function add_goods()
		{
		if(empty($_POST["id_category"])){
			//die ("Field ID Category must be filled in!! ");
		}
		//elseif(empty($_POST["id_merk"])){
		//	die ("Field ID Merk must be filled in!! ");
		//}
		else{
			$data = array(
					'id_goods' => $this->input->post('id_goods'),
					'code_goods' => $this->input->post('code_goods'),
					'id_category' => $this->input->post('id_category'),
					'id_merk' => $this->input->post('id_merk'),
					'name_goods' => $this->input->post('name_goods'),
					'description' => $this->input->post('description'),
					'name_user' => $this->input->post('name_user'),
					'id_dept' => $this->input->post('id_dept'),
					'branch_id' => $this->input->post('branch_id'),
					'date_loan' => $this->input->post('datepicker'),
					'remarks' => $this->input->post('remarks'),
				);
			$insert = $this->M_scrap->add_goods($data);
		}
		echo 'Insert';
		}
		
	
	public function ajax_edit($id)
		{
			$data = $this->M_scrap->get_by_id($id);
			echo json_encode($data);
		}
		
	public function ajax_scrap($id)
		{
			$data = $this->M_scrap->get_by_id($id);
			echo json_encode($data);
		}
		
	public function ajax_hand($id)
		{
			$data = $this->M_scrap->get_by_id($id);
			echo json_encode($data);
		}


	public function update_goods()
		{
		if(empty($_POST["id_category"])){
			die ("Field ID Category must be filled in!! ");
		//}
		//elseif(empty($_POST["id_merk"])){
		//	die ("Field ID Merk must be filled in!! ");
		}
		else{
			$data = array(
					'id_goods' => $this->input->post('id_goods'),
					'code_goods' => $this->input->post('code_goods'),
					'id_category' => $this->input->post('id_category'),
					'id_merk' => $this->input->post('id_merk'),
					'name_goods' => $this->input->post('name_goods'),
					'description' => $this->input->post('description'),
					'name_user' => $this->input->post('name_user'),
					'id_dept' => $this->input->post('id_dept'),
					'branch_id' => $this->input->post('branch_id'),
					'date_loan' => $this->input->post('datepicker'),
					'remarks' => $this->input->post('remarks'),
				);
		$this->M_scrap->update_goods(array('id_goods' => $this->input->post('id_goods')), $data);
		}
		echo 'Insert';
	}
	
	
	public function update_hand()
		{
			$aaa = $this->input->post('repeated_handover');
			$bbb = $aaa + 1;
			$data = array(
					'status' => 3,
				);
		$this->M_scrap->update_goods(array('id_goods' => $this->input->post('id_goods')), $data);
			$data1 = array(
					'code_goods' => $this->input->post('code_goods'),
					'id_category' => $this->input->post('id_category'),
					'id_merk' => $this->input->post('id_merk'),
					'name_goods' => $this->input->post('name_goods'),
					'description' => $this->input->post('description'),
					'name_user' => $this->input->post('name_user'),
					'id_dept' => $this->input->post('id_dept'),
					'branch_id' => $this->input->post('branch_id'),
					'date_loan' => $this->input->post('datepicker'),
					'remarks' => $this->input->post('remarks'),
					'repeated_handover' => $bbb,
				);
			$insert = $this->M_scrap->add_goods($data1);
		echo 'Insert';
	}
	
	public function update_scrap()
		{
			$data = array(
					'date_loan' => $this->input->post('datepicker1'),
					'remark_scrap' => $this->input->post('remarks_scrap'),
					'status' => 2,
				);
		$this->M_scrap->update_goods(array('id_goods' => $this->input->post('id_goods')), $data);
		echo 'Insert';
	}
	
	public function ajax_category()
    {
        $id = $this->input->post('id_category');
        $data['merk'] = $this->M_scrap->get_merk($id);
        $this->load->view('merk',$data);
    }
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array (
						'status' => '0',
				);
				$this->db->where('id_goods', $id);
				$this->db->update('tbl_goods', $data);
			}
		}
	}
	
	function pos_parent3($id_parent3)
	{
    	$query = $this->db->get_where('tbl_child_menu',array('id_parent'=>$id_parent3));
    	$data = "<option value=''>- Select Child -</option>";
    	foreach ($query->result() as $value) {
        	$data .= "<option value='".$value->id_child."'>".$value->desc_child."</option>";
    	}
    	echo $data;
	}
  
	function pos_child3()
	{
		$id = $this->input->post('id_child');
        $data['subchild'] = $this->M_scrap->get_subchild($id);
		$data['subchild2'] = $this->M_scrap->get_all_count($id);
        $this->load->view('subchild',$data);
    }
	
	public function update_position()
		{
				$count = 0;
				$displayed_city = array();
				$city = array();

				foreach ($_POST['pos'] as $pos_key => $pos_value){
				  $jumlah = count($_POST['pos']); 
				  $count	++;
				  $city = '';
				  if( isset($pos_value)){
					$city = $pos_value;
					if(!in_array($city, $displayed_city)){
					  $displayed_city[] = $city;
					  $total = count($displayed_city);
					}
				  }
		}
		 if($total!=$jumlah){
			die('Duplicate Data !');
			
		  }else{
			echo 'Insert';
			   foreach ($_POST['pos'] as $pos_key => $pos_value){
				$dede[$pos_key] = $pos_value;
				$parentnya['order_subcild'] = $pos_value;
				$this->db->where("id_goods",$pos_key);
				$this->db->update("tbl_goods",$parentnya);
			 
		   }
}
}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');
        
    }




}
