<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_scrap extends CI_Model
{
	var $table = 'tbl_goods';
	var $tablenew = 'qv_complite_goods';
	var $column_order = array(null, null, 'id_goods','code_goods','name_goods','name_category','name_merk','description','name_user','dept','name_branch','date_loan','date_scrap','remark_scrap'); //set column field database for datatable orderable
	var $column_search = array('id_goods','code_goods','name_goods','name_category','name_merk','description','name_user','dept','name_branch','date_loan','date_scrap','remark_scrap'); //set column field database for datatable searchable 
	var $order = array('id_goods' => 'desc'); // default order 
	
    
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	private function _get_datatables_query()
	{
		
		$this->db->from($this->tablenew);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
	    $this->db->where('status = 2');
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$this->db->where('status = 2');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->tablenew);
		$this->db->where('status = 2');
		return $this->db->count_all_results();
	}
	
	public function get_all_goods()
		{
			$this->db->from('qv_complite_goods');
			$this->db->where('status = 2');
			$query=$this->db->get();
			return $query->result();

		}
	
	public function get_all_merk()
		{
			$this->db->from('tbl_goods_merk');
			$query=$this->db->get();
			return $query->result();

		}

	public function get_all_dept()
		{
			$this->db->from('tbl_dept');
			$query=$this->db->get();
			return $query->result();

		}
		
	public function get_all_branch()
		{
			$this->db->from('tbl_branches');
			$query=$this->db->get();
			return $query->result();

		}
		
	public function get_all_category()
		{
			$this->db->from('tbl_goods_category');
			$query=$this->db->get();
			return $query->result();

		}
		
	public function get_all_cate()
		{
			$this->db->from('tbl_goods_category');
			$query=$this->db->get();
			return $query->result();

		}

		
	public function get_all_par()
		{
			$this->db->from('tbl_goods_category');
			$query=$this->db->get();
			return $query->result();

		}
	
	public function get_all_mix($data,$field)
		{
			$this->db->from('tbl_goods_category');
			$this->db->where('id_category', $data );
			$query=$this->db->get();
			$aaa = $query->result();
			if($aaa){
				return $aaa[0]->$field;
			}else{
				return $aaa;
			}

		}

	public function get_by_id($id)
		{
			$this->db->from($this->tablenew);
			$this->db->where('id_goods',$id);
			$query = $this->db->get();
			return $query->row();
		}
	
	public function add_goods($data)
		{
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}
	
	public function get_merk($id)
		{
        $q = $this->db->query("select * from tbl_goods_merk left join tbl_goods_category on tbl_goods_merk.id_category=tbl_goods_category.id_category where tbl_goods_merk.id_category='$id'");
        return $q;
		}
	
	public function get_goods($id)
		{
        $q = $this->db->query("select * from tbl_goods left join tbl_goods_merk on tbl_goods.id_merk=tbl_goods_merk.id_merk where tbl_goods.id_merk='$id'");
        return $q;
		}
		
	public function update_goods($where, $data)
		{
			$this->db->update($this->table, $data, $where);
			return $this->db->affected_rows();
		}

	public function delete_by_id($id)
		{
			$this->db->where('id_merk', $id);
			$this->db->delete($this->table);
		}
		
	public function get_all_count($id)
		{
			$this->db->select('count(name_goods) as totaldata');
			$this->db->where('id_merk', $id);
			$this->db->from('tbl_goods');
			$query=$this->db->get();
			return $query->result();
		}
	
	public function get_count_id()
		{
			$this->db->select('max(id_goods) as totaldata');
			$this->db->from($this->table);
			$query=$this->db->get();
			return $query->result();
		}
		
	public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}
}
