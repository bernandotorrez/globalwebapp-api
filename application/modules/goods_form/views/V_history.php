<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>

									<div>
									<td><a href="#" class="btn btn-danger btn-setting report" id="report" style="text-decoration:none"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>Report</a></td>
									</div>
                                    <br/>
									<div class="row">
									<div class="col-xs-12" style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">                               
	                                <table  class="table table-striped table-bordered table-hover" id="myTable" >
	                                    <thead class="text-warning">
											<th width="5%" style="text-align:center">
												<label class="pos-rel">
                                                    <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
                                                    <span class="lbl"></span>
                                                </label>
                                            </th>
											<th>No</th>
	                                        <th>Id Goods</th>
											<th>Code Goods</th>
											<th>Name Goods</th>
											<th>Name Category</th>
											<th>Name Merk</th>
											<th>Description</th>
											<th>Name User</th>
											<th>Dept</th>
											<th>Name Branch</th>
											<th>Date Loan</th>
											<th>Date Scrap</th>
											<th>Remarks</th>
											<th>Status</th>
											<th>Repeated Handover</th>
	                                    </thead>
                                     </table> 
      </div>                               
</div>      
                                 	                       
<!-- Bootstrap modal -->
<form action="#" id="form" class="form-horizontal">
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-80p">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">×</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Goods Form
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body" style="padding-top:10px">       
			 

        <div class="form-group form-group-sm">
            <!-- left column -->
            <div class="col-sm-6">
			
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">ID</label>
					<div class="col-sm-8">
					<?php
					$totaldata = $total[0]->totaldata+1;
					?>
					<input name="id_goods" placeholder="Id" class="form-control" type="text" readonly value="<?php echo $totaldata ?>">
					</div>
				</div>
				
                <div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Code Goods</label>
				  <div class="col-sm-8">
					<input name="code_goods" placeholder="" class="form-control" type="text">
				  </div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Category</label>
					<div class="col-sm-8">
						<select class="form-control" name="id_category" id="id_category"  onchange="cate()">
						<option  value="">Choose Category</option> 
						<?php foreach($category as $ddd) { ?>
						<option value="<?php echo $ddd->id_category;?>"><?php echo $ddd->name_category;?>
						</option>
						<?php } ?>				
						</select>    
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Merk</label>
					<div class="col-sm-8">
					   <select class="form-control" name="id_merk" id="id_merk">
						<option  value="">Choose Merk</option>
						<?php foreach($category1 as $bbb) { 
						$sqlpo	  = "select * from tbl_merk where id_category='".$bbb->id_category."'";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo 	  = $qrypo->result();					
						foreach($adapo as $aaa) { ?>
						<option value="<?php echo $aaa->id_merk;?>"><?php echo $aaa->name_merk;?>
						</option>
						<?php } }?>	
						</select>    
					</div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Name Goods</label>
				  <div class="col-sm-8">
					<input name="name_goods" id="name_goods" placeholder="" class="form-control" type="text">
				  </div>
				</div>

			    <div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Description</label>
				  <div class="col-sm-8">
                    <textarea name="description" placeholder="Core i3 14 inch" class="form-control" type="text"></textarea>
                  </div>
                </div>
				
            </div>
		
            <!-- right column -->
            <div class="col-sm-6">
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Name User</label>
				  <div class="col-sm-8">
					<input name="name_user" placeholder="Brian Yunanda" class="form-control" type="text">
				  </div>
				</div>
				
                <div class="form-group">
					<label class="col-sm-3 control-label" style="padding-right:10px">Department</label>
					<div class="col-sm-8">
                        <select class="form-control" name="id_dept" id="id_dept">
						<option  value="">Choose Department</option> 
						<?php foreach($dept as $row) { ?>
						<option value="<?php echo $row->id_dept;?>"><?php echo $row->dept;?>
						</option>
						<?php } ?>				
						</select>    
                    </div>
                </div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Branches</label>
					<div class="col-sm-8">
						<select class="form-control" name="branch_id" id="branch_id">
						<option  value="">Choose Branches</option>
						<?php foreach($branch as $row) { ?>
						<option value="<?php echo $row->branch_id;?>"><?php echo $row->name_branch;?>
						</option>
						<?php } ?>				
						</select>    
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Date Loaned</label>
					<div class="col-sm-6">
					   <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="datepicker" class="form-control" id="datepicker">
					</div>
				</div>
                
                
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Remarks</label>
				  <div class="col-sm-8">
                    <textarea name="remarks" placeholder="" class="form-control" type="text"></textarea>
                  </div>
                </div>
				
                </div>
				</fieldset>
                <!-- End contact information -->
            </div>
        
      <!-- Modal Footer -->
            <div class="modal-footer">
            <button type="button" id="btnSave" name="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div><!-- End Modal Footer -->
		</div> <!-- End modal body div -->
      </div> <!-- End modal content div -->
  </div> <!-- End modal dialog div -->
</div> <!-- End modal div -->
	  
	  
	  
	  <div class="modal fade" id="modal_form2" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3 class="modal-title">Scrap Form</h3>
				</div>
				
					<input type="hidden" value="" name="id_goods"/>
				<div class="modal-body form" style="padding-top:10px">
				
				  <div class="form-group">
                  <label for="date" class="col-sm-3 control-label" style="padding-right:10px">Date Scrap</label>

                 <div class="input-group date col-sm-8">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" name="datepicker1" id="datepicker1">
                </div>
                </div>
                
                
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Remarks Scrap</label>
				  <div class="col-sm-8">
                    <textarea name="remarks_scrap" placeholder="" class="form-control" type="text"></textarea>
                  </div>
                </div>
				
					<div class="modal-footer">
					<button type="button" id="btnSave" onclick="save2()" class="btn btn-primary">Save</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	   </div>
	   
	   
<div class="modal fade" id="modal_form3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-80p">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">×</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Hand Over Form
                </h4>
            </div>
				
					 <!-- Modal Body -->
            <div class="modal-body" style="padding-top:10px">       
			 

        <div class="form-group form-group-sm">
            <!-- left column -->
            <div class="col-sm-6">
			
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">ID</label>
					<div class="col-sm-8">
					<?php
					$totaldata = $total[0]->totaldata+1;
					?>
					<input name="id_goods" placeholder="Id" class="form-control" type="text" value="<?php echo $totaldata ?>" readonly>
					</div>
				</div>
				
                <div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Code Goods</label>
				  <div class="col-sm-8">
					<input name="code_goods" placeholder="" class="form-control" type="text" readonly>
				  </div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Category</label>
					<div class="col-sm-8">
						<select class="form-control" name="id_category" id="id_category" readonly>
						<option  value="">Choose Category</option> 
						<?php foreach($category as $ddd) { ?>
						<option value="<?php echo $ddd->id_category;?>"><?php echo $ddd->name_category;?>
						</option>
						<?php } ?>				
						</select>    
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Merk</label>
					<div class="col-sm-8">
					   <select class="form-control" name="id_merk" id="id_merk" readonly>
						<option  value="">Choose Merk</option>
						<?php foreach($category1 as $bbb) { 
						$sqlpo	  = "select * from tbl_merk where id_category='".$bbb->id_category."'";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo 	  = $qrypo->result();					
						foreach($adapo as $aaa) { ?>
						<option value="<?php echo $aaa->id_merk;?>"><?php echo $aaa->name_merk;?>
						</option>
						<?php } }?>	
						</select>    
					</div>
				</div>
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Name Goods</label>
				  <div class="col-sm-8">
					<input name="name_goods" id="name_goods" placeholder="" class="form-control" readonly type="text">
				  </div>
				</div>

			    <div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Description</label>
				  <div class="col-sm-8">
                    <textarea name="description" placeholder="Core i3 14 inch" class="form-control" readonly type="text"></textarea>
                  </div>
                </div>
				
            </div>
		
            <!-- right column -->
            <div class="col-sm-6">
				
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Name User</label>
				  <div class="col-sm-8">
					<input name="name_user" placeholder="Brian Yunanda" class="form-control" type="text">
				  </div>
				</div>
				
                <div class="form-group">
					<label class="col-sm-3 control-label" style="padding-right:10px">Department</label>
					<div class="col-sm-8">
                        <select class="form-control" name="id_dept" id="id_dept">
						<option  value="">Choose Department</option> 
						<?php foreach($dept as $row) { ?>
						<option value="<?php echo $row->id_dept;?>"><?php echo $row->dept;?>
						</option>
						<?php } ?>				
						</select>    
                    </div>
                </div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Branches</label>
					<div class="col-sm-8">
						<select class="form-control" name="branch_id" id="branch_id">
						<option  value="">Choose Branches</option>
						<?php foreach($branch as $row) { ?>
						<option value="<?php echo $row->branch_id;?>"><?php echo $row->name_branch;?>
						</option>
						<?php } ?>				
						</select>    
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-sm-3" style="padding-right:10px">Date Loaned</label>
					<div class="col-sm-6">
					   <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" name="datepicker2" data-date-format="dd-mm-yyyy" class="form-control datepick" id="datepicker2">
					</div>
				</div>
                
                
				<div class="form-group">
				  <label class="control-label col-sm-3" style="padding-right:10px">Remarks</label>
				  <div class="col-sm-8">
                    <textarea name="remarks" placeholder="" class="form-control" type="text"></textarea>
                  </div>
                </div>
				
				 </div>
				</fieldset>
                <!-- End contact information -->
            </div>
        
      <!-- Modal Footer -->
            <div class="modal-footer">
            <button type="button" id="btnSave" name="btnSave" onclick="save3()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	   </div>
	   
	<!-- Modal For Report -->
 
        <div class="modal fade" id="rptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog  ">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Report convert to excel</h4>
                    </div>
                    
                    <div class="modal-body-report">
                       <table class="table-condensed" align="center"  >
                       <tr>
                          <td style="color:#900"><label for="Date start">Date Start  </label></td>
                          <td>
                           <div class="control-group">
                           <div class="col-xs-11"> 
                          	<input id="txtrptdtstart" name="txtrptdtstart" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepick" />
                            </div>
                           </div> 
                          </td>
                       </tr>
                        <tr>
                          <td style="color:#900"><label for="Date End"  >Date End   </label></td>
                          <td>
                          <div class="control-group">
                           <div class="col-xs-11"> 
                          	<input id="txtrptdtend" name="txtrptdtend" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepick" />
                            </div>
                           </div>
                          </td>
                       </tr>
                       <tr>
                       	  <td><label for="Company"   >Company  : </label></td>
                          <td>
                         <div class="control-group">
                           <div class="col-xs-11">                                                
                            <select id="cbocomp" name="cbocomp" class="form-control" > 
                             <!-- <option value="all">- ALL -</option> --->
                               <?php foreach($tampil_company as $rowcom){ ?>                            
                             	 <option value="<?php echo $rowcom->id_company; ?>"><?php echo $rowcom->company ;?></option>
                               <?php }?> 
                            </select>                         
                           </div>
                         </div>   
                          </td>                        
                       </tr>
                        <tr>
                       	  <td><label for="Departement"   >Dept </label></td>
                          <td>
                         <div class="control-group">
                         <div class="col-xs-11"> 
                           <select id="cbodept" name="cbodept"  class="form-control" >
                           <?php  if($this->session->userdata('aproval_flag') =="3"){ ?>
                            <option value="<?php echo $this->session->userdata('id_dept'); ?>"><?php echo $this->session->userdata('dept') ;?></option>
                           <?php }else{ ?> 
                              <option value="all">- ALL -</option>
                               <?php foreach($tampil_dept as $rowdept){ ?>                            
                             	 <option value="<?php echo $rowdept->id_dept; ?>"><?php echo $rowdept->dept ;?></option>
                               <?php }?> 
                            <?php }?>    
                            </select>
                            </div>
                           </div> 
                          </td>                        
                       </tr>
                       <tr>
                       <td><label for="Status BPK">Status BPK </label></td>
                         <td>
                         <div class="control-group">
                           <div class="col-xs-11"> 
                            <select id="cbobpk" name="cbobpk"  class="form-control" > 
                              <option value="all">- ALL -</option> 
                              <option value="1">BPK ONLY</option>                                                           
                            </select>
                            </div>
                           </div> 
                          </td>                        
                       </tr>
                       </table>            
                    </div> 
                    
                     <div class="modal-footer">
                        
                        <input id="btnreportexcel" type="submit" name="btnreportexcel" value="Report To Excel" class="btn btn-danger btn_dobpkprint"/>   
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
       
<!-- Modal -->  

</form>

<script>
 //modala\ popup report.
$(function(){
  $(document).on('click','.report',function(e){	
      $("#myModal").modal('hide');    
	  $("#rptModal").modal('show');  
	  $("#txtrptdtstart").val('');
	  $("#txtrptdtend").val('');  	
	  
	  document.getElementById('cbocomp').selectedIndex= '0';
	  document.getElementById('cbodept').selectedIndex = '0';
	  document.getElementById('cbobpk').selectedIndex = '0';	  	    								
   });	
});
</script>
<script type="text/javascript">
 $('.datepick').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 
 <script type="text/javascript">
            $(function () {
                $('#datepicker').datetimepicker();
            });
			$(function () {
                $('#datepicker1').datetimepicker();
            });
			$(function () {
                $('#datepicker2').datetimepicker();
            });
 </script>
	 
<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>


 <script type="text/javascript"> 
        $("#id_category").change(function(){
                var id_category = {id_category:$("#id_category").val()};
                   $.ajax({
               type: "POST",
               url : "<?php echo site_url('goods_form/c_history/ajax_category/') ?>",
               data: id_category,
               success: function(msg){
               $('#id_merk').html(msg);
               }
            });
              });
</script>

<script>
        	$(document).ready(function(){
	            $("#id_category3").change(function (){
	                var url = "<?php echo site_url('goods_form/c_history/pos_parent3');?>/"+$(this).val();
	                $('#id_merk3').load(url);
	                return false;
	            })
	  
				$("#id_merk3").change(function(){
                var id_merk = {id_merk:$("#id_merk3").val()};
                   $.ajax({
               type: "POST",
               url : "<?php echo site_url('goods_form/c_history/pos_child3');?>",
               data: id_merk,
               success: function(msg){
               $('#id_subchild3').html(msg);
               }
            });
              });
	        });
    	</script>	
<script>
//$('#myTable').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
var tableUsers = $('#myTable').DataTable({		
		"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		"dom": 'lfBrtip',
		"buttons": [
            {
		   "extend":    'copy',
		   "text":      '<i class="fa fa-files-o"></i> Copy',
		   "titleAttr": 'Copy'
		   },
		   {
		   "extend":    'print',
		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
		   "titleAttr": 'Print',
		   "orientation": 'landscape',
           "pageSize": 'A4'
		   },
		   {
		   "extend":    'excel',
		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
		   "titleAttr": 'Excel'
		   },
		   {
		   "extend":    'pdf',
		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
		   "titleAttr": 'PDF',
		   "orientation": 'landscape',
           "pageSize": 'A4'
		   }
        ],
		"autoWidth" : false,
		"scrollY" : '250',
		"scrollX" : true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('goods_form/c_history/ajax_list') ?>",
            "type": "POST"
        },
		'columnDefs': [
		{
        'targets': [0],
		'orderable': false,
		}
		],

      'order': [[2, 'desc']],
        });

/*var tableUsers = $('#myTable').DataTable({
            searching:true,
			//width:'100%',
			autoWidth : false,
  		    responsive : true,
			"scrollY" : '250',
			"scrollX" : true,
			ordering: false,
			/*oLanguage: {
				sProcessing: "<img src='' />",
			},
			processing: true,
			serverSide: true,
			ajax: {
			  url: "<?php echo base_url('goods_form/c_history/ambil_data') ?>",
			  type:'POST',
	}
			
        });	*/		
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	$('#scrapTriger').prop("disabled", true);
	$('#handTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif edit--------------------------------------------------------

var counterCheckedhand = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckedhand++ : counterCheckedhand--;
    counterCheckedhand == 1 ? $('#handTriger').prop("disabled", false): $('#handTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif scrap--------------------------------------------------------

var counterCheckedscrap = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckedscrap++ : counterCheckedscrap--;
    counterCheckededit == 1 ? $('#scrapTriger').prop("disabled", false): $('#scrapTriger').prop("disabled", true);
});

//end---------------------------------------------------------------


//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>

<script type="text/javascript">
   function cate() {
	var id_category = document.getElementById("id_categroy").value;
	var id_category = {id_category:$("#id_category").val()};
		$.ajax({
            url : "<?php echo site_url('goods_form/c_history/ajax_category/') ?>",
            type: "POST",
            data: id_category,									
			success: function (msg)            
            {
               $('#id_merk').html(msg);
			}
		});
	}
		


</script>
	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
    function add_goods()
    {
      save_method = 'add';
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
	
	
	function edit_position()
    {
		$.ajax({
        url:"<?php echo site_url('goods_form/c_history/ajax_parent/')?>/", //the page containing php script
        type: "POST", //request type
        success:function(result){
			$('#id_position').html(result);
            $('#modal_form2').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Position'); // Set title to Bootstrap modal title
       },
	    error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
     });
	}
	
	function edit_goods(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('goods_form/c_history/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_goods"]').val(data.id_goods);
			$('[name="code_goods"]').val(data.code_goods);
            $('[name="id_category"]').val(data.id_category);
			$('[name="id_merk"]').val(data.id_merk);
			$('[name="name_goods"]').val(data.name_goods);
			$('[name="description"]').val(data.description);
			$('[name="name_user"]').val(data.name_user);
			$('[name="id_dept"]').val(data.id_dept);
			$('[name="branch_id"]').val(data.branch_id);
			$('[name="datepicker"]').val(data.date_loan);
			$('[name="remarks"]').val(data.remarks);
	
            
            $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Goods'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



     function save()
     {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('goods_form/c_history/add_goods')?>";
      }
      else
      {
		   url = "<?php echo site_url('goods_form/c_history/update_goods')?>";
      }
		var formData = new FormData(document.getElementById('form')) 	    
			$.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }
	
	function edit_hand(id)
    {
      $('#form')[0].reset(); // reset form on modals
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('goods_form/c_history/ajax_hand/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_goods"]').val(data.id_goods);
			$('[name="code_goods"]').val(data.code_goods);
            $('[name="id_category"]').val(data.id_category);
			$('[name="id_merk"]').val(data.id_merk);
			$('[name="name_goods"]').val(data.name_goods);
			$('[name="description"]').val(data.description);
	
            
            $('#modal_form3').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Add Scrap'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
	
	
	  function save3()
     {
      var url;
		url = "<?php echo site_url('goods_form/c_history/update_hand')?>";
		var formData = new FormData(document.getElementById('form')) 	    
			$.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form3').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }
	
	function edit_scrap(id)
    {
      $('#form')[0].reset(); // reset form on modals
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('goods_form/c_history/ajax_scrap/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
		{
			$('[name="id_goods"]').val(data.id_goods);
			
            $('#modal_form2').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Add Scrap'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

	
	 function save2()
     {
      var url;
		url = "<?php echo site_url('goods_form/c_history/update_scrap')?>";
		var formData = new FormData(document.getElementById('form')) 	    
			$.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
             if(data=='Insert'){
				$('#modal_form2').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }
	
    function delete_goods()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('goods_form/c_history/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
      
    }

	
  </script>
</html>
