<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>

									<div>
									<button class="btn bg-purple btn-flat" onclick="add_merk()"><i class="fa fa-plus-circle"></i> Add Data</button>
									<button onclick="delete_merk()" id="deleteTriger" name="deleteTriger" class="btn bg-maroon btn-flat" disabled><i class="fa fa-trash"></i> Delete Data</button>
									<button onclick="edit_merk()" id="editTriger"  name="editTriger"class="btn bg-maroon btn-flat" disabled><i class="fa fa-edit"></i> Edit Data</button>
									</div>
                                    <br/>
									<div class="row">
									<div class="col-xs-12" style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">                               
	                                <table  class="table table-striped table-bordered table-hover" id="myTable" >
	                                    <thead class="text-warning">
											<th width="5%" style="text-align:center">
												<label class="pos-rel">
                                                    <input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
                                                    <span class="lbl"></span>
                                                </label>
                                            </th>
											<th>No</th>
	                                        <th>Id Merk</th>
											<th>Id Category</th>
											<th>Name Merk</th>
	                                    </thead>
                                     </table> 
      </div>                               
</div>      
                                 	                       
<!-- Bootstrap modal -->
 <form action="#" id="form" class="form-horizontal">
 <div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Merk Form</h3>
      </div>
      <div class="modal-body form" style="padding-top:10px">
         <div class="form-group">
					<label class="control-label col-md-4" style="padding-right:10px">ID</label>
					<div class="col-md-8">
					<?php
					$totaldata = $total[0]->totaldata+1;
					?>
					<input name="id_merk" placeholder="1" class="col-md-9" type="text" readonly value="<?php echo $totaldata ?>">
					</div>
				</div>
			<div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">ID Category</label>
              <div class="col-md-8">
			   <select class="col-md-9" name="id_category">
                <option  value=""></option>                    
				<?php foreach($category as $row) { ?>
                <option value="<?php echo $row->id_category;?>"><?php echo $row->name_category;?></option>
            <?php } ?>
			</select>    
              </div>
            </div>
			<div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Name Category</label>
              <div class="col-md-8">
                <input name="name_merk" placeholder="Samsung" class="col-md-9" type="text">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  
  <!-- End Bootstrap modal -->
 <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
       <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Position Form</h3>
       </div>
	    <input type="hidden" value="" name="id_subchild"/>
       <div class="modal-body form">
	  <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">ID Parent</label>
              <div class="col-md-8">
			   <select class="col-md-9" name="id_category2" id="id_category2">
                <option  value="">Choose Parent</option> 
				<?php foreach($parent as $row) { ?>
                <option value="<?php echo $row->id_category;?>"><?php echo $row->parent_menu_desc;?>
				</option>
				<?php } ?>				
			</select>    
              </div>
            </div>
			<div class="form-group">
              <div class="col-xs-10 col-xs-offset-1" id="id_merk2" name="id_merk2">
            </div>
            </div>
			<div class="modal-footer">
			<button type="button" id="btnSave" onclick="save2()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
       </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  <!-- End Bootstrap modal -->
</form>
  
  
  
  <script type="text/javascript"> 
        $("#id_category2").change(function(){
                var id_category = {id_category:$("#id_category2").val()};
                   $.ajax({
               type: "POST",
               url : "<?php echo site_url('goods_merk/c_goods_merk/ajax_parentnew/') ?>",
               data: id_category,
               success: function(msg){
               $('#id_merk2').html(msg);
               }
            });
              });
</script>
  <script>


//$('#myTable').dataTable();
//-----------------------------------------data table custome----
var rows_selected = [];
var tableUsers = $('#myTable').DataTable({
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		"dom": 'lfBrtip',
		"buttons": [
            {
		   "extend":    'copy',
		   "text":      '<i class="fa fa-files-o"></i> Copy',
		   "titleAttr": 'Copy'
		   },
		   {
		   "extend":    'print',
		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
		   "titleAttr": 'Print',
		   "orientation": 'landscape',
           "pageSize": 'A4'
		   },
		   {
		   "extend":    'excel',
		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
		   "titleAttr": 'Excel'
		   },
		   {
		   "extend":    'pdf',
		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
		   "titleAttr": 'PDF',
		   "orientation": 'landscape',
           "pageSize": 'A4'
		   }
        ],
		"autoWidth" : false,
		"scrollY" : '250',
		"scrollX" : true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('goods_merk/c_goods_merk/ajax_list') ?>",
            "type": "POST"
        },
		'columnDefs': [
		{
        'targets': [0],
		'orderable': false,
		}
		],

		'order': [[2, 'desc']],
        });
/*var tableUsers = $('#myTable').DataTable({
            searching:true,
			//width:'100%',
			autoWidth : false,
  		    responsive : true,
			"scrollY" : '250',
			"scrollX" : true,
			ordering: false,
			/*oLanguage: {
				sProcessing: "<img src='' />",
			},
			processing: true,
			serverSide: true,
			ajax: {
			  url: "<?php echo base_url('goods_merk/c_goods_merk/ambil_data') ?>",
			  type:'POST',
	}
			
        });	*/
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>


<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>


	<script type="text/javascript">
	var save_method; //for save method string
    var table;
	
    function add_merk()
    {
      save_method = 'add';
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
	
	function edit_position()
    {
		$.ajax({
        url:"<?php echo site_url('goods_merk/c_goods_merk/ajax_category/')?>/", //the page containing php script
        type: "POST", //request type
        success:function(result){
			$('#id_position').html(result);
            $('#modal_form2').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Position'); // Set title to Bootstrap modal title
       },
	    error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
     });
	}
	
	function edit_merk(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('goods_merk/c_goods_merk/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_merk"]').val(data.id_merk);
            $('[name="id_category"]').val(data.id_category);
			$('[name="name_merk"]').val(data.name_merk);
            
            $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Merk'); // Set title to Bootstrap modal title
			

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



     function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('goods_merk/c_goods_merk/add_merk')?>";
      }
      else
      {
		   url = "<?php echo site_url('goods_merk/c_goods_merk/update_merk')?>";
      }
		 var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
              if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
			
        });
    }

	function save2()
    {
      var url;
      url = "<?php echo site_url('goods_merk/c_goods_merk/update_position')?>";
      var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
              if(data=='Insert'){
				$('#modal_form2').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }

    function delete_merk()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('goods_merk/c_goods_merk/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}   
    }
	
  </script>
  
</html>
