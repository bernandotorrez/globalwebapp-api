<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_goods_merk extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_goods_merk');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_goods_merk->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}	
	 	}
		
	public function index()
		{

		$data['merk']=$this->M_goods_merk->get_all_merk();
		$data['total']=$this->M_goods_merk->get_count_id();
		$data['category']=$this->M_goods_merk->get_all_category();
		$data['show_view'] = 'goods_merk/V_goods_merk';
		$this->load->view('dashboard/Template',$data);		
		}

	public function ajax_list()
	{
		$list = $this->M_goods_merk->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_goods_merk) {
			$chk_idmaster ='<div align="center"><input id="checkmerk" name="checkmerk" type="checkbox" value='.$tbl_goods_merk->id_merk.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_goods_merk->id_merk;
			$row[] = $tbl_goods_merk->name_category;
			$row[] = $tbl_goods_merk->name_merk;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_goods_merk->count_all(),
						"recordsFiltered" => $this->M_goods_merk->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_merk');
		$this->db->from('tbl_goods_merk');
		$total = $this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_merk",$search);
		$this->db->or_like("id_category",$search);
		$this->db->or_like("name_merk",$search);
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_merk','ASC');
		$query=$this->db->get('tbl_goods_merk');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("id_merk",$search);
		$jum=$this->db->get('tbl_goods_merk');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $tbl_goods_merk) {
		$chk_idmaster ='<div align="center"><input id="checkmerk" name="checkmerk" type="checkbox" value='.$tbl_goods_merk["id_merk"].' class="editRow ace" req_id_del='.$tbl_goods_merk["id_merk"].' />
          <span class="lbl"></span> ';

			$output['data'][]=array($tbl_goods_merk['id_merk'],$tbl_goods_merk['id_category'],$tbl_goods_merk['name_merk'],$chk_idmaster
									);

		}

		echo json_encode($output);


	}

										
	public function add_merk()
		{
		if(empty($_POST["id_category"])){
			die ("Field ID Category must be filled in!! ");
		}
		elseif(empty($_POST["name_merk"])){
			die ("Field Name Merk must be filled in!! ");
		}
		else{
			$data = array(
					'id_category' => $this->input->post('id_category'),
					'name_merk' => $this->input->post('name_merk'),
				);
			$insert = $this->M_goods_merk->add_merk($data);
		  }
		  echo 'Insert';
		}
	
	public function view_category()
		{
			$this->db->from('tbl_goods_category');
			$query=$this->db->get();
			return $query->result();
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_goods_merk->get_by_id($id);
			echo json_encode($data);
		}
		
	public function ajax_category()
		{
			$data = $this->M_goods_merk->get_by_aaa();
			echo json_encode($data);
		}
	
	public function ajax_parentnew()
    {
        $id = $this->input->post('id_category');
        $data['child'] = $this->M_goods_merk->get_child($id);
		$data['child2'] = $this->M_goods_merk->get_all_count($id);
        $this->load->view('child',$data);
    }

	public function update_merk()
		{
		if(empty($_POST["id_category"])){
			die ("Field ID Category must be filled in!! ");
		}
		elseif(empty($_POST["name_merk"])){
			die ("Field Name Merk must be filled in!! ");
		}
		else{
		$data = array(
					'id_category' => $this->input->post('id_category'),
					'name_merk' => $this->input->post('name_merk'),
				);
		$this->M_goods_merk->update_merk(array('id_merk' => $this->input->post('id_merk')), $data);
		  }
		  echo 'Insert';
		}
	
	
	public function update_position()
		{
				$count = 0;
				$displayed_city = array();
				$city = array();

				foreach ($_POST['pos'] as $pos_key => $pos_value){
				  $jumlah = count($_POST['pos']); 
				  $count	++;
				  $city = '';
				  if( isset($pos_value)){
					$city = $pos_value;
					if(!in_array($city, $displayed_city)){
					  $displayed_city[] = $city;
					  $total = count($displayed_city);
					}
				  }
		}
		 if($total!=$jumlah){
			die('Duplicate Data !');
			
		  }else{
			echo "Insert";
			   foreach ($_POST['pos'] as $pos_key => $pos_value){
				$dede[$pos_key] = $pos_value;
				$parentnya['order_child'] = $pos_value;
				$this->db->where("id_merk",$pos_key);
				$this->db->update("tbl_goods_merk",$parentnya);
			 
		   }
}
}
		
			
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array (
						'status' => '0',
				);
				$this->db->where('id_merk', $id);
				$this->db->update('tbl_goods_merk', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}



}
