<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>

<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>
<div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Info:</span>
  <?php  echo $this->session->flashdata('pesan');	?>
</div>
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>
<div class="alert alert-info" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Info:</span>
  <?php  echo $this->session->flashdata('pesan_succes');	?>
</div>
<?php } ?>

<?php
  $access = $this->session->userdata('apps_accsess');
?>

<?php  echo form_open('inv_receipt/c_inv_receipt/multiple_submit');  ?>

<!-- <table>
  <tr>
    <td>
      <button class="btn btn-app btn-danger btn-xs radius-4 btnclear btn-clean" type="submit" id="btnclearadjust"
        name="btnclearadjust" value="Clear Adjustment" disabled="disabled" />
      <i class="ace-icon fa fa-exchange bigger-160"></i>
      Clear Adj
      </button>
    </td>
    <td>&nbsp;</td>
  </tr>
</table> -->
<br />

<div class="table-header btn-info"> <?php echo " ".$header ;?></div>
<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">

  <table id="myTable" class="table table-striped table-bordered table-hover">
    <thead align="center">
      <th> PP No </th>
      <th>Detail INV</th>
      <th>Print INV</th>

      <?php if($access == 1 || $access == 2) { ?>
        <th>Give Right</th>
      <?php } ?>

      <th>Company</th>
      <th>Subbmision</th>
      <th>Dept</th>
      <th>Description</th>
      <th>Vendor</th>
      <th>Date Sent</th>
      <th>Term</th>
      <th>Type</th>
      <th>Curr</th>
      <th>Total </th>
      <th>Total+PPN </th>
    </thead>
  </table>

</div>
</div>

</div>
</div>



<!-- Modal -->
<div class="modal fade" id="modaldetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
            class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Info & Remarks</h4>
      </div>

      <div class="modal-body">
        <!-- body di kosongin buat data yg akan di tampilkan dari database -->
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->


<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
  $('.datepicker').datetimepicker({
    language: 'id',
    weekStart: 1,
    todayBtn: 1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0
  });
</script>

<script>
  $(function () {
    $("#txtcari").focus();
  });
  //set focus ----------------------------  
</script>


<script>
  $(function () { // Show modal pop up send noted BPK print
    $(document).on('click', '.bpkprint', function (e) {
      var req_id = $(this).attr(
      'req_id'); //atribut from <a ref php echo $row->id_master ke variable req_id  ;																	
      var req_gtotal = $(this).attr('req_gtotal');
      var req_ppn = $(this).attr('req_ppn')
      var req_pph = $(this).attr('req_pph');
      var req_gtotalppn = $(this).attr('req_gtotalppn');
      var req_gtotal_adjust = $(this).attr('req_gadjust');
      var req_note = $(this).attr('req_note');

      if (req_gtotal_adjust ==
        "") { // jika grand total adjustment ada maka tampilkan information already exsist
        $('#tdinfo').hide('fast');
        $('#imgsucces').hide('fast');
        $('#note_row').show('fast');
        $('#table_detail_adjust').show('fast');
        $('#btnprintbpk').show('fast');
      } else {
        $('#tdinfo').show('fast');
        $('#imgsucces').show('fast');
        $('#note_row').hide('fast');
        $('#table_detail_adjust').hide('fast');
        $('#btnprintbpk').hide('fast');
      }

      $("#myModal-bpkprint").modal('show');
      $('#txtppno').val(req_id); //menampilkan isi dari  idmaster kedalam inputbox 			
      $('#txtgtotal').val(numeral(req_gtotal).format(
      '0,0.00')); //menampilkan isi dari  grantotal kedalam inputbox 
      $('#txtppn').val(numeral(req_ppn).format('0,0.00')); //menampilkan isi dari  grantotal kedalam inputbox 
      $('#txtpph').val(numeral(req_pph).format('0,0.00')); //menampilkan isi dari  grantotal kedalam inputbox 
      $('#txtgtotalppn').val(numeral(req_gtotalppn).format(
      '0,0.00')); //menampilkan isi dari  grantotal kedalam inputbox 							
      $('#txtcathadjust').val(numeral(req_gtotal_adjust).format('0,0.00')); //  textbox txtcathadjust				
      $('#txtdesc1').val(''); //mengkosongkan description 1		
      $('#txtprice1').val(''); //mengkosongkan adjust 1		  				
      $('#txtdesc2').val(''); //mengkosongkan description 2					  				
      $('#txtprice2').val(''); //mengkosongkan adjust 2				  				
      $('#txtnote').val(req_note); //mengkosongkan note		  				
      $('#txtgadjust').val(''); //mengkosongkan total adjustmen				
    });
  });
</script>



<script>
  //setelah di print adjustment cecklist total adjust and hide text box adjust
  $(document).on('click', '.btn_dobpkprint', function (e) {
    var strgadjust = $('#txtgadjust').val();

    if (strgadjust != '') {
      $('#tdinfo').show('fast');
      $('#imgsucces').show('fast');
      $('#note_row').hide('fast');
      $('#table_detail_adjust').hide('fast');
      $('#btnprintbpk').hide('fast');
    }

  });



  //setelah di hide lalu di refresh page parent
  $('#myModal-bpkprint').on('hidden.bs.modal', function () { //refresh page on close modal
    location.reload();
  });


  var counterCheckbpk = 0;
  $('body').on('change', 'input[type="checkbox"]', function () {
    this.checked ? counterCheckbpk++ : counterCheckbpk--;
    counterCheckbpk == 1 ? $('#btnclearadjust').prop("disabled", false) : $('#btnclearadjust').prop("disabled",
      true);
  });
</script>

<script>
  //--------------------function disbaled enabled button with check book
  $(function () { //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function () {
      if ($('input[type="checkbox"]:checked').length >= 1) {
        $('.btnclear').removeAttr('disabled');
      } else {
        $('.btnclear').attr('disabled', 'disabled');
      }
    });
  });
</script>

<script>
  $(function () {
    $('#myform').on('submit', function (e) {
      e.preventDefault();
      var url = '<?php echo base_url("inv_receipt/c_inv_receipt/multiple_submit"); ?>';
      $.ajax({
        url: url, //this is the submit URL
        type: 'POST', //or POST
        data: $('#myform').serialize(),
        success: function (data) {}
      });
    });
  });
</script>

<script>
  function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }
</script>

<script script type="text/javascript">
  $(document).on('change', '#txtprice1', '#txtprice2', function (e) {
    var tr = $(this).parent().parent();
    var price = numeral().unformat($('#txtprice1').val());
    var price2 = numeral().unformat($('#txtprice2').val());
    total();
  });

  $(document).on('blur', '#txtprice1', function (e) {
    var tr = $(this).parent().parent();

    var price = $('#txtprice1').val();
    var price2 = $('#txtprice2').val();

    if (price != "") {
      $('#txtprice1').val(numeral(price).format('0,0.00'));
    }
    total(); //masuk ke fungsi total		
  });

  $(document).on('blur', '#txtprice2', function (e) {
    var tr = $(this).parent().parent();

    var price = $('#txtprice1').val();
    var price2 = $('#txtprice2').val();

    if (price2 != "") {
      $('#txtprice2').val(numeral(price2).format('0,0.00'));
    }
    total(); //masuk ke fungsi total		
  });


  function total() {

    var hasilprice = numeral().unformat($('#txtprice1').val());
    var hasilprice2 = numeral().unformat($('#txtprice2').val());
    var gtstatic = numeral().unformat($('#txtgtotalppn').val()); //textboxt grandtotal	
    var cathgadjust = numeral().unformat($('#txtcathadjust').val()); //textboxt grandtotal adjust		

    //---------------------------------------------


    if (hasilprice == "0" && hasilprice2 == "0") { //jika text boxt price satu dan dua kosong
      $('.hasil').val('');
      $('#txtprice1').val('');
      $('#txtprice2').val('');
    } else {
      if (cathgadjust != "0") {
        var amount = hasilprice + hasilprice2;
        var result_t = cathgadjust + amount;
      } else {
        var amount = hasilprice + hasilprice2;
        var result_t = gtstatic + amount;
      }
      $('.hasil').val(numeral(result_t).format('0,0.00'));
    }


  }
</script>

<script type="text/javascript">
  $("#myTable").DataTable({
    ordering: false,
    autoWidth: false,
    processing: true,
    serverSide: true,
    "scrollY": 250,
    "scrollX": true,
    ajax: {
      url: "<?php echo base_url('inv_receipt/c_inv_receipt/cath_data_fordatatables_inv') ?>",
      type: 'POST',
    }
  });

  //modal fpb list---------------------------------------
  $(function () {
    $(document).on('click', '.fpbbutton', function (e) {
      var req_id = $(this).attr('req_id');
      var url = '<?=base_url('inv_receipt/c_inv_receipt/get_recieved_modal');?>';

      $("#FpbPrintModal").modal('show');

      $.ajax({
        type: 'POST',
        url: url,
        data: {
          id_master: req_id
        },
        success: function (msg) {
          $('.modal-body').html(msg);
        }

      });
    });
  });
  //end fpblist-------------------------------------------------------------------------------

  // Give Right
  function giveRightINV(e) {
    var value = $(e).attr('req_id');
    var url = "<?=base_url('inv_receipt/c_inv_receipt/giveRightINV');?>";
    var acc = confirm("Give Right this INV?");
    //var tr =  $(e).parent().parent();

    if (acc == true) {

      $.ajax({
        url: url,
        data: {
          id_master: value
        },
        dataType: "json",
        type: "GET",
        beforeSend: function () {
          $(e).prop('disabled', true);
        },
        success: function (response) {
          //$('#btnsendtab').prop('disabled', false);

          if (response == 'Success') {
            alert('Successfully Re-Print this INV !');
            //tr.find('.btn-info').prop('disabled', false);
            location.reload();
          } else {
            alert('Failed Re-Print this INV !');
          }

          $(e).prop('disabled', false);

        }
      });

    }

    return false;
  }
  // End Give Right

  // Tampil Detail GRN
  function getDetailINV(e) {
    var value = $(e).attr('req_id'); 
    var url = "<?=base_url('inv_receipt/c_inv_receipt/getDetailINV');?>";
    //var acc = confirm("See Detail?");


    //if (acc == true) {
    //$("#FpbPrintModal").modal('hide');
    $('#modaldetail').modal('show');

    $.ajax({
      url: url,
      data: {
        id_master: value
      },

      type: "GET",
      beforeSend: function () {
        //$(e).prop('disabled', true);
      },
      success: function (msg) {
        console.log(msg)
        //$('#btnsendtab').prop('disabled', false);
        //$('#titleHistoryGRN').html('History GRN +')
        $('.modal-body').html(msg);

      }
    });

    //}

    return false;
  }
    // End Tampil Detail GRN
</script>