<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_inv_receipt extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('M_inv_receipt','',TRUE);	
		//$this->load->library('fpdf');   		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('terbilang');
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_inv_receipt->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser[0]->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}		
	}		  	
		
  public function index()
  {					    																
		$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_pp= $this->M_inv_receipt->tampil_table()->result();	
	  $total_rows =$this->M_inv_receipt->tampil_table()->num_rows();
	  									
	  if ($tampil_table_pp)
		{		
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;	
		$data['intno'] = ""; //variable buat looping no table.					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		$data['ceck_row'] = $total_rows;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'inv_receipt/V_inv_receipt';		
		$this->load->view('dashboard/Template',$data);				
	  }else{	
	    $dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;	
		$data['intno'] = ""; //variable buat looping no table.					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data PP table is empty';		
		$data['show_view'] = 'inv_receipt/V_inv_receipt';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }
      						 			
	
	public function do_search_date()  	
	{
	  $tampil_table_pp= $this->M_inv_receipt->get_search_date()->result();	
	  $total_rows =$this->M_inv_receipt->get_search_date()->num_rows();
	  
		  if ($tampil_table_pp)
		  {
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('company') ;		
			$data['pp_view'] =	 $tampil_table_pp ;		
			$data['intno'] = ""; //variable buat looping no table.	
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;														
			$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
			$data['show_view'] = 'inv_receipt/V_inv_receipt';		
			$this->load->view('dashboard/Template',$data);				
		  }else{	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('company') ;	
			$data['intno'] = ""; //variable buat looping no table.					
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;	
			$data['pesan'] = 'Data PP table is empty';		
			$data['show_view'] = 'inv_receipt/V_inv_receipt';
			$this->load->view('dashboard/Template',$data);				
		  } 
	}
	
	public function do_search()  	
	{
	   $tampil_table_pp= $this->M_inv_receipt->get_search()->result();	
	   $total_rows =$this->M_inv_receipt->get_search()->num_rows();
	    if ($tampil_table_pp)
		  {
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;		
			$data['pp_view'] =	 $tampil_table_pp ;		
			$data['intno'] = ""; //variable buat looping no table.	
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;														
			$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
			$data['show_view'] = 'inv_receipt/V_inv_receipt';		
			$this->load->view('dashboard/Template',$data);				
		  }else{	
			$dept  = $this->session->userdata('dept') ;	
			$branch  = $this->session->userdata('name_branch') ; 
			$company = $this->session->userdata('short') ;	
			$data['intno'] = ""; //variable buat looping no table.					
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
			$data['ceck_row'] = $total_rows;	
			$data['pesan'] = 'Data PP table is empty';		
			$data['show_view'] = 'inv_receipt/V_inv_receipt';
			$this->load->view('dashboard/Template',$data);				
		  } 
	  
	}
	
	//------------------------------------pashing result to modal popup-------------------
	public function get_nopp_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $nopp = $this->input->post('idmaster',TRUE); //       
	     $query_master = $this->db->query("select id_master,remarks,gran_total,ppn,pph,gran_totalppn,gran_total_adjust,currency from qv_head_pp_complite where id_master ='".$nopp."'");  
         $query_detail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   
		   
		      $data['str_pp_master']=$query_master->result();
			  $data['str_pp_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_nopp'] = $nopp ;
			  $tampil_detail = $this->load->view('inv_receipt/V_conten_detail',$data,true);
			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end phasing--------------------
	
	 public function do_print_bpk()
  	{		
		if($this->M_inv_receipt->select_data_rpt_bpk())
		 {	
															
			 $res['currency'] = $this->M_inv_receipt->select_query_rpt_bpk();				  
			 $res['data'] = $this->M_inv_receipt->select_query_rpt_bpk();	
			 
			 //array[data_detail_adjust] buat nampilin detail adjustment finance yang di input di form.			
			 $res['data_detail_adjust']=$this->M_inv_receipt->select_query_rpt_detail_adjustment();			
			 //end--------------------------------------------------------------------------------------
			 $this->load->view('inv_receipt/V_rptpdf_bpk',$res);				 				  
		  }else{     		
			 $this->session->set_flashdata('pesan_rpt','Print BPK Failed!!!!!! ....'); 					
			 redirect('inv_receipt/c_history_finance');	   
		  } 
		  
	}
	
	public function do_clear_adjust()
	{
		if($this->M_inv_receipt->clear_adjustment()) {
			
		    $this->session->set_flashdata('pesan_succes',"Clear Adjustmen Succesfully");
			 redirect('inv_receipt/c_history_finance');	  
		}else{
	         $this->session->set_flashdata('pesan',"Clear Adjustmen Failed"); 	
			  redirect('inv_receipt/c_history_finance');	  
		}
	}
	
	
	public function multiple_submit()
	{
		if ($this->input->post('btncaridate'))
		{
			$this->do_search_date();
		}else{
			if ($this->input->post('btncari'))
			{
			    $this->do_search();	
			}else{
				if ($this->input->post('btnprintbpk'))
			    {
			        $this->do_print_bpk();										
				}else{
					if ($this->input->post('btnclearadjust'))
			    	{
			        	$this->do_clear_adjust();	
					}else{
				    	redirect('inv_receipt/c_history_finance');
					}
				}
			}
		}
	}
	
	

	 
	 function cath_data_fordatatables_inv(){
		
		//  $str_flag_approval = $this->session->userdata('aproval_flag'); //1=B.o.D 2=F.C 3=Manager up 4 = purchase	
		  $str_idcompany = $this->session->userdata('id_company'); 
		  $str_iddept = $this->session->userdata('id_dept'); 
		  $str_status_send = "1";
		  $str_status_approved = "1";

		  $access = $this->session->userdata('apps_accsess');

		 /*Menagkap semua data yang dikirimkan oleh client*/
	
		 /*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		 server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		 sesuai dengan urutan yang sebenarnya */
		 $draw=$_REQUEST['draw'];
	
		 /*Jumlah baris yang akan ditampilkan pada setiap page*/
		 $length=$_REQUEST['length'];
	
		 /*Offset yang akan digunakan untuk memberitahu database
		 dari baris mana data yang harus ditampilkan untuk masing masing page
		 */
		 $start=$_REQUEST['start'];
	
		 /*Keyword yang diketikan oleh user pada field pencarian*/
		 $search=$_REQUEST['search']["value"];
	
		 //order short column
		 $column_order =array(null,"id_master","company","dept","vendor","user_submission","flag_purchase","header_desc","remarks","aprove_head","approve_purchasing","aprove_fc","aprove_bod","date_send_aproval","date_aproval","date_pp","time_approved","type_purchase","currency","gran_total","ppn","gran_totalppn","term_top");
		 
		 /*Menghitung total qv didalam database*/
		  $this->db->select('id_master');
		  $this->db->from('qv_head_pp_complite');
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
		  $this->db->where("flag_status_inv_receipt","1");
		  $this->db->where("flag_inv_receipt","0");
		  $this->db->where("status","1");
		  $this->db->like("id_master",$search);
		  $this->db->or_like("user_submission",$search);
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");	
		  $this->db->where("flag_status_inv_receipt","1");
		  $this->db->where("flag_inv_receipt","0");			
		  $this->db->where("status","1");
		  $this->db->or_like("header_desc",$search);	
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
		  $this->db->where("flag_status_inv_receipt","1");
		  $this->db->where("flag_inv_receipt","0");
		  $this->db->where("status","1");
		  $this->db->or_like("short",$search);	
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
		  $this->db->where("flag_status_inv_receipt","1");
		  $this->db->where("flag_inv_receipt","0");
		  $this->db->where("status","1");
		  $this->db->or_like("vendor",$search);	
		  $this->db->where("status_send_aprove",$str_status_send);
		  $this->db->where("id_company",$str_idcompany);
		  //$this->db->where("id_dept",$str_iddept);
		  $this->db->where("aprove_bod","1");
		  $this->db->where("flag_status_inv_receipt","1");
		  $this->db->where("flag_inv_receipt","0");
		  $this->db->where("status","1");
		  $total = $this->db->count_all_results();
	
		 /*Mempersiapkan array tempat kita akan menampung semua data
		 yang nantinya akan server kirimkan ke client*/
		 $output=array();
	
		 /*Token yang dikrimkan client, akan dikirim balik ke client*/
		 $output['draw']=$draw;
	
		 /*
		 $output['recordsTotal'] adalah total data sebelum difilter
		 $output['recordsFiltered'] adalah total data ketika difilter
		 Biasanya kedua duanya bernilai sama, maka kita assignment 
		 keduaduanya dengan nilai dari $total
		 */
		 $output['recordsTotal']=$output['recordsFiltered']=$total;
	
		 /*disini nantinya akan memuat data yang akan kita tampilkan 
		 pada table client*/
		 $output['data']=array();
	
	
		 /*Jika $search mengandung nilai, berarti user sedang telah 
		 memasukan keyword didalam filed pencarian*/
		 if($search!=""){
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("flag_status_inv_receipt","1");
		  			$this->db->where("flag_inv_receipt","0");
				  $this->db->where("status","1");
				  $this->db->like("id_master",$search);
				  $this->db->or_like("user_submission",$search);
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);		
				  $this->db->where("aprove_bod","1");	
				  $this->db->where("flag_status_inv_receipt","1");
		  			$this->db->where("flag_inv_receipt","0");	
				  $this->db->where("status","1");
				  $this->db->or_like("header_desc",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("flag_status_inv_receipt","1");
		  			$this->db->where("flag_inv_receipt","0");
				  $this->db->where("status","1");
				  $this->db->or_like("short",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("flag_status_inv_receipt","1");
		  			$this->db->where("flag_inv_receipt","0");
				  $this->db->where("status","1");
				  $this->db->or_like("vendor",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("flag_status_inv_receipt","1");
		  			$this->db->where("flag_inv_receipt","0");
				  $this->db->where("status","1");
		 }
	
			  /*Lanjutkan pencarian ke database*/
			  $this->db->limit($length,$start);		
			  $this->db->where("status_send_aprove",$str_status_send);
			  $this->db->where("id_company",$str_idcompany);
			  //$this->db->where("id_dept",$str_iddept);			 
			  $this->db->where("aprove_bod","1");
			  $this->db->where("flag_status_inv_receipt","1");
		  			$this->db->where("flag_inv_receipt","0");
			  /*Urutkan dari alphabet paling terkahir*/
			  $this->db->where("status","1");
			  // $this->db->order_by('date_pp','DESC');
			 
			  //order column
			  if (isset($_REQUEST["order"])){
				   $this->db->order_by($column_order[$_REQUEST["order"][0]["column"]],$_REQUEST["order"][0]['dir']);
			  }else{
				   $this->db->order_by('aprove_head','desc');
				   $this->db->order_by('approve_purchasing','desc');
				   $this->db->order_by('aprove_fc','desc');
				   $this->db->order_by('aprove_bod','desc');
			   }
		   
			   $this->db->order_by('aprove_head','asc');
			   $this->db->order_by('approve_purchasing','asc');
			   $this->db->order_by('aprove_fc','asc');
			   $this->db->order_by('aprove_bod','asc');	
			   $this->db->order_by('date_send_aproval','DESC');  
			   $query=$this->db->get('qv_head_pp_complite');
	
	
		 /*Ketika dalam mode pencarian, berarti kita harus mengatur kembali nilai 
		 dari 'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		 yang mengandung keyword tertentu
		 */
		 
		 if($search!=""){
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("flag_status_inv_receipt","1");
		  			$this->db->where("flag_inv_receipt","0");
				  $this->db->where("status","1");
				  $this->db->like("id_master",$search);
				  $this->db->or_like("user_submission",$search);
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);		
				  $this->db->where("aprove_bod","1");	
				  $this->db->where("flag_status_inv_receipt","1");
		  			$this->db->where("flag_inv_receipt","0");	
				  $this->db->where("status","1");
				  $this->db->or_like("header_desc",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("flag_status_inv_receipt","1");
		  			$this->db->where("flag_inv_receipt","0");
				  $this->db->where("status","1");
				  $this->db->or_like("short",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("aprove_bod","1");
				  $this->db->where("flag_status_inv_receipt","1");
		  			$this->db->where("flag_inv_receipt","0");
				  $this->db->where("status","1");
				  $this->db->or_like("vendor",$search);	
				  $this->db->where("status_send_aprove",$str_status_send);
				  $this->db->where("id_company",$str_idcompany);
				  //$this->db->where("id_dept",$str_iddept);
				  $this->db->where("aprove_bod","1");
				  $this->db->where("flag_status_inv_receipt","1");
		  			$this->db->where("flag_inv_receipt","0");
				  $this->db->where("status","1");
				 
				  $jum=$this->db->get('qv_head_pp_complite');
				  $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		 }
	
	
	
	
		 
		 foreach ($query->result_array() as $row_tbl) {
			  
			  If ($row_tbl['attach_quo'] != "")
			   {  
				$attach_quo = '<div  align="center"><a href='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-yellow btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".''.'</a></div></center>' ; //button qutattion
			   }else{
				 $attach_quo = '<div style=" color:#EB293D" align="center">'."No Quo".'</div>';	  
			   }
		 
			
			
			  $btn_view_detail = '<div  align="center"><a href="#" class="btn btn-app btn-info btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <i class="glyphicon glyphicon-edit " aria-hidden="true"></i></a></center>';//button detail
			
			
				
				 
				
				   If ($row_tbl['aprove_bod'] == "1")
				  {
						  $bod_approval ="<center><span class='btn btn-info glyphicon glyphicon-ok btn-xs' aria-hidden='true'></span></center>";
					 
				   }
				  
			 
	$strbodaprove  = $row_tbl["aprove_bod"] ;
	$strpurchaseaprove = $row_tbl["approve_purchasing"] ;
	$strfcaprove   = $row_tbl["aprove_fc"] ;
	$strheadaprove = $row_tbl["aprove_head"] ;			
				
	
	//Pembedaan Warna pada type purchase.
			if ($row_tbl['flag_purchase'] == "1") {
				$type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
			}else{
				  if ($row_tbl['flag_purchase'] == "2") {
						$type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
					}else{
						  if ($row_tbl['flag_purchase'] == "3") {
							$type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
						}else{
							  if ($row_tbl['flag_purchase'] == "4") {
									$type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
								}else{
									 $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
							  };
					  };
				  };   
			  };
	//end------------------------------			 
	
	
	//label colum 1 idepp
	$idmas = "<span class='label label-primary arrowed-in-right arrowed'>".$row_tbl['id_master']."</span>"; //id PP
	
	//end label
	$header_desc = "<div align= 'center' style='color:#2292d8'>".$row_tbl['header_desc']."</div>";
	
	//tombol fpb......
	//$strprintfbp_ok = '<button class="btn btn-app btn-success btn-xs radius-4 btnpo btn-clean fpbbutton" type="button" id="fpbbutton" name="fpbbutton" req_id ='.$row_tbl["id_master"].' onclick="window.location.href="" ><i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i> </button>';
	
	$strprintinv = '<a target="_blank" href="'.base_url('inv_receipt/c_inv_receipt/print_inv?pp=').base64_encode($row_tbl["id_master"]).'" class="btn btn-app btn-success btn-xs radius-4"><i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i></a>';
	//end---------------------------


	// Button Detail Inv
	$btn_detail_inv = '<button type="button" class="btn btn-app btn-info btn-xs radius-4" onClick="getDetailINV(this)" req_id="'.base64_encode($row_tbl["id_master"]).'"><i class="ace-iconfa glyphicon glyphicon-print   bigger-160"></i></button>';
	// Button Detail Inv

	// Button Give Right
	$btn_give_right = '<button type="button" class="btn btn-success" onClick="giveRightINV(this)" req_id="'.base64_encode($row_tbl["id_master"]).'">Give Right</button>';
	// Button Give Right

	if($access == 1 || $access == 2) {
		$output['data'][]=array(
			$idmas,
		   // $btn_view_detail,
		   // $attach_quo,
		    $btn_detail_inv,
			$strprintinv,
			$btn_give_right,
			$row_tbl['short'],
			$row_tbl['user_submission'],
			$row_tbl['dept'],
			$header_desc ,//$row_tbl['header_desc'],
			$row_tbl['vendor'],																								
			date('d-m-Y', strtotime($row_tbl['date_pp'])),
			$row_tbl['term_top']." "."Days",									 
			$type_purchase, //$row_tbl['type_purchase'],
			$row_tbl['currency'],
			number_format($row_tbl['gran_total'],2,'.',','),
			number_format($row_tbl['gran_totalppn'],2,'.',','),
					
	  );	
	} else {
		$output['data'][]=array(
			$idmas,
		   // $btn_view_detail,
		   // $attach_quo,
		    $btn_detail_inv,
			$strprintinv,
			$row_tbl['short'],
			$row_tbl['user_submission'],
			$row_tbl['dept'],
			$header_desc ,//$row_tbl['header_desc'],
			$row_tbl['vendor'],																								
			date('d-m-Y', strtotime($row_tbl['date_pp'])),
			$row_tbl['term_top']." "."Days",									 
			$type_purchase, //$row_tbl['type_purchase'],
			$row_tbl['currency'],
			number_format($row_tbl['gran_total'],2,'.',','),
			number_format($row_tbl['gran_totalppn'],2,'.',','),
					
	  );	
	}
		  
					
		 }
		 echo json_encode($output);
	 }

	 public function getDetailINV() {
		$status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
		 $nopp = base64_decode($this->input->get('id_master',TRUE)); //  
	     $query_master = $this->db->query("select id_master,remarks,gran_total,ppn,pph,gran_totalppn,gran_total_adjust,currency from qv_head_pp_complite where id_master ='".$nopp."'");  
         $query_detail = $this->db->query("select * from tbl_detail_pp where id_master ='".$nopp."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   
		   
		      $data['str_pp_master']=$query_master->result();
			  $data['str_pp_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_nopp'] = $nopp ;
			  $tampil_detail = $this->load->view('inv_receipt/V_conten_detail',$data,true);
			 
			  echo $tampil_detail ;			 
		   }			
	 }
 
	 public function get_recieved_modal(){
		$id_master = $this->input->post('id_master');

	

		$getdatadetailgrn = $this->M_inv_receipt->getListHistoryParsial($id_master);
		$access = $this->session->userdata('apps_accsess');
		$no="1";	 	
		echo "<div style='overflow-x:auto;'>" ;
		echo "<table class='table table-striped table-bordered table-hover'>";
		echo  "<tr style='font-weight:bold; font-size:11px'>";
		echo  "<td>No</td>";
		echo  "<td >GRN No</td>";
		echo  "<td>Date GRN</td>";
		echo  "<td >Grand Total</td>";
		echo  "<td >Detail</td>";
		echo  "<td >Print</td>";

		if($access == 1 || $access == 2) {
			echo  "<td >Give Right</td>";
		}
		
		echo  "</tr> "; 	 	 	 				
		 foreach ($getdatadetailgrn as $row_det_grn) {
			 $no_pp = base64_encode($row_det_grn->id_master);
			 $no_pp = base64_encode($row_det_grn->number_grn);
			$url = base_url('inv_receipt/c_history_finance/print_bpk?pp='.$no_pp.'&grn='.$no_pp);
			if($row_det_grn->flag_print_bpk == '0') {
				$button_print = '<a target="_blank" OnClick=\'return confirm("Print this Data?");\' href="'.$url.'" class="btn btn-info">
				<span class="fa fa-print"></span> Print BPK
				</a>';
			} else {
				$button_print = '<button type="button" class="btn btn-info" disabled>
				<span class="fa fa-print"></span> Print BPK
				</button>';
			}

			 echo  "<tr style='font-size:12px'> ";
			 echo '<td>'.$no++.'</td>';	
			 echo '<td>'.$row_det_grn->number_grn.'</td>';		
			 echo '<td>'.$row_det_grn->date_grn.'</td>';
			 echo '<td>'.number_format($row_det_grn->gran_total,2,'.',',').'</td>';
			 echo '<td>
			 		<button type="button" class="btn btn-success" onClick="tampilDetailGRN(this)" req_id='.$row_det_grn->number_grn.'>
					<span class="fa fa-search"></span> Detail
					</button>

					
					</a>
				  </td>';
			 echo '<td>
			 		'.$button_print.'
				  </td>';

				if($access == 1 || $access == 2) {
					echo '<td><button type="button" class="btn btn-success" onClick="giveRightINV(this)" req_id='.$row_det_grn->number_grn.'>Give Right</button></td>';
				}

			
			 echo "</tr>"; 							
			 
		 }

		 

		 echo '</div>';

	}
	
	public function giveRightINV() {
		$id_master = base64_decode($this->input->get('id_master'));

		$data = array('flag_print_inv_receipt' => '0'
					);

		$update = $this->M_inv_receipt->giveRightINV($id_master, $data);
		
		if($update) {
			echo json_encode('Success');
		} else {
			echo json_encode('Failed');
		}
	}



	public function get_history_grn()
    {		
	
		$no_pp = $this->input->get('number_grn');
     $master_pp = $this->M_inv_receipt->getMasterGRN($no_pp);
	 $detail_pp = $this->M_inv_receipt->getDetailGRN($no_pp);	 
	  
	 $no="1";	 	
	 echo  "<div style='overflow-x:auto'>";
	 echo  "<table class ='table table-striped table-bordered table-hover'>";
	 echo  "<thead>" ;
	 echo  "<tr style='font-weight:bold; font-size:11px'>";
	 echo  "<td width='2%'>No</td>";
	 echo  "<td width='22%'>Description</td>";
	 echo  "<td width='22%'>Specs</td>";
	 echo  "<td width='7%'>Qty</td>";
	 echo  "<td width='15%'>Prices</td>";
	 echo  "<td width='25%'>Total Prices</td>";
	 echo  "<td width='7%'>PPN</td>";
	 echo  "<td width='7%'>PPH</td>";
	 echo  "<td width='25%'>PPN Amount</td>";
	 echo  "<td width='25%'>PPH Amount</td>";
	 echo  "<td width='25%'>Total + Tax</td>";
	 echo  "</tr> "; 
	 echo  "</thead>";	 	 	 				
		foreach ($detail_pp as $row_jq) {
			$total_plus_tax = $row_jq->amount_in + ($row_jq->tax_ppn - $row_jq->tax_pph);
			

		    echo  "<tr style='font-size:12px'> ";
			echo '<td>'.$no++.'</td>';			
			echo '<td>'.$row_jq->description.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			echo '<td>'.$row_jq->qty_in.'</td>';
			echo '<td>'.number_format($row_jq->price,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->amount_in,2,'.',',').'</td>';
			echo '<td>'.$row_jq->type_ppn.'</td>';
			echo '<td>'.$row_jq->type_pph.'</td>';
			echo '<td>'.number_format($row_jq->tax_ppn,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->tax_pph,2,'.',',').'</td>';
			echo '<td>'.number_format($total_plus_tax,2,'.',',').'</td>';
		    echo "</tr>"; 							
			
		}
				
	 foreach ($master_pp as $row_jm) {	
		
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>Sub Total :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_amount,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>PPN Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_ppn,2,'.',',')."</td>";	
		 echo  "</tr> "; 	
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>PPH Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->total_pph,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 echo  "<tr style='font-weight:bold'>";
		 echo  "<td width='25%' colspan='10' align='right'>Total + Tax :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_total,2,'.',',')."</td>";	
		 echo  "</tr> "; 								
		 
	 }

	 echo "</table>";	
		 echo "</div>";
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}

	public function print_inv() {

		$no_pp = base64_decode($this->input->get('pp'));

		$master_pp = $this->M_inv_receipt->getDataMasterPPForPrint($no_pp)[0];
		$detail_pp = $this->M_inv_receipt->getDetailPP($no_pp);

		// Cek apakah sudah di print atau belum

		if($master_pp->flag_print_inv_receipt > 0) {
			echo '<a style="font-size: 24px" href="'.base_url('inv_receipt/c_inv_receipt').'"> This Data has been Printed!, please click to back </a>';
			die;
		}

			// Dummy update flag Print Inv Receipt
			$data = array('flag_print_inv_receipt' => '1',
							'flag_inv_receipt' => '1',
							'date_print_inv_receipt' => date('Y-m-d H:i:s')
			);

		$update = $this->M_inv_receipt->updateFlagPrint($no_pp, $data);
		// Dummy update flag Print Inv Receipt
		
		$this->load->library('Pdf');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator('IT @Eurokars Group Indonesia');
        $pdf->SetAuthor('IT @Eurokars Group Indonesia');
        $pdf->SetTitle("Form Invoice Receipt - ".$no_pp);
        $pdf->SetSubject("Form Invoice Receipt - ".$no_pp);

         // set header and footer fonts
         $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
         $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
 
         // set default monospaced font
         $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 
         // set margins
         $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
         $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
         $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
 
         // set auto page breaks
         $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
         $pdf->setPrintHeader(false);
         //$pdf->setPrintFooter(false);
 
         // set image scale factor
 
 
         // set some language-dependent strings (optional)
         if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
             require_once(dirname(__FILE__).'/lang/eng.php');
             $pdf->setLanguageArray($l);
         }
                 $pdf->SetFont('dejavusans', '', 9);

                 $pdf->SetPrintHeader(false);
                 $pdf->SetPrintFooter(false);
 
         // add a page
         $pdf->AddPage();

         $html = '
        
         <!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<style type="text/css">
		.center {
			text-align: center;
		}

		.judul {
			text-align: center;
			font-weight: bold;
			font-size: 10px;
		}

        .garis_bawah {
            border-bottom: 1px solid black;
        }

        .disetujui { 
            text-decoration: line-through;
        }

	</style>

</head>

<body>


	<br>

	<table width="100%" border="0">
		<tr>
			<td> <img src="'.base_url('asset/images/euro.jpg').'" height="20"/> </td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>PT. '.$master_pp->company.'</td>
			<td align="right">No : '.$no_pp.'</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Departement : '.$master_pp->dept.'</td>
			<td align="right">Printed Date : '.date('d-m-Y').'</td>
		</tr>
	</table>

	<br>
	<br>

	<table width="100%" border="0" class="center">
		<tr>
			<td width="5%"></td>
			<td width="90%" class="judul">

			<u>FORM INVOICE RECEIPT</u>
				

			</td>

			<td width="5%"></td>
		</tr>
	</table>

	<br>
	<br>

	<table width="100%" border="0">
		<tr>
		
			<td>Payment To : '.$master_pp->vendor.'</td>
			
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Amount Of Money : '.$master_pp->currency.' '.number_format($master_pp->gran_total,2,'.',',').'</td>
		</tr>
	</table>

	<br>
	<br>

	<table width="100%" border="1">
		<tr align="center">
		
			<th width="5%">No</th>
			<th width="18%">Description</th>
			<th width="18%">Specs</th>
			<th width="5%">Qty</th>
			<th width="18%">Price</th>
			<th width="18%">Total</th>
			<th width="18%">Total + Tax</th>
		</tr>

		
		';
		$no = 1;
		$tax_ppn_pph = $master_pp->ppn - $master_pp->pph;
		$gran_total_ppn_pph = $master_pp->gran_total - $tax_ppn_pph;
		foreach($detail_pp as $det) { 
		$total_plus_tax = $det->total + ($det->tax_detail - $det->tax_detailpph);
		$html .= '
		<tr >
			<th align="center"> '.$no++.'</th>
			<th align="center"> '.$det->desc.' </th>
			<th align="center"> '.$det->spec.' </th>
			<th align="center"> '.$det->qty.' </th>
			<th align="right"> '.number_format($det->harga,2,'.',',').' &nbsp; </th>
			<th align="right"> '.number_format($det->total,2,'.',',').' &nbsp; </th>
			<th align="right"> '.number_format($total_plus_tax,2,'.',',').' &nbsp; </th>
		</tr>
		';

		}

		$html .= '

		<tr>
			<td colspan="6" align="right"><b>Grand Total : '.$master_pp->currency.' &nbsp; </b></td>
			<td align="right"> <b>'.number_format($master_pp->gran_total,2,'.',',').' &nbsp; </b> </td>
		</tr>

		<tr>
			<td colspan="6" align="right"><b>PPN Amount :  &nbsp; </b></td>
			<td align="right"> <b>'.number_format($master_pp->gran_totalppn,2,'.',',').' &nbsp; </b> </td>
		</tr>

		<tr>
			<td colspan="6" align="right"><b>PPH Amount :  &nbsp; </b></td>
			<td align="right"> <b>'.number_format($master_pp->gran_total_adjust,2,'.',',').' &nbsp; </b> </td>
		</tr>

		<tr>
			<td colspan="6" align="right"><b>Total + Tax :  &nbsp; </b></td>
			<td align="right"> <b>'.number_format($gran_total_ppn_pph,2,'.',',').' &nbsp; </b> </td>
		</tr>

		<tr>
			<td colspan="7"> <b> Mention : '.terbilang($master_pp->gran_total).' &nbsp; </b> </td>
		</tr>

	</table>

        
    <br>
    <br>
    <br>

    
	<table width="100%" border="1" align="center">
		
		<tr>
			<th colspan="2"> <b> TTD Giro / Cheque </b> </th>
			<th rowspan="2"> <b> Pimpinan </b> </th>
			<th rowspan="2"> <b> Pemeriksa II </b> </th>
			<th rowspan="2"> <b> Pemeriksa I  </b> </th>
			<th rowspan="2"> <b> Accounting  </b> </th>
			<th rowspan="2"> <b> Finance  </b> </th>
			<th rowspan="2"> <b> Penerima  </b>  </th>
		
		</tr>

		<tr>
			<td> I </td>
			<td> II </td>
		</tr>

		<tr>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
			<td> <br><br>  <br><br></td>
		</tr>

		<tr>
			<td> '.$this->session->userdata('sign_giro1').' </td>
			<td> '.$this->session->userdata('sign_giro2').' </td>
			<td> '.$this->session->userdata('sign_pimpinan').' </td>
			<td> '.$this->session->userdata('sign_pemeriksa2').' </td>
			<td> '.$this->session->userdata('sign_pemeriksa1').' </td>
			<td> '.$this->session->userdata('sign_accounting').' </td>
			<td> '.$this->session->userdata('sign_finance').' </td>
			<td> '.$this->session->userdata('sign_penerima').' </td>
		</tr>

	</table>
    

</body>

</html>

                 
         ';

         // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        $file = 'Form Invoice Receipt - '.$no_pp.'.pdf';

        // reset pointer to the last page
        $pdf->lastPage();

        $pdf->Output($file, 'I');
	}
	
}

