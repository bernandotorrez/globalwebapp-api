<form method="post" id="request-form">

    <input type="hidden" id="cek_validasi" value="" />
    <input type="hidden" id="sign_req" name="sign_req" value="<?=$data_request->sign_req;?>" />
    <input type="hidden" id="sign_1" name="sign_1" value="<?=$data_request->sign_1;?>" />
    <input type="hidden" id="sign_2" name="sign_2" value="<?=$data_request->sign_2;?>" />
    </div>

    <div class="table-header btn-info text-center">
        <?php //echo " ".$header ;?>

        Formulir Permintaan Persetujuan
        <p>
            Penggunaan Kendaraan Demo Operasional Perusahaan
        </p>
    </div>

 
    <!-- Responsive Form for Mobile and Web Start -->
    <div class="container">

        <!-- Row Start -->
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label class="control-label">ID Req Demo Car</label>
                    <input type="text" id="id_req_democar" name="id_req_democar" data-title="ID Req Demo Car"
                            class="form-control" value="<?=$data_request->id_req_democar_group;?>" readonly="readonly" />
                </div>
            </div>

            <div class="col-sm-1">


            </div>

            <div class="col-sm-5">
                <div class="form-group">
                    <label for="Nama Requester" style="color:#900">Nama Requester *</label>

                    <input type="text" id="nama_requester" name="nama_requester" data-title="Nama Requester"
                            class="form-control validation" placeholder="Nama Requester" 
                            value="<?=$data_request->nama_requester;?>"
                            />
                </div>
            </div>

        </div>
        <!-- Row End -->

        <!-- Row Start -->
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label for="No KTP/ID" style="color:#900">No KTP/ID *</label>

                    <input type="text" onkeypress="return isNumberKey(event)" id="no_ktp" name="no_ktp" data-title="No KTP/ID"
                            class="form-control validation" placeholder="No KTP/ID" 
                            value="<?=$data_request->no_ktp;?>"
                            />

                </div>
            </div>

            <div class="col-sm-1">


            </div>

            <div class="col-sm-5">
                <div class="form-group">
                    <label for="Alamat" style="color:#900">Alamat *</label>

                    <textarea class="form-control validation" id="alamat" name="alamat" data-title="Alamat" rows="5"
                            placeholder="Alamat"><?=$data_request->alamat;?></textarea>

                </div>
            </div>

        </div>
        <!-- Row End -->

        <!-- Row Start -->
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label for="No Telepon/HP" style="color:#900">No Telepon/HP *</label>

                    <input type="text" onkeypress="return isNumberKey(event)" id="no_hp" name="no_hp" data-title="No HP" class="form-control validation"
                            placeholder="No HP" 
                            value="<?=$data_request->no_hp;?>"
                            />

                </div>
            </div>

            <div class="col-sm-1">


            </div>

            <div class="col-sm-5">
                <div class="form-group">
                    <label for="Pilih Stock Demo Car" style="color:#900">Stock Demo Car *</label>

                    <select class="chosen-select form-control validation" id="stock_no" name="stock_no"
                            data-placeholder="- Stock No | Vin | No Polisi | Engine | Status Stock - " data-title="Stock Demo Car">
                            <option value="-"> - Stock No | Vin | No Polisi | Engine | Status Stock - </option>

                            <?php foreach($data_stock as $value_stock) { ?>

                            <option value="<?=base64_encode($value_stock->stock_no);?>"
                                <?php if($data_request->stock_no == $value_stock->stock_no) {
                                        echo 'selected';
                                    }
                                ?>
                            >
                                <?=$value_stock->stock_no;?> | <?=$value_stock->vin;?> | <?=$value_stock->no_polisi;?> |
                                <?=$value_stock->engine;?> | <?=$value_stock->status_stock;?>
                            </option>

                            <?php } ?>

                        </select>

                </div>
            </div>

        </div>
        <!-- Row End -->

        <!-- Row Start -->
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label for="Type Kendaraan" style="color:#900">Type Kendaraan *</label>

                    <input type="text" id="type_kendaraan" name="type_kendaraan" data-title="Type Kendaraan"
                            class="form-control validation" placeholder="Type Kendaraan" readonly 
                            value="<?=$data_request->type_name;?>"
                            />

                </div>
            </div>

            <div class="col-sm-1">


            </div>

            <div class="col-sm-5">
                <div class="form-group">
                    <label for="No Polisi Kendaraan" style="color:#900">No Polisi Kendaraan *
                    </label>

                    <input type="text" id="no_polisi" name="no_polisi" data-title="No Polisi Kendaraan"
                            class="form-control validation" placeholder="No Polisi" readonly 
                            value="<?=$data_request->no_polisi;?>"
                            />

                </div>
            </div>

        </div>
        <!-- Row End -->

        <!-- Row Start -->
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label for="No Rangka Kendaraan" style="color:#900">No Rangka Kendaraan *</label>

                    <input type="text" id="no_rangka" name="no_rangka" data-title="No Rangka Kendaraan"
                            class="form-control validation" placeholder="No Rangka Kendaraan" readonly 
                            value="<?=$data_request->vin;?>"
                            />

                </div>
            </div>

            <div class="col-sm-1">


            </div>

            <div class="col-sm-5">
                <div class="form-inline">
                    <label for="Tujuan" >Tujuan </label>

                    <br>

                    <label for="Dari" >Dari </label>
                    <input type="text" id="dari" name="dari" data-title="Tujuan Dari"
                            class="form-control " placeholder="Tujuan Dari" 
                            value="<?=$tujuan_dari;?>"
                            />

                    <label for="Ke" >Ke *</label>
                    <input type="text" id="ke" name="ke" data-title="Tujuan Ke" class="form-control"
                            placeholder="Tujuan Ke" 
                            value="<?=$tujuan_ke;?>"
                            />

                </div>
            </div>

        </div>
        <!-- Row End -->

        <!-- Row Start -->
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label for="Kilometer Berangkat" style="color:#900">Kilometer Berangkat *</label>

                    <input type="text" onkeypress="return isNumberKey(event)" id="kilometer_berangkat" name="kilometer_berangkat"
                            data-title="Kilometer Berangkat" class="form-control validation"
                            placeholder="Kilometer Berangkat" 
                            value="<?=$data_request->kilo_berangkat;?>"
                            />

                </div>
            </div>

            <div class="col-sm-1">


            </div>

            <div class="col-sm-5">
                <div class="form-group">
                    <label for="Kilometer Kepulangan">Kilometer Kepulangan</label>

                    <input type="text" onkeypress="return isNumberKey(event)" id="kilometer_kepulangan" name="kilometer_kepulangan"
                            data-title="Kilometer Kepulangan" class="form-control" placeholder="Kilometer Kepulangan" 
                            value="<?=$data_request->kilo_pulang;?>"
                            />

                </div>
            </div>

        </div>
        <!-- Row End -->

        <!-- Row Start -->
        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label for="Jabatan Requester">Jabatan Requester</label>
                   
                     <input type="text" id="jabatan_requester" name="jabatan_requester"
                            data-title="Jabatan Requester" class="form-control" placeholder="Jabatan Requester" 
                            value="<?=$data_request->jabatan_requester;?>"
                            />

                </div>
            </div>

            <div class="col-sm-1">


            </div>

            <div class="col-sm-5">
                <div class="form-group">
                    <label for="Keterangan">Keterangan</label>

                    <textarea class="form-control" id="keterangan" name="keterangan" data-title="Keterangan"
                            rows="5" placeholder="Keterangan"><?=$data_request->keterangan;?></textarea>

                </div>
            </div>

        </div>
        <!-- Row End -->
        

        <hr>

        <div class="row">
            <div class="form-group text-center">
                <button id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
                    <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                    Save
                </button>


                <a href="<?=base_url('demo_car/C_demo_car_request');?>" class="btn btn-app btn-danger btn-xs radius-4">
                    <i class="ace-icon fa fa-close bigger-160"></i>
                    Cancel
                </a>

            </div>
        </div>


        <hr>

    </div>
    <!-- container close -->

    <!-- Responsive Form for Mobile and Web End -->

    <script type="text/javascript">
        function isNumberKey(e) {
            var charCode = (e.which) ? e.which : e.keyCode;
            if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode >
                57)) {
                return false;
            } else {
                return true;
            }
        }
    </script>

    <script type="text/javascript">
        $('.chosen-select').chosen({
            allow_single_deselect: true
        }).change(function (event) {
            if (event.target == this) {
                var value = $(this).val()

                if (value == '-') {
                    // Kosongkan value ke type kendaraan, no polisi, no rangka kendaraan
                    $('#type_kendaraan').val('');
                    $('#no_rangka').val('');
                    $('#no_polisi').val('')
                } else {
                    ajax(value);
                }


            }
        });
        //resize the chosen on window resize


        // Ajax Chosen Select
        function ajax(val) {
            var url = '<?php echo site_url('demo_car/C_demo_car_request/autocomplete'); ?>';
            $.ajax({
                url: url,
                data: {
                    query: val
                },
                dataType: "json",
                type: "GET",
                success: function (data) {
                    console.log(data)

                    // Pasang value ke type kendaraan, no polisi, no rangka kendaraan
                    $('#type_kendaraan').val(data.type_name);
                    $('#no_rangka').val(data.vin);
                    $('#no_polisi').val(data.no_polisi)

                }
            });
        }

        $(window)
            .off('resize.chosen')
            .on('resize.chosen', function () {
                $('.chosen-select').each(function () {
                    var $this = $(this);
                    $this.next().css({
                        'width': $this.parent().width()
                    });
                })
            }).trigger('resize.chosen');
        //resize chosen on sidebar collapse/expand
        $(document).on('settings.ace.chosen', function (e, event_name, event_val) {
            if (event_name != 'sidebar_collapsed') return;
            $('.chosen-select').each(function () {
                var $this = $(this);
                $this.next().css({
                    'width': $this.parent().width()
                });
            })
        });
    </script>


    <script type="text/javascript">
        $('#request-form').submit(function (e) {
            var formData = new FormData(document.getElementById('request-form'));
            var url = "<?=base_url('demo_car/C_demo_car_request/update_request_demo_operational');?>";

            $(".validation").each(function () {
                var val = $(this).val();
                var title = $(this).attr('data-title');

                //var desc = 
                if (val == '' || val == '-') {
                    alert(`${title} Harus di isi!`);
                    $('#cek_validasi').val('0');
                    $(this).css({
                        'border': '1px solid red'
                    });
                    $(this).focus();
                    return false;
                } else {
                    $('#cek_validasi').val('1');
                    $(this).css({
                        'border': '1px solid #d5d5d5'
                    });

                }



            });

            //return false;

            var cek_validasi = $('#cek_validasi').val();
            if (cek_validasi == '1') {
                //alert('ok')
                ajax();
                return false;
            }

            function ajax() {
                $.ajax({
                    url: url,
                    method: "POST",
                    data: formData,
                    async: false,
                    processData: false,
                    contentType: false,
                    cache: false,
                    beforeSend: function () {
                        $('#btnSave').prop('disabled', true);
                    },
                    success: function (response) {

                        $('#btnSave').prop('disabled', false);

                        if (response == 'Update') {
                            alert("Update Berhasil");
                            var url_redirect = "<?=base_url('demo_car/C_demo_car_request');?>";
                            window.location.href = url_redirect;
                        } else {
                            alert("Update Gagal");
                        }

                    },
                    error: function (err) {
                        alert(err)
                    }
                });
                return false;
            }

            e.preventDefault();
        })
    </script>


</form>