<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<div>
    <button onclick="add_type()" class="btn btn-app btn-primary btn-xs radius-4 caddnew" type="submit">
        <i class="ace-icon fa fa-book bigger-160"></i>
        Add
    </button>
    <button onclick="delete_type()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit"
        id="deleteTriger" name="deleteTriger" value="btndel" disabled="disabled">
        <i class="ace-icon fa fa-ban bigger-160"></i>
        Delete
    </button>
    <button onclick="edit_type()" class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="editTriger"
        name="editTriger" value="btnedit" disabled="disabled">
        <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
        Edit
    </button>
</div>
<br />
<div class="table-header btn-info"> <?php echo " ".$header ;?> </div>
<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">
    <table id="myTable" cellpadding="0" cellspacing="0" width="100%"
        class="table table-striped table-bordered table-hover">
        <thead class="text-warning">
            <th width="5%" style="text-align:center">
                <label class="pos-rel">
                    <input type="checkbox" class="ace ace-checkbox-1" id="checkAll" />
                    <span class="lbl"></span>
                </label>
            </th>
            <th>No</th>
            <th>ID Type</th>
            <th>Model Name</th>
            <th>Type Name</th>
        </thead>
    </table>
</div>
<!-- Bootstrap modal -->
<div class="modal fade " id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content col-md-9">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Type Demo Car Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <div class="widget-body">
                        <div class="widget-main">
                            <input type="hidden" value="" name="id_type" />
                            <div>
                                <label for="id_type">ID Type</label>
                                <?php
							    $totaldata = $total[0]->totaldata+1;
						        ?>

                                <input class="form-control" id="id_increment" name="id_increment" placeholder="ID Type"
                                    type="hidden" readonly value="<?php echo $totaldata ?>" />

                                <input class="form-control" id="id_type" name="id_type" placeholder="ID Type"
                                    type="text" readonly value="<?php echo $totaldata ?>" />
                            </div>
                            <div>
                                <label for="dept">Model Name</label>
                                <select class="form-control" id="model_name" name="model_name" required>
                                    <option value='-'>- Pilih Brand - </option>

                                    <?php
                                        foreach($data_model as $value_model) {
                                    ?>
                                    <option value="<?=$value_model->id_model;?>"> <?=$value_model->model_name;?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div>
                                <label for="model_name">Type Name</label>
                                <input class="form-control" id="type_name" name="type_name" placeholder="Type Name"
                                    type="text" required />
                            </div>


                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button onclick="save()" id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4"
                    type="submit">
                    <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                    Save
                </button>
                <button class="btn btn-app btn-danger btn-xs radius-4" type="submit" data-dismiss="modal">
                    <i class="ace-icon fa fa-close bigger-160"></i>
                    Cancel
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script type="text/javascript">
    // $('#company').change(function() {
    //   $("#branch").chained("#company");
    // })
</script>


<script>
    //$('#myTable').dataTable();
    //-----------------------------------------data table custome----
    var rows_selected = [];
    var tableUsers = $('#myTable').DataTable({
        "lengthMenu": [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
        "dom": 'lfBrtip',
        "buttons": [{
                "extend": 'copy',
                "text": '<i class="fa fa-files-o"></i> Copy',
                "titleAttr": 'Copy'
            },
            {
                "extend": 'print',
                "text": '<i class="fa fa-print" aria-hidden="true"></i> Print',
                "titleAttr": 'Print',
                "orientation": 'landscape',
                "pageSize": 'A4'
            },
            {
                "extend": 'excel',
                "text": '<i class="fa fa-file-excel-o"></i> Excel',
                "titleAttr": 'Excel'
            },
            {
                "extend": 'pdf',
                "text": '<i class="fa fa-file-pdf-o"></i> PDF',
                "titleAttr": 'PDF',
                "orientation": 'landscape',
                "pageSize": 'A4'
            }
        ],
        "autoWidth": false,
        "scrollY": '250',
        "scrollX": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('demo_car/C_demo_car_type/ajax_list') ?>",
            "type": "POST"
        },
        'columnDefs': [{
            'targets': [0],
            'orderable': false,
        }],

        'order': [
            [2, 'desc']
        ],
    });

    //end--------------------------------------------------------------


    //check all--------------------------
    $('#checkAll').change(function () {

        $('#editTriger').prop("disabled", true);
        var table = $('#myTable').DataTable();
        var cells = table.cells().nodes();
        $(cells).find(':checkbox').prop('checked', $(this).is(':checked'));
    });
    //end---------------------------------			

    //aktif edit--------------------------------------------------------

    var counterCheckededit = 0;
    $('body').on('change', 'input[type="checkbox"]', function () {
        this.checked ? counterCheckededit++ : counterCheckededit--;
        counterCheckededit == 1 ? $('#editTriger').prop("disabled", false) : $('#editTriger').prop("disabled",
            true);
    });

    //end---------------------------------------------------------------

    //aktif dell--------------------------------------------------------
    var counterChecked = 0;
    $('body').on('change', 'input[type="checkbox"]', function () {

        this.checked ? counterChecked++ : counterChecked--;
        counterChecked > 0 ? $('#deleteTriger').prop("disabled", false) : $('#deleteTriger').prop("disabled",
            true);

    });
    //--------------------------------------------------------------------
</script>


<script>
    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>


<script type="text/javascript">
    var save_method; //for save method string
    var table;

    function add_type() {
        save_method = 'add';
        $('#modal_form').modal('show'); // show bootstrap modal
        //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title

        // Hilangkan semua value input ketika tombol edit di tekan pertama kali sebelum add
        var id_increment = $('#id_increment').val();
        $('#id_type').val(id_increment);
        $('#model_name').val('-');
        $('#type_name').val('');

    }

    function edit_type(id) {
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals

        if ($('.editRow:checked').length > 1) {
            alert("Just One Allowed Data!!!");
        } else {
            var eee = $('.editRow:checked').val();
        }

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('demo_car/C_demo_car_type/ajax_edit/')?>" + eee,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $('#id_type').val(data[0].id_type);
                $('#model_name').val(data[0].id_model);
                $('#type_name').val(data[0].type_name);

                $('#modal_form').modal({
                    backdrop: 'static',
                    keyboard: false
                }); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Type Demo Car Form'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }


    function save() {
        var url, action;
        if (save_method == 'add') {
            url = "<?php echo site_url('demo_car/C_demo_car_type/add_type')?>";
            action = 'Tambah';
        } else {
            url = "<?php echo site_url('demo_car/C_demo_car_type/update_type')?>";
            action = 'Ubah';
        }

        var model_name = $('#model_name').val();
        var type_name = $('#type_name').val();

        // Client Side Validation
        if (model_name == '-') {
            alert('Silahkan Pilih Model Name');
            $('#model_name').focus();
        } else if (type_name == '' || type_name.trim() == '') {
            alert('Silahkan Isi Type Name');
            $('#type_name').focus();
        } else {
            var formData = new FormData(document.getElementById('form'))
            $.ajax({
                url: url,
                type: "POST",
                data: formData,
                processData: false,
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function () {
                    $("#btnSave").prop('disabled', true);
                },
                success: function (data, textStatus, jqXHR) {

                    if (data == 'Insert' || data == 'Update') {
                        $('#modal_form').modal('hide');
                        location.reload(); // for reload a page
                    } else if (data == 'Insert Gagal' || data == 'Update Gagal') {
                        alert('Data Gagal di ' + action);
                    } else {
                        alert(data);
                    }

                    $("#btnSave").prop('disabled', false);
                },
            });
        }


    }

    function delete_type() {

        if ($('.editRow:checked').length >= 1) {
            var ids = [];
            $('.editRow').each(function () {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            var rss = confirm("Are you sure you want to delete this data???");
            if (rss == true) {
                var ids_string = ids.toString();
                $.post('<?=@base_url('demo_car/C_demo_car_type/delete_data')?>', {
                        ID: ids_string
                    },
                    function (result) {

                        var json = JSON.parse(result)

                        if (json == 'Delete') {
                            $('#modal_form').modal('hide');
                            location.reload(); // for reload a page
                        } else {
                            alert('Delete Data Gagal!');
                        }


                    });
            }
        }




    }
</script>



</html>