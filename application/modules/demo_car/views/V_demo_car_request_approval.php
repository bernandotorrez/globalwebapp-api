<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>

<style type="text/css">
.isDisabled {
  color: currentColor;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;
}
</style>

<div>

    <button class="btn btn-app btn-info btn-xs radius-4" type="button" id="btn_detail" name="btn_detail"  value = ""  disabled="disabled" >
            <i class="ace-icon fa fa-search bigger-160"></i>
            View Doc 
		</button> 
    
    <button class="btn btn-app btn-danger btn-xs radius-4 btn_reject" type="button" id="btn_reject" name="btn_reject"  value ="2"  disabled="disabled" >
            <i class="ace-icon fa fa-times-circle-o bigger-160"></i>
            Reject 
		</button> 

    <button class="btn btn-app btn-success btn-xs radius-4 btn_approve" type="button" id="btn_approve" name="btn_approve"  value ="1"  disabled="disabled" >
            <i class="ace-icon fa fa-check-circle-o bigger-160"></i>
            Approve 
		</button> 
</div>
<br />

<!-- Keterangan -->
<!-- 
    <div id="keterangan" class="text-center">
    <h3>Keterangan</h3>
<br>
<table class="table text-center" width="100%">
<th width="20%"><label><i class="fa fa-share fa-2x" aria-hidden="true"></i> = Approval not yet Sent </label></th>
<th width="35%"><label><i class="fa fa-envelope fa-2x text-warning" aria-hidden="true"></i> = Approval Sent, and Being Checked by Sign 1 or Sign 2  </label></th>
<th width="20%"><label><i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true"></i> = Approved</label></th>
<th width="25%"><label><i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true"></i> = Rejected</label></th>
</table>

</div> -->

<div class="table-header btn-info"> <?php echo " ".$header ;?> </div>
<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">
    <table id="myTable" cellpadding="0" cellspacing="0" width="300%"
        class="table table-striped table-bordered table-hover">
        <thead class="text-warning">
            <th width="5%" style="text-align:center">
                Action
            </th>
            <th>No</th>
            
            <th>Sign Req</th>
            <th>Sign 1</th>
            <th>Sign 2</th>
            <th>Status Approval</th>
            <th>ID Req Demo Car</th>
            <th>Requester Name</th>
            <th>Requester Position</th>
            <th>Sales Name</th>
            <th>Contact Sales</th>
            <th>Customer Name</th>
            <th>KTP</th>
            <th>Address</th>
            <th>HP</th>
            <th>Stock No</th>
            <th>Kind Vehicle</th>
            <th>Type</th>
            <th>Plat No</th>
            <th>Vin / Chasis</th>
            <th>Location</th>
            <th>Company</th>
            <th>Branch</th>
            <th>Destination</th>
            <th>km Start</th>
            <th>km Back</th>
            <th>Keterangan</th>
        </thead>
    </table>
</div>

<!-- Content Popup -->
<div id="dialog" style="display: none;">
    <div>
        <iframe id="frame" width="750px" height="550px"></iframe>
    </div>
</div>
<!-- Content Popup -->

<script type="text/javascript">

    function PDFPopup(e) {
		var url = $(e).attr('req_id');
		$("#dialog").dialog({
			width: 'auto',
			height: 'auto',
			resize: 'auto',
			autoResize: true
		});
		$("#frame").attr("src", url + "#toolbar=0");
        
    };

    $('[type=search]').keyup(function() {
        var value = $(this).val();
        alert(value)
    })
</script>


<script>
    //$('#myTable').dataTable();
    //-----------------------------------------data table custome----
    var flag_approval_democar = "<?=$this->session->userdata('approval_democar');?>";
    if(flag_approval_democar == '1') {
        var url_ajax_datatable = "<?=base_url('demo_car/C_demo_car_request/ajax_list_approval1');?>";
    } else {
        var url_ajax_datatable = "<?=base_url('demo_car/C_demo_car_request/ajax_list_approval2');?>";
    }

    var rows_selected = [];
    var tableUsers = $('#myTable').DataTable({
        "lengthMenu": [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
        "dom": 'lfBrtip',
        "buttons": [{
                "extend": 'copy',
                "text": '<i class="fa fa-files-o"></i> Copy',
                "titleAttr": 'Copy'
            },
            {
                "extend": 'print',
                "text": '<i class="fa fa-print" aria-hidden="true"></i> Print',
                "titleAttr": 'Print',
                "orientation": 'landscape',
                "pageSize": 'A4'
            },
            {
                "extend": 'excel',
                "text": '<i class="fa fa-file-excel-o"></i> Excel',
                "titleAttr": 'Excel'
            },
            {
                "extend": 'pdf',
                "text": '<i class="fa fa-file-pdf-o"></i> PDF',
                "titleAttr": 'PDF',
                "orientation": 'landscape',
                "pageSize": 'A4'
            }
        ],
        "autoWidth": true,
        "scrollY": '250',
        "scrollX": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": url_ajax_datatable,
            "type": "POST"
        },
        "search": {
            "search": "<?=base64_decode($this->input->get('id'));?>"
        },
        'columnDefs': [{
            'targets': [0],
            'orderable': false,
        }],

        'order': [
            [2, 'desc']
        ],
    });

    //end--------------------------------------------------------------


    //check all--------------------------
    $('#checkAll').change(function () {

        $('#btn_approve').prop("disabled", true);
        $('#btn_reject').prop("disabled", true);
        $('#btn_detail').prop("disabled", true);
        var table = $('#myTable').DataTable();
        var cells = table.cells().nodes();
        $(cells).find(':checkbox').prop('checked', $(this).is(':checked'));
    });
    //end---------------------------------			

     // Button Detail
     var counterCheckedDetail = 0;
    $('body').on('change', 'input[type="checkbox"]', function () {

        this.checked ? counterCheckedDetail++ : counterCheckedDetail--;
        counterCheckedDetail == 1 ? $('#btn_detail').prop("disabled", false) : $('#btn_detail').prop("disabled",
            true);

    });

    //end---------------------------------------------------------------

    // Button Approve
    var counterCheckedApprove = 0;
    $('body').on('change', 'input[type="checkbox"]', function () {

        this.checked ? counterCheckedApprove++ : counterCheckedApprove--;
        counterCheckedApprove == 1 ? $('#btn_approve').prop("disabled", false) : $('#btn_approve').prop("disabled",
            true);

    });

    //end---------------------------------------------------------------

    // Button Reject
    var counterCheckedReject = 0;
    $('body').on('change', 'input[type="checkbox"]', function () {

        this.checked ? counterCheckedReject++ : counterCheckedReject--;
        counterCheckedReject == 1 ? $('#btn_reject').prop("disabled", false) : $('#btn_reject').prop("disabled",
            true);

    });
    //--------------------------------------------------------------------

</script>


<script>
    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>

<script type="text/javascript">
    $('#btn_approve').click(function() {
        var acc = confirm("Approve this Request Demo Car?");
        var val = $('.editRow:checked').val();

        var flag_approval_democar = "<?=$this->session->userdata('approval_democar');?>";
        if(flag_approval_democar == '1') {
            var url = "<?=base_url('demo_car/C_demo_car_request/approved1');?>";
        } else {
            var url = "<?=base_url('demo_car/C_demo_car_request/approved2');?>";
        }

        if (acc == true) {
            $.ajax({
                url: url,
                data: {
                    ID: val
                },
                dataType: "json",
                type: "GET",
                beforeSend: function () {
                    $('#btn_approve').prop('disabled', true);
                },
                success: function (response) {
                    //$('#btnsendtab').prop('disabled', false);
                    
                    var status_email = response.email;
                    var status_approval = response.status_approval;
                    var error_email = response.error_email

                    if(status_approval=='Approved') {
                        alert('Successfully Approved this Request of Demo Car !');
                        location.reload();
                    } else {
                        alert('System failure, Error Msg : '+error_email);
                        $('#btn_approve').prop('disabled', false);
                    }

                }
            });
        }
    })
</script>

<script type="text/javascript">
    $('#btn_reject').click(function() {
        var acc = confirm("Reject this Request Demo Car?");
        var val = $('.editRow:checked').val();

        var flag_approval_democar = "<?=$this->session->userdata('approval_democar');?>";
        if(flag_approval_democar == '1') {
            var url = "<?=base_url('demo_car/C_demo_car_request/rejected1');?>";
        } else {
            var url = "<?=base_url('demo_car/C_demo_car_request/rejected2');?>";
        }

        if (acc == true) {
            $.ajax({
                url: url,
                data: {
                    ID: val
                },
                dataType: "json",
                type: "GET",
                beforeSend: function () {
                    $('#btn_reject').prop('disabled', true);
                },
                success: function (response) {
                    //$('#btnsendtab').prop('disabled', false);
                    
                    var status_email = response.email;
                    var status_approval = response.status_approval;
                    var error_email = response.error_email

                    if(status_approval=='Rejected') {
                        alert('Successfully Rejected this Request of Demo Car !');
                        location.reload();
                    } else {
                        alert('System failure, Error Msg : '+error_email)
                        $('#btn_reject').prop('disabled', false);
                    }

                }
            });
        }
    })
</script>

<script type="text/javascript">
      $('#btn_detail').click(function() {
        var val = $('.editRow:checked').attr('data-upload');
        
        
        if(val===undefined) {
            var url = "#";
        } else {
           
            var url = "<?=base_url('asset/upload_demo_car/');?>"+val;
            //window.location.href = url;
            $("#dialog").dialog({
			width: 'auto',
			height: 'auto',
			resize: 'auto',
			autoResize: true
            });
            $("#frame").attr("src", url + "#toolbar=0");
            //window.open(url, '_blank');
        }
    })
</script>

<script type="text/javascript">
    var save_method; //for save method string
    var table;

    function add_stock() {
        save_method = 'add';
        $('#modal_form').modal('show'); // show bootstrap modal
        //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title

        // Hilangkan semua value input ketika tombol edit di tekan pertama kali sebelum add
        var id_increment = $('#id_increment').val();
        $('#id_stock').val(id_increment);
        $('#model_name').val('-');
        $('#type_name').val('');

    }

    function edit_stock(id) {
        save_method = 'update';
        //$('#form')[0].reset(); // reset form on modals

        if ($('.editRow:checked').length > 1) {
            alert("Just One Allowed Data!!!");
        } else {
             
            var eee = $('.editRow:checked').val();
        }
        
        if(eee===undefined) {
            var url = "#";
        } else {
           
            var url = "<?=base_url('demo_car/C_demo_car_request/edit?id=');?>"+eee;
            window.location.href = url;
        }

        
    }


    function save() {
        var url, action;
        if (save_method == 'add') {
            url = "<?php echo site_url('demo_car/C_demo_car_request/add_stock')?>";
            action = 'Tambah';
        } else {
            url = "<?php echo site_url('demo_car/C_demo_car_request/update_stock')?>";
            action = 'Ubah';
        }

        var model_name = $('#model_name').val();
        var type_name = $('#type_name').val();

        // Client Side Validation
        if (model_name == '-') {
            alert('Silahkan Pilih Model Name');
            $('#model_name').focus();
        } else if (type_name == '' || type_name.trim() == '') {
            alert('Silahkan Isi Type Name');
            $('#type_name').focus();
        } else {
            var formData = new FormData(document.getElementById('form'))
            $.ajax({
                url: url,
                type: "POST",
                data: formData,
                processData: false,
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function () {
                    $("#btnSave").prop('disabled', true);
                },
                success: function (data, textStatus, jqXHR) {

                    if (data == 'Insert' || data == 'Update') {
                        $('#modal_form').modal('hide');
                        location.reload(); // for reload a page
                    } else if (data == 'Insert Gagal' || data == 'Update Gagal') {
                        alert('Data Gagal di ' + action);
                    } else {
                        alert(data);
                    }

                    $("#btnSave").prop('disabled', false);
                },
            });
        }


    }

    function delete_stock() {

        if ($('.editRow:checked').length >= 1) {
            var ids = [];
            $('.editRow').each(function () {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            var rss = confirm("Are you sure you want to delete this data???");
            if (rss == true) {
                var ids_string = ids.toString();
                $.post('<?=@base_url('demo_car/C_demo_car_request/delete_data')?>', {
                        ID: ids_string
                    },
                    function (result) {

                        var json = JSON.parse(result)

                        if (json == 'Delete') {
                            $('#modal_form').modal('hide');
                            location.reload(); // for reload a page
                        } else {
                            alert('Delete Data Gagal!');
                        }


                    });
            }
        }




    }
</script>



</html>