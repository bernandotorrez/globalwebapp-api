<form method="post" id="stock-form" enctype="multipart/form-data">

    <input type="hidden" id="cek_validasi" value="" />
    <input type="hidden" id="branch_id" name="branch_id" value="<?=$data_stock->branch_id;?>" />
    </div>

    <div class="table-header btn-info text-center">
        <?php //echo " ".$header ;?>

        Edit Data Stock Demo Car
    </div>

    <div id="msg"> </div>


    <table class="table borderless ">

        <tr>
            <td>
                <div class="form-group">
                    <label for="Stock Number" class="col-xs-3">Stock Number</label>
                    <div class="col-xs-5">
                        <input type="text" id="stock_no" name="stock_no" data-title="Stock Number" class="form-control"
                            readonly="readonly" value="<?=$data_stock->stock_no;?>" />
                    </div>
                </div>
            </td>

            <td>
                <div class="form-group">
                    <label for="Branch" class="col-xs-3">Branch</label>
                    <div class="col-xs-5">

                        <input type="text" id="branch_name" name="branch_name" data-title="Branch" class="form-control"
                            value="<?=$data_stock->name_branch;?>" readonly />
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label for="Location Stock" class="col-xs-3" style="color:#900">Location Stock *</label>
                    <div class="col-xs-5">
                        <select class="form-control validation" id="id_locstock" data-title="Location Stock"
                            name="id_locstock" required>
                            <option value='-'>- Pilih Location - </option>

                            <?php
                                foreach($data_location as $value_location) {
                            ?>
                            <option value="<?=$value_location->id_locstock;?>" <?php 
                                if($data_stock->id_locstock == $value_location->id_locstock) {
                                    echo 'selected';
                                }
                            ?>> <?=$value_location->name_location;?>
                            </option>
                            <?php
                                }
                            ?>
                        </select>

                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <label for="Stock Keeper" class="col-xs-3">Stock Keeper</label>

                    <div class="col-xs-5">
                        <input type="text" id="stock_keeper" name="stock_keeper" data-title="Stock Keeper"
                            class="form-control" value="<?=$data_stock->stock_keeper;?>" readonly="readonly" />

                    </div>
                </div>

            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label for="Type" class="col-xs-3" style="color:#900">Type *</label>
                    <div class="col-xs-5">
                        <select class="form-control validation" id="id_type" data-title="Type" name="id_type" onChange="getDataColour(this.value)" required>
                            <option value='-'>- Pilih Type - </option>

                            <?php
                                foreach($data_type as $value_type) {
                            ?>

                            <option value="<?=$value_type->id_type;?>" <?php 
                                if($data_stock->id_type == $value_type->id_type) {
                                    echo 'selected';
                                }
                            ?>> <?=$value_type->type_name;?>
                            </option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <label for="Colour" class="col-xs-3" style="color:#900">Colour *</label>
                    <div class="col-xs-5">

                    <input type="hidden" value="<?=$data_stock->id_colour_group;?>" name="id_colour" id="id_colour" class="form-control" />

                        <div id="colour-input">
                        <select class="form-control" name="colour" id="colour" onChange="getIDColour(this.value)">
                            <option value=""> - Pilih Colour - </option>

                            <?php foreach($data_colour as $value) { ?>
                            
                            <option value="<?=$value->id_colour_group;?>"
                                <?php if($data_stock->id_colour_group == $value->id_colour_group) { echo 'selected'; } ?>
                            > <?=$value->colour;?> </option>

                            <?php } ?>

                            </select>
                        </div>
                    </div>

                </div>
            </td>

        </tr>

        <tr>
            <td>
                <div class="form-group">
                    <label for="Vin" class="col-xs-3" style="color:#900">Vin *</label>
                    <div class="col-xs-5">
                        <input type="text" id="vin" name="vin" data-title="Vin" class="form-control validation"
                            value="<?=$data_stock->vin;?>" placeholder="Vin" />
                    </div>
                </div>
            </td>

            <td>
                <div class="form-group form-inline">
                    <label for="Upload Vin" class="col-xs-3" style="color:#900">Upload Vin * (pdf)</label>
                    <div class="col-xs-5">
                        <input type="file" id="upload_vin" name="upload_vin" data-title="Upload Vin"
                            class="btn btn-info validation" accept="application/pdf" onChange="checkExtension(this.value)"/>

                        <a href="<?=base_url("asset/upload_demo_car/$data_stock->upload_vin");?>" target="_blank"> Lihat
                        </a>
                    </div>
                </div>

            </td>
        </tr>

        <tr>
            <td>
                <div class="form-group">
                    <label for="Receive Date" class="col-xs-3" style="color:#900">Receive Date Unit *</label>
                    <div class="col-xs-5">
                        <input type="text" id="receive_date" name="receive_date" data-title="Receive Date"
                            class="form-control validation" value="<?=$data_stock->receive_date;?>" readonly />
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <label for="Engine" class="col-xs-3" style="color:#900">Engine *</label>

                    <div class="col-xs-5">
                        <input type="text" id="engine" name="engine" data-title="Engine" class="form-control validation"
                            value="<?=$data_stock->engine;?>" placeholder="Colour" />
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td>
                <div class="form-group">
                    <label for="Vehicle Status" class="col-xs-3" style="color:#900">Vehicle Status *</label>
                    <div class="col-xs-5">
                        <select class="form-control validation" id="veh_status" data-title="Vehicle Status"
                            name="veh_status">
                            <option value="-">- Pilih Vehicle Status - </option>
                            <option value="1" <?php 
                                if($data_stock->veh_status == '1') {
                                    echo 'selected';
                                }
                            ?>>Ready</option>
                            <option value="0" <?php 
                                if($data_stock->veh_status == '0') {
                                    echo 'selected';
                                }
                            ?>>Sold</option>
                        </select>
                    </div>
                </div>
            </td>
            <td rowspan="1">

                <div class="form-group">
                    <label for="Vehicle Status" class="col-xs-3" style="color:#900">Status Stock *</label>
                    <div class="col-xs-5">
                        <select class="form-control validation" id="status_stock" data-title="Status Stock"
                            name="status_stock">
                            <option value="-">- Pilih Status Stock - </option>
                            <option value="Demo" <?php 
                                if($data_stock->status_stock == 'Demo') {
                                    echo 'selected';
                                }
                            ?>>Demo</option>
                            <option value="Operational" <?php 
                                if($data_stock->status_stock == 'Operational') {
                                    echo 'selected';
                                }
                            ?>>Operational</option>
                        </select>
                    </div>
                </div>


            </td>
        </tr>

        <tr>
            <td>
                <div class="form-group">
                    <label for="Engine" class="col-xs-3" style="color:#900">No Polisi *</label>

                    <div class="col-xs-5">
                        <input type="text" id="no_polisi" name="no_polisi" data-title="No Polisi"
                            class="form-control validation" placeholder="No Polisi"
                            value="<?=$data_stock->no_polisi;?>" />
                    </div>
                </div>
            </td>
            <td rowspan="1">
                <div class="form-group">
                    <label for="Engine" class="col-xs-3" style="color:#900">Jenis Kendaraan *</label>
                    <div class="col-xs-5">
                    <select class="form-control validation" id="id_kind_vehicle" data-title="Jenis Kendaraan"
                        name="id_kind_vehicle">
                        <option value="-">- Pilih Jenis Kendaraan - </option>


                        <?php
                                foreach($data_jeniskendaraan as $value_jeniskendaraan) {
                            ?>

                        <option value="<?=$value_jeniskendaraan->id_kind_vehicle;?>" 
                        <?php 
                                if($data_stock->id_kind_vehicle == $value_jeniskendaraan->id_kind_vehicle) {
                                    echo 'selected';
                                }
                            ?>> <?=$value_jeniskendaraan->jenis_kendaraan;?>
                        </option>
                        <?php
                                }
                            ?>
                    </select>
                </div>
                </div>

            </td>
        </tr>

        <tr>
            <td>
                <div class="form-group">
                    <label for="Remarks" class="col-xs-3">Remarks</label>
                    <div class="col-xs-5">
                        <textarea name="remarks" class="form-control" id="remarks" name="remarks" data-title="Remarks"
                            rows="5" placeholder="Remarks"><?=$data_stock->remarks;?></textarea>

                    </div>
                </div>
            </td>
            <td>
            <?php if($access == '1') { ?>
            <div class="form-group">
                    <label for="Remarks" class="col-xs-3">Branch</label>
                    <div class="col-xs-5">
                   
                    <select class="form-control validation" id="branch_id" data-title="Jenis Kendaraan" name="branch_id" required>
                            <option value='-'>- Pilih Branch - </option>

                            <?php
                                foreach($data_branch as $value_branch) {
                            ?>
                            <option value="<?=$value_branch->branch_id;?>"
                            
                            <?php if($data_stock->branch_id == $value_branch->branch_id) { echo 'selected'; } ?>
                            
                            > <?=$value_branch->name_branch;?>
                            </option>
                            <?php
                                }
                            ?>
                        </select>
                            <?php } else { ?>
                        <input type="hidden" id="branch_id" name="branch_id" value="<?php echo $this->session->userdata('branch_id'); ?>" />
                           
                    </div>
                </div>
                <?php } ?>
          


            </td>
        </tr>


    </table>

    <div class="form-group text-center">
        <button id="btnSave" name="btnSave" class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit">
            <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
            Edit
        </button>


        <a href="<?=base_url('demo_car/C_demo_car_stock');?>" class="btn btn-app btn-danger btn-xs radius-4">
            <i class="ace-icon fa fa-close bigger-160"></i>
            Cancel
        </a>

    </div>

    <script type="text/javascript">

    function checkExtension(e) {
            var myfile="";
            myfile = e;
            var ext = myfile.split('.').pop();
            if(ext!="pdf"){
                alert('File Extension Must Be .pdf');
                $('#msg').html('<div class="alert alert-danger text-center"><strong>File Extension Must Be .pdf, Please Check your File Extension. </strong></div>');
                $('#btnSave').prop('disabled', true);
            } else{
                $('#msg').html('');
                $('#btnSave').prop('disabled', false);
            }
            
        }
        

    function getDataColour(e) {
        var value = e;
        var url = "<?=base_url('demo_car/C_demo_car_stock/ajax_colour?type=');?>"+value;

        $.ajax({
            url: url,
            type: "GET",
                beforeSend: function () {
                    $("#btnSave").prop('disabled', true);
                },
                success: function (data, textStatus, jqXHR) {
                    //console.log(data)
                    //$('#model').html($data);
                    
                    $('#colour-input').html(data);

                    $("#btnSave").prop('disabled', false);
                }
            });
        }

        function getIDColour(e) {
            $('#id_colour').val(e);
        }

        $('#stock-form').submit(function (e) {
            var formData = new FormData(document.getElementById('stock-form'));
            var url = "<?=base_url('demo_car/C_demo_car_stock/update_stock');?>";

            $(".validation").each(function () {
                var val = $(this).val();
                var title = $(this).attr('data-title');

                //var desc = 
                if (val == '' || val == '-') {
                    alert(`${title} Harus di isi!`);
                    $('#cek_validasi').val('0');
                    $(this).css({
                        'border': '1px solid red'
                    });
                    $(this).focus();
                    return false;
                } else {
                    $('#cek_validasi').val('1');
                    $(this).css({
                        'border': '1px solid #d5d5d5'
                    });

                }



            });

            //return false;

            var cek_validasi = $('#cek_validasi').val();
            if (cek_validasi == '1') {
                var rss = confirm("Are you sure you want to Edit this data???");
                if (rss == true) {
                    ajax();
                    return false;
                }
            }

            function ajax() {
                $.ajax({
                    url: url,
                    method: "POST",
                    enctype: "multipart/form-data",
                    data: formData,
                    async: false,
                    processData: false,
                    contentType: false,
                    cache: false,
                    beforeSend: function () {
                        $('#btnSave').prop('disabled', true);
                    },

                    success: function (response) {
                        var json = JSON.parse(response);
                        var status_upload = json.status_upload;
                        var message_upload = json.message_upload;
                        var status_insert = json.status_insert;

                        $('#btnSave').prop('disabled', false);

                        if (status_insert == 'No Polisi') {
                            alert('No Polisi sudah ada!');
                        } else if (status_insert == 'Upload' && status_upload == 'berhasil') {
                            alert("Insert & Upload Berhasil");
                            var url_redirect = "<?=base_url('demo_car/C_demo_car_stock');?>";
                            window.location.href = url_redirect;
                        } else if (status_insert == 'Upload Gagal' && status_upload == 'berhasil') {
                            alert("Insert Gagal, Upload Berhasil");
                        } else {
                            alert("Insert & Upload Gagal, Message : " + message_upload.error);
                        }

                    },
                    error: function (err) {
                        alert(err)
                    }
                });
                return false;
            }

            e.preventDefault();
        })
    </script>


</form>