<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>

<style type="text/css">
.isDisabled {
  color: currentColor;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;
}
</style>

<div>

    <a href="<?=base_url('demo_car/C_demo_car_stock/add');?>" class="btn btn-app btn-primary btn-xs radius-4 caddnew">
        <i class="ace-icon fa fa-book bigger-160"></i>
            Add
    </a>

    <button onclick="delete_stock()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit"
        id="deleteTriger" name="deleteTriger" value="btndel" disabled="disabled">
        <i class="ace-icon fa fa-ban bigger-160"></i>
        Cancel
    </button>

    <a href="#" onclick="edit_stock()" id="editTriger" name="editTriger" class="btn btn-app btn-success btn-xs radius-4 btn_edit isDisabled">
        <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
            Edit
    </a>
</div>
<br />
<div class="table-header btn-info"> <?php echo " ".$header ;?> </div>
<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">
    <table id="myTable" cellpadding="0" cellspacing="0" width="180%"
        class="table table-striped table-bordered table-hover">
        <thead class="text-warning">
            <th style="text-align:center">
                <label class="pos-rel">
                    <input type="checkbox" class="ace ace-checkbox-1" id="checkAll" />
                    <span class="lbl"></span>
                </label>
            </th>
            <th>No</th>
            <th>Data Upload</th>
            <th>Stock No</th>
            <th>Location</th>
            <th>Type</th>
            <th>Vin / Chasis</th>
            <th>Receive Date</th>
            <th>Vehicle Status</th>
            <th>Branch</th>
            <th>Stock Keeper</th>
            <th>Colour</th>
            <th>Engine</th>
            <th>Remarks</th>
            <th>Status Stock</th>
            <th>Plat No</th>
            <th>Kind Vehicle</th>
        </thead>
    </table>
</div>

<!-- Content Popup -->
<div id="dialog" style="display: none;">
    <div>
        <iframe id="frame" width="750px" height="550px"></iframe>
    </div>
</div>
<!-- Content Popup -->

<script type="text/javascript">
    // $('#company').change(function() {
    //   $("#branch").chained("#company");
    // })
</script>


<script>

function PDFPopup(e) {
    var url = $(e).attr('req_id');
    $("#dialog").dialog({
        width: 'auto',
        height: 'auto',
        resize: 'auto',
        autoResize: true
    });
    $("#frame").attr("src", url + "#toolbar=0");

};
    //$('#myTable').dataTable();
    //-----------------------------------------data table custome----
    var rows_selected = [];
    var tableUsers = $('#myTable').DataTable({
        "lengthMenu": [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
        "dom": 'lfBrtip',
        "buttons": [{
                "extend": 'copy',
                "text": '<i class="fa fa-files-o"></i> Copy',
                "titleAttr": 'Copy'
            },
            {
                "extend": 'print',
                "text": '<i class="fa fa-print" aria-hidden="true"></i> Print',
                "titleAttr": 'Print',
                "orientation": 'landscape',
                "pageSize": 'A4'
            },
            {
                "extend": 'excel',
                "text": '<i class="fa fa-file-excel-o"></i> Excel',
                "titleAttr": 'Excel'
            },
            {
                "extend": 'pdf',
                "text": '<i class="fa fa-file-pdf-o"></i> PDF',
                "titleAttr": 'PDF',
                "orientation": 'landscape',
                "pageSize": 'A4'
            }
        ],
        "autoWidth": true,
        "scrollY": '250',
        "scrollX": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('demo_car/C_demo_car_stock/ajax_list') ?>",
            "type": "POST"
        },
        'columnDefs': [{
            'targets': [0],
            'orderable': false,
        }],

        'order': [
            [2, 'desc']
        ],
    });

    //end--------------------------------------------------------------


    //check all--------------------------
    $('#checkAll').change(function () {

        $('#editTriger').prop("disabled", true);
        $('#deleteTriger').prop("disabled", true);
        var table = $('#myTable').DataTable();
        var cells = table.cells().nodes();
        $(cells).find(':checkbox').prop('checked', $(this).is(':checked'));
    });
    //end---------------------------------			

    //aktif edit--------------------------------------------------------

    var counterCheckededit = 0;
    $('body').on('change', 'input[type="checkbox"]', function () {
        this.checked ? counterCheckededit++ : counterCheckededit--;
        counterCheckededit == 1 ? $('#editTriger').removeClass('isDisabled') : $('#editTriger').addClass('isDisabled');
    });

    //end---------------------------------------------------------------

    //aktif dell--------------------------------------------------------
    var counterChecked = 0;
    $('body').on('change', 'input[type="checkbox"]', function () {

        this.checked ? counterChecked++ : counterChecked--;
        counterChecked == 1 ? $('#deleteTriger').prop("disabled", false) : $('#deleteTriger').prop("disabled",
            true);

    });
    //--------------------------------------------------------------------
</script>


<script>
    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>


<script type="text/javascript">
    var save_method; //for save method string
    var table;

    function add_stock() {
        save_method = 'add';
        $('#modal_form').modal('show'); // show bootstrap modal
        //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title

        // Hilangkan semua value input ketika tombol edit di tekan pertama kali sebelum add
        var id_increment = $('#id_increment').val();
        $('#id_stock').val(id_increment);
        $('#model_name').val('-');
        $('#type_name').val('');

    }

    function edit_stock(id) {
        save_method = 'update';
        //$('#form')[0].reset(); // reset form on modals

        if ($('.editRow:checked').length > 1) {
            alert("Just One Allowed Data!!!");
        } else {
             
            var eee = $('.editRow:checked').val();
        }
        
        if(eee===undefined) {
            var url = "#";
        } else {
           
            var url = "<?=base_url('demo_car/C_demo_car_stock/edit?id=');?>"+eee;
            window.location.href = url;
        }

        
    }


    function save() {
        var url, action;
        if (save_method == 'add') {
            url = "<?php echo site_url('demo_car/C_demo_car_stock/add_stock')?>";
            action = 'Tambah';
        } else {
            url = "<?php echo site_url('demo_car/C_demo_car_stock/update_stock')?>";
            action = 'Ubah';
        }

        var model_name = $('#model_name').val();
        var type_name = $('#type_name').val();

        // Client Side Validation
        if (model_name == '-') {
            alert('Silahkan Pilih Model Name');
            $('#model_name').focus();
        } else if (type_name == '' || type_name.trim() == '') {
            alert('Silahkan Isi Type Name');
            $('#type_name').focus();
        } else {
            var formData = new FormData(document.getElementById('form'))
            $.ajax({
                url: url,
                type: "POST",
                data: formData,
                processData: false,
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function () {
                    $("#btnSave").prop('disabled', true);
                },
                success: function (data, textStatus, jqXHR) {

                    if (data == 'Insert' || data == 'Update') {
                        $('#modal_form').modal('hide');
                        location.reload(); // for reload a page
                    } else if (data == 'Insert Gagal' || data == 'Update Gagal') {
                        alert('Data Gagal di ' + action);
                    } else {
                        alert(data);
                    }

                    $("#btnSave").prop('disabled', false);
                },
            });
        }


    }

    function delete_stock() {

        if ($('.editRow:checked').length >= 1) {
            var ids = [];
            $('.editRow').each(function () {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            var rss = confirm("Are you sure you want to delete this data???");
            if (rss == true) {
                var ids_string = ids.toString();
                $.post('<?=@base_url('demo_car/C_demo_car_stock/delete_data')?>', {
                        ID: ids_string
                    },
                    function (result) {

                        var json = JSON.parse(result)

                        if (json == 'Delete') {
                            $('#modal_form').modal('hide');
                            location.reload(); // for reload a page
                        } else {
                            alert('Delete Data Gagal!');
                        }


                    });
            }
        }




    }
</script>



</html>