<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>

<style type="text/css">
.isDisabled {
  color: currentColor;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;
}
</style>

<div>

    <?php 
        // Check Flag Approval Demo Car
        $flag = $this->session->userdata('approval_democar');
        $access = $this->session->userdata('apps_accsess');
        if($flag == 0 || $flag == null) {
            
        // Check Flag Approval Demo Car
    ?>

    <a href="<?=base_url('demo_car/C_demo_car_request/add_dc');?>" class="btn btn-app btn-primary btn-xs radius-4 caddnew">
        <i class="ace-icon fa fa-book bigger-160"></i>
        DC
    </a>

    <a href="<?=base_url('demo_car/C_demo_car_request/add_opr');?>" class="btn btn-app btn-primary btn-xs radius-4 caddnew">
        <i class="ace-icon fa fa-book bigger-160"></i>
        Opr

    <a href="<?=base_url('demo_car/C_demo_car_request/add_dcopr');?>" class="btn btn-app btn-primary btn-xs radius-4 caddnew">
        <i class="ace-icon fa fa-book bigger-160"></i>
        DC Opr
    </a>

    <button onclick="delete_stock()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit"
        id="deleteTriger" name="deleteTriger" value="btndel" disabled="disabled">
        <i class="ace-icon fa fa-ban bigger-160"></i>
        Delete
    </button>

    <a href="#" onclick="edit_stock()" id="editTriger" name="editTriger"
        class="btn btn-app btn-success btn-xs radius-4 btn_edit isDisabled">
        <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
        Edit
    </a>

    <button class="btn btn-app btn-yellow btn-xs radius-4 btn_send" type="button" id="btnsendtab" name="btnsendtab"
        value="btnsendtab" disabled="disabled">
        <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
        Send
    </button>

    <?php } ?>

    <?php 
        if($access == '1') {
     ?>

    <!-- <button class="btn btn-app btn-info btn-xs radius-4 btnreset" type="button" id="btnreset" name="btnreset"
        value="btnreset" disabled="disabled">
        <i class="ace-icon fa fa-refresh bigger-160"></i>
        Reset
    </button> -->

    <?php } ?>

</div>
<br />

<!-- Keterangan -->
<!-- 
    <div id="keterangan" class="text-center">
    <h3>Keterangan</h3>
<br>
<table class="table text-center" width="100%">
<th width="20%"><label><i class="fa fa-share fa-2x" aria-hidden="true"></i> = Approval not yet Sent </label></th>
<th width="35%"><label><i class="fa fa-envelope fa-2x text-warning" aria-hidden="true"></i> = Approval Sent, and Being Checked by Sign 1 or Sign 2  </label></th>
<th width="20%"><label><i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true"></i> = Approved</label></th>
<th width="25%"><label><i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true"></i> = Rejected</label></th>
</table>

</div> -->

<div class="table-header btn-info"> <?php echo " ".$header ;?> </div>
<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8">
    <table id="myTable" cellpadding="0" cellspacing="0" width="310%"
        class="table table-striped table-bordered table-hover">
        <thead class="text-warning">
            <th width="5%" style="text-align:center">
                <label class="pos-rel">
                    <input type="checkbox" class="ace ace-checkbox-1" id="checkAll" />
                    <span class="lbl"></span>
                </label>
            </th>
            <th>No</th>
            <th>Sign Req</th>
            <th>Sign 1</th>
            <th>Sign 2</th>
            <th>Status Approval</th>
            <?php 
                if($access == '1') {
            ?>

            <th>Give Right</th>

            <?php } ?>
            <th>ID Req Demo Car</th>
            <th>Requester Name</th>
            <th>Requester Position</th>
            <th>Status Request</th>
            <th>Sales Name</th>
            <th>Contact Sales</th>
            <th>Customer Name</th>
            <th>KTP</th>
            <th>Address</th>
            <th>HP</th>
            <th>Stock No</th>
            <th>Kind Vehicle</th>
            <th>Type</th>
            <th>Plat No</th>
            <th>Vin / Chasis</th>
            <th>Location</th>
            <th>Company</th>
            <th>Branch</th>
            <th>Destination</th>
            <th>km Start</th>
            <th>km Back</th>
            <th>Keterangan</th>
            
        </thead>
    </table>
</div>

<script type="text/javascript">
    // $('#company').change(function() {
    //   $("#branch").chained("#company");
    // })
</script>


<script>
    //$('#myTable').dataTable();
    //-----------------------------------------data table custome----
    var rows_selected = [];
    var tableUsers = $('#myTable').DataTable({
        "lengthMenu": [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
        "dom": 'lfBrtip',
        "buttons": [{
                "extend": 'copy',
                "text": '<i class="fa fa-files-o"></i> Copy',
                "titleAttr": 'Copy'
            },
            {
                "extend": 'print',
                "text": '<i class="fa fa-print" aria-hidden="true"></i> Print',
                "titleAttr": 'Print',
                "orientation": 'landscape',
                "pageSize": 'A4'
            },
            {
                "extend": 'excel',
                "text": '<i class="fa fa-file-excel-o"></i> Excel',
                "titleAttr": 'Excel'
            },
            {
                "extend": 'pdf',
                "text": '<i class="fa fa-file-pdf-o"></i> PDF',
                "titleAttr": 'PDF',
                "orientation": 'landscape',
                "pageSize": 'A4'
            }
        ],
        "autoWidth": true,
        "scrollY": '250',
        "scrollX": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('demo_car/C_demo_car_request/ajax_list') ?>",
            "type": "POST"
        },
        'columnDefs': [{
            'targets': [0],
            'orderable': false,
        }],

        'order': [
            [2, 'desc']
        ],
    });

    //end--------------------------------------------------------------


    //check all--------------------------
    $('#checkAll').change(function () {

        $('#editTriger').prop("disabled", true);
        $('#btnsendtab').prop("disabled", true);
        $('#btn_print').prop("disabled", true);
        $('#deleteTriger').prop("disabled", true);
        $('#btnreset').prop("disabled", true);
        var table = $('#myTable').DataTable();
        var cells = table.cells().nodes();
        $(cells).find(':checkbox').prop('checked', $(this).is(':checked'));
    });
    //end---------------------------------			

    //aktif edit--------------------------------------------------------

    var counterCheckededit = 0;
    $('body').on('change', 'input[type="checkbox"]', function () {
        this.checked ? counterCheckededit++ : counterCheckededit--;
        counterCheckededit == 1 ? $('#editTriger').removeClass('isDisabled') : $('#editTriger').addClass('isDisabled');
    });

    //end---------------------------------------------------------------

    //aktif dell--------------------------------------------------------
    var counterChecked = 0;
    $('body').on('change', 'input[type="checkbox"]', function () {

        this.checked ? counterChecked++ : counterChecked--;
        counterChecked == 1 ? $('#deleteTriger').prop("disabled", false) : $('#deleteTriger').prop("disabled",
            true);

    });
    //--------------------------------------------------------------------

    // Button Print
    var counterCheckedPrint = 0;
    $('body').on('change', 'input[type="checkbox"]', function () {
        this.checked ? counterCheckedPrint++ : counterCheckedPrint--;
        //counterCheckedPrint == 1 ? $('#btn_print').prop('disabled', false) : $('#btn_print').prop('disabled', true);

        //if($('#checkAll').not(':checked').length){ 
        var sign1 = $(this).attr('data-sign1');
        var sign2 = $(this).attr('data-sign1');
        
        if(counterCheckedPrint == 1) {
            if(sign1 != '1' || sign2 != '1') {
                $('#btn_print').prop("disabled", true)
            } else{
                $('#btn_print').prop("disabled", false)
            }
        } else {
            $('#btn_print').prop("disabled", true);
        }

	   //counterCheckesendeamail == 1 ? $('#btnsendtab').prop("disabled", false): $('#btnsendtab').prop("disabled", true);
	//}
    });

    //end---------------------------------------------------------------

    
//aktif button send email--------------------------------------------------------
var counterCheckesendeamail = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckesendeamail++ : counterCheckesendeamail--;
	if($('#checkAll').not(':checked').length){ 
        var value = $(this).attr('data-approval');
        
        if(counterCheckesendeamail == 1) {
            if(value == '0') {
                $('#btnsendtab').prop("disabled", false)
            } else{
                $('#btnsendtab').prop("disabled", true)
            }
        } else {
            $('#btnsendtab').prop("disabled", true);
        }

	   //counterCheckesendeamail == 1 ? $('#btnsendtab').prop("disabled", false): $('#btnsendtab').prop("disabled", true);
	}
});
//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterCheckedReset = 0;
    $('body').on('change', 'input[type="checkbox"]', function () {

        this.checked ? counterCheckedReset++ : counterCheckedReset--;
        counterCheckedReset == 1 ? $('#btnreset').prop("disabled", false) : $('#btnreset').prop("disabled",
            true);

    });
    //--------------------------------------------------------------------

</script>

<script type="text/javascript">
      $('#btn_print').click(function() {
        var val = $('.editRow:checked').val();
        var sign1 = $('.editRow:checked').attr('data-sign1');
        var sign2 = $('.editRow:checked').attr('data-sign2');
        
        if(sign1!='1' || sign2!='1') {
            var url = "#";
        } else {
           
            var url = "<?=base_url('demo_car/C_demo_car_request/pdf?id=');?>"+val;
            //window.location.href = url;
            window.open(url, '_blank');
        }
    })
</script>


<script>
    $.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>

<script type="text/javascript">
    $('#btnsendtab').click(function() {
        var acc = confirm("Send Request Demo Car?");
        var val = $('.editRow:checked').val();
        var url = '<?php echo site_url('demo_car/C_demo_car_request/send_approval'); ?>';

        if (acc == true) {
            $.ajax({
                url: url,
                data: {
                    ID: val
                },
                dataType: "json",
                type: "GET",
                beforeSend: function () {
                    $('#btnsendtab').prop('disabled', true);
                },
                success: function (response) {
                    //$('#btnsendtab').prop('disabled', false);
                    
                    var status_email = response.email;
                    var status_approval = response.status_approval;
                    var error_email = response.error_email

                    //console.log(error_email)

                    if(status_approval=='Approval') {
                        alert('Send Approval Berhasil');
                        location.reload();
                    } else {
                        alert('Send Approval Gagal, Error Msg : '+error_email)
                        $('#btnsendtab').prop('disabled', false);
                    }

                }
            });
        }
    })

    $('#btnreset').click(function() {
        var val = $('.editRow:checked').val();
        var url = '<?php echo site_url('demo_car/C_demo_car_request/reset_flag_print'); ?>';
            $.ajax({
                url: url,
                data: {
                    ID: val
                },
                type: "GET",
                beforeSend: function () {
                    $('#btnreset').prop('disabled', true);
                },
                success: function (response) {
                    //$('#btnsendtab').prop('disabled', false);
                    

                    if(response=='ok') {
                        alert('Reset Flag Print Berhasil!');
                        $('#btnreset').prop('disabled', false);
                        location.reload();
                    } else {
                        alert('Reset Flag Print Gagal!');
                        $('#btnreset').prop('disabled', false);
                    }

                }
            });
    })

    function giveRight(e) {
        var val = $(e).attr('req_id');
        var url = '<?php echo site_url('demo_car/C_demo_car_request/reset_flag_print'); ?>';

        var rss = confirm("Give Right this Data?");
        if (rss == true) {

            $.ajax({
                url: url,
                data: {
                    ID: val
                },
                type: "GET",
                beforeSend: function () {
                    $(e).prop('disabled', true);
                },
                success: function (response) {
                    //$('#btnsendtab').prop('disabled', false);


                    if (response == 'ok') {
                        alert('Reset Flag Print Berhasil!');
                        $(e).prop('disabled', false);
                        location.reload();
                    } else {
                        alert('Reset Flag Print Gagal!');
                        $(e).prop('disabled', false);
                    }

                }
            });

        }
    }
</script>

<script type="text/javascript">
    var save_method; //for save method string
    var table;

    function add_stock() {
        save_method = 'add';
        $('#modal_form').modal('show'); // show bootstrap modal
        //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title

        // Hilangkan semua value input ketika tombol edit di tekan pertama kali sebelum add
        var id_increment = $('#id_increment').val();
        $('#id_stock').val(id_increment);
        $('#model_name').val('-');
        $('#type_name').val('');

    }

    function edit_stock() {
        save_method = 'update';
        //$('#form')[0].reset(); // reset form on modals

        var status, eee, url;

        if ($('.editRow:checked').length > 1) {
            alert("Just One Allowed Data!!!");
        } else {
             
            eee = $('.editRow:checked').val();
            status = $('.editRow:checked').attr('data-status');
        }
        
        if(eee===undefined) {
            url = "#";
        } else {
            
            if(status == 1) {
                url = "<?=base_url('demo_car/C_demo_car_request/edit_dc?id=');?>"+eee;
            } else if(status == 2) {
                url = "<?=base_url('demo_car/C_demo_car_request/edit_opr?id=');?>"+eee;
            } else if(status == 3) {
                url = "<?=base_url('demo_car/C_demo_car_request/edit_dcopr?id=');?>"+eee;
            } else {
                url = "#";
            }
            
            window.location.href = url;
        }

        
    }


    function save() {
        var url, action;
        if (save_method == 'add') {
            url = "<?php echo site_url('demo_car/C_demo_car_request/add_stock')?>";
            action = 'Tambah';
        } else {
            url = "<?php echo site_url('demo_car/C_demo_car_request/update_stock')?>";
            action = 'Ubah';
        }

        var model_name = $('#model_name').val();
        var type_name = $('#type_name').val();

        // Client Side Validation
        if (model_name == '-') {
            alert('Silahkan Pilih Model Name');
            $('#model_name').focus();
        } else if (type_name == '' || type_name.trim() == '') {
            alert('Silahkan Isi Type Name');
            $('#type_name').focus();
        } else {
            var formData = new FormData(document.getElementById('form'))
            $.ajax({
                url: url,
                type: "POST",
                data: formData,
                processData: false,
                async: false,
                processData: false,
                contentType: false,
                cache: false,
                beforeSend: function () {
                    $("#btnSave").prop('disabled', true);
                },
                success: function (data, textStatus, jqXHR) {

                    if (data == 'Insert' || data == 'Update') {
                        $('#modal_form').modal('hide');
                        location.reload(); // for reload a page
                    } else if (data == 'Insert Gagal' || data == 'Update Gagal') {
                        alert('Data Gagal di ' + action);
                    } else {
                        alert(data);
                    }

                    $("#btnSave").prop('disabled', false);
                },
            });
        }


    }

    function delete_stock() {

        if ($('.editRow:checked').length >= 1) {
            var ids = [];
            $('.editRow').each(function () {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            var rss = confirm("Are you sure you want to delete this data???");
            if (rss == true) {
                var ids_string = ids.toString();
                $.post('<?=@base_url('demo_car/C_demo_car_request/delete_data')?>', {
                        ID: ids_string
                    },
                    function (result) {

                        var json = JSON.parse(result)

                        if (json == 'Delete') {
                            $('#modal_form').modal('hide');
                            location.reload(); // for reload a page
                        } else {
                            alert('Delete Data Gagal!');
                        }


                    });
            }
        }




    }
</script>



</html>