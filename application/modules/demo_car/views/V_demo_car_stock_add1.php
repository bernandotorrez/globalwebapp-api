<form method="post" id="stock-form" enctype="multipart/form-data">

    <input type="hidden" id="cek_validasi" value="" />

    </div>

    <div class="table-header btn-info text-center">
        <?php //echo " ".$header ;?>

        Tambah Data Stock Demo Car
    </div>

    <div id="msg"> </div>

    <table class="table borderless ">

        <tr>
            <td>
                <div class="form-group">
                    <label for="Stock Number" class="col-xs-3">Stock Number</label>
                    <div class="col-xs-5">
                        <input type="text" id="stock_no" name="stock_no" data-title="Stock Number" class="form-control"
                            value="<?=$stock_no;?>" readonly="readonly" />
                    </div>
                </div>
            </td>

            <td>
                <div class="form-group">
                    <label for="Branch" class="col-xs-3">Branch</label>
                    <div class="col-xs-5">

                        <input type="text" id="branch_name" name="branch_name" data-title="Branch" class="form-control"
                            value="<?php echo $this->session->userdata('name_branch'); ?>" readonly />
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label for="Location Stock" class="col-xs-3" style="color:#900">Location Stock *</label>
                    <div class="col-xs-5">
                        <select class="form-control validation" id="id_locstock" data-title="Location Stock"
                            name="id_locstock" required>
                            <option value='-'>- Pilih Location - </option>

                            <?php
                                foreach($data_location as $value_location) {
                            ?>
                            <option value="<?=$value_location->id_locstock;?>"> <?=$value_location->name_location;?>
                            </option>
                            <?php
                                }
                            ?>
                        </select>

                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <label for="Stock Keeper" class="col-xs-3">Stock Keeper</label>

                    <div class="col-xs-5">
                        <input type="text" id="stock_keeper" name="stock_keeper" data-title="Stock Keeper"
                            class="form-control" value="<?php echo $this->session->userdata('name'); ?>"
                            readonly="readonly" />

                    </div>
                </div>

            </td>
        </tr>
        <tr>
            <td>
                <div class="form-group">
                    <label for="Type" class="col-xs-3" style="color:#900">Type *</label>
                    <div class="col-xs-5">
                        <select class="form-control validation" id="id_type" data-title="Type" name="id_type"
                            onChange="getDataColour(this.value)" required>
                            <option value='-'>- Pilih Type - </option>

                            <?php
                                foreach($data_type as $value_type) {
                            ?>
                            <option value="<?=$value_type->id_type;?>"> <?=$value_type->type_name;?>
                            </option>
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <label for="Colour" class="col-xs-3" style="color:#900">Colour *</label>
                    <div class="col-xs-5">

                        <input type="hidden" value="" name="id_colour" id="id_colour" class="form-control" />

                        <div id="colour-input">
                            <input type="text" value="" name="" id="" class="form-control"
                                placeholder="Please select Type first" readonly />
                        </div>
                    </div>

                </div>
            </td>

        </tr>

        <tr>
            <td>
                <div class="form-group">
                    <label for="Vin" class="col-xs-3" style="color:#900">Vin *</label>
                    <div class="col-xs-5">
                        <input type="text" id="vin" name="vin" data-title="Vin" class="form-control validation"
                            placeholder="Vin" />
                    </div>
                </div>
            </td>

            <td>
                <div class="form-group">
                    <label for="Upload Vin" class="col-xs-3" style="color:#900">Upload Vin * (pdf)</label>
                    <div class="col-xs-5">
                        <input type="file" id="upload_vin" name="upload_vin" data-title="Upload Vin"
                            class="btn btn-info validation" accept="application/pdf"
                            onChange="checkExtension(this.value)" />
                    </div>
                </div>

            </td>
        </tr>

        <tr>
            <td>
                <div class="form-group">
                    <label for="Receive Date" class="col-xs-3" style="color:#900">Receive Date Unit *</label>
                    <div class="col-xs-5">
                        <input type="text" id="receive_date" name="receive_date" data-title="Receive Date"
                            class="form-control validation" value="<?php echo date('d-m-Y'); ?>" readonly />
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <label for="Engine" class="col-xs-3" style="color:#900">Engine *</label>

                    <div class="col-xs-5">
                        <input type="text" id="engine" name="engine" data-title="Engine" class="form-control validation"
                            placeholder="Engine" />
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td>
                <div class="form-group">
                    <label for="Vehicle Status" class="col-xs-3" style="color:#900">Vehicle Status *</label>
                    <div class="col-xs-5">
                        <select class="form-control validation" id="veh_status" data-title="Vehicle Status"
                            name="veh_status">
                            <option value="-">- Pilih Vehicle Status - </option>
                            <option value="1">Ready</option>
                            <option value="0">Sold</option>
                        </select>
                    </div>
                </div>
            </td>
            <td rowspan="1">

                <div class="form-group">
                    <label for="Vehicle Status" class="col-xs-3" style="color:#900">Status Stock *</label>
                    <div class="col-xs-5">
                        <select class="form-control validation" id="status_stock" data-title="Status Stock"
                            name="status_stock">
                            <option value="-">- Pilih Status Stock - </option>
                            <option value="Demo">Demo</option>
                            <option value="Operational">Operational</option>
                        </select>
                    </div>
                </div>


            </td>
        </tr>

        <tr>
            <td>
                <div class="form-group">
                    <label for="Engine" class="col-xs-3" style="color:#900">No Polisi *</label>

                    <div class="col-xs-5">
                        <input type="text" id="no_polisi" name="no_polisi" data-title="No Polisi"
                            class="form-control validation" placeholder="No Polisi" />
                    </div>
                </div>
            </td>
            <td rowspan="1">

                <div class="form-group">
                    <label for="Remarks" class="col-xs-3" style="color:#900">Jenis Kendaraan *</label>
                    <div class="col-xs-5">
                        <select class="form-control validation" id="id_kind_vehicle" data-title="Jenis Kendaraan"
                            name="id_kind_vehicle" required>
                            <option value='-'>- Pilih Jenis Kendaraan - </option>

                            <?php
                                foreach($data_jeniskendaraan as $value_jeniskendaraan) {
                            ?>
                            <option value="<?=$value_jeniskendaraan->id_kind_vehicle;?>">
                                <?=$value_jeniskendaraan->jenis_kendaraan;?>
                            </option>
                            <?php
                                }
                            ?>
                        </select>

                    </div>
                </div>


            </td>
        </tr>

        <tr>
            <td>
                <div class="form-group">
                    <label for="Remarks" class="col-xs-3">Remarks</label>
                    <div class="col-xs-5">
                        <textarea name="remarks" class="form-control" id="remarks" name="remarks" data-title="Remarks"
                            rows="5" placeholder="Remarks"></textarea>

                    </div>
                </div>
            </td>
            <td>
                <?php if($access == '1') { ?>
                <div class="form-group">
                    <label for="Remarks" class="col-xs-3">Branch</label>
                    <div class="col-xs-5">

                        <select class="form-control validation" id="branch_id" data-title="Jenis Kendaraan"
                            name="branch_id" required>
                            <option value='-'>- Pilih Branch - </option>

                            <?php
                                foreach($data_branch as $value_branch) {
                            ?>
                            <option value="<?=$value_branch->branch_id;?>"> <?=$value_branch->name_branch;?>
                            </option>
                            <?php
                                }
                            ?>
                        </select>
                        <?php } else { ?>
                        <input type="hidden" id="branch_id" name="branch_id"
                            value="<?php echo $this->session->userdata('branch_id'); ?>" />

                    </div>
                </div>
                <?php } ?>



            </td>
        </tr>

    </table>



    <div class="form-group text-center">
        <button id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
            <i class="ace-icon fa fa-floppy-o bigger-160"></i>
            Save
        </button>


        <a href="<?=base_url('demo_car/C_demo_car_stock');?>" class="btn btn-app btn-danger btn-xs radius-4">
            <i class="ace-icon fa fa-close bigger-160"></i>
            Cancel
        </a>

    </div>


    <!-- coba -->


    <div class="container">

        <div class="row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label class="control-label">Stock Number</label>

                    <input type="text" id="stock_no" name="stock_no" data-title="Stock Number" class="form-control"
                        value="<?=$stock_no;?>" readonly="readonly" />
                </div>
            </div>

            <div class="col-sm-1">


            </div>



            <div class="col-sm-5">
                <div class="form-group">
                    <label class="control-label">Branch</label>

                    <input type="text" id="branch_name" name="branch_name" data-title="Branch" class="form-control"
                        value="<?php echo $this->session->userdata('name_branch'); ?>" readonly />
                </div>
            </div>

        </div>
        <hr>

        <div class="row">
            <div class="form-group text-center">
                <button id="btnSave" name="btnSave" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
                    <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                    Save
                </button>


                <a href="<?=base_url('demo_car/C_demo_car_stock');?>" class="btn btn-app btn-danger btn-xs radius-4">
                    <i class="ace-icon fa fa-close bigger-160"></i>
                    Cancel
                </a>

            </div>
        </div>


        <hr>

    </div>
    <!-- container close -->

    <!-- coba -->


    <form role="form" id="pendaftaran-form" method="post">
        <div class="row">
            <div class="col-md-3 col-md-offset-3">
                <div class="form-group">
                    <label class="control-label">NIM</label>
                    <input type="text" name="nim" class="form-control" id="nim" minlength="10" maxlength="10" />
                    <span class="help-block" id="error"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" minlength="5" maxlength="50" />
                    <span class="help-block" id="error"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-md-offset-3">
                <div class="form-group">
                    <label class="control-label">Jenis Kelamin</label>
                    <select class="form-control" data-style="select-with-transition" title="Jenis Kelamin"
                        data-size="10" id="jenis_kelamin" name="jenis_kelamin">
                        <option value=""> Jenis Kelamin</option>
                        <option value="L"> Laki - Laki</option>
                        <option value="P"> Perempuan</option>
                    </select>
                    <span class="help-block" id="error"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Nomor Handphone</label>
                    <input type="text" name="no_hp" class="form-control" id="no_hp" minlength="10" maxlength="12" />
                    <span class="help-block" id="error"></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-md-offset-3">
                <div class="form-group">
                    <label class="control-label">Tempat Lahir</label>
                    <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir" minlength="3"
                        maxlength="25" />
                    <span class="help-block" id="error"></span>
                </div>
            </div>
            <div class="col-md-3">

                <div class="form-group">
                    <label class="control-label">Tanggal Lahir</label>
                    <input type="text" name="tanggal_lahir" class="form-control datepicker" id="tanggal_lahir"
                        data-date-format='YYYY-MM-DD' />
                    <span class="help-block" id="error"></span>
                </div>

            </div>
        </div>



        <div class="row">
            <div class="col-md-3 col-md-offset-3">
                <div class="form-group">
                    <label class="control-label">Angkatan Kuliah</label>
                    <input type="text" name="angkatan_kuliah" class="form-control" id="angkatan_kuliah" minlength="4"
                        maxlength="4" />
                    <span class="help-block" id="error"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">Angkatan Jujitsu</label>
                    <input type="text" name="angkatan_jujitsu" class="form-control" id="angkatan_jujitsu" minlength="4"
                        maxlength="4" />
                    <span class="help-block" id="error"></span>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-md-offset-3">
                <div class="form-group">
                    <label class="control-label">LINE</label>
                    <input type="text" name="line" class="form-control" id="line" minlength="3" maxlength="25" />
                    <span class="help-block" id="error"></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">WhatsApp</label>
                    <input type="text" name="whatsapp" class="form-control" id="whatsapp" minlength="10"
                        maxlength="12" />
                    <span class="help-block" id="error"></span>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3">



                <div class="form-group label-floating">
                    <label class="control-label">Alamat</label>
                    <textarea name="alamat" class="form-control" id="alamat" rows="4"></textarea>
                    <span class="help-block" id="error"></span>
                </div>
                <input type="hidden" name="csrf_protection" value="78f1b3384ac5353be4a2a900082ddb97" />


                <div class="submit text-center">
                    <button type="submit" id="btn-pendaftaran" name="btn-pendaftaran"
                        class="btn btn-success btn-raised btn-square btn-lg" value="Contact Us">Daftar</button>
                </div>
            </div>
        </div>
    </form>


    <script type="text/javascript">
        function checkExtension(e) {
            var myfile = "";
            myfile = e;
            var ext = myfile.split('.').pop();
            if (ext != "pdf") {
                alert('File Extension Must Be .pdf');
                $('#msg').html(
                    '<div class="alert alert-danger text-center"><strong>File Extension Must Be .pdf, Please Check your File Extension. </strong></div>'
                    );
                $('#btnSave').prop('disabled', true);
            } else {
                $('#msg').html('');
                $('#btnSave').prop('disabled', false);
            }

        }


        function getDataColour(e) {
            var value = e;
            var url = "<?=base_url('demo_car/C_demo_car_stock/ajax_colour?type=');?>" + value;

            $.ajax({
                url: url,
                type: "GET",
                beforeSend: function () {
                    $("#btnSave").prop('disabled', true);
                },
                success: function (data, textStatus, jqXHR) {
                    //console.log(data)
                    //$('#model').html($data);

                    $('#colour-input').html(data);

                    $("#btnSave").prop('disabled', false);
                }
            });
        }

        function getIDColour(e) {
            $('#id_colour').val(e);
        }

        $('#stock-form').submit(function (e) {
            var formData = new FormData(document.getElementById('stock-form'));
            var url = "<?=base_url('demo_car/C_demo_car_stock/add_stock');?>";

            $(".validation").each(function () {
                var val = $(this).val();
                var title = $(this).attr('data-title');

                //var desc = 
                if (val == '' || val == '-') {
                    alert(`${title} Harus di isi!`);
                    $('#cek_validasi').val('0');
                    $(this).css({
                        'border': '1px solid red'
                    });
                    $(this).focus();
                    return false;
                } else {
                    $('#cek_validasi').val('1');
                    $(this).css({
                        'border': '1px solid #d5d5d5'
                    });

                }



            });

            //return false;

            var cek_validasi = $('#cek_validasi').val();
            if (cek_validasi == '1') {
                ajax();
                return false;
            }

            function ajax() {
                $.ajax({
                    url: url,
                    method: "POST",
                    enctype: "multipart/form-data",
                    data: formData,
                    async: false,
                    processData: false,
                    contentType: false,
                    cache: false,
                    beforeSend: function () {
                        $('#btnSave').prop('disabled', true);
                    },
                    success: function (response) {
                        var json = JSON.parse(response);
                        var status_upload = json.status_upload;
                        var message_upload = json.message_upload;
                        var status_insert = json.status_insert;

                        $('#btnSave').prop('disabled', false);

                        if (status_insert == 'Insert' && status_upload == 'berhasil') {
                            alert("Insert & Upload Berhasil");
                            var url_redirect = "<?=base_url('demo_car/C_demo_car_stock');?>";
                            window.location.href = url_redirect;
                        } else if (status_insert == 'Insert Gagal' && status_upload == 'berhasil') {
                            alert("Insert Gagal, Upload Berhasil");
                        } else {
                            alert("Insert & Upload Gagal, Message : " + message_upload.error);
                        }

                    },
                    error: function (err) {
                        alert(err)
                    }
                });
                return false;
            }

            e.preventDefault();
        })
    </script>


</form>