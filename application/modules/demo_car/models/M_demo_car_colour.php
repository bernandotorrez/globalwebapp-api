<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_demo_car_colour extends CI_Model
{

    var $table = 'tbl_colour_group';
    var $column_order = array(null, null,'id_colour_group', 'colour','type_name', 'model_name'); //set column field database for datatable orderable
	var $column_search = array('id_colour_group', 'colour','type_name', 'model_name'); //set column field database for datatable searchable 
    var $order = array('id_colour_group' => 'desc'); // default order 
    var $view_table = 'qv_colour_group';

    public function getDataColour() {
        $query = $this->db->get_where($this->view_table, array('status' => '1'));
        return $query->result();
	}

    public function getDataType() {
        $query = $this->db->get_where('qv_type_group', array('status' => '1'));
        return $query->result();
    }
    
    public function getDataBrand() {
        $this->db->order_by('brand_name', 'asc');
        $query = $this->db->get_where('tbl_brand', array('status' => '1'));
        return $query->result();
    }
    
    public function getDataBrandByCompany($company) {
        $this->db->select('*');
        $this->db->from('tbl_brand tb');
        $this->db->join('tbl_company_det tcd', 'tb.id_brand = tcd.id_brand', 'inner');
        $this->db->where('tb.status', '1');
        $this->db->where('id_company', $company);
        $query = $this->db->get();
        return $query->result();
	}
	
	public function getDataModelByCompany($company) {
        $this->db->where('status', '1');
        $this->db->where('id_company', $company);
        $query = $this->db->get('qv_model_group');
        return $query->result();
    }

    public function getDataModelByID($id) {
        $this->db->where('status', '1');
        $this->db->where('id_brand', $id);
        $this->db->order_by('model_name', 'asc');
        $query = $this->db->get('tbl_model_group');
        return $query->result();
    }

    public function getDataTypeByID($id) {
        $this->db->where('status', '1');
        $this->db->where('id_model', $id);
        $this->db->order_by('type_name', 'asc');
        $query = $this->db->get('tbl_type_group');
        return $query->result();
    }

    public function get_count_id()
	{
		$this->db->select('max(id_colour_group) as totaldata');
		$this->db->from($this->view_table);
		$query=$this->db->get();
		return $query->result();
    }
    
    public function get_data($start, $length, $order, $dir) 
    {

        if($order !=null) {
            $this->db->order_by($order, $dir);
        }

        return $this->db
            ->limit($length,$start)
            ->get($this->view_table);
    }

    function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->where('status = 1');
		$query = $this->db->get();
		return $query->result();
    }
    
    private function _get_datatables_query()
	{
		
		$access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        //$access = '0';
        if($access == '1') {
            $this->db->from($this->view_table);
        } else {
            $this->db->from($this->view_table);
            $this->db->where('id_company', $id_company);
            $this->db->where('id_branch', $id_branch);
        }

        //$this->db->from($this->view_table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
    }
    
    function count_filtered()
	{
		$this->_get_datatables_query();
		$this->db->where('status = 1');
		$query = $this->db->get();
		return $query->num_rows();
    }

    public function count_all()
	{
		$this->db->from($this->view_table);
		$this->db->where('status = 1');
		return $this->db->count_all_results();
    }
    
    public function insert($data) {
        return $this->db->insert($this->table, $data);
    }

    public function delete($id, $data) {
        $this->db->where('id_colour_group', $id);
        $query = $this->db->update($this->table, $data);
        return $query;
    }
    
    public function update($id, $data) {
        $this->db->where('id_colour_group', $id);
        $query = $this->db->update($this->table, $data);
        return $query;
    }
    
    public function get_by_id($id) {
        $query = $this->db->get_where($this->view_table, array('id_colour_group' => $id));
        return $query->result();
    }

}