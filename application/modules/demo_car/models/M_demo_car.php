<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_demo_car extends CI_Model
{

    var $table = 'tbl_model_group';
    var $column_order = array(null, null,'id_model', 'brand_name','model_name','name_branch', 'company'); //set column field database for datatable orderable
	var $column_search = array('id_model','brand_name','model_name', 'name_branch', 'company'); //set column field database for datatable searchable 
    var $order = array('id_model' => 'desc'); // default order 
    var $view_table = 'qv_model_group';
    
    public function getDataModel() {
       
        $query = $this->db->get_where('qv_model_group', array('status' => '1'));
        return $query->result();
    }

    public function getDataModelByCompany($company) {
        $this->db->where('status', '1');
        $this->db->where('id_company', $company);
        $query = $this->db->get('qv_model_group');
        return $query->result();
    }


    public function getDataBrand() {
        $this->db->select('id_brand, brand_name');
        $this->db->from('tbl_brand');
        $this->db->order_by('brand_name', 'ASC');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataBrandByCompany($company) {
        $this->db->select('tb.id_brand, tb.brand_name, tb.status, tcd.id_company');
        $this->db->from('tbl_brand tb');
        $this->db->join('tbl_company_det tcd', 'tcd.id_brand = tb.id_brand', 'inner');
        $this->db->where('tb.status', '1');
        $this->db->where('tcd.id_company', $company);
        $this->db->order_by('tb.brand_name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataCompany() {
        $this->db->select('id_company, company');
        $this->db->from('tbl_company');
        $this->db->order_by('company', 'ASC');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataBranch() {
        $this->db->select('branch_id, id_company, name_branch');
        $this->db->from('tbl_branches');
        $this->db->order_by('name_branch', 'ASC');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataBranchByCompany($company) {
        $this->db->select('branch_id, id_company, name_branch');
        $this->db->from('tbl_branches');
        $this->db->order_by('name_branch', 'ASC');
        $this->db->where('status', '1');
        $this->db->where('id_company', $company);
        $query = $this->db->get();
        return $query->result();
    }

    public function insert($data) {
        return $this->db->insert('tbl_model_group', $data);
    }

    public function delete($id, $data) {
        $this->db->where('id_model', $id);
        $query = $this->db->update('tbl_model_group', $data);
        return $query;
    }
    
    public function update($id, $data) {
        $this->db->where('id_model', $id);
        $query = $this->db->update('tbl_model_group', $data);
        return $query;
    }
    
    public function get_by_id($id) {
        $query = $this->db->get_where('qv_model_group', array('id_model' => $id));
        return $query->result();
    }

    public function get_count_id()
		{
			$this->db->select('max(id_model) as totaldata');
			$this->db->from('qv_model_group');
			$query=$this->db->get();
			return $query->result();
		}

    public function get_data($start, $length, $order, $dir) 
    {

        if($order !=null) {
            $this->db->order_by($order, $dir);
        }

        return $this->db
            ->limit($length,$start)
            ->get("qv_model_group");
    }

    function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->where('status = 1');
		$query = $this->db->get();
		return $query->result();
    }
    
    private function _get_datatables_query()
	{
        
        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        //$access = '0';
        if($access == '1') {
            $this->db->from($this->view_table);
        } else {
            $this->db->from($this->view_table);
            $this->db->where('id_company', $id_company);
            $this->db->where('id_branch', $id_branch);
        }
		

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
    }
    
    function count_filtered()
	{
		$this->_get_datatables_query();
		$this->db->where('status = 1');
		$query = $this->db->get();
		return $query->num_rows();
    }

    public function count_all()
	{
		$this->db->from($this->view_table);
		$this->db->where('status = 1');
		return $this->db->count_all_results();
	}

}