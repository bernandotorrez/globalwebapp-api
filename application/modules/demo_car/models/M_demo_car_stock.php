<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_demo_car_stock extends CI_Model
{

    var $table = 'tbl_stock_demo_car';
    var $column_order = array(null, null,'stock_no', 'name_location', 'type_name', 'vin', 'receive_date', 'veh_status' , 'name_branch', 'stock_keeper', 'colour', 'upload_vin', 'engine', 'remarks', 'status_stock', 'no_polisi', 'jenis_kendaraan'); //set column field database for datatable orderable
	var $column_search = array('stock_no', 'name_location', 'type_name', 'vin', 'receive_date', 'veh_status' , 'name_branch', 'stock_keeper', 'colour', 'upload_vin', 'engine', 'remarks', 'status_stock', 'no_polisi','jenis_kendaraan'); //set column field database for datatable searchable 
    var $order = array('stock_no' => 'desc'); // default order 
    var $view_table = 'qv_stock_demo_car';

    public function getDataStock() {
        $query = $this->db->get_where($this->view_table, array('veh_status' => '1'));
        return $query->result();
	}
	
	public function getDataLocationStock() {
		$this->db->order_by('name_location', 'asc');
		$query = $this->db->get_where('tbl_location_stock_group', array('status' => '1'));
        return $query->result();
	}

	public function getDataLocationStockByBranch($branch) {
		$this->db->where('status', '1');
		$this->db->where('branch_id', $branch);
		$this->db->order_by('name_location', 'asc');
		$query = $this->db->get('tbl_location_stock_group');
        return $query->result();
	}

	public function getDataJenisKendaraan() {
		$this->db->order_by('jenis_kendaraan', 'asc');
		$query = $this->db->get_where('tbl_kind_vehicle', array('status' => '1'));
        return $query->result();
	}

    public function getDataType() {
		$this->db->order_by('type_name', 'asc');
		$query = $this->db->get_where('qv_type_group', array('status' => '1'));
        return $query->result();
	}
	
	public function getDataTypeByCompany($company, $branch) {
		$this->db->order_by('type_name', 'asc');
		$this->db->where('status', '1');
		$this->db->where('id_company', $company);
		$this->db->where('id_branch', $branch);
		$query = $this->db->get('qv_type_group');
        return $query->result();
	}

	public function getTypeStock($company) {
		$this->db->where('status', '1');
		$this->db->where('id_company', $company);
		$query = $this->db->get('qv_get_type_for_stock_democar');
		return $query->result();
	}
 
	public function getDataColourByType($id) {
        $this->db->where('status', '1');
        $this->db->where('id_type', $id);
        $this->db->order_by('colour', 'asc');
        $query = $this->db->get('tbl_colour_group');
        return $query->result();
	}
	
	public function getDataBranch() {
        $this->db->select('branch_id, id_company, name_branch');
        $this->db->from('tbl_branches');
        $this->db->order_by('name_branch', 'ASC');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }


    public function get_count_id()
	{
		//substr($string, -4);
		$short_company = $this->session->userdata('short');
        $short_branch = $this->session->userdata('branch_short');
        $tahun = substr(date('Y'), 1, 3);
        $bulan = date('m');
        $tanggal = date('d');
		$search = $short_company.'/'.$short_branch.'/'.$tahun.'/'.$bulan.'/'.$tanggal;
		$this->db->select('max(stock_no) as totaldata');
		$this->db->from($this->table);
		$this->db->like('stock_no', $search);
		$query=$this->db->get();
		return $query->result();
    }
    
    public function get_data($start, $length, $order, $dir) 
    {

        if($order !=null) {
            $this->db->order_by($order, $dir);
        }

        return $this->db
            ->limit($length,$start)
            ->get($this->view_table);
    }

    function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->where('veh_status = 1');
		$query = $this->db->get();
		return $query->result();
    }
    
    private function _get_datatables_query()
	{
		
		$access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        //$access = '0';
        if($access == '1') {
            $this->db->from($this->view_table);
        } else {
            $this->db->from($this->view_table);
			$this->db->where('id_company', $id_company);
			$this->db->where('branch_id', $id_branch);
        }

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
    }
    
    function count_filtered()
	{
		$this->_get_datatables_query();
		$this->db->where('veh_status = 1');
		$query = $this->db->get();
		return $query->num_rows();
    }

    public function count_all()
	{
		$this->db->from($this->view_table);
		$this->db->where('veh_status = 1');
		return $this->db->count_all_results();
    }
    
    public function insert($data) {
        return $this->db->insert($this->table, $data);
    }

    public function delete($id, $data) {
        $this->db->where('stock_no', $id);
        $query = $this->db->update($this->table, $data);
        return $query;
    }
    
    public function update($id, $data) {
        $this->db->where('stock_no', $id);
        $query = $this->db->update($this->table, $data);
        return $query;
    }
    
    public function get_by_id($id) {
        $query = $this->db->get_where($this->view_table, array('stock_no' => $id));
        return $query->result();
	}
	
	public function get_no_pol($no_pol) {
		$this->db->select('no_polisi');
		$query = $this->db->get_where($this->table, array('no_polisi' => $no_pol));
        return $query->num_rows();
	}

}