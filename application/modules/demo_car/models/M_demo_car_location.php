<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_demo_car_location extends CI_Model
{

    var $table = 'tbl_location_stock_group';
    var $column_order = array(null, null,'id_locstock', 'name_location','alamat', 'name_branch'); //set column field database for datatable orderable
	var $column_search = array('id_locstock', 'name_location','alamat', 'name_branch'); //set column field database for datatable searchable 
    var $order = array('id_locstock' => 'desc'); // default order 
    var $view_table = 'qv_location_stock_group';
    
    public function getDataModel() {
       
        $query = $this->db->get_where($this->view_table, array('status' => '1'));
        return $query->result();
	}
	
	public function getDataBranch() {
		$this->db->order_by('name_branch', 'asc');
        $query = $this->db->get_where('tbl_branches', array('status' => '1'));
        return $query->result();
	}

    public function insert($data) {
        return $this->db->insert($this->table, $data);
    }

    public function delete($id, $data) {
        $this->db->where('id_locstock', $id);
        $query = $this->db->update($this->table, $data);
        return $query;
    }
    
    public function update($id, $data) {
        $this->db->where('id_locstock', $id);
        $query = $this->db->update($this->table, $data);
        return $query;
    }
    
    public function get_by_id($id) {
        $query = $this->db->get_where($this->view_table, array('id_locstock' => $id));
        return $query->result();
    }

    public function get_count_id()
		{
			$this->db->select('max(id_locstock) as totaldata');
			$this->db->from($this->view_table);
			$query=$this->db->get();
			return $query->result();
		}

    public function get_data($start, $length, $order, $dir) 
    {

        if($order !=null) {
            $this->db->order_by($order, $dir);
        }

        return $this->db
            ->limit($length,$start)
            ->get($this->view_table);
    }

    function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->where('status = 1');
		$query = $this->db->get();
		return $query->result();
    }
    
    private function _get_datatables_query()
	{
		
		$access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        //$access = '0';
        if($access == '1') {
            $this->db->from($this->view_table);
        } else {
            $this->db->from($this->view_table);
            $this->db->where('branch_id', $id_branch);
        }

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
    }
    
    function count_filtered()
	{
		$this->_get_datatables_query();
		$this->db->where('status = 1');
		$query = $this->db->get();
		return $query->num_rows();
    }

    public function count_all()
	{
		$this->db->from($this->view_table);
		$this->db->where('status = 1');
		return $this->db->count_all_results();
	}

}