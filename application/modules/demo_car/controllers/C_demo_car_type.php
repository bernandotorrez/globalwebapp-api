<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_demo_car_type extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_demo_car_type');
    }

    public function index() {
        $data['show_view'] = 'demo_car/V_demo_car_type';

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        //$access = '0';
        if($access == '1') {
            $data['data_model'] = $this->M_demo_car_type->getDataModel();
        } else {
            $data['data_model'] = $this->M_demo_car_type->getDataModelByCompany($id_company);
        }

        $data['total'] = $this->M_demo_car_type->get_count_id();
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function ajax_list()
	{
		$list = $this->M_demo_car_type->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_type) {
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_type->id_type.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;                       // No
			$row[] = $tbl_type->id_type;      // ID Model
            $row[] = $tbl_type->model_name;    // Brand Name
            $row[] = $tbl_type->type_name;    // Model Name
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_demo_car_type->count_all(),
						"recordsFiltered" => $this->M_demo_car_type->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
    }

    function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
        $this->db->from('qv_type_group');
		$this->db->where('status = 1');
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
            $this->db->like("id_type",$search);
            $this->db->or_like("model_name",$search);
            $this->db->or_like("type_name",$search);
            $this->db->where('status = 1');
		}


		/*Lanjutkan pencarian ke database*/
		//$this->db->limit($length,$start);
		//$this->db->where('status = 1');
		/*Urutkan dari alphabet paling terkahir*/
		//$this->db->order_by('id_type','ASC');
		//$query=$this->db->get('tbl_type');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
            $this->db->like("id_type",$search);
            $jum=$this->db->get('qv_type_group');
            $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		// Datatables Variables
        $order = $this->input->get("order");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }

        $columns_valid = array(
            "qv_type_group.id_type", 
            "qv_type_group.model_name",
            "qv_type_group.type_name"
        );

        if(!isset($columns_valid[$col])) {
            $order = null;
        } else {
            $order = $columns_valid[$col];
        }
		$this->db->where('status = 1');
        $query = $this->M_demo_car_type->get_data($start, $length, $order, $dir);
		
		foreach ($query->result_array() as $tbl_type) {
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_type["id_type"].' class="editRow ace" req_id_del='.$tbl_type["id_type"].' />
           <span class="lbl"></span> ';
		   
			$output['data'][]=array($tbl_type['id_type'],$tbl_type['model_name'], $tbl_type['type_name'],
									$chk_idmaster);

		}

		echo json_encode($output);


	}
    
    public function ajax_edit($id)
    {
        $data = $this->M_demo_car_type->get_by_id($id);
        echo json_encode($data);
    }

    public function add_type(){
        $id_model = $this->input->post('model_name');
        $type_name = $this->input->post('type_name');
        $status = '1';

        $data = array(  'id_model' => $id_model,
                        'type_name' => strtoupper($type_name),
                        'status' => $status
                    );

        $insert = $this->M_demo_car_type->insert($data);
        
        if($insert) {
            echo 'Insert';
        } else {
            echo 'Insert Gagal';
        }

    }

    public function update_type(){
        $id_type = $this->input->post('id_type');
        $id_model = $this->input->post('model_name');
        $type_name = $this->input->post('type_name');
        $status = '1';

        $data = array(  'id_model' => $id_model,
                        'type_name' => strtoupper($type_name),
                        'status' => $status
                    );

        $update = $this->M_demo_car_type->update($id_type, $data);
        
        if($update) {
            echo 'Update';
        } else {
            echo 'Update Gagal';
        }

    }

    public function delete_data() {
        $id_type = $this->input->post('ID');

        $data_id_array = explode(",", $id_type);
        if(!empty($data_id_array)) {
            foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);
				$delete = $this->M_demo_car_type->delete($id, $data);
			}
        } 

        echo json_encode('Delete');
    }

    public function coba() {
        $data['show_view'] = 'demo_car/V_demo_car_type1';
        $data['data_model'] = $this->M_demo_car_type->getDataModel();
        $data['data_type'] = $this->M_demo_car_type->getDataType();
        $data['total'] = $this->M_demo_car_type->get_count_id();
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

}