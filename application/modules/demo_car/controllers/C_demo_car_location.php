<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_demo_car_location extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_demo_car_location');
    }

    public function index() {
        
        $data['show_view'] = 'demo_car/V_demo_car_location';
		$data['total'] = $this->M_demo_car_location->get_count_id();
		$data['data_branch'] = $this->M_demo_car_location->getDataBranch();
		$access = $this->session->userdata('apps_accsess');
		//$access = '0';
		$data['access'] = $access;

        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function add_location(){
        $name_location = $this->input->post('name_location');
        $alamat = $this->input->post('alamat');

		$access = $this->session->userdata('apps_accsess');
        
        if($access == '1') {
            $branch_id = $this->input->post('branch');
        } else {
            $branch_id = $this->session->userdata('branch_id');
        }

        $status = '1';

        $data = array(  'name_location' => ucwords($name_location),
                        'alamat' => ucwords($alamat),
                        'status' => $status,
                        'branch_id' => $branch_id
                    );

        $insert = $this->M_demo_car_location->insert($data);
        
        if($insert) {
            echo 'Insert';
        } else {
            echo 'Insert Gagal';
        }

    }

    public function update_location(){
        $id_location = $this->input->post('id_location');
        $name_location = $this->input->post('name_location');
		$alamat = $this->input->post('alamat');
		
		$access = $this->session->userdata('apps_accsess');
        //$access = '0';
        if($access == '1') {
            $branch_id = $this->input->post('branch');
        } else {
            $branch_id = $this->session->userdata('branch_id');
        }

        $status = '1';

        $data = array(  'name_location' => ucwords($name_location),
                        'alamat' => ucwords($alamat),
						'status' => $status,
						'branch_id' => $branch_id
                    );

        $update = $this->M_demo_car_location->update($id_location, $data);
        
        if($update) {
            echo 'Update';
        } else {
            echo 'Update Gagal';
        }

    }

    public function delete_data() {
        $id_location = $this->input->post('ID');

        $data_id_array = explode(",", $id_location);
        if(!empty($data_id_array)) {
            foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);
				$delete = $this->M_demo_car_location->delete($id, $data);
			}
        } 

        echo json_encode('Delete');
    }

    public function ajax_edit($id)
		{
			$data = $this->M_demo_car_location->get_by_id($id);
			echo json_encode($data);
		}

    public function ajax_list()
	{
		$list = $this->M_demo_car_location->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_location) {
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_location->id_locstock.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;                       // No
			$row[] = $tbl_location->id_locstock;      // ID Model
            $row[] = $tbl_location->name_location;    // Brand Name
            $row[] = $tbl_location->alamat;    // Model Name
            $row[] = $tbl_location->name_branch;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_demo_car_location->count_all(),
						"recordsFiltered" => $this->M_demo_car_location->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

    function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
        $this->db->from('tbl_location_stock_group');
		$this->db->where('status = 1');
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
            $this->db->like("id_locstock",$search);
            $this->db->or_like("name_location",$search);
            $this->db->or_like("alamat",$search);
            $this->db->where('status = 1');
		}


		/*Lanjutkan pencarian ke database*/
		//$this->db->limit($length,$start);
		//$this->db->where('status = 1');
		/*Urutkan dari alphabet paling terkahir*/
		//$this->db->order_by('id_location','ASC');
		//$query=$this->db->get('tbl_location');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
            $this->db->like("id_locstock",$search);
            $jum=$this->db->get('tbl_location_stock_group');
            $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		// Datatables Variables
        $order = $this->input->get("order");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }

        $columns_valid = array(
            "tbl_location_stock_group.id_locstock", 
            "tbl_location_stock_group.brand_name",
            "tbl_location_stock_group.model_name"
        );

        if(!isset($columns_valid[$col])) {
            $order = null;
        } else {
            $order = $columns_valid[$col];
        }
		$this->db->where('status = 1');
        $query = $this->M_demo_car_location->get_data($start, $length, $order, $dir);
		
		foreach ($query->result_array() as $tbl_location) {
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_location["id_locstock"].' class="editRow ace" req_id_del='.$tbl_location["id_locstock"].' />
           <span class="lbl"></span> ';
		   
			$output['data'][]=array($tbl_location['id_locstock'],$tbl_location['name_location'], $tbl_location['alamat'],
									$chk_idmaster);

		}

		echo json_encode($output);


	}

}