<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_demo_car extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_demo_car');
    }

    public function index() {
        //echo '<pre>';print_r($this->session->userdata());die;
        $data['show_view'] = 'demo_car/V_demo_car';

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        //$access = '0';
        if($access == '1') {
            $data['data_brand'] = $this->M_demo_car->getDataBrand();
        } else {
            $data['data_brand'] = $this->M_demo_car->getDataBrandByCompany($id_company);
        }

        $data['data_company'] = $this->M_demo_car->getDataCompany();
        $data['data_branch'] = $this->M_demo_car->getDataBranch();
        $data['total'] = $this->M_demo_car->get_count_id();
        $data['access'] = $access;
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function add_model(){
        $id_brand = $this->input->post('brand_name');
        $model_name = $this->input->post('model_name');
        $access = $this->session->userdata('apps_accsess');
        //$access = '0';
        if($access == '1') {
            $id_company = $this->input->post('company');
            $id_branch = $this->input->post('branch');
        } else {
            $id_company = $this->session->userdata('id_company');
            $id_branch = $this->session->userdata('branch_id');
        }

        $status = '1';

        $data = array(  'id_brand' => $id_brand,
                        'model_name' => strtoupper($model_name),
                        'status' => $status,
                        'id_company' => $id_company,
                        'id_branch' => $id_branch
                    );

        $insert = $this->M_demo_car->insert($data);
        
        if($insert) {
            echo 'Insert';
        } else {
            echo 'Insert Gagal';
        }

    }

    public function update_model(){
        $id_model = $this->input->post('id_model');
        $id_brand = $this->input->post('brand_name');
        $model_name = $this->input->post('model_name');
        $access = $this->session->userdata('apps_accsess');
        //$access = '0';
        if($access == '1') {
            $id_company = $this->input->post('company');
            $id_branch = $this->input->post('branch');
        } else {
            $id_company = $this->session->userdata('id_company');
            $id_branch = $this->session->userdata('branch_id');
        }
        $status = '1';

        $data = array(  'id_brand' => $id_brand,
                        'model_name' => strtoupper($model_name),
                        'status' => $status,
                        'id_company' => $id_company,
                        'id_branch' => $id_branch
                    );

        $update = $this->M_demo_car->update($id_model, $data);
        
        if($update) {
            echo 'Update';
        } else {
            echo 'Update Gagal';
        }

    }

    public function delete_data() {
       
        $id_model = $this->input->post('ID');

        $data_id_array = explode(",", $id_model);
        if(!empty($data_id_array)) {
            foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);
				$delete = $this->M_demo_car->delete($id, $data);
			}
        } 

        echo json_encode('Delete');
    }

    public function ajax_edit($id)
		{
			$data = $this->M_demo_car->get_by_id($id);
            //echo json_encode($data);

            // Branch
            $data_branch = $this->M_demo_car->getDataBranchByCompany($data[0]->id_company);

            $html_branch = '<label for="model_name">Type</label>
            <select class="form-control" name="branch" id="branch">
            <option value=""> - Pilih Branch - </option>
            ';

            foreach($data_branch as $value_type) {
                $html_branch .= '<option value="'.$value_type->branch_id.'"> '.$value_type->name_branch.' </option>';
            }       

            $html_branch .= '</select>';

            
            $callback = array('data' => $data,
                            'html_branch' => $html_branch
        );

        echo json_encode($callback);
		}

    public function ajax_list()
	{
		$list = $this->M_demo_car->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_model) {
            
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_model->id_model.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;                       // No
			$row[] = $tbl_model->id_model;      // ID Model
            $row[] = $tbl_model->brand_name;    // Brand Name
            $row[] = $tbl_model->model_name;    // Model Name
            $row[] = $tbl_model->company;       // Brand Name
            $row[] = $tbl_model->name_branch;        // Model Name
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_demo_car->count_all(),
						"recordsFiltered" => $this->M_demo_car->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

    function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
        $this->db->from('qv_model_group');
		$this->db->where('status = 1');
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
            $this->db->like("id_model",$search);
            $this->db->or_like("brand_name",$search);
            $this->db->or_like("model_name",$search);
            $this->db->where('status = 1');
		}


		/*Lanjutkan pencarian ke database*/
		//$this->db->limit($length,$start);
		//$this->db->where('status = 1');
		/*Urutkan dari alphabet paling terkahir*/
		//$this->db->order_by('id_model','ASC');
		//$query=$this->db->get('tbl_model');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
            $this->db->like("id_model",$search);
            $jum=$this->db->get('qv_model_group');
            $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		// Datatables Variables
        $order = $this->input->get("order");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }

        $columns_valid = array(
            "qv_model_group.id_model", 
            "qv_model_group.brand_name",
            "qv_model_group.model_name"
        );

        if(!isset($columns_valid[$col])) {
            $order = null;
        } else {
            $order = $columns_valid[$col];
        }
		$this->db->where('status = 1');
        $query = $this->M_demo_car->get_data($start, $length, $order, $dir);
		
		foreach ($query->result_array() as $tbl_model) {
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_model["id_model"].' class="editRow ace" req_id_del='.$tbl_model["id_model"].' />
           <span class="lbl"></span> ';
		   
			$output['data'][]=array($tbl_model['id_model'],$tbl_model['brand_name'], $tbl_model['model_name'],
									$chk_idmaster);

		}

		echo json_encode($output);


    }
    
    public function ajax_branch() {
        $id_company = $this->input->get('company');

        $data = $this->M_demo_car->getDataBranchByCompany($id_company);

        $html = ' <label for="model_name">Branch</label>
                
        <select class="form-control" id="branch" name="branch">
            <option value=""> - Pilih Branch - </option>
        ';

        foreach($data as $value) {
            $html .= '<option value="'.$value->branch_id.'"> '.$value->name_branch.' </option>';
        }       

        $html .= '</select>';

        echo $html;
        
    }

}