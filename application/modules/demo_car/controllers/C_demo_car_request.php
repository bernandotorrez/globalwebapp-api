<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_demo_car_request extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_demo_car_request');
        $this->load->helper('xss_filter');
    }

    public function index() {
        //echo '<pre>';print_r($this->session->userdata());die;

        // Check Flag Approval Demo Car
        $flag = $this->session->userdata('approval_democar');
        if($flag == 0) {
            $data['show_view'] = 'demo_car/V_demo_car_request';
        } else {
            $data['show_view'] = 'demo_car/V_demo_car_request_approval';
        }
        // Check Flag Approval Demo Car
        
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function add_dc() {

        // Check Flag Approval Demo Car
        $flag = $this->session->userdata('approval_democar');
        if($flag != 0) {
            redirect(base_url('demo_car/C_demo_car_request'));
        }
        // Check Flag Approval Demo Car

        $data['id_req_democar'] = $this->id_req_democar();
        $data['show_view'] = 'demo_car/V_demo_car_request_add_democar';

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');

        $status_stock = 'Demo';
        //$access = '0';
        if($access == '1') {
            $data['data_stock'] = $this->M_demo_car_request->getDataStock();
        } else {
            $data['data_stock'] = $this->M_demo_car_request->getDataStockByCompany($id_company, $id_branch, $status_stock);
        }

       
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function add_opr() {

        // Check Flag Approval Demo Car
        $flag = $this->session->userdata('approval_democar');
        if($flag != 0) {
            redirect(base_url('demo_car/C_demo_car_request'));
        }
        // Check Flag Approval Demo Car

        $data['id_req_democar'] = $this->id_req_democar();
        $data['show_view'] = 'demo_car/V_demo_car_request_add_operational';

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');

        $status_stock = 'Operational';
        //$access = '0';
        if($access == '1') {
            $data['data_stock'] = $this->M_demo_car_request->getDataStock();
        } else {
            $data['data_stock'] = $this->M_demo_car_request->getDataStockByCompany($id_company, $id_branch, $status_stock);
        }

       
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function add_dcopr() {

        // Check Flag Approval Demo Car
        $flag = $this->session->userdata('approval_democar');
        if($flag != 0) {
            redirect(base_url('demo_car/C_demo_car_request'));
        }
        // Check Flag Approval Demo Car

        $data['id_req_democar'] = $this->id_req_democar();
        $data['show_view'] = 'demo_car/V_demo_car_request_add_democar_operational';

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');

        $status_stock = '';
        //$access = '0';
        if($access == '1') {
            $data['data_stock'] = $this->M_demo_car_request->getDataStock();
        } else {
            $data['data_stock'] = $this->M_demo_car_request->getDataStockByCompany($id_company, $id_branch, $status_stock);
        }

       
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function ajax_list()
	{
		$list = $this->M_demo_car_request->get_datatables();
		$data = array();
        $no = $_POST['start'];
        $flag_approval_democar = $this->session->userdata('approval_democar');

        $access = $this->session->userdata('apps_accsess');
        
		foreach ($list as $tbl_request) {
            //if($this->session->userdata('approval_democar') == '0') {
                if($tbl_request->send_approval == '0' || $tbl_request->send_approval == '2') {
            $chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" data-print='.base64_encode($tbl_request->id_req_democar_group).' 
            data-approval='.$tbl_request->send_approval.' data-sign1 = '.$tbl_request->sign_1.' data-sign2 = '.$tbl_request->sign_2.' data-status = '.$tbl_request->flag_status_req.' value='.base64_encode($tbl_request->id_req_democar_group).' class="editRow ace" />
           <span class="lbl"></span> ';
            $no++;
            $url_print = base_url("demo_car/C_demo_car_request/pdf?id=").base64_encode($tbl_request->id_req_democar_group).'#toolbar=0';

            if($tbl_request->sign_req == '0') {
                $sign_req = '<i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true"></i>';
            } else {
                $sign_req = '<i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true"></i>';
            }

            // 0 = Belum di approve
            // 1 = Approved
            // 2 = Rejected

            if($tbl_request->sign_1 == '0') {
                $sign_1 = '<i class="fa fa-question-circle fa-2x text-danger" aria-hidden="true" title="Approval not yet Received"></i>';
            } elseif($tbl_request->sign_1 == '1') {
                $sign_1 = '<i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true" title="Approved"></i>';
            } else {
                $sign_1 = '<i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true" title="Rejected"></i>';
            }

            // 0 = Belum di approve
            // 1 = Approved
            // 2 = Rejected

            if($tbl_request->sign_2 == '0') {
                $sign_2 = '<i class="fa fa-question-circle fa-2x text-danger" aria-hidden="true" title="Approval not yet Received"></i>';
            } elseif($tbl_request->sign_2 == '1') {
                $sign_2 = '<i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true" title="Approved"></i>';
            } else {
                $sign_2 = '<i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true" title="Rejected"></i>';
            }

            // 0 = Belum dikirim
            // 1 = Sudah dikirim
            // 2 = Ended

            if($tbl_request->send_approval == '0') {
                $send_approval = '<i class="fa fa-question-circle fa-2x" aria-hidden="true" title="Approved no yet Sent"></i>';
            } elseif($tbl_request->send_approval == '1' || $tbl_request->send_approval == '2') {
                if($tbl_request->sign_1 == '1' && $tbl_request->sign_2 == '1') {
                    if($tbl_request->flag_print == '0') {
                        $send_approval = '<a target="_blank" href="'.$url_print.'"><i class="fa fa-print fa-2x text-success" aria-hidden="true" title="Print PDF"></i></a>';
                    } else {
                        $send_approval = '<i onClick="alert(\'Data Already Printed!\')" class="fa fa-print fa-2x text-danger" aria-hidden="true" title="Already Print PDF"></i>';
                    }
                    
                } else {
                    $send_approval = '<i class="fa fa-envelope fa-2x text-info" aria-hidden="true" title="Approval Sent, and Being Checked by Sign 1 or Sign 2"></i>';
                }
                
            }

            // Disable button print when user is sign 1 and sign 2
            if($flag_approval_democar != 0) {
                $send_approval = '<i onClick="alert(\'Because youre Approval, You cant Print this Data!\')" class="fa fa-print fa-2x text-danger" aria-hidden="true" title="Already Print PDF"></i>';
            }

            $id_req_democar_group = "<span class='label label-primary arrowed-in-right arrowed'>$tbl_request->id_req_democar_group</span>";

            $stock_no = "<span class='label label-primary arrowed-in-right arrowed'>$tbl_request->stock_no</span>";
            $jenis_kendaraan = "<span class='label label-info arrowed-in arrowed-in-right'>$tbl_request->jenis_kendaraan</span>";
            $type_name = "<span class='label label-info arrowed-in arrowed-in-right'>$tbl_request->type_name</span>";
           
            $give_right = '<button type="button" class="btn btn-success" onClick="giveRight(this)" req_id="'.base64_encode($tbl_request->id_req_democar_group).'"> Give Right </button>';

            if($tbl_request->flag_status_req == 1) {
                $status_req = "<span class='label label-success arrowed-in arrowed-in-right'>Demo</span>";
            } elseif($tbl_request->flag_status_req == 2) {
                $status_req = "<span class='label label-info arrowed-in arrowed-in-right'>Operational</span>";
            } elseif($tbl_request->flag_status_req == 3) {
                $status_req = "<span class='label label-warning arrowed-in arrowed-in-right'>Demo For Operational</span>";
            } else {
                $status_req = "-";
            }

			$row = array();
			$row[] = $chk_idmaster;
            $row[] = $no;                       // No
            $row[] = $sign_req;
            $row[] = $sign_1;
            $row[] = $sign_2;
            $row[] = $send_approval;

            if($access == 1) {
                $row[] = $give_right;
            }
            

            $row[] = $id_req_democar_group;      // ID Model
            $row[] = $tbl_request->nama_requester;
            $row[] = $tbl_request->jabatan_requester;
            $row[] = $status_req;
            $row[] = $tbl_request->sales_name;
            $row[] = $tbl_request->contact_sales;
			$row[] = $tbl_request->nama_konsumen;      // ID Model
            $row[] = $tbl_request->no_ktp;    // Brand Name
            $row[] = $tbl_request->alamat;    // Model Name
            $row[] = $tbl_request->no_hp;
            $row[] = $stock_no;
            $row[] = $jenis_kendaraan;
            $row[] = $type_name;
            $row[] = $tbl_request->no_polisi;
            $row[] = $tbl_request->vin;         // rangka
            $row[] = $tbl_request->name_location;
            $row[] = $tbl_request->company;
            $row[] = $tbl_request->name_branch;
            $row[] = $tbl_request->tujuan;
            //$row[] = "<a target='_blank' href=".$url.">Lihat</a>";
            $row[] = $tbl_request->kilo_berangkat;
            $row[] = $tbl_request->kilo_pulang;
            $row[] = $tbl_request->keterangan;
           
			$data[] = $row;
            }
        //}
    }

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_demo_car_request->count_all(),
						"recordsFiltered" => $this->M_demo_car_request->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
    }
    
    public function ajax_edit($id)
    {
        $data = $this->M_demo_car_request->get_by_id($id);
        echo json_encode($data);
    }

    public function add_request_democar(){

        $id_req_democar = $this->input->post('id_req_democar');
        $nama_konsumen = $this->input->post('nama_konsumen');
        $no_ktp = $this->input->post('no_ktp');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $stock_no = base64_decode($this->input->post('stock_no'));
        $tujuan = checkNULL($this->input->post('dari')).' : '.checkNULL($this->input->post('ke'));
        $kilometer_berangkat = $this->input->post('kilometer_berangkat');
        $kilometer_kepulangan = checkNULL($this->input->post('kilometer_kepulangan'));
        $keterangan = checkNULL($this->input->post('keterangan'));
        $nama_requester = $this->input->post('nama_requester');
        $jabatan_requester = checkNULL($this->input->post('jabatan_requester'));
        $contact_sales = $this->input->post('contact_sales');
        $flag_status_req = $this->input->post('flag_status_req');
        $status = '1';
        $sign_req = '1';
        $sign_1 = '0';
        $sign_2 = '0';
        $sales_name = checkNULL($this->input->post('sales_name'));
        $submission_email = $this->session->userdata('email');

         $data = array(  'id_req_democar_group' => $id_req_democar,
                            'nama_konsumen' => ucwords($nama_konsumen),
                            'no_ktp' => $no_ktp,
                            'alamat' => ucwords($alamat),
                            'no_hp' => $no_hp,
                            'stock_no' => $stock_no,
                            'tujuan' => ucwords($tujuan),
                            'kilo_berangkat' => $kilometer_berangkat,
                            'kilo_pulang' => $kilometer_kepulangan,
                            'keterangan' => ucwords($keterangan),
                            'nama_requester' => ucwords($nama_requester),
                            'jabatan_requester' => ucwords($jabatan_requester),
                            'contact_sales' => $contact_sales,
                            'sign_req' => $sign_req,
                            'sign_1' => $sign_1,
                            'sign_2' => $sign_2,
                            'status' => $status,
                            'sales_name' => $sales_name,
                            'submission_email' => $submission_email,
                            'flag_status_req' => $flag_status_req
                        );

        $insert = $this->M_demo_car_request->insert($data);

        if($insert) {
            echo 'Insert';
        } else {
            echo 'Insert Gagal';
        }
           

    }

    public function add_request_operational(){

        $id_req_democar = $this->input->post('id_req_democar');
        $nama_konsumen = '-';
        $no_ktp = $this->input->post('no_ktp');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $stock_no = base64_decode($this->input->post('stock_no'));
        $tujuan = checkNULL($this->input->post('dari')).' : '.checkNULL($this->input->post('ke'));
        $kilometer_berangkat = $this->input->post('kilometer_berangkat');
        $kilometer_kepulangan = checkNULL($this->input->post('kilometer_kepulangan'));
        $keterangan = checkNULL($this->input->post('keterangan'));
        $nama_requester = $this->input->post('nama_requester');
        $jabatan_requester = checkNULL($this->input->post('jabatan_requester'));
        $contact_sales = '-';
        $flag_status_req = $this->input->post('flag_status_req');
        $status = '1';
        $sign_req = '1';
        $sign_1 = '0';
        $sign_2 = '0';
        $sales_name = '-';
        $submission_email = $this->session->userdata('email');

         $data = array(  'id_req_democar_group' => $id_req_democar,
                            'nama_konsumen' => ucwords($nama_konsumen),
                            'no_ktp' => $no_ktp,
                            'alamat' => ucwords($alamat),
                            'no_hp' => $no_hp,
                            'stock_no' => $stock_no,
                            'tujuan' => ucwords($tujuan),
                            'kilo_berangkat' => $kilometer_berangkat,
                            'kilo_pulang' => $kilometer_kepulangan,
                            'keterangan' => ucwords($keterangan),
                            'nama_requester' => ucwords($nama_requester),
                            'jabatan_requester' => ucwords($jabatan_requester),
                            'contact_sales' => $contact_sales,
                            'sign_req' => $sign_req,
                            'sign_1' => $sign_1,
                            'sign_2' => $sign_2,
                            'status' => $status,
                            'sales_name' => $sales_name,
                            'submission_email' => $submission_email,
                            'flag_status_req' => $flag_status_req
                        );

        $insert = $this->M_demo_car_request->insert($data);

        if($insert) {
            echo 'Insert';
        } else {
            echo 'Insert Gagal';
        }
           

    }

    public function add_request_demo_operational(){

        $id_req_democar = $this->input->post('id_req_democar');
        $nama_konsumen = '-';
        $no_ktp = $this->input->post('no_ktp');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $stock_no = base64_decode($this->input->post('stock_no'));
        $tujuan = checkNULL($this->input->post('dari')).' : '.checkNULL($this->input->post('ke'));
        $kilometer_berangkat = $this->input->post('kilometer_berangkat');
        $kilometer_kepulangan = checkNULL($this->input->post('kilometer_kepulangan'));
        $keterangan = checkNULL($this->input->post('keterangan'));
        $nama_requester = $this->input->post('nama_requester');
        $jabatan_requester = checkNULL($this->input->post('jabatan_requester'));
        $contact_sales = '-';
        $flag_status_req = $this->input->post('flag_status_req');
        $status = '1';
        $sign_req = '1';
        $sign_1 = '0';
        $sign_2 = '0';
        $sales_name = '-';
        $submission_email = $this->session->userdata('email');

         $data = array(  'id_req_democar_group' => $id_req_democar,
                            'nama_konsumen' => ucwords($nama_konsumen),
                            'no_ktp' => $no_ktp,
                            'alamat' => ucwords($alamat),
                            'no_hp' => $no_hp,
                            'stock_no' => $stock_no,
                            'tujuan' => ucwords($tujuan),
                            'kilo_berangkat' => $kilometer_berangkat,
                            'kilo_pulang' => $kilometer_kepulangan,
                            'keterangan' => ucwords($keterangan),
                            'nama_requester' => ucwords($nama_requester),
                            'jabatan_requester' => ucwords($jabatan_requester),
                            'contact_sales' => $contact_sales,
                            'sign_req' => $sign_req,
                            'sign_1' => $sign_1,
                            'sign_2' => $sign_2,
                            'status' => $status,
                            'sales_name' => $sales_name,
                            'submission_email' => $submission_email,
                            'flag_status_req' => $flag_status_req
                        );

        $insert = $this->M_demo_car_request->insert($data);

        if($insert) {
            echo 'Insert';
        } else {
            echo 'Insert Gagal';
        }
           

    }

    public function update_request_democar(){

        $id_req_democar = $this->input->post('id_req_democar');
        $nama_konsumen = $this->input->post('nama_konsumen');
        $no_ktp = $this->input->post('no_ktp');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $stock_no = base64_decode($this->input->post('stock_no'));
        $tujuan = checkNULL($this->input->post('dari')).' : '.checkNULL($this->input->post('ke'));
        $kilometer_berangkat = $this->input->post('kilometer_berangkat');
        $kilometer_kepulangan = checkNULL($this->input->post('kilometer_kepulangan'));
        $keterangan = $this->input->post('keterangan');
        $nama_requester = $this->input->post('nama_requester');
        $jabatan_requester = checkNULL($this->input->post('jabatan_requester'));
        $contact_sales = $this->input->post('contact_sales');
        $status = '1';
        // $sign_req = '1';
        // $sign_1 = '0';
        // $sign_2 = '0';

        $sign_req = $this->input->post('sign_req');
        $sign_1 = $this->input->post('sign_1');
        $sign_2 = $this->input->post('sign_2');
        $sales_name = $this->input->post('sales_name');

         $data = array(  
                            'nama_konsumen' => ucwords($nama_konsumen),
                            'no_ktp' => $no_ktp,
                            'alamat' => ucwords($alamat),
                            'no_hp' => $no_hp,
                            'stock_no' => $stock_no,
                            'tujuan' => ucwords($tujuan),
                            'kilo_berangkat' => $kilometer_berangkat,
                            'kilo_pulang' => $kilometer_kepulangan,
                            'keterangan' => ucwords($keterangan),
                            'nama_requester' => ucwords($nama_requester),
                            'jabatan_requester' => ucwords($jabatan_requester),
                            'contact_sales' => $contact_sales,
                            'sign_req' => $sign_req,
                            'sign_1' => $sign_1,
                            'sign_2' => $sign_2,
                            'status' => $status,
                            'sales_name' => $sales_name
                        );

        $update = $this->M_demo_car_request->update($id_req_democar, $data);

        if($update) {
            echo 'Update';
        } else {
            echo 'Update Gagal';
        }
    }

    public function update_request_operational(){

        $id_req_democar = $this->input->post('id_req_democar');
        $nama_konsumen = $this->input->post('nama_konsumen');
        $no_ktp = $this->input->post('no_ktp');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $stock_no = base64_decode($this->input->post('stock_no'));
        $tujuan = checkNULL($this->input->post('dari')).' : '.checkNULL($this->input->post('ke'));
        $kilometer_berangkat = $this->input->post('kilometer_berangkat');
        $kilometer_kepulangan = checkNULL($this->input->post('kilometer_kepulangan'));
        $keterangan = $this->input->post('keterangan');
        $nama_requester = $this->input->post('nama_requester');
        $jabatan_requester = $this->input->post('jabatan_requester');
        $contact_sales = '-';
        $status = '1';
        // $sign_req = '1';
        // $sign_1 = '0';
        // $sign_2 = '0';

        $sign_req = $this->input->post('sign_req');
        $sign_1 = $this->input->post('sign_1');
        $sign_2 = $this->input->post('sign_2');
        $sales_name = '-';

         $data = array(  
                            'nama_konsumen' => ucwords($nama_konsumen),
                            'no_ktp' => $no_ktp,
                            'alamat' => ucwords($alamat),
                            'no_hp' => $no_hp,
                            'stock_no' => $stock_no,
                            'tujuan' => ucwords($tujuan),
                            'kilo_berangkat' => $kilometer_berangkat,
                            'kilo_pulang' => $kilometer_kepulangan,
                            'keterangan' => ucwords($keterangan),
                            'nama_requester' => ucwords($nama_requester),
                            'jabatan_requester' => ucwords($jabatan_requester),
                            'contact_sales' => $contact_sales,
                            'sign_req' => $sign_req,
                            'sign_1' => $sign_1,
                            'sign_2' => $sign_2,
                            'status' => $status,
                            'sales_name' => $sales_name
                        );

        $update = $this->M_demo_car_request->update($id_req_democar, $data);

        if($update) {
            echo 'Update';
        } else {
            echo 'Update Gagal';
        }
    }

    public function update_request_demo_operational(){

        $id_req_democar = $this->input->post('id_req_democar');
        $nama_konsumen = $this->input->post('nama_konsumen');
        $no_ktp = $this->input->post('no_ktp');
        $alamat = $this->input->post('alamat');
        $no_hp = $this->input->post('no_hp');
        $stock_no = base64_decode($this->input->post('stock_no'));
        $tujuan = checkNULL($this->input->post('dari')).' : '.checkNULL($this->input->post('ke'));
        $kilometer_berangkat = $this->input->post('kilometer_berangkat');
        $kilometer_kepulangan = checkNULL($this->input->post('kilometer_kepulangan'));
        $keterangan = $this->input->post('keterangan');
        $nama_requester = $this->input->post('nama_requester');
        $jabatan_requester = $this->input->post('jabatan_requester');
        $contact_sales = '-';
        $status = '1';
        // $sign_req = '1';
        // $sign_1 = '0';
        // $sign_2 = '0';

        $sign_req = $this->input->post('sign_req');
        $sign_1 = $this->input->post('sign_1');
        $sign_2 = $this->input->post('sign_2');
        $sales_name = '-';

         $data = array(  
                            'nama_konsumen' => ucwords($nama_konsumen),
                            'no_ktp' => $no_ktp,
                            'alamat' => ucwords($alamat),
                            'no_hp' => $no_hp,
                            'stock_no' => $stock_no,
                            'tujuan' => ucwords($tujuan),
                            'kilo_berangkat' => $kilometer_berangkat,
                            'kilo_pulang' => $kilometer_kepulangan,
                            'keterangan' => ucwords($keterangan),
                            'nama_requester' => ucwords($nama_requester),
                            'jabatan_requester' => ucwords($jabatan_requester),
                            'contact_sales' => $contact_sales,
                            'sign_req' => $sign_req,
                            'sign_1' => $sign_1,
                            'sign_2' => $sign_2,
                            'status' => $status,
                            'sales_name' => $sales_name
                        );

        $update = $this->M_demo_car_request->update($id_req_democar, $data);

        if($update) {
            echo 'Update';
        } else {
            echo 'Update Gagal';
        }
    }

    public function delete_data() {
        $id_req_democar = base64_decode($this->input->post('ID'));

        $data = array('status' => '0');

        $delete = $this->M_demo_car_request->delete($id_req_democar, $data);

        if($delete) {
            $callback = 'Delete';
        } else {
            $callback = 'Delete Gagal';
        }

        echo json_encode($callback);
    }

    function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
        $this->db->from('qv_request_demo_car');
		$this->db->where('veh_status = 1');
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
            $this->db->like("stock_no",$search);
            $this->db->or_like("name_location",$search);
            $this->db->or_like("type_name",$search);
            $this->db->or_like("vin",$search);
            $this->db->or_like("receive_date",$search);
            $this->db->or_like("veh_status",$search);
            $this->db->or_like("name_branch",$search);
            $this->db->or_like("stock_keeper",$search);
            $this->db->or_like("colour",$search);
            $this->db->or_like("upload_vin",$search);
            $this->db->or_like("engine",$search);
            $this->db->or_like("remarks",$search);
            $this->db->or_like("email_user_request",$search);
            $this->db->or_like("date_send_approval",$search);
            $this->db->where('status = 1');
		}


		/*Lanjutkan pencarian ke database*/
		//$this->db->limit($length,$start);
		//$this->db->where('status = 1');
		/*Urutkan dari alphabet paling terkahir*/
		//$this->db->order_by('id_request','ASC');
		//$query=$this->db->get('tbl_request');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
            $this->db->like("stock_no",$search);
            $jum=$this->db->get('qv_request_demo_car');
            $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		// Datatables Variables
        $order = $this->input->get("order");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }

        $columns_valid = array(
            "qv_request_demo_car.stock_no", 
            "qv_request_demo_car.name_location",
            "qv_request_demo_car.type_name",
            "qv_request_demo_car.vin",
            "qv_request_demo_car.receive_date",
            "qv_request_demo_car.veh_status",
            "qv_request_demo_car.name_branch",
            "qv_request_demo_car.stock_keeper",
            "qv_request_demo_car.colour",
            "qv_request_demo_car.upload_vin",
            "qv_request_demo_car.engine",
            "qv_request_demo_car.remarks",
            "qv_request_demo_car.email_user_request",
            "qv_request_demo_car.date_send_approval"
        );

        if(!isset($columns_valid[$col])) {
            $order = null;
        } else {
            $order = $columns_valid[$col];
        }
		$this->db->where('status = 1');
        $query = $this->M_demo_car_location->get_data($start, $length, $order, $dir);
		
		foreach ($query->result_array() as $tbl_request) {
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_request["stock_no"].' class="editRow ace" req_id_del='.$tbl_request["stock_no"].' />
           <span class="lbl"></span> ';
		   
            $output['data'][]=array($tbl_request['stock_no'],
                                    $tbl_request['name_location'], 
                                    $tbl_request['type_name'],
									$chk_idmaster);

		}

		echo json_encode($output);


    }

    public function edit_dc() {
        $id = base64_decode($this->input->get('id'));

        $data['show_view'] = 'demo_car/V_demo_car_request_edit_democar';
        $data['data_request'] = $this->M_demo_car_request->get_by_id($id)[0];

        if(!$data['data_request']) {
            redirect('demo_car/C_demo_car_request');
        } 

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        $status_stock = 'Demo';
        //$access = '0';
        if($access == '1') {
            $data['data_stock'] = $this->M_demo_car_request->getDataStock();
        } else {
            $data['data_stock'] = $this->M_demo_car_request->getDataStockByCompany($id_company, $id_branch, $status_stock);
        }

        $tujuan = explode(' : ', $data['data_request']->tujuan);
        $data['tujuan_dari'] = $tujuan[0];
        $data['tujuan_ke'] = $tujuan[1];
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);	
    }

    public function edit_opr() {
        $id = base64_decode($this->input->get('id'));

        $data['show_view'] = 'demo_car/V_demo_car_request_edit_operational';
        $data['data_request'] = $this->M_demo_car_request->get_by_id($id)[0];

        if(!$data['data_request']) {
            redirect('demo_car/C_demo_car_request');
        } 

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        $status_stock = 'Operational';
        //$access = '0';
        if($access == '1') {
            $data['data_stock'] = $this->M_demo_car_request->getDataStock();
        } else {
            $data['data_stock'] = $this->M_demo_car_request->getDataStockByCompany($id_company, $id_branch, $status_stock);
        }

        $tujuan = explode(' : ', $data['data_request']->tujuan);
        $data['tujuan_dari'] = $tujuan[0];
        $data['tujuan_ke'] = $tujuan[1];
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);	
    }

    public function edit_dcopr() {
        $id = base64_decode($this->input->get('id'));

        $data['show_view'] = 'demo_car/V_demo_car_request_edit_demo_operational';
        $data['data_request'] = $this->M_demo_car_request->get_by_id($id)[0];

        if(!$data['data_request']) {
            redirect('demo_car/C_demo_car_request');
        } 

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        $status_stock = '';
        //$access = '0';
        if($access == '1') {
            $data['data_stock'] = $this->M_demo_car_request->getDataStock();
        } else {
            $data['data_stock'] = $this->M_demo_car_request->getDataStockByCompany($id_company, $id_branch, $status_stock);
        }

        $tujuan = explode(' : ', $data['data_request']->tujuan);
        $data['tujuan_dari'] = $tujuan[0];
        $data['tujuan_ke'] = $tujuan[1];
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);	
    }

    public function id_req_democar() {
        $short_company = 'REQ';
        $short_branch = 'DC';
        $tahun = substr(date('Y'), 1, 3);
        $bulan = date('m');
        $tanggal = date('d');
        $total = $this->M_demo_car_request->get_count_id();
        $total = $total[0]->totaldata;
        $total = substr($total, -4);
        $total = $total+1;

        if($total<10) {
            $total = '000'.$total;
        } elseif($total<100) {
            $total = '00'.$total;
        } elseif($total<1000) {
            $total = '0'.$total;
        } else {
            $total = $total;
        }

        $kode = $short_company.'/'.$short_branch.'/'.$tahun.'/'.$bulan.'/'.$tanggal.'/'.$total;

        return $kode;
    }

    public function get_data_request($id=NULL) {

        if($id===NULL OR $id=='') {
            redirect('demo_car/C_demo_car_request');
        }

        $data = $this->M_demo_car_request->get_no_pol($id);

        echo json_encode($data);
    }

    public function autocomplete() {
        $query = base64_decode($this->input->get('query'));

        $hasil = $this->M_demo_car_request->getSearchDataStock($query)[0];

        echo json_encode($hasil);
    }

    public function send_approval() {
        $id_req_democar = base64_decode($this->input->get('ID'));

        $data = array('send_approval' => '1');

        $data_request = $this->M_demo_car_request->get_by_id($id_req_democar)[0];
        
        $kirim_email = $this->kirim_email_ke_sign1($data_request);

            if($kirim_email == 'ok') {
                $approval = $this->M_demo_car_request->update($id_req_democar, $data);
                $callback = array(  'status_email' => 'ok',
                                    'status_approval' => 'Approval',
                                    'error_email' => null
                                );
            } else {
                $callback = array(  'status_email' => 'fail',
                                    'status_approval' => 'Approval Gagal',
                                    'error_email' => $kirim_email
                                );
            }

        echo json_encode($callback);
    }

    public function kirim_email_ke_sign1($data_request){        
        //echo '<pre>';print_r($this->session->userdata());die;
        $this->load->library('email');  
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html';
        $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
        $config['smtp_pass']= "3mim@zd4!";
        $config['validation'] = TRUE;     

        $this->email->initialize($config);                                                                               

        $sender_email = "helpdesk@eurokars.co.id";
		$sender_name =  "Democar Request";                                              
	
		$this->email->from($sender_email, $sender_name);

        $user_head = $this->session->userdata('email_head');
        $user_email = $this->session->userdata('email');
        //$user_test = 'Bernand.Hermawan@eurokars.co.id';
        //$user_head = 'Brian.Yunanda@eurokars.co.id';
        //$user_email = 'Bernand.Hermawan@eurokars.co.id';

        //end-----------------------------------------

        $to_email = $user_email.','.$user_head;

        $this->email->to($to_email);
        $this->email->subject('Request Approval Democar Step 1 (Sign 1)');
        $data['data_request'] = $data_request;
        $data['status_approval'] = 'Waiting Approval From Sign 1';
        $email_body = $this->load->view('demo_car/V_content_email',$data,true);
        //return $email_body;die;
        
       
        $this->email->message($email_body);
        
        if($this->email->send()){

            return 'ok';

        } else {
            return $this->email->print_debugger();
            //print_r($this->email->print_debugger());
        }


    }

    public function kirim_email_ke_sign2($status, $data_request){        
        //echo '<pre>';print_r($this->session->userdata());die;
        $this->load->library('email');  
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html';
        $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
        $config['smtp_pass']= "3mim@zd4!";
        $config['validation'] = TRUE;     

        $this->email->initialize($config);                                                                               

        $sender_email = "helpdesk@eurokars.co.id";
		$sender_name =  "Democar Request";                                              
	
		$this->email->from($sender_email, $sender_name);

        $user_head = $this->session->userdata('email_head');
        $user_email = $this->session->userdata('email');
        //$user_test = 'Bernand.Hermawan@eurokars.co.id';
        //$user_head = 'Brian.Yunanda@eurokars.co.id';
        //$user_email = 'Bernand.Hermawan@eurokars.co.id';

        //end-----------------------------------------

        $to_email = $user_email.','.$user_head;

        $this->email->to($to_email);
        $this->email->subject('Demo Car Approval Step 2 (Sign 2)');
        $data['data_request'] = $data_request;
        

       // $email_body = $this->load->view('book_meeting_room/V_content_email_book',$data_email,true);
        if($status == 'Approved') {
            $data['status_approval'] = 'Approved by Sign 1, Waiting Approval from Sign 2';
        } else {
            $data['status_approval'] = 'Rejected by Sign 1';
        }
        
        $email_body = $this->load->view('demo_car/V_content_email',$data,true);
        //return $email_body;die;
        $this->email->message($email_body);
        
        if($this->email->send()){

            return 'ok';

        } else {
            return $this->email->print_debugger();
            //print_r($this->email->print_debugger());
        }


    }

    public function kirim_email_final_result($status, $data_request){        
        //echo '<pre>';print_r($this->session->userdata());die;
        $this->load->library('email');  
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html';
        $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
        $config['smtp_pass']= "3mim@zd4!";
        $config['validation'] = TRUE;     

        $this->email->initialize($config);                                                                               

        $sender_email = "helpdesk@eurokars.co.id";
        //$sender_name =  "Democar Request"; 
        $sender_name = "Helpdesk";                                             
	
		$this->email->from($sender_email, $sender_name);

        $user_head = $this->session->userdata('email_head');
        $user_email = $this->session->userdata('email');
        $submission_email = $data_request->submission_email;
        //$user_test = 'Bernand.Hermawan@eurokars.co.id';
        //$user_head = 'Brian.Yunanda@eurokars.co.id';
        //$user_email = 'Bernand.Hermawan@eurokars.co.id';

        //end-----------------------------------------

        $to_email = $user_email.','.$user_head.','.$submission_email;

        $this->email->to($to_email);
        $this->email->subject('Demo Car Approval Result');
        $data['data_request'] = $data_request;
        

       // $email_body = $this->load->view('book_meeting_room/V_content_email_book',$data_email,true);
        if($status == 'Approved') {
            $data['status_approval'] = 'Approved by Sign 1 and Sign 2';
        } else {
            $data['status_approval'] = 'Rejected by Sign 2';
        }
        
        $email_body = $this->load->view('demo_car/V_content_email',$data,true);
        //return $email_body;die;
        $this->email->message($email_body);
        
        if($this->email->send()){

            return 'ok';

        } else {
            return $this->email->print_debugger();
            //print_r($this->email->print_debugger());
        }


    }
    
    public function pdf() {
        $this->load->library('Pdf');

        $id = base64_decode($this->input->get('id'));

        if($id===NULL || $id=='') {
            redirect('demo_car/C_demo_car_request');
        }

        $data = $this->M_demo_car_request->get_by_id($id)[0];

        //echo '<pre>';print_r($data);

        if($data->id_req_democar_group===NULL || $data->id_req_democar_group=='') {
            redirect('demo_car/C_demo_car_request');
        }

        if($data->flag_print != '0') {
            echo '<h3> <a href="'.base_url('demo_car/C_demo_car_request').'"> This Data already Printed!, please go back here</a> </h3>';die;
        }

        // Update flag_print req demo car
        $data_update = array(  'flag_print' => '1'
                    );

        $update = $this->M_demo_car_request->update($data->id_req_democar_group, $data_update);

        $tujuan = $tujuan = explode(' : ', $data->tujuan);
        $tujuan_dari = $tujuan[0];
        $tujuan_ke = $tujuan[1];

        if($data->sign_1 == '1' || $data->sign_2 == '1') {
            //$disetujui = 'DISETUJUI / <font class="disetujui">TIDAK DISETUJUI</font>';
            $disetujui = 'DISETUJUI';
        } else {
            //$disetujui = '<font class="disetujui">DISETUJUI</font> / TIDAK DISETUJUI';
            $disetujui = 'TIDAK DISETUJUI';
        }

        if($data->flag_status_req == 1) {
           // $democar = 'UNIT DEMOCAR / <font class="disetujui">KENDARAAN OPERASIONAL PERUSAHAAN</font>';
            $democar = 'UNIT DEMOCAR';
        } elseif($data->flag_status_req == 2) { 
            //$democar = '<font class="disetujui">UNIT DEMOCAR</font> / KENDARAAN OPERASIONAL PERUSAHAAN';
            $democar = 'KENDARAAN OPERASIONAL PERUSAHAAN';
        } elseif($data->flag_status_req == 3) {
            //$democar = '<font class="disetujui">UNIT DEMOCAR</font> / KENDARAAN OPERASIONAL PERUSAHAAN';
            $democar = 'DEMOCAR UNTUK OPERASIONAL';
        } else {
            $democar = '';
        }

        if($data->sign_req == '1'){
            $sign_req = '&#10004;';
        } else {
            $sign_req = '&#10006;';
        }

        if($data->sign_1 == '1'){
            $sign_1 = '	&#10004;';
        } else {
            $sign_1 = '&#10006;';
        }

        if($data->sign_2 == '1'){
            $sign_2 = '	&#10004;';
        } else {
            $sign_2 = '&#10006;';
        }

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4', true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator('IT @Eurokars Group Indonesia');
        $pdf->SetAuthor('IT @Eurokars Group Indonesia');
        $pdf->SetTitle("Demo Car Request - ".$data->id_req_democar_group);
        $pdf->SetSubject("Demo Car Request - ".$data->id_req_democar_group);

         // set header and footer fonts
         $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
         $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
 
         // set default monospaced font
         $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 
         // set margins
         $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
         $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
         $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
 
         // set auto page breaks
         $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 
         $pdf->setPrintHeader(false);
         //$pdf->setPrintFooter(false);
 
         // set image scale factor
 
 
         // set some language-dependent strings (optional)
         if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
             require_once(dirname(__FILE__).'/lang/eng.php');
             $pdf->setLanguageArray($l);
         }
                 $pdf->SetFont('dejavusans', '', 9);

                 $pdf->SetPrintHeader(false);
                 $pdf->SetPrintFooter(false);
 
         // add a page
         $pdf->AddPage();

         $html = '
        
         <!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>

	<style type="text/css">
		.center {
			text-align: center;
		}

		.judul {
			text-align: center;
			font-weight: bold;
			font-size: 10px;
		}

        .garis_bawah {
            border-bottom: 1px solid black;
        }

        .disetujui { 
            text-decoration: line-through;
        }

	</style>

</head>

<body>
    
    <table width="100%" border="0">
		<tr>
            
            <td width="90%"> <img src="'.base_url('asset/images/euro.jpg').'" height="20"/> </td>
            <td rowspan="3" width="10%"> </td>
		</tr>
	</table>
	<table width="100%" border="0">
		<tr>
            
            <td width="90%"> PT. '.$data->company.' </td>
            <td rowspan="3" width="10%"> </td>
		</tr>

		<tr>
			<td> '.$data->address.'</td>
			
		</tr>
		<tr>
			<td> Telp. '.$data->phone.'</td>

		</tr>
	</table>

        
    <br>
    <br>
    <br>

	<table width="100%" border="0" class="center">
		<tr>
			<td width="5%"></td>
			<td width="90%" class="judul">

				Formulir Permintaan Persetujuan
				<p>
					Penggunaan Unit Democar & Kendaraan Operasional Perusahaan
				</p>

			</td>

			<td width="5%"></td>
		</tr>
	</table>

    <br>
    <br>

    <table width="100%" border="0">
        <tr>
            <td width="5%"></td>
            <td width="75%">Yang bertanda tangan dibawah ini menerangkan, bahwa *) :</td>
            <td width="10%"></td>
            <td width="10%"></td>
        </tr>
    </table>

    <br>
    <br>
    
    <table width="100%" border="0" cellpadding="1.5">
		<tr>
			<td width="10%"></td>
			<td width="33%">Nama Konsumen</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->nama_konsumen.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">No KTP / ID</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->no_ktp.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">Alamat</td>
            <td width="2%"> : </td>
            <td width="50%" rowspan="3" class="garis_bawah" style="word-wrap: break-word">'.$data->alamat.'</td>
            <td width="5%"></td>
        </tr>

        <tr>
			<td width="10%"></td>
			<td width="33%"></td>
            <td width="2%">  </td>
            <td width="5%"></td>
        </tr>
        
         <tr>
			<td width="10%"></td>
			<td width="33%"></td>
            <td width="2%">  </td>
            <td width="5%"></td>
		</tr>
        
        
        <tr>
			<td width="10%"></td>
			<td width="33%">No. Telepon / HP</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->no_hp.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">Jenis Kendaraan</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->jenis_kendaraan.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">Type Kendaraan</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->type_name.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">No. Polisi Kendaraan</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->no_polisi.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">No. Rangka Kendaraan</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->vin.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">Tujuan</td>
            <td width="2%"> : </td>
            <td width="25%" class="garis_bawah">Dari : '.$tujuan_dari.'</td>
            <td width="25%" class="garis_bawah">Ke : '.$tujuan_ke.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">Kilometer Keberangkatan</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->kilo_berangkat.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">Kilometer Kepulangan</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->kilo_pulang.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">Keterangan</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->keterangan.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">Nama Requester</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->nama_requester.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">Jabatan Requester</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->jabatan_requester.'</td>
            <td width="5%"></td>
		</tr>

        <tr>
			<td width="10%"></td>
			<td width="33%">Contact Sales / Employee</td>
            <td width="2%"> : </td>
			<td width="50%" class="garis_bawah">'.$data->contact_sales.'</td>
            <td width="5%"></td>
		</tr>
	</table>

    <br>
    <br>

	<table width="100%" border="0" class="judul center">
		<tr>
			<td width="5%"></td>
			<td width="90%">

                '.$disetujui.' ** )
				<p>
					Menggunakan
				</p>

                <p>
					'.$democar.' ** )
				</p>

			</td>

			<td width="5%"></td>
		</tr>
	</table>

    <br>
    <br>

    <table width="100%" border="0">
		<tr>
			<td width="10%"></td>
			<td width="50%">Jakarta, '.date('d/m/Y').' <br></td>
            <td width="30%"></td>
			<td width="10%"></td>
		</tr>
	</table>

    

    <table width="100%" border="0" class="center">
        <tr>
            <th width="10%" rowspan="7"></th>
            <th width="27%" rowspan="7" border="1">Diajukan / Requester 
            <br>
            <br>
            <br>

    
            <font size="8px">Signed</font>
            

            <br>
            <br>
            <br>
            '.$data->nama_requester.'
            
            </th>
            <th width="28%" rowspan="7" border="1" style="border-top: none; vertical-align: middle;  display: inline-block;">Menyetujui
            <br>
            <br>
            <br>

            <font size="8px">Signed</font>

            <br>
            <br>
            <br>
            Natasia Anggraini *** )

            </th>
            <th width="30%" rowspan="7" border="1" style="border-top: none; vertical-align: middle;  display: inline-block;">Menyetujui
            <br>
            <br>
            <br>

            <font size="8px">Signed</font>

            <br>
            <br>
            <br>
            Eko Pramudito / Fastwin A. Sutojo *** )
            </th>
            <th width="5%"> </th>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

         <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

         <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
 
    </table>    
    
    <br>
    <br>

    <table width="100%" border="0">
    <tr>
        <td width="10%"></td>
        <td width="30%">Ket: *) Diisi Oleh Requester</td>
        <td width="25%">**) Coret yang tidak perlu </td>
        <td width="30%">***) Mengikuti Tabel keberadaan PT </td>
        <td width="5%"> </td>
    </tr>

</table> 


</body>

</html>

                 
         ';

         // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        $file = 'Request_Demo_Car - '.$data->id_req_democar_group.'.pdf';

        // reset pointer to the last page
        $pdf->lastPage();

        $pdf->Output($file, 'I');

    }

    public function ajax_list_approval1()
	{
		$list = $this->M_demo_car_request->get_datatables();
		$data = array();
        $no = $_POST['start'];
        
		foreach ($list as $tbl_request) {
            if($this->session->userdata('approval_democar') == '1') {
                if($tbl_request->send_approval == '1' && $tbl_request->sign_1 == '0' && $tbl_request->sign_2 == '0') {
                $chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" data-upload='.$tbl_request->upload_vin.' data-approval='.$tbl_request->send_approval.' value='.base64_encode($tbl_request->id_req_democar_group).' class="editRow ace" />
           <span class="lbl"></span> ';
            $no++;
            $url_print = base_url("demo_car/C_demo_car_request/pdf?id=").base64_encode($tbl_request->id_req_democar_group);
            //$url_upload = base_url("asset/upload_demo_car/$tbl_request->upload_vin");
            if($tbl_request->sign_req == '0') {
                $sign_req = '<i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true"></i>';
            } else {
                $sign_req = '<i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true"></i>';
            }

            // 0 = Belum di approve
            // 1 = Approved
            // 2 = Rejected

            if($tbl_request->sign_1 == '0') {
                $sign_1 = '<i class="fa fa-question-circle fa-2x text-danger" aria-hidden="true" title="Approval not yet Received"></i>';
            } elseif($tbl_request->sign_1 == '1') {
                $sign_1 = '<i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true" title="Approved"></i>';
            } else {
                $sign_1 = '<i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true" title="Rejected"></i>';
            }

            // 0 = Belum di approve
            // 1 = Approved
            // 2 = Rejected

            if($tbl_request->sign_2 == '0') {
                $sign_2 = '<i class="fa fa-question-circle fa-2x text-danger" aria-hidden="true" title="Approval not yet Received"></i>';
            } elseif($tbl_request->sign_2 == '1') {
                $sign_2 = '<i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true" title="Approved"></i>';
            } else {
                $sign_2 = '<i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true" title="Rejected"></i>';
            }

            // 0 = Belum dikirim
            // 1 = Sudah dikirim

            if($tbl_request->send_approval == '0') {
                $send_approval = '<i class="fa fa-question-circle fa-2x" aria-hidden="true" title="Approved no yet Sent"></i>';
            } elseif($tbl_request->send_approval == '1' || $tbl_request->send_approval == '2') {
                if($tbl_request->sign_1 == '1' && $tbl_request->sign_2 == '1') {
                    $send_approval = '<a target="_blank" href="'.$url_print.'"><i class="fa fa-print fa-2x text-success" aria-hidden="true" title="Print PDF"></i></a>';
                } else {
                    $send_approval = '<i class="fa fa-envelope fa-2x text-info" aria-hidden="true" title="Approval Sent, and Being Checked by Sign 1 or Sign 2"></i>';
                }
                
            }

            $id_req_democar_group = "<span class='label label-primary arrowed-in-right arrowed'>$tbl_request->id_req_democar_group</span>";

            $stock_no = "<span class='label label-primary arrowed-in-right arrowed'>$tbl_request->stock_no</span>";
            $jenis_kendaraan = "<span class='label label-info arrowed-in arrowed-in-right'>$tbl_request->jenis_kendaraan</span>";
            $type_name = "<span class='label label-info arrowed-in arrowed-in-right'>$tbl_request->type_name</span>";
            

			$row = array();
			$row[] = $chk_idmaster;
            $row[] = $no;                       // No
            $row[] = $sign_req;
            $row[] = $sign_1;
            $row[] = $sign_2;
            $row[] = $send_approval;
            $row[] = $id_req_democar_group;      // ID Model
            $row[] = $tbl_request->nama_requester;
            $row[] = $tbl_request->jabatan_requester;
            $row[] = $tbl_request->sales_name;
            $row[] = $tbl_request->contact_sales;
			$row[] = $tbl_request->nama_konsumen;      // ID Model
            $row[] = $tbl_request->no_ktp;    // Brand Name
            $row[] = $tbl_request->alamat;    // Model Name
            $row[] = $tbl_request->no_hp;
            $row[] = $stock_no;
            $row[] = $jenis_kendaraan;
            $row[] = $type_name;
            $row[] = $tbl_request->no_polisi;
            $row[] = $tbl_request->vin;         // rangka
            $row[] = $tbl_request->name_location;
            $row[] = $tbl_request->company;
            $row[] = $tbl_request->name_branch;
            $row[] = $tbl_request->tujuan;
            //$row[] = "<a target='_blank' href=".$url.">Lihat</a>";
            $row[] = $tbl_request->kilo_berangkat;
            $row[] = $tbl_request->kilo_pulang;
            $row[] = $tbl_request->keterangan;
            
			$data[] = $row;
                }
            }
			
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_demo_car_request->count_all(),
						"recordsFiltered" => $this->M_demo_car_request->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
    }

    public function ajax_list_approval2()
	{
		$list = $this->M_demo_car_request->get_datatables();
		$data = array();
        $no = $_POST['start'];
        
		foreach ($list as $tbl_request) {
            if($this->session->userdata('approval_democar') == '2') {
                if($tbl_request->send_approval == '1' && $tbl_request->sign_1 == '1' && $tbl_request->sign_2 == '0') {
                $chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" data-upload='.$tbl_request->upload_vin.' data-approval='.$tbl_request->send_approval.' value='.base64_encode($tbl_request->id_req_democar_group).' class="editRow ace" />
           <span class="lbl"></span> ';
            $no++;
            $url_print = base_url("demo_car/C_demo_car_request/pdf?id=").base64_encode($tbl_request->id_req_democar_group);
            //$url_upload = base_url("asset/upload_demo_car/$tbl_request->upload_vin");
            if($tbl_request->sign_req == '0') {
                $sign_req = '<i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true"></i>';
            } else {
                $sign_req = '<i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true"></i>';
            }

            // 0 = Belum di approve
            // 1 = Approved
            // 2 = Rejected

            if($tbl_request->sign_1 == '0') {
                $sign_1 = '<i class="fa fa-question-circle fa-2x text-danger" aria-hidden="true" title="Approval not yet Received"></i>';
            } elseif($tbl_request->sign_1 == '1') {
                $sign_1 = '<i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true" title="Approved"></i>';
            } else {
                $sign_1 = '<i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true" title="Rejected"></i>';
            }

            // 0 = Belum di approve
            // 1 = Approved
            // 2 = Rejected

            if($tbl_request->sign_2 == '0') {
                $sign_2 = '<i class="fa fa-question-circle fa-2x text-danger" aria-hidden="true" title="Approval not yet Received"></i>';
            } elseif($tbl_request->sign_2 == '1') {
                $sign_2 = '<i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true" title="Approved"></i>';
            } else {
                $sign_2 = '<i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true" title="Rejected"></i>';
            }

            // 0 = Belum dikirim
            // 1 = Sudah dikirim

            if($tbl_request->send_approval == '0') {
                $send_approval = '<i class="fa fa-question-circle fa-2x" aria-hidden="true" title="Approved no yet Sent"></i>';
            } elseif($tbl_request->send_approval == '1' || $tbl_request->send_approval == '2') {
                if($tbl_request->sign_1 == '1' && $tbl_request->sign_2 == '1') {
                    $send_approval = '<a target="_blank" href="'.$url_print.'"><i class="fa fa-print fa-2x text-success" aria-hidden="true" title="Print PDF"></i></a>';
                } else {
                    $send_approval = '<i class="fa fa-envelope fa-2x text-info" aria-hidden="true" title="Approval Sent, and Being Checked by Sign 1 or Sign 2"></i>';
                }
                
            }

            $id_req_democar_group = "<span class='label label-primary arrowed-in-right arrowed'>$tbl_request->id_req_democar_group</span>";

            $stock_no = "<span class='label label-primary arrowed-in-right arrowed'>$tbl_request->stock_no</span>";
            $jenis_kendaraan = "<span class='label label-info arrowed-in arrowed-in-right'>$tbl_request->jenis_kendaraan</span>";
            $type_name = "<span class='label label-info arrowed-in arrowed-in-right'>$tbl_request->type_name</span>";
            

			$row = array();
			$row[] = $chk_idmaster;
            $row[] = $no;                       // No
            $row[] = $sign_req;
            $row[] = $sign_1;
            $row[] = $sign_2;
            $row[] = $send_approval;
            $row[] = $id_req_democar_group;      // ID Model
            $row[] = $tbl_request->nama_requester;
            $row[] = $tbl_request->jabatan_requester;
            $row[] = $tbl_request->sales_name;
            $row[] = $tbl_request->contact_sales;
			$row[] = $tbl_request->nama_konsumen;      // ID Model
            $row[] = $tbl_request->no_ktp;    // Brand Name
            $row[] = $tbl_request->alamat;    // Model Name
            $row[] = $tbl_request->no_hp;
            $row[] = $stock_no;
            $row[] = $jenis_kendaraan;
            $row[] = $type_name;
            $row[] = $tbl_request->no_polisi;
            $row[] = $tbl_request->vin;         // rangka
            $row[] = $tbl_request->name_location;
            $row[] = $tbl_request->company;
            $row[] = $tbl_request->name_branch;
            $row[] = $tbl_request->tujuan;
            //$row[] = "<a target='_blank' href=".$url.">Lihat</a>";
            $row[] = $tbl_request->kilo_berangkat;
            $row[] = $tbl_request->kilo_pulang;
            $row[] = $tbl_request->keterangan;
            
			$data[] = $row;
                }
            
         }

			
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_demo_car_request->count_all(),
						"recordsFiltered" => $this->M_demo_car_request->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
    }

    public function view_approval() {
        $data['show_view'] = 'demo_car/V_demo_car_request_approval';
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function approved1() { // Sign 1 Approve
        $id_req_democar = base64_decode($this->input->get('ID'));

        $data = array(  'sign_1' => '1'
                    );

        $data_request = $this->M_demo_car_request->get_by_id($id_req_democar)[0];

        $kirim_email = $this->kirim_email_ke_sign2('Approved', $data_request);

            if($kirim_email == 'ok') {
                $approval = $this->M_demo_car_request->update($id_req_democar, $data);
                $callback = array(  'status_email' => 'ok',
                                    'status_approval' => 'Approved',
                                    'error_email' => null
                                );
            } else {
                $callback = array(  'status_email' => 'fail',
                                    'status_approval' => 'Approved Gagal',
                                    'error_email' => $kirim_email
                                );
            }

        echo json_encode($callback);
    }

    public function approved2() { // Sign 2 Approve
        $id_req_democar = base64_decode($this->input->get('ID'));

        $data = array(  'sign_2' => '1',
                        'send_approval' => '2'
                    );

        $data_request = $this->M_demo_car_request->get_by_id($id_req_democar)[0];

        $kirim_email = $this->kirim_email_final_result('Approved', $data_request);
        
            if($kirim_email == 'ok') {
                $approval = $this->M_demo_car_request->update($id_req_democar, $data);
                $callback = array(  'status_email' => 'ok',
                                    'status_approval' => 'Approved',
                                    'error_email' => null
                                );
            } else {
                $callback = array(  'status_email' => 'fail',
                                    'status_approval' => 'Approved Gagal',
                                    'error_email' => $kirim_email
                                );
            }

        echo json_encode($callback);
    }

    public function rejected1() {
        $id_req_democar = base64_decode($this->input->get('ID'));

        $data = array(  'sign_1' => '2',
                        'send_approval' => '2'
                    );
        
        $data_request = $this->M_demo_car_request->get_by_id($id_req_democar)[0];

        $kirim_email = $this->kirim_email_ke_sign2('Rejected', $data_request);

            if($kirim_email == 'ok') {
                $approval = $this->M_demo_car_request->update($id_req_democar, $data);
                $callback = array(  'status_email' => 'ok',
                                    'status_approval' => 'Rejected',
                                    'error_email' => null
                                );
            } else {
                $callback = array(  'status_email' => 'fail',
                                    'status_approval' => 'Rejected Gagal',
                                    'error_email' => $kirim_email
                                );
            }

        echo json_encode($callback);
    }

    public function rejected2() {
        $id_req_democar = base64_decode($this->input->get('ID'));

        $data = array(  
                        'sign_2' => '2',
                        'send_approval' => '2'
                    );
        
        $data_request = $this->M_demo_car_request->get_by_id($id_req_democar)[0];

        $kirim_email = $this->kirim_email_final_result('Rejected', $data_request);

            if($kirim_email == 'ok') {
                $approval = $this->M_demo_car_request->update($id_req_democar, $data);
                $callback = array(  'status_email' => 'ok',
                                    'status_approval' => 'Rejected',
                                    'error_email' => null
                                );
            } else {
                $callback = array(  'status_email' => 'fail',
                                    'status_approval' => 'Rejected Gagal',
                                    'error_email' => $kirim_email
                                );
            }

        echo json_encode($callback);
    }

    public function reset_flag_print() {
        $id_req_democar = base64_decode($this->input->get('ID'));

        $data = array(  
            'flag_print' => '0'
        );

        $reset = $this->M_demo_car_request->update($id_req_democar, $data);

        if($reset) {
            echo 'ok';
        } else {
            echo 'fail';
        }
    }

}