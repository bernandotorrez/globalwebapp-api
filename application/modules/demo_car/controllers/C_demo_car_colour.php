<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_demo_car_colour extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_demo_car_colour');
    }

    public function index() {
        $data['show_view'] = 'demo_car/V_demo_car_colour';

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        //$id_company = '1';
        $id_branch = $this->session->userdata('branch_id');
        //$access = '0';
        if($access == '1') {
            //data['data_type'] = $this->M_demo_car_colour->getDataType();
            $data['data_brand'] = $this->M_demo_car_colour->getDataBrand();
        } else {
            //$data['data_type'] = $this->M_demo_car_colour->getDataTypeByCompany($id_company);
            $data['data_brand'] = $this->M_demo_car_colour->getDataBrandByCompany($id_company);
        }

        

        $data['total'] = $this->M_demo_car_colour->get_count_id();
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function ajax_list()
	{
		$list = $this->M_demo_car_colour->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_colour) {
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_colour->id_colour_group.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;                       // No
			$row[] = $tbl_colour->id_colour_group;      // ID Model
            $row[] = $tbl_colour->colour;    // Brand Name
            $row[] = $tbl_colour->type_name;    // Model Name
            $row[] = $tbl_colour->model_name; 
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_demo_car_colour->count_all(),
						"recordsFiltered" => $this->M_demo_car_colour->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
    }

    function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
        $this->db->from('qv_colour_group');
		$this->db->where('status = 1');
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
            $this->db->like("id_colour",$search);
            $this->db->or_like("model_name",$search);
            $this->db->or_like("type_name",$search);
            $this->db->where('status = 1');
		}


		/*Lanjutkan pencarian ke database*/
		//$this->db->limit($length,$start);
		//$this->db->where('status = 1');
		/*Urutkan dari alphabet paling terkahir*/
		//$this->db->order_by('id_colour','ASC');
		//$query=$this->db->get('tbl_colour');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
            $this->db->like("id_colour",$search);
            $jum=$this->db->get('qv_colour_group');
            $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		// Datatables Variables
        $order = $this->input->get("order");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }

        $columns_valid = array(
            "qv_colour_group.id_colour", 
            "qv_colour_group.model_name",
            "qv_colour_group.type_name"
        );

        if(!isset($columns_valid[$col])) {
            $order = null;
        } else {
            $order = $columns_valid[$col];
        }
		$this->db->where('status = 1');
        $query = $this->M_demo_car_colour->get_data($start, $length, $order, $dir);
		
		foreach ($query->result_array() as $tbl_colour) {
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_colour["id_colour"].' class="editRow ace" req_id_del='.$tbl_colour["id_colour"].' />
           <span class="lbl"></span> ';
		   
			$output['data'][]=array($tbl_colour['id_colour'],$tbl_colour['model_name'], $tbl_colour['type_name'],
									$chk_idmaster);

		}

		echo json_encode($output);


	}
    
    public function ajax_edit($id)
    {
        $data = $this->M_demo_car_colour->get_by_id($id);
        
        // Model
        $model = $this->M_demo_car_colour->getDataModelByID($data[0]->id_brand);

        $html_model = '<label for="model_name">Model</label>
        <select class="form-control" name="model" id="model" onChange="getDataType(this.value)">
        <option value=""> - Pilih Model - </option>
        ';

        foreach($model as $value_model) {
            $html_model .= '<option value="'.$value_model->id_model.'"> '.$value_model->model_name.' </option>';
        }       

        $html_model .= '</select>';

        // Type
        $type = $this->M_demo_car_colour->getDataTypeByID($data[0]->id_model);

        $html_type = '<label for="model_name">Type</label>
        <select class="form-control" name="type" id="type">
        <option value=""> - Pilih Type - </option>
        ';

        foreach($type as $value_type) {
            $html_type .= '<option value="'.$value_type->id_type.'"> '.$value_type->type_name.' </option>';
        }       

        $html_type .= '</select>';

        $callback = array('data' => $data,
                            'html_model' => $html_model,
                            'html_type' => $html_type
        );

        echo json_encode($callback);
    }

    public function add_colour(){
        
        $type_name = $this->input->post('type');
        $colour = $this->input->post('colour');
        $status = '1';

        $data = array(  'id_type' => $type_name,
                        'colour' => ucwords($colour),
                        'status' => $status
                    );

        $insert = $this->M_demo_car_colour->insert($data);
        
        if($insert) {
            echo 'Insert';
        } else {
            echo 'Insert Gagal';
        }

    }

    public function update_colour(){
        $id_colour = $this->input->post('id_colour');
        $type_name = $this->input->post('type');
        $colour = $this->input->post('colour');
        $status = '1';

        $data = array(  'id_type' => $type_name,
                        'colour' => ucwords($colour),
                        'status' => $status
                    );

        $update = $this->M_demo_car_colour->update($id_colour, $data);
        
        if($update) {
            echo 'Update';
        } else {
            echo 'Update Gagal';
        }

    }

    public function delete_data() {
        $id_colour = $this->input->post('ID');

        $data_id_array = explode(",", $id_colour);
        if(!empty($data_id_array)) {
            foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);
				$delete = $this->M_demo_car_colour->delete($id, $data);
			}
        } 

        echo json_encode('Delete');
    }

    public function coba() {
        $data['show_view'] = 'demo_car/V_demo_car_colour1';
        $data['data_model'] = $this->M_demo_car_colour->getDataModel();
        $data['data_colour'] = $this->M_demo_car_colour->getDataType();
        $data['total'] = $this->M_demo_car_colour->get_count_id();
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function ajax_model() {
        $id_brand = $this->input->get('brand');

        $data = $this->M_demo_car_colour->getDataModelByID($id_brand);

        $html = '<label for="model_name">Model</label>
        <select class="form-control" name="model" id="model" onChange="getDataType(this.value)">
        <option value=""> - Pilih Model - </option>
        ';

        foreach($data as $value) {
            $html .= '<option value="'.$value->id_model.'"> '.$value->model_name.' </option>';
        }       

        $html .= '</select>';

        echo $html;
        
    }

    public function ajax_type() {
        $id_model = $this->input->get('model');

        $data = $this->M_demo_car_colour->getDataTypeByID($id_model);

        $html = '<label for="model_name">Type</label>
        <select class="form-control" name="type" id="type">
        <option value=""> - Pilih Type - </option>
        ';

        foreach($data as $value) {
            $html .= '<option value="'.$value->id_type.'"> '.$value->type_name.' </option>';
        }       

        $html .= '</select>';

        echo $html;
        
    }

}