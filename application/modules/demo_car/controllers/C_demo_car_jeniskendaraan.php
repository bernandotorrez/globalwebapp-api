<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_demo_car_jeniskendaraan extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_demo_car_jeniskendaraan');
    }

    public function index() {
        $data['show_view'] = 'demo_car/V_demo_car_jeniskendaraan';
        $data['total'] = $this->M_demo_car_jeniskendaraan->get_count_id();
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function add_jeniskendaraan(){
        $jenis_kendaraan = $this->input->post('jenis_kendaraan');
        $status = '1';

        $data = array(  'jenis_kendaraan' => ucwords($jenis_kendaraan),
                        'status' => $status
                    );

        $insert = $this->M_demo_car_jeniskendaraan->insert($data);
        
        if($insert) {
            echo 'Insert';
        } else {
            echo 'Insert Gagal';
        }

    }

    public function update_jeniskendaraan(){
        $id_kind_vehicle = $this->input->post('id_kind_vehicle');
        $jenis_kendaraan = $this->input->post('jenis_kendaraan');
        $status = '1';

        $data = array(  'jenis_kendaraan' => ucwords($jenis_kendaraan),
                        'status' => $status
                    );

        $update = $this->M_demo_car_jeniskendaraan->update($id_kind_vehicle, $data);
        
        if($update) {
            echo 'Update';
        } else {
            echo 'Update Gagal';
        }

    }

    public function delete_data() {
		
        $id_kind_vehicle = $this->input->post('ID');

        $data_id_array = explode(",", $id_kind_vehicle);
        if(!empty($data_id_array)) {
            foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);
				$delete = $this->M_demo_car_jeniskendaraan->delete($id, $data);
			}
        } 

        echo json_encode('Delete');
    }

    public function ajax_edit($id)
		{
			$data = $this->M_demo_car_jeniskendaraan->get_by_id($id);
			echo json_encode($data);
		}

    public function ajax_list()
	{
		$list = $this->M_demo_car_jeniskendaraan->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_jeniskendaraan) {
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_jeniskendaraan->id_kind_vehicle.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;                       // No
			$row[] = $tbl_jeniskendaraan->id_kind_vehicle;      // ID Model
            $row[] = $tbl_jeniskendaraan->jenis_kendaraan;    // Brand Name
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_demo_car_jeniskendaraan->count_all(),
						"recordsFiltered" => $this->M_demo_car_jeniskendaraan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

    function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
        $this->db->from('tbl_kind_vehicle');
		$this->db->where('status = 1');
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
            $this->db->like("id_kind_vehicle",$search);
            $this->db->or_like("jenis_kendaraan",$search);
            $this->db->where('status = 1');
		}


		/*Lanjutkan pencarian ke database*/
		//$this->db->limit($length,$start);
		//$this->db->where('status = 1');
		/*Urutkan dari alphabet paling terkahir*/
		//$this->db->order_by('id_jeniskendaraan','ASC');
		//$query=$this->db->get('tbl_jeniskendaraan');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
            $this->db->like("id_kind_vehicle",$search);
            $jum=$this->db->get('tbl_kind_vehicle');
            $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		// Datatables Variables
        $order = $this->input->get("order");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }

        $columns_valid = array(
            "tbl_kind_vehicle.id_kind_vehicle", 
            "tbl_kind_vehicle.jenis_kendaraan",
        );

        if(!isset($columns_valid[$col])) {
            $order = null;
        } else {
            $order = $columns_valid[$col];
        }
		$this->db->where('status = 1');
        $query = $this->M_demo_car_jeniskendaraan->get_data($start, $length, $order, $dir);
		
		foreach ($query->result_array() as $tbl_jeniskendaraan) {
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_jeniskendaraan["id_kind_vehicle"].' class="editRow ace" req_id_del='.$tbl_jeniskendaraan["id_kind_vehicle"].' />
           <span class="lbl"></span> ';
		   
			$output['data'][]=array($tbl_jeniskendaraan['id_kind_vehicle'],$tbl_jeniskendaraan['jenis_kendaraan'],
									$chk_idmaster);

		}

		echo json_encode($output);


	}

}