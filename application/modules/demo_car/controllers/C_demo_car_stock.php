<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_demo_car_stock extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_demo_car_stock');
        $this->load->helper('xss_filter_helper');
    }

    public function index() {
        //echo '<pre>';print_r($this->session->userdata());die;
        $data['show_view'] = 'demo_car/V_demo_car_stock';
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function add() {

        $data['stock_no'] = $this->stock_no();
        $data['show_view'] = 'demo_car/V_demo_car_stock_add';
        $data['data_stock'] = $this->M_demo_car_stock->getDataStock();
        $data['data_branch'] = $this->M_demo_car_stock->getDataBranch();

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        $data['access'] = $access;
        //$access = '0';
        if($access == '1') {
            $data['data_type'] = $this->M_demo_car_stock->getDataType();
            $data['data_location'] = $this->M_demo_car_stock->getDataLocationStock();
        } else {
            $data['data_type'] = $this->M_demo_car_stock->getTypeStock($id_company, $id_branch);
            $data['data_location'] = $this->M_demo_car_stock->getDataLocationStockByBranch($id_branch);
        }

        $data['data_jeniskendaraan'] = $this->M_demo_car_stock->getDataJenisKendaraan();
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function ajax_list()
	{
		$list = $this->M_demo_car_stock->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_stock) {

            if($tbl_stock->veh_status == '1') {
                $veh_status = "<span class='label label-info arrowed-in arrowed-in-right'>Ready</span>";
            } else {
                $veh_status = "<span class='label label-warning arrowed-in arrowed-in-right'>Sold</span>";
            }

            $stock_no = "<span class='label label-primary arrowed-in-right arrowed'>$tbl_stock->stock_no</span>";

            if($tbl_stock->status_stock == 'Demo') {
                $status_stock = "<span class='label label-success arrowed-in arrowed-in-right'>Demo</span>";
            } elseif($tbl_stock->status_stock == 'Operational') {
                $status_stock = "<span class='label label-info arrowed-in arrowed-in-right'>Operational</span>";
            } else {
                $status_stock = "<span class='label label-warning arrowed-in arrowed-in-right'>Demo For Operational</span>";
            }

            $jenis_kendaraan = "<span class='label label-info arrowed-in arrowed-in-right'>$tbl_stock->jenis_kendaraan</span>";
            $type_name = "<span class='label label-info arrowed-in arrowed-in-right'>$tbl_stock->type_name</span>";
            $colour = "<span class='label label-info arrowed-in arrowed-in-right'>$tbl_stock->colour</span>";

			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.base64_encode($tbl_stock->stock_no).' class="editRow ace" />
           <span class="lbl"></span> ';
            $no++;
            $url = base_url("asset/upload_demo_car/$tbl_stock->upload_vin");
			$row = array();
			$row[] = $chk_idmaster;
            $row[] = $no;                       // No
            $row[] = "<a onClick='PDFPopup(this)' req_id=".$url."><i class='fa fa-folder fa-2x text-info' aria-hidden='true'></i></a>";
			$row[] = $stock_no;      // ID Model
            $row[] = $tbl_stock->name_location;    // Brand Name
            $row[] = $type_name;    // Model Name
            $row[] = $tbl_stock->vin;
            $row[] = $tbl_stock->receive_date;
            $row[] = $veh_status;
            $row[] = $tbl_stock->name_branch;
            $row[] = $tbl_stock->stock_keeper;
            $row[] = $colour;
            $row[] = $tbl_stock->engine;
            $row[] = $tbl_stock->remarks;
            $row[] = $status_stock;
            $row[] = $tbl_stock->no_polisi;
            $row[] = $jenis_kendaraan;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_demo_car_stock->count_all(),
						"recordsFiltered" => $this->M_demo_car_stock->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
    }
    
    public function ajax_edit($id)
    {
        $data = $this->M_demo_car_stock->get_by_id($id);
        echo json_encode($data);
    }

    public function add_stock(){

        // Upload pdf
        $config['upload_path']          = './asset/upload_demo_car';
        $config['allowed_types']        = 'pdf';
        $config['max_size']             = 5000;
        

        $stock_no = $this->input->post('stock_no');
        $branch_id = $this->input->post('branch_id');
        $id_locstock = $this->input->post('id_locstock');
        $id_type = $this->input->post('id_type');
        $colour = $this->input->post('id_colour');
        $vin = $this->input->post('vin');
        $receive_date = $this->input->post('receive_date');
        $engine = $this->input->post('engine');
        $veh_status = $this->input->post('veh_status');
        $status_stock = $this->input->post('status_stock');
        $email = $this->input->post('email');
        $remarks = xss_filter($this->input->post('remarks'));
        $stock_keeper = $this->input->post('stock_keeper');
        $no_polisi = $this->input->post('no_polisi');
        $id_kind_vehicle = $this->input->post('id_kind_vehicle');
        $status = '1';

        $receive_date = date("Y-m-d", strtotime($receive_date));

        // Set file name upload
        $config['file_name'] = str_replace('/','-',$stock_no);
        $upload_vin = $config['file_name'].'.pdf';

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('upload_vin')) {
            
            $message_upload = array('error' => $this->upload->display_errors());
            $status_upload = 'gagal';

            $callback = array('status_upload' => $status_upload,
                                'message_upload' => $message_upload,
                                'status_insert' => 'Insert Gagal'
                            );

            echo json_encode($callback);
        } else {
            $message_upload = array('upload_data' => $this->upload->data());
            $status_upload = 'berhasil';

            $data = array(  'stock_no' => $stock_no,
                            'id_locstock' => $id_locstock,
                            'id_type' => $id_type,
                            'vin' => ucwords($vin),
                            'receive_date' => $receive_date,
                            'veh_status' => $veh_status,
                            'branch_id' => $branch_id,
                            'stock_keeper' => $stock_keeper,
                            'id_colour_group' => $colour,
                            'upload_vin' => $upload_vin,
                            'engine' => ucwords($engine),
                            'remarks' => ucwords($remarks),
                            'status_stock' => $status_stock,
                            'status' => $status,
                            'no_polisi' => strtoupper($no_polisi),
                            'id_kind_vehicle' => $id_kind_vehicle
                        );

            $insert = $this->M_demo_car_stock->insert($data);

            if($insert) {
                $callback = array('status_upload' => $status_upload,
                                'message_upload' => $message_upload,
                                'status_insert' => 'Insert'
                            );
            } else {
                 $callback = array('status_upload' => $status_upload,
                                'message_upload' => $message_upload,
                                'status_insert' => 'Insert Gagal'
                            );
            }

            echo json_encode($callback);

        }

    }

    public function update_stock(){

        // Upload pdf
        $config['upload_path']          = './asset/upload_demo_car';
        $config['allowed_types']        = 'pdf';
        $config['max_size']             = 5000;
        
        $stock_no = $this->input->post('stock_no');
        $branch_id = $this->input->post('branch_id');
        $id_locstock = $this->input->post('id_locstock');
        $id_type = $this->input->post('id_type');
        $colour = $this->input->post('id_colour');
        $vin = $this->input->post('vin');
        $receive_date = $this->input->post('receive_date');
        $engine = $this->input->post('engine');
        $veh_status = $this->input->post('veh_status');
        $status_stock = $this->input->post('status_stock');
        $email = $this->input->post('email');
        $remarks = xss_filter($this->input->post('remarks'));
        $stock_keeper = $this->input->post('stock_keeper');
        $no_polisi = $this->input->post('no_polisi');
        $id_kind_vehicle = $this->input->post('id_kind_vehicle');
        $status = '1';
        
        $receive_date = date("Y-m-d", strtotime($receive_date));

        // Set file name upload
        $config['file_name'] = str_replace('/','-',$stock_no);
        $upload_vin = $config['file_name'].'.pdf';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('upload_vin')) {
            
            $message_upload = array('error' => $this->upload->display_errors());
            $status_upload = 'gagal';

            $callback = array('status_upload' => $status_upload,
                                'message_upload' => $message_upload,
                                'status_insert' => 'Upload Gagal'
                            );

            echo json_encode($callback);
        } else {
            $message_upload = array('upload_data' => $this->upload->data());
            $status_upload = 'berhasil';
            $upload_vin = $this->upload->data('file_name');

             $data = array(  
                            'id_locstock' => $id_locstock,
                            'id_type' => $id_type,
                            'vin' => ucwords($vin),
                            'receive_date' => $receive_date,
                            'veh_status' => $veh_status,
                            'branch_id' => $branch_id,
                            'stock_keeper' => $stock_keeper,
                            'id_colour_group' => $colour,
                            'upload_vin' => $upload_vin,
                            'engine' => ucwords($engine),
                            'remarks' => ucwords($remarks),
                            'status_stock' => $status_stock,
                            'status' => $status,
                            'no_polisi' => strtoupper($no_polisi),
                            'id_kind_vehicle' => $id_kind_vehicle
                        );

            $upload = $this->M_demo_car_stock->update($stock_no, $data);

            if($upload) {
                $callback = array('status_upload' => $status_upload,
                                'message_upload' => $message_upload,
                                'status_insert' => 'Upload'
                            );
            } else {
                 $callback = array('status_upload' => $status_upload,
                                'message_upload' => $message_upload,
                                'status_insert' => 'Upload Gagal'
                            );
            }

            echo json_encode($callback);

        }
    }

    public function delete_data() {
        $stock_no = base64_decode($this->input->post('ID'));

        $data = array('status' => '0');

        $delete = $this->M_demo_car_stock->delete($stock_no, $data);

        if($delete) {
            $callback = 'Delete';
        } else {
            $callback = 'Delete Gagal';
        }

        echo json_encode($callback);
    }

    function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
        $this->db->from('qv_stock_demo_car');
		$this->db->where('veh_status = 1');
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
            $this->db->like("stock_no",$search);
            $this->db->or_like("name_location",$search);
            $this->db->or_like("type_name",$search);
            $this->db->or_like("vin",$search);
            $this->db->or_like("receive_date",$search);
            $this->db->or_like("veh_status",$search);
            $this->db->or_like("name_branch",$search);
            $this->db->or_like("stock_keeper",$search);
            $this->db->or_like("colour",$search);
            $this->db->or_like("upload_vin",$search);
            $this->db->or_like("engine",$search);
            $this->db->or_like("remarks",$search);
            $this->db->or_like("email_user_stock",$search);
            $this->db->or_like("date_send_approval",$search);
            $this->db->where('status = 1');
		}


		/*Lanjutkan pencarian ke database*/
		//$this->db->limit($length,$start);
		//$this->db->where('status = 1');
		/*Urutkan dari alphabet paling terkahir*/
		//$this->db->order_by('id_stock','ASC');
		//$query=$this->db->get('tbl_stock');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
            $this->db->like("stock_no",$search);
            $jum=$this->db->get('qv_stock_demo_car');
            $output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		// Datatables Variables
        $order = $this->input->get("order");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }

        $columns_valid = array(
            "qv_stock_demo_car.stock_no", 
            "qv_stock_demo_car.name_location",
            "qv_stock_demo_car.type_name",
            "qv_stock_demo_car.vin",
            "qv_stock_demo_car.receive_date",
            "qv_stock_demo_car.veh_status",
            "qv_stock_demo_car.name_branch",
            "qv_stock_demo_car.stock_keeper",
            "qv_stock_demo_car.colour",
            "qv_stock_demo_car.upload_vin",
            "qv_stock_demo_car.engine",
            "qv_stock_demo_car.remarks",
            "qv_stock_demo_car.email_user_stock",
            "qv_stock_demo_car.date_send_approval"
        );

        if(!isset($columns_valid[$col])) {
            $order = null;
        } else {
            $order = $columns_valid[$col];
        }
		$this->db->where('status = 1');
        $query = $this->M_demo_car_location->get_data($start, $length, $order, $dir);
		
		foreach ($query->result_array() as $tbl_stock) {
			$chk_idmaster ='<div align="center"><input id="checkmodel" name="checkmodel" type="checkbox" value='.$tbl_stock["stock_no"].' class="editRow ace" req_id_del='.$tbl_stock["stock_no"].' />
           <span class="lbl"></span> ';
		   
            $output['data'][]=array($tbl_stock['stock_no'],
                                    $tbl_stock['name_location'], 
                                    $tbl_stock['type_name'],
									$chk_idmaster);

		}

		echo json_encode($output);


    }

    public function edit() {
        $id = base64_decode($this->input->get('id'));

        $data['show_view'] = 'demo_car/V_demo_car_stock_edit';
        $data['data_stock'] = $this->M_demo_car_stock->get_by_id($id)[0];
        $data['data_colour'] = $this->M_demo_car_stock->getDataColourByType($data['data_stock']->id_type);
        $data['data_branch'] = $this->M_demo_car_stock->getDataBranch();
        
        if(!$data['data_stock']) {
           redirect('demo_car/C_demo_car_stock');
        }

        $access = $this->session->userdata('apps_accsess');
        $id_company = $this->session->userdata('id_company');
        $id_branch = $this->session->userdata('branch_id');
        $data['access'] = $access;
        //$access = '0';
        if($access == '1') {
            $data['data_type'] = $this->M_demo_car_stock->getDataType();
            $data['data_location'] = $this->M_demo_car_stock->getDataLocationStock();
        } else {
            $data['data_type'] = $this->M_demo_car_stock->getTypeStock($id_company, $id_branch);
            $data['data_location'] = $this->M_demo_car_stock->getDataLocationStockByBranch($id_branch);
        }

        $data['data_jeniskendaraan'] = $this->M_demo_car_stock->getDataJenisKendaraan();
        $data['total'] = $this->M_demo_car_stock->get_count_id();
        $model  = $this->session->userdata('model');	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "model". " : ".$model ;
		$this->load->view('dashboard/Template',$data);		
    }

    public function stock_no() {
        $short_company = $this->session->userdata('short');
        $short_branch = $this->session->userdata('branch_short');
        $tahun = substr(date('Y'), 1, 3);
        $bulan = date('m');
        $tanggal = date('d');
        $total = $this->M_demo_car_stock->get_count_id();
        $total = $total[0]->totaldata;
        $total = substr($total, -4);
        $total = $total+1;

        if($total<10) {
            $total = '000'.$total;
        } elseif($total<100) {
            $total = '00'.$total;
        } elseif($total<1000) {
            $total = '0'.$total;
        }

        $kode = $short_company.'/'.$short_branch.'/'.$tahun.'/'.$bulan.'/'.$tanggal.'/'.$total;

        return $kode;
    }

    public function check_no_pol($id=NULL) {

        if($id===NULL OR $id=='') {
            redirect('demo_car/C_demo_car_stock');
        }

        $data = $this->M_demo_car_stock->get_no_pol($id);

        echo json_encode($data);
    }

    public function ajax_colour() {
        $id_type = $this->input->get('type');

        $data = $this->M_demo_car_stock->getDataColourByType($id_type);

        $html = '
      
        <select id="colour" name="colour" data-title="Colour" class="form-control validation" onChange="getIDColour(this.value)">
                            <option value=""> - Pilih Colour - </option>
        ';

        foreach($data as $value) {
            $html .= '<option value="'.$value->id_colour_group.'"> '.$value->colour.' </option>';
        }       

        $html .= '</select> ';

        echo $html;
        
    }

}