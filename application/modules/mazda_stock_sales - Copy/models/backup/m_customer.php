<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_customer extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_customer';
	 
    public function tampil_vendor(){ //tampil table untuk ngececk num_row						     	     		  
	     $ses_branch_id = $this->session->userdata('branch_id'); //manggili session id 			
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		  		 		 		
		 
		 $this->load->database();		
		 $this->db->select('*');	
		 $this->db->where('branch_id',$ses_branch_id);								 
		 $this->db->where('status',$status_delete);				
		 $this->db->order_by('id_cust','desc');
		 $result = $this->db->get('qv_complite_customer');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	     
		 $ses_branch_id = $this->session->userdata('branch_id'); //manggili session id 				 				
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  	     		 
	     $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('branch_id',$ses_branch_id);		
		 $this->db->where('status',$status_delete);				  		 
		 $this->db->order_by('id_cust','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_customer')->result();		 		   
		 return $result ;
	}							  		
		
		
  public function save_data()
  {
	
	$ses_branch_id = $this->session->userdata('branch_id'); //manggili session id 	
	
	$str_customer_name = strtoupper($this->input->post('txtcust'));  

	$rdl = $this->input->post('rdl');  
	$rdp = $this->input->post('rdp');  	
	$str_email = $this->input->post('txtemail');
	$str_phone  = $this->input->post('txtphone'); 
	$str_phone2  = $this->input->post('txtphone2'); 
	$str_phoneoff  = $this->input->post('txtphoneoff');  	 	
	$str_alamat = $this->input->post('txtalamat');  
	$stragama =$this->input->post('cboagama');
	
	
	if ($rdl =="" ) :
	   $str_value_gender = $rdp;
	else:
		$str_value_gender = $rdl;
	endif;   
	
	if ($this->input->post('txtdatettg') != ""): 	  	 			  
			    $txtdatettg = date('Y-m-d',strtotime($this->input->post('txtdatettg')));
	else:
		$txtdatettg=null;
	endif; 	
	
	$str_status = "1";  
	
	$insert_data = array(
	  				"cust_name" => $str_customer_name,
					"address" => $str_alamat,
					"phone" => $str_phone,	
					"phone2" => $str_phone2,	
					"phone_office" => $str_phoneoff,	
					"religion" => $stragama,			
					"jenkel" => $str_value_gender,
					"ttg" => $txtdatettg,
					"email"=> $str_email,				
					"branch_id" => $ses_branch_id,
					"religion" => $stragama	,
					"status" =>$str_status					
					);
	$this->db->insert('tbl_customer',$insert_data);		
	return true;							
  }		
  
    public function edit_data()
	{		 		
	    $ses_branch_id = $this->session->userdata('branch_id'); //manggili session id 	
		$str_idcust = $this->input->post('txtid');  				
		$str_customer_name = strtoupper($this->input->post('txtcust'));  
		$str_phone  = $this->input->post('txtphone'); 
		$str_phone2  = $this->input->post('txtphone2'); 
		$str_phoneoff  = $this->input->post('txtphoneoff');  	
		$str_alamat = $this->input->post('txtalamat');  
		$rdl = $this->input->post('rdl');  
	    $rdp = $this->input->post('rdp'); 
		$str_email = $this->input->post('txtemail');
		$stragama =$this->input->post('cboagama');
		
		if ($rdl =="" ) :
		    $str_value_gender = $rdp;
		else:
			$str_value_gender = $rdl;
		endif;   
		
		
		if ($this->input->post('txtdatettg') != ""): 	  	 			  
			$txtdatettg = date('Y-m-d',strtotime($this->input->post('txtdatettg')));
		else:
			$txtdatettg=null;
		endif; 	
		 		
		
		if ($str_idcust !='') {
			$edit_data = array(
							"cust_name" => $str_customer_name,
							"address" => $str_alamat,
							"jenkel" => $str_value_gender,
					        "ttg" => $txtdatettg,
							"email"=> $str_email,			
							"phone" => $str_phone,	
							"phone2" => $str_phone2,	
							"phone_office" => $str_phoneoff,	
							"religion" => $stragama																						
							);			
			$this->db->where('branch_id',$ses_branch_id);				
			$this->db->where('id_cust',$str_idcust);
			$this->db->update('tbl_customer',$edit_data); 															
			return true;		
		}else{
			return false;		
		}
								
	  }		
	  
	    public function delete_data()
	    {		 		
			$delete = $this->input->post('msg');  
			$str_status_delete = "0" ;
								
			if ($delete !='') {						
				for ($i=0; $i < count($delete) ; $i++) {	
					$del_data = array("status" => $str_status_delete,);																																																
					
					$this->db->where('id_cust',$delete[$i]);
					$this->db->update('tbl_customer',$del_data); 
				}		
				return true;		
			}else{
				return false;		
			}								
	  }		
				
		public function search_data() 	// fungsi cari data vendor
		{
		   $cari = $this->input->post('txtcari');
		   $kategory =  $this->input->post('cbostatus');	  		   		  
		   $ses_branch_id = $this->session->userdata('branch_id');
		   $status_del = "1";	   
		   
		   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
		   {
			   redirect('c_customer/c_customer');  //direct ke url c_vendor/c_vendor
		   }else{	
			   $this->load->database();		   			  
			   $this->db->where('branch_id',$ses_branch_id);
			   $this->db->where('status', $status_del);
			   $this->db->like($kategory,$cari);			  
			   $result = $this->db->get('qv_complite_customer');		 
			   return $result->result(); 
		   }
		} 		    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}