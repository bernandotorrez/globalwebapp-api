<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_print_bstk extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_master_stockin';
	 
    public function tampil_add_sell(){ //tampil table untuk ngececk num_row						     	     		  	   
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_bm = "1" ; //jika 1 bm sudah approve
	     $flag_finance = "1" ; //jika 1 fiannce sudah approve
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $status_flagaproval = "1"; //jika 1 sudah terkirim.
		 $status_flag_sell = "0"; // flag kosong sudah terjual
		 
		 $this->load->database();		
		 $this->db->select('id_trans_sales,name_branch,branch_id,nospk,stock_no,vin,engine,desc_type,colour,curr_sell,price_sell,remaining_amount_customer,discount,total_paymet_customer,date_aloc,lama_spk,cust_name,sal_name,wrs_date,remarks,date_transaksi,date_reminder_spk,status_flag_sell,date_bstk,flag_print_bstk,flag_upload_faktur,path_up_fatktur,status');				
		 if($this->session->userdata('aproval_flag') !='4'){	//jika status group user nya bukan purchase						
		 	$this->db->where('branch_id',$ses_id_branch );				 
		 }		 		 				
		 $this->db->where('flag_send_approval',$status_flagaproval);		 
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('approve_fin', $flag_finance);	
		 $this->db->where('aprove_bm',$flag_bm);	
		 $this->db->where('status_flag_sell',$status_flag_sell);				 
		 $this->db->order_by('date_transaksi','desc');
		 $result = $this->db->get('qv_complite_master_sell');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_bm = "1" ; //jika 1 bm sudah approve
	     $flag_finance = "1" ; //jika 1 fiannce sudah approve
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $status_flagaproval = "1"; //jika 1 sudah terkirim.
		 $status_flag_sell = "0"; // flag kosong sudah terjual
		  
		 $this->load->database();		
		 $this->db->select('id_trans_sales,name_branch,branch_id,nospk,stock_no,vin,engine,desc_type,colour,curr_sell,price_sell,remaining_amount_customer,discount,total_paymet_customer,date_aloc,lama_spk,cust_name,sal_name,wrs_date,date_transaksi,date_reminder_spk,remarks,status_flag_sell,date_bstk,flag_print_bstk,flag_upload_faktur,path_up_fatktur,status');	
		 if($this->session->userdata('aproval_flag') !='4'){	//jika status group user nya bukan purchase					
		 	$this->db->where('branch_id',$ses_id_branch );				 
		 }
		 $this->db->where('flag_send_approval',$status_flagaproval);		 
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('approve_fin', $flag_finance);	
		 $this->db->where('aprove_bm',$flag_bm);	
		 $this->db->where('status_flag_sell',$status_flag_sell);		 		
		 $this->db->order_by('date_transaksi','desc');		
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_master_sell')->result();		 		   
		 return $result ;
	}							  		
	
	function search_data() 	// fungsi cari data pp model
	{
	   $cari          =  $this->input->post('txtcari');
	   $kategory      =  $this->input->post('cbostatus');	  	   	 
	   $ses_branch_id =  $this->session->userdata('branch_id');
	   $flag_status   = "1" ;  //jika 0 terdelete
	  
	    $flag_bm = "1" ; //jika 1 bm sudah approve
	    $flag_finance = "1" ; //jika 1 fiannce sudah approve
		$status_delete = "1"; //jika 0 terdelete jika 1 aktif
		$status_flagaproval = "1"; //jika 1 sudah terkirim.
	   	   	   
	   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
	   {
		   redirect('c_print_bstk/c_print_bstk');  //direct ke url add_menu_booking/add_menu_booking
	   }else{	
	       $this->load->database();	
		    if($this->session->userdata('aproval_flag') !='4'){	//jika status group user nya bukan purchase						
		 		$this->db->where('branch_id',$ses_branch_id );				 
			}			  	   		  		 
	      // $this->db->where('branch_id',$ses_branch_id);
	       $this->db->where('status',$flag_status);
	       $this->db->where('approve_fin', $flag_finance);	
		   $this->db->where('aprove_bm',$flag_bm);	
		   $this->db->where('flag_send_approval',$status_flagaproval);
	       $this->db->like($kategory,$cari);		    		   
	       $result = $this->db->get('qv_complite_master_sell');		 
	   
	       $this->session->set_flashdata('cari',$cari);	
	       return $result->result(); 
	   }
	} 
	
	function get_branch()
	{
		$stridbranch = $this->session->userdata("branch_id");	
		
		$this->load->database();
		$this->db->select("*");
		$this->db->where('branch_id',$stridbranch);
		$this->db->where('status', "1");
		$query_result = $this->db->get('tbl_branches');
		
		if ($query_result->num_rows() == 1) :
			foreach ($query_result->result() as $rowbranch) :
			   $data_branch_ses =array( 'ses_name_branch' => $rowbranch->place,
			   							'ses_city'		  => $rowbranch->city,		
			   							'ses_address'	  => $rowbranch->address,
										'ses_phone' 	  => $rowbranch->phone,
										'ses_fax' 		  => $rowbranch->fax													   									  );
				 $this->session->set_userdata($data_branch_ses);						
			endforeach;
		endif;
		
	}			 
	
	function ever_print_bstk()
	{
        $strid_trans =$this->input->post('msg'); 	 	
	    $status = "1"; //jika 0 terdelete jika 1 aktif
	    $status_flag_bstk = "1"; //jika 1 sudah terkirim.
			
			for ($i=0; $i < count($strid_trans) ; $i++) : 			
				$query_check = $this->db->select('id_trans_sales,flag_print_bstk,status')
								->where('id_trans_sales',$strid_trans[$i])
								->where('flag_print_bstk',$status_flag_bstk)
								->where('status',$status)							
								->get('qv_complite_report_bstk');
			endfor;
													
			if ($query_check->num_rows() == 1): //Jika ketemu record					
				return true;		
			endif;		
	}
	
	function ever_print_kwetansi()
	{
	    $strid_trans =$this->input->post('msg'); 	 	
	    $status = "1"; //jika 0 terdelete jika 1 aktif
	    echo $status_flag_kwetansi = "1"; //jika 1 sudah terkirim.
			
			for ($i=0; $i < count($strid_trans) ; $i++) : 			
				$query_check = $this->db->select('id_trans_sales,flag_print_kwetansi,status')
								->where('id_trans_sales',$strid_trans[$i])
								->where('flag_print_kwetansi',$status_flag_kwetansi)
								->where('status',$status)							
								->get('qv_complite_report_bstk');
			endfor;
													
			if ($query_check->num_rows() == 1): //Jika ketemu record								  
				return true;		
			endif;			
	}
	function get_data_forcetak_bstk()
	{
	  
	 ( $strid_trans =$this->input->post('msg')); 	 	  						  					    						   	  $ses_branch_id =  $this->session->userdata('branch_id');
	  $flag_bm = "1" ; //jika 1 bm sudah approve
	  $flag_finance = "1" ; //jika 1 fiannce sudah approve
	  $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	  $status_flagaproval = "1"; //jika 1 sudah terkirim.	
			   	   					  				   			  			   	  		  
			for ($i=0; $i < count($strid_trans) ; $i++) { 				
				  $query = $this->db->select('*')
								->where('id_trans_sales',$strid_trans[$i])
								->where('branch_id',$ses_branch_id)
								->where('status',$status_delete)
								->where('approve_fin',$flag_finance)
								->where('aprove_bm',$flag_bm)								
								->where('flag_send_approval',$status_flagaproval)
								->get('qv_complite_report_bstk');
				  if ($query->num_rows() > 0)
				  {							     			      					 
					 foreach ($query->result() as $row)
					 {
						$data = array('sesidtrans_bstk' => $strid_trans[$i],
									  'ses_csutomer'	=> $row->cust_name,
									  'ses_address_cus' => $row->address,
									  'ses_phone_cus' 	=> $row->phone,
									  'ses_vin' 		=> $row->vin,
									  'ses_engine' 		=> $row->engine,
									  'ses_desc_type' 	=> $row->desc_type, 															
									  'ses_colour' 		=> $row->colour,
									  'ses_date_bstk' 	=> $row->date_bstk,
									  'ses_year' 		=> $row->year 							
									  ); //buat array di session		
						$this->session->set_userdata($data); //simpan di session
					 }	
					
					 //check flag print bstk dan status bstk------------------------------
					   if($row->flag_already_bstk != "1"):
					      $update_flag_print =array('flag_already_bstk' => "1",
						  							'flag_print_bstk' => "1", 	
					                                'date_bstk' => date('Y-m-d')  
					 						       );					     
						else :
					       $update_flag_print =array('flag_print_bstk' => "1",
						   							 'date_bstk' => date('Y-m-d')
						   							);					
						endif;			
									
						$this->db->where('id_trans_sales',$strid_trans[$i]);  			
					    $this->db->update('tbl_sales_unit_transaksi',$update_flag_print); 
					  //----------------------------------------------------------------------	  									 					   
					   return $query->result();										 
				  }else{
					   return false;
				  }
			  }
	
	   
	 }   	
	 
	 
	 function get_data_forcetak_kwetansi()
	{
	  
	 ( $strid_trans =$this->input->post('msg')); 	 	  						  					    						   	  $ses_branch_id =  $this->session->userdata('branch_id');
	  $flag_bm = "1" ; //jika 1 bm sudah approve
	  $flag_finance = "1" ; //jika 1 fiannce sudah approve
	  $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	  $status_flagaproval = "1"; //jika 1 sudah terkirim.	
			   	   					  				   			  			   	  		  
			for ($i=0; $i < count($strid_trans) ; $i++) { 				
				  $query = $this->db->select('*')
								->where('id_trans_sales',$strid_trans[$i])
								->where('branch_id',$ses_branch_id)
								->where('status',$status_delete)
								->where('approve_fin',$flag_finance)
								->where('aprove_bm',$flag_bm)								
								->where('flag_send_approval',$status_flagaproval)
								->get('qv_complite_report_bstk');
				  if ($query->num_rows() > 0)
				  {							     			      					 
					 foreach ($query->result() as $row)
					 {
						$data = array('sesidtrans_bstk' => $strid_trans[$i],
									  'ses_csutomer'	=> $row->cust_name,
									  'ses_address_cus' => $row->address,
									  'ses_phone_cus' 	=> $row->phone,
									  'ses_vin' 		=> $row->vin,
									  'ses_engine' 		=> $row->engine,
									  'ses_price_sell' 	=> $row->price_sell,
									  'ses_totalpay_cust'=> $row->total_paymet_customer,
									  'ses_desc_type' 	=> $row->desc_type, 															
									  'ses_colour' 		=> $row->colour,
									  'ses_date_bstk' 	=> $row->date_bstk,
									  'ses_year' 		=> $row->year 							
									  ); //buat array di session		
						$this->session->set_userdata($data); //simpan di session
					 }	
					
					 //check flag print bstk dan status bstk------------------------------
					   if($row->flag_already_kwetansi != "1"):
					      $update_flag_print =array('flag_already_kwetansi' => "1",
						  							'flag_print_kwetansi' => "1", 	
					                                'date_kwetansi' => date('Y-m-d')  
					 						       );					     
						else :
					       $update_flag_print =array('flag_already_kwetansi' => "1",
						   							 'date_kwetansi' => date('Y-m-d')
						   							);					
						endif;			
									
						$this->db->where('id_trans_sales',$strid_trans[$i]);  			
					    $this->db->update('tbl_sales_unit_transaksi',$update_flag_print); 
					  //----------------------------------------------------------------------	  									 					   
					   return $query->result();										 
				  }else{
					   return false;
				  }
			  }
	
	   
	 }   	   		   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			

function insert_edit_detail()
	{				
		 //dari master-------
		  $txtidtrans= $this->input->post('txtidtrans');
		  $txtvin= $this->input->post('txtvin');
		  $txtengin= $this->input->post('txtengin');
		 //end-------------------------------------
		 
		 //dari detail-------
		 $txtiddet = $this->input->post('txtiddet');
		 $txtdescpay =$this->input->post('txdescdet');			 		
		 $txtdatedet = $this->input->post('txtdatedet');			
		 		  
		 //-------	  
			  //---------------------unformat price---------			  			   
			   $str_equel_amount = $this->input->post('txtunitprice');
			    if (strstr( $str_equel_amount, ',' ) ) $str_equel_amount = str_replace( ',','', $str_equel_amount );  
			  //end----------------------
		
		//uploafoto
		$str_notrans =$this->session->userdata("ses_notrans") ;	 		
		$str_branch= $this->session->userdata("branch_short");
				
		$stryear =  substr(date('Y'),-3);
		$str_date = $stryear."-".date("m")."-".date("d");
	    $str_count= substr($txtidtrans,-3);		
	    $str_path='asset/upload_foto/'.$str_branch; // alamat upload quatation nya. 
									     		 		
		//alamat upload quatation beserta filenya.		    				
		$str_address = $str_path."/"."INV"."-".$str_branch."-".$str_date."-".$str_count;		
		$str_name_foto ="INV"."-".$str_branch."-".$str_date."-".$str_count;
		//------------------------
		if(count($txtiddet)==0) :
		   return false;	 
		else:
		   for ($intdetail=0;$intdetail < count($txtiddet);$intdetail++) : 	
		        $payment_cust = $this->input->post('txtpaydet');					
			if (strstr( $payment_cust[$intdetail], ',' ) ) $payment_cust[$intdetail] = str_replace( ',','', $payment_cust[$intdetail] );   			 	   			    
			 if ($txtiddet[$intdetail] == "") :  					  				   															
				//if ($txtdescpay[$intdetail] !="" && $payment_cust[$intdetail] !="" && $payment_cust[$intdetail] !="0.00" &&  $txtdatedet[$intdetail] !="" ):			
				if ($payment_cust[$intdetail] !="" && $payment_cust[$intdetail] !="0.00"):			
						
						//buat anama alamat pdf
						if (empty($_FILES['userfile']['name'])) :						
							$str_adress_apload ="";
							$str_name_aplod ="";
						else:
							$str_adress_apload =$str_address.($intdetail+1).".pdf";
							$str_name_aplod =$str_name_foto.($intdetail+1);	
						endif;	
						
						//end--------------------------------------------		
						
						//date--------------------------------------------------
						 $buff_date = date('Y-m-d',strtotime($txtdatedet[$intdetail]));			      
							   if ($txtdatedet[$intdetail] !="" ) :
									$txthasildate  = date('Y-m-d',strtotime($txtdatedet[$intdetail]));
							   else :	
									$txthasildate =null ;
							   endif;	
						 //end date	-----------------------------											
							
						$data_detail=array(
											'id_trans_sales' => $txtidtrans,
											'desc_det' =>$txtdescpay[$intdetail] ,
											'pay_date' => $txthasildate ,												
											'amount' => $payment_cust[$intdetail],
											'equel_amount' =>$str_equel_amount,
											'status' => "1",	
											'attach_trans'=>$str_adress_apload,							
											'temp_vin' =>$txtvin,
											'temp_engine' =>$txtengin,
										  );
						 $this->db->insert('tbl_sales_unit_detail',$data_detail);	
						
						 $this->session->unset_userdata('ses_name_apload'); 
			             $buff_nama_foto=array('ses_name_apload' => $str_name_aplod);	
						 $this->session->set_userdata($buff_nama_foto);
						 
						 
						 
					     //update data master----------------------
						 //---------------------unformat price---------			  			   
						  $txtpcr= $this->input->post('txtpcr');
						  if (strstr( $txtpcr, ',' ) ) $txtpcr = str_replace( ',','', $txtpcr);  
						  
						  $txtdiscount= $this->input->post('txtdiscount');
						  if (strstr( $txtdiscount, ',' ) ) $txtdiscount = str_replace( ',','', $txtdiscount);  
						  
						  $txttotalpayment= $this->input->post('txttotalpayment');
						  if (strstr( $txttotalpayment, ',' ) ) $txttotalpayment = str_replace( ',','', $txttotalpayment);  
						  
						  $txtdpplusdiscount= $this->input->post('txtdpplusdiscount');
						  if (strstr( $txtdpplusdiscount, ',' ) ) $txtdpplusdiscount = str_replace( ',','', $txtdpplusdiscount);  																					
						 // unformat end----------------------	
							  
							 //proses update master
							 
								 $data_master=array(											
										'remaining_amount_customer' => $txtpcr,	
										'discount'=> $txtdiscount,																													
										'total_paymet_customer'=>$txttotalpayment,																								
										'payment_plus_discount'=>$txtdpplusdiscount,								
										);																		 															
								  $this->db->where('id_trans_sales',$txtidtrans);  			
								  $this->db->update('tbl_sales_unit_transaksi',$data_master);	
							//end--------------------------------------   
							
							
							 //---remove row detail sell from  array format wjen tombol remove click ------------		  
							   $get_iddet_remove_row = $this->input->post('txtbuffid_det') ;
							
							   if(isset($get_iddet_remove_row) && trim($get_iddet_remove_row!='')){					 			   
								   $stsrconvert_iddet = explode(",",$get_iddet_remove_row) ;				   			  		   
								   $flag_remove = "0";		   			   
								   for ($intiddet=0; $intiddet < count($stsrconvert_iddet) ; $intiddet++)
									   {				   									   				   	   
											//table detail 								
											$data = array('status'=>$flag_remove);											
											$this->db->where('id_detail', $stsrconvert_iddet[$intiddet]);
											$this->db->update('tbl_sales_unit_detail',$data);						   	 				
									   }  			
							   };		  
							 //end---------------------------------------------------------------------------
					else :
						
							 //---remove row detail sell from  array format wjen tombol remove click ------------		  
							   $get_iddet_remove_row = $this->input->post('txtbuffid_det') ;
							
							   if(isset($get_iddet_remove_row) && trim($get_iddet_remove_row!='')){					 			   
								   $stsrconvert_iddet = explode(",",$get_iddet_remove_row) ;				   			  		   
								   $flag_remove = "0";		   			   
								   for ($intiddet=0; $intiddet < count($stsrconvert_iddet) ; $intiddet++)
									   {				   									   				   	   
											//table detail 								
											$data = array('status'=>$flag_remove);											
											$this->db->where('id_detail', $stsrconvert_iddet[$intiddet]);
											$this->db->update('tbl_sales_unit_detail',$data);						   	 				
									   }  			
							   };		  
							 //end---------------------------------------------------------------------------						
						return false	 ;
				   endif;							  				        							 					
				else :		
				       //date------------------------	
							 $buff_date = date('Y-m-d',strtotime($txtdatedet[$intdetail]));			      
							   if ($txtdatedet[$intdetail] !="" ) :
									$txthasildate  = date('Y-m-d',strtotime($txtdatedet[$intdetail]));
							   else :	
									$txthasildate =null ;
							   endif;	
						 //end date								 					
						$data_detail=array(
											'id_trans_sales' => $txtidtrans,
											'desc_det' =>$txtdescpay[$intdetail] ,
											'pay_date' => $txthasildate,												
											'amount' => $payment_cust[$intdetail],
											'equel_amount' =>$str_equel_amount,
											'status' => "1",	
											//'attach_trans'=>$str_adress_apload,							
											'temp_vin' =>$txtvin,
											'temp_engine' =>$txtengin,
										  );						  				  
						 $this->db->where('id_detail',$txtiddet[$intdetail]);  					  
						 $this->db->update('tbl_sales_unit_detail',$data_detail);	
					 
					 
						 //update data master----------------------
						 //---------------------unformat price---------			  			   
						  $txtpcr= $this->input->post('txtpcr');
						  if (strstr( $txtpcr, ',' ) ) $txtpcr = str_replace( ',','', $txtpcr);  
						  
						  $txtdiscount= $this->input->post('txtdiscount');
						  if (strstr( $txtdiscount, ',' ) ) $txtdiscount = str_replace( ',','', $txtdiscount);  
						  
						  $txttotalpayment= $this->input->post('txttotalpayment');
						  if (strstr( $txttotalpayment, ',' ) ) $txttotalpayment = str_replace( ',','', $txttotalpayment);  
						  
						  $txtdpplusdiscount= $this->input->post('txtdpplusdiscount');
						  if (strstr( $txtdpplusdiscount, ',' ) ) $txtdpplusdiscount = str_replace( ',','', $txtdpplusdiscount);  																				
						  //end----------------------	
						  
							  //proses update master total payment, 
								 $data_master=array(											
										'remaining_amount_customer' => $txtpcr,	
										'discount'=> $txtdiscount,																													
										'total_paymet_customer'=>$txttotalpayment,																								
										'payment_plus_discount'=>$txtdpplusdiscount,								
										);																		 															
								  $this->db->where('id_trans_sales',$txtidtrans);  			
								  $this->db->update('tbl_sales_unit_transaksi',$data_master);	
							 //end--------------------------------------   */	
							 							 								 	
			 endif;	
		 endfor ;			
		 
		 	  //---remove row detail sell from  array format wjen tombol remove click ------------		  
			   $get_iddet_remove_row = $this->input->post('txtbuffid_det') ;			
			   if(isset($get_iddet_remove_row) && trim($get_iddet_remove_row!='')){					 			   
				   $stsrconvert_iddet = explode(",",$get_iddet_remove_row) ;				   			  		   
				   $flag_remove = "0";		   			   
				   for ($intiddet=0; $intiddet < count($stsrconvert_iddet) ; $intiddet++)
					   {				   									   				   	   
							//table detail 								
							$data = array('status'=>$flag_remove);											
							$this->db->where('id_detail', $stsrconvert_iddet[$intiddet]);
							$this->db->update('tbl_sales_unit_detail',$data);						   	 				  
					   }  			
			   }		  
			 //end---------------------------------------------------------------------------	
												 
			 //nyimpen session address  pdf. 		
					 $this->session->unset_userdata('ses_notrans'); 				
					 $buff_idtrans=array('ses_notrans'=> $txtidtrans				 				
										);					 								 			
					 $this->session->set_userdata($buff_idtrans);
			//end-----------------------					
		return true;	
	  endif;		
	}		
			
 function check_lunas_payment()
 {
    $strid_master =$this->input->post('msg'); 		
	  for ($i=0; $i < count($strid_master) ; $i++) : 							
			$query_cash = $this->db->select("id_trans_sales,remaining_amount_customer") 
								->where('id_trans_sales',$strid_master[$i])
								->where('remaining_amount_customer',"0.00")								
								->get('tbl_sales_unit_transaksi');													
			  if ($query_cash->num_rows() == 1) :	
			  		return true;
			  endif;
		endfor;	  				  		 
 }				 



}

