<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_type_vehicle extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_type_vehicle';
	 
    public function tampil_type(){ //tampil table untuk ngececk num_row						     	     		  	    
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		  		 		 		
		 
		 $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('status',$status_delete);				
		 $this->db->order_by('id_type','desc');
		 $result = $this->db->get('qv_complite_type_vehicle');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	     $ses_id_dept = $this->session->userdata('id_dept'); 
		 $ses_id_company = $this->session->userdata('id_company');  			 				
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  	     		 
	     $this->load->database();		
		 $this->db->select('*');				
		 $this->db->where('status',$status_delete);				  		 
		 $this->db->order_by('id_type','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_type_vehicle')->result();		 		   
		 return $result ;
	}							  		
	
	
  public function tampil_model()
  {
	  $strstatus = "1";
	  $this->load->database();
	  $this->db->select('*');
	  $this->db->where('status',$strstatus);
	  $result = $this->db->get('tbl_brand_model');
	  
	  return $result ;	  
  }	
	
  public function tampil_currency()
  {
	  $this->load->database();
	  $this->db->select('*');	
	  $result = $this->db->get('tbl_curr');
	  
	  return $result ;	  
  }			
  
  public function save_data()
  {		  
	$cbomodel  = $this->input->post('cbomodel');  
	$str_type = strtoupper($this->input->post('txttype'));
	$cbocurr = $this->input->post('cbocurr');  
	
	 //---------------------unformat price---------
	  $txtcostprice = $this->input->post('txtcost');  
     if ( strstr( $txtcostprice, ',' ) ) $txtcostprice = str_replace( ',','', $txtcostprice ); 
	 
	 
     $txtprice = $this->input->post('txtprice');  
     if ( strstr( $txtprice, ',' ) ) $txtprice = str_replace( ',','', $txtprice ); 
 	//-----------------------------------------------------------------
	
	$strfuel =  strtoupper($this->input->post('txtfuel'));  	
	$cbotransmision = $this->input->post('cbotransmision');  
	
	if 	($this->input->post('txtdatewaranty')) :
		$txtdatewaranty = date('Y-m-d',strtotime($this->input->post('txtdatewaranty')));  	
	else:
		$txtdatewaranty = null;
	endif;	
	
	$strmill = $this->input->post('txtmill');  	
	$str_status = "1";  
	
	$insert_data = array(
	  				"id_model" => $cbomodel,
					"desc_type" => $str_type,
					"id_curr" => $cbocurr,
					"price_cost" => $txtcostprice ,
					"price_sell" => $txtprice ,
					"fuel" => $strfuel,					
					"tranmission" => $cbotransmision,
					"man_warnty" => $txtdatewaranty,
					"man_mileage" =>$strmill,
					"status" => $str_status
					);
	$this->db->insert('tbl_brand_type',$insert_data);		
	return true;							
  }		
  
    public function edit_data()
	{		 		
		$str_idtype= $this->input->post('txtid');  
		$cbomodel  = $this->input->post('cbomodel');  
		$cbocurr = $this->input->post('cbocurr');  
	    $str_type = strtoupper($this->input->post('txttype'));
		
	    //---------------------unformat price---------
		 $txtcostprice = $this->input->post('txtcost');  
     if ( strstr( $txtcostprice, ',' ) ) $txtcostprice = str_replace( ',','', $txtcostprice ); 
		
        $txtprice = $this->input->post('txtprice');  
        if ( strstr( $txtprice, ',' ) ) $txtprice = str_replace( ',','', $txtprice ); 
 	    //-----------------------------------------------------------------
	   
		$strfuel =  strtoupper($this->input->post('txtfuel'));  	
		$cbotransmision = $this->input->post('cbotransmision');  	
		
		if 	($this->input->post('txtdatewaranty')) :
			$txtdatewaranty = date('Y-m-d',strtotime($this->input->post('txtdatewaranty')));
		else:
			$txtdatewaranty = null;
		endif;	  	
		
		$strmill = $this->input->post('txtmill');  	  		
		
		if ($str_idtype !='') {
			$edit_data = array(
							"id_model" => $cbomodel,
							"desc_type" => $str_type,
							"id_curr" => $cbocurr,
							"price_cost" => $txtcostprice ,
							"price_sell" => $txtprice ,
							"fuel" => $strfuel,					
							"tranmission" => $cbotransmision,
							"man_warnty" => $txtdatewaranty,
							"man_mileage" =>$strmill,																
							);
							
			$this->db->where('id_type',$str_idtype);
			$this->db->update('tbl_brand_type',$edit_data); 															
			return true;		
		}else{
			return false;		
		}
								
	  }		
	  
	    public function delete_data()
	    {		 		
			$delete = $this->input->post('msg');  
			$str_status_delete = "0" ;
								
			if ($delete !='') {						
				for ($i=0; $i < count($delete) ; $i++) {	
					$del_data = array("status" => $str_status_delete,);																																																
					
					$this->db->where('id_type',$delete[$i]);
					$this->db->update('tbl_brand_type',$del_data); 
				}		
				return true;		
			}else{
				return false;		
			}								
	  }		
				
		public function search_data() 	// fungsi cari data vendor
		{
		   $cari = $this->input->post('txtcari');
		   $kategory =  $this->input->post('cbostatus');	  		   		  
		   $status_del = "1";	   
		   
		   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
		   {
			   redirect('mazda_stock_sales/c_type_vehicle/c_type_vehicle');  //direct ke url c_vendor/c_vendor
		   }else{	
			   $this->load->database();		   			 
			   $this->db->where('status',$status_del);
			   $this->db->like($kategory,$cari);			  
			   $result = $this->db->get('qv_complite_type_vehicle');		 
			   return $result->result(); 
		   }
		} 		    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}