<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class c_approval_cross_sell extends MY_Controller
{
 	
  public function __construct()
    {
		parent::__construct();					
	    $this->load->model('m_approval_cross_sell/M_approval_cross_sell','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();	
	}
	
	public function index()
    {		
	    $this->show_table(); //manggil fungsi show_table		    		   		
	}	
		
	 public function show_table()
    {             					
	     $tampil_table_sell= $this->M_approval_cross_sell->tampil_add_sell()->result();	
	     $total_rows =$this->M_approval_cross_sell->tampil_add_sell()->num_rows();
		 
		 $call_branch =$this->M_approval_cross_sell->tampil_cabang_mazda()->result();	
	  									
	  if ($tampil_table_sell)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(5);
		$per_page =10;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 5;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['stock_view'] =$this->M_approval_cross_sell->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
			
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
		$data['ceck_row'] = $total_rows;	
		$data['tampil_branch_ok'] = $call_branch;													
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'v_approval_cross_sell/v_approval_cross_sell';		
		$this->load->view('dashboard/Template',$data);				
	  }else{		   
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
	    $data['ceck_row'] = $total_rows;	
		$data['tampil_branch_ok'] = $call_branch;	
		$data['pesan'] = 'Data sales unitt table is empty';		
		$data['show_view'] = 'v_approval_cross_sell/v_approval_cross_sell';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }	
  
  public function do_search_data()  // fungsi cari data controler
  {				  			    	
		$tampung_cari = $this->M_approval_cross_sell->search_data(); // manggil hasil cari di model 	
		$call_branch =$this->M_approval_cross_sell->tampil_cabang_mazda()->result();
		 				
		$branch  = $this->session->userdata('name_branch') ; 
	    $company = $this->session->userdata('company') ;		
		
		if($tampung_cari == null){		   		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = "0";			
			$data['stock_view']  = "";
			$data['tampil_branch_ok'] = $call_branch;
			$data['show_view'] = 'v_approval_cross_sell/v_approval_cross_sell';
			$this->load->view('dashboard/Template',$data);			
		}else{				   		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = "1";				
			$data['stock_view']  = $tampung_cari;	
			$data['tampil_branch_ok'] = $call_branch;			
			$data['show_view'] = 'v_approval_cross_sell/v_approval_cross_sell';
			$this->load->view('dashboard/Template',$data);	 
		}	  
    }	
  
  
  
  //------------------------------------VIEW pashing result to modal popup-------------------
	public function get_idtrans_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $idtrans= $this->input->post('id_trans_sales',TRUE); //       
	     $query_master = $this->db->query("select * from qv_complite_master_sell where id_trans_sales ='".$idtrans."'");  
         $query_detail = $this->db->query("select * from tbl_sales_unit_detail where id_trans_sales ='".$idtrans."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   		   
		      $data['str_trans_master']=$query_master->result();
			  $data['str_trans_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_idtrans'] = $idtrans ;
			  $tampil_detail = $this->load->view('v_approval_cross_sell/v_conten_detail',$data,true);			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end phasing------------------------------------------------------------------------------	
	
	
		
		public function do_rejected()
		{							
			if ($this->M_approval_cross_sell->give_rejected()) :			
			      $this->kirim_email_rejected();
				  $this->session->set_flashdata('pesan_succes','Rejected Successfull..!!');				 				
				  redirect('mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell');	 
			else:
				 $this->session->set_flashdata('pesan_fail','Rejected failed, Or Already Approved !  ');
				 redirect('mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell');	   				
			endif;
					
		}
		
		public function kirim_email_rejected()
		{
			   			
			   $this->load->library('email');				 								   		   		  	        
			   $config = array();
			   $config['charset'] = "utf-8";
			   $config['useragent'] = "I.T"; 
			   $config['protocol']= "smtp";
			   $config['mailtype']= "html";
			   $config['mailpath'] = '/usr/sbin/sendmail';
			   $config['charset'] = 'iso-8859-1';
			   $config['wordwrap'] = TRUE;				 
			   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
			   $config['smtp_port']  = '587';		  		  		  
			   $config['newline']    = "\r\n";		  
			   $config['mailtype']   ="html" ;		 	   		  
			   $config['smtp_user']= "helpdesk@eurokars.co.id";
			   $config['smtp_pass']= "#ur0kar5"; 
			   //--------------------------------		 
			   
					    				  
				//Select untuk email master sales----------------------------
				$strstockno =  $this->session->userdata('sess_stockno_reject') ;
				
				$query =$this->db->query("select * from qv_complite_master_stockin where stock_no='".$strstockno."'");				
			    if ($query->num_rows() == 1  ) :					   					   					 
					   $data_email['str_reject_cross_selling'] = $query->result();														
					   $message = $this->load->view('v_approval_cross_sell/v_content_email_reject',$data_email,true);		
					   $result = $this->email ;						   					   
					   
					   $this->email->initialize($config);  
					   $this->email->set_newline("\r\n"); 
					   $sender_email = "helpdesk@eurokars.co.id"; 					   
					   $sender_name = "Epurchasing Notification";					   					    
																		
					   
					   //simpan session alamat email kedalam variable..
							$struseremail = $this->session->userdata('email');
							$strheademail = $this->session->userdata('email_head');
							$strfaemail = $this->session->userdata('email_fa');
						//end-----------------------------------------
					   
						//$to = 'brian.yunanda@eurokars.co.id';
						foreach($query->result() as $row_sell) :
						   // $to = $struseremail.",".$strheademail.",".$strfaemail;
							$to = $row_sell->email_user_stock;
						endforeach; 
						//$to = $struseremail.",".$strheademail.",".$strccemail;
						
						$subject = "Rejected BSTK"." | ".$this->session->userdata('name_branch');				   
						$this->email->from($sender_email, $sender_name);
						$this->email->to($to);
						$this->email->subject($subject);  
						
						//$this->email->message(print_r($message, true));			   
					  
						$this->email->message($message);// tanpa array	
																 
						 if ($this->email->send()) :	  	
							//$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
						//	redirect(current_url());
						// } else {
						 //   show_error($this->email->print_debugger());
						 endif;  		
						 
						  $this->session->unset_userdata('sess_idtrans_reject');						   											   
			    endif;		 
		}

	 public function kirim_email_approved(){          
 		   
		   $this->load->library('email');				 								   		   		  	        
		  		
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;		
		  
		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   $config['smtp_port']  = '587';		  		  
		 
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   
		 
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5"; 
		   //--------------------------------		   		  
		    		   		 		          				   				     		   				  									
			$strid_stock_no =$this->session->userdata("ses_srtock_no");	
			$status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete		
					
		   if ($strid_stock_no  != '') :			        	  	
				//Select untuk email master sales----------------------------
				   $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
				$query =$this->db->query("select * from qv_complite_master_stockin where stock_no ='".$strid_stock_no."'and status ='".$status_aktif_record."'");				
			   if ($query->num_rows() > 0 ) :					   					   										   					
					
					 $data_email['str_cross_selling'] = $query->result();
					 $message = $this->load->view('v_approval_cross_sell/v_content_email_approve',$data_email,true);	
				endif;	   
			endif;
					 
			$result = $this->email ;							   
			$this->email->initialize($config);  
			$this->email->set_newline("\r\n"); 
		   
		    //Exchange						
			
			$sender_email = "helpdesk@eurokars.co.id"; 					   
			$sender_name = "Stock Notification";					   					    
			
			
		   //simpan session alamat email kedalam variable..
				$struseremail = $this->session->userdata('email');
				$strheademail = $this->session->userdata('email_head');
				$strfaemail = $this->session->userdata('email_fa');
			//end-----------------------------------------
		   
			//$to = 'brian.yunanda@eurokars.co.id';
			
			foreach($query->result() as $row_sell) :
				   // $to = $struseremail.",".$strheademail.",".$strfaemail;
					$to = $row_sell->email_user_stock;
			endforeach; 
			
			$subject = "Request ApProval Cross Selling -- Eurokar Surya Utama";				   
			$this->email->from($sender_email, $sender_name);
			$this->email->to($to);
			$this->email->subject($subject);  
			
			//$this->email->message(print_r($message, true));			   
		  
		    $this->email->message($message);// tanpa array	
												  	 
		     if ($this->email->send()) :		  	
			    $this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
			    //redirect(current_url());
		     //} else {
			//    show_error($this->email->print_debugger());
		     endif; 
								
		
			 //Destroy session per variable			 $this->session->unset_userdata('ses_noppnew');		

	 }
	public function multiple_submit()
	{
		if ($this->input->post("btnaprove")) :
			$this->do_approved();
		else:
		   if ($this->input->post("btnreject")) :
		       $this->do_rejected();
		   else:
		   		if ($this->input->post('btncross-approved')):
					$this->approved_cross_selling();
				else:
					if ($this->input->post('btncari')):
					    $this->do_search_data();
					else:   																		
						redirect('mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell');  												
					endif;	
				endif; 
		   endif;
		 endif;  
		
	}

	
public function approved_cross_selling()
 {
   // $this->form_validation->set_rules('cbobranchutama','cbobranchutama','required');	
    $this->form_validation->set_rules('txtid_b','txtid_b','required');
    $this->form_validation->set_rules('txtnostock','txtnostock','required');		
	if ($this->form_validation->run() == FALSE) :			
		 echo $this->session->set_flashdata('pesan','Destination Branch Must Be Required, approval can not be prosses!!');
		   redirect('mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell'); 			   
	else:	 
			 if ($this->M_approval_cross_sell->update_approval_cross_selling()) :
				  $this-> kirim_email_approved();			 
			 	  $this->session->set_flashdata('pesan_succes','approval Cross Selling  Successfull..!!');	
				   redirect('mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell');
			 else:
			 	 echo $this->session->set_flashdata('pesan','approval Cross Selling fail !!');
				  redirect('mazda_stock_sales/c_approval_cross_sell/c_approval_cross_sell');			 
			 endif;
	endif;		 
 }	

public function get_stock_no()//jquery
    {
						
		$result_sellprice = $this->M_approval_cross_sell->get_detail_modal_cross_selling();//from model					
			foreach ($result_sellprice as $row_full):			   			 
				$data['txtvin'] = $row_full->vin; 		 			    
				$data['txtengine']= $row_full->engine;				
				$data['txttype']= $row_full->desc_type;
				$data['txtwarna']= $row_full->colour;	
				$data['txtfrom']= $row_full->name_branch;
				$data['txtiddest']= $row_full->dst_id_branch_cs;	
				$data['txtdest']= $row_full->dst_cross_sell;	
				$data['txtremark']= $row_full->remarks_cross_sell;									
			endforeach;				
		echo json_encode($data);
	}	
	
}