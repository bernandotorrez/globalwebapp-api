<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_control_sell extends MY_Controller
{
 	
  public function __construct()
    {
		parent::__construct();					
	    $this->load->model('m_control_sell/M_control_sell','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();	
	}
	
	public function index()
    {		
	   $this->show_table(); //manggil fungsi show_table			
	}	
		
	 public function show_table()
    {             					
	     $tampil_table_pp= $this->M_control_sell->tampil_add_stock()->result();	
	     $total_rows =$this->M_control_sell->tampil_add_stock()->num_rows();
	  	
		 $tampil_type = $this->M_control_sell->get_type_vehicle();
		 $tampil_branch = $this->M_control_sell->tampil_cabang_mazda()->result();		
										
		 $total_rows_dp =$this->M_control_sell->hitung_jumlah_dp()->num_rows();
		 $total_rows_selling =$this->M_control_sell->hitung_jumlah_sell()->num_rows();								
										
	  if ($tampil_table_pp)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(5);
		$per_page =5;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'mazda_stock_sales/c_control_sell/c_control_sell/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 5;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['selling_view'] =$this->M_control_sell->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
			
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
		$data['ceck_row'] = $total_rows;
		$data['tampil_branch'] = $tampil_branch ;
		$data['tampil_type'] = $tampil_type ;		
		$data['count_row_dp'] =  $total_rows_dp ;															
		$data['count_row_selling'] = $total_rows_selling;
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'v_control_sell/v_control_sell';		
		$this->load->view('dashboard/Template',$data);				
	  }else{	
	    	   
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
	    $data['ceck_row'] = $total_rows;
		$data['tampil_branch'] = $tampil_branch ;
		$data['tampil_type'] = $tampil_type ;
		$data['count_row_dp'] =  $total_rows_dp ;															
		$data['count_row_selling'] = $total_rows_selling;
		$data['pesan'] = 'Data stock unit table is empty';		
		$data['show_view'] = 'v_control_sell/v_control_sell';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }	
  
  public function do_search_data()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_control_sell->search_data()->result();  
		$total_rows =$this->M_control_sell->search_data()->num_rows();			
		
		 $tampil_type = $this->M_control_sell->get_type_vehicle();
		 $tampil_branch = $this->M_control_sell->tampil_cabang_mazda()->result();		
										
		 $total_rows_selling =$this->M_control_sell->total_selling_cari_data()->num_rows();	 
		 $total_rows_dp =$this->M_control_sell->total_dp_cari_data()->num_rows();		;		
		
		 $branch  = $this->session->userdata('name_branch') ; 
	     $company = $this->session->userdata('company') ;		
		
		if($tampung_cari == null){		   		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = $total_rows;	
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;
			$data['count_row_dp'] =  $total_rows_dp ;															
		    $data['count_row_selling'] = $total_rows_selling;
			$data['selling_view']  = "";
			$data['show_view'] = 'v_control_sell/v_control_sell';
			$this->load->view('dashboard/Template',$data);			
		}else{				   		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = $total_rows;
			$data['tampil_branch'] = $tampil_branch ;	
			$data['tampil_type'] = $tampil_type ;
			$data['count_row_dp'] =  $total_rows_dp ;															
		    $data['count_row_selling'] = $total_rows_selling;
			$data['selling_view']  = $tampung_cari;				
			$data['show_view'] = 'v_control_sell/v_control_sell';
			$this->load->view('dashboard/Template',$data);	 
		}	  
    }
	
	 public function do_search_date_data()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_control_sell->get_search_date()->result();  
		$total_rows =$this->M_control_sell->get_search_date()->num_rows();			
		
		 $tampil_type = $this->M_control_sell->get_type_vehicle();
		 $tampil_branch = $this->M_control_sell->tampil_cabang_mazda()->result();		
										
		 $total_rows_selling =$this->M_control_sell->total_stock_date()->num_rows();
		 $total_rows_dp =$this->M_control_sell->total_stock_dp()->num_rows();		
		
		$branch  = $this->session->userdata('name_branch') ; 
	    $company = $this->session->userdata('company') ;		
		
		if($tampung_cari == null){		   		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = $total_rows;	
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;
			$data['count_row_dp'] =  $total_rows_dp ;															
		    $data['count_row_selling'] = $total_rows_selling;
			$data['selling_view']  = "";
			$data['show_view'] = 'v_control_sell/v_control_sell';
			$this->load->view('dashboard/Template',$data);			
		}else{				   		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = $total_rows;
			$data['tampil_type'] = $tampil_type ;
			$data['tampil_branch'] = $tampil_branch ;	
			$data['count_row_dp'] =  $total_rows_dp ;															
		    $data['count_row_selling'] = $total_rows_selling;
			$data['selling_view']  = $tampung_cari;				
			$data['show_view'] = 'v_control_sell/v_control_sell';
			$this->load->view('dashboard/Template',$data);	 
		}	  
    }
  
  public function multiple_submit()
  {
	  if ($this->input->post('btncari')):	
		  $this->do_search_data();
	   else:  
	   	     if ($this->input->post('btnshort')):				    
		         $this->tampil_branch_combo();
	   	     else:  
			    if ($this->input->post('btncaridate')) :
					$this->do_search_date_data();
				else:	
					if ($this->input->post('btnexport')) :					    
						$this->toExcelAll();
					else:							
	  	        		redirect('mazda_stock_sales/c_control_sell/c_control_sell'); 			   	
					endif;	
				endif;
			endif;
		endif;	   	    
	  
  }
  
  public function tampil_branch_combo()
  {	    		   			
        $tampung_cari = $this->M_control_sell->shorting_table_branch()->result();  
		$total_rows =$this->M_control_sell->shorting_table_branch()->num_rows();			
		
		$tampil_branch = $this->M_control_sell->tampil_cabang_mazda()->result();												
		$total_rows_selling =$this->M_control_sell->hitung_jumlah_sell()->num_rows();
		$total_rows_dp =$this->M_control_sell->hitung_jumlah_dp()->num_rows();
		
		$tampil_type = $this->M_control_sell->get_type_vehicle();
		
		$branch  = $this->session->userdata('name_branch') ; 
	    $company = $this->session->userdata('company') ;		
		
		//print_r($tampung_cari);
		if($tampung_cari == null){			      		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = $total_rows;
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;	
			$data['count_row_dp'] =  $total_rows_dp ;															
		    $data['count_row_selling'] = $total_rows_selling;  	
			$data['selling_view']  = "";
			$data['show_view'] = 'v_control_sell/v_control_sell';
			$this->load->view('dashboard/Template',$data);			
		}else{				     		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = $total_rows;	
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;	
			$data['count_row_dp'] =  $total_rows_dp ;															
		    $data['count_row_selling'] = $total_rows_selling;
			$data['selling_view']  = $tampung_cari;				
			$data['show_view'] = 'v_control_sell/v_control_sell';
			$this->load->view('dashboard/Template',$data);	 
		} 	 
  }
  
   public function toExcelAll() {	 
   
        $this->form_validation->set_rules('txtrptdtstart','txtrptdtstart','required');	
		$this->form_validation->set_rules('txtrptdtend','txtrptdtend','required');		
		
		if($this->form_validation->run()==FALSE):
		   echo $this->session->set_flashdata('pesan','Date start and Date end Report is Empty, Report can not be prosses!!');
		   redirect('mazda_stock_sales/c_control_sell/c_control_sell'); 			   
		else:
		   $tampil_to_excel =  $this->M_control_sell->ToExcelAll(); 
		   $hitung_total_selling = $this->M_control_sell->ToExcelAll_total_selling()->num_rows(); 
		   $hitung_total_dp = $this->M_control_sell->ToExcelAll_total_dp()->num_rows();
		endif;  
		
		
		$data['data_excel'] = $tampil_to_excel ;	
		$data['count_row_dp'] =  $hitung_total_dp ;															
		$data['count_row_selling'] = $hitung_total_selling;	
		$this->load->view('v_control_sell/excel_view',$data);
    }
  
  	//------------------------------------VIEW pashing result to modal popup-------------------
	public function get_idtrans_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $idtrans= $this->input->post('id_trans_sales',TRUE); //       
	     $query_master = $this->db->query("select * from qv_complite_master_sell where id_trans_sales ='".$idtrans."'");  
         $query_detail = $this->db->query("select * from tbl_sales_unit_detail where id_trans_sales ='".$idtrans."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   		   
		      $data['str_trans_master']=$query_master->result();
			  $data['str_trans_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_idtrans'] = $idtrans ;
			  $tampil_detail = $this->load->view('v_control_sell/v_conten_detail',$data,true);			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end phasing------------------------------------------------------------------------------	
	
}