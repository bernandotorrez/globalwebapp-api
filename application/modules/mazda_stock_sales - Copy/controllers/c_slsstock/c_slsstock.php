<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_slsstock extends MY_Controller
{
 	
  public function __construct()
    {
		parent::__construct();					
	    $this->load->model('m_slsstock/M_slsstock','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();	
	}
	
	public function index()
    {		
	       $this->show_table(); //manggil fungsi show_table		    		   		
	}	
		
	 public function show_table()
    {             					
	     $tampil_table_pp= $this->M_slsstock->tampil_add_stock()->result();	
	     $total_rows =$this->M_slsstock->tampil_add_stock()->num_rows();
		 $tampil_branch = $this->M_slsstock->tampil_cabang_mazda()->result();
		 
		 $total_rows_stock =$this->M_slsstock->hitung_jumlah_stock()->num_rows();
		 $total_rows_dp =$this->M_slsstock->hitung_jumlah_dp()->num_rows();
		 $total_rows_wrs =$this->M_slsstock->hitung_jumlah_wrs()->num_rows();
	  									
	  if ($tampil_table_pp)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(5);
		$per_page =10;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'mazda_stock_sales/c_slsstock/c_slsstock/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 5;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['stock_view'] =$this->M_slsstock->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
			
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;		
		$data['ceck_row'] = $total_rows;
		$data['count_row_stock'] = $total_rows_stock;
		$data['count_row_dp'] = $total_rows_dp;
		$data['count_row_wrs'] = $total_rows_wrs;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['tampil_branch'] = $tampil_branch ;
		$data['show_view'] = 'v_slsstock/v_table_stock';		
		//$this->load->view('dashboard/Template',$data);
		$this->load->view('dashboard/Template',$data);				
	  }else{		   
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
	    $data['ceck_row'] = $total_rows;	
		$data['count_row_stock'] = $total_rows_stock;
		$data['count_row_dp'] = $total_rows_dp;
		$data['count_row_wrs'] = $total_rows_wrs;	
		$data['tampil_branch'] = $tampil_branch ;
		$data['pesan'] = 'Data stock unit table is empty';		
		$data['show_view'] = 'v_slsstock/v_table_stock';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }	
  
  public function get_stock_no()//jquery
    {
						
		$result_sellprice = $this->M_slsstock->get_detail_modal_cross_selling();//from model					
			foreach ($result_sellprice as $row_full):			   			 
				$data['txtvin'] = $row_full->vin; 		 			    
				$data['txtengine']= $row_full->engine;				
				$data['txttype']= $row_full->desc_type;
				$data['txtwarna']= $row_full->colour;									
			endforeach;				
		echo json_encode($data);
	}	
	
	
}