<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_control_stock extends MY_Controller
{
 	
  public function __construct()
    {
		parent::__construct();					
	    $this->load->model('m_control_stock/M_control_stock','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();	
	}
	
	public function index()
    {		
	   $this->show_table(); //manggil fungsi show_table			
	}	
		
	 public function show_table()
    {             					
	     $tampil_table_pp= $this->M_control_stock->tampil_add_stock()->result();	
	     $total_rows =$this->M_control_stock->tampil_add_stock()->num_rows();
	  	
		 $tampil_type = $this->M_control_stock->get_type_vehicle();
		 $tampil_branch = $this->M_control_stock->tampil_cabang_mazda()->result();		
										
		 $total_rows_stock =$this->M_control_stock->hitung_jumlah_stock()->num_rows();
		 $total_rows_dp =$this->M_control_stock->hitung_jumlah_dp()->num_rows();
		 $total_rows_wrs =$this->M_control_stock->hitung_jumlah_wrs()->num_rows();	
		 $total_rows_cross_sell =$this->M_control_stock->hitung_jumlah_cross_selling()->num_rows();								
										
	  if ($tampil_table_pp)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(5);
		$per_page =5;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'mazda_stock_sales/c_control_stock/c_control_stock/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 5;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['stock_view'] =$this->M_control_stock->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
				
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;		
		
		//-----buat BM login percabang stock-------			
		$data['str_aprovalflag']  = $this->session->userdata('aproval_flag') ; //check untuk kondisi B.M Login
	    $data['str_idgroup'] = $this->session->userdata('id_group') ;  //check untuk kondisi B.M Login
		$data['str_branch_id'] = $this->session->userdata('branch_id') ;  //check untuk kondisi B.M Login
		$data['str_name_branch'] = $this->session->userdata('name_branch') ;  //check untuk kondisi B.M Login
		//-----------------------------------------
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
		$data['ceck_row'] = $total_rows;
		$data['tampil_branch'] = $tampil_branch ;
		$data['tampil_type'] = $tampil_type ;
		$data['count_row_stock'] = $total_rows_stock;
		$data['count_row_dp'] = $total_rows_dp;
		$data['count_row_wrs'] = $total_rows_wrs;		
		$data['count_row_cross_selling'] = $total_rows_cross_sell;															
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'v_control_stock/v_control_stock';		
		$this->load->view('dashboard/Template',$data);				
	  }else{	
	    	   
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;
		//-----buat BM login percabang stock-------			
		$data['str_aprovalflag']  = $this->session->userdata('aproval_flag') ; //check untuk kondisi B.M Login
	    $data['str_idgroup'] = $this->session->userdata('id_group') ;  //check untuk kondisi B.M Login
		$data['str_branch_id'] = $this->session->userdata('branch_id') ;  //check untuk kondisi B.M Login
		$data['str_name_branch'] = $this->session->userdata('name_branch') ;  //check untuk kondisi B.M Login
		//-----------------------------------------					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
	    $data['ceck_row'] = $total_rows;
		$data['tampil_branch'] = $tampil_branch ;
		$data['tampil_type'] = $tampil_type ;
		$data['count_row_stock'] = $total_rows_stock;
		$data['count_row_dp'] = $total_rows_dp;
		$data['count_row_wrs'] = $total_rows_wrs;	
		$data['count_row_cross_selling'] = $total_rows_cross_sell;			
		$data['pesan'] = 'Data stock unit table is empty';		
		$data['show_view'] = 'v_control_stock/v_control_stock';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }	
  
  public function do_search_data()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_control_stock->search_data()->result();  
		$total_rows =$this->M_control_stock->search_data()->num_rows();			
		
		 $tampil_type = $this->M_control_stock->get_type_vehicle();
		 $tampil_branch = $this->M_control_stock->tampil_cabang_mazda()->result();		
										
		 $total_rows_stock =$this->M_control_stock->total_stock_cari_data()->num_rows();		 
		 $total_rows_dp =$this->M_control_stock->total_dp_cari_data()->num_rows();
		 $total_rows_wrs =$this->M_control_stock->total_wrs_cari_data()->num_rows();		
		 $total_rows_cross_sell =$this->M_control_stock->hitung_jumlah_cross_selling_cari_data()->num_rows();	
		 
		 $branch  = $this->session->userdata('name_branch') ; 
	     $company = $this->session->userdata('company') ;		
		
		if($tampung_cari == null){		   		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			//-----buat BM login percabang stock-------			
			$data['str_aprovalflag']  = $this->session->userdata('aproval_flag') ; //check untuk kondisi B.M Login
			$data['str_idgroup'] = $this->session->userdata('id_group') ;  //check untuk kondisi B.M Login
			$data['str_branch_id'] = $this->session->userdata('branch_id') ;  //check untuk kondisi B.M Login
			$data['str_name_branch'] = $this->session->userdata('name_branch') ;  //check untuk kondisi B.M Login
			//-----------------------------------------
			$data['ceck_row'] = $total_rows;	
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;
			$data['count_row_stock'] = $total_rows_stock;
		    $data['count_row_dp'] = $total_rows_dp;
		    $data['count_row_wrs'] = $total_rows_wrs;
			$data['count_row_cross_selling'] = $total_rows_cross_sell;	
			$data['stock_view']  = "";
			$data['show_view'] = 'v_control_stock/v_control_stock';
			$this->load->view('dashboard/Template',$data);			
		}else{				   		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			//-----buat BM login percabang stock-------			
			$data['str_aprovalflag']  = $this->session->userdata('aproval_flag') ; //check untuk kondisi B.M Login
			$data['str_idgroup'] = $this->session->userdata('id_group') ;  //check untuk kondisi B.M Login
			$data['str_branch_id'] = $this->session->userdata('branch_id') ;  //check untuk kondisi B.M Login
			$data['str_name_branch'] = $this->session->userdata('name_branch') ;  //check untuk kondisi B.M Login
			//-----------------------------------------
			$data['ceck_row'] = $total_rows;
			$data['tampil_branch'] = $tampil_branch ;	
			$data['tampil_type'] = $tampil_type ;
			$data['count_row_stock'] = $total_rows_stock;
		    $data['count_row_dp'] = $total_rows_dp;
		    $data['count_row_wrs'] = $total_rows_wrs;
			$data['count_row_cross_selling'] = $total_rows_cross_sell;	
			$data['stock_view']  = $tampung_cari;				
			$data['show_view'] = 'v_control_stock/v_control_stock';
			$this->load->view('dashboard/Template',$data);	 
		}	  
    }
	
	 public function do_search_date_data()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_control_stock->get_search_date()->result();  
		$total_rows =$this->M_control_stock->get_search_date()->num_rows();			
		
		 $tampil_type = $this->M_control_stock->get_type_vehicle();
		 $tampil_branch = $this->M_control_stock->tampil_cabang_mazda()->result();		
										
		 $total_rows_stock =$this->M_control_stock->total_stock_date()->num_rows();
		 $total_rows_dp =$this->M_control_stock->total_stock_dp()->num_rows();
		 $total_rows_wrs =$this->M_control_stock->total_stock_wrs()->num_rows();
		 $total_rows_cross_sel =$this->M_control_stock->total_stock_cross_sell()->num_rows();		
		
		$branch  = $this->session->userdata('name_branch') ; 
	    $company = $this->session->userdata('company') ;	
				
		
		if($tampung_cari == null){		   		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			//-----buat BM login percabang stock-------			
			$data['str_aprovalflag']  = $this->session->userdata('aproval_flag') ; //check untuk kondisi B.M Login
			$data['str_idgroup'] = $this->session->userdata('id_group') ;  //check untuk kondisi B.M Login
			$data['str_branch_id'] = $this->session->userdata('branch_id') ;  //check untuk kondisi B.M Login
			$data['str_name_branch'] = $this->session->userdata('name_branch') ;  //check untuk kondisi B.M Login
			//-----------------------------------------
			$data['ceck_row'] = $total_rows;			
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;
			$data['count_row_stock'] = $total_rows_stock;
		    $data['count_row_dp'] = $total_rows_dp;
		    $data['count_row_wrs'] = $total_rows_wrs;
		    $data['count_row_cross_selling'] = $total_rows_cross_sel;
			$data['stock_view']  = "";
			$data['show_view'] = 'v_control_stock/v_control_stock';
			$this->load->view('dashboard/Template',$data);			
		}else{				   		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			//-----buat BM login percabang stock-------			
			$data['str_aprovalflag']  = $this->session->userdata('aproval_flag') ; //check untuk kondisi B.M Login
			$data['str_idgroup'] = $this->session->userdata('id_group') ;  //check untuk kondisi B.M Login
			$data['str_branch_id'] = $this->session->userdata('branch_id') ;  //check untuk kondisi B.M Login
			$data['str_name_branch'] = $this->session->userdata('name_branch') ;  //check untuk kondisi B.M Login
			//-----------------------------------------
			$data['ceck_row'] = $total_rows;
			$data['tampil_type'] = $tampil_type ;
			$data['tampil_branch'] = $tampil_branch ;	
			$data['count_row_stock'] = $total_rows_stock;
		    $data['count_row_dp'] = $total_rows_dp;
		    $data['count_row_wrs'] = $total_rows_wrs;
			$data['count_row_cross_selling'] = $total_rows_cross_sel;
			$data['stock_view']  = $tampung_cari;				
			$data['show_view'] = 'v_control_stock/v_control_stock';
			$this->load->view('dashboard/Template',$data);	 
		}	  
    }
  
  public function multiple_submit()
  {
	  if ($this->input->post('btncari')):	
		  $this->do_search_data();
	   else:  
	   	     if ($this->input->post('btnshort')):				    
		         $this->tampil_branch_combo();
	   	     else:  
			    if ($this->input->post('btncaridate')) :
					$this->do_search_date_data();
				else:	
					if ($this->input->post('btnexport')) :					    
						$this->toExcelAll();
					else:							
	  	        		redirect('mazda_stock_sales/c_control_stock/c_control_stock'); 			   	
					endif;	
				endif;
			endif;
		endif;	   	    
	  
  }
  
  public function tampil_branch_combo()
  {	    		   			
        $tampung_cari = $this->M_control_stock->shorting_table_branch()->result();  
		$total_rows =$this->M_control_stock->shorting_table_branch()->num_rows();			
		
		$tampil_branch = $this->M_control_stock->tampil_cabang_mazda()->result();												
		$total_rows_stock =$this->M_control_stock->hitung_jumlah_stock()->num_rows();
		$total_rows_dp =$this->M_control_stock->hitung_jumlah_dp()->num_rows();
		$total_rows_wrs =$this->M_control_stock->hitung_jumlah_wrs()->num_rows();	
		$total_rows_cross =$this->M_control_stock->hitung_jumlah_cross_selling()->num_rows();	 
		
		$tampil_type = $this->M_control_stock->get_type_vehicle();
		
		$branch  = $this->session->userdata('name_branch') ; 
	    $company = $this->session->userdata('company') ;		
		
		//print_r($tampung_cari);
		if($tampung_cari == null){			      		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = $total_rows;
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;	
			$data['count_row_stock'] = $total_rows_stock;
		    $data['count_row_dp'] = $total_rows_dp;
		    $data['count_row_wrs'] = $total_rows_wrs;	
			$data['count_row_cross_selling'] =$total_rows_cross;
			$data['stock_view']  = "";
			$data['show_view'] = 'v_control_stock/v_control_stock';
			$this->load->view('dashboard/Template',$data);			
		}else{				     		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = $total_rows;	
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;	
			$data['count_row_stock'] = $total_rows_stock;
		    $data['count_row_dp'] = $total_rows_dp;
		    $data['count_row_wrs'] = $total_rows_wrs;
			$data['count_row_cross_selling'] =$total_rows_cross;
			$data['stock_view']  = $tampung_cari;				
			$data['show_view'] = 'v_control_stock/v_control_stock';
			$this->load->view('dashboard/Template',$data);	 
		} 	 
  }
  
   public function toExcelAll() {	 
   	    $this->form_validation->set_rules('txtrptdtstart','txtrptdtstart','required');	
		$this->form_validation->set_rules('txtrptdtend','txtrptdtend','required');		
		
		if($this->form_validation->run()==FALSE):
		   echo $this->session->set_flashdata('pesan','Date start and Date end Report is Empty, Report can not be prosses!!');
		   redirect('mazda_stock_sales/c_control_stock/c_control_stock'); 			   
		else:
			$tampil_to_excel =  $this->M_control_stock->ToExcelAll(); 		
			$str_flag_stock = $this->input->post('cbostatusstock'); // jika stock combo.
			
			if  ($str_flag_stock == 1) :
				$hitung_total_stock = $this->M_control_stock->ToExcelAll_total_stock()->num_rows(); 
				$hitung_total_dp = $this->M_control_stock->ToExcelAll_total_dp()->num_rows();
				$hitung_total_wrs = $this->M_control_stock->ToExcelAll_total_wrs()->num_rows(); 
				$hitung_total_cross_sell = $this->M_control_stock->ToExcelAll_total_cross_sell()->num_rows(); 
				
				$data['data_excel'] = $tampil_to_excel ;	
				$data['total_stock'] =  $hitung_total_stock ;
				$data['total_dp'] =  $hitung_total_dp ;
				$data['total_wrs'] =  $hitung_total_wrs;	
				$data['total_cross_sale'] =  $hitung_total_cross_sell;
				$data['str_flag_stock'] =  $str_flag_stock;	
				$this->load->view('v_control_stock/excel_view',$data);
			else:	
				$hitung_total_stock_bstk = $this->M_control_stock->ToExcelAll_total_BSTK()->num_rows(); ;		
				$hitung_total_stock = "0" ;
				$hitung_total_dp = "0" ;
				$hitung_total_wrs = "0" ;
				$hitung_total_cross_sell = "0" ;	
				
				$data['data_excel'] = $tampil_to_excel ;			
				$data['total_stock_bstk'] = $hitung_total_stock_bstk;	
				$data['str_flag_stock'] =  $str_flag_stock;	
				$this->load->view('v_control_stock/excel_view',$data);
			endif;													
		endif;	
    }
  
	
}