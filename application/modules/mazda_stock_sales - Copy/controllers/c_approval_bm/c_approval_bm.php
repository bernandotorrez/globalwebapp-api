<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_approval_bm extends MY_Controller
{
 	
  public function __construct()
    {
		parent::__construct();					
	    $this->load->model('m_approval_bm/M_approval_bm','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();	
	}
	
	public function index()
    {		
	    $this->show_table(); //manggil fungsi show_table		    		   		
	}	
		
	 public function show_table()
    {             					
	     $tampil_table_sell= $this->M_approval_bm->tampil_add_sell()->result();	
	     $total_rows =$this->M_approval_bm->tampil_add_sell()->num_rows();
	  									
	  if ($tampil_table_sell)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(5);
		$per_page =10;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'mazda_stock_sales/c_approval_bm/C_approval_bm/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 5;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['sell_view'] =$this->M_approval_bm->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
			
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
		$data['ceck_row'] = $total_rows;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'v_approval_bm/v_approval_bm';		
		$this->load->view('dashboard/Template',$data);				
	  }else{		   
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data sales unitt table is empty';		
		$data['show_view'] = 'v_approval_bm/v_approval_bm';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }	
  
  //------------------------------------VIEW pashing result to modal popup-------------------
	public function get_idtrans_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $idtrans= $this->input->post('id_trans_sales',TRUE); //       
	     $query_master = $this->db->query("select * from qv_complite_master_sell where id_trans_sales ='".$idtrans."'");  
         $query_detail = $this->db->query("select * from tbl_sales_unit_detail where id_trans_sales ='".$idtrans."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   		   
		      $data['str_trans_master']=$query_master->result();
			  $data['str_trans_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_idtrans'] = $idtrans ;
			  $tampil_detail = $this->load->view('v_approval_bm/v_conten_detail',$data,true);			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end phasing------------------------------------------------------------------------------	
	
	public function do_approved()
	{	
		
		if ($this->M_approval_bm->give_flag_approval_bm()) :
		      $this->kirim_email_approve();
		      $this->session->set_flashdata('pesan_succes','Approved GM / BM Successfull..!!');				 							
			  redirect('mazda_stock_sales/c_approval_bm/c_approval_bm');	 
		else:
			 $this->session->set_flashdata('pesan_fail','Approved GM / BM fail, or Already Approved! ');
			 redirect('mazda_stock_sales/c_approval_bm/c_approval_bm');	 			
		endif;
	}
		
		public function do_rejected()
		{				
			//if ($this->M_approval_bm->give_flag_reject_bm()) :
			if ($this->M_approval_bm->give_rejected()) :			
			      $this->kirim_email_rejected();
				  $this->session->set_flashdata('pesan_succes','Rejected Successfull..!!');				 				redirect('mazda_stock_sales/c_approval_bm/c_approval_bm');	 
			else:
				 $this->session->set_flashdata('pesan_fail','Rejected failed, Or Already Approved !  ');
				 redirect('mazda_stock_sales/c_approval_bm/c_approval_bm');	   				
			endif;
					
		}
		
		public function kirim_email_rejected()
		{
			   			
			   $this->load->library('email');				 								   		   		  	        
			   $config = array();
			   $config['charset'] = "utf-8";
			   $config['useragent'] = "I.T"; 
			   $config['protocol']= "smtp";
			   $config['mailtype']= "html";
			   $config['mailpath'] = '/usr/sbin/sendmail';
			   $config['charset'] = 'iso-8859-1';
			   $config['wordwrap'] = TRUE;				 
			   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
			   $config['smtp_port']  = '587';		  		  		  
			   $config['newline']    = "\r\n";		  
			   $config['mailtype']   ="html" ;		 	   		  
			   $config['smtp_user']= "helpdesk@eurokars.co.id";
			   $config['smtp_pass']= "#ur0kar5"; 
			   //--------------------------------		 
			   
					    				  
				//Select untuk email master sales----------------------------
				$stridtrans =  $this->session->userdata('sess_idtrans_reject') ;
				
				$query =$this->db->query("select * from qv_complite_master_sell where id_trans_sales='".$stridtrans."'");				
			    if ($query->num_rows() > 0 ) :					   					   					 
					   $data_email['str_reject_selling'] = $query->result();														
					   $message = $this->load->view('v_approval_bm/v_content_email_reject',$data_email,true);		
					   $result = $this->email ;						   					   
					   
					   $this->email->initialize($config);  
					   $this->email->set_newline("\r\n"); 
					   $sender_email = "helpdesk@eurokars.co.id"; 					   
					   $sender_name = "Reject Notification";					   					    
																		
					   
					   //simpan session alamat email kedalam variable..
							$struseremail = $this->session->userdata('email');
							$strheademail = $this->session->userdata('email_head');
							$strfaemail = $this->session->userdata('email_fa');
						//end-----------------------------------------
					   
						//$to = 'brian.yunanda@eurokars.co.id';
						foreach($query->result() as $row_sell) :
						   // $to = $struseremail.",".$strheademail.",".$strfaemail;
							$to = $row_sell->email_user_sell;
						endforeach; 
						//$to = $struseremail.",".$strheademail.",".$strfaemail;
						
						$subject = "Rejected BSTK"." | ".$this->session->userdata('name_branch');				   
						$this->email->from($sender_email, $sender_name);
						$this->email->to($to);
						$this->email->subject($subject);  
						
						//$this->email->message(print_r($message, true));			   
					  
						$this->email->message($message);// tanpa array	
																 
						 if ($this->email->send()) :	  	
							//$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
						//	redirect(current_url());
						// } else {
						 //   show_error($this->email->print_debugger());
						 endif;  		
						 
						  $this->session->unset_userdata('sess_idtrans_reject');						   											   
			    endif;		 
		}

	public function kirim_email_approve()
	{
		   $this->load->library('email');				 								   		   		
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;				 
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   $config['smtp_port']  = '587';		  		  		  
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   		  
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5"; 
		   //--------------------------------		 
		   
		   $stridtrans =  $this->session->userdata("ses_approval_idtrans_bm") ;
		   $strstatus = "1"; //jika 1 belum terdelete			   		    				  
		   //Select untuk email master sales----------------------------						
		   $query =$this->db->query("select * from qv_complite_master_sell where id_trans_sales='".$stridtrans."'and status ='".$strstatus."'");				
			if ($query->num_rows() > 0 ) :														   			
				   $data_email['str_reject_selling'] = $query->result();														
				   $message = $this->load->view('v_approval_finance/v_content_email_approve',$data_email,true);		
				   $result = $this->email ;						   					   
				   
				   $this->email->initialize($config);  
				   $this->email->set_newline("\r\n"); 
				   $sender_email = "helpdesk@eurokars.co.id"; 					   
				   $sender_name = "Approve Notification";					   					    
																	
				   
				   //simpan session alamat email kedalam variable..
						$struseremail = $this->session->userdata('email');
						$strheademail = $this->session->userdata('email_head');
						$strfaemail = $this->session->userdata('email_fa');
					//end-----------------------------------------
				   
					$to = 'brian.yunanda@eurokars.co.id';
					
					foreach($query->result() as $row_sell) :
					   // $to = $struseremail.",".$strheademail.",".$strfaemail;
						$to = $row_sell->email_user_sell;
					endforeach; 
					//$to = $struseremail.",".$strheademail.",".$strccemail;
					
					$subject = "APPROVED BSTK"." | ".$this->session->userdata('name_branch');				   
					$this->email->from($sender_email, $sender_name);
					$this->email->to($to);
					$this->email->subject($subject);  
					
					//$this->email->message(print_r($message, true));			   
				  
					$this->email->message($message);// tanpa array	
															 
					 if ($this->email->send()) :	  	
						//$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
					//	redirect(current_url());
					// } else {
					 //   show_error($this->email->print_debugger());
					 endif;  
					 
					 $this->session->unset_userdata('ses_approval_idtrans_finance');						   										
			endif;		 			
	}
	public function multiple_submit()
	{
		if ($this->input->post("btnaprove")) :
			$this->do_approved();
		else:
		   if ($this->input->post("btnreject")) :
		       $this->do_rejected();
		   else:
		    	if ($this->input->post("btncari")) :				   
					 $this->do_search_data();
				 else:
		   		 	redirect('mazda_stock_sales/c_approval_bm/c_approval_bm');
			     endif; 			  
		   endif;
		 endif;  
		
	}
	
	public function do_search_data()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_approval_bm->search_data(); // manggil hasil cari di model 										
		$branch  = $this->session->userdata('name_branch') ; 
	    $company = $this->session->userdata('company') ;		
		
		if($tampung_cari == null){		   		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = "0";	
			$data['sell_view']  = "";
			$data['show_view'] = 'v_approval_bm/v_approval_bm';
			$this->load->view('dashboard/Template',$data);			
		}else{				   		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = "1";	
			$data['sell_view']  = $tampung_cari;				
			$data['show_view'] = 'v_approval_bm/v_approval_bm';
			$this->load->view('dashboard/Template',$data);	 
		}	  
    }	

public function get_idtran_reject()//jquery
    {			
	   $result_sellprice = $this->M_approval_bm->get_detail_modal_reject();//from model					
			foreach ($result_sellprice as $row_full):			   			 
				$data['txtvin'] = $row_full->vin; 		 			    
				$data['txtengine']= $row_full->engine;				
				$data['txttype']= $row_full->desc_type;
				$data['txtwarna']= $row_full->colour;									
			endforeach;				
		echo json_encode($data);
	}		
	
}