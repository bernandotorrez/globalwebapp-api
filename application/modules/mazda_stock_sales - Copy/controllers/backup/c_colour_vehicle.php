<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_colour_vehicle extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('m_colour_vehicle/M_colour_vehicle','',TRUE);	
		$this->load->library('form_validation');	   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();							
	}		  	
		
  public function index()
  {					    																
	$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_colour= $this->M_colour_vehicle->tampil_colour()->result();	
	  $total_rows =$this->M_colour_vehicle->tampil_colour()->num_rows();
	  
	  $tampil_table_type = $this->M_colour_vehicle->tampil_type()->result();	
	  									
	  if ($tampil_table_colour)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(4);
		$per_page =10;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'c_colour_vehicle/c_colour_vehicle/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 4;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['colour_view'] =$this->M_colour_vehicle->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
				
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;		
		$data['intno'] = ""; //variable buat looping no table.				
		$data['tampil_type'] = $tampil_table_type;
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;	
		$data['ceck_row'] = $total_rows;														
		$data['intno'] = ""; //variable buat looping no table.														
		$data['show_view'] = 'v_colour_vehicle/v_colour_vehicle';		
		$this->load->view('template_admin',$data);				
	  }else{		    
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;	
	    $data['ceck_row'] = $total_rows;	
		$data['tampil_type'] = $tampil_table_type;
		$data['pesan'] = 'Colour table is empty';		
		$data['show_view'] = 'v_colour_vehicle/v_colour_vehicle';
		$this->load->view('template_admin',$data);				
	  } 
  }
 
 public  function do_save()
 {
	    $this->form_validation->set_rules('cbotype','cbotype','required');
        $this->form_validation->set_rules('txtcolour','txtcolour','required');		
		
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan','Insert Failed  some data must be required!!');
		   redirect('c_colour_vehicle/c_colour_vehicle'); 	
		}else{   		 
			if ($this->M_colour_vehicle->save_data())
			{	 	
				$this->session->set_flashdata('pesan_succes','Insert Modell Succesfully');			
				redirect('c_colour_vehicle/c_colour_vehicle');   
			}else{
				$this->session->set_flashdata('pesan','Insert Modell Failed');		
				redirect('c_colour_vehicle/c_colour_vehicle');   
			}
		}
 } 
 
  public  function do_update()
 {
	    $this->form_validation->set_rules('cbotype','cbotype','required');
        $this->form_validation->set_rules('txtcolour','txtcolour','required');			
		
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan','Update Failed some data must be required!!');
		   redirect('c_colour_vehicle/c_colour_vehicle'); 	
		}else{   		 
			if ($this->M_colour_vehicle->edit_data())
			{	 	
				$this->session->set_flashdata('pesan_succes','Update Model Succesfully');			
				redirect('c_colour_vehicle/c_colour_vehicle');   
			}else{
				$this->session->set_flashdata('pesan','Update Model Failed');		
				redirect('c_colour_vehicle/c_colour_vehicle');   
			}
		}
 } 
 
 public function do_delete()
 {
	 if ($this->M_colour_vehicle->delete_data())
	{	 	
		$this->session->set_flashdata('pesan','Delete Model Succesfully');			
		redirect('c_colour_vehicle/c_colour_vehicle');   
	}else{
		$this->session->set_flashdata('pesan','Delete Model Failed');		
		redirect('c_colour_vehicle/c_colour_vehicle');   
	}
 }
 
 public function do_search()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_colour_vehicle->search_data(); // manggil hasil cari di model 
		$tampil_table_type = $this->M_colour_vehicle->tampil_type()->result();
		  	 	
		if($tampung_cari == null){		 
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;
		   $data['intno'] = ""; //variable buat looping no table.						
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;			   		   $data['ceck_row'] = "0"; // jika tidak ketemu
		   $data['tampil_type'] = $tampil_table_type;	
		   $data['colour_view'] =$tampung_cari;
		   $data['pesan'] = 'Modell Data Not a found'; 		 		  
		   $data['show_view'] = 'v_colour_vehicle/v_colour_vehicle';			
		   $this->load->view('template_admin',$data);					
		}else{					  
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;		
			$data['intno'] = ""; //variable buat looping no table.				
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;	
		    $data['intno'] = ""; //variable buat looping no table.
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['tampil_type'] = $tampil_table_type;
			$data['colour_view'] =$tampung_cari;		   			
			$data['show_view'] = 'v_colour_vehicle/v_colour_vehicle';		
		    $this->load->view('template_admin',$data);		
		}	   
    }
 
 public function multiple_submit()
 {
	 if ($this->input->post('btnsave')) {
		 $this->do_save();
	 }else{
		 if ($this->input->post('btncari')) {
			 $this->do_search();
	 	 }else{
			 if ($this->input->post('btndel')) {
			     $this->do_delete();
	 	     }else{
				  if ($this->input->post('btnupdate')) {
			          $this->do_update();
	 	     	  }else{
					  redirect('c_colour_vehicle/c_colour_vehicle');
				  }			
			 }
		 }
	 }
		 
 }
 
 //------------------------------------pashing result to modal popup View Edit vendor-------------------
	
	public function get_colour_modal_update() {    	       			
	     $id_colour = $this->input->post('id_colour',TRUE); //       
	     $query = $this->db->query("select * from qv_complite_colour where id_colour ='".$id_colour."'");  	    
		
		 foreach ($query->result() as $row)		
		 {
			$data['id_colour'] = $row->id_colour;		
			$data['id_type'] = $row->id_type;
			$data['colour'] = $row->colour;		
		 }
	    
		 echo json_encode($data); //masukan kedalam jasson jquery untuk menampilkan data	   		  			   		      		
    }      					
      						 		
	
}

