<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_slsssell_wrs extends MY_Controller
{
 	
  public function __construct()
    {
		parent::__construct();					
	    $this->load->model('m_slsstock_wrs/M_slssell_wrs','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();	
	}
	
	public function index()
    {		
	       $this->show_table(); //manggil fungsi show_table		    		   		
	}	
		
	 public function show_table()
    {             					
	     $tampil_table_sell= $this->M_slssell_wrs->tampil_add_sell()->result();	
	     $total_rows =$this->M_slssell_wrs->tampil_add_sell()->num_rows();
	  									
	  if ($tampil_table_sell):		
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(4);
		$per_page =10;		
			if (trim($start_row =='')):			
				$start_row ==0;
			endif;		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'c_slssell_wrs/c_slsssell_wrs/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 4;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['sell_view'] =$this->M_slssell_wrs->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
			
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
		$data['ceck_row'] = $total_rows;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'v_slsstock_wrs/v_table_wrs';		
		$this->load->view('template_admin',$data);				
	  else:		   
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data sales unitt table is empty';		
		$data['show_view'] = 'v_slsstock_wrs/v_table_wrs';
		$this->load->view('template_admin',$data);				
	  endif; 
  }	
  
  //------------------------------------VIEW pashing result to modal popup-------------------
	public function get_idtrans_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $idtrans= $this->input->post('id_trans_sales',TRUE); //       
	     $query_master = $this->db->query("select * from qv_complite_master_sell where id_trans_sales ='".$idtrans."'");  
         $query_detail = $this->db->query("select * from tbl_sales_unit_detail where id_trans_sales ='".$idtrans."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   		   
		      $data['str_trans_master']=$query_master->result();
			  $data['str_trans_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_idtrans'] = $idtrans ;
			  $tampil_detail = $this->load->view('v_slsstock_wrs/v_table_wrs',$data,true);			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end 
	
 public  function do_save()
 {
        $this->form_validation->set_rules('txtvinmod','txtvinmod','required');
		$this->form_validation->set_rules('txtwrsdatemod','txtwrsdatemod','required');		
		$this->form_validation->set_rules('txtstocmod','txtstocmod','required');
		
		if($this->form_validation->run()==FALSE) :		
		   echo $this->session->set_flashdata('pesan','Insert WRS Failed some data must be required!!');
		   redirect('c_slssell_wrs/c_slsssell_wrs'); 	
		else :   
			if ($this->M_slssell_wrs->save_data()) :	 	
			    $this->session->set_flashdata('pesan_succes','Insert WRS Succesfully');			
				redirect('c_slssell_wrs/c_slsssell_wrs');   
			else :
				$this->session->set_flashdata('pesan','Insert Vendor Failed');		
				redirect('c_slssell_wrs/c_slsssell_wrs');   
			endif;
		endif;				
 } 
 
 public  function do_save_real_wrs()
 {
        $this->form_validation->set_rules('txtvinmod_sel','txtvinmod_sel','required');
		$this->form_validation->set_rules('txtidtrans','txtidtrans','required');		
		$this->form_validation->set_rules('txtwrsdatemod_sell','txtwrsdatemod_sell','required');
		
		if($this->form_validation->run()==FALSE) :		
		   echo $this->session->set_flashdata('pesan','Insert Real WRS Failed some data must be required!!');
		   redirect('c_slssell_wrs/c_slsssell_wrs'); 	
		else :   
			if ($this->M_slssell_wrs->save_edit_data_real_wrs()) :	 	
			    $this->session->set_flashdata('pesan_succes','Insert WRS Succesfully');			
				redirect('c_slssell_wrs/c_slsssell_wrs');   
			else :
				$this->session->set_flashdata('pesan','Insert Vendor Failed');		
				redirect('c_slssell_wrs/c_slsssell_wrs');   
			endif;
		endif;	
			

 } 
 
  public  function do_update()
  {
	    $this->form_validation->set_rules('txtvinmod','txtvinmod','required');
		$this->form_validation->set_rules('txtwrsdatemod','txtwrsdatemod','required');		
		$this->form_validation->set_rules('txtstocmod','txtstocmod','required');
		
		if($this->form_validation->run()==FALSE):			
		   echo $this->session->set_flashdata('pesan','Update Failed some data must be required!!');
		   redirect('c_slssell_wrs/c_slsssell_wrs');
		else :   		 
			if ($this->M_slssell_wrs->edit_data()):				 	
				$this->session->set_flashdata('pesan_succes','Update WRS Succesfully');			
				redirect('c_slssell_wrs/c_slsssell_wrs');
			else :
				$this->session->set_flashdata('pesan','Update WRS Failed');		
				redirect('c_slssell_wrs/c_slsssell_wrs');   
			endif;
		endif;
  } 
 
 public function do_delete()
 {
	 if ($this->M_slssell_wrs->delete_data()):
		$this->session->set_flashdata('pesan','Delete Vendor Succesfully');			
		redirect('c_slssell_wrs/c_slsssell_wrs');
	 else:
		$this->session->set_flashdata('pesan','Update Vendor Failed');		
		redirect('c_slssell_wrs/c_slsssell_wrs'); 
	 endif; 
 }
 
 public function do_send_ready_stock()
 {
	 if ($this->M_slssell_wrs->send_ready_stock()):
		$this->session->set_flashdata('pesan_succes','Send Ready stock from WRS to Sellling Succesfully');			
		redirect('c_slssell_wrs/c_slsssell_wrs');
	 else:
		$this->session->set_flashdata('pesan_succes',' Failed');		
		redirect('c_slssell_wrs/c_slsssell_wrs'); 
	 endif; 	 
 }
 
 
 public function do_search()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_slssell_wrs->search_data(); // manggil hasil cari di model 
			 	
		if($tampung_cari == null) :
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;
		   $data['intno'] = ""; //variable buat looping no table.						
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;			   		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['sell_view'] =$tampung_cari;
		   $data['pesan'] = 'Vendor Data Not a found'; 		 		  
		   $data['show_view'] = 'v_slsstock_wrs/v_table_wrs';			
		   $this->load->view('template_admin',$data);					
		else:		
		    $dept  = $this->session->userdata('dept') ;	
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;		
			$data['intno'] = ""; //variable buat looping no table.				
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		    $data['intno'] = ""; //variable buat looping no table.
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['sell_view'] =$tampung_cari;			   			
			$data['show_view'] = 'v_slsstock_wrs/v_table_wrs';		
		    $this->load->view('template_admin',$data);		
		endif;   
    }

 public function multiple_submit()
 {
	 if ($this->input->post('btnsave')) {
		 $this->do_save();
	 }else{
		  if ($this->input->post('btnsave_real')) {
			  $this->do_save_real_wrs();
		  }else{
				 if ($this->input->post('btncari')) {
					 $this->do_search();
				 }else{
					 if ($this->input->post('btndel')) {
						 $this->do_delete();
					 }else{
						  if ($this->input->post('btnupdate')) {
							  $this->do_update();
						  }else{
							   if ($this->input->post('btnsendready')) {
								   $this->do_send_ready_stock();
							   }else{
									redirect('c_slssell_wrs/c_slsssell_wrs');
							   }
						  }			
					 }
			  }
		 }
	 }
		 
 }
 
 //auto complet vin engine-----------------------------
   public function suggest_vin_engine()//jquery
   {
						
		$hasil_model_rows = $this->M_slssell_wrs->get_data_novin();																		
		$json_array = array();
		foreach ($hasil_model_rows as $row)				
		$json_array[]= array("value" => $row->vin, 
							  "label" => $row->vin." | ".$row->engine." | ".$row->desc_type,	
							  "txtnostock"=> $row->stock_no,				 					
							  "txtengine"=> $row->engine,
							  "txtmodel"=> $row->desc_model,
							  "txttype"=> $row->desc_type,
							  "txtwarna"=> $row->colour,
							  "txtlokasi"=> $row->location_stock,
							  "curr"=> $row->currency, 
							  "price_unit"=> number_format($row->price_sell,2),	
							  "result"=> number_format($row->price_sell,2),					 					
					          );					   
		echo json_encode($json_array);
	}
	
	public function get_vin_engine_full()//jquery
    {
						
		$result_sellprice = $this->M_slssell_wrs->get_data_novin_full();//from model					
			foreach ($result_sellprice as $row_full):
				$data['txtvin'] = $row_full->vin; 		 
			    $data['txtnostock'] = $row_full->stock_no; 													
				$data['txtengine']= $row_full->engine;
				$data['txtmodel']= $row_full->desc_model;
				$data['txttype']= $row_full->desc_type;
				$data['txtwarna']= $row_full->colour;
				$data['txtlokasi']= $row_full->location_stock;
				$data['curr']= $row_full->currency;
				$data['price_unit']= number_format($row_full->price_sell,2);	
				$data['result']= number_format($row_full->price_sell,2);			
			endforeach;	
			
		echo json_encode($data);
	}
	
	public function get_edit_wrs()//jquery
    {
						
		$result_sellprice = $this->M_slssell_wrs->get_edit_wrs_id();//from model					
			foreach ($result_sellprice as $row_full):			   
			    $data['txtidwrs']= $row_full->id_wrs;		
				$data['txtvin'] = $row_full->vin; 		 
			    $data['txtnostock'] = $row_full->stock_no; 													
				$data['txtengine']= $row_full->engine;
				$data['txtmodel']= $row_full->desc_model;
				$data['txttype']= $row_full->desc_type;
				$data['txtwarna']= $row_full->colour;
				$data['txtlokasi']= $row_full->location_stock;				
				$data['txtdatewrs']= date('d-m-Y', strtotime($row_full->date_wrs_bodong));	
			endforeach;	
			
		echo json_encode($data);
	}
//end -----------------------------	



//auto complet vin engine from table salelling for real WRS-----------------------------
   public function suggest_vin_engine_sell()//jquery
   {
						
		$hasil_model_rows = $this->M_slssell_wrs->get_data_novin_from_sell();	
																					
		$json_array = array();
		foreach ($hasil_model_rows as $row_sell)				
							
		$json_array[]= array("value" => $row_sell->vin, 
							  "label" => $row_sell->vin." | ".$row_sell->engine." | ".$row_sell->desc_type,	
							  "txtidtrans"=>  $row_sell->id_trans_sales,
							 // "txtdatewrs"=> $strbuffdate,
							  "txtengine"=> $row_sell->engine,
							  "txtmodel"=> $row_sell->desc_model,
							  "txttype"=> $row_sell->desc_type,
							  "txtwarna"=> $row_sell->colour,							 							
					          );					   
		echo json_encode($json_array);
	}
	
	public function get_vin_engine_full_sell()//jquery
    {
						
		$result_sellprice = $this->M_slssell_wrs->get_data_novin_from_sell_full();//from model					
			foreach ($result_sellprice as $row_full):
			    $data_sell['txtidtrans'] = $row_full->id_trans_sales;
				$data_sell['txtvin'] = $row_full->vin; 		 													
				
				$data_sell['txtengine']= $row_full->engine;
				$data_sell['txtmodel']= $row_full->desc_model;
				$data_sell['txttype']= $row_full->desc_type;
				$data_sell['txtwarna']= $row_full->colour;													
			endforeach;	
			
		echo json_encode($data_sell);
	}
 
	
}