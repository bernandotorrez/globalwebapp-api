<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_type_vehicle extends MY_Controller
{  
  public function __construct()
	{
		parent::__construct();					
	    $this->load->model('m_type_vehicle/M_type_vehicle','',TRUE);	
		$this->load->library('form_validation');	   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();							
	}		  	
		
  public function index()
  {					    																
	$this->show_table(); //manggil fungsi show_table		   				
  }	
  
  public function show_table()
  {             					
	  $tampil_table_type= $this->M_type_vehicle->tampil_type()->result();		  
	  $total_rows =$this->M_type_vehicle->tampil_type()->num_rows();
	  
	  $tampil_table_model= $this->M_type_vehicle->tampil_model()->result();	
	  $tampil_table_curr= $this->M_type_vehicle->tampil_currency()->result();	
		
	  									
	  if ($tampil_table_type)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(5);
		$per_page =10;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'mazda_stock_sales/c_type_vehicle/c_type_vehicle/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 5;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['type_view'] =$this->M_type_vehicle->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
				
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;		
		$data['intno'] = ""; //variable buat looping no table.				
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch ;	
		$data['ceck_row'] = $total_rows;														
		$data['intno'] = ""; //variable buat looping no table.														
		$data['tampil_model'] = $tampil_table_model;
		$data['tampil_curr'] = $tampil_table_curr;
		$data['show_view'] = 'v_type_vehicle/v_type_vehicle';		
		$this->load->view('dashboard/Template',$data);				
	  }else{		   
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;	
	    $data['ceck_row'] = $total_rows;	
		$data['tampil_model'] = $tampil_table_model;
		$data['tampil_curr'] = $tampil_table_curr;
		$data['pesan'] = 'Data Vendor table is empty';		
		$data['show_view'] = 'v_type_vehicle/v_type_vehicle';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }
 
 public  function do_save()
 {
        $this->form_validation->set_rules('cbomodel','cbomodel','required');
	    $this->form_validation->set_rules('txttype','txttype','required');		
	    $this->form_validation->set_rules('cbocurr','cbocurr','required');
		$this->form_validation->set_rules('txtprice','txtprice','required');
		$this->form_validation->set_rules('txtcost','txtcost','required');
		
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan','Insert Failed  some data must be required!!');
		   redirect('mazda_stock_sales/c_type_vehicle/c_type_vehicle'); 	
		}else{   		 
			if ($this->M_type_vehicle->save_data())
			{	 	
				$this->session->set_flashdata('pesan_succes','Insert Type Vehicle Succesfully');			
				redirect('mazda_stock_sales/c_type_vehicle/c_type_vehicle');   
			}else{
				$this->session->set_flashdata('pesan','Insert Vendor Failed');		
				redirect('mazda_stock_sales/c_type_vehicle/c_type_vehicle');   
			}
		}
 } 
 
  public  function do_update()
 {
	    $this->form_validation->set_rules('cbomodel','cbomodel','required');
	    $this->form_validation->set_rules('txttype','txttype','required');		
	    $this->form_validation->set_rules('cbocurr','cbocurr','required');
		$this->form_validation->set_rules('txtprice','txtprice','required');
		$this->form_validation->set_rules('txtcost','txtcost','required');
		
		if($this->form_validation->run()==FALSE){			
		   echo $this->session->set_flashdata('pesan','Update Failed some data must be required!!');
		   redirect('mazda_stock_sales/c_type_vehicle/c_type_vehicle'); 	
		}else{   		 
			if ($this->M_type_vehicle->edit_data())
			{	 	
				$this->session->set_flashdata('pesan_succes','Type Vehicle Succesfully');			
				redirect('mazda_stock_sales/c_type_vehicle/c_type_vehicle');  
			}else{
				$this->session->set_flashdata('pesan','Update Type Vehicle failed');		
				redirect('mazda_stock_sales/c_type_vehicle/c_type_vehicle');   
			}
		}
 } 
 
 public function do_delete()
 {
	 if ($this->M_type_vehicle->delete_data())
	{	 	
		$this->session->set_flashdata('pesan','Delete Type Vehicle Succesfully');			
		redirect('mazda_stock_sales/c_type_vehicle/c_type_vehicle');    
	}else{
		$this->session->set_flashdata('pesan','Delete Type Vehicle Failed');		
		redirect('mazda_stock_sales/c_type_vehicle/c_type_vehicle');    
	}
 }
 
 public function do_search()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_type_vehicle->search_data(); // manggil hasil cari di model 
		
		$tampil_table_model= $this->M_type_vehicle->tampil_model()->result();	
	    $tampil_table_curr= $this->M_type_vehicle->tampil_currency()->result();	
			 	
		if($tampung_cari == null){
		   $dept  = $this->session->userdata('dept') ;	
		   $branch  = $this->session->userdata('name_branch') ; 
		   $company = $this->session->userdata('short') ;
		   $data['intno'] = ""; //variable buat looping no table.
		   $data['tampil_model'] = $tampil_table_model;
		   $data['tampil_curr'] = $tampil_table_curr;						
		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;			   		   $data['ceck_row'] = "0"; // jika tidak ketemu	
		   $data['type_view'] =$tampung_cari;
		   $data['pesan'] = 'Vendor Data Not a found'; 		 		  
		   $data['show_view'] = 'v_type_vehicle/v_type_vehicle';			
		   $this->load->view('dashboard/Template',$data);					
		}else{			
		    $dept  = $this->session->userdata('dept') ;	
		    $branch  = $this->session->userdata('name_branch') ; 
		    $company = $this->session->userdata('short') ;		
			$data['intno'] = ""; //variable buat looping no table.				
		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;	
		    $data['intno'] = ""; //variable buat looping no table.
			$data['tampil_model'] = $tampil_table_model;
		    $data['tampil_curr'] = $tampil_table_curr;
			$data['ceck_row'] = "1"; // jika  ketemu	
			$data['type_view'] =$tampung_cari;			   			
			$data['show_view'] = 'v_type_vehicle/v_type_vehicle';		
		    $this->load->view('dashboard/Template',$data);		
		}	   
    }
 
 public function multiple_submit()
 {
	 if ($this->input->post('btnsave')) {
		 $this->do_save();
	 }else{
		 if ($this->input->post('btncari')) {
			 $this->do_search();
	 	 }else{
			 if ($this->input->post('btndel')) {
			     $this->do_delete();
	 	     }else{
				  if ($this->input->post('btnupdate')) {
			          $this->do_update();
	 	     	  }else{
					  redirect('mazda_stock_sales/c_type_vehicle/c_type_vehicle');
				  }			
			 }
		 }
	 }
		 
 }
 
 //------------------------------------pashing result to modal popup View Edit vendor-------------------
	
public function get_idtype_modal_update() {    	       			
	 $id_type = $this->input->post('id_type',TRUE); //       
	 $query = $this->db->query("select * from qv_complite_type_vehicle where id_type ='".$id_type."'");  	    
	
	 foreach ($query->result() as $row)		
	 {
		$data['id_type'] = $row->id_type;
		$data['id_model']    = $row->id_model;
		$data['desc_model'] = $row->desc_model;
		$data['desc_type'] = $row->desc_type; 
		$data['id_curr'] = $row->id_curr; 
		$data['currency'] = $row->currency; 
		$data['price_cost'] = number_format($row->price_cost,2,'.',','); 
		$data['price_sell'] = number_format($row->price_sell,2,'.',','); 
		$data['fuel'] = $row->fuel; 
		$data['tranmission'] = $row->tranmission; 
		if ($row->man_warnty !=null) :
			$data['man_warnty'] = date('d-m-Y',strtotime($row->man_warnty)); 
	    else:
			$data['man_warnty'] =""; 
		endif;		
		$data['man_mileage'] = $row->man_mileage; 
	 }
	
	 echo json_encode($data); //masukan kedalam jasson jquery untuk menampilkan data	   		  			   		      		
}      					
      						 		
	
}

