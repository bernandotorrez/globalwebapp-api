<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js');?>" ></script>
<script>




// fungsi untik checklist semua otomatis
function CheckAll()
{
    l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = true;		
		}	
		document.getElementById('btnedit').disabled = true;	
		document.getElementById('btndel').disabled = false;	
		document.getElementById('btnsendtab').disabled = true;	
}

function UncheckAll()
{
   l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = false;			 
		}
		document.getElementById('btnedit').disabled = true;
		document.getElementById('btndel').disabled = true;
		document.getElementById('btnsendtab').disabled = true;
}


//--------------------function disbaled enabled button with check book

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_edit').removeAttr('disabled');			
        }
        else{
            $('.btn_edit').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_edit_real').removeAttr('disabled');			
        }
        else{
            $('.btn_edit_real').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_send').removeAttr('disabled');			
        }
        else{
            $('.btn_send').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btn_delete').removeAttr('disabled');			
        }
        else{
            $('.btn_delete').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btn_ready').removeAttr('disabled');			
        }
        else{
            $('.btn_ready').attr('disabled', 'disabled');								
        }
    });
});

//-------------------------------------------------------------------------		 	

//-------------------------------------------------------------------------		 	
  
//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});		  
//set focus ----------------------------  
 
</script>

 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_fail');?>  
        </div>	
<?php } ?>

      
  
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" ><?php echo "Table Unit WRS"." | ".$header ; ?> </div>
   <div class="panel-body">               
   
                                             
 <?php  echo form_open('mazda_stock_sales/c_slssell_wrs/c_slsssell_wrs/multiple_submit','id = form_my');  ?>          
 
 <table  width="100%">
     <tr>
	 <td colspan="3" width="6%">
       <label >Selected : &nbsp; </label>
       </td>
       <td width="9%">
       <select name="cbostatus" id="cbostatus" class="form-control">                                      
             <option value="vin">VIN</option>
             <option value="engine">ENGINE</option>
             <option value="stock_no">STOCK NO</option>
             <option value="csi">CSI</option>             
             <option value="location_stock">LOCATION</option>             
       </select> 
       </td>
       <td>&nbsp;</td>   

	   <td colspan="3">
        <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari" value="<?php echo $this->session->flashdata('cari'); ?>" > 
        </td>

       <td width="22%"  >

	   <button id="btncari" name="btncari" class="btn btn-app btn-primary btn-xs radius-4 caddnew" value="Search" type="submit">
                  <i class="ace-icon fa fa-search bigger-160"></i>
                   Search
                </button>  
       
       	  <!-- <div class="input-group">             
             <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari" value="<?php echo $this->session->flashdata('cari'); ?>" >    
                <div class="input-group-btn">                                      
                 <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF" />              
                </div>   
            </div> -->
         
       </td> 
       <td>&nbsp; </td>    
       <td colspan="4">                        
       <!-- <input id="btnrefresh" name="btnrefresh" type="submit" value="Refresh"  class="btn btn-warning " /> 
       <input id="btnadd" name="btnadd" type="button" value="Add WRS"  class="btn btn-info addnew " />    
        <input id="btnaddreal" name="btnaddreal" type="button" value="Add Real WRS"  class="btn btn-info addnewreal " />           
       <input id="btnedit" name="btnedit" type="button" value="Edit WRS" class="btn btn-success btn_edit" disabled="disabled"/>             
       <input id="btndel" name="btndel" type="submit" value="Delete"  class="btn btn-danger btn_delete" onclick="return confirm('are you sure want to delete this data!')" disabled="disabled"/>
       <input id="btnsendready" name="btnsendready" type="submit" value="Change To Sell Unit"  class="btn btn-primary btn_ready" disabled="disabled"/>
		 -->
	   <button id="btnrefresh" name="btnrefresh" class="btn btn-app btn-warning btn-xs radius-4" value="Refresh" type="submit">
          <i class="ace-icon fa fa-refresh bigger-160"></i>
            Refresh
        </button>

        <button id="btnadd" name="btnadd" class="btn btn-app btn-primary btn-xs radius-4 addnew" value="Add WRS" type="button">
          <i class="ace-icon fa fa-book bigger-160"></i>
            Add New
        </button> 

		<button id="btnaddreal" name="btnaddreal" class="btn btn-app btn-primary btn-xs radius-4 addnewreal" value="Add Real WRS" type="button">
          <i class="ace-icon fa fa-book bigger-160"></i>
            Real
        </button>   

        <button id="btnedit" name="btnedit" class="btn btn-app btn-success btn-xs radius-4 btn_edit" value="Edit WRS" type="button" disabled>
           <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
            Edit
        </button> 

        <button id="btndel" name="btndel" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" value="Delete" type="submit" onclick="return confirm('are you sure want to delete this data!')" disabled>
          <i class="ace-icon fa fa-ban bigger-160"></i>
            Delete
        </button>  

        <button id="btnsendready" name="btnsendready" class="btn btn-app btn-primary btn-xs radius-4 btn_ready" value="Change To Sell Unit" type="submit" disabled>
          <i class="ace-icon fa fa-envelope bigger-160"></i>
            Change
        </button> 

         </td>
      </tr> 
    </table>   
 <br />
 
<div class="row">
 <div class="col-md-12 col-md-offset-0">   
	<?php 
		echo '<table width="100%" class="table-bordered table-condensed"  >';		
		echo '<tr style=" color:#144b79;background:url('.base_url("asset/images/child-panel-bg.png").');font-weight:bold;font-size: 12px; ">';		
		echo '<td width="3%" align="center">WRS Date</td>' ;																
    	echo '<td width="8%"align="center">Stock Number</td>';	
		echo '<td width="9%" align="center">Model</td>' ;
		echo '<td width="9%" align="center">Type</td>' ;
		echo '<td width="3%" align="center">Vin</td>' ;
		echo '<td width="3%" align="center">CSI</td>' ;
		echo '<td width="7%" align="center">Engine</td>' ;	
		echo '<td width="8%" align="center">Colour</td>' ;
		echo '<td width="3%" align="center">Intake</td>' ;
		echo '<td width="3%" align="center">Year</td>' ;
		echo '<td width="8%" align="center">Location</td>' ;																		
					       
		echo '<td width="5%" hidden="true" align="center">Add/Edit Detail</td>' ;					
		echo '<td width="7%"><a href="javascript:void(0)" onclick="CheckAll()" style="text-decoration:none"> All</a>';
		echo ' / ';
		echo '<a href="javascript:void(0)" onclick="UncheckAll()" style="text-decoration:none">Uncheck</a></td>';
		echo '</tr>' ;		    		
		
	if ($ceck_row >= 1) :		  
		foreach($sell_view as $row):
		 
		/* if ($row->status_flag_sell==0) : 	  
			echo '<tr style="font-size:11px;" class="btn-danger">'; 
		 else:		
			echo '<tr style="font-size:11px;">'; 
		 endif; */
		if ($row->status == "0") : 	  
			 echo '<tr style="font-size:11px;" class="btn-success">';  
		 else:				 	 
			  echo '<tr style="font-size:11px;" class="btn-primary">';   		
		 endif;				 
     ?>   
     	
                <td align="center"><?php echo date('d-m-Y', strtotime($row->date_wrs_bodong)) ; ?></td>
                <td width="8%" align="center"><?php echo $row->stock_no; ?></td>          
                <td align="center"><?php echo $row->desc_model ; ?></td>		                       		
                <td align="center"><?php echo $row->desc_type ; ?></td>		
                <td align="center"><?php echo $row->vin ; ?></td>	               
                <td align="center"><?php echo $row->engine ; ?></td>
                 <td align="center"><?php echo $row->csi ; ?></td>
                <td align="center"><?php echo $row->colour ; ?></td>	
                <td align="center"><?php echo $row->intake ; ?></td>
                <td align="center"><?php echo $row->year ; ?></td>	
                <td align="center"><?php echo $row->location_stock ; ?></td>                                               
                <td align="center"> 	
                <?php if ($row->status == "0") :  ?>
                <div align="center">X</div>
                <?php else: ?>
			 	<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value="<?php echo $row->id_wrs ; ?>" />                
				<?php endif; ?>                
    <?php form_close(); ?>             			            					
                </div>
                </td>
		  </tr> 	  
		 <?php 		    
		  endforeach;
 	   endif;
   echo'</table>';	  				
		 ?> 
		 
		<?php		   			
		     if (! empty($pagination))			
			 { 
		  	   echo '<center><table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table></center>';				 
			 }			  	  						
		?>
        
 </div>  
</div> 
      
          
  </div>
 </div>
   
</div>


 <!-- Modal Buat save and edit WRS Bodong -->
    <div id="tampilanform"></div>
        <div class="modal fade" id="Mod-AddEditDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add WRS Unit</h4>
                    </div>
                    
                    <div class="modal-body-add">
                   <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
                   <br />  
                     <table  class=" table-condensed table-bordered" width="100%" style="font-size:12px" >
					
                      <tr bgcolor="#CEDFFF">
					  <td>VIN. : </td><td><input id="txtvinmod" name="txtvinmod" class="form-control"  /> </td>                     
					  </tr>	
                      <tr bgcolor="#FFFF99">
                      <td>WRS DATE. : </td><td><input id="txtwrsdatemod" name="txtwrsdatemod" data-date-format="dd-mm-yyyy" class="form-control datepicker" readonly  /></td>
					  </tr>			
                      
                      <tr>
					  <td>STOCK NO. : </td><td><input id="txtstocmod" name="txtstocmod"  class="form-control " readonly /></td>
					  </tr>
                      
                      <tr hidden="true" >
                        <td >STOCK NO OLD. : </td><td><input id="txtstocnoold" name="txtstocnoold" class="form-control" hidden="true"  /><input id="txtidwrs" name="txtidwrs" class="form-control"   /></td>
                      </tr>  
                      
                      
                       <tr>
					  <td>MODEL. : </td><td><input id="txtmodelmod" name"txtmodelmod" class="form-control" readonly /></td>
					  </tr> 
					 
                      <tr>
					  <td>TYPE. : </td><td><input id="txttypemod" name"txttypemod" class="form-control" readonly /></td>
					  </tr>
					 
                      <tr>
					  <td>COLOUR. :</td><td><input id="txtcolormod" name"txtcolormod" class="form-control" readonly   /></td>
					  </tr>	 					  
					 
                      <tr>
					  <td>ENGINE. : </td><td><input id="txtengmod" name"txtengmod" class="form-control" readonly /></td>
					  </tr>	 
                     
                      <tr>
					  <td>LOCATION STOCK : </td><td><input id="txtlokasimod" name"txtlokasimod" class="form-control" readonly  /></td>
					  </tr>		
					  <tr>					  		
					  </table>	  
                      </div>  		   
                    </div>                    
                     <div class="modal-footer">
                      <br />
                        <input type="submit" name="btnsave" id="btnsave" value="Submit " class="btn btn-primary"/>
                        <input type="submit" name="btnupdate" id="btnupdate" value="Submit Update" class="btn btn-primary edit-wrs"/>                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal Buat edit -->  	




 <!-- Modal Buat save and edit Real WRS -->
    <div id="tampilanform"></div>
        <div class="modal fade" id="Mod-AddEditDetailReal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Real WRS Unit</h4>
                    </div>
                    
                    <div class="modal-body-add">
                   <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
                   <br />  
                     <table  class=" table-condensed table-bordered" width="100%" style="font-size:12px" >
					
                      <tr bgcolor="#CEDFFF">
					  <td>VIN. : </td><td><input id="txtvinmod_sel" name="txtvinmod_sel" class="form-control"  /> </td>                     
					  </tr>	
                      <tr bgcolor="#FFFF99">
                      <td>WRS DATE. : </td><td><input id="txtwrsdatemod_sell" name="txtwrsdatemod_sell" data-date-format="dd-mm-yyyy" class="form-control datepicker" readonly /></td>
					  </tr>			
                      
                      <tr>
					  <td>TRANSAKSI ID. : </td><td><input id="txtidtrans" name="txtidtrans"  class="form-control " readonly /></td>
					  </tr>
                                                                 
                       <tr>
					  <td>MODEL. : </td><td><input id="txtmodelmod_sell" name"txtmodelmod_sell" class="form-control" readonly /></td>
					  </tr> 
					 
                      <tr>
					  <td>TYPE. : </td><td><input id="txttypemod_sell" name"txttypemod_sell" class="form-control" readonly /></td>
					  </tr>
					 
                      <tr>
					  <td>COLOUR. :</td><td><input id="txtcolormod_sell" name"txtcolormod_sell" class="form-control" readonly   /></td>
					  </tr>	 					  
					 
                      <tr>
					  <td>ENGINE. : </td><td><input id="txtengmod_sell" name"txtengmod_sell" class="form-control" readonly /></td>
					  </tr>	 
                                         			  		
					  </table>	  
                      </div>  		   
                    </div>                    
                     <div class="modal-footer">
                      <br />
                        <input type="submit" name="btnsave_real" id="btnsave" value="Submit " class="btn btn-primary"/><button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal Buat edit -->  	

	

<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- jQuery_ui -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->

<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 

<script>
//view detail detail with get id_trans.
$(function(){
  $(document).on('click','.print',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("mazda_stock_sales/c_slssell_wrs/c_crud_wrs/get_add_edit_idtrans_modal"); ?>';		
		$("#ViewDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_trans_sales:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});
</script>

<script>
//get data table to modal popup. add edit
$(function(){
  $(document).on('click','.add_detail',function(e){
		var req_id_add_edit_detail = $(this).attr('req_id_add_edit_detail'); //atribut from <a ref 	php echo $row->id_master 
		var url = '<?php echo site_url("mazda_stock_sales/c_slssell_wrs/c_crud_wrs/get_add_edit_idtrans_modal"); ?>';
		$("#AddEditDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_trans_sales_edit:req_id_add_edit_detail},
			success: function(msg) {
				$('.modal-body-add').html(msg);				
			}
						
		}); 				
		
   });	
});
</script>

<script>
// Show modal pop up for addnew----------
$(function(){ 
	  $(document).on('click','.addnew',function(e){		       		  
			$("#Mod-AddEditDetail").modal('show');						  
			$("#myModalLabel").html("Add WRS Unit");							
			$('#txtvinmod').removeAttr('disabled');						
			$("#txtstocmod").val("")	;	
			$("#txtengmod").val("")	;
			$("#txtvinmod").val("")	;
			$("#txtmodelmod").val("")	;
			$("#txttypemod").val("")	;	
			$("#txtcolormod").val("")	;
			$("#txtwrsdatemod").val("")	;	
			$("#txtlokasimod").val("") ;													  
						
		    $("#row_label").hide();	
			$("#row_input").hide();		
			$("#btnupdate").hide();						
		    $("#btnsave").show();		       												
	  });																						   	 	   
});		

//----------------------------------------clear when hide
/*$('#myModal').on('show.bs.modal', function (e) {
  $(this)
    .find("input,textarea,select")
       .val('')
       .end()
    .find("input[type=checkbox], input[type=radio]")
       .prop("checked", "")
       .end();
}) */
</script>

<script>
// Show modal pop up for addnew_Sell----------
$(function(){ 
	  $(document).on('click','.addnewreal',function(e){		       		  
			$("#Mod-AddEditDetailReal").modal('show');						  
			$("#myModalLabel").html("Add Real WRS Unit");							
			$('#txtvinmod_sel').removeAttr('disabled');								
			$("#txtengmod_sell").val("")	;
			$("#txtvinmod_sel").val("")	;
			$("#txtmodelmod_sell").val("")	;
			$("#txttypemod_sell").val("")	;	
			$("#txtcolormod_sell").val("")	;
			$("#txtwrsdatemod_sell").val("")	;																  			$('#txtidtrans').val("");
						
		    $("#row_label").hide();	
			$("#row_input").hide();		
			$("#btnupdate").hide();						
		    $("#btnsave").show();		       												
	  });																						   	 	   
});		

//----------------------------------------clear when hide
/*$('#myModal').on('show.bs.modal', function (e) {
  $(this)
    .find("input,textarea,select")
       .val('')
       .end()
    .find("input[type=checkbox], input[type=radio]")
       .prop("checked", "")
       .end();
}) */
</script>



<script type="text/javascript">
//---------add new function autocomplite jquery from table stock
$(document).ready(function () {
	$(function () {
		$("#txtvinmod").autocomplete({
			minLength:0,
			delay:0,
			source: function(request, response) {
				
				var str_url = '<?php echo site_url('mazda_stock_sales/c_slssell_wrs/c_slsssell_wrs/suggest_vin_engine'); ?>';					
				var str_novin =$("#txtvinmod").val();
														
				$.ajax({ 
					url: str_url ,												
					data: {txtvinno:str_novin},																																		
					dataType: "json",
					type: "POST",																	
					success: function(data){response(data);																																										
					}						
				});
			},	
				select:function(event, ui){
				   $('#txtstocmod').val(ui.item.txtnostock);
				   $('#txtvinmod').val(ui.item.txtvin);
				   $('#txtengmod').val(ui.item.txtengine);
				   $('#txtmodelmod').val(ui.item.txtmodel);
				   $('#txttypemod').val(ui.item.txttype);
				   $('#txtcolormod').val(ui.item.txtwarna);					 					   					
				   $('#txtlokasimod').val(ui.item.txtlokasi);				 					   						
				}						                  				                     
		});		
			
	});

});	  
</script>


<script type="text/javascript">
// addnew automatic full
var htmlobjek;
$(document).ready(function(){  
	 $("#txtvinmod").change(function(){
			var txtvinno_full = $("#txtvinmod").val();
			var url = '<?php echo site_url('mazda_stock_sales/c_slssell_wrs/c_slsssell_wrs/get_vin_engine_full'); ?>';
			
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{txtvinno:txtvinno_full},	
				cache: false,          
				success: function(data, textStatus, jqXHR){           
				    $('#txtstocmod').val(data.txtnostock);	
				 	$('#txtvinmod').val(data.txtvin);			
					$('#txtengmod').val(data.txtengine);
					$('#txtmodelmod').val(data.txtmodel);
					$('#txttypemod').val(data.txttype);
				    $('#txtcolormod').val(data.txtwarna);
				    $('#txtlokasimod').val(data.txtlokasi);				 
								
				}
			});
		  });
});	  
</script>

<!----- buat modal popup sell real WRS  ------------------------------------------------------->

<script type="text/javascript">
//---------add new function autocomplite jquery from table saling
$(document).ready(function () {
	$(function () {
		$("#txtvinmod_sel").autocomplete({
			minLength:0,
			delay:0,
			source: function(request, response) {
				
				var str_url = '<?php echo site_url('mazda_stock_sales/c_slssell_wrs/c_slsssell_wrs/suggest_vin_engine_sell'); ?>';					
				var str_novin_sel =$("#txtvinmod_sel").val();
														
				$.ajax({ 
					url: str_url ,												
					data: {txtvinno_sell:str_novin_sel},																																		
					dataType: "json",
					type: "POST",																	
					success: function(data){response(data);																																										
					}						
				});
			},	
				select:function(event, ui){
				   $('#txtidtrans').val(ui.item.txtidtrans);
				   $('#txtvinmod_sel').val(ui.item.txtvin);
				  // $('#txtwrsdatemod_sell').val(ui.item.txtdatewrs);		
				   $('#txtengmod_sell').val(ui.item.txtengine);
				   $('#txtmodelmod_sell').val(ui.item.txtmodel);
				   $('#txttypemod_sell').val(ui.item.txttype);
				   $('#txtcolormod_sell').val(ui.item.txtwarna);					 					   									  		 					   						
				}						                  				                     
		});		
			
	});

});	  
</script>

<script type="text/javascript">
// addnew automatic full
var htmlobjek;
$(document).ready(function(){  
	 $("#txtvinmod_sel").change(function(){
			var txtvinno_full_sell = $("#txtvinmod_sel").val();
			var url = '<?php echo site_url('mazda_stock_sales/c_slssell_wrs/c_slsssell_wrs/get_vin_engine_full_sell'); ?>';
			
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{txtvinno_sell:txtvinno_full_sell},	
				cache: false,          
				success: function(data, textStatus, jqXHR){           
				    $('#txtidtrans').val(data.txtidtrans);	
				 	$('#txtvinmod_sell').val(data.txtvin);
					//$('#txtwrsdatemod_sell').val(data.txtdatewrs);								
					$('#txtengmod_sell').val(data.txtengine);
					$('#txtmodelmod_sell').val(data.txtmodel);
					$('#txttypemod_sell').val(data.txttype);
				    $('#txtcolormod_sell').val(data.txtwarna);				 		 
								
				}
			});
		  });
});	  
</script>
 <!----- end------------------------------------------------>
 
 
 
 <!----- auto complit efrom stock table------------------------------------------------>
<script type="text/javascript">
//--------- Update function autocomplite jquery
$(document).ready(function () {
	$(function () {
		$("#txtvinmodupdate").autocomplete({
			minLength:0,
			delay:0,
			source: function(request, response) {
				
				var str_url = '<?php echo site_url('mazda_stock_sales/c_slssell_wrs/c_slsssell_wrs/suggest_vin_engine'); ?>';					
				var str_novin =$("#txtvinmodupdate").val();
														
				$.ajax({ 
					url: str_url ,												
					data: {txtvinno:str_novin},																																		
					dataType: "json",
					type: "POST",																	
					success: function(data){response(data);																																										
					}						
				});
			},	
				select:function(event, ui){
				   $('#txtstocmod').val(ui.item.txtnostock);
				   $('#txtvinmod').val(ui.item.txtvin);
				   $('#txtvinmodupdate').val(ui.item.txtvin);
				   $('#txtengmod').val(ui.item.txtengine);
				   $('#txtmodelmod').val(ui.item.txtmodel);
				   $('#txttypemod').val(ui.item.txttype);
				   $('#txtcolormod').val(ui.item.txtwarna);					 					   					
				   $('#txtlokasimod').val(ui.item.txtlokasi);				 					   						
				}						                  				                     
		});		
			
	});

});	  
</script>
<!----- end------------------------------------------------>


<!-----get engine for edit------------------------------------------------>
<script type="text/javascript">
//update automatic full
var htmlobjek;
$(document).ready(function(){  
	 $("#txtvinmodupdate").change(function(){
			var txtvinno_full = $("#txtvinmodupdate").val();
			var url = '<?php echo site_url('mazda_stock_sales/c_slssell_wrs/c_slsssell_wrs/get_vin_engine_full'); ?>';
			
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{txtvinno:txtvinno_full},	
				cache: false,          
				success: function(data, textStatus, jqXHR){           
				    $('#txtstocmod').val(data.txtnostock);	
				 	$('#txtstocnoupdate').val(data.txtnostock);			
					$('#txtengmod').val(data.txtengine);
					$('#txtmodelmod').val(data.txtmodel);
					$('#txttypemod').val(data.txttype);
				    $('#txtcolormod').val(data.txtwarna);
				    $('#txtlokasimod').val(data.txtlokasi);				 
								
				}
			});
		  });
});	  
</script>
 <!----- end------------------------------------------------>
 
 
 
 
<script>
 // Show modal pop up for edit and then get data table edit wrs to modal popup
$(function(){
  $(document).on('click','.btn_edit',function(e){
	
		var req_id = $('input[type="checkbox"]:checked').val()
		var url = '<?php echo site_url('mazda_stock_sales/c_slssell_wrs/c_slsssell_wrs/get_edit_wrs'); ?>';
					
		$("#Mod-AddEditDetail").modal('show');  	
		$("#myModalLabel").html("Edit WRS");			
						
		$.ajax({			
			type:'POST',
			url: url,
			dataType: "json",
			data: {txtidwrs:req_id}, 
			success: function(data, textStatus, jqXHR ) {				    			
			    $("#row_label").show();	
			    $("#row_input").show();	
				$("#btnsave").hide();
				$("#btnupdate").show();								  
				$('#txtstocmod').val(data.txtnostock);	
				$('#txtvinmod').val(data.txtvin);			
				$('#txtengmod').val(data.txtengine);
				$('#txtmodelmod').val(data.txtmodel);
				$('#txttypemod').val(data.txttype);
				$('#txtcolormod').val(data.txtwarna);
				$('#txtlokasimod').val(data.txtlokasi);				 		
				$('#txtwrsdatemod').val(data.txtdatewrs);	
				$('#txtstocnoold').val(data.txtnostock);
				$('#txtidwrs').val(data.txtidwrs);
				//$('#txtstocnoupdate').show('fast');
               // $('#txtvinmod').attr('disabled', 'disabled');	
						
			}
						
		});				
		
   });	
});	
</script>


<script>
//post-----------------------------------------
$(function(){
	//jquery untuk post submit save / edit
    $('#myform').on('submit', function(e){ //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
        e.preventDefault();		
		var url = '<?php  echo base_url('mazda_stock_sales/c_slssell_wrs/c_slsssell_wrs/multiple_submit');  ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
            success: function(data){              
            }
        });
    });
});
</script>

<?php if ($this->session->userdata('id_group') != 'admin001'): ?>
   <script>
	$(document).ready(function() { 	
		$('#btndel').hide();		
	});
	</script>
<?php endif; ?>