<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_fail') ;?>  
        </div>	
<?php } ?>




<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">                 
<div class="panel panel-default ">
  <div class="panel-heading" id="label_head_panel" style="color:#B66F03; font-size:18px;">Selling Unit</div>                 
     <?php  echo form_open_multipart('c_slssell_unit/c_crud_sell/do_edit_sell',array('class'=>'form-multi'));  ?> 
  
       <table class="table "  >      
         <?php
			  if ($check_row >= 1) :  
				 foreach($tampil_edit_sell as $row_sell) :
	          ?>    
           <tr> 
              <td> 
               <div class="form-group">
                   <label for="Company"  class="col-xs-3">Company</label>
                    <div class="col-xs-6">
                    <input type="text" id="txtcom" name="txtcom" class="form-control" readonly="readonly" value="<?php echo $this->session->userdata('company') ;?>" />
                    </div>
                </div> 
               </td> 
               <td>   
        		  <div class="form-group">
                   <label for="Transaction No"  class="col-xs-3">Transaction No</label>
                    <div class="col-xs-6">
                    
                    <input type="text" id="txtnotrans" name="txtnotrans" class="form-control" readonly="readonly" value="<?php echo $row_sell->id_trans_sales ; ?>"   />
                   
                    </div>
                </div>
              </td>
           </tr>
         <tr> 
            <td>  
              <div class="form-group">
                <label for="PP Number"  class="col-xs-3">Branch</label>
                  <div class="col-xs-6">
                    <input type="text" id="txtbranch" name="txtbranch" class="form-control" readonly="readonly" value="<?php echo $this->session->userdata('name_branch') ;?>" />
                  </div>
              </div> 
             </td> 
             <td>   
        	     
           	   <div class="form-group">
            <label for="Date Recieve"  class="col-xs-3" >User Created</label>
           
<div class="col-xs-7">
                 <input type="text" id="txtusersell" name="txtusersell"   class="form-control " readonly="readonly" value="<?php echo $this->session->userdata('name'); ?>">
            </div>
        </div>	
            </td>
           </tr>
         <tr> 
             <td>  
               
                 <div class="form-group">
                  <label for="Company"  class="col-xs-3">Engine</label>
                    <div class="col-xs-6">
                     <input type="text" id="txtengine" name="txtengine" class="form-control" readonly value="<?php echo $row_sell->engine ; ?>" />
                    </div>
                </div>     
             </td>              
             <td>   
        		<div class="form-group">
               <label for="Date Submission" style="color:#A6090D"  class="col-xs-3">VIN / Engine *</label>
                <div class="col-xs-7">
                
                 <input type="text" id="txtvinno" name="txtvinno" class="form-control" value="<?php echo $row_sell->vin ; ?>"  />                
                  <input name="txtnostock" type="text" id="txtnostock" size="30" value="<?php echo $row_sell->stock_no ; ?>"    hidden="true" />  
                   <input name="txtnostockbuff" type="text" id="txtnostockbuff" size="30" value="<?php echo $row_sell->stock_no ; ?>"  hidden="true"   />  
                </div>
              </div>  
            </td>                    		
        </tr>
        
           <tr>
              <td rowspan="2" >   
                <div class="form-group">
                <label for="Model Unit"  class="col-xs-3">Model Unit</label>
                <div class="col-xs-7">
                 <input type="text" id="txtmodel" name="txtmodel" class="form-control"  readonly value="<?php echo $row_sell->desc_model ; ?>"   />
                </div>
            </div>         
             </td>  
              <td>   
               <div class="form-group">
                   <label for="SPK No"  class="col-xs-3" >SPK No Current</label>
                   <div class="col-xs-4">
                    <input type="text" id="txtspk" name="txtspk" class="form-control" value="<?php echo $row_sell->nospk ; ?>" readonly="readonly" />                                      
                    </div>
                </div>                               	   
             </td> 
             <tr>   
             <td>
              <div class="form-group">
               <label for="SPK No"  class="col-xs-3" style="color:#A6090D">SPK No Edit*</label>
                <div class="col-xs-4">
                   <input type="text" id="txtnospkupdate" name="txtnospkupdate" class="form-control" value="<?php echo $row_sell->nospk ; ?>"    />
                   </div>
                 </div>            
             </td>                     
             </tr>
           </tr>             
             <tr>   
               <td>
				<div class="form-group">
                <label for="Branch"  class="col-xs-3">Type Unit</label>
                <div class="col-xs-6">
                 <input type="text" id="txttype" name="txttype" class="form-control"  readonly value="<?php echo $row_sell->desc_type ; ?>"  />
                </div>
            </div>    
               </td>
                <td>
          			  <div class="form-group">
            <label for="Date Recieve"  class="col-xs-3" style="color:#A6090D">Date Alocation*</label>
           
                <div class="col-xs-6">
                 <input type="text" id="txtdatealoc" name="txtdatealoc"  data-date-format="dd-mm-yyyy" class="form-control datepicker" readonly="readonly"  value="<?php  echo date('d-m-Y', strtotime($row_sell->date_aloc)) ; ?>" >
            </div>
        </div>
              </td>         
           </tr>             
         <tr>   
         	<td>
            	   <div class="form-group">
                <label for="colour"  class="col-xs-3">Colour</label>
                <div class="col-xs-6">
                 <input type="text" id="txtwarna" name="txtwarna" class="form-control" readonly  value="<?php echo $row_sell->colour ; ?>" />
                </div>
            </div>    
           </td>
            <td>
               		     <div class="form-group">
            <label for="WRS Date"  class="col-xs-3" style="color:#A6090D"> WRS Date *</label>
           
<div class="col-xs-6">
                 <input type="text" id="txtwrsdate" name="txtwrsdate"  data-date-format="dd-mm-yyyy" class="form-control datepicker" readonly="readonly"  value="<?php  echo date('d-m-Y', strtotime($row_sell->wrs_date)) ; ?>" >
            </div>
        </div>               
           </td>                                                
         </tr>           
          <tr>   
         	<td>
                 <div class="form-group">
                   <label for="Location Stock"  class="col-xs-3">Location Stock</label>
                   <div class="col-xs-5">
                     <input type="text" id="txtlokasi" name="txtlokasi" class="form-control"   readonly="readonly" value="<?php echo $row_sell->location_stock ; ?>" />
                    </div>
                </div>
           </td> 
           <td>
                      <div class="form-group">
                        <label for="Date Recieve"  class="col-xs-3" style="color:#A6090D">Customer *</label>
                        <div class="col-xs-7">
                         <input name="txtcust" type="text" class="form-control" id="txtcust" placeholder="Customer"  maxlength="40" value="<?php echo $row_sell->cust_name ; ?>"  />                           <input name="txtidcust" type="text" id="txtidcust" size="20"  value="<?php echo $row_sell->id_cust ; ?>"  hidden="true"/>                     
                   </div>
            </div>                     
            </td>                                                 
           </tr>                        
   	     <tr>   
     		 <td>  
                  <div class="form-group">
           <label for="Unit Price" style="color:#900" class="col-xs-3">Currency </label>   
           <div class="col-xs-3">                 
 		   <input  maxlength="20" id="curr" name="curr" class="form-control" readonly="readonly" value="<?php echo $row_sell->curr_sell ; ?>"  />
           </div>
         </div>  
             </td>    
              <td>
     			 <div class="form-group">
           <label for="Sales Name"  class="col-xs-3" style="color:#A6090D">Sales*</label>
            <div class="col-xs-7">
             <select id="cbosales" name="cbosales"  class="form-control">
              <option value="<?php echo $row_sell->id_sales ; ?>"> <?php echo $row_sell->sal_name ; ?></option>
	             <?php foreach($tampil_sales as $row_sal){ ?>               
               		 <option value="<?php echo $row_sal->id_sales ;?>"><?php echo $row_sal->sal_name ;?></option>
                 <?php } ?>  
               </select>
                </div>
            </div>             
              </td>                                               
     </tr>         
     <tr>
    	<td>
        <div class="form-group">
           <label for="Unit Price" style="color:#900" class="col-xs-3">Price Unit </label>
           <div class="col-xs-7">
           <input  maxlength="70" id="price_unit" name="price_unit" class="form-control unit_price" readonly="readonly" style="color:#A6090D"  placeholder="0.00" value="<?php echo  number_format($row_sell->price_sell ,2,'.',',');  ?>" />   
           </div>   
        </div>
      
      
       </td> 
         <td>
        	  <div class="form-group">
           <label for="Lama SPK"  class="col-xs-3" style="color:#A6090D">Lama SPK *</label>
           <div class="col-xs-3">
             <input name="txtlamaspk" type="text" class="form-control" id="txtlamaspk" maxlength="4" value="<?php echo $row_sell->lama_spk ; ?>" />
            </div>
        </div> 	
         </td>    
    </tr>
    <tr>
      <td>
           <div class="form-group">
          <label for="Discount" class="col-xs-3">Discount </label>
           <div class="col-xs-7">
         <input  maxlength="70" id="txtdixc" name="txtdixc" class="discount form-control"  style="color:#A6090D"  placeholder="0.00" value="<?php echo  number_format($row_sell->discount ,2,'.',','); ?>"  onkeypress="return isNumberKey(event)"  />
           </div>
         </div>  
       
      </td>   
       <td  >
        		 <div class="form-group">
         			 <label for="Remarks"  class="col-xs-3">Remarks</label>
         			<div class="col-xs-5">
          			<textarea name="txtnote" class="form-control" id="txtnote"><?php echo $row_sell->remarks; ?></textarea>
                    </div>
                 </div>    
         </td>      	     
    </tr>
      <tr>
       <td>
   			  
       <div class="form-group">
           <label for="Discount" style="color:#900" class="col-xs-3">Payment + Disc </label>
           <div class="col-xs-7">
            <input  maxlength="70" id="result" name="result" class="amount form-control" readonly="readonly" style="color:#A6090D" placeholder="0.00" value="<?php echo  number_format($row_sell->payment_plus_discount ,2,'.',','); ?>"  />      
           </div>
         </div>  
       </td>
       <td>
       	 <div class="form-group">
          <label for="Discount"  style="color:#A6090D" class="col-xs-3">Payment Status :</label>
           <div class="col-xs-5">
         <select id="cbopaystatus" name="cbopaystatus"  class="form-control">
               <?php 
			     if ($row_sell->flag_payment_status == '1') :
					echo '<option value="1">'.'Customer'.'</option>'	 ;   
			   	 else:
				    echo '<option value="2">'.'Leasing'.'</option>';	
				endif;	
			   ?>
               <option value="1">Customer</option>
               <option value="2">Leasing</option>               
               </select>
                </div>
           </div>
       </td>   
     </tr>
      <?php endforeach ; ?> 				 			 
  <?php endif ;?> 	
 </table>


<div class="panel panel-default ">
  	
  <div class="panel-heading" id="label_head_panel"> 
        <span style="color:#A6090D; font-weight:bold; ">       
         Total Payment :    
       </span>             
      	                    
       <input  maxlength="70" id="txt_tot_pay" name="txt_tot_pay" class="tot_pay " readonly="readonly" style="color:#A6090D" placeholder="0.00" value="<?php echo number_format($row_sell->total_paymet_customer ,2,'.',',');  ?>"  /> 
  
  
       <span style="color:#A6090D; font-weight:bold; ">       
         Remaining Payment :    
       </span>             
      	                    
       <input  maxlength="70" id="txt_remain" name="txt_remain" class="r_payment " readonly="readonly" style="color:#A6090D" placeholder="0.00" value="<?php echo number_format($row_sell->remaining_amount_customer ,2,'.',',');  ?>"  /> 
         
                            
    </div>
   <div class="panel-body">                   
             <div class="col-md-12 col-md-offset-0">  
                <table class="table-striped table-bordered">
                
                     <thead align="center" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);font-weight:bold;font-size: 14px">
                
                      		<td width="2%" >No</td>
                            <td width="35%"  style="color:#A6090D;" align="center">Payment Description *</td>
                            <td width="15%"  style="color:#A6090D;" >Date *</td>
                            <td width="25%" style="color:#A6090D;">DP Or Payment Customer *</td>                         
                            <td align="center"  style="color:#A6090D;" width="5%" >Upload Transfer *</td>
                        <td  style="color:#A6090D;">Evidence</td>    
                        </thead>    
                        
                  <tbody class="detail">   
                    <?php
				 if ($check_row >= 1) :  
					 foreach($tampil_detail_sell as $row_det)  : $intno = $intno +1;
	          	 ?>                        
                      <tr>
                         <td class="no" align="center"><?php echo $intno ?></td>                                            
                        <td>
                      
                        <input type="text" maxlength="70" id="txtdescpay"  name="txtdescpay"  placeholder="Description" size="24" class="form-control txtdescpay" value="<?php echo $row_det->desc_det ; ?>" readonly="readonly"  />
                        </td>
                        <td>
                          <input type="text" maxlength="70"  name="txtdatedet"  placeholder="Date Payment" size="30" class="form-control"   data-date-format="dd-mm-yyyy" readonly="readonly" value="<?php  echo date('d-m-Y', strtotime($row_det->pay_date)) ; ?>"  />
                        </td>                     
                      
                        <td>
                         <input maxlength="14"  name="txtdppay" type="text" placeholder="0.00" size="20" onkeypress="return isNumberKey(event)" class="form-control price" style="color:#A6090D"   id="txtdatedet"value="<?php echo  number_format($row_det->amount ,2,'.',','); ?>" readonly="readonly"  /> 
                        </td>                      
                        <td>                        
                         <input type="file" id="txtupload" name="userfile" class="btn btn-success uploadfoto" disabled="disabled" />   
                         <input type="text" name"txtaddresedit" id="txtaddresedit" value="<?php echo $row_det->attach_trans ?>" hidden="true"  />                    
                        </td>  
                        <td align="center">
                          <?php
						   echo anchor_popup($row_det->attach_trans,"<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".'View'.'</a>'); 
                        ?>
                        </td>
                     </tr>
                    </tbody> 
                      <?php  endforeach; ?>   
               <?php endif; ?>
                 </table>                                                                                                                                
               </div>   
                
                <div  class="col-xs-4 col-xs-offset-0 ">
                    <br />
                   <table>
                      <tr>
                          <td> 
                    <input id="btnsave" name="btnsave" type="submit" value="save"  class="btn btn-danger " />     
                    </td>
                    <td>&nbsp;</td>                   
                    <td>&nbsp;  </td>
                    <td>                                      			
                    <a href="<?php echo base_url('c_slssell_unit/c_slsssell');?>" style="text-decoration:none;"><input id="btnback" name="btnback" type="button" value="Back To Table" class="btn btn-warning" /> </a>	                     </td>
                  </tr>
               
               </table>                        
               </div>                                                   	                     
      </div>                                                                                         				                         
 </div> 
 		
		<?php form_close(); ?> 
      
    </div> <!-- /panel -->             
</div>  <!-- /div class="col-lg-12 -->   
 
 <?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>    


<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- jQuery_ui -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->

<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 


<script type="text/javascript">
//--------------function number only
 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	

//----------------set focus----
 $(function() {
  $("#txtpayfrom").focus();    
});	

</script>


<script script type="text/javascript">

//calculate and add field--------------
$(function(){		
	
	 
	 $('body').delegate('.discount','change',function(){
		   var tr =  $(this).parent().parent();			 
		   var unit_price= numeral().unformat($('.unit_price').val());			 	      
		   var discount=numeral().unformat($('.discount').val());
		   var price = numeral().unformat(tr.find('.price').val());	
				   
		   var t=0;
		   var jmlbaris=parseInt($('.price').length);
		   		  		   
		   for(var i=0;i < jmlbaris;++i){
			   t+=parseFloat(numeral().unformat($('.price').eq(i).val()));
		   }	
		   			
		   $('.tot_pay').val(t);							     		   		  		   		  
		   var hasil_payment_total = discount + t ;
		   var hasil_r_payment = unit_price - hasil_payment_total	;		
		   
		  
		   $('.r_payment').val(hasil_r_payment);
		   $('.amount').val(hasil_payment_total);
 	 });
	 
	$('body').delegate('.discount','blur',function(){  		   
		   var discount=$('.discount').val();
		   var tot_pay=$('.tot_pay').val();
		   var r_payment=$('.r_payment').val();
		   var amount=$('.amount').val();
		   
		   $('.discount').val(numeral(discount).format('0,0.00')) ;
		   $('.tot_pay').val(numeral(tot_pay).format('0,0.00'));
		   $('.r_payment').val(numeral(r_payment).format('0,0.00'));
		   $('.amount').val(numeral(amount).format('0,0.00')) ;		
 	 });  	
	 
	/* $('body').delegate('.price','change',function(){  	
	       var tr =  $(this).parent().parent();			 
		   var price = numeral().unformat(tr.find('.price').val());	
		   var discount=numeral().unformat($('.discount').val());
		   var unit_price = numeral().unformat($('.unit_price').val());
		   
		   
		   var t=0;
		   var jmlbaris=parseInt($('.evidence').length);
		   		  		   
		   for(var i=0;i < jmlbaris;++i){
			   t+=parseFloat(numeral().unformat($('.price').eq(i).val()));
		   }	
		   
		  		  			 									
		   var hasil_payment_total = discount + t ;
		   var hasil_r_payment = unit_price - hasil_payment_total	;		
		        		        		   
		   $('.r_payment').val(hasil_r_payment);
		   $('.amount').val(hasil_payment_total);
 	 });
	 
	 	$('body').delegate('.price','blur',function(){  		 
		   var price=$('.price').val();
		   var r_payment=$('.r_payment').val();
		   var amount=$('.amount').val();
		   		   
		   $('.price').val(numeral(price).format('0,0.00')) ;		  	  
		   $('.r_payment').val(numeral(r_payment).format('0,0.00')) ;
		   $('.amount').val(numeral(amount).format('0,0.00')) ;		  		 		 		 
 	 });  	*/
	
	 	  	
});


</script>

<script type="text/javascript">
//---------function autocomplite jquery
$(document).ready(function () {
		$(function () {
			$("#txtvinno").autocomplete({
				minLength:0,
                delay:0,
				source: function(request, response) {
					
					var str_url = '<?php echo site_url('c_slssell_unit/c_crud_sell/suggest_vin_engine'); ?>';					
					var str_novin =$("#txtvinno").val();
					
					  
					
					$.ajax({ 
						url: str_url ,												
						data: {txtvinno:str_novin},																																		
						dataType: "json",
						type: "POST",																	
						success: function(data){response(data);																																										
						}						
					});
				},	
				    select:function(event, ui){
					   $('#txtnostock').val(ui.item.txtnostock);
					   $('#txtengine').val(ui.item.txtengine);
					   $('#txtmodel').val(ui.item.txtmodel);
					   $('#txttype').val(ui.item.txttype);
					   $('#txtwarna').val(ui.item.txtwarna);
					   $('#txtlokasi').val(ui.item.txtlokasi);
					   $('#curr').val(ui.item.curr);
					   $('#price_unit').val(ui.item.price_unit);
					   $('#txt_remain').val(ui.item.price_unit);
					   
					   //enabled detail
					   $('.txtdescpay').removeAttr('disabled');
					   $('.datepicker').removeAttr('disabled');
					   $('.price').removeAttr('disabled');			
					   $('.discount').removeAttr('disabled');			
					   $('.uploadfoto').removeAttr('disabled');
					   
					   //=============================					
					   $('.discount').val('0.00');
					   $('.amount').val('0.00');
					   $('.price').val('0.00');			
					   
					}						                  				                     
			});		
				
		});
	
	});	  
</script>    

<script type="text/javascript">
//automatic show price 

var htmlobjek;
$(document).ready(function(){  
	 $("#txtvinno").change(function(){
			var txtvinno_full = $("#txtvinno").val();
			var url = '<?php echo site_url('c_slssell_unit/c_crud_sell/get_vin_engine_full'); ?>';
			
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{txtvinno:txtvinno_full},	
				cache: false,          
				success: function(data, textStatus, jqXHR){           
				    $('[name="txtnostock"]').val(data.txtnostock);	
				 	$('[name="txtvinno"]').val(data.txtvin);			
					$('[name="txtengine"]').val(data.txtengine);
					$('[name="txtmodel"]').val(data.txtmodel);
					$('[name="txttype"]').val(data.txttype);
				    $('[name="txtwarna"]').val(data.txtwarna);
				    $('[name="txtlokasi"]').val(data.txtlokasi);
				    $('[name="curr"]').val(data.curr);
				    $('[name="price_unit"]').val(data.price_unit);
					$('[name="txt_remain"]').val(data.result);
					
					 //enabled detail
				   $('.txtdescpay').removeAttr('disabled');
				   $('.datepicker').removeAttr('disabled');
				   $('.price').removeAttr('disabled');			
				   $('.discount').removeAttr('disabled');			
				   $('.uploadfoto').removeAttr('disabled');
				   
				   //------------------------
				   $('.discount').val('0.00');
				   $('.amount').val('0.00');
				   $('.price').val('0.00');
								
				}
			});
		  });
});	  
</script>


<script type="text/javascript">
//---------function autocomplite jquery
$(document).ready(function () {
		$(function () {
			$("#txtcust").autocomplete({
				minLength:0,
                delay:0,
				source: function(request, response) {
					
					var str_url = '<?php echo site_url('c_slssell_unit/c_crud_sell/suggest_customer'); ?>';					
					var str_customer =$("#txtcust").val();
															
					$.ajax({ 
						url: str_url ,												
						data: {txtcust:str_customer},																																		
						dataType: "json",
						type: "POST",																	
						success: function(data){response(data);																																										
						}						
					});
				},	
				    select:function(event, ui){					  
					   $('#txtidcust').val(ui.item.txtidcust);					  
					}						                  				                     
			});		
				
		});
	
	});	  
</script>    

<script type="text/javascript">
//automatic show price 

var htmlobjek;
$(document).ready(function(){  
	 $("#txtcust").change(function(){
			var txtcust_full = $("#txtcust").val();
			var url = '<?php echo site_url('c_slssell_unit/c_crud_sell/get_customer_full'); ?>';
			
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{txtcust:txtcust_full},	
				cache: false,          
				success: function(data, textStatus, jqXHR){           
				 	$('[name="txtcust"]').val(data.txtcust);			
					$('[name="txtidcust"]').val(data.txtidcust);													
				}
			});
		  });
});	  
</script>
