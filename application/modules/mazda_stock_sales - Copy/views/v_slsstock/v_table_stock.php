



 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
 
 <?php 
   If ( $this->session->flashdata('pesan') != ""){ ?>     
         <div class="alert alert-danger" role="alert">
             <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
             <span class="sr-only">Info:</span>                        
             <?php  echo $this->session->flashdata('pesan');	?>  
         </div>	
 <?php } ?>
 
 <?php 
   If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
         <div class="alert alert-info" role="alert">
             <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
             <span class="sr-only">Info:</span>                        
             <?php  echo $this->session->flashdata('pesan_succes');	?>  
         </div>	
 <?php } ?>
       
 <?php   	
   If ($this->session->flashdata('pesan_aproval') == "1"){ ?>      
       <div class="alert alert-info" role="alert">
                 <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                 <span class="sr-only">Info:</span>                        
                 <?php  echo "Send Approval Successfully"	?>  
       </div>		
 <?php } ?>
 
 <?php   	
   If ($this->session->flashdata('pesan_aproval') == "0"){ ?>                
     <div class="alert alert-danger" role="alert">
       <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
       <span class="sr-only">info:</span>                        
       <?php  echo "Send Approval failed"	?>  
         </div>		      
 <?php } ?>	
   
 <div class="panel panel-default">
   <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" ><?php echo "Table Stock Unit "." | ".$header ; ?> </div>
    <div class="panel-body">               
    
           
  <?php  echo form_open('mazda_stock_sales/c_slsstock/c_crud_stock/multiple_submit');  ?>     
  
  <table>
      <tr>
       <td colspan="3" width="6%">
       <label>Selected : </label>
        </td>
        <td>
        <select name="cbostatus" id="cbostatus" class="form-control">                          
              <option value="vin">VIN</option>
              <option value="engine">ENGINE</option>
              <option value="stock_no">STOCK NO</option>
              <option value="flag_demo_unit">STATUS STOCK</option>
              <option value="csi">CSI</option>
              <option value="desc_type">TYPE</option>
              <option value="location_stock">LOCATION</option>             
        </select> 
        </td>
        <td>&nbsp;

        <td colspan="3">
        <input type="text" name="txtcari" class="form-control" placeholder="Search" id="txtcari" value="<?php echo $this->session->flashdata('cari'); ?>" > 
        </td>
          
        <td width="40%" >
                
              <button id="btncari" name="btncari" class="btn btn-app btn-primary btn-xs radius-4" value="Search" type="submit">
                <i class="ace-icon fa fa-search bigger-160"></i>
                 Search
              </button>       
                                                        
             <!-- <input id="btncari" name="btncari" type="submit" value="Search"  class="btn btn-default" style="background:#CCC;color:#FFF" />               -->
             
        </td>
        <td colspan="3">&nbsp;  
              
        </td> 
        <td colspan="3">                        
        
        <!-- <input id="btnadd" name="btnadd" type="submit" value="Add New"  class="btn btn-primary " />    
             <input id="btnedit" name="btnedit" type="submit" value="Edit"   class="btn btn-success btn_edit" disabled="disabled"/>
             <input id="btndel" name="btndel" type="submit" value="Delete"  class="btn btn-danger btn_delete" onclick="return confirm('are you sure want to delete this data!')" disabled="disabled"/>	
              <input id="btnrefresh" name="btnrefresh" type="submit" value="Refresh Table"  class="btn btn-warning " />    
              <input type="button" name="btn_cross_sell" id="btn_cross_sell" value="Cross Selling" class= "btn btn-danger btn_cross_do" disabled="disabled" />  	       
        -->
        <button id="btnadd" name="btnadd" class="btn btn-app btn-primary btn-xs radius-4" value="Add New" type="submit">
          <i class="ace-icon fa fa-book bigger-160"></i>
            Add New
        </button>  

        <button id="btnedit" name="btnedit" class="btn btn-app btn-success btn-xs radius-4 btn_edit" value="Edit" type="submit" disabled>
           <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
            Edit
        </button> 

        <button id="btndel" name="btndel" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" value="Delete" type="submit" onclick="return confirm('are you sure want to delete this data!')" disabled>
          <i class="ace-icon fa fa-ban bigger-160"></i>
            Delete
        </button> 

        <button id="btnrefresh" name="btnrefresh" class="btn btn-app btn-warning btn-xs radius-4" value="Refresh Table" type="submit">
          <i class="ace-icon fa fa-refresh bigger-160"></i>
            Refresh
        </button> 

        <button id="btn_cross_sell" name="btn_cross_sell" class="btn btn-app btn-danger btn-xs radius-4 btn_cross_do" value="Cross Selling" type="submit" disabled>
          <i class="ace-icon fa fa-money bigger-160"></i>
            Cross Sell
        </button> 
       
          </td>
       </tr> 
     </table>   
  <br />
  
 <div class="row">
  <div class="col-md-12 col-md-offset-0">   
   <?php 
     echo '<table width="100%" class="table-bordered  table-condensed"  >';		
     echo '<tr style=" color:#144b79;background:url('.base_url("asset/images/child-panel-bg.png").');font-weight:bold;font-size: 12px; ">';		
     echo '<td width="8%"align="center">Stock Number </td>';
     echo '<td width="5%" align="center">Branch</td>';													
     echo '<td width="9%" align="center">Type</td>' ;
     echo '<td width="3%" align="center">Vin</td>' ;
     echo '<td width="7%" align="center">Engine</td>' ;	
     echo '<td width="8%" align="center">Colour</td>' ;				
     echo '<td width="3%" align="center">CSI</td>' ;
     echo '<td width="6%" align="center" >Location Stock</td>';	
     echo '<td width="5%" align="center">Intake</td>' ;	
     echo '<td width="5%" align="center">Year</td>' ;	
     echo '<td width="3%" align="center">Status Stock</td>' ;		
     echo '<td width="5%" align="center">Date Recived</td>' ;
     echo '<td width="5%" align="center">Date Pay MMI</td>' ;
     echo '<td width="5%" align="center">Bank</td>' ;	
     echo '<td width="3%" align="center">Status</td>' ;		
     echo '<td width="5%" align="center">PDF View</td>' ;			
     echo '<td width="7%"><a href="javascript:void(0)" onclick="CheckAll()" style="text-decoration:none"> All</a>';
     echo ' / ';
     echo '<a href="javascript:void(0)" onclick="UncheckAll()" style="text-decoration:none">Uncheck</a></td>';
     echo '</tr>' ;		    		
     
   if ($ceck_row >= 1) :		  
     foreach($stock_view as $row):
      
      if ($row->status_flag_stock == "-1") : 	  
        echo '<tr style="font-size:11px;" class="btn-warning">';  
      else:		
        if ($row->status_flag_stock == "2") : 	
         echo '<tr style="font-size:11px;" class="btn-success">';  
       else:		
         if ($row->status_flag_stock =="3"):
            echo '<tr style="font-size:11px;" class="btn-danger">';
         else:	    
           echo '<tr style="font-size:11px;">'; 
         endif;	
       endif;
      endif;		 
      ?>                         
              <?php //echo number_format($row->gran_total,2,'.',',') ;?>                                   
                 <td width="8%"><?php echo $row->stock_no; ?></td>
                 <td><?php echo $row->name_branch ; ?></td>		
                 <td><?php echo $row->desc_type ; ?></td>		
                 <td><?php echo $row->vin ; ?></td>	
                 <td align="center"><?php echo $row->engine ; ?></td>	
                 <td><?php echo $row->colour ; ?></td>
                 <td><?php echo $row->csi ; ?></td>									
                 <td  align="center"><?php echo $row->location_stock ; ?></td>                       
                 <td align="center"><?php echo $row->intake ; ?></td>    
                 <td align="center" ><?php echo $row->year ; ?></td>    
                 <td align="center" >
           <?php 
               if($row->flag_demo_unit=="1") :
                echo '<label style="color:red;">'.'DEMO CAR'.'</label>' ;
             else: 
                echo '<label style="color:blue;">'.'STOCK UNIT'.'</label>' ;   
             endif;   
           ?>
                 </td>               
                 <td>
           <?php 
              if ($row->rec_date != null || $row->rec_date != ""  ) :						
               echo date('d-m-Y', strtotime($row->rec_date)) ; 
              else:	
                echo "-";
              endif;	
           ?>
                 </td> 
                 <td>
           <?php 
              if ($row->pay_date_mmi != null || $row->pay_date_mmi != ""  ) :
               echo date('d-m-Y', strtotime($row->pay_date_mmi)) ;
                else:
               echo "-" ;
             endif;	
           ?>
                 </td>                   
                 <td align="center"><?php echo $row->name_bank ; ?></td>   
                 <td align="center">
          <?php
               if ($row->status_flag_stock==2) : 	 
                echo'<div style="font-weight:bold;color:blue;" align="center">'."Booked".'</div>';	 
             else:
                 if ($row->status_flag_stock==-1) : 	 
                   echo'<div style="font-weight:bold;color:blue;" align="center">'."WRS".'</div>';
               else:	 
                 if ($row->status_flag_stock == 3) :
                 echo'<div style="font-weight:bold;color:white;" align="center">'."CROSS SELLING".'</div>';
               else: 	
                 echo '<div style="font-weight:bold;color:blue;" align="center">'."READY".'</div>';
                endif;		
               endif;	 
             endif;	  
           ?>
                 </td>    
                 <td align="center"><?php 
             If ($row->attach_foto != "")
             {  
             ;
              echo anchor_popup($row->attach_foto,"<span class='glyphicon glyphicon-search btn btn-primary' aria-hidden='true'>".'View'."</span>");
            }else{
               echo '<div style=" color:#EB293D" align="center">'."No PDF".'</div>';	  
            }
           ?>
                     
                 </td>                
                 <td>
                  <?php
               if ($row->status_flag_stock==2) : 	 
                echo'<div style="font-weight:bold;color:red;" align="center">'."X".'</div>';	 
             else:
                 if ($row->status_flag_stock==-1) : 	 
                   echo'<div style="font-weight:bold;color:#EB293D;" align="center">'."WRS".'</div>';
               else:	 
                   if ($row->status_flag_stock==3) : 	 
                       echo'<div style="font-weight:bold;color:white ;" align="center">'."Waiting Approve".'</div>';
                   else:	 
                ?>
                               <div align="center"><input id="checkchild" name="msg[]" type="checkbox" value="<?php echo $row->stock_no ; ?>"/>
                              <?php
                endif;	 
               endif;	 
             endif;	  
           ?>
                
     <?php form_close(); ?>             			            					
                 </div>
             </td>
       </tr> 	  
      <?php 		    
       endforeach;
       endif;
    echo'</table>';	  				
      ?> 
          
        
          <table  class=" table-bordered">         
          <tr>
            <td class="btn-primary"><?php echo 'TOTAL STOCK FREE'." : ".$count_row_stock.".."  ?></td>
             <td class="btn-success"> <?php echo 'TOTAL STOCK BOOKED'." : ".$count_row_dp.".. "  ?></td>
             <td class="btn-warning"><?php echo 'TOTAL STOCK WRS'." : ".$count_row_wrs.".. "?></td>
          </tr>
          </table>         		                  
          
          
     <?php		   			
          if (! empty($pagination))			
        { 
            echo '<center><table>';
          echo '<tr>';
          echo '<td>';
          //echo '<div class="row">';
          echo '<div class="col-md-12 col-md-offset-2 text-center">'.$pagination.'</div>';
          //echo '</div>';
          echo '</td>';
          echo '</tr>';
          echo '</table></center>';				 
        }			  	  						
     ?>
         
  </div>  
 </div> 
       
           
   </div>
  </div>
    
 </div>
 
 
 <!-- Modal Buat Cross Selling -->
 
   <div class="modal fade" id="myModal-cross" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
             <div class="modal-dialog">
                 <div class="modal-content">
                     <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                         <h4 class="modal-title" id="myModalLabel">Send Approval Cross Selling</h4>
                     </div>
                     
                     <div class="modal-body-reject">
                        <table width="90%" class="table-condensed" align="center">
                             <tr> 
                                 <td> <label for="Transaction ID"  class="col-xs-8">Stock No</label></td> 
                              
                                   <td>
                                    <div class="col-xs-12">
                                     <input type="text" id="txtnostock" name="txtnostock" class="form-control" readonly>
                                    </div> 
                                   </td>                                  
                              </tr>
                               <tr> 
                                 <td> <label for="VIN"  class="col-xs-8">VIN</label></td>                              
                                   <td>
                                    <div class="col-xs-12">
                                     <input type="text" id="txtvin_cross" name="txtvin_reject" class="form-control" readonly>
                                    </div> 
                                   </td>                                  
                              </tr>
                               <tr> 
                                 <td> <label for="Engine"  class="col-xs-8">Engine</label></td>                              
                                   <td>
                                    <div class="col-xs-12">
                                     <input type="text" id="txtengine_cross" name="txtengine_reject" class="form-control" readonly>
                                    </div> 
                                   </td>                                  
                              </tr>
                               <tr> 
                                 <td> <label for="Type"  class="col-xs-8">Type</label></td>                              
                                   <td>
                                    <div class="col-xs-12">
                                     <input type="text" id="txttype_cross" name="txttype_reject" class="form-control" readonly>
                                    </div> 
                                   </td>                                  
                              </tr>
                               <tr> 
                                 <td> <label for="Colour"  class="col-xs-8">Colour</label></td>                              
                                   <td>
                                    <div class="col-xs-12">
                                     <input type="text" id="txtcolour_cross" name="txtcolour_reject" class="form-control" readonly>
                                    </div> 
                                   </td>                                  
                              </tr>    
                              
                              </tr>
                               <tr> 
                                 <td> <label for="Colour"  class="col-xs-8">Send To Branch</label></td>                              
                                   <td>
                                    <div class="col-xs-12">
                                       <select name="cbobranchutama" id="cbobranchutama" class="form-control" > 
                                        <option value="">--Chosse Destination--</option> 
                                        <?php foreach ($tampil_branch as $row_branch) : ?>	   			        
                                        <option value="<?php echo $row_branch->branch_id ?>"><?php echo $row_branch->name_branch ?></option>
                                         <?php endforeach ; ?>         			         
                                        </select> 
                                    </div> 
                                   </td>                                  
                              </tr>                             
                              <tr> 
                                 <td ><label for="Remarks Cross Selling"  class="col-xs-9">Remarks Cross Selling</label></td> 
                                 <td>
                                   <div class="col-xs-12">
                                     <textarea id="txtremarks_cross" name="txtremarks_cross"  class="form-control"></textarea>
                                   </div>  
                                 </td>    
                              </tr>
                              <tr> 
                                 <td></td> 
                                 <td>
                                    <div class="col-xs-12">
                                    
                                    </div>        
                                 </td>    
                              </tr>
                             </table> 
            
                     </div> 
                     
                      <div class="modal-footer">
                      <input type="submit" name="btncross-send" value="Send Approval" class="btn btn-danger"/>
                      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                     </div>                  
                 </div>
             </div>           
         </div>
     
 <!-- Modal Buat Cross Selling -->	
 
 <!-- jQuery Version 1.11.0 -->
 <script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>
 
 <!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
 <script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>
 
 <!-- jQuery_ui -->
 <script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>
 
 <!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
 <script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>
 
 <!-- numeral buat format angka -->
 <script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
   
 <!--file include Bootstrap js dan datepickerbootstrap.js-->
 <script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
 
 <script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
 <!-- Fungsi datepickier yang digunakan -->
 
 <script>
 
 // fungsi untik checklist semua otomatis
 function CheckAll()
 {
     l =document.forms[0].length;
     for(i=0;i<l;i++) 	
     {
      document.forms[0].elements[i].checked = true;		
     }	
     document.getElementById('btnedit').disabled = true;	
     document.getElementById('btndel').disabled = false;	
     document.getElementById('btnsendtab').disabled = true;	
 }
 
 function UncheckAll()
 {
    l =document.forms[0].length;
     for(i=0;i<l;i++) 	
     {
      document.forms[0].elements[i].checked = false;			 
     }
     document.getElementById('btnedit').disabled = true;
     document.getElementById('btndel').disabled = true;
     document.getElementById('btnsendtab').disabled = true;
 }
 
 
 //--------------------function disbaled enabled button with check book
 
 $(function() {  //.btn hnya untuk tombol delete yang disabled.
     $('input[type="checkbox"]').change(function(){
         if($('input[type="checkbox"]:checked').length == 1){
             $('.btn_edit').removeAttr('disabled');			
         }
         else{
             $('.btn_edit').attr('disabled', 'disabled');								
         }
     });
 });
 
 $(function() {  //.btn hnya untuk tombol delete yang disabled.
     $('input[type="checkbox"]').change(function(){
         if($('input[type="checkbox"]:checked').length == 1){
             $('.btn_send').removeAttr('disabled');			
         }
         else{
             $('.btn_send').attr('disabled', 'disabled');								
         }
     });
 });
 
 $(function() {  //.btn hnya untuk tombol delete yang disabled.
     $('input[type="checkbox"]').change(function(){
         if($('input[type="checkbox"]:checked').length >= 1){
             $('.btn_delete').removeAttr('disabled');			
         }
         else{
             $('.btn_delete').attr('disabled', 'disabled');								
         }
     });
 });
 
 $(function() {  //.btn hnya untuk tombol cross selling yang disabled.
     $('input[type="checkbox"]').change(function(){
         if($('input[type="checkbox"]:checked').length == 1){
             $('.btn_cross_do').removeAttr('disabled');			
         }
         else{
             $('.btn_cross_do').attr('disabled', 'disabled');								
         }
     });
 });
 
 
 //-------------------------------------------------------------------------		 	
 
 //-------------------------------------------------------------------------		 	
   
 //set focus ----------------------------
 $(function() {
   $("#txtcari").focus();
 });		  
 //set focus ----------------------------  
 </script>
 
 <script>
 
 $(function(){
   $(document).on('click','.btn_cross_do',function(e){	
     var req_id_stock = $('input[type="checkbox"]:checked').val()	
     var url = '<?php echo site_url('mazda_stock_sales/c_slsstock/c_slsstock/get_stock_no'); ?>';
               
     $("#myModal-cross").modal('show'); 
     $("#txtnostock").val(req_id_stock); 	
     $("#txtremarks_cross").val("");  
     
     $.ajax({			
       type:'POST',
       url: url,
       dataType: "json",
       data: {txtidstock:req_id_stock}, 
       success: function(data, textStatus, jqXHR ) {				    									
         $('#txtvin_cross').val(data.txtvin);			
         $('#txtengine_cross').val(data.txtengine);				
         $('#txttype_cross').val(data.txttype);
         $('#txtcolour_cross').val(data.txtwarna);			
             
       }
     });		
    });	
 });
 
 </script>
 
 <script>
 $(function(){
   //jquery untuk post submit save / edit
     $('#myform').on('submit', function(e){ //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
         e.preventDefault();		
     var url = '<?php  echo base_url('mazda_stock_sales/c_slsstock/c_crud_stock/multiple_submit');  ?>';	 // url			
         $.ajax({
             url: url, //this is the submit URL
             type: 'POST', //or POST
             data: $('#myform').serialize(), //id #myform di buat untuk membedakan dengan id form_open "form_my" C.igniter
             success: function(data){              
             }
         });
     });
 });
 </script>
 
 
 <?php if ($this->session->userdata('id_group') != 'admin001'): ?>
    <script>
   $(document).ready(function() { 	
     $('#btndel').hide();		
   });
   </script>
 <?php endif; ?>