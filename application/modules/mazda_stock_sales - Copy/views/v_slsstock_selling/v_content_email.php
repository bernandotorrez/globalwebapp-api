<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.css');?>" />  
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" />        
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />      

<h2>
	<?php
	    $strcom = $this->session->userdata('company');
		$strbranch = $this->session->userdata('name_branch');
		$strdept = $this->session->userdata('dept');
		 
	    echo  'Company : '.$strcom. " | " ." Cabang : " .$strbranch ;
	 ?>
</h2>

<table width="70%" class="table-striped table-bordered table-hover table-condensed" >
 <?php foreach ($str_selling_master as $row)  :  ?>
    <tr style="background:#FBF5B7">
      <td width="30%" style="font-weight:bold;">SPK NO  :</td>
      <td><?php echo $row->nospk ;?></td>
    </tr>
    <tr >
      <td width="30%" style="font-weight:bold;">Pay type :</td>
      <td style="font-weight:bold;color:RED">
	        <?php			 
				echo $row->desc_pay ; 			
	  		?>
      </td>
    </tr>
    <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;">User Submission :</td>
      <td><?php echo $row->user_sell ; ?></td>
    </tr>
     <tr style="font-weight:bold;" >
      <td width="30%" style="font-weight:bold;">Customer :</td>
      <td><?php echo $row->cust_name ; ?></td>
    </tr>
     <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;">Sales :</td>
      <td><?php echo $row->sal_name ; ?></td>
    </tr>
    <tr >
      <td width="30%" style="font-weight:bold;">Date Send Approval : </td>
      <td><?php echo date('d-m-Y') ; ?></td>
    </tr>
    <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;" >Model :</td>
       <td><?php echo $row->desc_model ; ?></td>
    </tr>
     <tr >
      <td width="30%" style="font-weight:bold;" >Type :</td>
       <td><?php echo $row->desc_type ;?></td>
    </tr>
     <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;" >Colour :</td>
       <td><?php echo $row->colour ; ?></td>
    </tr>
     <tr >
      <td width="30%" style="font-weight:bold;" >Vin :</td>
       <td><?php echo $row->vin ;?></td>
    </tr>
     <tr style="background:#FBF5B7">
       <td width="30%" style="font-weight:bold;">Engine:</td>
       <td><?php echo $row->engine ; ?></td>
    </tr>
     <tr >
       <td width="30%" style="font-weight:bold;">Year:</td>
       <td><?php echo $row->year ;?></td>
    </tr>
     <tr style="background:#FBF5B7">
       <td width="30%" style="font-weight:bold;"><?php echo "Price Unit: " ?></td>
       <td style="color:#FC2E33;"><?php echo $row->curr_sell." ".number_format($row->price_sell,2,'.',',') ?></td>
    </tr>
    <tr >
       <td width="30%" style="font-weight:bold;"><?php echo "Discount Unit:  " ?></td>
       <td style="color:#FC2E33;"><?php echo $row->curr_sell." ".number_format($row->discount,2,'.',',') ?></td>
    </tr>
    <tr style="background:#FBF5B7">
       <td width="50%" style="font-weight:bold;"><?php echo "Total Payment : " ?></td>
       <td style="color:#FC2E33; font-weight:bold;"><?php echo $row->curr_sell." ". number_format($row->total_paymet_customer,2,'.',',')?></td>
    </tr>  
      <tr >
       <td width="50%" style="font-weight:bold;"><?php echo "Remaining Payment : " ?></td>
       <td style="color:#FC2E33; font-weight:bold;"><?php echo $row->curr_sell." ". number_format($row->remaining_amount_customer,2,'.',',')?></td>
    </tr>     
 <?php endforeach; ?>   
</table>

</br>

<h2>Detail Payment</h2>
<table class="table-striped table-bordered table-hover table-condensed" width="70%">    
    <tr style="background:#CCC;">
        <td width="2%" style="font-weight:bold;">No</td>
        <td width="28%" style="font-weight:bold;">Description</td>
        <td width="28%" style="font-weight:bold;">Pay Date</td>
        <td width="5%" style="font-weight:bold;">Amount</td>     
    </tr>
    <?php foreach ($str_selling_detail as $row_detail)  : $intno = $intno +1;  ?>
   		<tr>	
          <td><?php echo $intno ?></td>							  
          <td><?php echo $row_detail->desc_det ; ?></td>							  
          <td><?php echo date('d-m-Y', strtotime($row_detail->pay_date)) ;?></td>																																																				
          <td ><?php echo number_format($row_detail->amount,2,'.',',') ;?></td> 
        </tr>
    <?php endforeach; ?>
</table>