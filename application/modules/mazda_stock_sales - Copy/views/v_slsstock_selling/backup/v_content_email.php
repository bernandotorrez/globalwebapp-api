<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.css');?>" />  
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" />        
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />      

<h2>
	<?php
	    $strcom = $this->session->userdata('company');
		$strbranch = $this->session->userdata('name_branch');
		$strdept = $this->session->userdata('dept');
		 
	    echo  'Company : '.$strcom. " | " ." Cabang : " .$strbranch. " | " ."Dept : ".$strdept;
	 ?>
</h2>

<table width="70%" class="table-striped table-bordered table-hover table-condensed" >
 <?php foreach ($str_pp_master as $row)  :  ?>
    <tr style="background:#FBF5B7">
      <td width="30%" style="font-weight:bold;">PP Number :</td>
      <td><?php echo $this->session->userdata("ses_noppnew");?></td>
    </tr>
    <tr >
      <td width="30%" style="font-weight:bold;">User Submission :</td>
      <td><?php echo $row->user_submission ?></td>
    </tr>
    <tr style="background:#FBF5B7">
      <td width="30%" style="font-weight:bold;">Date Send Approval : </td>
      <td><?php echo date('d-m-Y', strtotime($row->date_send_aproval)) ?></td>
    </tr>
    <tr >
      <td width="30%" style="font-weight:bold;" >Termin :</td>
       <td><?php echo $row->term_top ?></td>
    </tr>
     <tr style="background:#FBF5B7">
       <td width="30%" style="font-weight:bold;">Remarks:</td>
       <td><?php echo $row->remarks ?></td>
    </tr>
    <tr>
       <td width="50%" style="font-weight:bold;"><?php echo "Grand Total : "." ".$row->currency ?></td>
       <td style="color:#FC2E33; font-weight:bold;"><?php echo number_format($row->gran_total,2,'.',',')?></td>
    </tr>
 <?php endforeach; ?>   
</table>

</br>

<h2>Detail PP</h2>
<table class="table-striped table-bordered table-hover table-condensed" width="70%">    
    <tr style="background:#CCC;">
        <td width="2%" style="font-weight:bold;">No</td>
        <td width="28%" style="font-weight:bold;">Description</td>
        <td width="28%" style="font-weight:bold;">Spec</td>
        <td width="5%" style="font-weight:bold;">Qty</td>
        <td width="20%" style="font-weight:bold;">Harga </td>							
        <td width="20%" style="font-weight:bold;">Total Harga</td>
    </tr>
    <?php foreach ($str_pp_detail as $row_detail)  : $intno = $intno +1;  ?>
   		<tr>	
          <td><?php echo $intno ?></td>							  
          <td><?php echo $row_detail->desc ?></td>							  
          <td><?php echo $row_detail->spec ?></td>																																																				
          <td align="center"><?php echo  number_format($row_detail->qty) ?></td>							
          <td><?php echo  number_format($row_detail->harga,2,'.',',') ?></td>							
          <td ><?php echo number_format($row_detail->total,2,'.',',') ?></td> 
        </tr>
    <?php endforeach; ?>
</table>