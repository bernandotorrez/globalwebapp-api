<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.css');?>" />  
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" />        
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />      

<h2>
	<?php
	    $strcom = $this->session->userdata('company');
		$strbranch = $this->session->userdata('name_branch');
		$strdept = $this->session->userdata('dept');
		 
	    echo  'Company : '.$strcom. " | " ." Cabang : " .$strbranch ;
	 ?>
</h2>

<table width="100%" class="table-striped table-bordered table-hover table-condensed" >
 
    <tr style="font-size:13px;background:#FBF5B7;" >
       <td  style="font-weight:bold;">SPK NO  </td>       
       <td  style="font-weight:bold;">Pay type </td>     
       <td  style="font-weight:bold;">Due Date Payment  </td>                 
        <td  style="font-weight:bold;">User Submission </td>    
       <td  style="font-weight:bold;">Customer </td>       
       <td  style="font-weight:bold;">Sales </td> 
       <td  style="font-weight:bold;">Date Send Approval </td>             
       <td  style="font-weight:bold;">Type </td>
       <td  style="font-weight:bold;">Colour </td>
       <td  style="font-weight:bold;">Vin </td> 
       <td  style="font-weight:bold;">Engine</td>           
       <td  style="font-weight:bold;"><?php echo "Price Unit" ?></td>       
       <td  style="font-weight:bold;"><?php echo "Discount Unit" ?></td>
       <td  style="font-weight:bold;"><?php echo "Total Payment" ?></td>      
       <td  style="font-weight:bold;"><?php echo "Remain Payment" ?></td>      
    </tr>    
    
    <?php foreach ($reminder_duedate_selling as $row)  :  ?>
    <tr style="font-size:13px;">
        <td><?php echo $row->nospk ;?></td>
      <td>
	        <?php
			  if($row->flag_payment_status  =="1") :  
				 echo "customer" ; 
			  else:
			  	 echo "Leasing" ; 
			  endif;	
	  		?>
      </td>     
     <td style="color:#FC2E33;"><?php echo date(("d-m-Y"),strtotime($row->date_dudate_spk)); ?></td>
     <td><?php echo $row->user_sell ; ?></td> 
     <td><?php echo $row->cust_name ; ?></td>  
     <td><?php echo $row->sal_name ; ?></td>
     <td><?php echo date('d-m-Y') ; ?></td>     
     <td><?php echo $row->desc_type ;?></td>
     <td><?php echo $row->colour ; ?></td>
     <td><?php echo $row->vin ;?></td>  
     <td><?php echo $row->engine ; ?></td>    
     <td style="color:#FC2E33;"><?php echo $row->curr_sell." ".number_format($row->price_sell,2,'.',',') ?></td>   
     <td style="color:#FC2E33;"><?php echo $row->curr_sell." ".number_format($row->discount,2,'.',',') ?></td> 
     <td style="color:#FC2E33; font-weight:bold;"><?php echo $row->curr_sell." ". number_format($row->total_paymet_customer,2,'.',',')?></td>   
     <td style="color:#FC2E33; font-weight:bold;"><?php echo $row->curr_sell." ". number_format($row->remaining_amount_customer,2,'.',',')?></td>
    </tr>
    
 <?php endforeach; ?>   
</table>
