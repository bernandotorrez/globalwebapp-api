<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.css');?>" />  
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>" />        
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>" />      

<h2>
	<?php
	    $strcom = $this->session->userdata('company');
		$strbranch = $this->session->userdata('name_branch');
		$strdept = $this->session->userdata('dept');
		 
	    echo  'Company : '.$strcom. " | " ." Cabang : " .$strbranch ;
	 ?>
</h2>

<table width="70%" class="table-striped table-bordered table-hover table-condensed" >
 <?php foreach ($str_reject_cross_selling as $row)  :  ?>
 
 <tr style="background:#FBF5B7;color:#900; font-weight:bold">
 	<td colspan="2">Cross Selling Rejected by. <?php echo $this->session->userdata('name'); ?></td>    
  </tr>
 
 <tr style="color:#900; font-weight:bold">
 	<td>REJECT REASON</td>    
    <td><?php echo $row->remarks_reject_cross ;?></td>
 </tr>
 
  
    <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;">User stock :</td>
      <td><?php echo $row->user_stock ; ?></td>
    </tr>
    
    <tr >
      <td width="30%" style="font-weight:bold;">Date Send Approval : </td>
      <td><?php echo date('d-m-Y',strtotime($row->date_send_approval)) ; ?></td>
    </tr>
    <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;" >Model :</td>
       <td><?php echo $row->desc_model ; ?></td>
    </tr>
     <tr >
      <td width="30%" style="font-weight:bold;" >Type :</td>
       <td><?php echo $row->desc_type ;?></td>
    </tr>
     <tr style="background:#FBF5B7" >
      <td width="30%" style="font-weight:bold;" >Colour :</td>
       <td><?php echo $row->colour ; ?></td>
    </tr>
     <tr >
      <td width="30%" style="font-weight:bold;" >Vin :</td>
       <td><?php echo $row->vin ;?></td>
    </tr>
     <tr style="background:#FBF5B7">
       <td width="30%" style="font-weight:bold;">Engine:</td>
       <td><?php echo $row->engine ; ?></td>
    </tr>
   
 <?php endforeach; ?>   
</table>

