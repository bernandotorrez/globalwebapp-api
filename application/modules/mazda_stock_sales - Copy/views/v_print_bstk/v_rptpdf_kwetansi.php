<?php
			
	require('mc_table.php');
	$pdf=new PDF_MC_Table('P','cm',"A4");
	$pdf->Open();
	$pdf->AddPage();	
	$pdf->AliasNbPages();
	$pdf->SetMargins(1,1,1);
	$pdf->SetFont('times','B',12);

    $pdf->Image('asset/images/mazda_logo.jpg',1,1,4); //tampilan yg pertama di tampilkan report : P,L,B
	
			//-----------------------------------------------------				
	$pdf->Ln(1); // baris berikut nya pada report denga lebar (line)garis ke bawah sebanyak 15(line) 
	$pdf->setFont('Arial','B',16); //Type huruf(font beserta besaran huruf)
	$pdf->setFillColor(255,255,255); //warna pada baris tulisan                								
	$pdf->cell(20,0.8,"PT.".$this->session->userdata('company'),0,1,'C',1); //tampil session company 
	$pdf->setFont('Arial','I',10); //Type huruf(font beserta besaran huruf)
	$pdf->cell(20,0.2,"Authorized Mazda Dealer",0,1,'C',1); //tampil session company 
	$pdf->Image('asset/images/alamat_mazda.jpg',1.8,3.2,17); //tampilan yg pertama di tampilkan report : P,L,B				
	//$pdf->Ln(1);
	//$pdf->Image('asset/images/alamat_mazda.jpg',1,3.1,17); //tampilan yg pertama di tampilkan report : P,L,B
	
	
	$pdf->Ln(2);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(100,0,"Date  : ".date('d-m-Y'),0,1,'L',1);                       
	
	$pdf->Ln(0.3);
	$pdf->setFont('Arial','BIU',12);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.7,"KUITANSI",1,1,'C',1); 
	$pdf->setFont('Arial','',10);
	$pdf->setFillColor(255,255,255);   
	$pdf->cell(0,0.8,"No.				   : ".$this->session->userdata('sesidtrans_bstk'),1,1,'L',1);	
	$pdf->cell(0,0.8,"Telah terima Dari     : ".$this->session->userdata('ses_csutomer'),1,1,'L',1);	
	$pdf->cell(0,0.8,"Jumlah				                     : ".number_format($this->session->userdata('ses_totalpay_cust'),2,'.',','),1,1,'L',1);	
	$pdf->cell(0,0.8,"Terbilang				                  : ".terbilang($this->session->userdata('ses_totalpay_cust')),1,1,'L',1);	
	$pdf->cell(0,0.8,"Untuk Pembayaran	  : Unit ".$this->session->userdata('ses_desc_type').", Warna ".$this->session->userdata('ses_colour').", Tahun ".$this->session->userdata('ses_year'),1,1,'L',1);	
	$pdf->cell(0,0.8,"					                                    No rangka :".$this->session->userdata('ses_vin').", No Mesin :".$this->session->userdata('ses_engine'),1,1,'L',1);	

	$pdf->Ln(0.2);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);
	//$pdf->cell(9.5,1,"PT.Eurokars Surya Utama",1,0,'C',1);  	  
	$pdf->cell(14,1,"",0,0,'C',1);  	  
	$pdf->cell(5,0.5,"SIGNATURE",1,1,'C',1);  	  
	//$pdf->cell(9.5,3,"BCA Cabang Arjuna, Account No. 6010 - 23 - 5454",0,0,'L',1);	 	
	$pdf->cell(14,3,"",0,0,'L',1);	 	
	$pdf->cell(5,1.5,"",1,1,'C',1); 
	$pdf->cell(14,1,"",0,0,'C',1);  	  
	$pdf->cell(5,0.5,"Kepala Cabang",1,1,'C',1);  	  
   
	
	
	/*
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(100,0,"Amount Of Money :".number_format($this->session->userdata('ses_grand'),2,'.',','),0,1,'L',1); 
	
	
	$pdf->Ln(0.5);				
	$no = 1;// intuk index petama	
	$pdf->SetFont('Arial','B',9);
	$pdf->SetWidths(array(1, 5, 5, 1,3.7,3.7));
	$pdf->SetHeight(0.1);
	$pdf->Row(array("No", "Description", "Specs", "Qty","Price","Total"));
	
	
	
	$pdf->setFont('Arial','B',10);
	$pdf->setFillColor(255,255,255);               
	$pdf->cell(15.7,0.6,"Grand Total :"." ".$key->currency." ",1,0,'R',1);
	$pdf->cell(3.7,0.6,number_format($key->gran_total,2,'.',','),1,1,'R',1);
	$pdf->cell(19.4,0.6,"Mention:"." "."(".terbilang($key->gran_total).")",1,1,'L',1);
	
	$pdf->Ln(0.6);				
	$pdf->setFont('Arial','B',8);
	$pdf->setFillColor(255,255,255);    	
	$pdf->cell(13.4,0.5,"",0,0,'C',1);  	// buat header jabatan yg menandatangani		    				
	$pdf->cell(3,0.5,"Manag.Dept / Head",1,0,'C',1);    
	$pdf->cell(3,0.5,"Applicant",1,1,'C',1);    								                              			   				
	
	$pdf->cell(13.4,1.5,"",0,0,'C',1); //buat space tanda tangan 			    					
	$pdf->cell(3,1.5,"",1,0,'C',1);    
	$pdf->cell(3,1.5,"",1,1,'C',1);     								                              			
	
	$pdf->setFont('Arial','I'.'U',8);
	$pdf->setFillColor(255,255,255); 
	$pdf->cell(13.4,0.5,"",0,0,'C',1);  // buat nama yg menandatangani			    	
	$pdf->cell(3,0.5,$this->session->userdata('sign_head'),1,0,'C',1);  
	$pdf->cell(3,0.5,$this->session->userdata('name'),1,1,'C',1);  
   
    $pdf->Ln(0.3);
	$pdf->setFont('Arial','B'.'I',8);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"Note: PP Has Been Approved By B.O.D "." ".$this->session->userdata('sign_bod')." "."and also Approved By F.C "." ".$this->session->userdata('sign_fc') ,0,1,'L',1); */
				 
	$pdf->Output(); //hasil out put ke browser
	
?>