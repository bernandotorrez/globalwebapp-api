<?php
			
	require('mc_table.php');
	$pdf=new PDF_MC_Table('P','cm',"A4");
	$pdf->Open();
	$pdf->AddPage();	
	$pdf->AliasNbPages();
	$pdf->SetMargins(1,1,1);
	$pdf->SetFont('times','B',12);

	$pdf->Image('asset/images/mazda_logo.jpg',1,1,4); //tampilan yg pertama di tampilkan report : P,L,B
			//-----------------------------------------------------				
	$pdf->Ln(1); // baris berikut nya pada report denga lebar (line)garis ke bawah sebanyak 15(line) 
	$pdf->setFont('Arial','',10); //Type huruf(font beserta besaran huruf)
	$pdf->setFillColor(255,255,255); //warna pada baris tulisan                								
	$pdf->cell(90,0,"PT.".$this->session->userdata('company'),0,0,'L',1); //tampil session company 	
	
	$pdf->Ln(0.3); // baris berikut nya pada report denga lebar (line)garis ke bawah sebanyak 15(line) 
	$pdf->setFont('Arial','',10); //Type huruf(font beserta besaran huruf)
	$pdf->setFillColor(255,255,255); //warna pada baris tulisan                									
	$pdf->cell(0,0.5,"MAZDA ".$this->session->userdata('ses_name_branch'),0,1,'L',1); 				
	
	$pdf->Ln(0.3);
	$pdf->setFont('Arial','',10);
	$pdf->setFillColor(255,255,255);                													   					
	$pdf->cell(0,0,"City : " .$this->session->userdata('ses_city'),0,0,'L',1); 
	
	$pdf->Ln(0.5); // baris berikut nya pada report denga lebar (line)garis ke bawah sebanyak 15(line) 
	$pdf->setFont('Arial','',10); //Type huruf(font beserta besaran huruf)
	$pdf->setFillColor(255,255,255); //warna pada baris tulisan          
	$pdf->cell(0,0,"Address : " . $this->session->userdata('ses_address'),0,1,'L',1);
			                		
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(100,0,"Phone : ".$this->session->userdata('ses_phone'),0,1,'L',1); 
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(100,0,"Fax : ".$this->session->userdata('ses_fax'),0,1,'L',1);     
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(100,0,"Date Print : ".date('d-m-Y'),0,1,'L',1);                       
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','B',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.7,"BUKTI SERAH TERIMA",1,0,'C',1);  
	
	$pdf->Ln(0.7);
	$pdf->setFont('Arial','B',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.5,"Harap dikirim kendaraan berikut ini kepada:",1,0,'L',1);  
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19.0,0.5,"Nama : ".$this->session->userdata('ses_csutomer'),1,1,'L',1); 
	$pdf->cell(19.0,0.5,"Alamat : ".$this->session->userdata('ses_address_cus'),1,1,'L',1);  	
	$pdf->cell(6.7,0.5,"No Telpon : ".$this->session->userdata('ses_phone_cus'),1,0,'L',1); 	
	$pdf->cell(12.3,0.5,"Tanggal Penyerahan : ".date("d-m-Y",strtotime($this->session->userdata('ses_date_bstk'))),1,0,'L',1);
	//$pdf->cell(12.3,0.5,"Tanggal Penyerahan : ".date('d-m-Y'),1,0,'L',1);  	
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','B',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.7,"DATA KENDARAAN",1,0,'C',1);  
	
	$pdf->Ln(0.7);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(6.7,0.5,"Merk / Type : ",1,0,'L',1); 
	$pdf->cell(12.3,0.5,"MAZDA"." / ".$this->session->userdata('ses_desc_type'),1,1,'L',1); 
	$pdf->cell(6.7,0.5,"Tahun / Warna : ",1,0,'L',1);  	
	$pdf->cell(12.3,0.5,$this->session->userdata('ses_year')."/".$this->session->userdata('ses_colour'),1,1,'L',1);  	
	$pdf->cell(6.7,0.5,"No Rangka /No Mesin :",1,0,'L',1); 
	$pdf->cell(12.3,0.5,$this->session->userdata('ses_vin')." / ".$this->session->userdata('ses_engine'),1,1,'L',1); 
	$pdf->cell(6.7,0.5,"No Buku Service :",1,0,'L',1); 
	$pdf->cell(12.3,0.5,"",1,1,'L',1);
	
	$pdf->Ln(0.0);
	$pdf->setFont('Arial','B',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.7,"PERLENGKAPAN",1,0,'C',1);  
	
	$pdf->Ln(0.7);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(4.7,0.5,  "Ban serep  :",1,0,'L',1); 
	$pdf->cell(4.7,0.5,  "Ada / Tidak",1,0,'L',1);
	$pdf->cell(4.7,0.5, "Antena 	 :",1,0,'L',1);
	$pdf->cell(4.9,0.5, "Ada / Tidak",1,1,'L',1);  	
	$pdf->cell(4.7,0.5,  "Tool Kit  :",1,0,'L',1); 
	$pdf->cell(4.7,0.5,  "Ada / Tidak",1,0,'L',1);
	$pdf->cell(4.7,0.5, "Kunci Utama 	 :",1,0,'L',1);
	$pdf->cell(4.9,0.5, "Ada / Tidak",1,1,'L',1);  	
	$pdf->cell(4.7,0.5,  "Donkrak  :",1,0,'L',1); 
	$pdf->cell(4.7,0.5,  "Ada / Tidak",1,0,'L',1);
	$pdf->cell(4.7,0.5, "Kunci Serep 	 :",1,0,'L',1);
	$pdf->cell(4.9,0.5, "Ada / Tidak",1,1,'L',1); 
	$pdf->cell(4.7,0.5,  "Buku Manual  :",1,0,'L',1); 
	$pdf->cell(4.7,0.5,  "Ada / Tidak",1,0,'L',1);
	$pdf->cell(4.7,0.5, "Kaca Film	 :",1,0,'L',1);
	$pdf->cell(4.9,0.5, "Ada / Tidak",1,1,'L',1);  
	$pdf->cell(4.7,0.5,  "Buku sevice  :",1,0,'L',1); 
	$pdf->cell(4.7,0.5,  "Ada / Tidak",1,0,'L',1);
	$pdf->cell(4.7,0.5, "Sauvernir	 :",1,0,'L',1);
	$pdf->cell(4.9,0.5, "Ada / Tidak",1,1,'L',1); 
	$pdf->cell(4.7,0.5,  "Karpet  :",1,0,'L',1); 
	$pdf->cell(4.7,0.5,  "Ada / Tidak",1,0,'L',1);
	$pdf->cell(4.7,0.5, "Velg	 :",1,0,'L',1);
	$pdf->cell(4.9,0.5, "Ada / Tidak",1,1,'L',1); 
	$pdf->cell(4.7,0.5, "AudioKit	:",1,0,'L',1);
	$pdf->cell(4.7,0.5, "Ada / Tidak",1,0,'L',1); 
	$pdf->cell(4.7,0.5, "GPS	:",1,0,'L',1);
	$pdf->cell(4.9,0.5, "Ada / Tidak",1,1,'L',1); 
	//$pdf->cell(14.3,0.5, "Ada / Tidak",1,1,'L',1); 
	
	$pdf->Ln(0.0);
	$pdf->setFont('Arial','B',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.7,"LAIN-LAIN :",1,1,'L',1); 	
				
	$pdf->Ln(0.0);
	$pdf->setFont('Arial','B',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.7,"SIGNATURE",1,1,'C',1);
	
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);
	$pdf->cell(9.5,0.7,"Yang Menyerahkan",1,0,'C',1); 
	$pdf->cell(9.5,0.7,"Yang Menerima",1,1,'C',1);  	  
	$pdf->cell(4.8,1," ",1,0,'L',1); 
	$pdf->cell(4.7,1," ",1,0,'L',1); 
	$pdf->cell(9.5,1," ",1,1,'L',1); 	 	
	$pdf->cell(4.8,0.7,"Office",1,0,'L',1); 
	$pdf->cell(4.7,0.7,$this->session->userdata('head_user'),1,0,'C',1); 
	$pdf->cell(9.5,0.7,"....",1,1,'C',1);  
	
	$pdf->Ln(0.2);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.7,"Catatan :",0,1,'L',1);
	$pdf->cell(19,0.4,"Barang dalam keadaan baik/baru dan telah diperiksa bersama oleh penerima dan yang menyerahkan",0,1,'L',1);
	$pdf->cell(19,0.4,"saat berserah terima dilakukan, Claim sesudah serah terima tidak kami layani mohon agar di cek dengan baik",0,1,'L',1);
	$pdf->cell(19,0.4,"#Putih - Customer * Merah - Admin * Kuning - Security * Hijau - Sales",0,1,'L',1);
		
	
	/*
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',9);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(100,0,"Amount Of Money :".number_format($this->session->userdata('ses_grand'),2,'.',','),0,1,'L',1); 
	
	
	$pdf->Ln(0.5);				
	$no = 1;// intuk index petama	
	$pdf->SetFont('Arial','B',9);
	$pdf->SetWidths(array(1, 5, 5, 1,3.7,3.7));
	$pdf->SetHeight(0.1);
	$pdf->Row(array("No", "Description", "Specs", "Qty","Price","Total"));
	
	
	
	$pdf->setFont('Arial','B',10);
	$pdf->setFillColor(255,255,255);               
	$pdf->cell(15.7,0.6,"Grand Total :"." ".$key->currency." ",1,0,'R',1);
	$pdf->cell(3.7,0.6,number_format($key->gran_total,2,'.',','),1,1,'R',1);
	$pdf->cell(19.4,0.6,"Mention:"." "."(".terbilang($key->gran_total).")",1,1,'L',1);
	
	$pdf->Ln(0.6);				
	$pdf->setFont('Arial','B',8);
	$pdf->setFillColor(255,255,255);    	
	$pdf->cell(13.4,0.5,"",0,0,'C',1);  	// buat header jabatan yg menandatangani		    				
	$pdf->cell(3,0.5,"Manag.Dept / Head",1,0,'C',1);    
	$pdf->cell(3,0.5,"Applicant",1,1,'C',1);    								                              			   				
	
	$pdf->cell(13.4,1.5,"",0,0,'C',1); //buat space tanda tangan 			    					
	$pdf->cell(3,1.5,"",1,0,'C',1);    
	$pdf->cell(3,1.5,"",1,1,'C',1);     								                              			
	
	$pdf->setFont('Arial','I'.'U',8);
	$pdf->setFillColor(255,255,255); 
	$pdf->cell(13.4,0.5,"",0,0,'C',1);  // buat nama yg menandatangani			    	
	$pdf->cell(3,0.5,$this->session->userdata('sign_head'),1,0,'C',1);  
	$pdf->cell(3,0.5,$this->session->userdata('name'),1,1,'C',1);  
   
    $pdf->Ln(0.3);
	$pdf->setFont('Arial','B'.'I',8);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(0,0,"Note: PP Has Been Approved By B.O.D "." ".$this->session->userdata('sign_bod')." "."and also Approved By F.C "." ".$this->session->userdata('sign_fc') ,0,1,'L',1); */
				 
	$pdf->Output(); //hasil out put ke browser
	
?>