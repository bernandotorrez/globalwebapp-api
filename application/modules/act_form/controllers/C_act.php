<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_act extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_act');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_act->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}	
	 	}
		
	public function index()
		{
		$data['act']=$this->M_act->get_all_act();
		$data['coa']=$this->M_act->get_all_coa();
		$data['company']=$this->M_act->get_all_company();
		$data['divisi']=$this->M_act->get_all_divisi();
		$data['divisi2']=$this->M_act->get_all_divisi();
		$data['department']=$this->M_act->get_all_dept();
		$data['total']=$this->M_act->get_count_id();
		$data['show_view'] = 'act_form/V_act';
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function ajax_list()
	{
		$list = $this->M_act->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tbl_act) {
			$chk_idmaster ='<div align="center"><input id="checkact" name="checkact" type="checkbox" value='.$tbl_act->id_act.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $tbl_act->id_act;
			$row[] = $tbl_act->nomor_act;
			$row[] = $tbl_act->desc_act;
			$row[] = $tbl_act->nomor_coa;
			$row[] = $tbl_act->desc_coa;
			$row[] = $tbl_act->act_amount;
			$row[] = $tbl_act->company;
			$row[] = $tbl_act->divisi;
			$row[] = $tbl_act->dept;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_act->count_all(),
						"recordsFiltered" => $this->M_act->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	public function add_act()
		{
		$act_amount = $this->input->post('act_amount');
		
		if(empty($_POST["nomor_act"])){
			die ("Field Nomor ACT must be filled in!! ");
		}
		elseif(empty($_POST["desc_act"])){
			die ("Field Description must be filled in!! ");
		}
		elseif(!preg_match('/^[0-9]{1,3}(,[0-9]{3})*(.[0-9]+)?$/', $act_amount)){
			die ("Field Amount must be filled in!! ");
		}
		elseif(empty($_POST["id_company"])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_POST["id_dept"])){
			die ("Field Department must be filled in!! ");
		}
		else{
			$data = array(
					'id_act' => $this->input->post('id_act'),
					'nomor_act' => $this->input->post('nomor_act'),
					'desc_act' => $this->input->post('desc_act'),
					'act_amount' => $this->input->post('act_amount'),
					'id_company' => $this->input->post('id_company'),
					'id_divisi' => $this->input->post('id_divisi'),
					'id_dept' => $this->input->post('id_dept'),
					'status' => '1',
				);
			$insert = $this->M_act->add_act($data);
		  }
		  echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_act->get_by_id($id);
			echo json_encode($data);
		}

	public function update_act()
		{
		$act_amount = $this->input->post('act_amount');
		
		if(empty($_POST["nomor_act"])){
			die ("Field Nomor ACT must be filled in!! ");
		}
		elseif(empty($_POST["desc_act"])){
			die ("Field Description must be filled in!! ");
		}
		elseif(!preg_match('/^-?[0-9]+(?:\.[0-9]{1,2})?$/', $act_amount)){
			die ("Field Amount must be filled in!! ");
		}
		elseif(empty($_POST["id_company"])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_POST["id_dept"])){
			die ("Field Department must be filled in!! ");
		}
		else{
			$data = array(
					'id_act' => $this->input->post('id_act'),
					'nomor_act' => $this->input->post('nomor_act'),
					'desc_act' => $this->input->post('desc_act'),
					'act_amount' => $this->input->post('act_amount'),
					'id_company' => $this->input->post('id_company'),
					'id_divisi' => $this->input->post('id_divisi'),
					'id_dept' => $this->input->post('id_dept'),
					'status' => '1',
				);
		$this->M_act->update_act(array('id_act' => $this->input->post('id_act')), $data);
		  }
		   echo 'Insert';
		}
	
	public function ajax_divisi()
    {
        $id = $this->input->post('id_coa');
		$data = $this->M_act->get_by_coa($id);
		echo json_encode($data);
    }
	
	public function ajax_coa()
    {
        $id = $this->input->post('id_coa');
		$this->db->from('qv_coa');
		$this->db->where('id_coa', $id);
		$data=$this->db->get();
		echo json_encode($data);
    }
	
	public function tampilkandatacompanydivisidepartemen(){
		$this->load->view('tampilcompanydivisidepartemen');
	}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);	
				$this->db->where('id_act', $id);
				$this->db->update('tbl_actno', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}



}
