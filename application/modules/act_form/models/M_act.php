<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_act extends CI_Model
{

	var $table = 'tbl_actno';
	var $tablenew = 'qv_actno';
	var $column_order = array(null, null,'id_act','nomor_act','desc_act','nomor_coa','desc_coa','act_amount','company','divisi','dept'); //set column field database for datatable orderable
	var $column_search = array('id_act','nomor_act','desc_act','nomor_coa','desc_coa','act_amount','company','divisi','dept'); //set column field database for datatable searchable 
	var $order = array('id_act' => 'desc'); // default order 
	
    
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	private function _get_datatables_query()
	{
		
		$this->db->from($this->tablenew);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$this->db->where('status = 1');
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$this->db->where('status = 1');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->tablenew);
		$this->db->where('status = 1');
		return $this->db->count_all_results();
	}
	
	public function get_all_act()
		{
			$this->db->from('tbl_actno');
			$this->db->where('status = 1');
			$query=$this->db->get();
			return $query->result();
		}
		
	public function get_all_coa()
		{
			$this->db->from('tbl_coa');
			$this->db->where('status = 1');
			$query=$this->db->get();
			return $query->result();
		}
	
	public function get_all_company()
		{
			$this->db->select('id_company, company');
			$this->db->from('tbl_company');
			$this->db->where('status = 1');
			$query=$this->db->get();
			return $query->result();
		}
	
	public function get_all_dept()
		{
			$this->db->select('id_dept, dept');
			$this->db->from('tbl_dept');
			$this->db->where('status = 1');
			$query=$this->db->get();
			return $query->result();
		}
	
	public function get_all_divisi()
		{
			$this->db->from('tbl_divisi');
			$this->db->where('status = 1');
			$query=$this->db->get();
			return $query->result();

		}
	
	public function get_count_id()
		{
			$this->db->select('max(id_act) as totaldata');
			$this->db->from('tbl_actno');
			$query=$this->db->get();
			return $query->result();
		}
		
	public function get_by_id($id)
		{
			$this->db->from($this->table);
			$this->db->where('id_act',$id);
			$query = $this->db->get();
			return $query->row();
		}
	
	public function get_by_coa($id)
		{
			$this->db->from('qv_coa');
			$this->db->where('id_coa',$id);
			$query = $this->db->get();
			return $query->row();
		}
		
	public function get_company($id)
		{
        $q = $this->db->query("select * from qv_coa where id_coa='$id'");
        return $q;
		}
		
	public function get_divisi($id)
		{
        $q = $this->db->query("select * from qv_coa where id_coa='$id'");
        return $q;
		}
		
	public function get_dept($id)
		{
        $q = $this->db->query("select * from qv_coa where id_coa='$id'");
        return $q;
		}
		
	public function get_coa($id)
		{
		$this->db->from('qv_coa');
		$this->db->where('status = 1');
		$query=$this->db->get();
		return $query->result();
		}
	
	public function add_act($data)
		{
			$this->db->insert($this->table, $data);
			return $this->db->insert_id();
		}

	public function update_act($where, $data)
		{
			$this->db->update($this->table, $data, $where);
			return $this->db->affected_rows();
		}

	public function delete_by_id($id)
		{
			$this->db->where('id_act', $id);
			$this->db->delete($this->table);
		}
	
	public function get_value($where, $val, $table){
		$this->db->where($where, $val);
		$_r = $this->db->get($table);
		$_l = $_r->result();
		return $_l;
	}


}
