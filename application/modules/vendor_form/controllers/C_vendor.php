<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_vendor extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_vendor');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_vendor->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}	
	 	}
		
	public function index()
		{
		$data['vendor']=$this->M_vendor->get_all_vendor();
		$data['company']=$this->M_vendor->get_all_company();
		$data['dept']=$this->M_vendor->get_all_dept();
		//$data['divisi']=$this->M_vendor->get_all_divisi();
		//$data['divisi2']=$this->M_vendor->get_all_divisi();
		$data['department'] = $this->M_vendor->get_all_dept();
		$data['total']=$this->M_vendor->get_count_id();
		$data['show_view'] = 'vendor_form/V_vendor';
		$dept  = $this->session->userdata('dept') ;	
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('short') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function ajax_list()
	{
		$list = $this->M_vendor->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $qv_vendor) {
			$chk_idmaster ='<div align="center"><input id="checkvendor" name="checkvendor" type="checkbox" value='.$qv_vendor->id_vendor.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $qv_vendor->id_vendor;
			$row[] = $qv_vendor->vendor;
			$row[] = $qv_vendor->alamat;
			$row[] = $qv_vendor->notelp;
			$row[] = $qv_vendor->email;
			$row[] = $qv_vendor->fax;
			$row[] = $qv_vendor->kode_pos;
			$row[] = $qv_vendor->npwp;
			$row[] = $qv_vendor->company;
			$row[] = $qv_vendor->dept;
			$row[] = $qv_vendor->date_create;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_vendor->count_all(),
						"recordsFiltered" => $this->M_vendor->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->select('id_vendor');
		$this->db->from('qv_vendor');
		$this->db->where('status','1');
		$total = $this->db->count_all_results();
		//$total=$this->db->count_all_results("qv_vendor");

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_vendor",$search);
		$this->db->where('status','1');
		$this->db->or_like("vendor",$search);
		$this->db->where('status','1');
		$this->db->or_like("alamat",$search);
		$this->db->where('status','1');
		$this->db->or_like("notelp",$search);
		$this->db->where('status','1');
		$this->db->or_like("email",$search);
		$this->db->where('status','1');
		$this->db->or_like("fax",$search);
		$this->db->where('status','1');
		$this->db->or_like("company",$search);
		$this->db->where('status','1');
		$this->db->or_like("dept",$search);
		$this->db->where('status','1');
		$this->db->or_like("create_by",$search);
		$this->db->where('status','1');
		}


		/*Lanjutkan pencarian ke database*/
		$this->db->limit($length,$start);
		$this->db->where('status','1');
		/*Urutkan dari alphabet paling terkahir*/
		$this->db->order_by('id_vendor','ASC');
		$query=$this->db->get('qv_vendor');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("vendor",$search);
		$jum=$this->db->get('qv_vendor');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}


		
		foreach ($query->result_array() as $tbl_vendor) {
		$chk_idmaster ='<div align="center"><input id="checkvendor" name="checkvendor" type="checkbox" value='.$tbl_vendor["id_vendor"].' class="editRow ace" req_id_del='.$tbl_vendor["id_vendor"].' />
          <span class="lbl"></span> ';
		$tanggal = date('d-m-Y', strtotime($tbl_vendor["tgl_input"])); 

			$output['data'][]=array($tbl_vendor['id_vendor'],$tbl_vendor['vendor'],
									$tbl_vendor['alamat'],$tbl_vendor['notelp'],
									$tbl_vendor['email'],$tbl_vendor['fax'],
									$tbl_vendor['company'],$tbl_vendor['dept'],
									$tanggal,$tbl_vendor['create_by'],
									$chk_idmaster
									);

		}

		echo json_encode($output);


	}

	
	public function add_vendor()
		{
		$email = $this->input->post('email');
		$phone = $this->input->post('notelp');

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_add = 'Action : Insert | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_add = date('Y-m-d H:i:s');

		if(empty($_POST["id_vendor"])){
			die ("Field Code Vendor must be filled in!! ");
		}
		elseif(empty($_POST["vendor"])){
			die ("Field Name Vendor must be filled in!! ");
		}
		elseif(empty($_POST["alamat"])){
			die ("Field Address must be filled in!! ");
		}
		elseif(empty($phone) || !preg_match('/^[+#*\(\)\[\]]*([0-9][ ext+-pw#*\(\)\[\]]*){6,45}$/', $phone)){
			die ("Field Phone must be filled in and numeric!! ");
		}
		elseif(empty($_POST["email"])){
			die ("Field Email must be filled in!! ");
		}
		elseif(!preg_match('/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/', $email)) {
			die ("Field Email isn't valid! ");
		}
		elseif(empty($_POST["fax"])){
			die ("Field Fax must be filled in!! ");
		}
		elseif(empty($_POST["kode_pos"])){
			die ("Field Postal Code must be filled in!! ");
		}
		elseif(empty($_POST["npwp"])){
			die ("Field NPWP must be filled in!! ");
		}
		elseif(empty($_POST["id_company"])){
			die ("Field Company must be filled in!! ");
		}
		
		elseif(empty($_POST["id_dept"])){
			die ("Field Dept must be filled in!! ");
		}
		elseif(empty($_POST["create_by"])){
			die ("Field Create By must be filled in!! ");
		}
		else{
			$data = array(
					'id_vendor' => $this->input->post('id_vendor'),
					//'code_vendor' => $this->input->post('code_vendor'),
					'vendor' => $this->input->post('vendor'),
					'alamat' => $this->input->post('alamat'),
					'notelp' => $this->input->post('notelp'),
					'email' => $this->input->post('email'),
					'fax' => $this->input->post('fax'),
					'kode_pos' => $this->input->post('kode_pos'),
					'npwp' => $this->input->post('npwp'),
					'id_company' => $this->input->post('id_company'),
					//'id_divisi' => $this->input->post('id_divisi'),
					'id_dept' => $this->input->post('id_dept'),
					'tgl_input' => $this->input->post('tgl_input'),
					'create_by' => $this->input->post('create_by'),
					'contact_person' => $this->input->post('contact_person'),
					'status' => '1',
					'date_create' => $date_add,
					'remarks' => $remarks_add
				);
			$insert = $this->M_vendor->add_vendor($data);
		  }
		  echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_vendor->get_by_id($id);
			echo json_encode($data);
		}

	public function update_vendor()
		{
		$email = $this->input->post('email');
		$phone = $this->input->post('notelp');

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_update = 'Action : Update | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_update = date('Y-m-d H:i:s');

		if(empty($_POST["id_vendor"])){
			die ("Field Code Vendor must be filled in!! ");
		}
		elseif(empty($_POST["vendor"])){
			die ("Field Name Vendor must be filled in!! ");
		}
		elseif(empty($_POST["alamat"])){
			die ("Field Address must be filled in!! ");
		}
		elseif(empty($phone) || !preg_match('/^[+#*\(\)\[\]]*([0-9][ ext+-pw#*\(\)\[\]]*){6,45}$/', $phone)){
			die ("Field Phone must be filled in and numeric!! ");
		}
		elseif(empty($_POST["email"])){
			die ("Field Email must be filled in!! ");
		}
		elseif(!preg_match('/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/', $email)) {
			die ("Field Email isn't valid! ");
		}
		elseif(empty($_POST["fax"])){
			die ("Field Fax must be filled in!! ");
		}
		elseif(empty($_POST["kode_pos"])){
			die ("Field Postal Code must be filled in!! ");
		}
		elseif(empty($_POST["npwp"])){
			die ("Field NPWP must be filled in!! ");
		}
		elseif(empty($_POST["id_company"])){
			die ("Field Company must be filled in!! ");
		}
		elseif(empty($_POST["id_dept"])){
			die ("Field Dept must be filled in!! ");
		}
		elseif(empty($_POST["create_by"])){
			die ("Field Create By must be filled in!! ");
		}
		else{
			$data = array(
					'id_vendor' => $this->input->post('id_vendor'),
					//'code_vendor' => $this->input->post('code_vendor'),
					'vendor' => $this->input->post('vendor'),
					'alamat' => $this->input->post('alamat'),
					'notelp' => $this->input->post('notelp'),
					'email' => $this->input->post('email'),
					'fax' => $this->input->post('fax'),
					'kode_pos' => $this->input->post('kode_pos'),
					'npwp' => $this->input->post('npwp'),
					'id_company' => $this->input->post('id_company'),
					//'id_divisi' => $this->input->post('id_divisi'),
					'id_dept' => $this->input->post('id_dept'),
					'tgl_input' => $this->input->post('tgl_input'),
					'create_by' => $this->input->post('create_by'),
					'status' => '1',
					'date_update' => $date_update,
					'remarks' => $remarks_update
				);
		$this->M_vendor->update_vendor(array('id_vendor' => $this->input->post('id_vendor')), $data);
		  }
		  echo 'Insert';
		}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];

		// Log
		$id = $this->session->userdata('id');
		$username = $this->session->userdata('username');
		$nama = $this->session->userdata('name');
		$remarks_delete = 'Action : Delete | User Created : id -> '.$id.' , username -> '.$username.' , nama -> '.$nama;
		$date_delete = date('Y-m-d H:i:s');

		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
						'date_update' => $date_delete,
						'remarks' => $remarks_delete
				);	
				$this->db->where('id_vendor', $id);
				$this->db->update('tbl_vendor', $data);
			}
		}
	}
	
	public function ajax_divisi()
    {
        $id = $this->input->post('id_divisi');
        $data['dept'] = $this->M_vendor->get_dept($id);
        $this->load->view('dept',$data);
    }
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}



}
