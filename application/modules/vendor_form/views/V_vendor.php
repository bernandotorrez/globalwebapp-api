<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>
<div>
		<button onclick="add_vendor()" class="btn btn-app btn-primary btn-xs radius-4 caddnew" type="submit">
			<i class="ace-icon fa fa-book bigger-160"></i>
            Add
		</button>
		<button onclick="delete_vendor()" class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="submit" id="deleteTriger" name="deleteTriger"  value ="btndel"  disabled="disabled" >
            <i class="ace-icon fa fa-ban bigger-160"></i>
            Delete
		</button>
		<button onclick="edit_vendor()" class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="editTriger"  name="editTriger"  value ="btnedit"  disabled="disabled" >
            <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
            Edit
		</button> 
</div>
<br/>
<div class="table-header btn-info"> <?php echo " ".$header ;?>   </div>  
	<div style="padding-top:20px;padding-bottom:20px;background-color:#EFF3F8"> 

  <div class="form-group col-xs-6">
        <div class="col-xs-10">
            <div class="form-inline ">
                <div class="form-group">Date start</div>
                
                <div class="form-group">
                    <div class="input-group date">
                       <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                       </div>
                       
                       <input type="text" class="form-control input-daterange" id="start_date" name="start_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                    </div> <!-- /.input-group datep -->       
                </div>
               
               <div class="form-group">
                       <label for="From" class="col-xs-1 control-label">To</label>
               </div>
              
               <div class="form-group">
                      <div class="input-group date">
                               <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                               </div>
                                     <input type="text" class="form-control input-daterange" id="end_date" name="end_date" data-date-format="dd-mm-yyyy" autocomplete="off">
                       </div>  
               </div>
                
            </div>
        </div>
      </div>

		<table id="myTable" cellpadding="0" cellspacing="0" width="100%"  class="table table-striped table-bordered table-hover">
			<thead class="text-warning" >
					<th width="5%" style="text-align:center">
						<label class="pos-rel">
 							<input type="checkbox" class="ace ace-checkbox-1" id="checkAll"/>
              <span class="lbl"></span>
            </label>
          </th>
					<th>No</th>
	        <th>Id</th>
	        <th>Name Vendor</th>
					<th>Address</th>
					<th>Telp</th>
					<th>Email</th>
					<th>Fax</th>
					<th>Postal Code</th>
					<th>NPWP</th>
					<th>Company</th>
					<th>Dept</th>
          <th>Date Create</th>
	    </thead>
	  </table> 
  </div>                               
</div>      
                                 	                       
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Vendor Form</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="id_vendor" />
          <div class="form-body" style="padding-top:10px">
            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">ID Vendor</label>
              <div class="col-md-8">
                <?php
				$totaldata = $total[0]->totaldata+1;
				?>
                <input name="id_vendor" class="col-md-9" type="text" readonly value="<?php echo $totaldata ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Name Vendor</label>
              <div class="col-md-8">
                <input name="vendor" placeholder="PT ADITYA OCTAVO LUMECCO" class="col-md-9" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Address</label>
              <div class="col-md-8">
                <input name="alamat" placeholder="Bukit Gading Indah Blok G No.15" class="col-md-9" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Phone</label>
              <div class="col-md-8">
                <input name="notelp" placeholder="45840101" class="col-md-9" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Contact Person</label>
              <div class="col-md-8">
                <input name="contact_person" placeholder="45840101" class="col-md-9" type="text">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Email</label>
              <div class="col-md-8">
                <input name="email" placeholder="siti@aol.com" class="col-md-9" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Fax</label>
              <div class="col-md-8">
                <input name="fax" placeholder="45840202" class="col-md-9" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Postal Code</label>
              <div class="col-md-8">
                <input name="kode_pos" placeholder="16455" class="col-md-9" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">NPWP</label>
              <div class="col-md-8">
                <input name="npwp" placeholder="45840202" class="col-md-9" type="text">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Company</label>
              <div class="col-md-8">
                <select class="col-md-9" name="id_company" id="id_company">
                  <?php foreach($company as $aaa) { ?>
                  <option value="<?php echo $aaa->id_company;?>"><?php echo $aaa->company;?>
                  </option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <!-- <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Group Divisi</label>
              <div class="col-sm-8">
                <select class="col-md-9" name="id_divisi" id="id_divisi">
                  <option value="">Choose Divisi</option>
                  <?php foreach($divisi as $abc) { ?>
                  <option value="<?php echo $abc->id_divisi;?>"><?php echo $abc->divisi;?>
                  </option>
                  <?php } ?>
                </select>
              </div>
            </div> -->

            <!-- <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Department</label>
              <div class="col-sm-8">
                <select class="col-md-9" name="id_dept" id="id_dept">
                  <option value="">Select Department</option>
                  <?php foreach($divisi2 as $bbb) { 
						$sqlpo	  = "select * from tbl_dept where id_divisi='".$bbb->id_divisi."'";
						$qrypo	  = $this->db->query($sqlpo);
						$adapo 	  = $qrypo->result();					
						foreach($adapo as $aaal) { ?>
                  <option value="<?php echo $aaal->id_dept;?>"><?php echo $aaal->dept;?>
                  </option>
                  <?php } }?>
                </select>
              </div>
            </div> -->

            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Department</label>
              <div class="col-sm-8">
                <select class="col-md-9" name="id_dept" id="id_dept">
                  <option value="">Select Department</option>

                  <?php foreach($department as $row) { ?>
                      <option value="<?=$row->id_dept;?>"><?=$row->dept;?></option>
                  <?php } ?>

                
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Date Input</label>
              <div class="col-md-8">
                <input name="tgl_input" class="col-md-9" type="text" readonly value="<?php echo date('Y-m-d'); ?>">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4" style="padding-right:10px">Create By</label>
              <div class="col-md-8">
                <input name="create_by" placeholder="Create By" value="<?=$this->session->userdata('username');?>" class="col-md-9" type="text" readonly>
              </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


  <script>


//$('#myTable').dataTable();
//-----------------------------------------data table custome----	
	
var rows_selected = [];
// var tableUsers = $('#myTable').DataTable({
// 		"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
// 		"dom": 'lfBrtip',
// 		"buttons": [
//             {
// 		   "extend":    'copy',
// 		   "text":      '<i class="fa fa-files-o"></i> Copy',
// 		   "titleAttr": 'Copy'
// 		   },
// 		   {
// 		   "extend":    'print',
// 		   "text":      '<i class="fa fa-print" aria-hidden="true"></i> Print',
// 		   "titleAttr": 'Print',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   },
// 		   {
// 		   "extend":    'excel',
// 		   "text":      '<i class="fa fa-file-excel-o"></i> Excel',
// 		   "titleAttr": 'Excel'
// 		   },
// 		   {
// 		   "extend":    'pdf',
// 		   "text":      '<i class="fa fa-file-pdf-o"></i> PDF',
// 		   "titleAttr": 'PDF',
// 		   "orientation": 'landscape',
//            "pageSize": 'A4'
// 		   }
//         ],
//         "autoWidth" : false,
// 		"scrollY" : '250',
// 		"scrollX" : true,
//         "processing": true, //Feature control the processing indicator.
//         "serverSide": true, //Feature control DataTables' server-side processing mode.
//         // Load data for the table's content from an Ajax source
//         "ajax": {
//             "url": "<?php echo base_url('vendor_form/C_vendor/ajax_list') ?>",
//             "type": "POST"
//         },
// 		'columnDefs': [
// 		{
//         'targets': [0],
// 		'orderable': false,
// 		}
// 		],

//       'order': [[2, 'desc']],
//         });
//end--------------------------------------------------------------
		
//check all--------------------------
$('#checkAll').change(function(){
	
	$('#editTriger').prop("disabled", true);
	var table = $('#myTable').DataTable();
    var cells = table.cells( ).nodes();
    $( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));		
});
//end---------------------------------			

//aktif edit--------------------------------------------------------

var counterCheckededit = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckededit++ : counterCheckededit--;
    counterCheckededit == 1 ? $('#editTriger').prop("disabled", false): $('#editTriger').prop("disabled", true);
});

//end---------------------------------------------------------------

//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {

	this.checked ? counterChecked++ : counterChecked--;
    counterChecked > 0 ? $('#deleteTriger').prop("disabled", false): $('#deleteTriger').prop("disabled", true);

});
//--------------------------------------------------------------------
</script>

<!-- Datatable -->
<script>
$(document).ready(function(){ 
		$('.input-daterange').datepicker({
			todayBtn: 'linked',
			dateFormat: "dd-mm-yy",
			format: "yyyy-mm-dd",
			todayHighlight: true,
			autoclose: true
		});
		
	    fetch_data('no'); //
	
		function fetch_data(is_date_search,start_date='', end_date=''){ //ambil data table
			var dataTable = $('#myTable').DataTable({
			autoWidth : false,
			processing: true,
			serverSide: true,	
			"scrollY": 250,
			"scrollX": true,
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
				"ajax":{
				    url: "<?php echo base_url('vendor_form/C_vendor/ajax_list') ?>",
					type: "POST",
					data:{
						is_date_search:is_date_search, start_date:start_date, end_date:end_date
					},
				 },
				 
				 'columnDefs': [
							{
							'targets': [0],
							'orderable': false,
							}
					],

						'order': [[2, 'desc']],
			 });
		  }
		  	  
		  
//proses change date-----------------------------------------------
		$('#start_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				 $('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
		$('#end_date').change(function(){
			var start_date =    $('#start_date').val();
			var end_date   =    $('#end_date').val();
		
			if(start_date != '' && end_date != ''){
				$('#myTable').DataTable().destroy();
				 fetch_data('yes', start_date, end_date);		
			} else if(start_date == '' && end_date == ''){ 
				$('#myTable').DataTable().destroy();
				 fetch_data('no');
			}
		});
		
 //end change date----------------------------------------------------------------------	


  }); //end document on ready	
		
</script>

<script>
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
</script>


	<script type="text/javascript">
	var save_method; //for save method string
    var table;
		
    function add_vendor()
    {
      save_method = 'add';
      $('#modal_form').modal('show'); // show bootstrap modal
    //$('.modal-title').text('Add Person'); // Set Title to Bootstrap modal title
    }
	
	function edit_vendor(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
		
		if( $('.editRow:checked').length > 1 ){
			alert("Just One Allowed Data!!!");
		}
		else{
		var eee = $('.editRow:checked').val();
		}
		
      //Ajax Load data from ajax
       $.ajax({
        url : "<?php echo site_url('vendor_form/C_vendor/ajax_edit/')?>/" + eee,
		type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_vendor"]').val(data.id_vendor);
            $('[name="vendor"]').val(data.vendor);
			$('[name="alamat"]').val(data.alamat);
			$('[name="notelp"]').val(data.notelp);
			$('[name="email"]').val(data.email);
			$('[name="fax"]').val(data.fax);
			$('[name="kode_pos"]').val(data.kode_pos);
			$('[name="npwp"]').val(data.npwp);
			$('[name="id_company"]').val(data.id_company);
			$('[name="id_dept"]').val(data.id_dept);
			$('[name="tgl_input"]').val(data.tgl_input);
			$('[name="create_by"]').val(data.create_by);
      $('[name="contact_person"]').val(data.contact_person);
            
            $('#modal_form').modal({backdrop: 'static', keyboard: false}); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Vendor'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }



     function save()
    {
      var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('vendor_form/C_vendor/add_vendor')?>";
      }
      else
      {
		   url = "<?php echo site_url('vendor_form/C_vendor/update_vendor')?>";
      }
		  var formData = new FormData(document.getElementById('form')) 	    
          $.ajax({
            url : url,
            type: "POST",
            data: formData,										
			processData: false,															
			async: false,
			processData: false,
			contentType: false,		
			cache : false,									
			success: function (data, textStatus, jqXHR)            
            {
               if(data=='Insert'){
				$('#modal_form').modal('hide');
				location.reload();// for reload a page
			   }else {
			   alert (data);
			   }
            },
        });
    }

     function delete_vendor()
    {
		
			if( $('.editRow:checked').length >= 1 ){
				var ids = [];
				$('.editRow').each(function(){
					if($(this).is(':checked')) { 
						ids.push($(this).val());
					}
				});
				var rss = confirm("Are you sure you want to delete this data???");
				if (rss == true){
					var ids_string = ids.toString();
					$.post('<?=@base_url('vendor_form/C_vendor/deletedata')?>',{ID:ids_string},function(result){ 
						  $('#modal_form').modal('hide');
						  location.reload();// for reload a page
					}); 
				}
			}
			
		

      
    }
	
  </script>

  

</html>
