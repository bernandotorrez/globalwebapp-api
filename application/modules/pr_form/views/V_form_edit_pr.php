

<script type="text/javascript">
//--------------function number only
 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true ;
		}      
}	


</script>


<script script type="text/javascript">

//calculate and add field--------------
$(function(){		
	$('#add').click(function(){
	  addnewrow();	
	 }); 	
	 
	
	 $('body').delegate('.remove','click',function(){				 				 
	 	 $(this).parent().parent().remove();
		  		
		    var tr =  $(this).parent().parent();  
		    var get_iddet =  tr.find('.txtid').val();//getiddetailpp from 									
		    var buff_iddet =$('.buffarray_iddet').val()			
			
		    if  (buff_iddet == "") { 	
			    $('.buffarray_iddet').val(get_iddet);
			}else{
		        $('.buffarray_iddet').val($('.buffarray_iddet').val() + "," + get_iddet );
			} 
			//end--------------------------------------------------------------------------
		     total();
		   			  		  			 		 
	 });
	 	
	 
	  $('body').delegate('.quantity,.price','change',function(){  
		   var tr =  $(this).parent().parent();
		   var qty=numeral().unformat(tr.find('.quantity').val());
		   var price=numeral().unformat(tr.find('.price').val());
		   var amt=numeral().unformat(tr.find('.amount').val());
		   var stattaxppn =  tr.find(".txtstat").val();
		   var stattaxpph =  tr.find(".txtstatpph").val();
		   var amt=qty*price;  
		   
		   tr.find('.amount').val(amt);
		   
		   if(qty <= 0) {
				//alert("Please input Qty above 0")
				tr.find('.quantity').focus()
				$('#error_msg').html('<div class="alert alert-danger">Please input Qty above 0 !</div>')
				$('#btnsavebtn').prop('disabled', true)
			} else {
				$('#error_msg').html('')
				$('#btnsavebtn').prop('disabled', false)
			}
		   			
		   if (stattaxppn == "PPN") { //ppn 10%			   		       		   	
		       var ttlppn= amt * (10/100);
			   tr.find('.taxcharge').val(numeral(ttlppn).format('0,0.00'));								
		   }		   
		  
		    if (stattaxpph =="PPH2%") { //pph 2%			   
		       var ttlpph = amt * (2/100);	   
			   tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));								
		   }else{
			    if (stattaxpph =="PPH4%") { //pph 2%			   
		      	    var ttlpph = amt * (4/100);
				    tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
				}else{	
					if (stattaxpph =="PPH15%") { //pph 2%			   
		      	    	var ttlpph = amt * (15/100);
						 tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
					}else{	
			   			 tr.find('.taxchargepph').val('0,0.00');
					}
				}
		   }		 
		   total(); 
 	 });
	 	 
	 
	$('body').delegate('.quantity,.price','blur',function(){  
		  var tr =  $(this).parent().parent();
		   var qty=tr.find('.quantity').val();
		   var price=tr.find('.price').val();
		   amt=numeral().unformat(tr.find('.amount').val());		
		   
		   tr.find('.quantity').val(numeral(qty).format('0,0')) ;
		   tr.find('.price').val(numeral(price).format('0,0.00')) ;
		   tr.find('.amount').val(numeral(amt).format('0,0.00')) ;
		   total();
 	 });  	
	 
	  $('body').delegate('.txtstat','change',function(){  		
		  var tr =  $(this).parent().parent();
		   var amt = numeral().unformat(tr.find('.amount').val());
		   var stattaxppn =  tr.find(".txtstat").val()	 ;
			
		   if (stattaxppn == "PPN") { //ppn 10%			   		       		   	
		       var ttlppn= amt * (10/100)
			   tr.find('.taxcharge').val(numeral(ttlppn).format('0,0.00'));								
		   }		   		  
		   
		   if (stattaxppn == "NONE") { //none			   
				tr.find('.taxcharge').val('0,0.00');
		   }	
		 
		    //function total----------------
		      total() ;
		    //end---------------------------		   		  	   	   		   
 	 }); 	
	 
	  $('body').delegate('.txtstatpph','click',function(){  		
		   var tr =  $(this).parent().parent();
		   var amt = numeral().unformat(tr.find('.amount').val())	;			 
		   var stattaxpph =  tr.find(".txtstatpph").val();
					 
		   if (stattaxpph =="PPH2%") { //pph 2%			   
		       var ttlpph = amt * (2/100);	   
			   tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));								
		   }else{
			    if (stattaxpph =="PPH4%") { //pph 2%			   
		      	    var ttlpph = amt * (4/100);
					 tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
				}else{	
					if (stattaxpph =="PPH15%") { //pph 2%			   
		      	    	var ttlpph = amt * (15/100);
						 tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
					}else{	
			   			 tr.find('.taxchargepph').val('0,0.00');
						 tr.find('.taxchargepph').val(numeral(ttlpph).format('0,0.00'));
					}
				}
		   }
		 
		    //function total----------------
		      total() ;
		    //end---------------------------	   		  	   	   		   
 	 }); 	
	 	
	 	  	
});


function total()
{
	var t=0;
	var tppn=0;
	var tpph=0;	
	var jmlbaris=parseInt($('.remove').length);
	
	for(var i=0;i<jmlbaris;++i){
	   t+=parseFloat(numeral().unformat($('.amount').eq(i).val()));
	}	
	
	for(var i=0;i<jmlbaris;++i){	
	
	      tppn+=parseFloat(numeral().unformat($('.taxcharge').eq(i).val()));			  
	  	  tpph+=parseFloat(numeral().unformat($('.taxchargepph').eq(i).val()));	   		 		  
	}		
	
			
	var hasilgranppn = (t+tppn) - tpph;
	$('.total').val(numeral(t).format('0,0.00')) ;	
	$('.ppncalc').val(numeral(tppn).format('0,0.00'));
	$('.pphcalc').val(numeral(tpph).format('0,0.00'))	;
	$('.grandppn').val(numeral(hasilgranppn).format('0,0.00'));
	
}


function addnewrow()
{

var strnopp =$('#txtnopp').val()

var n = ($('.detail tr').length-0)+1;	
var tr = '<tr>' +
	'<td class="no" align="center">' + n + '</td>' +
	'<td><input type="text" id="txtidmasdetid" name="txtidmasdet[]"  value="' + strnopp + '"  hidden="hide" /><input type="text" name="txtiddet[]" class="txtid" value="" hidden="hide"  /> <input type="text" name="flagstat[]"  value="1" hidden="hide" /><input type="text" maxlength="70"  name="txtdesc[]"  placeholder="Item Type" size="24" class="form-control txtdesc" /></td>' + '<td><input type="text" maxlength="70"  name="txtspecs[]"  placeholder="Description" size="30" class="form-control txtspecs" /></td>' + '<td><input maxlength="6" id="txtqty" style="text-align: center;" name="txtqty[]" type="text" placeholder="0" size="7" onkeypress="return isNumberKey(event)" class="form-control quantity qty_validation" /></td>' +
	'<td><input maxlength="14" style="text-align: right" name="txtprice[]" type="text" placeholder="0.00" size="30" onkeypress="return isNumberKey(event)" class="form-control price price_validation"/> </td>' +
	'<td><input  maxlength="70" style="text-align: right"  name="txtamount[]"  placeholder="0.00"  size="35"  class="form-control amount" readonly="readonly"/></td>'+ '<td> <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button></td>' +
	'</tr>';
 
 $('.detail').append(tr);

//  var n = ($('.detail tr').length-0)+1;
// var tr = '<tr>'+
// '<td class="no" align="center">'+ n +'</td>'+
// '<td><input type="hidden" id="txtidmasdetid" name="txtidmasdet[]"  value="'+strnopp+'"  hidden="hide" /><input type="hidden" name="txtiddet[]" class="txtid" value="" hidden="hide"  /><td><input type="text" name="flagstat[]"  value="1"  hidden="true"  /> <input type="text" maxlength="70"  name="txtdesc[]"  placeholder="Description" size="24" class="form-control txtdesc" /></td>'+'<td><input type="text" maxlength="70"  name="txtspecs[]"  placeholder="Specs" size="30" class="form-control txtspecs" /></td>'+'<td><input maxlength="6" id="txtqty"  name="txtqty[]" type="text" placeholder="0" size="7" onkeypress="return isNumberKey(event)" class="form-control quantity" /></td>'+
// '<td><input maxlength="14" style="text-align: right;"  name="txtprice[]" type="text" placeholder="0.00" size="30" onkeypress="return isNumberKey(event)" class="form-control price"/> </td>'+
// '<td><input  maxlength="70" style="text-align: right;"  name="txtamount[]"  placeholder="0.00"  size="35"  class="form-control amount" readonly="readonly"/></td>'+'<td><button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button></td>'+
// '</tr>';

//  $('.detail').append(tr);

}


</script>

<script>
$(function(){		
	$('.ppnnot').change(function(){		 		
		 if($(this).is(':checked')){
				  $('.ppncalc').val("0.00");	
				  $('.grandppn').val($('.total').val());						 		 
		 }else{					
			total()
		 }		 		
	});
});
</script>

<script type="text/javascript">
//upload menggunakan userfile type file
$(document).ready(function(){	
	 $('#fileSelect').bind('change',function(event){     	 
	  event.preventDefault(); 	  
	  var formData = new FormData(document.getElementById('form-upload'));		 										
		   $.ajax({					          					
					url:"<?php echo base_url('pr_form/c_create_pr/do_upload_excel');  ?>", 										
					type: 'POST',						
					//enctype: "multipart/form-data", 					
					data: formData,										
					processData: false,															
		    		//async: false,
					processData: false,
					contentType: false,		
					cache : false,	
					beforeSend:function()
					 {
						$("#prog").show();
						$("#prog").attr('value','0');
						   
				     },
					   uploadProgress:function(event,position,total,percentCompelete)
					   {
						  $("#prog").attr('value',percentCompelete); 
						  $("#percent").html(percentCompelete+'%');
					   },		            
									
				    success:function (data, textStatus, jqXHR) {			 											
					  alert(data);
					  $('#hexcel').show();
					  $('#hdel').show();																	
				    }
		  });	 
    });
});

</script>
<script>
//check file exsist 
	$(document).ready(function(e) {		
	   var checkfile = "<?php 
		                 $company_cut =substr($this->session->userdata('short'),0,-1);
				         $id_dept =$this->session->userdata('id_dept');
						 echo base_url('./asset/uploads/'.$company_cut.'/'. $id_dept .'/data_part.csv'); ?>" ;			  
	  
	   $.ajax({
		url:checkfile ,
		type:'post',
		error: function()
		{
			 $('#hexcel').hide();
			 $('#hdel').hide();
		},
		success: function()
		{
			 $('#hexcel').show();
			 $('#hdel').show();
		}
	});	   					
 });
</script>

<script>
//delete excel.....file

$(function(){
    $('a.deleteexcel').click(function(){
      $.ajax({
       url:"<?php echo base_url('pr_form/c_create_pr/clear_excel'); ?>",      
       method:'POST',
       success:function(data){       
           alert("Excel Removed");    
		   location.reload();		
		   
       }
      });
    });
});
</script>


<script>
$(document).ready(function(){ //fungsi check file excel jika exist.
 $('#load_data').click(function(){     	
	var n = $('.detail tr').length ;	// banyak nya detail untuk dihapus seblm upload.					
		$.ajax({
		   url:"<?php 
				    $company_cut =substr($this->session->userdata('short'),0,-1);
				    $id_dept =$this->session->userdata('id_dept');
				    echo base_url('asset/uploads/'.$company_cut."/".$id_dept.'/data_part.csv'); 
				?>",
			type:'HEAD',
			error: function()
			{
				alert('Nothing csv File');
			},
			success: function()
			{
				for(var row = 0; row < n   ; row++)
				{				  				   
				   var tr =  $(this).parent().parent();  		          
				   var get_iddet =$('.txtid').eq(row).val();						
		           var buff_iddet =$('.buffarray_iddet').val()					  				   				   				  
				   if (buff_iddet == ""  ) { 	
					$('.buffarray_iddet').val(get_iddet);
				   }else{							       
			        $('.buffarray_iddet').val($('.buffarray_iddet').val() + "," + get_iddet );								
				   } 				   				   
				}
				
				for(var row = 0; row < n +1  ; row++)
				{					
				   $('.detail tr').remove(row); //remove lengt of tr detail
				}
							
				load_fromexcel_data_part(); // function uploa dexcel 											
			}
		});
 	 });	
});
</script>

<script>
function load_fromexcel_data_part() //fungsi load
{   
  $.ajax({
   url:"<?php 
          //$company_cut =substr($this->session->userdata('short'),0,-1);
		  $company_cut =substr($this->session->userdata('short'),0,-1);
		  $id_dept =$this->session->userdata('id_dept');
          echo base_url('asset/uploads/'.$company_cut."/".$id_dept.'/data_part.csv'); 
		?>",
   dataType:"text",
   success:function(data)
   {		
	//---------------------------------------------------     
    var part_data = data.split(/\r?\n|\r/);	  
	//---------------------------------------------------
	var strnopp =$('#txtnopp').val()
    for(var count = 0;count< part_data.length - 1; count++)
    {
	     	 
     var cell_data = part_data[count].split(",");	 	
		 
	 var tr = '<tr id="rowdetail">'+
'<td class="no" align="center">'+ cell_data[0] +'</td>'+'<td><input type="text" id="txtidmasdetid" name="txtidmasdet[]"  value="'+strnopp+'" hidden="hide" /><input type="text" name="txtiddet[]" class="txtid" value="" hidden="hide" /> <input type="text" name="flagstat[]"  value="1" hidden="hide" /><input type="text" maxlength="70"  name="txtdesc[]" value='+ cell_data[1] +' placeholder="Item Type" size="24" class="form-control txtdesc" /></td>'+'<td><input type="text" maxlength="70"  name="txtspecs[]" value='+ cell_data[2] +'  placeholder="Description" size="30" class="form-control txtspecs" /></td>'+'<td><input maxlength="6" id="txtqty" style="text-align: center;" name="txtqty[]" value='+ cell_data[6] +' type="text" placeholder="0" size="7" onkeypress="return isNumberKey(event)" class="form-control quantity qty_validation"  /></td>'+                                                		                                  
'<td><input maxlength="14" style="text-align: right;" name="txtprice[]" value='+ numeral(cell_data[7]).format('0,0.00')  +' type="text" placeholder="0.00" size="30" onkeypress="return isNumberKey(event)" class="form-control price price_validation"/> </td>'+	
'<td><input  maxlength="70" style="text-align: right;"  name="txtamount[]" value='+ numeral(cell_data[8]).format('0,0.00') +' placeholder="0.00"  size="35"  class="form-control amount" readonly="readonly"/></td>'+'<td>  <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button></td>'+
'</tr>';    
	$('.detail').append(tr);   	 	
	
	//------call total detail....	
	       var tr =  $(this).parent().parent();	
		  // numeral().unformat($('.taxcharge').eq(i).val())	  
		   var amt =  numeral().unformat(cell_data[8]);		   
		   var stattaxppn =  tr.find(".txtstat").val();
		   var stattaxpph =  tr.find(".txtstatpph").val();
				  
		   if ( cell_data[9] == "PPN") { //ppn 10%			   		       		   	
		       var ttlppn= amt * (10/100)	;		  			  
			   $('.taxcharge').eq(count).val(numeral(ttlppn).format('0,0.00'))	;							
		   }		   		  		  
		   
		    if ( cell_data[9] == "NONE") { //none			   				
				$('.taxcharge').eq(count).val('0,0.00')	;
		    }		
		   
		  
					 
		   if (cell_data[10] =="PPH2%") { //pph 2%			   
		       var ttlpph = amt * (2/100);	   
			   $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));								
		   }else{
			    if (cell_data[10] =="PPH4%") { //pph 2%			   
		      	    var ttlpph = amt * (4/100);
					 $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));
				}else{	
					if (cell_data[10] =="PPH15%") { //pph 2%			   
		      	    	var ttlpph = amt * (15/100);
						 $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));
					}else{	
			   			 $('.taxchargepph').eq(count).val('0,0.00');
						 $('.taxchargepph').eq(count).val(numeral(ttlpph).format('0,0.00'));
					}
				}
		   }	 			
	      
		   total(); //call total
	  //--------------------------	
	
    }//endfor	

	readOnlyPOReffForLoadExcel()

   }   
  });
};

</script>

<script>
$(document).ready(function(){ //fungsi check file excel jika exist.
 $('#btnsavebtn').click(function(){
	  var url = "<?php echo site_url('pr_form/c_create_pr/do_edit_pp'); ?>";
	  var formData = new FormData(document.getElementById('form-upload'));
	  var m =  $('#ViewloadinglModal');

	  var isPriceValidation = 1;
		var isQtyValidation = 1;

	  $('.price_validation').each(function () {

	  	var val = $(this).val();

	  	//var desc = 
	  	if (val == 0) {
	  		isPriceValidation = 1;
	  		$(this).css({
	  			'border': '1px solid red'
	  		});
	  		$(this).val('');
	  		$(this).focus();

	  		$('#error_msg').html('<div class="alert alert-danger">Please fill Price above 0 !</div>')
	  		return false;
	  	} else {
	  		$(this).css({
	  			'border': '1px solid #d5d5d5'
	  		});
	  		$('#error_msg').html('')
	  		isPriceValidation = 0;


	  	}

	  })

	  $('.qty_validation').each(function () {

	  	var val = $(this).val();

	  	//var desc = 
	  	if (val == 0) {
			isQtyValidation = 1;
	  		$(this).css({
	  			'border': '1px solid red'
	  		});
	  		$(this).val('');
	  		$(this).focus();

	  		$('#error_msg').html('<div class="alert alert-danger">Please fill Qty above 0 !</div>')
	  		return false;
	  	} else {
	  		$(this).css({
	  			'border': '1px solid #d5d5d5'
	  		});
	  		$('#error_msg').html('')
	  		isQtyValidation = 0;


	  	}

	  })
	  	
	  
	  if(isPriceValidation == 0 && isQtyValidation == 0) {
	 		  
		if ($('#cbotype_pur').val()=="" || $('#cbotype_pur').val()=="-"){
				  alert("Purchase Type  Must  be Required");
				  $('#cbotype_pur').focus();
				  return false;
			  }else{
					 if ($('#cbovendor').val()==""){
						alert("Vendor Must be Required");
						$('#cbovendor').focus();
						return false;
				   }else{
					   	 if ($('#txtterm').val()==""){
							   alert("Term Of Payment Must be Required");
							   $('#txtterm').focus();
							   return false;
					   	}else{
							if ($('#cbocur').val()==""){
								alert("curency Must be Required");
								$('#cbocur').focus();
								return false;
					  		 }else{
								 if ($('#txtremarks').val()==""){
									alert("Remark Must be Required");
									$('#txtremarks').focus();
									return false;
								}else{
									 if ($('#txtupload').val()==""){
										alert("PDF File Upload Must be Required");
										$('#txtupload').focus();
										return false;
									 }else{
										if ($('#po_ref_pr').val()==""){
										alert("Quo Ref / PO Ref Must be Required");
										$('#po_ref_pr').val('-');
										$('#po_ref_pr').focus();
										return false;
									 } else {
									  //cek validation detail....
										var jmlbaris=parseInt($('.remove').length);
										for(var i=0;i<jmlbaris;++i){
										   var txtdesc =$('.txtdesc').eq(i).val();
										   var txtspecs =$('.txtspecs').eq(i).val();
										 //  var txtporeff =$('.txtporeff').eq(i).val();
										 //  var cbocoa =$('.cbocoa_search_data').eq(i).val();
										 //  var cboact =$('.cboact_search_data').eq(i).val();
										   var qty =$('.quantity').eq(i).val();
										   var price =$('.price').eq(i).val();

										   if(txtdesc == ""){
											  $('.txtdesc').eq(i).focus();
											  alert("Desc Data Must be  Required");
											  return false;
										   }else{
										
											   if(qty == ""){
												 $('.quantity').eq(i).focus();
												 alert("Qty Data Must be  Required");
												 return false;
											   }else{
												  if(price == ""){
													 $('.price').eq(i).focus();
													 alert("Price Data Must  be Required");
													 return false;														 														  												   }
												}
												
											}
										}//enfor

										
									//after validation--------------------------------------
											
											   $.ajax({
												
											       url:url,
												   method:"POST",
												   enctype: "multipart/form-data",
												   data: formData,
												   async: false,
												   processData: false,
												   contentType: false,
												   cache : false,
												   beforeSend: function (){
													   $('#btnsavebtn').prop('disabled', true);
														m.modal('show')
													},
												   success:function(data){   
													   $('#btnsavebtn').prop('disabled', false);
													   m.modal('hide');
											       },  

												   complete:function(data) {
													  // setTimeout(btnsavebtn, 300000);
													   alert("Insert PP & Upload Quotation  Successfull!!");
													   //location.reload();
													   window.location.replace("<?=base_url('pr_form/c_create_pr');?>");
												   }
												});
									 }
								 }
							  }
							}
						}
				     }
			    } //endif last

	  }
 	 });
});
</script>

<script>

$(document).ready(function () {
	$(function () {
		$("#cbovendor_search").autocomplete({
			minLength:0,
			delay:0,
			source: function(request, response) {	
			var str_url = '<?php echo site_url('pr_form/c_create_pr/suggest_vendor'); ?>';					
			var str_vendor =$("#cbovendor_search").val();																																		
				$.ajax({ 
					url: str_url ,												
					data: {vendor:str_vendor},																																		
					dataType: "json",
					type: "POST",										  					
					success: function(data){						
						response(data);	
						$(".ui-autocomplete").css("z-index", "2147483647");																																																						
					}						
				});
			},	
				select:function(event, ui){						   		  
				   $('#cbovendor').val(ui.item.idvendor);					  
				}							                  				                     
		});		
			
	});

});	  



 </script> 
 
 
 <script>

/*$('body').delegate('.cbocoa_search_data','focusin',function() {	   
	        var tr =  $(this).parent().parent();	
		    tr.find(".cbocoa_search_data").autocomplete({			 
			
			
			minLength:0,
			delay:0,			
			source: function(request, response) {				
			var str_url = '<?php //echo site_url('pr_form/c_create_pr/suggest_coa'); ?>';					
			var str_coa = tr.find('.cbocoa_search_data').val()					
				$.ajax({ 
					url: str_url ,												
					data: {coa:str_coa},																																		
					dataType: "json",
					type: "POST",										  					
					success: function(data){						
						response(data);	
						//alert(str_coa);
						$(".ui-autocomplete").css("z-index", "2147483647");																																																						
					}						
				});
			},	
				select:function(event, ui){					   				  				 
				   tr.find('.cbocoa').val(ui.item.idcoa);					  
				}						                  				                     
		});		
			
}); */


$('body').delegate('.cbocoa_search','blur',function(){  	
  var coa = $('#cbocoa').val() ;
  var coasearch = $('#cbocoa_search').val() ;
  
	if(coa == "" ) {
	  $('#cbocoa_search').val("") ;
	  $('#cbocoa_search').focus();
	}
	
	if (coasearch==""){
		$('#cbocoa').val("");
		$('#cbocoa_search').focus();
	}
	  
		   		
}); 	

 </script> 
 
 <script>
  $('body').delegate('.cboact_search_data','focusin','change',function() { //using focusin
	        var tr =  $(this).parent().parent();
			var str_coa = tr.find('.cbocoa_search_data').val();
			
		    tr.find(".cboact_search_data").autocomplete({
			
			minLength:0,
			delay:0,
			source: function(request, response) {
			var str_url = '<?php echo site_url('pr_form/c_create_pr/suggest_act'); ?>';
			var str_act = tr.find('.cboact_search_data').val();
			//var totalrow=parseInt($('.remove').length);
		        /* for(var i=0;i<totalrow;++i){
		            var str_coa =$('.cbocoa_search_data').eq(i).val();
						if (str_coa=="") {
							$('.cboact_search_data').eq(i).val("")
							alert("coa must be required");
							$('.cbocoa_search_data').eq(i).focus()
							return;	
						}
				   }*/	
			
				$.ajax({
					url: str_url ,
					data: {act:str_act},
					dataType: "json",
					type: "POST",
					success: function(data){
						response(data);
						$(".ui-autocomplete").css("z-index", "2147483647");
						
					}
				});
			},
				select:function(event, ui){
					var thisRow = $(this).parents("tr");
					thisRow.find('.cbocoa_search_data').val(ui.item.nomorcoa);
				}
		});

    }); //bodydelagite

	// Cek company ESU dan direct unit atau direct part, added by Bernand
	function readOnlyPOReff(e) {
	var value = e.options[e.selectedIndex].text;
	var short_company = "<?=$this->session->userdata('short');?>";

	if(short_company == 'ESU' && value == 'Direct Units' || value == 'Direct Parts') {
		$('#po_ref_pr').val('-').prop('readonly', true);

		$('#add').removeClass('btn-info').addClass('btn-disabled');
		$('#add').prop('disabled', true);

		$('.remove').removeClass('btn-danger').addClass('btn-disabled');
		$('.remove').prop('disabled', true);

		$('.txtdesc').prop('readonly', true);
		$('.txtspecs').prop('readonly', true);
		$('.quantity').prop('readonly', true);
		$('.price').prop('readonly', true);
	} else {
		$('#po_ref_pr').val('').prop('readonly', false);

		$('#add').removeClass('btn-disabled').addClass('btn-info');
		$('#add').prop('disabled', false);

		$('.remove').removeClass('btn-disabled').addClass('btn-danger');
		$('.remove').prop('disabled', false);

		$('.txtdesc').prop('readonly', false);
		$('.txtspecs').prop('readonly', false);
		$('.quantity').prop('readonly', false);
		$('.price').prop('readonly', false);
	}
  }

  function readOnlyPOReffForLoadExcel() {
	var e = document.getElementById("cbotype_pur");
	var value = e.options[e.selectedIndex].text;
	var short_company = "<?=$this->session->userdata('short');?>";

	if(short_company == 'ESU' && value == 'Direct Units' || value == 'Direct Parts') {
		$('#po_ref_pr').val('').prop('readonly', true);

		$('#add').removeClass('btn-info').addClass('btn-disabled');
		$('#add').prop('disabled', true);

		$('.remove').removeClass('btn-danger').addClass('btn-disabled');
		$('.remove').prop('disabled', true);

		$('.txtdesc').prop('readonly', true);
		$('.txtspecs').prop('readonly', true);
		$('.quantity').prop('readonly', true);
		$('.price').prop('readonly', true);
	} else {
		$('#po_ref_pr').prop('readonly', false);

		$('#add').removeClass('btn-disabled').addClass('btn-info');
		$('#add').prop('disabled', false);

		$('.remove').removeClass('btn-disabled').addClass('btn-danger');
		$('.remove').prop('disabled', false);

		$('.txtdesc').prop('readonly', false);
		$('.txtspecs').prop('readonly', false);
		$('.quantity').prop('readonly', false);
		$('.price').prop('readonly', false);
	}
  }

/*$('body').delegate('.cbocoa_search_data','blur',function(){
   var tr =  $(this).parent().parent();
   var coa = tr.find('.cbocoa').val() ;
   var coasearch = tr.find('cbocoa_search_data').val() ;

	if(coa=="") {
	  tr.find('.cbocoa_search_data').val("") ;
	  tr.find('.cbocoa_search_data').focus();
	}

	if (coasearch==""){
		tr.find('.cbocoa').val("");
		tr.find('.cbocoa_search_data').focus();
	}
}); */
 /*$(function() {
    $(".chosenselect").chosen();
  }); */
 </script>

<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_fail') ;?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_insert_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_insert_fail');?>  
        </div>	
<?php } ?>

	<?php 
		$flag_inv_receipt = $this->session->userdata('flag_inv_receipt');
	?>

  <?php  echo form_open_multipart('pr_form/c_create_pr/multiple_submit',array('class'=>'form-multi','id' => 'form-upload',));  ?> 
  
    <div class="table-header btn-info">
                <?php echo " ".$header ;?> 
</div>
          

<!-- Responsive Form for Mobile and Web Start -->
<div class="container">
	
	<?php foreach($tampil_pp as $row) {	 ?>  
	<!-- Row Start -->
	<div class="row">
		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label">PR Number</label>

				<input type="text" id="txtnopp" name="txtnopp" class="form-control" readonly="readonly"
					value="<?php echo $row->id_master ?>" on />
			</div>
		</div>

		<div class="col-sm-1">


		</div>

		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label" style="color:#900">Requester *</label>

				<input type="text" id="txtsub" name="txtsub" class="form-control"
					value="<?php echo $this->session->userdata('name'); ?>" readonly="readonly" />
			</div>
		</div>

	</div>
	<!-- Row End -->

	<!-- Row Start -->
	<div class="row">
		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label">Date Submission</label>

				<input type="text" id="txtdate" name="txtdate" class="form-control" readonly value="<?php echo date("d-m-Y") ?>" />
			</div>
		</div>

		<div class="col-sm-1">


		</div>

		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label" style="color:#900">Upload Quotation (PDF, Max 2 MB)</label>

				<input type="file" id="txtupload" onChange="checkSize(this)" name="userfile" class="btn btn-success" 
				value="<?php echo $row->attach_quo ?>" accept="application/pdf" />  

				<br>
					<font class="text-danger" style="font-size:15px" size="15px" id="alert-msg"></font>
			</div>
		</div>

	</div>
	<!-- Row End -->

	<!-- Row Start -->
	<div class="row">
		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label">Company</label>

				<input type="text" id="txtcompany" name="txtcompany" class="form-control" value="<?php echo $this->session->userdata('company');  ?>"  readonly />
			</div>
		</div>

		<div class="col-sm-1">


		</div>

		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label" style="color:#900">Attached Quotation</label>

				<?php      				
						  If ($row->attach_quo != "")
						  {  						 						  
					    ?>	  		                       
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						 <a onClick="PDFPopup(this)" req_id="<?php echo site_url($row->attach_quo); ?>" >
                         <i id="imgexcel" class="fa fa-file-pdf-o" style="font-size:30px">
                           <label>Quotation</label>
                         </i>                           
                 		 </a>                  			       						 
                        <?php    				
						  }else{
							echo'No Attachment' ;	  
						  }
						?>     
			</div>
		</div>

	</div>
	<!-- Row End -->

	<!-- Row Start -->
	<div class="row">
		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label">Branch</label>

				<input type="text" id="txtbranch" name="txtbranch" class="form-control" value="<?php echo $this->session->userdata('name_branch'); ?>" readonly />
			</div>
		</div>

		<div class="col-sm-1">


		</div>

		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label" style="color:#900">Vendor Name *</label>

				<select data-placeholder="Dealer Code | Vendor Name | ID" id="cbovendor" name="cbovendor" class="myselect" tabindex="2" style="width: 100%;">
		         <option value="<?php echo $row->id_vendor ;?>"><?php echo $row->vendor ;?></option>  
	             <?php foreach($tampil_vendor as $row11){ ?>
               		 <option value="<?php echo $row11->id_vendor ;?>"><?php echo $row11->vendor. '|'. $row11->contact_person ;?></option>
                 <?php } ?>
                     </select>    
			</div>
		</div>

	</div>
	<!-- Row End -->

	<!-- Row Start -->
	<div class="row">
		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label">Department</label>

				<input type="text" id="txtdept" name="txtdept" class="form-control" 
				value="<?php echo $this->session->userdata('dept'); ?>" readonly />
			</div>
		</div>

		<div class="col-sm-1">


		</div>

		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label" style="color:#900">Term Of Payment *</label>

				<input name="txtterm" type="text" class="form-control" id="txtterm" placeholder="T.O.P" onkeypress="return isNumberKey(event)" 
				 maxlength="3"  value="<?php echo $row->term_top ?>" />    
			</div>
		</div>

	</div>
	<!-- Row End -->

	<!-- Row Start -->
	<div class="row">
		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label">E-mail</label>

				<input type="text" id="txtmail" name="txtmail" class="form-control" 
				readonly="readonly" value="<?php echo $this->session->userdata('email'); ?>">
			</div>
		</div>

		<div class="col-sm-1">


		</div>

		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label" style="color:#900">Currency *</label>

				<select id="cbocur" name="cbocur"  class="myselect" style="width: 100%;">
                     <option value="<?php echo $row->id_curr ;?>"><?php echo $row->currency ;?></option>
	             <?php foreach($tampil_curr as $row_curr){ ?>               
               		 <option value="<?php echo $row_curr->id_curr; ?>"><?php echo $row_curr->currency ;?></option>
                 <?php } ?>  
              </select>    
			</div>
		</div>

	</div>
	<!-- Row End -->

	<!-- Row Start -->
	<div class="row">
		<div class="col-sm-5">
			<div class="form-group">
				<label class="control-label" style="color:#900">Purchase Type</label>

				<select id="cbotype_pur" name="cbotype_pur" onChange="readOnlyPOReff(this)" class="select2" style="width: 100%;"
>
				 <option value="<?php echo $row->flag_purchase ;?>"><?php echo $row->type_purchase ;?></option>
	             <?php foreach($tampil_type_purchase as $row_pur){ ?>             
                     <?php if ( $row_pur->flag=="1") { ?>
   		       		      <option value="<?php echo $row_pur->flag ;?>" class="btn-info"><?php echo $row_pur->type_purchase ;?></option>
               		 <?php }else{ ?>
                    	   <?php if ( $row_pur->flag=="2") { ?>
                     	 	      <option value="<?php echo $row_pur->flag ;?>" class="btn-success"><?php echo $row_pur->type_purchase ;?></option>
                            <?php }else{ ?>
                  			       <?php if ( $row_pur->flag=="3") { ?>
                     					  <option value="<?php echo $row_pur->flag ;?>" class="btn-yellow"><?php echo $row_pur->type_purchase ;?></option>
                         	       <?php }else{ ?>
                     						<?php if ( $row_pur->flag=="4") { ?>
                     							 <option value="<?php echo $row_pur->flag ;?>" class="btn-purple"><?php echo $row_pur->type_purchase ;?></option>
                                             <?php }else{ ?>  
                                             	 <option value="<?php echo $row_pur->flag ;?>" class="btn-danger"><?php echo $row_pur->type_purchase ;?></option>
                                              <?php } ?>   
								     <?php } ?>   
   		       		   	     <?php } ?>
                       <?php } ?>   
                 <?php } ?>  
              </select>
			</div>
		</div>

		<div class="col-sm-1">


		</div>

		<div class="col-sm-5">
			<div class="form-group">
			<?php if($row->reason_reject != "") { ?>
					<label class="control-label" style="color:#900">Reject Reason</label>
					<textarea name="txtreject" class="form-control" id="txtreject" readonly="readonly"><?php echo $row->reason_reject ?>
					</textarea>
				<?php }else{ ?>
					<label class="control-label" style="color:#900">Remarks *</label>
					<textarea name="txtremarks" class="form-control" id="txtremarks"><?php echo $row->remarks ?>
					</textarea>
				<?php } ?>
			</div>
		</div>

	</div>
	<!-- Row End -->

	<?php if($flag_inv_receipt == "1") { ?>
	<!-- Row Start -->
	<div class="row">
			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label">Invoice Received ?</label>

					<label>
						<input name="inv_receipt" id="inv_receipt" type="checkbox" 
						value="1" class="ace form-control" 
						
						<?php if($row->flag_status_inv_receipt == 1) {echo 'checked' ;} ?>
						
						/>
						<span class="lbl"> Yes</span>
					</label>
				</div>
			</div>

			<div class="col-sm-1">


			</div>

			<div class="col-sm-5">
				<div class="form-group">
				</div>
			</div>

		</div>
		<!-- Row End -->


		<?php } ?>

	<?php } ?>

</div>
<!-- Responsive Form for Mobile and Web Start -->

<!-- Content Popup -->
<div id="dialog" style="display: none;">
    <div>
        <iframe id="frame" width="750px" height="550px"></iframe>
    </div>
</div>
<!-- Content Popup -->
   
<div class="table-header btn-info">
       Purchase detail
</div>
 	  <!--<br />
  
	 <table class="">
       	   <tr>            
            	<td>
                 <input id="fileSelect" name="fileSelect" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" class="form-control btn btn-info" />
                </td>
                <td>&nbsp;</td>
              <td hidden id="hexcel">
              <div style="margin-left:5px; border:thin; border-color: #666;" >
                                                                 
                  <a href="<?php    
				    /*$company_cut =substr($this->session->userdata('short'),0,-1);
				    $id_dept =$this->session->userdata('id_dept');
				    echo site_url('asset/uploads/'.$company_cut."/".$id_dept.'/data_part.csv'); */                                  
                    ?>"  class="btn btn-success" >
                      <i id="imgexcel" class="fa fa-file-excel-o" style="font-size:20px" ></i>
                  </a>
                </div>
             </td> 
                 <td hidden id="hdel">                   
                  <div style="margin-left:5px; border:thin; border-color: #666;" >
                   <a href="#" class="deleteexcel btn btn-success"  >
                	  <i id="imgdel" class="fa fa-trash-o" style="font-size:20px" ></i>
                   </a>                   
               </div>     
             </td>
             <td>&nbsp;</td>
                <td>
                	   <div align="center">
                                                  <button type="button" name="load_data" id="load_data" class="btn btn-app btn-info btn-sm radius-6 "> <i class="fa fa-pencil-square-o bigger-160"></i>Load</button>

                       </div>
                </td>
           </tr>
            <tr>           	             
            <div id="prog" class="progress" style="display:none;">
                          <div class="progress-bar progress-bar-striped active" role="progressbar"aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:100%"  >
                100% upload Successfully
                         </div>
            </div>            
           </tr>                                   
         </table>
      -->   
           
       <input  type="text" name="txtiddet_buff"  readonly="readonly" class ="buffarray_iddet" width="70"  hidden="hide"  />
       
        <br />
		<div id="error_msg"></div>
<div class="table-responsive">
	<table class="table">
		<thead>
			<tr>
				<th>No</th>
				<th>Item Type</th>
				<th>Description</th>
				<th>Qty</th>
				<th>Estimation Price</th>
				<th>Total Estimation Price</th>
				<!--<th>Tax PPN</th>
				<th>Tax PPH</th>
				<th>Charge PPN</th>
				<th>Charge PPH</th> -->

				<th align="center"><strong>
						<input type="button" id="add" Class="btn-sm radius-6 btn-info" value="+" />
				</th>
			</tr>
		</thead>
		<tbody class="detail">
			<?php $ino = 1; foreach($tampil_det_pp as $row2  ){  // buat  perulangan tampilan detail ?> 
				<tr>
                        <td class="no" align="center"><?php echo $ino++ ;?></td>
                        <td>        
                         <input type="text" id="txtidmasdetid" name="txtidmasdet[]" maxlength="25"  value="<?php echo $ppnumber  ?>"  hidden="hide"    />                                     
                        <input type="text" name="txtiddet[]" value="<?php echo $row2->id_detail ?>" class="txtid" hidden="hide"     />       
                        <input type="text" name="flagstat[]"  value="1" hidden="hide" />                 
                        <input type="text" maxlength="70" id="txtdesc"  name="txtdesc[]"  placeholder="Item Type" size="24" class="form-control txtdesc"  value="<?php echo $row2->desc ?>"/>
                        </td>
                        
                        <td>
                          <input type="text" maxlength="70"  name="txtspecs[]"  placeholder="Description" size="30" class="form-control txtspecs" value="<?php echo $row2->spec ?>" />
                        </td>                     
                        <td>
                        <input maxlength="6" style="text-align: center;" name="txtqty[]" type="text" placeholder="0" size="12" onkeypress="return isNumberKey(event)" class="form-control quantity qty_validation" id="txtqty" value="<?php echo number_format($row2->qty) ?>" />                          
                        </td>                                                		                                  
                        <td>
                           <input maxlength="14" style="text-align: right;" name="txtprice[]" type="text" placeholder="0.00" size="20" onkeypress="return isNumberKey(event)" class="form-control price price_validation" id="txtprice" value="<?php echo number_format($row2->harga,2) ?>" /> 
                        </td>
                        
                       <td>
                  			<input  maxlength="70" style="text-align: right;" name="txtamount[]"  placeholder="0.00"  size="30"  class="form-control amount" readonly="readonly" value="<?php echo number_format($row2->total,2) ?>" />                       </td>                       
                      <!--  <td width="7%">
                         <select  name="cbotax[]" class="form-control txtstat">
                          <option value =" <?php //echo $row2->tax_type ?>"> <?php //echo $row2->tax_type ?></option>
                            <option value="NONE">NONE</option>    
                            <option value="PPN">PPN</option>                        
                         </select>
                        </td> 
                         <td width="9%">
                         <select  name="cbotaxpph[]" class="form-control txtstatpph" >
                            <option value =" <?php //echo $row2->tax_typepph ?>"> <?php //echo $row2->tax_typepph ?></option>
                            <option value="NONE">NONE</option>                              
                            <option value="PPH2%">PPH2%</option>   
                            <option value="PPH4%">PPH4%</option>   
                            <option value="PPH15%">PPH15%</option>      
                         </select>
                        </td> 
                                                            	
                        </td>
                         <td>
                         <input  maxlength="70" name="txttaxcharge[]"  placeholder="0.00"  size="30"  class="form-control taxcharge" readonly="readonly" value="<?php //echo number_format($row2->tax_detail,2) ?>" />                                                
                        </td>                        
                        <td>
                  		<input  maxlength="70" name="txttaxchargepph[]"  placeholder="0.00"  size="30"  class="form-control taxchargepph" readonly="readonly" value="<?php// echo number_format($row2->tax_detailpph,2) ?>" />
                       </td>  -->
                        <td>
                        <button class="btn-app btn-danger btn-sm radius-6 remove"><i class="glyphicon glyphicon-remove"></i></button>
                        </td>
                       
                     </tr>
					 <?php } ?>
		</tbody>
         <tr class="btn-danger" style=" color:#FFF;" align="right">              
              <td colspan="5" >     
                   <label >  Grand Total :   </label>
               </td>
			   <td >   
                 <input  maxlength="70" style="text-align: right;" id="result" name="result" class="form-control total" readonly="readonly" style="color:#900" value="<?php echo number_format($row->gran_total,2 ) ?>" />     
               </td>
               
            </tr>
	</table>
	
</div>

           	
        <!--  </table>     
            <table class="btn-danger" style=" color:#FFF;"  >
            <tr>              
              <td colspan="0" width="25%" >     
                   <label >  Sub Total :   </label>
               </td>
			   <td width="40%">   
                 <input  maxlength="70" id="result" name="result" class="form-control total" readonly="readonly" style="color:#900" value="<?php// echo number_format($row->gran_total,2 ) ?>" />     
               </td>
               
            </tr>
             <tr>
               <td colspan="0" width="25%" >    
                    <label>  Total Tax PPN:   </label>
               </td>
               <td width="40%">   
                 <input  maxlength="70" id="txtppn" name="txtppn" class="form-control ppncalc" readonly="readonly" style="color:#900" value="<?php //echo number_format($row->ppn,2) ?>"  />     
               </td>
             
            </tr>
            
            <tr>
               <td colspan="0" width="25%" >       
                    <label>  Total Tax PPH:   </label>
               </td>
               <td width="40%">   
                 <input  maxlength="70" id="txtpph" name="txtpph" class="form-control pphcalc" readonly="readonly" style="color:#900" value="<?php //echo number_format($row->pph,2 ) ?>"  />     
               </td>
             
            </tr>
            
             <tr>
               <td colspan="0" width="25%" >       
                  <label> Grand Total + TAX  </label>
               </td>
               <td width="40%">   
                 <input  maxlength="70" id="gpppn" name="gpppn" class="form-control grandppn" readonly="readonly" style="color:#900" value="<?php// echo number_format($row->gran_totalppn,2 ) ?>" />     
               </td>
            </tr>
         </table> --> 

		 

                     <tr>
                       <td>
                           <button class="btn btn-app btn-info btn-xs radius-4 cbtnsavebtn" type="button" id="btnsavebtn" name="btnsavebtn"  value ="btnsavebtn"  >
            <i class="ace-icon fa fa-book sm-160"></i>
            Submit
		</button>    
                    </td>                 
                   
                    <td>&nbsp;  </td>
                    <td>                                      			
                     <a href="<?php echo base_url('pr_form/c_create_pr');?>" style="text-decoration:none;" class="btn btn-app btn-success btn-xs radius-4 btnback"> <i class="ace-icon fa fa-exchange bigger-160"></i>Back</a>                          </td>
                  </tr>
               </table>                                                	
  
  
<p>
  <?php form_close(); ?> 
                  
                  
   <?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>    
                  
  <!-- Modal -->
</p>
        <div class="modal fade" id="ViewloadinglModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">                                        
                    <div class="modal-body" align="center">
                            <img src="<?php echo base_url('assets/img/loading_spinner.gif') ; ?>" />
                    </div> 
                    
                </div>
            </div>
        </div>
<!-- Modal -->

<!-- combobox autocomplete -->
<script type="text/javascript">
      $(".myselect").select2();

	  function PDFPopup(e) {
		var url = $(e).attr('req_id');
		$("#dialog").dialog({
			width: 'auto',
			height: 'auto',
			resize: 'auto',
			autoResize: true
		});
		$("#frame").attr("src", url + "#toolbar=0");
        
    };

	function checkSize(e) {
		  	var size = e.files[0].size;
		  	var ext = e.files[0].type;

			if (ext != 'application/pdf') {
				$('#alert-msg').html('Your chosen file is not PDF, please choose PDF !');
				$('#btnsavebtn').prop('disabled', true);
				return false;
			} else {
				if (size > 2097152) {
					$('#alert-msg').html('File size is more than 2MB ! <br> Please change another file under 2MB');
					$('#btnsavebtn').prop('disabled', true);
					return false;
				} else {
					$('#alert-msg').html('');
					$('#btnsavebtn').prop('disabled', false);
					return true;
				}
			}			
	  }
</script>   
<!-- combobox autocomplete end --> 