<style>
   th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
</style>	

<?php
if($_SESSION['username']) {
$username = $_SESSION['username'];
$location = $_SERVER['PHP_SELF'];
$data1 = array(
'location' => $location
);
$this->db->where('username',$username);					
$this->db->update('tbl_user',$data1);
}
?>


      <div class="modal fade" id="ViewloadinglModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">                
                    <div class="modal-body-load-spiner" align="center">
                            <img src="<?php echo base_url('assets/img/loading_spinner.gif') ; ?>" />
                    </div> 
                    
                </div>
            </div>
        </div>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>
      
<?php   	
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>      
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>                        
                <?php  echo "Send Approval Successfully"	?>  
		  </div>		
<?php } ?>

<?php   	
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>                
        <div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>                        
			<?php  echo "Send Approval failed"	?>  
        </div>		      
<?php } ?>	
  
  
                
 <?php  echo form_open('pr_form/c_create_pr/multiple_submit',array('id' => 'form-tablepr'));  ?>     
 
    <div>     
    <table>
       <tr class="col-md-9">
        <td>         
        <a href="<?php echo site_url('pr_form/c_create_pr/view_form_pr'); ?>" class="btn btn-app btn-info btn-xs radius-4 caddnew"> <i class="ace-icon fa fa-book bigger-160"></i>AddNew</a>
        </td>
        <td>
        
        <button class="btn btn-app btn-success btn-xs radius-4 btn_edit" type="submit" id="btnedit" name="btnedit"  value ="btnedit"  disabled="disabled" >
            <i class="ace-icon fa fa-pencil-square-o bigger-160"></i>
            Edit
		</button> 
        <td>
         <button class="btn btn-app btn-danger btn-xs radius-4 btn_delete" type="button" id="btndel" name="btndel"  value ="btndel"  disabled="disabled" >
            <i class="ace-icon fa fa-ban bigger-160"></i>
            Cancel
		</button> 
        
       </td>
         
       <td>    
       
        <button class="btn btn-app btn-yellow btn-xs radius-4 btn_send" type="button" id="btnsendtab" name="btnsendtab"  value ="btnsendtab"  disabled="disabled" >
            <i class="ace-icon fa fa-paper-plane bigger-160"></i>
            Send P.R
		</button> 
        </td>
       </tr>
     </table>         
 </div>     
<br />
 
					
	 <div class="table-header btn-info"> <?php echo " ".$header ;?>   </div>    

	 <h3>Legend : <span class="badge badge-danger"> &nbsp;  &nbsp;  &nbsp;  &nbsp; </span> = Rejected </h3> 
       
		<table id="myTable" cellpadding="0" cellspacing="0" width="100%"  class="table table-striped table-bordered table-hover">
	      <thead align="center" class="text-warning" > 
            <tr>  
                <th style="text-align:center" class="col-name">           
                  <label class="pos-rel">
                    <input type="checkbox" id="checkAll" class="ace" />
                    <span class="lbl"></span>
                  </label>
				</th>	       		          
                <th >PR No </th>			
                <th >Vendor</th>			                	
                <th >Date Created</th>      
                <th >Type</th> 
                <th >Curr</th>                
                <th >Total Price</th>
                <!-- <th >Total + TAX</th> -->
                <th >T.O.P</th>
               <!-- <th >Submission</th>	-->			
                <th>Detail</th>		
                <!-- <th>Attach</th>	 -->
               </tr>
             </thead>    
         </table> 
    
</div>
   
  

<!-- Modal -->
        <div class="modal fade" id="ViewDetailModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content ">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Purchase</h4>
                    </div>
                    
                    <div class="modal-body">                      
                    </div> 
                    
                     <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-primary btnclose" data-dismiss="modal">Close</button>                       
                    </div>                   -->
                </div>
            </div>
        </div>
<!-- Modal -->  

<!-- Content Popup -->
<div id="dialog" style="display: none;">
    <div>
        <iframe id="frame" width="750px" height="550px"></iframe>
    </div>
</div>
<!-- Content Popup -->


<script>
 //get data table remarks to modal popup.
$(function(){
  $(document).on('click','.reject',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("pr_form/c_create_pr/get_info_reject"); ?>';
		
		$("#myModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {idmaster:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});


</script>

<script>
	
//check all--------------------------
$('#checkAll').change(function(){	
		var table = $('#myTable').DataTable();
		var cells = table.cells( ).nodes();
		$( cells ).find(':checkbox').prop('checked', $(this).is(':checked'));			
		
});  


		

//aktif edit--------------------------------------------------------  
var counterCheckededit = 0;
	$('body').on('change', 'input[type="checkbox"]', function() { 
		   this.checked ? counterCheckededit++ : counterCheckededit--;
		   if($('#checkAll').not(':checked').length){ 
		       counterCheckededit == 1 ? $('#btnedit').prop("disabled", false): $('#btnedit').prop("disabled", true);
		   }
	 });	


//end---------------------------------------------------------------


//aktif dell--------------------------------------------------------
var counterChecked = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterChecked++ : counterChecked--;
	counterChecked > 0 ? $('#btndel').prop("disabled", false): $('#btndel').prop("disabled", true);

});
//--------------------------------------------------------------------


//aktif button send email--------------------------------------------------------
var counterCheckesendeamail = 0;
$('body').on('change', 'input[type="checkbox"]', function() {
	this.checked ? counterCheckesendeamail++ : counterCheckesendeamail--;
	if($('#checkAll').not(':checked').length){ 
	   counterCheckesendeamail == 1 ? $('#btnsendtab').prop("disabled", false): $('#btnsendtab').prop("disabled", true);
	}
});
//end---------------------------------------------------------------



</script>

<script>
//get data from control to modal popup.
$(function(){
  $(document).on('click','.detail',function(e){
		var req_id = $(this).attr('req_id');  
		var url = '<?php echo site_url("pr_form/c_create_pr/get_idtrans_modal"); ?>';
		
		$("#ViewDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_pp:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		}); 			
		
   });	
});
</script>

<script>
$(function(){
    $(document).on('click','.btn_delete',function(e){
	  
	  if (confirm('Are you sure you want to cancel?'))
	  {
		  var url = "<?php echo base_url('pr_form/c_create_pr/do_delete_pp'); ?>";		 
		  var formData = new FormData(document.getElementById('form-tablepr'));	  		       
			  
		  $.ajax({
		   url:url,      
		   method:'POST',
		   //enctype: "multipart/form-data", 					
		   data: formData,										
		   processData: false,															
		   async: false,
		   processData: false,
		   contentType: false,		
		   cache : false,	
		   success:function(){ 	    
			   alert('Delete Successfully !!');   
			   location.reload();		  	               		  
		   }
		  }); 	
	
	  }
	  
	});  	 
});
</script>

<script>
$(function(){
    $(document).on('click','.btn_send',function(e){
	     var m =  $('#ViewloadinglModal');	
		 var url = "<?php echo base_url('pr_form/c_create_pr/send_flag_aproval'); ?>";		 
		 var formData = new FormData(document.getElementById('form-tablepr'));	

		 var conf = confirm("Are you sure Send this PR?");  		       
		
		if(conf==true) {
			function callBeforeAjax(){
			 m.modal('show');
	     }																																											      
		  
		  $.ajax({
			   beforeSend: callBeforeAjax,	
			   url:url,      
			   method:'POST',				
			   data: formData,										
			   processData: false,															
			   async: false,
			   processData: false,
			   contentType: false,		
			   cache : false,	
			   success:function(data){
					   m.modal('hide');
			   }, 	    
		   
			   complete:function(data) {  
				   alert('Send approval Successfully !!');   
				   //alert(data);
				   location.reload() ;		  	               		  
			   }
			 
		  });
		}
	  
	});  	 
});
</script>



<script type="text/javascript">

function PDFPopup(e) {
		var url = $(e).attr('req_id');
		$("#dialog").dialog({
			width: 'auto',
			height: 'auto',
			resize: 'auto',
			autoResize: true
		});
		$("#frame").attr("src", url + "#toolbar=0");
        
    };


$(document).ready(function(){ 
	$("#myTable").dataTable({
		"lengthMenu": [[10, 25, 50, 100],[10, 25, 50, 100]],
		autoWidth : true,
		processing: true,
		serverSide: true,
		"scrollY": 250,
		"scrollX": true,
		
		
		ajax: {
		  url: "<?php echo base_url('pr_form/c_create_pr/cath_data_fordatatables') ?>",
		  type:'POST',
		},
		
		"columnDefs": [ {
			 "targets": [0,7,8], /* column index */
             "orderable": false, /* true or false */
			 // "targets": [1,2], /* column index */
		} ],
		
        
		//----------------------------------end
		
		//incrase Width Column ----------------------------------
		/* "columnDefs": [{
						 "width": "20%", "targets": 1,
						 "width": "20%", "targets": 2,
						 "width": "15%", "targets": 3
					  }],
		  */					
		//----------------------------------end
		
	});
});	

</script>




<script type="text/javascript">
/*$(function(){
    $(document).on('click','.btnclose',function(e){
	 location.reload();	
	});
}) ; */	
</script>