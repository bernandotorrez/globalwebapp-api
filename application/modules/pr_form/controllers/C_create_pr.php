<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


//panggil modul MY_Controller pada folder core untuk autentikasi form  berdasarkan login

class C_create_pr extends MY_Controller
{
  public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	    $this->load->model('M_create_pr','',TRUE);
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');
		//$this->load->database();
		$this->load->model('login/M_login','',TRUE);
		$time = time();
		$sesiuser =  $this->M_create_pr->get_value('id',$this->session->userdata('id'),'tbl_user');
		$expiresession = $sesiuser->waktu;
		$session_update = 7200;
		if (($expiresession+$session_update) < $time){
		$this->disconnect();
		}
	}

  public function index()
  {
		$this->show_table(); //manggil fungsi show_table
  }

  public function show_table()
  {
	  //$tampil_table_pp = $this->M_create_pr->tampil_add_pp()['data'];
	  //$total_rows = $this->M_create_pr->tampil_add_pp()['length'];

	//   if ($tampil_table_pp)
	// 	{
	// 	$dept  = $this->session->userdata('dept') ;
	// 	$branch  = $this->session->userdata('name_branch') ;
	// 	$company = $this->session->userdata('company') ;
	// 	$data['header'] ="P.R"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
	// 	$data['pp_view'] = $tampil_table_pp;
	// 	$data['ceck_row'] = $total_rows;
	// 	$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.
	// 	$data['show_view'] = 'pr_form/V_table_pr';
	// 	$this->load->view('dashboard/Template',$data);
	//   }else{
	//     $dept  = $this->session->userdata('dept') ;
	// 	$branch  = $this->session->userdata('name_branch') ;
	// 	$company = $this->session->userdata('company') ;
	// 	$data['header'] ="P.R"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
	//     $data['ceck_row'] = $total_rows;
	// 	$data['pp_view'] = $tampil_table_pp;
	// 	$data['pesan'] = 'Data PP table is empty';
	// 	$data['show_view'] = 'pr_form/V_table_pr';
	// 	$this->load->view('dashboard/Template',$data);
	//   }

	$dept  = $this->session->userdata('dept') ;
	$branch  = $this->session->userdata('name_branch') ;
	$company = $this->session->userdata('company') ;
	$data['header'] ="P.R"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
	//$data['ceck_row'] = $total_rows;
	//$data['pp_view'] = $tampil_table_pp;
	//$data['pesan'] = 'Data PP table is empty';
	$data['show_view'] = 'pr_form/V_table_pr';
	$this->load->view('dashboard/Template',$data);
  }

public function get_info_reject() {
	     $id_master = $this->input->post('idmaster',TRUE); //
	     //$query = $this->db->query("select reason_reject from tbl_master_pp where id_master ='".$id_master."'");

		 $parameter = array('id_master' => $id_master);
		$query = http_request_get(API_URL.'/PRForm/getPP', $id_master);
		$query = json_decode($parameter, true);

		 if($query['length'] > 0){
			 foreach($query['data'] as $row);
			 {
			   echo $row->reason_reject ;
			 }
		 }
    }


	 public function view_form_pr()
	{

	  $dept  = $this->session->userdata('dept') ;
	  $branch  = $this->session->userdata('name_branch') ;
	  $company = $this->session->userdata('company') ;

	  $data['header'] ="P.R"." | "."Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
	  $data['tampil_pp']=$this->M_create_pr->get_edit_pp();
      $data['tampil_det_pp']= $this->M_create_pr->get_edit_detail_pp();
      $data['tampil_vendor']=$this->M_create_pr->get_vendor();
	  $data['tampil_curr']=$this->M_create_pr->get_currency();
	 // $data['tampil_coa']=$this->M_create_pr->get_coa();
	  $data['tampil_type_purchase']=$this->M_create_pr->get_purchase_type();
	  $data['ponumber']=$this->generatePRNumber();
	  $data['show_view']='pr_form/V_form_addnew_pr';
	  $this->load->view('dashboard/Template',$data);
	}

	public function do_upload()
	{

		$str_com =$this->session->userdata("short");
		$str_dept=$this->session->userdata("id_dept");
		$str_rep_masterid = preg_replace('~[\\\\/:*?"<>|.]~','', $this->session->userdata("ses_noppnew") );
		$str_nopp  = $str_rep_masterid.'.pdf';  // generate file name dari no pp di tambah ".pdf"
		$str_path  = './asset/uploads/'.substr($str_com,0,2)."/".$str_dept;  // generate path upload
		$str_fullpath =	$str_path.'/'.$str_nopp;

		$config['upload_path'] = $str_path;
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '120000kb';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$config['overwrite'] = TRUE;
		$config['file_name']= $str_nopp; //file name nya

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('pesan_fail',$str_nopp.implode(" ",$error));
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$data_insert = array('attach_quo'=>$str_fullpath, 'id_master' => $this->session->userdata("ses_noppnew"));
			//$this->db->where('id_master',$this->session->userdata("ses_noppnew"));
			//$this->db->update('tbl_master_pp',$data_insert);
			// API updateMasterPP
			$query = http_request_put(API_URL.'/PRForm/updateMasterPP', $data_insert);
			$query = json_decode($query, true);
			$this->session->set_flashdata('pesan_upload_succes',"1");

			
		}


	}


	public function do_upload_multi()
	{

		$number_of_files =sizeof($_FILES['userfile']['tmp_name']);

		$file =$_FILES['userfile'];

		$str_com =$this->session->userdata("short");
		$str_rep_masterid = preg_replace('~[\\\\/:*?"<>|.]~','', $this->session->userdata("ses_noppnew") );
		$str_nopp  = $str_rep_masterid.'.pdf';  // generate file name dari no pp di tambah ".pdf"
		$str_path  = './asset/uploads/'.substr($str_com,0,2);  // generate path upload
		$str_fullpath =	$str_path.'/'.$str_nopp;

		$config['upload_path'] = $str_path;
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '120000kb';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$config['overwrite'] = TRUE;

		for ($i=0; $i < $number_of_files; $i++){
		    //$config['file_name']= $str_nopp; //file name nya
			$_FILES['userfile']['name'] =$str_nopp.$i[$i];
			$_FILES['userfile']['type'] =$file['type'][$i];
			$_FILES['userfile']['tmp_name'] =$file['tmp_name'][$i];
			$_FILES['userfile']['error'] =$file['error'][$i];
			$_FILES['userfile']['size'] =$file['size'][$i];

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
		}


		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('pesan_fail',$str_nopp.implode(" ",$error));
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$data_insert = array('attach_quo'=>$str_fullpath, 'id_master' => $this->session->userdata("ses_noppnew"));
			//$this->db->where('id_master',$this->session->userdata("ses_noppnew"));
			//$this->db->update('tbl_master_pp',$data_insert);
			// API updateMasterPP
			$query = http_request_put(API_URL.'/PRForm/updateMasterPP', $data_insert);
			$query = json_decode($query, true);
			$this->session->set_flashdata('pesan_upload_succes',"1");

			
		}


	}

	public function do_upload_update()
	{
		$str_dept=$this->session->userdata("id_dept");
		$str_com =$this->session->userdata("short");
		$str_rep_masterid = preg_replace('~[\\\\/:*?"<>|.]~','', $this->session->userdata("ses_noppnew") );

		$str_nopp  = $str_rep_masterid.'.pdf'; 	 // generate file name dari no pp di tambah ".pdf"
		$str_path  = './asset/uploads/'.substr($str_com,0,2)."/".$str_dept;    // generate path upload
		$str_fullpath =	$str_path.'/'.$str_nopp;

		$config['upload_path'] = $str_path;
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '120000kb';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$config['overwrite'] = TRUE;
		$config['file_name']= $str_nopp;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('pesan',$str_nopp.implode(" ",$error));
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$data_update= array('attach_quo'=>$str_fullpath, 'id_master' => $this->session->userdata("ses_noppnew"));
			//$this->db->where('id_master',$this->session->userdata("ses_noppnew"));
			//$this->db->update('tbl_master_pp',$data_update);
			// API updateMasterPP
			$query = http_request_put(API_URL.'/PRForm/updateMasterPP', $data_insert);
			$query = json_decode($query, true);
			$this->session->set_flashdata('pesan_upload_succes',"1");
		}
	}
	
	public function do_upload_excel()
	{
		$company_cut =substr($this->session->userdata('short'),0,-1);
		$id_dept =$this->session->userdata('id_dept');
	    $str_path  = './asset/uploads/'.$company_cut."/".$id_dept;  // generate path upload
		$config['upload_path'] = $str_path;
		$config['allowed_types'] = '*';
		$config['max_size']	= '12000kb';
		$config['overwrite'] = TRUE;
		$config['file_name']= 'data_part.csv'; //file name nya

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( !$this->upload->do_upload('fileSelect'))
		{
			 $error = array('error' => $this->upload->display_errors());
		     print_r($error);
		}else{
		   	 echo 'Upload file data_part.csv successfull';
			 $data = array('upload_data' => $this->upload->data());
			//redirect('c_create_pp_direct_part/c_crud_pp_direct_part');
		}

	}

	public function clear_excel()
	{
		$company_cut =substr($this->session->userdata('short'),0,-1);
		$id_dept =$this->session->userdata('id_dept');
	    $get_file = './asset/uploads/'.$company_cut."/".$id_dept.'/data_part.csv';

		if(file_exists($get_file)) :
		   unlink($get_file);
	       echo "File Excel Remove"  ;
		   //redirect('pr_form/c_create_pp/view_form_pp');
		else:
		   echo "Nothing Excel File"  ;
		endif;
	}

	public function do_insert_pp()
	{	
		//echo json_encode($this->M_create_pr->insert_master_and_detail());

			if ($this->M_create_pr->insert_master_and_detail())

		    {
			      $this->do_upload(); //manggil fungsi Upload File
				  if ($this->session->flashdata('pesan_upload_succes') !=""){
				      $str_upload_success = "& Upload Quotation";					  							  				                  }else{
					   $str_upload_success = "";
				  }

				  //$this->M_create_pr->counterppnumber();  //add and manggil counter numberpp
				 
				  $this->session->set_flashdata('pesan_succes','Insert P.P '. $str_upload_success.' Successfull..!!');
				
				  if ($this->session->userdata("ses_strsend_aproval")=="1")
				  {
					  $this->session->unset_userdata("ses_strsend_aproval");
				  } 

			}else{
				  $this->session->set_flashdata('pesan_insert_fail','Insert P.P '. $str_upload_success.' Failed..!!');
		    }
		}


public function show_form_edit()
 {
	$tampil_edit_pp= $this->M_create_pr->get_edit_pp();
	$tampil_edit_det_pp= $this->M_create_pr->get_edit_detail_pp();

	$tampil_vendor =$this->M_create_pr->get_vendor();
	$tampil_cur = $this->M_create_pr->get_currency();

    $dept  = $this->session->userdata('dept') ;
	$branch  = $this->session->userdata('name_branch') ;
	$company = $this->session->userdata('company') ;

	foreach ($tampil_edit_det_pp as $row)
	{
		 $data['ppnumber'] =$row->id_master;
	}

  	if ($tampil_edit_pp == null and $tampil_edit_det_pp ==null)
	{
	    $data['ceck_row'] = "0"; // jika tidak ketemu
	    $data['tampil_pp'] =$tampil_edit_pp;
		//$data['tampil_coa']=$this->M_create_pr->get_coa();
		$data['tampil_type_purchase']=$this->M_create_pr->get_purchase_type();
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
	    $data['pesan'] = 'Data Type Not Found';
	    $data['show_view']='pr_form/V_form_edit_pr';
	    $this->load->view('dashboard/Template',$data);
     }else{
	 	$data['ceck_row'] = "1"; // jika  ketemu
	    $data['ino'] = ""; // variable kosong buat nocounter perulangan detail
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
		$data['tampil_pp'] =$tampil_edit_pp;
		//$data['tampil_coa']=$this->M_create_pr->get_coa();
		$data['tampil_type_purchase']=$this->M_create_pr->get_purchase_type();
		$data['tampil_det_pp']=$tampil_edit_det_pp;
		$data['tampil_vendor']=	$tampil_vendor ;
	    $data['tampil_curr']= $tampil_cur;
		$data['show_view']='pr_form/V_form_edit_pr';
		$this->load->view('dashboard/Template',$data);
     }

 }

public function do_edit_pp() //lakukan submit edit data
{



		   if ($this->M_create_pr->edit_pp())
		   {
		      $this->do_upload_update(); //manggil fungsi Upload File
		   	     if ($this->session->flashdata('pesan_upload_succes') !=""){
				      $str_upload_success = "& Upload Quotation";
				  }else{
				  	  $str_upload_success = "";
				  }
				  $this->session->set_flashdata('pesan_succes','update P.P '. $str_upload_success.' Successfull..!!');
				 // echo "Update Successfully"	;
		   }else{
				//  echo "Update failed"	;
				 $this->session->set_flashdata('pesan_succces','Data  Update Failed!!!!!! ....');
		   }

}


public function do_delete_pp()	 //lakukan submit delete data
{
	
	$this->M_create_pr->delete_with_edit_flag();
	/* if ($this->M_create_pr->delete_with_edit_flag())
	 {
	     echo "Delete Successfully";
		 //$this->session->set_flashdata('pesan','Delete Successfully....');
		 redirect('pr_form/c_create_pp');
	  }else{
		  //$this->session->set_flashdata('pesan','No Data Selected To Remove!!');
		  redirect('pr_form/c_create_pp');
	 } */
}

// public function do_search_data()  // fungsi cari data controler
//  {
// 		$tampung_cari = $this->M_create_pr->search_data(); // manggil hasil cari di model

// 		if($tampung_cari == null){
// 		   $dept  = $this->session->userdata('dept') ;
// 		   $branch  = $this->session->userdata('name_branch') ;
// 		   $company = $this->session->userdata('short') ;
// 		   $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;
// 		   $data['ceck_row'] = "0"; // jika tidak ketemu
// 		   $data['pp_view'] =$tampung_cari;
// 		   $data['pesan'] = 'Data PP Not a found';
// 		   $data['show_view'] = 'pr_form/V_table_pr';
// 		   $this->load->view('dashboard/Template',$data);
// 		}else{
// 		    $dept  = $this->session->userdata('dept') ;
// 		    $branch  = $this->session->userdata('name_branch') ;
// 		    $company = $this->session->userdata('short') ;
// 		    $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch. " | ". "Dept". " : ".$dept ;

// 			$data['ceck_row'] = "1"; // jika  ketemu
// 			$data['pp_view'] =$tampung_cari;
// 			$data['show_view'] = 'pr_form/V_table_pr';
// 		    $this->load->view('dashboard/Template',$data);
// 		}
//     }

public function send_flag_aproval()
{    
    $strid_master =$this->input->post('msg');
	if ($this->M_create_pr->update_send_flag_aproval())
    {
		 	 
		$this->kirim_email();
		redirect('pr_form/c_create_pr');
	}else{
		$this->session->set_flashdata('pesan','Sending Approval Failed');
		redirect('pr_form/c_create_pr');
	}
}



 public function kirim_email(){

		   $this->load->library('email');
		   //konfigurasi email
		  // $config = array();
		 /*  $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T";
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;
		   //------google
		   //$config['smtp_host']    = 'ssl://smtp.googlemail.com';

		   //-----------exchange
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   //$config['smtp_port']  = '587';
		   $config['smtp_port']  = '5220';
		   //$config['smtp_port']  = '465'; //port gmail
		   $config['newline']    = "\r\n";
		   $config['mailtype']   ="html" ;
		   //$config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   //$config['smtp_pass']= "3mim@zd4!";
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5"; */
		   
		   $config['charset']    = 'utf-8';
           $config['newline']    = "\r\n";
           $config['mailtype'] = 'html'; 
		   $config['smtp_user']= "3m1mazda@gmail.com"; //Gmail
		   $config['smtp_pass']= "3mim@zd4!"; 
           $config['validation'] = TRUE;      
           $this->email->initialize($config);
		   //--------------------------------

			$strponumber =$this->session->userdata("ses_noppnew");

			if ($strponumber != '')
			 {
				//Select untuk email master PP----------------------------
				//$query =$this->db->query("select user_submission,header_desc,date_send_aproval,term_top,currency,gran_total,ppn,pph,type_purchase,gran_totalppn,flag_purchase,remarks from qv_head_pp_complite where id_master ='".$strponumber."'");

				// API
				$parameter = array('id_master' => $strponumber);
				$query = http_request_get(API_URL.'/PRForm/getPP', $id_master);
				$query = json_decode($query);
				// API

			    if ($query->length > 0 ) {
					$data_email['str_pp_master'] = $query->data;

				    //Select untuk email deteail PP--------------------------------
					//$query_detail =$this->db->query("select * from tbl_detail_pp where id_master ='".$strponumber."'and status = '1'");

						// API
						$parameter = array('id_master' => $strponumber);
						$query = http_request_get(API_URL.'/PRForm/getDetailPP', $parameter);
						$query = json_decode($query);
						// API

					if ($query_detail->length > 0 ) {
					   $data_email['str_pp_detail'] = $query_detail->data;
					   $data_email['intno'] = ""; //untuk counter angka table

					   //simpan di dalam variable message untuk di kirim sebagai Content Email
					   $message = $this->load->view('pr_form/V_content_email',$data_email,true);

					}
			   }
			}

			$result = $this->email ;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");


			$sender_email = "helpdesk@eurokars.co.id";
			$sender_name = "Purchase_Request_Notification";

			
				$struseremail = $this->session->userdata('email');
				$strheademail = $this->session->userdata('email_head');
				$strccemail = $this->session->userdata('email_cc');

				if($strccemail == "-" || $strccemail ==  "" || $strccemail == null) {
					$to = $struseremail.",".$strheademail;
				} else {
					$to = $struseremail.",".$strheademail.",".$strccemail;
				}
			//end-----------------------------------------

			//$to = 'brian.yunanda@eurokars.co.id';

			

			$subject = "Request Approval -- EurokarsGroup";
			$this->email->from($sender_email, $sender_name);
			$this->email->to($to);
			$this->email->subject($subject);

			//$this->email->message(print_r($message, true));

		    $this->email->message($message);// tanpa array

		     if ($this->email->send()) {
			    $this->session->set_flashdata('pesan_succes','Sending Approval Successfully');
			   /*redirect(current_url());
		     } else {
			    show_error($this->email->print_debugger()); */
		     }


			//Destroy session per variable
			 $this->session->unset_userdata('ses_noppnew');
			 $this->session->unset_userdata('ses_usersubnew')	;
			 $this->session->unset_userdata('ses_desc_detail');
			 $this->session->unset_userdata('ses_urgent');
			 $this->session->unset_userdata('ses_grandtotal');
			 $this->session->unset_userdata('ses_term');
			 $this->session->unset_userdata('ses_remarks');


 }

public function multiple_submit() //fungsi submit dengan banyak button dalam satu form
{

if ($this->input->post('btnsave')){
    $this->do_insert_pp();
	}else{
	if ($this->input->post('btndel')){
		$this->do_delete_pp();
	}else{ 
		if ($this->input->post('btnedit')){
			$this->show_form_edit();
		}else{
			if ($this->input->post('btnsend')){
				$this->do_insert_pp();
				$this->kirim_email();
			}else{
				if ($this->input->post('btnsendtab')){
					$this->send_flag_aproval();
				}else{
					if ($this->input->post('btnsave_edit')){
					   $this->do_edit_pp();
					}else{
						if ($this->input->post('btnsend_edit')){
							$this->do_edit_pp();
							$this->kirim_email();
						}else{
							if ($this->input->post('btnremovepdf')){
								$this->do_del_quotation();
							}else{
								redirect('pr_form/c_create_pp');
							}
						}
					}
				}
			}
		}
	}
  }
}

public function get_idtrans_modal()
{
     $tampungmaster = $this->M_create_pr->m_mastergetid_pp();
	 $tampungdetail = $this->M_create_pr->m_getid_pp();
	 
	 $nopp = $this->input->post('id_pp',TRUE); //

	 $no="1";
	// echo "<div style='overflow-x:auto;'>" ;
	 echo "<p>";
	 echo "<div><label class ='btn-danger'>"."PO / PP N.O : ".$nopp."</label></div>";
	 echo "<div class='table-responsive'>" ;
	 echo "<table class='table table-striped table-bordered table-hover' width='70%'>";
	 echo  "<tr style='font-weight:bold; font-size:11px' class='btn-info'>";
	 echo  "<td width='1%' align='center'>No</td>";
	 echo  "<td width='20%'  align='center'>Item Type</td>";
	 echo  "<td width='20%'  align='center' >Description</td>";
	// echo  "<td width='10%'>P.O Reff</td>";
	// echo  "<td width='10%'>C.O.A</td>";
	// echo  "<td width='10%'>No Actifity</td>";
	 echo  "<td  align='center' >Qty</td>";
	 echo  "<td width='40%' align='center'>Unit Price</td>";
	 echo  "<td width='40%' align='center' >Total Price</td>";
	/* echo  "<td width='2%'>PPN Type</td>";
	 echo  "<td width='2%'>PPH Type</td>";
	 echo  "<td width='15%'>PPN Amount</td>";
	 echo  "<td width='20%'>PPH Amount</td>"; */
	 echo  "</tr> ";
		foreach ($tampungdetail as $row_jq) {
		    echo  "<tr style='font-size:12px'> ";
			echo '<td >'.$no++.'</td>';
			echo '<td>'.$row_jq->desc.'</td>';
			echo '<td>'.$row_jq->spec.'</td>';
			//echo '<td>'.$row_jq->po_reff.'</td>';
			//echo '<td>'.$row_jq->coa.'</td>';
			//echo '<td>'.$row_jq->no_act.'</td>';
			echo '<td>'.$row_jq->qty.'</td>';
			echo '<td>'.number_format($row_jq->harga,2,'.',',').'</td>';
			echo '<td>'.number_format($row_jq->total,2,'.',',').'</td>';
			//echo '<td>'.$row_jq->tax_type.'</td>';
		   // echo '<td>'.$row_jq->tax_typepph.'</td>';
			//echo '<td>'.number_format($row_jq->tax_detail,2,'.',',').'</td>';
			//echo '<td>'.number_format($row_jq->tax_detailpph,2,'.',',').'</td>';
		    echo "</tr>";

		}

	 foreach ($tampungmaster as $row_jm) {
	     echo  "<tr style='font-weight:bold' class='btn-success'>";
		 echo  "<td width='27%' colspan='0'>Remarks :</td>";
		 echo  "<td width='40%' colspan='5'>".$row_jm->remarks."</td>";
		 echo  "</tr> ";

		if($row_jm->reason_reject != '') {
			echo  "<tr style='font-weight:bold' class='btn-danger'>";
			echo  "<td width='27%' colspan='0'>Reject Reason :</td>";
			echo  "<td width='40%' colspan='5'>".$row_jm->reason_reject."</td>";
			echo  "</tr> ";
		}

		 echo  "<tr style='font-weight:bold' class ='btn-danger' >";
		 echo  "<td width='25%' colspan='5' align='right'>Total (".$row_jm->currency.") :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_total,2,'.',',')."</td>";
		 echo  "</tr> ";
		/* echo  "<tr style='font-weight:bold' class ='btn-light'>";
		 echo  "<td width='25%' colspan='9' align='right'>PPN Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->ppn,2,'.',',')."</td>";
		 echo  "</tr> "; 
		 echo  "<tr style='font-weight:bold' class ='btn-light'>";
		 echo  "<td width='25%' colspan='9' align='right'>PPH Amount :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->pph,2,'.',',')."</td>";
		 echo  "</tr> ";
		 echo  "<tr style='font-weight:bold' class ='btn-danger'>";
		 echo  "<td width='25%' colspan='9' align='right'>Total + Tax :</td>";
		 echo  "<td width='40%'>".number_format($row_jm->gran_totalppn,2,'.',',')."</td>";
		 echo  "</tr> ";*/
		 echo "</table>";
		 echo "</div>";
	 }

	 $data_attach = $this->M_create_pr->getQuotation($nopp);
	 $data_attach = $data_attach[0];

	 if($data_attach->attach_quo != '') {
		$attach_quo = '<a id="attach_quo" onClick="PDFPopup(this)" req_id='.base_url($data_attach->attach_quo).' href="#" class="btn btn-app btn-primary btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span></a>";
	 } else {
		$attach_quo = '<font color="red">'."No Quo".'</font>';
	 }

	 echo '<div class="modal-footer">
	 <div class="col-md-6 col-md-offset-4 text-left">
	 Attached Quotation : '.$attach_quo.'
	                        
	 </div>

	 <div class="col-md-2">
	 <button type="button" class="btn btn-app btn-primary btn-xs radius-8 btnclose" data-dismiss="modal">Close</button>                       
					
	 </div>

 </div>';
}

function cath_data_fordatatables(){
	$str_idcompany = $this->session->userdata("id_company"); 
	$str_iddept = $this->session->userdata("id_dept");
	$str_submission = $this->session->userdata("name");
	$str_status_send ="0";
	$str_status_sendreject ="-1";

	/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
	server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
	sesuai dengan urutan yang sebenarnya */
	@$draw=$_REQUEST['draw'];

	/*Jumlah baris yang akan ditampilkan pada setiap page*/
	@$length=$_REQUEST['length'];
	//end-------------------------------------------------
	
	/*Offset yang akan digunakan untuk memberitahu database
	dari baris mana data yang harus ditampilkan untuk masing masing page
	*/
	@$start=$_REQUEST['start'];
	//end-------------------------------------------------------------

	/*Keyword yang diketikan oleh user pada field pencarian*/
	@$search=$_REQUEST['search']["value"];
	//end---------------------------------------------------
	
	//order short column
	$column_order =array(null,"id_master","vendor","user_submission","date_pp","currency","gran_total","gran_totalppn","term_top");
	//end------------------------------------------

	/*Menghitung total data didalam database*/

	$param_count = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'user_submission' => $str_submission);
	$param_count = http_request_get(API_URL.'/PRForm/countAllResult', $param_count);
	$param_count = json_decode($param_count, true);
	$total = $param_count['length'];
	//end----------------------------------------
	
	/*Mempersiapkan array tempat kita akan menampung semua data
	yang nantinya akan server kirimkan ke client*/
	$output=array();
	//end------------------------------------------------------

	/*Token yang dikrimkan client, akan dikirim balik ke client*/
	$output['draw']=$draw;
	//end--------------------------------------------------------
	 
	/*
	$output['recordsTotal'] adalah total data sebelum difilter
	$output['recordsFiltered'] adalah total data ketika difilter
	*/
	$output['recordsTotal']=$output['recordsFiltered']=$total;
	//end----------------------------------------

	/*disini nantinya akan memuat data yang akan kita tampilkan pada table client*/
	$output['data']=array();
	//end----------------------------------------

	/*Jika $search mengandung nilai, jika user ada melakukan search */
	if($search!=""){
		$param_search = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'user_submission' => $str_submission, 'search' => $search, 'limit' => $length, 'start' => $start);
		$param_search = http_request_get(API_URL.'/PRForm/datatableSearch', $param_search);
		// balikan si API
		$param_search = json_decode($param_search, true);
		$output['recordsTotal']=$output['recordsFiltered']=$param_search['length'];
	}

	// proses paling awal saat load, dan saat user ada klik pada colum di table nya
	if (isset($_REQUEST["order"])){
		$parameter = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'user_submission' => $str_submission, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => $column_order[$_REQUEST["order"][0]["column"]], 'dir' => $_REQUEST["order"][0]['dir']);	
	}else{
		$parameter = array('id_company' => $str_idcompany, 'id_dept' => $str_iddept, 'user_submission' => $str_submission, 'search' => $search, 'limit' => $length, 'start' => $start, 'order' => 'date_pp', 'dir' => 'desc');
	}

		$query = http_request_get(API_URL.'/PRForm/datatableSearch', $parameter);
		$query = json_decode($query, true);

	// proses paling awal saat load


	/* menapilkan total record saat pencarian */


	foreach ($query['data'] as $row_tbl) {

		 if ($row_tbl['status_send_aprove'] == "-1")
		 {
			   $info_aproval = '<label style="color:red">'."Reject".'</label>';
		 }else{
			   $info_aproval = "New Request";
		 }

		 If ($row_tbl['attach_quo'] != "")
		  {
		   $attach_quo = '<div  align="center"><a href='.base_url($row_tbl['attach_quo']).' style="text-decoration:none" class="btn btn-app btn-primary btn-xs radius-8">'."<span class='glyphicon glyphicon-search' aria-hidden='true'></span>".'View'.'</a></div>' ;
		 }else{
			$attach_quo = '<div  align="center" style=" color:#EB293D">'."No Quo".'</div>';
		 }

		 $chk_idmaster ='<div align="center"><input id="checkchild" name="msg[]" type="checkbox" value='.$row_tbl["id_master"].' class="ace" req_id_del='.$row_tbl["id_master"].' />
	   <span class="lbl"></span> ';


	   $btn_view_detail = '<div align="center"><a href="#" class="btn btn-app btn-primary btn-xs radius-8 detail" id="detail" style="text-decoration:none" req_id='.$row_tbl["id_master"].'> <span class="fa fa-folder-open-o " aria-hidden="true"></span></a></div>';


//Pembedaan Warna pada type purchase.
	   if ($row_tbl['flag_purchase'] == "1") {
		   $type_purchase = "<div align='center'><span class='label label-info arrowed-in arrowed-in-right'>Indirect</span></div>";   
	   }else{
			 if ($row_tbl['flag_purchase'] == "2") {
				   $type_purchase = "<div align='center'><span class='label label-success arrowed-in arrowed-in-right'>Direct Unit</span></div>";   
			   }else{
					 if ($row_tbl['flag_purchase'] == "3") {
					   $type_purchase = "<div align='center'><span class='label label-yellow arrowed-in arrowed-in-right'>Direct Part</span></div>";   
				   }else{
						 if ($row_tbl['flag_purchase'] == "4") {
							   $type_purchase = "<div align='center'><span class='label label-purple arrowed-in arrowed-in-right'>Project</span></div>";
						   }else{
								$type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>Shiping</span></div>";
						 };
				 };
			 };   
		 };
//end------------------------------		
		 /* $id_master ="";
		  if ($row_tbl['flag_send_pr'] == "-1") {
			   $id_master =  $row_tbl["id_master"];
		  }*/

		  if($row_tbl['term_top'] > 1) {
				$day = 'Days';
			} else {
				$day = 'Day';
			}

		  if ($row_tbl['flag_send_pr'] == "-1")
		 {
			   $info_aproval = '<label style="color:red">'."Reject".'</label>';

			   $row_tbl['id_master'] = '<span class="badge badge-danger">'.$row_tbl['id_master'].'</span>';
			   $row_tbl['vendor'] = '<span class="badge badge-danger">'.$row_tbl['vendor'].'</span>';
			   $row_tbl['date_pp'] = '<span class="badge badge-danger">'.date('d-m-Y', strtotime($row_tbl['date_pp'])).'</span>';
			   $row_tbl['currency'] = '<span class="badge badge-danger">'.$row_tbl['currency'].'</span>';
			   $row_tbl['gran_total'] = '<span class="badge badge-danger">'.number_format($row_tbl['gran_total'],2,'.',',').'</span>';
			   $row_tbl['gran_totalppn'] = '<span class="badge badge-danger">'.number_format($row_tbl['gran_totalppn'],2,'.',',').'</span>';
			   $row_tbl['term_top'] = '<span class="badge badge-danger">'.$row_tbl['term_top'].' '.$day.'</span>';

			   $type_purchase = $row_tbl['type_purchase'];
			   $type_purchase = "<div align='center'><span class='label label-danger arrowed-in arrowed-in-right'>".$type_purchase."</span></div>";
			   //$chk_idmaster = '<center><span class="btn btn-danger glyphicon glyphicon-remove btn-xs" aria-hidden="true"></span></center>';
		 }else{
			   $info_aproval = "New Request";

			   $row_tbl['id_master'] = $row_tbl['id_master'];
			   $row_tbl['vendor'] = $row_tbl['vendor'];
			   $row_tbl['date_pp'] = date('d-m-Y', strtotime($row_tbl['date_pp']));
			   $row_tbl['currency'] = $row_tbl['currency'];
			   $row_tbl['gran_total'] = number_format($row_tbl['gran_total'],2,'.',',');
			   $row_tbl['gran_totalppn'] = number_format($row_tbl['gran_totalppn'],2,'.',',');
			   $row_tbl['term_top'] = $row_tbl['term_top'].' '.$day;
			   $type_purchase = $type_purchase;
			   //$chk_idmaster = $chk_idmaster;
		 }

		$output['data'][]=array($chk_idmaster,
								$row_tbl['id_master'],
								//$row_tbl['short'],
								//$row_tbl['dept'],
								$row_tbl['vendor'],
								$row_tbl['date_pp'],
								//$row_tbl['header_desc'],
								//substr($row_tbl['remarks'],0,25),
								//$row_tbl['nomor_coa'],
								//$row_tbl['type_purchase'],
								$type_purchase,
								$row_tbl['currency'],
								$row_tbl['gran_total'],
								//number_format($row_tbl['ppn'],2,'.',','),
								//number_format($row_tbl['pph'],2,'.',','),
								//$row_tbl['gran_totalppn'],
								$row_tbl['term_top'],
								//$row_tbl['user_submission'],
								//$info_aproval,
								$btn_view_detail,
								//$attach_quo

						  );
	}
	echo json_encode($output);
}

//   public function suggest_vendor()
// 	{
// 		$id_company = $this->session->userdata('id_company');
// 	    $id_dept = $this->session->userdata('id_dept');
// 		$status_del = "1"; //flag status delete pada table vendor

// 	    $icon = $this->input->post('vendor',TRUE);

// 	    $this->load->database();
// 	    $this->db->select('*');
// 	    $this->db->like("id_vendor",$icon);
// 		$this->db->where('id_company',$id_company);
// 		$this->db->where('id_dept',$id_dept);
// 		$this->db->where('status',$status_del);
// 	    $this->db->or_like("vendor",$icon);
// 		$this->db->where('id_company',$id_company);
// 		$this->db->where('id_dept',$id_dept);
// 		$this->db->where('status',$status_del);
// 		//add 6 Desember 2017 by Yoel
// 		$this->db->or_like("code_vendor",$icon);
// 		$this->db->where('id_company',$id_company);
// 		$this->db->where('id_dept',$id_dept);
// 		$this->db->where('status',$status_del);
// 	    $hasil = $this->db->get('tbl_vendor')->result();
       
	   
// 		$json_array = array();
// 		foreach ($hasil as $row)
// 		$json_array[]= array("value" =>$row->vendor,
//                       "label" =>$row->code_vendor." | ".$row->vendor." | ".$row->id_vendor,
// 					  "code" => $row->code_vendor,
// 					  "idvendor" => $row->id_vendor,
// 					  );
// 		echo json_encode($json_array);

// 	}

	// public function suggest_coa()
	// {

	// 	$id_company = $this->session->userdata('id_company');
	//     $id_dept = $this->session->userdata('id_dept');
	// 	$status_del = "1"; //flag status delete pada table vendor

	//     $icon = $this->input->post('coa',TRUE);

	//     $this->load->database();
	//     $this->db->select('*');
	//     $this->db->like("nomor_coa",$icon);
	// 	$this->db->where('id_company',$id_company);
	// 	$this->db->where('id_dept',$id_dept);
	// 	$this->db->where('status',$status_del);
	// 	$this->db->or_like("desc_coa",$icon);
	// 	$this->db->where('id_company',$id_company);
	// 	$this->db->where('id_dept',$id_dept);
	// 	$this->db->where('status',$status_del);
	//     $hasil = $this->db->get('tbl_coa')->result();

	// 	$json_array = array();
	// 	foreach ($hasil as $row2)
	// 	$json_array[]= array(//"value"=>$row2->desc_coa." | ".$row2->nomor_coa,
	// 						 "value"=>$row2->nomor_coa,
	// 						 "label" =>$row2->desc_coa." | ".$row2->nomor_coa,
	// 						 //"idcoa"=>$row2->id_coa
	// 						 );
	// 	echo json_encode($json_array);

	// }


	//  

	public function generatePRNumber() {
        $short_company = $this->session->userdata('short');
		$short_branch = $this->session->userdata('branch_short');
		$short_dept = substr(($this->session->userdata('dept')),0,3);
        $tahun = substr(date('Y'), 2, 3);
        $bulan = date('m');
        $tanggal = date('d');
		$total = $this->M_create_pr->generatePRNumber();
        $total = $total['data'][0]['id_master'];
        $total = substr($total, -3);
        $total = ((int)$total+1);

        if($total<10) {
            $total = '00'.$total;
        } elseif($total<100) {
            $total = '0'.$total;
        } elseif($total<1000) {
            $total = $total;
		}

		$kode = $short_company.'/'.$short_branch.'/'.$short_dept.'/'.$tahun.'/'.$bulan.'/'.$tanggal.'/'.$total;
		
		return $kode;

	} 

	public function disconnect()
	{
		$ddate = date('d F Y');
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];
		$strid_user =$this->session->userdata('id');

		$data=array('id' => $strid_user, 'login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);
		// $this->db->where('id',$strid_user);
		// $this->db->update('tbl_user',$data); // update login terakhir user

		$update = http_request_put(API_URL.'/login/doLogout', $data);
		$update = json_decode($update, true);

		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');

  }

}
