<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_control_history_wrs_mas extends MY_Controller
{
 	
  public function __construct()
    {
		parent::__construct();					
	    $this->load->model('m_control_history_wrs_mas/M_control_history_wrs_mas','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();	
	}
	
	public function index()
    {		
	   $this->show_table(); //manggil fungsi show_table			
	}	
		
	 public function show_table()
    {             					
	     $tampil_table_pp= $this->M_control_history_wrs_mas->tampil_wrs_history_stock()->result();	
	     $total_rows =$this->M_control_history_wrs_mas->tampil_wrs_history_stock()->num_rows();
	  	
		 $tampil_type = $this->M_control_history_wrs_mas->get_type_vehicle();
		 $tampil_branch = $this->M_control_history_wrs_mas->tampil_cabang_mazda()->result();		
										
		 $total_rows_history_wrs =$this->M_control_history_wrs_mas->hitung_jumlah_history_wrs()->num_rows();
		 $total_rows_dp_wrs_move_to_stock =$this->M_control_history_wrs_mas->hitung_jumlah_wrs_move_to_stock()->num_rows();										
										
	  if ($tampil_table_pp)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(5);
		$per_page =10;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'maserati_stock_sales/c_control_history_wrs_mas/c_control_history_wrs_mas/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 5;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['stock_view'] =$this->M_control_history_wrs_mas->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
			
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
		$data['ceck_row'] = $total_rows;
		$data['tampil_branch'] = $tampil_branch ;
		$data['tampil_type'] = $tampil_type ;
		$data['count_row_history_wrs'] = $total_rows_history_wrs;
		$data['count_row_wrs_move_to_stock'] = $total_rows_dp_wrs_move_to_stock;	
		$data['count_row_all_wrs'] = $total_rows_history_wrs + $total_rows_dp_wrs_move_to_stock;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'v_control_history_wrs_mas/V_control_history_wrs';		
		$this->load->view('dashboard/Template',$data);				
	  }else{	
	    	   
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
	    $data['ceck_row'] = $total_rows;
		$data['tampil_branch'] = $tampil_branch ;
		$data['tampil_type'] = $tampil_type ;
		$data['count_row_history_wrs'] = $total_rows_history_wrs;
		$data['count_row_wrs_move_to_stock'] = $total_rows_dp_wrs_move_to_stock;	
		$data['count_row_all_wrs'] = $total_rows_history_wrs + $total_rows_dp_wrs_move_to_stock;	
		$data['pesan'] = 'Data stock unit table is empty';		
		$data['show_view'] = 'v_control_history_wrs_mas/V_control_history_wrs';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }	
  
  public function do_search_data()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_control_history_wrs_mas->search_data()->result();  
		$total_rows =$this->M_control_history_wrs_mas->search_data()->num_rows();			
		
		 $tampil_type = $this->M_control_history_wrs_mas->get_type_vehicle();
		 $tampil_branch = $this->M_control_history_wrs_mas->tampil_cabang_mazda()->result();		
		
				 		 								
		 $total_rows_history_wrs =$this->M_control_history_wrs_mas->total_history_wrs_cari_data()->num_rows();
		 $total_rows_wrs_move_to_stock =$this->M_control_history_wrs_mas->total_history_wrs_move_stock_cari_data()->num_rows();			
		
		 $branch  = $this->session->userdata('name_branch') ; 
	     $company = $this->session->userdata('company') ;		
		
		if($tampung_cari == null){		   		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = $total_rows;	
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;
			$data['count_row_history_wrs'] = $total_rows_history_wrs;
		    $data['count_row_wrs_move_to_stock'] = $total_rows_wrs_move_to_stock;	
			$data['count_row_all_wrs'] =$total_rows_history_wrs + $total_rows_wrs_move_to_stock;	
			$data['stock_view']  = "";
			$data['show_view'] = 'v_control_history_wrs_mas/V_control_history_wrs';
			$this->load->view('dashboard/Template',$data);			
		}else{				   		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = $total_rows;
			$data['tampil_branch'] = $tampil_branch ;	
			$data['tampil_type'] = $tampil_type ;
			$data['count_row_history_wrs'] = $total_rows_history_wrs;
		    $data['count_row_wrs_move_to_stock'] = $total_rows_wrs_move_to_stock ;
			$data['count_row_all_wrs'] =$total_rows_history_wrs + $total_rows_wrs_move_to_stock;		
			$data['stock_view']  = $tampung_cari;				
			$data['show_view'] = 'v_control_history_wrs_mas/V_control_history_wrs';
			$this->load->view('dashboard/Template',$data);	 
		}	  
    }
	
	 public function do_search_date_data()  // fungsi cari data controler
 {				  			    	
		$tampung_cari = $this->M_control_history_wrs_mas->get_search_date()->result();  
		$total_rows =$this->M_control_history_wrs_mas->get_search_date()->num_rows();			
		
		$tampil_type = $this->M_control_history_wrs_mas->get_type_vehicle();
		$tampil_branch = $this->M_control_history_wrs_mas->tampil_cabang_mazda()->result();		
										
		$total_rows_history_wrs =$this->M_control_history_wrs_mas->total_history_wrs_date()->num_rows();
		$total_rows_wrs_move_to_stock =$this->M_control_history_wrs_mas->total_wrs_move_stock_date()->num_rows();		
		
		$branch  = $this->session->userdata('name_branch') ; 
	    $company = $this->session->userdata('company') ;		
		
		if($tampung_cari == null){		   		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = $total_rows;	
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;
			$data['count_row_history_wrs'] = $total_rows_history_wrs;
		    $data['count_row_wrs_move_to_stock'] = $total_rows_wrs_move_to_stock;	
		    $data['count_row_all_wrs'] =$total_rows_history_wrs + $total_rows_wrs_move_to_stock;
			$data['stock_view']  = "";
			$data['show_view'] = 'v_control_history_wrs/V_control_history_wrs';
			$this->load->view('dashboard/Template',$data);			
		}else{				   		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = $total_rows;
			$data['tampil_type'] = $tampil_type ;
			$data['tampil_branch'] = $tampil_branch ;	
			$data['count_row_history_wrs'] = $total_rows_history_wrs;
		    $data['count_row_wrs_move_to_stock'] = $total_rows_wrs_move_to_stock;
			$data['count_row_all_wrs'] =$total_rows_history_wrs + $total_rows_wrs_move_to_stock;	
			$data['stock_view']  = $tampung_cari;				
			$data['show_view'] = 'v_control_history_wrs/V_control_history_wrs';
			$this->load->view('dashboard/Template',$data);	 
		}	  
    }
  
  public function multiple_submit()
  {
	  if ($this->input->post('btncari')):	
		  $this->do_search_data();
	   else:  
	   	     if ($this->input->post('btnshort')):				    
		         $this->tampil_branch_combo();
	   	     else:  
			    if ($this->input->post('btncaridate')) :
					$this->do_search_date_data();
				else:	
					if ($this->input->post('btnexport')) :					    
						$this->toExcelAll();
					else:							
	  	        		redirect('maserati_stock_sales/c_control_history_wrs_mas/c_control_history_wrs_mas'); 			   	
					endif;	
				endif;
			endif;
		endif;	   	    
	  
  }
  
  public function tampil_branch_combo()
  {	    		   			
        $tampung_cari = $this->M_control_history_wrs_mas->shorting_table_branch()->result();  
		$total_rows =$this->M_control_history_wrs_mas->shorting_table_branch()->num_rows();			
		
		$tampil_branch = $this->M_control_history_wrs_mas->tampil_cabang_mazda()->result();												
		
		$total_rows_history_wrs =$this->M_control_history_wrs_mas->hitung_jumlah_history_wrs()->num_rows();
		$total_rows_wrs_move_to_stock =$this->M_control_history_wrs_mas->hitung_jumlah_wrs_move_to_stock()->num_rows();	
		
		$tampil_type = $this->M_control_history_wrs_mas->get_type_vehicle();
		
		$branch  = $this->session->userdata('name_branch') ; 
	    $company = $this->session->userdata('company') ;		
		
		//print_r($tampung_cari);
		if($tampung_cari == null){			      		  		   			
		    $this->session->set_flashdata('pesan_fail','Data not found !');				 			
	        $data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch."  "."Data not found...!!!";
			$data['ceck_row'] = $total_rows;
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;	
			$data['count_row_history_wrs'] = $total_rows_history_wrs;
		    $data['count_row_wrs_move_to_stock'] = $total_rows_wrs_move_to_stock;	
			$data['count_row_all_wrs'] =$total_rows_history_wrs + $total_rows_wrs_move_to_stock;		
			$data['stock_view']  = "";
			$data['show_view'] = 'v_control_history_wrs_mas/V_control_history_wrs';
			$this->load->view('dashboard/Template',$data);			
		}else{				     		   						
			$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
			$data['ceck_row'] = $total_rows;	
			$data['tampil_branch'] = $tampil_branch ;
			$data['tampil_type'] = $tampil_type ;	
			$data['count_row_history_wrs'] = $total_rows_history_wrs;
		    $data['count_row_wrs_move_to_stock'] = $total_rows_wrs_move_to_stock;
			$data['count_row_all_wrs'] =$total_rows_history_wrs + $total_rows_wrs_move_to_stock;		
			$data['stock_view']  = $tampung_cari;				
			$data['show_view'] = 'v_control_history_wrs_mas/V_control_history_wrs';
			$this->load->view('dashboard/Template',$data);	 
		} 	 
  }
  
   public function toExcelAll() {	 
        $this->form_validation->set_rules('txtrptdtstart','txtrptdtstart','required');	
		$this->form_validation->set_rules('txtrptdtend','txtrptdtend','required');		
		
		if($this->form_validation->run()==FALSE):			
		   echo $this->session->set_flashdata('pesan','Date start and Date end Report is Empty, Report can not be prosses!!');
		   redirect('maserati_stock_sales/c_control_history_wrs_mas/c_control_history_wrs_mas'); 			   
		else:
			$tampil_to_excel =  $this->M_control_history_wrs_mas->ToExcelAll(); 		
			$total_rows_history_wrs =$this->M_control_history_wrs_mas->ToExcelAll_total_history_wrs()->num_rows();
			$total_rows_wrs_move_to_stock =$this->M_control_history_wrs_mas->ToExcelAll_total_wrs_move_stock()->num_rows();	
			
			$data['data_excel'] = $tampil_to_excel ;	
			$data['count_row_history_wrs'] = $total_rows_history_wrs;
			$data['count_row_wrs_move_to_stock'] = $total_rows_wrs_move_to_stock;	
			$data['count_row_all_wrs'] = $total_rows_history_wrs + $total_rows_wrs_move_to_stock;			
			$this->load->view('v_control_history_wrs_mas/excel_view',$data);
		endif;	
    }
  
	
}