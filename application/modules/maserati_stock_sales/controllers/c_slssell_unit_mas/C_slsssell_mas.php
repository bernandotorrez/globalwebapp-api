<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_slsssell_mas extends MY_Controller
{
 	
  public function __construct()
    {
		parent::__construct();					
	    $this->load->model('m_slssell_unit_mas/M_slssell_mas','',TRUE);		   		
		$this->load->library('pagination'); //call pagination library
		$this->load->helper('form');
		$this->load->helper('url');			
		$this->load->database();	
	}
	
	public function index()
    {		
	       $this->show_table(); //manggil fungsi show_table		    		   		
	}	
		
	 public function show_table()
    {    $this->kirim_email_due_date(); 	         					
	     $tampil_table_sell= $this->M_slssell_mas->tampil_add_sell()->result();	
	     $total_rows =$this->M_slssell_mas->tampil_add_sell()->num_rows();
	  									
	  if ($tampil_table_sell)
		{			
		//pagnation--------------------------------------------																									
		$start_row= $this->uri->segment(5);
		$per_page =10;		
			if (trim($start_row ==''))
			{
				$start_row ==0;
			}		
		$config['full_tag_open'] = "<ul class='pagination'>"; //pagnation buat ke css nya bootstrap
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>"; //pagnation buat ke css nya bootstrap
						
		$config['base_url'] = base_url() . 'maserati_stock_sales/c_slssell_unit_mas/c_slsssell_mas/show_table/';		
        $config['total_rows'] = $total_rows;		
	    $config['per_page']=$per_page;		
	    $config['num_links'] = 5;		
	    $config['uri_segment'] = 5;													
		$this->pagination->initialize($config);
	    $data['pagination']=$this->pagination->create_links();	
		$data['sell_view'] =$this->M_slssell_mas->tampil_limit_table($per_page, $start_row);			
		//end pagnation ----------------------------------------
			
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
		$data['ceck_row'] = $total_rows;														
		$data['info_aproval'] = ""; //variable buat satus info rejected atau new created.														
		$data['show_view'] = 'v_slsstock_selling_mas/V_table_sell';		
		$this->load->view('dashboard/Template',$data);				
	  }else{		   
		$branch  = $this->session->userdata('name_branch') ; 
		$company = $this->session->userdata('company') ;					
		$data['header'] ="Company"." : ".$company. " | ". "Branch". " : ".$branch;
	    $data['ceck_row'] = $total_rows;	
		$data['pesan'] = 'Data sales unitt table is empty';		
		$data['show_view'] = 'v_slsstock_selling_mas/V_table_sell';
		$this->load->view('dashboard/Template',$data);				
	  } 
  }	
  
  //------------------------------------VIEW pashing result to modal popup-------------------
	public function get_idtrans_modal() {    
	     $status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete
	     $idtrans= $this->input->post('id_trans_sales',TRUE); //       
	     $query_master = $this->db->query("select * from qv_complite_master_sell_mas where id_trans_sales ='".$idtrans."'");  
         $query_detail = $this->db->query("select * from tbl_sales_unit_detail_mas where id_trans_sales ='".$idtrans."'and status ='".$status_aktif_record."'");  
		   		  
		   if($query_master->num_rows()>0 and $query_detail->num_rows() >0){	   		   
		      $data['str_trans_master']=$query_master->result();
			  $data['str_trans_detail']=$query_detail->result();
			  $data['intno']="";			  			  			  
			  $data['phasing_idtrans'] = $idtrans ;
			  $tampil_detail = $this->load->view('v_slsstock_selling_mas/V_conten_detail',$data,true);			 
			  echo $tampil_detail ;			 
		   }							   
    }      	
	//end phasing------------------------------------------------------------------------------	


	 public function kirim_email_due_date(){          
 		   
		   $this->load->library('email');				 								   		   		  	        
		   $config = array();
		   $config['charset'] = "utf-8";
		   $config['useragent'] = "I.T"; 
		   $config['protocol']= "smtp";
		   $config['mailtype']= "html";
		   $config['mailpath'] = '/usr/sbin/sendmail';
		   $config['charset'] = 'iso-8859-1';
		   $config['wordwrap'] = TRUE;				 
		   $config['smtp_host']  = 'webmail.eurokars.co.id' ;
		   $config['smtp_port']  = '587';		  		  		  
		   $config['newline']    = "\r\n";		  
		   $config['mailtype']   ="html" ;		 	   		  
		   $config['smtp_user']= "helpdesk@eurokars.co.id";
		   $config['smtp_pass']= "#ur0kar5"; 
		   //--------------------------------		   		  
		    		   		 		          				   				     		   				  									
			$date_now = date("Y-m-d");		
			$stridbranch= $this->session->userdata("branch_id");	
			$status_aktif_record = "1" ; // 1 = masih aktif		0 =terdelete			  
				//Select untuk email master sales----------------------------
				$query =$this->db->query("select * from qv_complite_master_sell where date_reminder_spk <= '".$date_now."'and 					 remaining_amount_customer !='"."0.00"."'and flag_notif_email ='".'0'."'and branch_id='".$stridbranch."'and status ='".$status_aktif_record."'");				
			    if ($query->num_rows() > 0 ) :					   					   					 
					   $data_email['reminder_duedate_selling'] = $query->result();														
					   $message = $this->load->view('v_slsstock_selling_mas/V_content_email_duedate',$data_email,true);		
					   $result = $this->email ;	
					   
					   //flag notif email jika 1 sudah di krirm email------------------------
					   $data_flagemail = array('flag_notif_email' => '1');
					   $this->db->where("(date_reminder_spk <= '".$date_now."')", NULL, FALSE); 
					   $this->db->update("tbl_sales_unit_transaksi",$data_flagemail);
					   //------------------------------
					   
					   $this->email->initialize($config);  
					   $this->email->set_newline("\r\n"); 
					   $sender_email = "helpdesk@eurokars.co.id"; 					   
					   $sender_name = "Epurchasing Notification";					   					    
																		
					   
					   //simpan session alamat email kedalam variable..
							$struseremail = $this->session->userdata('email');
							$strheademail = $this->session->userdata('email_head');
							$strfaemail = $this->session->userdata('email_fa');
						//end-----------------------------------------
					   
						//$to = 'brian.yunanda@eurokars.co.id';
						
						$to = $struseremail.",".$strheademail.",".$strfaemail;
						
						$subject = " --The remaining 3 days Due Date Payment Customer Eurokars Surya Utama--";				   
						$this->email->from($sender_email, $sender_name);
						$this->email->to($to);
						$this->email->subject($subject);  
						
						//$this->email->message(print_r($message, true));			   
					  
						$this->email->message($message);// tanpa array	
																 
						 if ($this->email->send()) :	  	
							//$this->session->set_flashdata('pesan_succes','Sending Approval Succesfully');
						//	redirect(current_url());
						// } else {
						 //   show_error($this->email->print_debugger());
						 endif;  						   											   
			    endif;			
				
				
					 
			
	 }

	
	
}