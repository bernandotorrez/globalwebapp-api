<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_crud_sell_mas extends CI_Model {	
				
	public $db_tabel_qv = 'qv_complite_master_sell_mas';	
	
	public function __construct() {
		parent::__construct();

		$this->load->helper('xss_filter_helper');
	}
		
	function get_trans_number(){	// view counter number when add new
	    $strbranchshort = $this->session->userdata('branch_short');				
	 	$stridbranch = $this->session->userdata('branch_id');				
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");			    
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	    $qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_sell_unit_mas where branch_id='".$stridbranch."'");
									  	   
	    $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); 	//check tanggal
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
				if($qcounter->num_rows()== 1):
					foreach($qcounter->result() as $cd):
						$tmp = ((int)$cd->code_max);
						$code = sprintf("%04s", $tmp);;
					endforeach;
					
					 if ($code== '0000') :						
						 $code = "0001";					 
					 endif;
					 
				 else:					  
					$code = "0001";								
				 endif;		
		 else:		
		       $code = "0001";					 
		 endif;
		 
	 //update tanggal ketable date--------------
	 $data = array('date_add'=>$strdates );																			
	 $this->db->update('tbl_date_compare',$data); 		 
	 //end--------------------
		 
	 //update tanggal ketable counter--------------
	  $data = array('counter'=> $code );	
	  $this->db->where('branch_id',$stridbranch);															  									
	  $this->db->update('tbl_counter_sell_unit_mas',$data); 		 
	 //end--------------------
		 				
	  return "INV"."/".$strbranchshort."/".$stryear."/".$strmonth."/".$strdates."/".$code ;	
	  //end----------------------
    }	
	
	function counter_sell_number() //counter stock no whesn save
	{
		$strbranchshort = $this->session->userdata('branch_short');	 
		$stridbranch = $this->session->userdata('branch_id');				
		$stryear = substr(date("Y"),-3,3);
		$strmonth = date("m");			    
		$strdates = date("d");	//date (tanggal buat comparasi pada table)
				    
		 
 	    $qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_sell_unit_mas where branch_id='".$stridbranch."'");
									  	   
	    $qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); 	//check tanggal
	   
	    $code = ""; //empty code
		 if ($qdate->num_rows()== 1) :			
				if($qcounter->num_rows()== 1):
					foreach($qcounter->result() as $cd):
						$tmp = ((int)$cd->code_max)+1;
						$code = sprintf("%04s", $tmp);
					endforeach;					
					 if ($code== '0000') :						
						 $code = "0001";					 
					 endif;
					 
				 else:					  
					$code = "0001";								
				 endif;		
		 else:		
		       $code = "0001";					 
		 endif;
		 
	 //update tanggal ketable date--------------
	 $data = array('date_add'=>$strdates );																			
	 $this->db->update('tbl_date_compare',$data); 		 
	 //end--------------------
		 
	 //update tanggal ketable counter--------------
	  $data = array('counter'=> $code );	
	  $this->db->where('branch_id',$stridbranch);															  									
	  $this->db->update('tbl_counter_sell_unit_mas',$data); 		 
	 //end--------------------
		 				
	  return true ;
		
	}
						
	function get_customer()
	{		
	     $this->load->database();		
		 $this->db->select('name_loc');	
		 $this->db->where('status','1');			
		 $result = $this->db->get('tbl_location_stock_mas');			     		 
		 return $result->result();	 	
	}	
	
	function get_sales()
	{			      
	     $ses_idbranch = $this->session->userdata('branch_id');
	     $this->load->database();		
		 $this->db->select('*');			 
		 $this->db->where('status','1');
		 $this->db->where('branch_id', $ses_idbranch);					
		 $this->db->order_by('sal_name','asc');
		 $result = $this->db->get('tbl_sales_mas');			     		 
		 return $result->result();	 	
	}	
	
	function get_tampil_payment()
	{	 		   
	   $this->load->database() ;
	   $this->db->select('*');	
	   $result = $this->db->get('tbl_payment_type');	
	   return $result->result();   	   		   	   	   			  
	}	
		
	 
	function insert_address_foto_transfer()
	{   
	    $str_notrans =$this->session->userdata("ses_notrans") ;	 		
		$str_branch= $this->session->userdata("branch_short");
		
		
		$stryear =  substr(date('Y'),-3);
		$str_date = $stryear."-".date("m")."-".date("d");
	    $str_count= substr($str_notrans,-3);
		
		$str_notrans =$this->session->userdata("ses_notrans") ;	    		
		$str_txtiddet= $this->input->post("txtiddet");
		$count_txtiddet = count($str_txtiddet);
		
		
		$str_path='asset/upload_foto/'.$str_branch; // alamat upload quatation nya. 
									     		 		
		//alamat upload quatation beserta filenya.		    		
		$str_address=$str_path."/"."INV"."-".$str_branch."-".$str_date."-".$str_count.$count_txtiddet.".pdf";			  				   			  			   			
		$data = array('attach_trans'=>$str_address);																						
		$this->db->where('id_trans_sales',$str_notrans);
		$this->db->update('tbl_sales_unit_detail_mas',$data); 
		
		$this->session->unset_userdata('ses_notrans');
		
		return true;					  	
	}
	
	function check_addnewduplicate_spk()
	{
	       $txtspk	= $this->input->post('txtspk');	
		   $txtbrpk	= $this->input->post('txtbrpk');	
		   $txtspk_fusion = $txtbrpk.'-'.$txtspk ;	
		   		   	   	   	
		   $this->load->database() ;
		   $this->db->select('nospk,status');
		   $this->db->where('nospk',$txtspk_fusion);
		   $this->db->where('status','1');
		   
		   $result = $this->db->get('tbl_sales_unit_transaksi_mas');
		    
		   if ($result->num_rows() > 0) :
			  return true;
	       endif ;  				    					  
	   	 	   	   			  
	}		
	
	function check_duplicate_spk()
	{
	   $txtspk	= $this->input->post('txtspk');	
	   $txtnospkupdate	= $this->input->post('txtnospkupdate');	
	   
	   if ($txtspk != $txtnospkupdate) :
		   $this->load->database() ;
		   $this->db->select('nospk,status');
		   $this->db->where('nospk',$txtnospkupdate);
		   $this->db->where('status','1');
		   
		   $result = $this->db->get('tbl_sales_unit_transaksi_mas');
		    
			 if ($result->num_rows() > 0) :
	   	   		 return true;
	   		endif ;  				    				
	   endif;
	   	 	   	   			  
	}	
	
	function check_ready_vin()
	{
	   $txtvin	= $this->input->post('txtvin');			   
	   $this->load->database() ;
	   $this->db->select('vin,status,');
	   $this->db->where('status','1');
	   $this->db->where('status_flag_stock','1');
	   $result = $this->db->get('tbl_stock_master_mas');
	   
	   if ($result->num_rows() == 0 ) :
		   return true;
	   endif ;		   		   	   	   			  
	}	
	
	function check_ready_customer()
	{
	   $txtidcust	= $this->input->post('txtidcust');			   
	   $this->load->database() ;
	   $this->db->select('id_cust');
	   $this->db->where('id_cust',$txtidcust);
	   $result = $this->db->get('tbl_customer_mas');
	   
	   if ($result->num_rows() == 0 ) :
		   return true;
	   endif ;		   		   	   	   			  
	}	
	
	function check_ready_sales()
	{
	   $cbosales	= $this->input->post('cbosales');			   
	   $this->load->database() ;
	   $this->db->select('id_sales');
	   $this->db->where('id_sales',$cbosales);
	   $result = $this->db->get('tbl_sales');
	   
	   if ($result->num_rows() == 0 ) :
		   return true;
	   endif ;		   		   	   	   			  
	}	
		
			
	function check_edit_duplicate_vin()
	{
	   $txtvin			= $this->input->post('txtvin');	
	   $txtvinupdate	= $this->input->post('txtvinupdate');	
	   
	     if ($txtvin != $txtvinupdate) :
			   $this->load->database() ;
			   $this->db->select('vin');
			   $this->db->where('vin',$txtvinupdate);
			   $result = $this->db->get('tbl_stock_master_mas');
			   
			   if ($result->num_rows() > 0 ) :
				   return true;
			   endif ;		   
		 endif;    	   	   
			  
	}	
	
	
	
	
	function insert_master_sell()	
	{ 
	   
	    //-------------------Checck Duplicate NOPP / Idmaster		 
		 $cektxtnotrans= $this->input->post('txtnotrans'); 		 		 
		 $query2=   $this->db->select('*')
						 ->where('id_trans_sales',$cektxtnotrans)	 //Checck Duplicate NOpo / Idmaster					
						 ->get('tbl_sales_unit_transaksi_mas');		
		 if ($query2->num_rows() == 1) // Jika ketemu buat NOPP baru.
		 {			
				 	
   		        $stridbranch = $this->session->userdata('branch_id');			 			  	
				$strbranchshort = $this->session->userdata('branch_short');	
				$stryear = substr(date("Y"),-3,3);
				$strmonth = date("m");			    
				$strdates = date("d");	//date (tanggal buat comparasi pada table)
							
				 
				$qcounter = $this->db->query("select RIGHT(counter,4) as code_max from tbl_counter_sell_unit_mas where branch_id='".$stridbranch."'");
												   
				$qdate = $this->db->query("select * from tbl_date_compare where date_add ='".$strdates."'"); 	//check tanggal
			   
				$code = ""; //empty code
				 if ($qdate->num_rows()== 1) :			
						if($qcounter->num_rows()== 1):
							foreach($qcounter->result() as $cd):
								$tmp = ((int)$cd->code_max)+1;
								$code = sprintf("%04s", $tmp);;
							endforeach;							
							 if ($code== '0000') :						
								 $code = "0001";					 
							 endif;
							 
						 else:					  
							$code = "0001";								
						 endif;		
				 else:		
					   $code = "0001";					 
				 endif;
				 
			 //update tanggal ketable date--------------
			 $data = array('date_add'=>$strdates );																			
			 $this->db->update('tbl_date_compare',$data); 		 
			 //end--------------------
				 
			 //update tanggal ketable counter--------------
			  $data = array('counter'=> $code );	
			  $this->db->where('branch_id',$stridbranch);															  									
			  $this->db->update('tbl_counter_sell_unit_mas',$data); 		 
			 //end--------------------
				 
			 //buat counter			
			  $txtnotrans = "INV"."/".$strbranchshort."/".$stryear."/".$strmonth."/".$strdates."/".$code ;			        			
			  //end------------------------------------------			 			
		 }else{			
		      //Ambil notrans yang ada di inputbox posting	 jika tidak duplicate			 
			 $txtnotrans = $this->input->post('txtnotrans'); 	   	  
	     }
		      //----mengambil data dari inputbox/textbox lalu di simpan didalam variable
			  
			  // $txtnotrans = $this->input->post('txtnotrans');			  
			  $txtnostock =$this->input->post('txtnostock');			 
			  $txtspk_fusion = $this->input->post('txtbrpk').'-'.$this->input->post('txtspk');	
			  $txtusersell = $this->input->post('txtusersell');  			 
			  $txtdatealoc = date('Y-m-d',strtotime($this->input->post('txtdatealoc')));				  
			  if($this->input->post('txtwrsdate')!= "" ):
			      $txtwrsdate = date('Y-m-d',strtotime($this->input->post('txtwrsdate')));	
			  else :
			 	  $txtwrsdate = null;
			  endif	;
			  
			  $txtidcust	= $this->input->post('txtidcust');		
			  $cbosales	= $this->input->post('cbosales');				  
			  $curr	= $this->input->post('curr');
			  $txtlamaspk	= $this->input->post('txtlamaspk');
			  $txtnote	= xss_filter($this->input->post('txtnote'));
			  $cbopaystatus = $this->input->post('cbopaystatus');
			  $str_flag_sell = "2";
			  
			  //---------------------unformat price---------
			   $price_offroad	= $this->input->post('price_offroad');	 
			   if ( strstr( $price_offroad, ',' ) ) $price_offroad = str_replace( ',','', $price_offroad ); 
			   
			   $discount	= $this->input->post('txtdixc');	 
			   if ( strstr( $discount, ',' ) ) $discount = str_replace( ',','', $discount );   
			   
			   $txtpph	= $this->input->post('txtpph');	 
			   if ( strstr( $txtpph, ',' ) ) $txtpph = str_replace( ',','', $txtpph );  
			   
			   $bbn	= $this->input->post('txtbbn');	 
			   if ( strstr( $bbn, ',' ) ) $bbn = str_replace( ',','', $bbn );  
			   
			   $onroad_txt = $this->input->post('txtonroad');	 
			   if ( strstr( $onroad_txt, ',' ) ) $onroad_txt = str_replace( ',','', $onroad_txt ); 
			   
			   $txtdpp = $this->input->post('txtdpp');	 
			   if ( strstr( $txtdpp, ',' ) ) $txtdpp = str_replace( ',','', $txtdpp ); 
			  
			 
			   $poles	= $this->input->post('txtpoles');	 
			   if ( strstr( $poles, ',' ) ) $poles = str_replace( ',','', $poles ); 
			   
			    $pdi	= $this->input->post('txtpdi');	 
			   if ( strstr( $pdi, ',' ) ) $pdi = str_replace( ',','', $pdi ); 
			   
			    $surjal	= $this->input->post('txtsj');	 
			   if ( strstr( $surjal, ',' ) ) $surjal = str_replace( ',','', $surjal ); 
			   
			    $kacafilm	= $this->input->post('txtkafilm');	 
			   if ( strstr( $kacafilm, ',' ) ) $kacafilm = str_replace( ',','', $kacafilm ); 
			  
			    $other	= $this->input->post('txtother');	 
			   if ( strstr( $other, ',' ) ) $other = str_replace( ',','', $other ); 			  			
			  
			   $txtinsetif	= $this->input->post('txtinsetif');	 
			   if ( strstr( $txtinsetif, ',' ) ) $txtinsetif = str_replace( ',','', $txtinsetif);
			   
			   $suport	= $this->input->post('txtsuport');	 
			   if ( strstr( $suport, ',' ) ) $suport = str_replace( ',','', $suport ); 
			   
			   $txtgp	= $this->input->post('txtgp');	 
			   if ( strstr( $txtgp, ',' ) ) $txtgp = str_replace( ',','', $txtgp );//grossprofit
			   
			    $remaining_pay	= $this->input->post('txt_remain');	 
			   if ( strstr( $remaining_pay, ',' ) ) $remaining_pay = str_replace( ',','', $remaining_pay );			  		  			 			   			 			  			   			  			  
			   $payment_cust = $this->input->post('txtdppay'); //buat master dan detail payment customer
			   if ( strstr( $payment_cust, ',' ) ) $payment_cust = str_replace( ',','', $payment_cust );   
			  //-------------------------------------
			 
			  //jumlah tanggal ke depan untuk due date SPK			  			   
			   $datetrans = date('Y-m-d');
			   $fusion_day = " "."+".$txtlamaspk." "."day" ;
			   $duedate_spk = strtotime($fusion_day,strtotime($datetrans));			  
			   $duedate_spk = date('Y-m-d', $duedate_spk);	
			   $hasil_duedate_spk = date('Y-m-d',strtotime($duedate_spk));			   
			  //end--------------------------------------------- 
			 			
			  //reminder date spk-------------------  
			   $datetrans = $this->input->post("txtdatealoc");
			   $strreminderspk = '4';			  
			   $fusion_day_remind = " "."+".$strreminderspk." "."day" ;
			   $remind_date_spk = strtotime($fusion_day_remind,strtotime($datetrans));			
			   $remind_date_spk = date('Y-m-d', $remind_date_spk);	
			   $hasil_reminder_spk = date('Y-m-d',strtotime($remind_date_spk));	 ;
			 //end--------------------------------------------- 					 				  			  			  			  
			  			 			  												 
			//---------------------------------------------- update insert tbl_master_pp		      			
			$data=array(
						'id_trans_sales'=>$txtnotrans,
						'stock_no' =>$txtnostock,						
						'nospk'=>$txtspk_fusion,		
						'user_sell'=> $this->session->userdata('name'),																	
						'date_aloc'=>$txtdatealoc, 	
						'lama_spk'=>$txtlamaspk,						
						'id_cust'=>$txtidcust,																		
						'id_sales'=>$cbosales, 
						'ws_date'=>$txtwrsdate,
						'remarks'=>$txtnote,	
						'user_sell'=>$txtusersell,
						'status_flag_sell'=> $str_flag_sell,
						'flag_send_approval'=>'0',
						'aprove_bm'=> '0',						
						'approve_fin'=> '0',	
						'price_curent'=> $price_offroad,					
						'opt_kacfilm'=> $kacafilm ,
						'opt_pdi'=>	  $pdi,
						'opt_poles'=>  $poles,
						'opt_surtjalan'=>  $surjal ,
						'opt_other'=>  $other,	
						'insetivfe_sales'=> $txtinsetif,	
						'bbn' => $bbn,
						'price_onroad' =>$onroad_txt,						
						'discount'=> $discount,	
						'suport_disc'=>$suport ,			
						'remaining_amount_customer' => $remaining_pay,	
						'gross_profit' => $txtgp,	
					    'total_paymet_customer'=>$payment_cust,																												
						'curr_sell' =>$curr,	
						'email_user_sell'=> $this->session->userdata('email'),		
						'date_transaksi' => date('Y-m-d'),
						'date_reminder_spk' => $hasil_reminder_spk ,	
						'date_dudate_spk' => $hasil_duedate_spk,	
						'flag_payment_status' =>$cbopaystatus, //falg leasing atau customerpay
						'flag_notif_email' =>'0', //flag leasing atau customerpay
						'flag_already_bstk' =>'0', 
						'flag_print_bstk' =>'0', 
						'status' => '1',																									
						);																		 															
			  $this->db->insert('tbl_sales_unit_transaksi_mas',$data);	
			
			  $data_ses = array('ses_notrans' => $txtnotrans);
			  $this->session->set_userdata($data_ses);
			
		 	  //Detail--------------
			  $txtvinno =$this->input->post('txtvinno');			 
			  $txtengine = $this->input->post('txtengine');	
			
			  $txtdescpay =$this->input->post('txtdescpay');			 
			  $txtdatedet = date('Y-m-d',strtotime($this->input->post('txtdatedet')));						 										 			  
			  //---------------------unformat price---------
			  /* $txtdppay = $this->input->post('txtdppay');
			   if ( strstr( $txtdppay, ',' ) ) $txtdppay = str_replace( ',','', $txtdppay );  */
			   
			   $str_equel_amount = $this->input->post('txt_remain');
			    if (strstr( $str_equel_amount, ',' ) ) $str_equel_amount = str_replace( ',','', $str_equel_amount );  
			  //end----------------------
			  
			  //uploafoto
			   $str_notrans =$this->session->userdata("ses_notrans") ;	 		
			   $str_branch= $this->session->userdata("branch_short");
						
			   $stryear =  substr(date('Y'),-3);
			   $str_date = $stryear."-".date("m")."-".date("d");
			   $str_count= substr($txtnotrans,-3);		
			   $str_path='asset/upload_foto/'.$str_branch; // alamat upload quatation nya. 
																
			   //alamat upload quatation beserta filenya.	
			    $idfoto_pertama	= "1";    					
				$str_adress_apload = $str_path."/"."INV"."-".$str_branch."-".$str_date."-".$str_count.$idfoto_pertama.".pdf";	
				$str_name_foto ="INV"."-".$str_branch."-".$str_date."-".$str_count.$idfoto_pertama;	
			   //end------------------------
			$data_detail=array(
								'id_trans_sales' => $txtnotrans,
								'desc_det' =>$txtdescpay ,
								'pay_date' => $txtdatedet,												
								'amount' => $payment_cust,
								'equel_amount' =>$str_equel_amount,
								'attach_trans'=>$str_adress_apload,
								'status' => "1",								
								'temp_vin' =>$txtvinno,
								'temp_engine' =>$txtengine,
						      );
			$this->db->insert('tbl_sales_unit_detail_mas',$data_detail);	
			
			//address uploaad foto--------------
			 $this->session->unset_userdata('ses_notrans'); 
			 $this->session->unset_userdata('ses_name_apload'); 
			 
			 $buff_save_master=array('ses_notrans'=> $txtnotrans,
								     'ses_name_apload' => $str_name_foto
								     );					 								 			
			 $this->session->set_userdata($buff_save_master);		
			//end-------------------
			
			//update----flag stock unit menjadi dp-----
			  $data = array('status_flag_stock'=>'2'); //flag menjadi 2 karena dp / payment but no waiting approve																						
		      $this->db->where('stock_no',$txtnostock);
		      $this->db->update('tbl_stock_master_mas',$data); 				
			//end---------------------------------------
			
			return true;											     							 
	}
	
	
	function edit_master_sell()	
	{
			  $txtnospkupdate	= $this->input->post('txtnospkupdate');
			  $txtnotrans = $this->input->post('txtnotrans');			  
			  $txtnostock =$this->input->post('txtnostock');			 
			  $txtspk = $this->input->post('txtspk');	
			  $txtusersell = $this->input->post('txtusersell');  			 
			  $txtdatealoc = date('Y-m-d',strtotime($this->input->post('txtdatealoc')));				  			  
			  $txtidcust	= $this->input->post('txtidcust');		
			  $cbosales	= $this->input->post('cbosales');				  
			  $curr	= $this->input->post('curr');
			  $txtlamaspk	= $this->input->post('txtlamaspk');
			  $txtnote	= xss_filter($this->input->post('txtnote'));
			  $str_flag_sell = "2";
			  $cbopaystatus = $this->input->post('cbopaystatus');
			  
			 //---------------------unformat price---------
			   $price_offroad	= $this->input->post('price_offroad');	 
			   if ( strstr( $price_offroad, ',' ) ) $price_offroad = str_replace( ',','', $price_offroad ); 
			   
			   $discount	= $this->input->post('txtdixc');	 
			   if ( strstr( $discount, ',' ) ) $discount = str_replace( ',','', $discount );   
			   
			   $txtpph	= $this->input->post('txtpph');	 
			   if ( strstr( $txtpph, ',' ) ) $txtpph = str_replace( ',','', $txtpph );  
			   
			   $bbn	= $this->input->post('txtbbn');	 
			   if ( strstr( $bbn, ',' ) ) $bbn = str_replace( ',','', $bbn );  
			   
			   $onroad_txt = $this->input->post('txtonroad');	 
			   if ( strstr( $onroad_txt, ',' ) ) $onroad_txt = str_replace( ',','', $onroad_txt ); 
			   
			   $txtdpp = $this->input->post('txtdpp');	 
			   if ( strstr( $txtdpp, ',' ) ) $txtdpp = str_replace( ',','', $txtdpp ); 
			  
			 
			   $poles	= $this->input->post('txtpoles');	 
			   if ( strstr( $poles, ',' ) ) $poles = str_replace( ',','', $poles ); 
			   
			    $pdi	= $this->input->post('txtpdi');	 
			   if ( strstr( $pdi, ',' ) ) $pdi = str_replace( ',','', $pdi ); 
			   
			    $surjal	= $this->input->post('txtsj');	 
			   if ( strstr( $surjal, ',' ) ) $surjal = str_replace( ',','', $surjal ); 
			   
			    $kacafilm	= $this->input->post('txtkafilm');	 
			   if ( strstr( $kacafilm, ',' ) ) $kacafilm = str_replace( ',','', $kacafilm ); 
			  
			    $other	= $this->input->post('txtother');	 
			   if ( strstr( $other, ',' ) ) $other = str_replace( ',','', $other ); 			  			
			  
			   $txtinsetif	= $this->input->post('txtinsetif');	 
			   if ( strstr( $txtinsetif, ',' ) ) $txtinsetif = str_replace( ',','', $txtinsetif);
			   
			   $suport	= $this->input->post('txtsuport');	 
			   if ( strstr( $suport, ',' ) ) $suport = str_replace( ',','', $suport ); 
			   
			   $txtgp	= $this->input->post('txtgp');	 
			   if ( strstr( $txtgp, ',' ) ) $txtgp = str_replace( ',','', $txtgp );//grossprofit
			   
			    $remaining_pay	= $this->input->post('txt_remain');	 
			   if ( strstr( $remaining_pay, ',' ) ) $remaining_pay = str_replace( ',','', $remaining_pay );			  		  			 			   			 			  			   			  			  
			   $payment_cust = $this->input->post('txtdppay'); //buat master dan detail payment customer
			   if ( strstr( $payment_cust, ',' ) ) $payment_cust = str_replace( ',','', $payment_cust );   
			  //-------------------------------------
			 
			 //jumlah tanggal ke depan untuk due date SPK
			   $datetrans = $this->input->post("txtdatetrans");
			   $fusion_day = " "."+".$txtlamaspk." "."day" ;
			   $duedate_spk = strtotime($fusion_day,strtotime($datetrans));			  
			   $duedate_spk = date('Y-m-d', $duedate_spk);	
			   $hasil_duedate_spk = date('Y-m-d',strtotime($duedate_spk));	
			   
			 //reminder date spk-------------------  
			   $datetrans = $this->input->post("txtdatetrans");
			   $strreminderspk = '4';			  
			   $fusion_day_remind = " "."+".$strreminderspk." "."day" ;
			   $remind_date_spk = strtotime($fusion_day_remind,strtotime($datetrans));			
			   $remind_date_spk = date('Y-m-d', $remind_date_spk);	
			   $hasil_reminder_spk = date('Y-m-d',strtotime($remind_date_spk));	 ;
			 //end---------------------------------------------
			  
			 //----wrs daet jika kosong--------------------------------			 	
			  if($this->input->post('txtwrsdate')!= "" ):
			      $txtwrsdate = date('Y-m-d',strtotime($this->input->post('txtwrsdate')));	
			  else :
			 	  $txtwrsdate = null;
			  endif	;	 
			 //---------------------------------------------- update insert tbl_master_sell		      			
			 $data=array(						
						'stock_no' =>$txtnostock,
						'nospk'=>$txtnospkupdate,	
						'user_sell'=> $this->session->userdata('name'),		
						'email_user_sell'=>	$this->session->userdata('email'),		
						'price_curent'=> $remaining_pay,														
						//'date_aloc'=>$txtdatealoc, 	
						'lama_spk'=>$txtlamaspk,						
						'id_cust'=>$txtidcust,																		
						'id_sales'=>$cbosales, 
						'ws_date'=>$txtwrsdate,
						'remarks'=>$txtnote,	
						'user_sell'=>$txtusersell,
						'status_flag_sell'=> $str_flag_sell,
						'flag_send_approval'=>'0',
						'aprove_bm'=> '0',						
						'approve_fin'=> '0',	
						'price_curent'=> $price_offroad,					
						'opt_kacfilm'=> $kacafilm ,
						'opt_pdi'=>	  $pdi,
						'opt_poles'=>  $poles,
						'opt_surtjalan'=>  $surjal ,
						'opt_other'=>  $other,	
						'insetivfe_sales'=> $txtinsetif,	
						'bbn' => $bbn,
						'price_onroad' =>$onroad_txt,						
						'discount'=> $discount,	
						'suport_disc'=>$suport ,			
						'remaining_amount_customer' => $remaining_pay,	
						'gross_profit' => $txtgp,	
					    'total_paymet_customer'=>$payment_cust,																												
						'curr_sell' =>$curr,	
					//	'date_reminder_spk' => $hasil_reminder_spk ,	
						//'date_dudate_spk' => $hasil_duedate_spk,
						'flag_payment_status' => $cbopaystatus, //flag leasing atau customer pay
					//	'date_transaksi' => date('Y-m-d'),																														
						);																		 															
			  $this->db->where('id_trans_sales',$txtnotrans);  			
			  $this->db->update('tbl_sales_unit_transaksi_mas',$data);	
			  
			
			  $data_ses = array('ses_notrans' => $txtnotrans);
			  $this->session->set_userdata($data_ses);
			  			  			
		 	  //Detail--------------
			  $txtvinno =$this->input->post('txtvinno');			 
			  $txtengine = $this->input->post('txtengine');	
			  $txtaddrsfoto =  $this->input->post('txtaddresedit');	
			
			  $txtdescpay =$this->input->post('txtdescpay');			 
			  $txtdatedet = date('Y-m-d',strtotime($this->input->post('txtdatedet')));						 										 			  
			  //---------------------unformat price---------			  			   
			   $str_equel_amount = $this->input->post('txt_remain');
			    if (strstr( $str_equel_amount, ',' ) ) $str_equel_amount = str_replace( ',','', $str_equel_amount );  
			  //end----------------------
			
			/*$data_detail=array(								
								'desc_det' =>$txtdescpay ,
								'pay_date' => $txtdatedet,												
								'amount' => $payment_cust,
								'equel_amount' => $str_equel_amount,
								//'status' => "1",
								'attach_trans'=>$txtaddrsfoto,								
								'temp_vin' =>$txtvinno,
								'temp_engine' =>$txtengine,
						      );
			  $this->db->where('id_trans_sales',$txtnotrans);  	
			  $this->db->where('status',"1");  			
			  $this->db->update('tbl_sales_unit_detail_mas',$data_detail);	
			//end-------------------		 */
			 
			  $this->session->unset_userdata('ses_name_apload'); 
			 
			  $dataidtrans = array('ses_notrans_msg' => $txtnotrans,
			  					  // 'ses_name_apload' => $txtaddrsfoto
			  					   );
			  $this->session->set_userdata($dataidtrans);
																																
			return true;										  
	}
	
	function return_dp_jadi_ready()
	{
		$nostock_edit = $this->input->post('txtnostock');
		$returnnostoc = $this->input->post('txtnostockbuff');
		
		if ($nostock_edit != $returnnostoc) :
		    //novin stock yg di jadikan stock redy kembali
		    $array_return_nostock = array('status_flag_stock' => "1");
			$this->db->where('stock_no',$returnnostoc);
			$this->db->update('tbl_stock_master_mas',$array_return_nostock);
			
			//novin stock yg di jadikan dp
			$array_return_nostock = array('status_flag_stock' => "2");
			$this->db->where('stock_no',$nostock_edit);
			$this->db->update('tbl_stock_master_mas',$array_return_nostock);
		endif;
								
	}
	
	function getedit_sell()
	{
	
	      $get_edit_sell =$this->input->post('msg');		   		      		  
		  $flag_status_del = "1" ;
	  
	 
	  if (isset($get_edit_sell) && trim($get_edit_sell!=''))
	   {		
	           for ($i=0; $i < count($get_edit_sell) ; $i++) { 		
					 $this->db->select("id_trans_sales,stock_no,nospk,branch_id,name_branch,desc_model,desc_type,colour,vin,engine,location_stock,price_curent,bbn,price_onroad,price_offroad,opt_kacfilm,opt_pdi,opt_poles,opt_surtjalan,opt_other,insetivfe_sales,pph22_sell,dpp,suport_disc,date_aloc,lama_spk,id_cust,cust_name,id_sales,sal_name,ws_date,remarks,user_sell,status_flag_sell,remaining_amount_customer,price_curent,discount,total_paymet_customer,curr_sell,gross_profit,date_transaksi,flag_payment_status,desc_pay")	;
					 $this->db->where('id_trans_sales',$get_edit_sell[$i]);
					 $this->db->where('status',$flag_status_del);											  						
					 $result_master = $this->db->get('qv_complite_master_sell_mas');						  					 					
			   }									 
		     
			  return $result_master->result() ;							  		  
		}else{
			  return false;
	    }   
	}
	
	function getedit_detail_sell()
	{
	
	      $get_edit_sell =$this->input->post('msg');		   		      		  
		  $flag_status_del = "1" ;
	  
	 
	  if (isset($get_edit_sell) && trim($get_edit_sell!=''))
	   {		
	           for ($i=0; $i < count($get_edit_sell) ; $i++) { 		
					 $this->db->select("id_detail,desc_det,pay_date,attach_trans,amount,equel_amount,status")	;
					 $this->db->where('id_trans_sales',$get_edit_sell[$i]);
					 $this->db->where('status',$flag_status_del);											  						
					 $result_master = $this->db->get('tbl_sales_unit_detail_mas');						  					 					
			   }									 
		     
			  return $result_master->result() ;							  		  
		}else{
			  return false;
	    }   
	}
									
	
	function delete_with_edit_flag() {				
		  $delete = $this->input->post('msg');		  				  		 
		  $flag_del = '0';
		  
		  if(isset($delete) && trim($delete!=''))
		  {	   
			  // --------delete table master dan detail pp menggunakan flag status			    
				for ($i=0; $i < count($delete) ; $i++) { 				    
				    //array variable flag del					
				    $data = array('status'=>$flag_del);											
					
					//master
					$this->db->where('id_trans_sales',$delete[$i]);
					$this->db->update('tbl_sales_unit_transaksi_mas',$data); 						   				   	   				
					
					//detail
					$this->db->where('id_trans_sales',$delete[$i]);
					$this->db->update('tbl_sales_unit_detail_mas',$data); 						   				   	   				
			    }			   					  	   
			   return true;
		  }else{
			   return false;	  			  		  
		  }
	}
			
	public function get_search_date()
	{
	    $strdatestart =$this->input->post('txtdatestart');	
	    $strdatend =$this->input->post('txtdateend');	
	    $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	    $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		$status_fpb_flag = "1";  //flag sudah di print FPB
	    $ses_id_company = $this->session->userdata('id_company');		
		 
	    if($strdatestart !="" and $strdatend !="" ){	
		   $this->load->database();		
	       $this->db->select('id_master,no_voucher,id_company,id_dept,company,dept,paye_from,header_desc,date_create,date_reciep,user_reciep,status,currency,gran_total,short');
		   $this->db->where('id_company',$ses_id_company);		  
		   $this->db->where('date_reciep BETWEEN "'.$datestart.'" and "'.$dateend.'"');
		   $this->db->order_by('date_reciep','desc');
	 	   $result = $this->db->get('qv_complite_master_bmk');		
	       return $result;		 			  		 	   	 
	    }else{
		    redirect('c_bmk/c_create_bmk');	
		}
		  
	}	
	
	function insert_edit_detail()
	{				
		 //dari master-------
		  $txtidtrans= $this->input->post('txtidtrans');
		  $txtvin= $this->input->post('txtvin');
		  $txtengin= $this->input->post('txtengin');
		 //end-------------------------------------
		 
		 //dari detail-------
		 $txtiddet = $this->input->post('txtiddet');
		 $txtdescpay =$this->input->post('txdescdet');			 		
		 $txtdatedet = $this->input->post('txtdatedet');			
		 		  
		 //-------	  
			  //---------------------unformat price---------			  			   
			   $str_equel_amount = $this->input->post('txtunitprice');
			    if (strstr( $str_equel_amount, ',' ) ) $str_equel_amount = str_replace( ',','', $str_equel_amount );  
			  //end----------------------
		
		//uploafoto
		$str_notrans =$this->session->userdata("ses_notrans") ;	 		
		$str_branch= $this->session->userdata("branch_short");
				
		$stryear =  substr(date('Y'),-3);
		$str_date = $stryear."-".date("m")."-".date("d");
	    $str_count= substr($txtidtrans,-3);		
	    $str_path='asset/upload_foto/'.$str_branch; // alamat upload quatation nya. 
									     		 		
		//alamat upload quatation beserta filenya.		    				
		$str_address = $str_path."/"."INV"."-".$str_branch."-".$str_date."-".$str_count;		
		$str_name_foto ="INV"."-".$str_branch."-".$str_date."-".$str_count;
		//------------------------
		if(count($txtiddet)==0) :
		   return false;	 
		else:
		   for ($intdetail=0;$intdetail < count($txtiddet);$intdetail++) : 	
		        $payment_cust = $this->input->post('txtpaydet');					
			if (strstr( $payment_cust[$intdetail], ',' ) ) $payment_cust[$intdetail] = str_replace( ',','', $payment_cust[$intdetail] );   			 	   			    
			 if ($txtiddet[$intdetail] == "") :  					  				   															
				//if ($txtdescpay[$intdetail] !="" && $payment_cust[$intdetail] !="" && $payment_cust[$intdetail] !="0.00" &&  $txtdatedet[$intdetail] !="" ):			
				if ($payment_cust[$intdetail] !=""):			
						
						//buat anama alamat pdf
						if (empty($_FILES['userfile']['name'])) :						
							$str_adress_apload ="";
							$str_name_aplod ="";
						else:
							$str_adress_apload =$str_address.($intdetail+1).".pdf";
							$str_name_aplod =$str_name_foto.($intdetail+1);	
						endif;	
						
						//end--------------------------------------------		
						
						//date--------------------------------------------------
						 $buff_date = date('Y-m-d',strtotime($txtdatedet[$intdetail]));			      
							   if ($txtdatedet[$intdetail] !="" ) :
									$txthasildate  = date('Y-m-d',strtotime($txtdatedet[$intdetail]));
							   else :	
									$txthasildate =null ;
							   endif;	
						 //end date	-----------------------------											
							
						$data_detail=array(
											'id_trans_sales' => $txtidtrans,
											'desc_det' =>$txtdescpay[$intdetail] ,
											'pay_date' => $txthasildate ,												
											'amount' => $payment_cust[$intdetail],
											'equel_amount' =>$str_equel_amount,
											'status' => "1",	
											'attach_trans'=>$str_adress_apload,							
											'temp_vin' =>$txtvin,
											'temp_engine' =>$txtengin,
										  );
						 $this->db->insert('tbl_sales_unit_detail_mas',$data_detail);	
						
						 $this->session->unset_userdata('ses_name_apload'); 
			             $buff_nama_foto=array('ses_name_apload' => $str_name_aplod);	
						 $this->session->set_userdata($buff_nama_foto);
						 
						 
						 
					     //update data master----------------------
						 //---------------------unformat price---------			  			   
						  $txtpcr= $this->input->post('txtpcr');
						  if (strstr( $txtpcr, ',' ) ) $txtpcr = str_replace( ',','', $txtpcr);  
						  
						  $txtdiscount= $this->input->post('txtdiscount');
						  if (strstr( $txtdiscount, ',' ) ) $txtdiscount = str_replace( ',','', $txtdiscount);  
						  
						  $txttotalpayment= $this->input->post('txttotalpayment');
						  if (strstr( $txttotalpayment, ',' ) ) $txttotalpayment = str_replace( ',','', $txttotalpayment);  
						  
																								
						 // unformat end----------------------	
							  
							 //proses update master
							 
								 $data_master=array(											
										'remaining_amount_customer' => $txtpcr,	
										'discount'=> $txtdiscount,																													
										'total_paymet_customer'=>$txttotalpayment,																																																											
										);																		 															
								  $this->db->where('id_trans_sales',$txtidtrans);  			
								  $this->db->update('tbl_sales_unit_transaksi_mas',$data_master);	
							//end--------------------------------------   
							
							
							 //---remove row detail sell from  array format wjen tombol remove click ------------		  
							   $get_iddet_remove_row = $this->input->post('txtbuffid_det') ;
							
							   if(isset($get_iddet_remove_row) && trim($get_iddet_remove_row!='')){					 			   
								   $stsrconvert_iddet = explode(",",$get_iddet_remove_row) ;				   			  		   
								   $flag_remove = "0";		   			   
								   for ($intiddet=0; $intiddet < count($stsrconvert_iddet) ; $intiddet++)
									   {				   									   				   	   
											//table detail 								
											$data = array('status'=>$flag_remove);											
											$this->db->where('id_detail', $stsrconvert_iddet[$intiddet]);
											$this->db->update('tbl_sales_unit_detail_mas',$data);						   	 				
									   }  			
							   };		  
							 //end---------------------------------------------------------------------------
					else :
						
							 //---remove row detail sell from  array format wjen tombol remove click ------------		  
							   $get_iddet_remove_row = $this->input->post('txtbuffid_det') ;
							
							   if(isset($get_iddet_remove_row) && trim($get_iddet_remove_row!='')){					 			   
								   $stsrconvert_iddet = explode(",",$get_iddet_remove_row) ;				   			  		   
								   $flag_remove = "0";		   			   
								   for ($intiddet=0; $intiddet < count($stsrconvert_iddet) ; $intiddet++)
									   {				   									   				   	   
											//table detail 								
											$data = array('status'=>$flag_remove);											
											$this->db->where('id_detail', $stsrconvert_iddet[$intiddet]);
											$this->db->update('tbl_sales_unit_detail_mas',$data);						   	 				
									   }  			
							   };		  
							 //end---------------------------------------------------------------------------						
						return false	 ;
				   endif;							  				        							 					
				else :		
				       //date------------------------	
							 $buff_date = date('Y-m-d',strtotime($txtdatedet[$intdetail]));			      
							   if ($txtdatedet[$intdetail] !="" ) :
									$txthasildate  = date('Y-m-d',strtotime($txtdatedet[$intdetail]));
							   else :	
									$txthasildate =null ;
							   endif;	
						 //end date								 					
						$data_detail=array(
											'id_trans_sales' => $txtidtrans,
											'desc_det' =>$txtdescpay[$intdetail] ,
											'pay_date' => $txthasildate,												
											'amount' => $payment_cust[$intdetail],
											'equel_amount' =>$str_equel_amount,
											'status' => "1",	
											//'attach_trans'=>$str_adress_apload,							
											'temp_vin' =>$txtvin,
											'temp_engine' =>$txtengin,
										  );						  				  
						 $this->db->where('id_detail',$txtiddet[$intdetail]);  					  
						 $this->db->update('tbl_sales_unit_detail_mas',$data_detail);	
					 
					 
						 //update data master----------------------
						 //---------------------unformat price---------			  			   
						  $txtpcr= $this->input->post('txtpcr');
						  if (strstr( $txtpcr, ',' ) ) $txtpcr = str_replace( ',','', $txtpcr);  
						  
						  $txtdiscount= $this->input->post('txtdiscount');
						  if (strstr( $txtdiscount, ',' ) ) $txtdiscount = str_replace( ',','', $txtdiscount);  
						  
						  $txttotalpayment= $this->input->post('txttotalpayment');
						  if (strstr( $txttotalpayment, ',' ) ) $txttotalpayment = str_replace( ',','', $txttotalpayment);  
						  																				
						  //end----------------------	
						  
							  //proses update master total payment, 
								 $data_master=array(											
										'remaining_amount_customer' => $txtpcr,	
										'discount'=> $txtdiscount,																													
										'total_paymet_customer'=>$txttotalpayment,																																																																										
										);																		 															
								  $this->db->where('id_trans_sales',$txtidtrans);  			
								  $this->db->update('tbl_sales_unit_transaksi_mas',$data_master);	
							 //end--------------------------------------   */	
							 							 								 	
			 endif;	
		 endfor ;			
		 
		 	  //---remove row detail sell from  array format wjen tombol remove click ------------		  
			   $get_iddet_remove_row = $this->input->post('txtbuffid_det') ;			
			   if(isset($get_iddet_remove_row) && trim($get_iddet_remove_row!='')){					 			   
				   $stsrconvert_iddet = explode(",",$get_iddet_remove_row) ;				   			  		   
				   $flag_remove = "0";		   			   
				   for ($intiddet=0; $intiddet < count($stsrconvert_iddet) ; $intiddet++)
					   {				   									   				   	   
							//table detail 								
							$data = array('status'=>$flag_remove);											
							$this->db->where('id_detail', $stsrconvert_iddet[$intiddet]);
							$this->db->update('tbl_sales_unit_detail_mas',$data);						   	 				  
					   }  			
			   }		  
			 //end---------------------------------------------------------------------------	
												 
			 //nyimpen session address  pdf. 		
					 $this->session->unset_userdata('ses_notrans'); 				
					 $buff_idtrans=array('ses_notrans'=> $txtidtrans				 				
										);					 								 			
					 $this->session->set_userdata($buff_idtrans);
			//end-----------------------					
		return true;	
	  endif;		
	}		
	
	
	function search_data() 	// fungsi cari data pp model
	{
	   $cari          =  $this->input->post('txtcari');
	   $kategory      =  $this->input->post('cbostatus');	  	   	 
	   $ses_branch_id =  $this->session->userdata('branch_id');
	   $flag_status   = "1" ;  //jika 0 terdelete
	  
	   $flag_sell = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	   $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP
	   	   	   
	   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
	   {
		   redirect('maserati_stock_sales/c_slssell_unit_mas/c_slsssell_mas');  //direct ke url add_menu_booking/add_menu_booking
	   }else{	
	     $this->load->database();			  	   		  		 
	     $this->db->where('branch_id',$ses_branch_id);
	     $this->db->where('status',$flag_status);
	     $this->db->where("(status_flag_sell='".$flag_sell."' OR status_flag_sell='".$flag_dp."' )", NULL, FALSE);
	     $this->db->like($kategory,$cari);		    		   
	     $result = $this->db->get('qv_complite_master_sell_mas');		 
	   
	     $this->session->set_flashdata('cari',$cari);	
	     return $result->result(); 
	   }
	} 
	  
 	function get_data_novin()//model auto complete	
	{		       
	  $txtvinno = $this->input->post('txtvinno',TRUE); 
	
	  $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP	 
	  $status_del = "1";
	  $branch_id = $this->session->userdata('branch_id');
	  			 
	  $this->load->database();
	  $this->db->select('branch_id,stock_no,desc_model,desc_type,vin,engine,currency,dpp,vat,pph22,ppnbm,total_dpp_vat,price_offroad,colour,interior_colour,location_stock,status_flag_stock,status,temp_date_wrs');	
	  $this->db->where("branch_id",$branch_id);	
	  $this->db->where("status_flag_stock",$flag_stock);
	  $this->db->where("(vin like '%".$txtvinno."%' OR engine like '%".$txtvinno."%' )", NULL, FALSE);
	  $this->db->where("status",$status_del);	
	  $query = $this->db->get('qv_complite_master_stockin_mas');					
	
	  return $query->result();
		//return $query;		
	}
	
	function get_data_novin_full()//model auto complete	
	{		       
	  $txtvinno = $this->input->post('txtvinno',TRUE); 
	
	  $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP	 
	  $status_del = "1";
	  $branch_id = $this->session->userdata('branch_id');
					 
	  $this->load->database();
	  $this->db->select('branch_id,stock_no,desc_model,desc_type,vin,engine,currency,colour,dpp,vat,pph22,ppnbm,total_dpp_vat,price_offroad,location_stock,status_flag_stock,status,temp_date_wrs');	
	  $this->db->where("branch_id",$branch_id);	
	  $this->db->where("status_flag_stock",$flag_stock);	
	  $this->db->where("(vin = '".$txtvinno."' OR engine = '".$txtvinno."' )", NULL, FALSE);	  
	  $this->db->where("status",$status_del);
	  $query = $this->db->get('qv_complite_master_stockin_mas');					
	
	  return $query->result();
		//return $query;		
	}
    					 	 	 	    

  
//------------------------------------------------------------------------------------------------------------------		
	function get_data_customer()//model auto complete	
	{		       
	  $txtcust = $this->input->post('txtcust',TRUE); 
	  $branch_id = $this->session->userdata('branch_id');
	  $status_del = "1";
				 
	  $this->load->database();
	  $this->db->select('id_cust,cust_name,branch_id,status');		 
	  $this->db->where("status",$status_del);	
	  $this->db->where("branch_id",$branch_id);	
	  $this->db->like("cust_name",$txtcust);	  
	  $query = $this->db->get('tbl_customer_mas');					
	
	  return $query->result();			
	}	   
	
	function get_data_customer_full()//model auto complete	
	{		       
	  $txtcust = $this->input->post('txtcust',TRUE); 
	  $branch_id = $this->session->userdata('branch_id');
	  $status_del = "1";
					 
	  $this->load->database();
	  $this->db->select('id_cust,cust_name,branch_id,status');
	  $this->db->where("status",$status_del);	
	  $this->db->where("branch_id",$branch_id);	
	  $this->db->where('cust_name',$txtcust);	  
	  $query = $this->db->get('tbl_customer_mas');		 			
	
	  return $query->result();			
	}
	
	//--------flag send approval ------------------------------------------------------
	function update_send_flag_aproval()
	{
		  $strid_master =$this->input->post('msg'); 			  		 
		  $flag_aproval = "1" ;	// flag 1 jika status nya sudah terkirim sebagai aproval		  					    							   				   	   					  				   			  			   	
		  if (isset($flag_aproval) && trim($flag_aproval!='')):
		  	  for ($i=0; $i < count($strid_master) ; $i++) : 							
					$query_cash = $this->db->select("id_trans_sales,remaining_amount_customer") 
										->where('id_trans_sales',$strid_master[$i])
										->where('remaining_amount_customer',"0.00")
										->where('flag_payment_status',"1")
										->get('tbl_sales_unit_transaksi_mas');													
					  if ($query_cash->num_rows() == 1) :			
						
							//session for email------------------------------
							  $buff=array('ses_notrans'=> $strid_master[$i],
										  );			 			 												
							  $this->session->set_userdata($buff);							
							//end session for email--------------------------
																
							  $data = array(
											'flag_send_approval'=>$flag_aproval								
										   );					
							  $this->db->where('id_trans_sales',$strid_master[$i]);
							  $this->db->update('tbl_sales_unit_transaksi_mas',$data); 															
							return true;
					  else :
					  		
							$query_none_cash = $this->db->select("id_trans_sales,remaining_amount_customer") 
										->where('id_trans_sales',$strid_master[$i])									
										->where('flag_payment_status !=',"1")
										->get('tbl_sales_unit_transaksi_mas');													
							  if ($query_none_cash->num_rows() == 1) :			
									
									//session for email------------------------------
									  $buff=array('ses_notrans'=> $strid_master[$i],
												  );			 			 												
									  $this->session->set_userdata($buff);							
									//end session for email--------------------------
																		
									$data = array(
													'flag_send_approval'=>$flag_aproval								
												 );					
									$this->db->where('id_trans_sales',$strid_master[$i]);
									$this->db->update('tbl_sales_unit_transaksi_mas',$data); 															
									return true;
					  		else:	
								    return false;			  	
							endif ;	
					  endif;
			  endfor;			  
		  else:
			  return false;
		  endif;
	}
	
	function upddate_date_send()
	{
		$get_idtrans = $this->input->post('msg');
		$date_now = date("Y-m-d");
		
		for ($intloop=0;$intloop < count($get_idtrans) ;$intloop++) : 		
			if (isset($get_idtrans[$intloop]) && trim($get_idtrans[$intloop]!='')) :
				$data_date = array("date_send_aproval"=>$date_now);
				$this->db->where('id_trans_sales',$get_idtrans[$intloop]);
				$this->db->update('tbl_sales_unit_transaksi_mas',$data_date);			
				return true ;
			else:
				return false ;
			endif;		
	   endfor;							
	}

	public function getAllDataCustomer() {
		$ses_branch_id = $this->session->userdata('branch_id'); //manggili session id 			
		$status_delete = "1"; //jika 0 terdelete jika 1 aktif		  		 		 		
		 
		$this->load->database();		
		$this->db->select('*');	
		$this->db->where('branch_id',$ses_branch_id);								 
		$this->db->where('status',$status_delete);				
		$this->db->order_by('id_cust','desc');
		$result = $this->db->get('qv_complite_customer_mas');			     		 
		return $result->result();
	}

	function getAllDataVin()//model auto complete	
	{		       
	
		$flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP	 
		$status_del = "1";
		$branch_id = $this->session->userdata('branch_id');
					 
		$this->load->database();
		$this->db->select('branch_id,stock_no,desc_model,desc_type,vin,engine,currency,dpp,vat,pph22,ppnbm,total_dpp_vat,price_offroad,colour,interior_colour,location_stock,status_flag_stock,status,temp_date_wrs');	
		$this->db->where("branch_id",$branch_id);	
		$this->db->where("status_flag_stock",$flag_stock);
		$this->db->where("status",$status_del);	
		$query = $this->db->get('qv_complite_master_stockin_mas');					
	  
		return $query->result();
		//return $query;		
	}
				 
}