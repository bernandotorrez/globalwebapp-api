<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_slssell_mas extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_master_sell_mas';
	 
    public function tampil_add_sell(){ //tampil table untuk ngececk num_row						     	     		  	   
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_sell = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		 		 		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $flagaproval = "0"; //jika 0 belum dikrirm approval
		 $flagreject = "-1"; //jika -1 reject
		 
		 $this->load->database();		
		 $this->db->select('id_trans_sales,name_branch,branch_id,nospk,stock_no,vin,engine,desc_type,colour,curr_sell,price_cost,remaining_amount_customer,discount,total_paymet_customer,date_aloc,lama_spk,cust_name,sal_name,ws_date,remarks,date_transaksi,date_reminder_spk,date_dudate_spk,flag_send_approval,desc_pay,status_flag_sell,status');				
		 $this->db->where('branch_id',$ses_id_branch );				 		 				
		 $this->db->where("(flag_send_approval='".$flagaproval."' OR flag_send_approval='".$flagreject."' )", NULL, FALSE);
		// $this->db->where('flag_send_approval',$status_flagaproval);		 
		 $this->db->where('status',$status_delete);		 
		 $this->db->where("(status_flag_sell='".$flag_sell."' OR status_flag_sell='".$flag_dp."' )", NULL, FALSE); 
		 $this->db->order_by('date_aloc','desc');
		 $result = $this->db->get('qv_complite_master_sell_mas');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    
		 $ses_id_branch = $this->session->userdata('branch_id'); 	 		 	 
		 
		 $flag_sell = "0" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		  		 		 			  		
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $flagaproval = "0"; //jika 0 belum dikrirm approval
		 $flagreject = "-1"; //jika -1 reject
		  
		 $this->load->database();		
		 $this->db->select('id_trans_sales,name_branch,branch_id,nospk,stock_no,vin,engine,desc_type,colour,curr_sell,price_cost,remaining_amount_customer,discount,total_paymet_customer,date_aloc,lama_spk,cust_name,sal_name,ws_date,date_transaksi,date_reminder_spk,date_dudate_spk,remarks,flag_send_approval,desc_pay,status_flag_sell,status');						
		 $this->db->where('branch_id',$ses_id_branch );			
		 $this->db->where("(flag_send_approval='".$flagaproval."' OR flag_send_approval='".$flagreject."' )", NULL, FALSE);
		 //$this->db->where('flag_send_approval',$status_flagaproval);		 
		 $this->db->where('status',$status_delete);		 
		 $this->db->where("(status_flag_sell='".$flag_sell."' OR status_flag_sell='".$flag_dp."' )", NULL, FALSE); 				
		 $this->db->order_by('date_aloc','desc');		
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_master_sell_mas')->result();		 		   
		 return $result ;
	}							  		
				    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}