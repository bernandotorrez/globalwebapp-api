<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_colour_vehicle_mas extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_colour_mas';
	 
    public function tampil_colour(){ //tampil table untuk ngececk num_row						     	     		  	    			
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		
		   		 		 				 
		 $this->load->database();		
		 $this->db->select('*');									 
		 $this->db->where('status',$status_delete);				
		 $this->db->order_by('id_colour','desc');
		 $result = $this->db->get('qv_complite_colour_mas');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    				 				
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  	     		 
	     $this->load->database();		
		 $this->db->select('*');											
		 $this->db->where('status',$status_delete);				  		 
		 $this->db->order_by('id_colour','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_colour_mas')->result();		 		   
		 return $result ;
	}							  		
		
  public function tampil_type()
  {
	  $str_status = "1" ;
	  $this->load->database();
	  $this->db->select('*');
	  $this->db->where('status',$str_status);
	  $this->db->order_by('id_type','desc'); 		 
	  $result = $this->db->get('tbl_brand_type_mas');		
	   		   
	  return $result ;	  
  }		
  
  public function save_data()
  {
	
	$str_colour = strtoupper($this->input->post('txtcolour'));  
	$strid_type = $this->input->post('cbotype');	
	$str_interior_colour= $this->input->post('txtinterior');		
	$str_status = "1";  
	
	$insert_data = array(
	  				"id_type" => $strid_type,
					"colour" => $str_colour,																
					"status" =>$str_status,
					"interior_colour" =>$str_interior_colour
					);
	$this->db->insert('tbl_colour_mas',$insert_data);		
	return true;							
  }		
  
    public function edit_data()
	{		 		
		$str_idcolour = $this->input->post('txtid');  				
		$str_colour = strtoupper($this->input->post('txtcolour'));  			
		$strid_type = $this->input->post('cbotype');
		$str_interior_colour= $this->input->post('txtinterior');
		
		if ($str_idcolour !='') {
			$edit_data = array( "colour" => $str_colour,	
								"id_type" => $strid_type,
								"interior_colour" =>$str_interior_colour	
								);						 																														
			$this->db->where('id_colour',$str_idcolour);								
			$this->db->update('tbl_colour_mas',$edit_data); 															
			return true;		
		}else{
			return false;		
		}
								
	  }		
	  
	    public function delete_data()
	    {		 		
			$delete = $this->input->post('msg');  
			$str_status_delete = "0" ;
								
			if ($delete !='') {						
				for ($i=0; $i < count($delete) ; $i++) {	
					$del_data = array("status" => $str_status_delete,);																																																
					
					$this->db->where('id_colour',$delete[$i]);
					$this->db->update('tbl_colour_mas',$del_data); 
				}		
				return true;		
			}else{
				return false;		
			}								
	  }		
				
		public function search_data() 	// fungsi cari data vendor
		{
		   $cari = $this->input->post('txtcari');
		   $kategory =  $this->input->post('cbostatus');	  		   		  		  
		   $status_del = "1";	   
		   
		   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
		   {
			   redirect('maserati_stock_sales/C_colour_vehicle_mas/C_colour_vehicle_mas');  //direct ke url C_sales/C_sales
		   }else{	
			   $this->load->database();		   			  	
			   $this->db->where('status', $status_del);
			   $this->db->like($kategory,$cari);			  
			   $result = $this->db->get('qv_complite_colour_mas');		 
			   return $result->result(); 
		   }
		} 		    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}