<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_control_stock_mas extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_master_stockin_mas';
	 
    public function tampil_add_stock(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 
		 	
		 $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  	
		 $flag_cross_sell = "3" ; //jika 3 cross selling	 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');						 		 		 				
		 $this->db->where('status',$status_delete);		 
		// $this->db->where('status_flag_stock',$flag_stock); 
		 $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' OR status_flag_stock='".$flag_cross_sell."' )", NULL, FALSE); 
		 $this->db->order_by('rec_date','desc');
		 $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    
		 		 
		 $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP    		 		 		
		 $flag_cross_sell = "3" ; //jika 3 cross selling
				   
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 $this->load->database();		
		 $this->db->select('*');										 
		 $this->db->where('status',$status_delete);		 
		// $this->db->where('status_flag_stock',$flag_stock); 			
		 $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' OR status_flag_stock='".$flag_cross_sell."' )", NULL, FALSE); 
		 $this->db->order_by('rec_date','desc');		
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_master_stockin_mas')->result();		 		   
		 return $result ;
	}
	
	 public function hitung_jumlah_stock(){ //
	 						     	 
	     $strbranch =  $this->input->post('cbobranchutama');		  	   			 		 	 		
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
	    
				 if  ($strbranch =="" ) : // jika combo branch tidak ada
					 $this->load->database();		
					 $this->db->select('*');									 		 		 				
					 $this->db->where('status',$status_delete);		 
					 $this->db->where('status_flag_stock',$flag_stock); 
					 $this->db->order_by('rec_date','desc');
					 $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
					 return $result;
				 else:	        
					 $this->load->database();		
					 $this->db->select('*');									 		 		 				
					 $this->db->where('branch_id',$strbranch);	
					 $this->db->where('status',$status_delete);		 
					 $this->db->where('status_flag_stock',$flag_stock); 
					 $this->db->order_by('rec_date','desc');
					 $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
					 return $result;
			endif;	 
		 
		  	 
	}
	
	 public function hitung_jumlah_dp(){ //tampil table untuk ngececk num_row						     	 
	     $strbranch =  $this->input->post('cbobranchutama');	    		  	   			 		 	 			
		 $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
		 if  ($strbranch =="" ) : // jika combo branch tidak ada	 
			 $this->load->database();		
			 $this->db->select('*');		 						 		 		 				
			 $this->db->where('status',$status_delete);		 
			 $this->db->where('status_flag_stock',$flag_dp); 
			 $this->db->order_by('rec_date','desc');
			 $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
			 return $result;			 
		 else:	 		  
						 
			 $this->load->database();		
			 $this->db->select('*');	
			 $this->db->where('branch_id',$strbranch);		 						 		 		
			 $this->db->where('status',$status_delete);		 
			 $this->db->where('status_flag_stock',$flag_dp); 
			 $this->db->order_by('rec_date','desc');
			 $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
			 return $result;			 
	     endif;		 
	}	
	
	  public function hitung_jumlah_wrs(){ //tampil table untuk ngececk num_row						
	 
	     $strbranch =  $this->input->post('cbobranchutama');    		  	   			 		 	 		
		 $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
	     if  ($strbranch =="" ) : // jika combo branch tidak ada
			  $this->load->database();		
			  $this->db->select('*');								 		 		 				
			  $this->db->where('status',$status_delete);		 
			  $this->db->where('status_flag_stock',$flag_wrs); 
			  $this->db->order_by('rec_date','desc');
			  $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
			  return $result;
		 else:			      
				  $this->load->database();		
				  $this->db->select('*');	
				  $this->db->where('branch_id',$strbranch);								 		 		
				  $this->db->where('status',$status_delete);		 
				  $this->db->where('status_flag_stock',$flag_wrs); 
				  $this->db->order_by('rec_date','desc');
				  $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
				  return $result;		    		  
		 endif;	 
	 }	    	    		
		
	 public function hitung_jumlah_cross_selling(){ //tampil table untuk ngececk num_row						
	 
	     $strbranch =  $this->input->post('cbobranchutama');    		  	   			 		 	 		
		 $flag_crsoss_Selling = "3" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 jika 3 crosseling
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 
	     if  ($strbranch =="" ) : // jika combo branch tidak ada
			  $this->load->database();		
			  $this->db->select('*');								 		 		 				
			  $this->db->where('status',$status_delete);		 
			  $this->db->where('status_flag_stock',$flag_crsoss_Selling); 
			  $this->db->order_by('rec_date','desc');
			  $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
			  return $result;
		 else:			      
				  $this->load->database();		
				  $this->db->select('*');	
				  $this->db->where('branch_id',$strbranch);								 		 		
				  $this->db->where('status',$status_delete);		 
				  $this->db->where('status_flag_stock',$flag_crsoss_Selling); 
				  $this->db->order_by('rec_date','desc');
				  $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
				  return $result;		    		  
		 endif;	 
	}	    	    		
	
	public function tampil_cabang_mazda(){ //tampil table untuk ngececk num_row						     	     		  	   			 		 	 		
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			 
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		 $id_company = "5"; //idcompany 3 MAZDA
		 
		 $this->load->database();		
		 $this->db->select('branch_id,name_branch,place,');									 		 		
		 $this->db->where('status',$status_delete);		 
		 $this->db->where('id_company',$id_company); 
		 $this->db->order_by('branch_id','desc');
		 $result = $this->db->get('tbl_branches');			     		 
		 return $result;
	}							  		
		
	public function get_search_date()
	{
	    $strdatestart =$this->input->post('txtdatestart');	
	    $strdatend =$this->input->post('txtdateend');	
	    $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	    $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		$status_delete = "1"; //jika 0 terdelete jika 1 aktif
		
		$flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	    $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	    $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		 
		 
	    if($strdatestart !="" and $strdatend !="" ){	
		   $this->load->database();		
	       $this->db->select('*');
		   $this->db->where('status',$status_delete);
		   $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' )", NULL, FALSE); 		  
		   $this->db->where('rec_date BETWEEN "'.$datestart.'" and "'.$dateend.'"');
		   $this->db->order_by('rec_date','desc');
	 	   $result = $this->db->get('qv_complite_master_stockin_mas');		
	       return $result;		 			  		 	   	 
	    }else{
		    redirect('maserati_stock_sales/c_control_stock_mas/c_control_stock_mas');	
		}
		  
	}
	
	function search_data() 	// fungsi cari data pp model
	{
	  // $strbranch          =  $this->input->post('cbobranch');	 	   	   	  	  
	   $kategory = $this->input->post('cbostatus');
	   $cari = $this->input->post('txtcari');
	   
	   $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	   $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	   $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		 
	   $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	
	   	   	   
	   if ($cari == null or $kategory =='--Choose--') 
	   {
		   redirect('maserati_stock_sales/c_control_stock_mas/c_control_stock_mas');  
	   }else{	
	     $this->load->database();			  	   		  		 	    
	     $this->db->where('status',$status_delete);
	     $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' )", NULL, FALSE); 
	     $this->db->like($kategory,$cari);		    		   
	     $result = $this->db->get('qv_complite_master_stockin_mas');		 	   
	     $this->session->set_flashdata('cari',$cari);	
		 
	     return  $result;
	   }
	} 				
	
	function shorting_table_branch() 	// fungsi cari data pp model
	{	   
	   $str_tampil_branch =  $this->input->post('cbobranchutama');	 	   	   	  	  	   	   	   
	   $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	   $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	   $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		 
	   $status_delete = "1"; //jika 0 terdelete jika 1 aktif
	   
	   	   	   
	  if ($str_tampil_branch == "") 
	   {
		   redirect('maserati_stock_sales/c_control_stock_mas/c_control_stock_mas');  
	   }else{	
	       $this->load->database();			  	   		  		 	    
		   $this->db->where('branch_id',$str_tampil_branch);
	       $this->db->where('status',$status_delete);
	       $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' )", NULL, FALSE); 	      	    		   
	       $result = $this->db->get('qv_complite_master_stockin_mas');		 	   	       
		 
	       return  $result;
	   } 
	} 			
	
	function get_type_vehicle()
	{			      
	     $this->load->database();		
		 $this->db->select('id_type,desc_type');			 
		 $this->db->where('status','1');			
		 $result = $this->db->get('tbl_brand_type_mas');			     		 
		 return $result->result();	 	
	}	
	
	function total_stock_date()
	{
		 $strdatestart =$this->input->post('txtdatestart');	
		 $strdatend =$this->input->post('txtdateend');
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif	
		  
		 $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
		 $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		 $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		  
	   if($strdatestart !="" and $strdatend !="" ):
		  $this->load->database();		
		  $this->db->select('*');								 		 		 				
		  $this->db->where('status',$status_delete);		
		  $this->db->where('rec_date BETWEEN "'.$datestart.'" and "'.$dateend.'"'); 
		  $this->db->where('status_flag_stock',$flag_stock); 
		  $this->db->order_by('rec_date','desc');
		  $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
		  return $result;
		endif;
		
	}		
	
	function total_stock_dp()
	{
		  $strdatestart =$this->input->post('txtdatestart');	
		  $strdatend =$this->input->post('txtdateend');	
		  $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  
		  $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
		  $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
		  $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
		  
		if($strdatestart !="" and $strdatend !="" ):
			 $this->load->database();		
			 $this->db->select('*');								 		 		 				
			 $this->db->where('status',$status_delete);		
			 $this->db->where('rec_date BETWEEN "'.$datestart.'" and "'.$dateend.'"'); 
			 $this->db->where('status_flag_stock',$flag_dp); 
			 $this->db->order_by('rec_date','desc');
			 $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
			 return $result;	
		endif;	 
	}		
	
	function total_stock_wrs()
	{		
		  $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP			
		  $strdatestart =$this->input->post('txtdatestart');	
	      $strdatend =$this->input->post('txtdateend');	
		  $status_delete = "1"; //jika 0 terdelete jika 1 aktif
				  
		  $datestart =date('Y-m-d', strtotime($strdatestart)); //rubah format tanggal
	      $dateend =date('Y-m-d', strtotime($strdatend)); //rubah format tanggal
			  
		    if($strdatestart !="" and $strdatend !="" ):
				 $this->load->database();		
				 $this->db->select('*');								 		 		 				
				 $this->db->where('status',$status_delete);		
				 $this->db->where('rec_date BETWEEN "'.$datestart.'" and "'.$dateend.'"'); 
				 $this->db->where('status_flag_stock',$flag_wrs); 
				 $this->db->order_by('rec_date','desc');
				 $result = $this->db->get('qv_complite_master_stockin_mas');			     		 
				 return $result;
			endif;	 
	}		
	
	
	
	function total_stock_cari_data()
	{
		$strcari = $this->input->post('txtcari');
		$strcategory = $this->input->post('cbostatus');
		$str_flag_stock = '1';
		$str_flag_delete = '1';
					
		$this->load->database();
		$this->db->select('*') ;
		$this->db->like($strcategory,$strcari);
		$this->db->where('status_flag_stock',$str_flag_stock);		
		$this->db->where('status',$str_flag_delete);
		
		
		$result = $this->db->get('qv_complite_master_stockin_mas');
		
		return $result;
			
	} 			  	   			 	
	
	function total_dp_cari_data()
	{
		$strcari = $this->input->post('txtcari');
		$strcategory = $this->input->post('cbostatus');
		$str_flag_stock = '2';
		$str_flag_delete = '1';				
		
		$this->load->database();
		$this->db->select('*') ;
		$this->db->like($strcategory,$strcari);
		$this->db->where('status_flag_stock',$str_flag_stock);		
		$this->db->where('status',$str_flag_delete);		
		
		$result = $this->db->get('qv_complite_master_stockin_mas');
		
		return $result;
	} 			  	   			 	
	
	function total_wrs_cari_data()
	{
		$strcari = $this->input->post('txtcari');
		$strcategory = $this->input->post('cbostatus');
		$str_flag_stock = '-1';
		$str_flag_delete = '1';				
		
		$this->load->database();
		$this->db->select('*') ;
		$this->db->like($strcategory,$strcari);
		$this->db->where('status_flag_stock',$str_flag_stock);		
		$this->db->where('status',$str_flag_delete);		
		
		$result = $this->db->get('qv_complite_master_stockin_mas');
		
		return $result;
			
	} 		
	
	function hitung_jumlah_cross_selling_cari_data()
	{
		$strcari = $this->input->post('txtcari');
		$strcategory = $this->input->post('cbostatus');
		$flag_crsoss_Selling = "3" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 jika 3 crosseling
		$str_flag_delete = '1';				
		
		$this->load->database();
		$this->db->select('*') ;
		$this->db->where($strcategory,$strcari);
		$this->db->where('status_flag_stock',$flag_crsoss_Selling);		
		$this->db->where('status',$str_flag_delete);		
		
		$result = $this->db->get('qv_complite_master_stockin_mas');
		
		return $result;
			
	} 		
	
//EXCEL ALL -----------------------------------------------------------------------	  	   			 	
	 function ToExcelAll(){ // buat narik table to excel						    
	 			 
		 $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		 $str_cbobranch =$this->input->post("cbobranch");
		 $str_flag_stock = $this->input->post('cbostatusstock'); // jika stock combo.
		 
		 $flag_wrs = "-1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_stock = "1" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS 2 DP
	     $flag_dp = "2" ; //jika 1 ready stock, jika 0 terjual, jika -1 WRS	 2 DP  		 
		 $flag_cross_sell = "3" ; //jika 3 cross selling
		 $str_flag_stock_bstk = "0";
		 
		 $tglstart = $this->input->post('txtrptdtstart');
		 $tglend =  $this->input->post('txtrptdtend'); 
		 
		 $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	     $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal 
	   
	  	 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
				 		           		    			 			
			 
		 if ($str_flag_stock == "1") :	
			      $this->load->database();				 	
				  $this->db->select('*');								 		 
				 
				 if ($str_cbobranch != "") :			 		  
					 $this->db->where('branch_id',$str_cbobranch);
				 endif;		
											
				 if ($str_cbotype != "") :			 		  
					 $this->db->where('id_type',$str_cbotype);
				 endif;	 	
				 	   	
				 $this->db->where('status',$status_delete );			 			 			
				 $this->db->where('rec_date BETWEEN "'. $datestart. '" and "'.$dateend.'"');	
				 $this->db->where("(status_flag_stock='".$flag_stock."' OR status_flag_stock='".$flag_dp."' OR status_flag_stock='".$flag_wrs."' OR status_flag_stock='".$flag_cross_sell."' )", NULL, FALSE); 			
				 $this->db->order_by('rec_date','desc');
				 $result = $this->db->get('qv_complite_master_stockin_mas');												 
	       else :
			     if ($str_flag_stock == "0") :					     	
					 
					  $this->load->database();				 	
					  $this->db->select('*');								 		 
					 
					  if ($str_cbobranch != "") :			 		  
						 $this->db->where('branch_id',$str_cbobranch);
					  endif;		
												
					  if ($str_cbotype != "") :			 		  
						 $this->db->where('id_type',$str_cbotype);					  
					  endif;	 
					  
					   $this->db->where('status',$status_delete );			 			 			
					   $this->db->where('rec_date BETWEEN "'. $datestart. '" and "'.$dateend.'"');	
					   $this->db->where('status_flag_stock',$str_flag_stock_bstk);			
					   $this->db->order_by('rec_date','desc');
					   $result = $this->db->get('qv_complite_master_stockin_mas');														 					
					  				  					
			 	  endif;	  
		 endif;	  			 							 	 
			
			 			
		 if ($result->num_rows() > 0):
				 return  $result->result_array();				
		 else:
				 return null ;				
		 endif;		 						 		  								 
	}	 
	
	 function ToExcelAll_total_stock(){ // buat narik table to excel						    
	 	  $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		  $str_cbobranch =$this->input->post("cbobranch");
		 
		  $tglstart = $this->input->post('txtrptdtstart');
		  $tglend =  $this->input->post('txtrptdtend'); 
		 
		  $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	      $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal 
		 
		  $str_flag_stock = '1'; // flag stock berari 1				   
	  	  $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
				 		           		    			 
			 $this->load->database();				 	
			 $this->db->select('*');								 		 
			 
			 if ($str_cbobranch != "") :			 		  
			 	 $this->db->where('branch_id',$str_cbobranch);
			 endif;		
			 						 	
			 if ($str_cbotype != "") :			 		  
			 	 $this->db->where('id_type',$str_cbotype);
			 endif;	 
			 							 	 
			 $this->db->where('status',$status_delete );	
			 $this->db->where('rec_date BETWEEN "'. $datestart. '" and "'.$dateend.'"');
			 $this->db->where('status_flag_stock', $str_flag_stock  );			 			 								
			 $this->db->order_by('rec_date','desc');
			 $result = $this->db->get('qv_complite_master_stockin_mas');
			 					
		     return  $result;	
			   		 
	 }
	 
	  function ToExcelAll_total_BSTK(){ // buat narik table to excel						    
	 	  $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		  $str_cbobranch =$this->input->post("cbobranch");
		 
		  $tglstart = $this->input->post('txtrptdtstart');
		  $tglend =  $this->input->post('txtrptdtend'); 
		 
		  $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	      $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal 
		 
		  $str_flag_stock = '1'; // flag stock berari 1				   
	  	  $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
		  $str_flag_stock_bstk = "0";
		   		 		           		    			 
			 $this->load->database();				 	
			 $this->db->select('*');								 		 
			 
			 if ($str_cbobranch != "") :			 		  
			 	 $this->db->where('branch_id',$str_cbobranch);
			 endif;		
			 						 	
			 if ($str_cbotype != "") :			 		  
			 	 $this->db->where('id_type',$str_cbotype);
			 endif;	 
			 							 	 
			  $this->db->where('status',$status_delete );			 			 			
			  $this->db->where('rec_date BETWEEN "'. $datestart. '" and "'.$dateend.'"');	
			  $this->db->where('status_flag_stock',$str_flag_stock_bstk);			
			  $this->db->order_by('rec_date','desc');
			  $result = $this->db->get('qv_complite_master_stockin_mas');		
			 					
		     return  $result;	
			   		 
	 }
				
	 function ToExcelAll_total_dp(){ // buat narik table to excel						    
	 			 
		 $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		 $str_cbobranch =$this->input->post("cbobranch");
		 
		 $tglstart = $this->input->post('txtrptdtstart');
		 $tglend =  $this->input->post('txtrptdtend'); 
		 
		 $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	     $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal 
		 
		 $str_flag_stock = '2'; // flag dp berari 2				   
	  	 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
				 		           		    			 
			 $this->load->database();				 	
			 $this->db->select('*');								 		 
			 
			 if ($str_cbobranch != "") :			 		  
			 	 $this->db->where('branch_id',$str_cbobranch);
			 endif;		
			 						 	
			 if ($str_cbotype != "") :			 		  
			 	 $this->db->where('id_type',$str_cbotype);
			 endif;	 
			 							 	 
			 $this->db->where('status',$status_delete );	
			  $this->db->where('rec_date BETWEEN "'. $datestart. '" and "'.$dateend.'"');
			 $this->db->where('status_flag_stock', $str_flag_stock  );			 			 								
			 $this->db->order_by('rec_date','desc');
			 $result = $this->db->get('qv_complite_master_stockin_mas');
			 			
		 	return  $result;
	 }			    	  	
	 
	 function ToExcelAll_total_wrs(){ // buat narik table to excel						    
	 			 
		 $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		 $str_cbobranch =$this->input->post("cbobranch");
		 
		 $tglstart = $this->input->post('txtrptdtstart');
		 $tglend =  $this->input->post('txtrptdtend'); 
		 
		 $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	     $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal 
		 
		 $str_flag_stock = '-1'; // flag WRS berari -1				   
	  	 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
				 		           		    			 
			 $this->load->database();				 	
			 $this->db->select('*');								 		 
			 
			 if ($str_cbobranch != "") :			 		  
			 	 $this->db->where('branch_id',$str_cbobranch);
			 endif;		
			 						 	
			 if ($str_cbotype != "") :			 		  
			 	 $this->db->where('id_type',$str_cbotype);
			 endif;	 
			 							 	 
			 $this->db->where('status',$status_delete );	
			 $this->db->where('rec_date BETWEEN "'. $datestart. '" and "'.$dateend.'"');
			 $this->db->where('status_flag_stock', $str_flag_stock  );			 			
			 $this->db->order_by('rec_date','desc');
			 $result = $this->db->get('qv_complite_master_stockin_mas');
			 return  $result;
	 }			    	  	
	 
	 function ToExcelAll_total_cross_sell(){ // buat narik table to excel						    
	 			 
		 $str_cbotype= $this->input->post("cbotype"); 		  		 	  				
		 $str_cbobranch =$this->input->post("cbobranch");
		 
		 $tglstart = $this->input->post('txtrptdtstart');
		 $tglend =  $this->input->post('txtrptdtend'); 
		 
		 $datestart =date('Y-m-d', strtotime($tglstart)); //rubah format tanggal
	     $dateend =date('Y-m-d', strtotime($tglend)); //rubah format tanggal 
		 
		 $str_flag_stock = '3'; // flag cross sell berari 3				   
	  	 $status_delete = "1"; //jika 0 terdelete jika 1 aktif			
				 		           		    			 
			 $this->load->database();				 	
			 $this->db->select('*');								 		 
			 
			 if ($str_cbobranch != "") :			 		  
			 	 $this->db->where('branch_id',$str_cbobranch);
			 endif;		
			 						 	
			 if ($str_cbotype != "") :			 		  
			 	 $this->db->where('id_type',$str_cbotype);
			 endif;	 
			 							 	 
			 $this->db->where('status',$status_delete );	
			 $this->db->where('rec_date BETWEEN "'. $datestart. '" and "'.$dateend.'"');
			 $this->db->where('status_flag_stock', $str_flag_stock  );			 			
			 $this->db->order_by('rec_date','desc');
			 $result = $this->db->get('qv_complite_master_stockin_mas');
			 return  $result;
	 }			    	  	
//end------------------------------------------------------------------------------------------------------------------			
				 
}