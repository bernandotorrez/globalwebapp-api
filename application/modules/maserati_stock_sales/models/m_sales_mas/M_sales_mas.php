<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_sales_mas extends CI_Model {	
				    
    public $db_tabel = 'qv_complite_sales_person_mas';
	 
    public function tampil_sales(){ //tampil table untuk ngececk num_row						     	     		  
	     $ses_branchid = $this->session->userdata('branch_id');  			
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif		
		   		 		 				 
		 $this->load->database();		
		 $this->db->select('*');						
		 $this->db->where('branch_id',$ses_branchid);		 
		 $this->db->where('status',$status_delete);				
		 $this->db->order_by('id_sales','desc');
		 $result = $this->db->get('qv_complite_sales_person_mas');			     		 
		 return $result;
	}	    	
	public function tampil_limit_table($start_row, $limit)	{  //tampil table untuk pagging 	    
	    
		 $ses_branchid = $this->session->userdata('branch_id'); 		 				
		 $status_delete = "1"; //jika 0 terdelete jika 1 aktif
		  	     		 
	     $this->load->database();		
		 $this->db->select('*');		
		 $this->db->where('branch_id',$ses_branchid);									
		 $this->db->where('status',$status_delete);				  		 
		 $this->db->order_by('id_sales','desc'); 		 
		 $this->db->limit($start_row, $limit);					     	 	   		
		 $result = $this->db->get('qv_complite_sales_person_mas')->result();		 		   
		 return $result ;
	}							  		
		
		
  public function save_data()
  {
	$ses_branchid = $this->session->userdata('branch_id');  	
	
	$str_vendor = strtoupper($this->input->post('txtsales'));  
	$str_phone  = $this->input->post('txtphone');  	
	$str_alamat = $this->input->post('txtalamat');  
	$str_position = $this->input->post('cbopostion');		
	$str_status = "1";  
	
	$insert_data = array(
	  				"sal_name" => $str_vendor,
					"alamat" => $str_alamat,					
					"phone" => $str_phone ,					
					"branch_id" =>$ses_branchid,
					"postion_class" => $str_position,
					"status" =>$str_status,
					);
	$this->db->insert('tbl_sales_mas',$insert_data);		
	return true;							
  }		
  
    public function edit_data()
	{		 		
		$str_idsales = $this->input->post('txtid');  
		
		$ses_branchid = $this->session->userdata('branch_id');  	
	
		$str_salesname = strtoupper($this->input->post('txtsales'));  
		$str_phone  = $this->input->post('txtphone');  	
		$str_alamat = $this->input->post('txtalamat'); 	
		$str_position = $this->input->post('cbopostion');
		
		if ($str_idsales !='') {
			$edit_data = array(
							"sal_name" => $str_salesname,
							"alamat" => $str_alamat,
							"phone" => $str_phone,	
							"postion_class" => $str_position,																															
							);
							
			$this->db->where('id_sales',$str_idsales);					
			$this->db->where('branch_id',$ses_branchid);
			$this->db->update('tbl_sales_mas',$edit_data); 															
			return true;		
		}else{
			return false;		
		}
								
	  }		
	  
	    public function delete_data()
	    {		 		
			$delete = $this->input->post('msg');  
			$str_status_delete = "0" ;
								
			if ($delete !='') {						
				for ($i=0; $i < count($delete) ; $i++) {	
					$del_data = array("status" => $str_status_delete,);																																																
					
					$this->db->where('id_sales',$delete[$i]);
					$this->db->update('tbl_sales_mas',$del_data); 
				}		
				return true;		
			}else{
				return false;		
			}								
	  }		
				
		public function search_data() 	// fungsi cari data vendor
		{
		   $cari = $this->input->post('txtcari');
		   $kategory =  $this->input->post('cbostatus');	  		   		  
		   $ses_branch_id = $this->session->userdata('branch_id');
		   $status_del = "1";	   
		   
		   if ($cari == null or $kategory =='--Choose--') // jika textbox cari kosong
		   {
			   redirect('maserati_stock_sales/c_sales_mas/c_sales_mas');  //direct ke url C_sales/C_sales
		   }else{	
			   $this->load->database();		   
			   $this->db->where('branch_id',$ses_branch_id );			
			   $this->db->where('status', $status_del);
			   $this->db->like($kategory,$cari);			  
			   $result = $this->db->get('qv_complite_sales_person_mas');		 
			   return $result->result(); 
		   }
		} 		    	   			  	   			 	 	 	   	
//------------------------------------------------------------------------------------------------------------------			
				 
}