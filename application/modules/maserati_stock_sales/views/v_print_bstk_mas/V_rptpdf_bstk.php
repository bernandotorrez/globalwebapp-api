<?php
			
	require('Mc_table.php');
	$pdf=new PDF_MC_Table('P','cm',"A4");
	$pdf->Open();
	$pdf->AddPage();	
	$pdf->AliasNbPages();
	$pdf->SetMargins(1,1,1);
	$pdf->SetFont('times','B',12);

	//$pdf->Image('asset/images/bmw_logo.jpg',17,0.8,2.5); //tampilan yg pertama di tampilkan report : P,L,B
			//-----------------------------------------------------					
	$pdf->Ln(1.2);
	$pdf->setFont('Arial','BU',12);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.7,"BUKTI ACARA SERAH TERIMA KENDARAAN",0,0,'C',1);  
	
	$pdf->Ln(1); // baris berikut nya pada report denga lebar (line)garis ke bawah sebanyak 15(line) 
	$pdf->setFont('Arial','',8); //Type huruf(font beserta besaran huruf)
	$pdf->setFillColor(255,255,255); //warna pada baris tulisan                									
	$pdf->cell(8,0.5,"No.Surat Pesanan : ".$this->session->userdata('ses_nospk'),0,0,'L',1); 				
	$pdf->cell(1.6,0.5,"Kepada : ".$this->session->userdata('ses_csutomer'),0,1,'R',1); 
	
	
	$pdf->Ln(0.1); // baris berikut nya pada report denga lebar (line)garis ke bawah sebanyak 15(line) 
	$pdf->setFont('Arial','',8); //Type huruf(font beserta besaran huruf)
	$pdf->setFillColor(255,255,255); //warna pada baris tulisan          
	$pdf->cell(6.8,0.4,"Syarat Pembayaran : " . $this->session->userdata('ses_desc_pay'),0,0,'L',1);
	$pdf->MultiCell(9,0.4,"Alamat : ".$this->session->userdata('ses_address_cus'),0,1,'R',1); 
	
	/*$pdf->Ln(0.3);
	$pdf->setFont('Arial','',7);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(8,0.5,"Alamat : ".$this->session->userdata('ses_address_cus'),0,1,'R',1); */
			                		
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',8);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(8,0,"1 (satu) unit  : "."BMW"." / ".$this->session->userdata('ses_desc_model'),0,1,'L',1); 
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',8);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(8,0,"Type : ".$this->session->userdata('ses_desc_type'),0,1,'L',1); 
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',8);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(8,0,"Tahun :".$this->session->userdata('ses_year'),0,1,'L',1);     
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',8);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(8,0,"No.Rangka :".$this->session->userdata('ses_vin'),0,1,'L',1);     
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',8);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(8,0,"Mesin :".$this->session->userdata('ses_engine'),0,1,'L',1);
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',8);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(8,0,"Warna :".$this->session->userdata('ses_colour'),0,1,'L',1);  
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','',8);
	$pdf->setFillColor(255,255,255);                               
	$pdf->cell(8,0,"TGL Cetak : ".date('d-m-Y'),0,1,'L',1);                       
		
	
	$pdf->Ln(0.3);
	$pdf->setFont('Arial','BU',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.7,"Perlengkapan Tambahan      : ",0,0,'L',1);  
	
	$pdf->Ln(0.7);
	$pdf->setFont('Arial','BU',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(6.3,0.5,  "Kondisi kebersihan",0,0,'L',1); 
	$pdf->cell(6.3,0.5,  "Fungsi-Fungsi Kontrol Saklar",0,0,'L',1);
	$pdf->cell(6.3,0.5,  "Kelengkapan",0,1,'L',1);
	$pdf->cell(6.3,0.5,  "",0,0,'L',1); 
	$pdf->cell(6.3,0.5,  "Dan Pengoperasian",0,0,'L',1);
	
	$pdf->Ln(0.7);
	$pdf->setFont('Arial','',8);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(6.3,0.5,  "- Exterior",0,0,'L',1); 
	$pdf->cell(6.3,0.5,  "- Intsrument Cluster & indikator",0,0,'L',1);
	$pdf->cell(6.3,0.5,  " ",0,1,'L',1);
	$pdf->cell(6.3,0.5,  "-	Bagasi & Ruang Mesin",0,0,'L',1); 
	$pdf->cell(6.3,0.5,  "- Radio/Tape/Disc",0,0,'L',1);
	$pdf->cell(6.3,0.5,  "- Owner Manual",0,1,'L',1);
	$pdf->cell(6.3,0.5,  "-	Plafon & Jok",0,0,'L',1); 
	$pdf->cell(6.3,0.5,  "- AC/Heater/Defogger",0,0,'L',1);
	$pdf->cell(6.3,0.5,  "- Kunci Kontak (  2  ) buah",0,1,'L',1);
	$pdf->cell(6.3,0.5,  "-	Karpet & Door Trim",0,0,'L',1); 
	$pdf->cell(6.3,0.5,  "- Central Door Lock& Bagasi",0,0,'L',1);
	$pdf->cell(6.3,0.5,  "- Lighter",0,1,'L',1);
	$pdf->cell(6.3,0.5,  "-	Asbak",0,0,'L',1); 
	$pdf->cell(6.3,0.5,  "- Power Window & Rear Screen",0,0,'L',1);
	$pdf->cell(6.3,0.5,  "- 1 Set Tools",0,1,'L',1);
	$pdf->cell(6.3,0.5,  " ",0,0,'L',1); 
	$pdf->cell(6.3,0.5,  "- Electric Mirror    ",0,0,'L',1);
	$pdf->cell(6.3,0.5,  "- 1 Set Karpet",0,1,'L',1);
	
	if( $this->session->userdata('ses_kacafilm') != "0.00") {
	    $pdf->cell(6.3,0.5,  "- Optional Kaca Film : YES",0,0,'L',1);
	}else{
		$pdf->cell(6.3,0.5, "- Optional Kaca Film : NO",0,0,'L',1);
	}
	//$pdf->cell(6.3,0.5,  " ",0,0,'L',1); 
	$pdf->cell(6.3,0.5,  "- Penghapus Kaca dan Cairan Pembersih",0,0,'L',1);
	$pdf->cell(6.3,0.5,  "- 4 Buah Dop Roda",0,1,'L',1);
	
	if( $this->session->userdata('ses_pdi') != "0.00") {
	   $pdf->cell(6.3,0.5,  "- PDI : YES ",0,0,'L',1); 
	}else{
	   $pdf->cell(6.3,0.5,  "- PDI : NO ",0,0,'L',1);
		
	}
	
	$pdf->cell(6.3,0.5,  "- Lampu Depan besar dan kecil",0,0,'L',1);
	$pdf->cell(6.3,0.5,  "- Bahan Bakar",0,1,'L',1);
	if( $this->session->userdata('ses_poles') != "0.00") {
	    $pdf->cell(6.3,0.5, "- Poles : YES",0,0,'L',1);
	}else{
		$pdf->cell(6.3,0.5, "- Poles : NO",0,0,'L',1);
	}
	$pdf->cell(6.3,0.5,  "- Head,Fog,Side Lamp ",0,0,'L',1);
	$pdf->cell(6.3,0.5,  " ",0,1,'L',1);
	
	if( $this->session->userdata('ses_suratjalan') != "0.00") {
		$pdf->cell(6.3,0.5,  "- Surat Jalan : YES ",0,0,'L',1); 
	}else{
		$pdf->cell(6.3,0.5,  "- Surat Jalan : NO ",0,0,'L',1);
	}
	
	$pdf->cell(6.3,0.5,  "- Turn Signal & Hazard",0,0,'L',1);
	$pdf->cell(6.3,0.5,  " ",0,1,'L',1);
	
	if( $this->session->userdata('ses_suratjalan') != "0.00") {
		$pdf->cell(6.3,0.5,  "- Other : NO",0,0,'L',1); 
	}else{
		$pdf->cell(6.3,0.5,  "- Other : NO",0,0,'L',1); 
	}
	
	$pdf->cell(6.3,0.5,  "- 1 Lampu Kabin Manual & Door Open",0,0,'L',1);
	$pdf->cell(6.3,0.5,  " ",0,1,'L',1);
	$pdf->cell(6.3,0.5,  " ",0,0,'L',1); 
	$pdf->cell(6.3,0.5,  "- Seat Adjuster",0,0,'L',1);
	$pdf->cell(6.3,0.5,  " ",0,1,'L',1);
	$pdf->cell(6.3,0.5,  " ",0,0,'L',1); 
	$pdf->cell(6.3,0.5,  "- Tail,Brake,reverse Light & back Fog Lamp",0,0,'L',1);
	$pdf->cell(6.3,0.5,  " ",0,1,'L',1);
	
	
	$pdf->Ln(0.0);
	$pdf->setFont('Arial','',8);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.4,"Penjelasan",0,1,'L',1); 
	$pdf->cell(19,0.4,"Layanan khusus",0,1,'L',1); 
	$pdf->cell(19,0.4,"Warranty kend dan tanggung jawab ",0,1,'L',1); 
	$pdf->cell(19,0.4,"Perawatan berkala kewajiban pemilik",0,1,'L',1); 
	$pdf->cell(19,0.4,"Jaringan & Staff Bengkel",0,1,'L',1); 
	$pdf->cell(19,0.4,"Hal-hal yang perlu diperhatikan",0,1,'L',1); 
	
	
	
	$pdf->Ln(0.2);
	$pdf->setFont('Arial','B',9);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.5,"Pengirim kendaraan :",0,1,'L',1);
	
	$pdf->Ln(0.1);
	$pdf->setFont('Arial','',8);
	$pdf->setFillColor(255,255,255);  
	$pdf->cell(19,0.5,"Telah saya pahami seluruh penjelasan kendaraan secara menyeluruh dan",0,1,'L',1);
	$pdf->cell(19,0.5,"Telah saya konfirmasikan keadaan dalam kondisi baik.",0,1,'L',1);
	$pdf->cell(19,0.5,"Selanjutnya merupakan kewajiban bagi saya untuk mematuhi segala",0,1,'L',1);
	$pdf->cell(19,0.5,"Penjelasan owner's manual book yang telah saya terima,sebagai pedoman",0,1,'L',1);
	$pdf->cell(19,0.5,"Pengoperasian kendaraan secara baik dan benar.",0,1,'L',1);
	
	$pdf->Ln(0.5);
	$pdf->setFont('Arial','BU',9);
	$pdf->setFillColor(255,255,255);
	$pdf->cell(3.3,0.7,"Diterima Pengirim ",0,0,'L',1); 
	$pdf->cell(7.5,0.7,"Diterima diperiksa disetujui",0,0,'L',1);  
	$pdf->cell(4.5,0.7,"Diserahkan gudang  ",0,0,'L',1); 
	$pdf->cell(3.3,0.7,"Branch Manager    ",0,1,'L',1); 
	$pdf->cell(3.3,0.3,"Tanggal	: 		  ",0,0,'L',1); 	  
	$pdf->cell(7.5,0.3,"Tanggal	: 		",0,0,'L',1); 
	$pdf->cell(4.5,0.3,"Tanggal	: 		",0,0,'L',1); 
	$pdf->cell(6.3,0.3,"Tanggal	: 		",0,1,'L',1);  	  
	
    $pdf->Ln(0.0);
	$pdf->setFont('Arial','',8);
	$pdf->setFillColor(255,255,255);
	
	$pdf->cell(6.3,1.5," ",0,0,'L',1); 
	$pdf->cell(6.3,1.5," ",0,0,'L',1); 
	$pdf->cell(6.3,1.5," ",0,1,'L',1); 	 	
	$pdf->cell(3.3,0.7,"(".$this->session->userdata('ses_sales_bmw').")",0,0,'L',1); 
	$pdf->cell(7.5,0.7,"(".$this->session->userdata('ses_csutomer').")",0,0,'L',1); 
	$pdf->cell(4.5,0.7,"(  ADH  )",0,0,'L',1); 
	$pdf->cell(7,0.7,"(  Jean Michael David  )",0,1,'L',1);  
	
	
	$pdf->Output(); //hasil out put ke browser
	
?>