<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">


<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_fail') ;?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_insert_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_insert_fail');?>  
        </div>	
<?php } ?>



<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">                 
<div class="panel panel-default">
  <div class="panel-heading" id="label_head_panel" style="color:#B66F03; font-size:18px;">Edit  Stock Unit Vehicle PT.<?php echo $this->session->userdata('company'); ?></div>                 
     <?php  echo form_open_multipart('bmw_stock_sales/c_slsstock_bmw/c_crud_stock_bmw/do_edit_stock',array('class'=>'form-multi'));  ?> 
  
       <table width="550" class="table ">                               
         <?php
			  if ($check_row >= 1) :  
				 foreach($tampil_edit_stock as $row_stock) :
	      ?>                     
         <tr> 
             <td width="301">  
                 <div class="form-group">
                   <label for="Stock Number"  class="col-xs-3">Stock Number</label>
                    <div class="col-xs-6">
                    
                    <input type="text" id="txtnostock" name="txtnostock" class="form-control" readonly value="<?php echo $row_stock->stock_no ;?>"  />
                   
                    </div>
                </div>
             </td> 
               <td width="218">  
                 <div class="form-group">
                   <label for="Branch"  class="col-xs-3"> Branch </label>
                   <div class="col-xs-5">
                     <input type="text" id="txtbranch" name="txtbranch" class="form-control"  value="<?php echo $this->session->userdata('name_branch'); ?>"  readonly />
                   </div>
                </div>
               </td> 
                   		                                                        
          </tr>
           <tr>
            <td>   
              <div class="form-group">
                <label for="Location Stock"  class="col-xs-3">Location Stock</label>
                <div class="col-xs-5">               
                 <select id="cboloc" name="cboloc"  class="form-control">
                      <option><?php echo $row_stock->location_stock ; ?></option>	                    
                        <?php foreach($tampil_lokasi as $row){ ?>                         
                         <option ><?php echo $row->name_loc ;?></option>
                       <?php } ?>  
               </select>
                </div>
              </div>                
             </td>  
              <td>   
                  <div class="form-group">
                   <label for="Stock keeper"  class="col-xs-3">Stock keeper</label>
                     <div class="col-xs-6">
                     <input type="text" id="txtuser" name="txtuser" class="form-control" 
                     value="<?php echo $this->session->userdata('name');  ?>"  readonly />
                    </div>
                </div>     
             </td>                         
       </tr>             
             <tr>   
               <td>
				  
             <div class="form-group">
                        <label for="Term Of VIN" style="color:#900"  class="col-xs-3">Curent VIN</label>
                        <div class="col-xs-6">
                         <input name="txtvin" type="text" class="form-control" 
                         id="txtvin"  value="<?php echo $row_stock->vin ; ?>"   
                         maxlength="40" readonly  />                                                
                   </div>
            </div>               
              </td>
               <td>
                <div class="form-group">
            	<label for="Type"  class="col-xs-3" style="color:#900">Type *</label>
            <div class="col-xs-7">                 
                 <select id="cbotype" name="cbotype" class="form-control">             											    	           <option value="<?php  echo $row_stock->id_type ?>"><?php echo $row_stock->desc_type ?></option>	 
                    <?php
						foreach ($tampil_type_vehicle as $row_type) :						
							echo '<option value="'.$row_type->id_type.'">'.$row_type->desc_type.'</option>';
						endforeach;
					?>
                 </select>
            </div>
          </div>
               </td>                        
            </tr>             
         <tr>   
         	<td>
               
            
            
             <div class="form-group">
                        <label for="Term Of VIN" style="color:#900"  class="col-xs-3">VIN Update *</label>
                        <div class="col-xs-6">
                         <input name="txtvinupdate" type="text" class="form-control"
                          id="txtvinupdate"  value="<?php echo $row_stock->vin ; ?>"  
                          placeholder="Vin" style="color:#900"  maxlength="40"  />                                                
                   </div>
            </div>           
            
          
           </td>
            <td>
            	 
       	  <div class="form-group">
            		<label for="Colour"  class="col-xs-3" style="color:#900">Colour *</label>
            <div  class="col-xs-6">                  
                <select id="cbowarna" name="cbowarna" class="form-control">         
                   <option value="<?php echo $row_stock->id_colour ; ?>"><?php echo $row_stock->colour ; ?></option>	
                     <?php
						foreach($warna_sesuai_type as $row_warna_type) :						
						      echo '<option value="'.$row_warna_type->id_colour.'">'.$row_warna_type->colour.'</option>';
						endforeach;
					 ?>                                        
                </select>
            </div>
		</div>
           </td>                                                
         </tr> 
          
           <tr>   
         	<td>
             <div class="form-group">
         			 <label for="Invoice Date" style="color:#900"  class="col-xs-3">Invoice Date*</label>
         			<div class="col-xs-4">
          		 <input type="text" id="cbointake" name="cbointake"  data-date-format="dd-mm-yyyy" 
               class="form-control datepicker" value="<?php if ($row_stock->pay_date_bi !="") :				 
							      echo date('d-m-Y',strtotime($row_stock->intake)) ; 
						    else :
						         echo "";
						    endif   				 
				 	        ?>" readonly /> 
               </div>
            </div>     
           </td> 
           
           <td>            
                <div class="form-group">
                   <label for="Colour Interior"  class="col-xs-3">Colour Interior</label>
                     <div class="col-xs-6">
                     <input type="text" id="txtinterior" name="txtinterior" 
                     class="form-control" readonly   
                     value="<?php echo $row_stock->interior_colour ; ?>"/>
                    </div>
                </div>       
         </td>                                                 
        </tr>   
            <tr>   
         	<td>
                 
        <div class="form-group">
         			 <label for="Receive Date Unit" style="color:#900"  class="col-xs-3">Receive  Date Unit*</label>
         			<div class="col-xs-4">
          			 <input type="text" id="txtdatericived" name="txtdatericived"  
                 data-date-format="dd-mm-yyyy" class="form-control datepicker" 
                 readonly  value="<?php echo date('d-m-Y',strtotime($row_stock->rec_date)) ; ?>">
                    </div>
             </div>             
                           
           </td> 
           
           <td>            
              <div class="form-group">
                <label for="attach"  class="col-xs-3">Upload VIN/Engine Unit</label>
                
            <div class="col-xs-8">                         
             <div class="btn btn-success fileinput-button">            
             <input type="file" id="txtupload" name="userfile" 
             class="fileinput-button" value="<?php echo $row_stock->attach_foto ?>" />                       				
             </div>
                   
			   <?php      				
                  If ($row_stock->attach_foto != "")
                  {  
                   echo  anchor_popup($row_stock->attach_foto,'PDF Unit Already Exsit');
                  }else{
                    echo '<label style="background-color:#f4f497;color:#900;">'.'Empty Foto Attachment!!!'.'</label>' ;	  
                  }
                ?>                  
                         
               </div>     
           </div>               
                                                  
         </td>                                                 
        </tr>                 
       
	 <tr>   
       <td>    
         <div class="form-group">
        	   <label for="Bank Form A"  class="col-xs-3">Fasilitas Bank</label>
        	   <div class="col-xs-5">
          			  <select id="cbobank" name="cbobank" class="form-control">
                      <option value="<?php echo $row_stock->id_bank ; ?>"><?php echo $row_stock->name_bank ; ?></option>
                    <?php
						foreach ($tampil_bank as $row_bank) :						
				  			 echo '<option value="'.$row_bank->id_bank.'">'.$row_bank->name_bank.'</option>';
						endforeach;
					?>                     
                      </select>
               </div>
          </div>    
       </td>
          <td>
             <div class="form-group">
                <label for="Engine" style="color:#900" class="col-xs-3">Curent Engine </label>
            
<div class="col-xs-6">
                 <input type="text" id="txtengine" name="txtengine" 
                 class="form-control"  value="<?php echo $row_stock->engine ; ?>"   
                 readonly="readonly"   />
                </div>
            </div>      
        </td>       
    </tr>        
    <tr>
    	<td>
        	<div class="form-group">
                 <label for="Stock Status " class="col-xs-3"  style="color:#900" >Stock Status *</label>
                  <div class="col-xs-5">
       			    <select  name="cbo_demo_unit" class="form-control">
					    
					<?php if($row_stock->flag_demo_unit == "0"): ?>
					   <option value="0" >STOCK UNIT</option>					
                    <?php else: ?>  
					   <option value="1" >DEMO CAR</option>
                    <?php  endif;?>
                 	   <option value="1">DEMO CAR</option>
                    </select>
                    
                 </div>
            </div>    
        </td>
        <td>
        	
        	<div class="form-group">
                <label for="Engine" style="color:#900" class="col-xs-3">Engine Update*</label>
                <div class="col-xs-5">
                 <input type="text" id="txtengineupdate" name="txtengineupdate" 
                 class="form-control"  value="<?php echo $row_stock->engine ; ?>" 
                 style="color:#900"  placeholder="Engine"   />
                </div>
            </div>      
                                	
        </td>
    </tr>    
    <tr>
    	<td>
        	
        	   <div class="form-group">
             <label for="Price Unit" style="color:#900" class="col-xs-3">DPP*</label>
             <table class="col-xs-6">
               <tr>  
                    <td class="col-xs-4" >             
                     <input  maxlength="20" id="curr" name="curr" class="form-control"  value="<?php echo  $row_stock->currency;  ?>" readonly   />
                 </td>
                 <td>  
 <input  maxlength="70" id="price_unit" name="price_unit" class="cost_price form-control"  style="color:#A6090D" value="<?php echo  number_format($row_stock->dpp,2,'.',',');  ?>"  />
              	  </td>          
           		</tr>   
             </table>  
            </div>       
        </td>
        <td>
        
        	 <div class="form-group">
                <label for="CSI"  class="col-xs-3" style="color:#900">Invoice  B.I</label>
             <table width="50%">
               <tr>  
                    <td class="col-xs-4" >
                                 <input type="text" id="txtcsiyear" name="txtcsiyear" class="form-control" onkeypress="return isNumberKey(event)"  placeholder="2 digit" maxlength="2"
                                    value="<?php
						            $str_pecahkiri= explode("/",$row_stock->csi) ;
						            echo $str_pecahkiri[0] ; 
						 		?>"
                                 />
                    </td>
                    <td >
                     <label style="color:#900;font-size:16px;">/</label>
                   </td>
                    
                    <td class="col-xs-12" >
                         <input type="text" id="txtcsi" name="txtcsi" class="form-control"  placeholder="Invoice B.I"
                         value="<?php
						            $str_pecahkanan = explode("/",$row_stock->csi) ;
						            echo $str_pecahkanan[1] ; 
						 		?>"
                          />
                    </td>
               </tr>     
               </table>
          </div>    
        
          
        </td>   
         <tr>
    	  <td>
           <div class="form-group">
          <label for="Others" class="col-xs-3" style="color:#007E0B">VAT/ PPN</label>
          <table class="col-xs-5">
           <tr>  
            <td class="col-xs-9" >  
          
         <input  maxlength="70" id="txtvat" name="txtvat" class="vat form-control"  
         style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00"  
         value="<?php echo  number_format($row_stock->vat,2,'.',',');  ?>"  readonly  />
            </td>
            </tr>  
           </table>
         </div>
       </td>
           <td>
           	      		         	        	
                <div class="form-group">
         			 <label for="Payment Date To B.I" style="color:#900"  class="col-xs-3">Payment Date To B.I *</label>
         			<div class="col-xs-4">
          		 <input type="text" id="txtdatetommi" name="txtdatetommi"  data-date-format="dd-mm-yyyy" 
               class="form-control datepicker" readonly value="<?php if ($row_stock->pay_date_bi !="") :				 
							      echo date('d-m-Y',strtotime($row_stock->pay_date_bi)) ; 
						    else :
						         echo "";
						    endif   				 
				 	        ?>">
               </div>
            </div>              
           </td>
         <tr>
    	<td> 
          <div class="form-group">
          <label for="Others" class="col-xs-3" style="color:#007E0B">PPH 22</label>
           <div class="col-xs-5">
         <input  maxlength="70" id="txtother" name="txtpph" class="pph form-control"  
         style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00"  
         value="<?php echo  number_format($row_stock->pph22,2,'.',',');  ?>"  />
           </div>
         </div>             
        </td>
        <td>
              <div class="form-group">
                 <label for="txtyear"  class="col-xs-3">Year</label>
                <div class="col-xs-3">
                <input name="txtyear" type="text" class="form-control" id="txtyear"  
                placeholder="Year" onkeypress="return isNumberKey(event)"  
                maxlength="4" value="<?php echo $row_stock->year ; ?>" />
                </div>
             </div>  
        </td>
     </tr> 
     
     <tr>
       <td>
          	<div class="form-group">
          <label for="Others" class="col-xs-3" style="color:#006">PPNBM : (Note)</label>
           <div class="col-xs-5">
         <input  maxlength="70" id="txtppnbm" name="txtppnbm" class="ppnbm form-control"  
         style="color:#A6090D" onkeypress="return isNumberKey(event)"  
         placeholder="0.00" value="<?php echo  number_format($row_stock->ppnbm,2,'.',',');  ?>"  />
           </div>
         </div>                                  
       </td>
       <td>
       		<div class="form-group">
           <label for="Unit Price" style="color:#900" class="col-xs-3">Price  Off Road</label>
           <div class="col-xs-5">
            <input  maxlength="70" id="price_offroad" name="price_offroad" 
            class="price_offroad form-control"  style="color:#A6090D" 
            onkeypress="return isNumberKey(event)"  placeholder="0.00" 
            value="<?php echo  number_format($row_stock->price_offroad,2,'.',',');  ?>"  />
           </div>   
        </div>
       </td>
       </tr>
     <tr>
       <td>
           <div class="form-group">
         <label for="total" class="col-xs-3" style="color:#007E0B">TOTAL </label>
         <div class="col-xs-5">
           <input  maxlength="70" id="txttotal" name="txttotal" class="total form-control"  
           style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" 
           value="<?php echo  number_format($row_stock->total_dpp_vat,2,'.',','); ?>" readonly />
           </div>
         </div>  
       </td>
       <td>
       <div class="form-group">
                 <label for="txtyear" class="col-xs-3" >Remarks </label>
                  <div class="col-xs-7">
       			     <textarea id="txtremarks" class="form-control" name="txtremarks" ><?php echo $row_stock->remarks_stock ;?>  </textarea>
                </div>
         </div>             
       </td>
       </tr> 
          
          
          
    <?php endforeach ; ?> 				 			 
  <?php endif ;?> 		    
   </table>    
     
     <div class="panel-body">                         
         <table>
           <tr>
              <td>                     
                 <input id="btnsaveedit" name="btnsavebtnsaveedit" type="submit" value="save"  class="btn btn-danger " />                
              </td>
               <td>&nbsp;</td>                   
               <td>&nbsp;  </td>
            <td>                                      			
            <a href="<?php echo base_url('bmw_stock_sales/c_slsstock_bmw/c_slsstock_bmw');?>" style="text-decoration:none;"><input id="btnback" name="btnback" type="button" value="Back To Table" class="btn btn-warning" /> </a>	                  
            </td>
          </tr>
         
       </table>    
    </div>
                                            	                     					           			 
 		
		<?php form_close(); ?> 
      
  </div> <!-- /panel -->             
</div>  <!-- /div class="col-lg-12 -->   
 
 <?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>    
 
<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 


<script type="text/javascript">
//--------------function number only
 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	

//----------------set focus----
 $(function() {
  $("#txtpayfrom").focus();    
});	

</script>

<script type="text/javascript">
//automatic multiple combo.

var htmlobjek;
$(document).ready(function(){  
	
	  $("#cbotype").change(function(){
		var cbottype_val = $("#cbotype").val();
		var url = '<?php echo site_url("bmw_stock_sales/c_slsstock_bmw/c_crud_stock_bmw/select_warna_combo_jquery"); ?>';
		
		$.ajax({
			type:'POST',
			url: url,		
			data:{id_type_post:cbottype_val},	
			cache: false,          
			success: function(msg){            
				$("#cbowarna").html(msg);							
			}
		});
	  });	  	   
	    
});

</script>

<script type="text/javascript">
//automatic show price 

var htmlobjek;
$(document).ready(function(){  
	 $("#cbowarna").change(function(){
			var colour_id= $("#cbowarna").val();
			var cbottype_price = $("#cbotype").val();
			var url = '<?php echo site_url("bmw_stock_sales/c_slsstock_bmw/c_crud_stock_bmw/doget_interior_colour_jquery"); ?>';
			//alert(colour_id); 
			//alert(cbottype_price);   
			 
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{id_type_post:cbottype_price,id_colour_post:colour_id},	
				cache: false,          
				success: function(data, textStatus, jqXHR){  	          
					$('[name="txtinterior"]').val(data.interior_colour);												
				}
			});
		  });
});	  
</script>

<script type="text/javascript">
//automatic show price 

var htmlobjek;
$(document).ready(function(){  
	 $("#cbotype").change(function(){
			var cbottype_price = $("#cbotype").val();
			var url = '<?php echo site_url("bmw_stock_sales/c_slsstock_bmw/c_crud_stock_bmw/doget_price_unit_jquery"); ?>';
			
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{id_type_post:cbottype_price},	
				cache: false,          
				success: function(data, textStatus, jqXHR){            
					$('[name="curr"]').val(data.currency);	
				    $('[name="txtyear"]').val(data.production);				
					$('[name="price_unit"]').val(numeral(data.price_cost).format('0,0.00'));			
			   var cost_price= numeral().unformat($('.cost_price').val()); 			  
			   var pph=numeral().unformat($('.pph').val());
			  
			   var vatcalculate  = cost_price * '0.1';	
			   var hasil_total = cost_price+vatcalculate + pph ; 
			   
			   $('.vat').val(numeral(vatcalculate).format('0,0.00'));
			   $('.total').val(numeral(hasil_total).format('0,0.00'));
				 
				}
			});
		  });
});	  
</script>

<script type="text/javascript">
$(function(){		
     $('body').delegate('.cost_price','change',function(){  		
	       var cost_price= numeral().unformat($('.cost_price').val()); 			
		   var vat=numeral().unformat($('.vat').val());
		   var pph=numeral().unformat($('.pph').val());
		  
	       var vatcalculate  = cost_price * '0.1';	
		   var hasil_total = cost_price + vatcalculate + pph ; 
		   	
		   $('.vat').val(vatcalculate);			   
		   $('.total').val(hasil_total);
		 
 	 });
	 
	$('body').delegate('.cost_price','blur',function(){  		   
		   var cost_price=$('.cost_price').val();
		   var vat=$('.vat').val();
		   var pph=$('.pph').val();
		   var total=$('.total').val();
		  
		   $('.cost_price').val(numeral(cost_price).format('0,0.00')) ;
		   $('.vat').val(numeral(vat).format('0,0.00'));
		   $('.pph').val(numeral(pph).format('0,0.00'));
		   $('.total').val(numeral(total).format('0,0.00'));		
 	 });  	
});

$(function(){		
     $('body').delegate('.pph','change',function(){  		
	       var cost_price= numeral().unformat($('.cost_price').val()); 			
		   var vat=numeral().unformat($('.vat').val());
		   var pph=numeral().unformat($('.pph').val());
		  
	       var vatcalculate  = cost_price * '0.1';	
		   var hasil_total = cost_price + vatcalculate + pph ; 
		   	
		   $('.vat').val(vatcalculate);			   
		   $('.total').val(hasil_total);
		 
		  
 	 });
	 
	$('body').delegate('.pph','blur',function(){  		   
		   var cost_price=$('.cost_price').val();
		   var vat=$('.vat').val();
		   var pph=$('.pph').val();
		   var total=$('.total').val();
		  
		   $('.cost_price').val(numeral(cost_price).format('0,0.00')) ;
		   $('.vat').val(numeral(vat).format('0,0.00'));
		   $('.pph').val(numeral(pph).format('0,0.00'));
		   $('.total').val(numeral(total).format('0,0.00'));		
	});
	 
	 $('body').delegate('.ppnbm','blur',function(){  		   
		   var ppnbm=$('.ppnbm').val();
		   $('.ppnbm').val(numeral(ppnbm).format('0,0.00'));		
 	 }); 
	 
	 $('body').delegate('.price_offroad','blur',function(){  		   
		   var price_offroad=$('.price_offroad').val();
		   $('.price_offroad').val(numeral(price_offroad).format('0,0.00')) ;
		 	
    });  
	  	
});
</script>     