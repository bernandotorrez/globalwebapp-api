<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">



<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_fail') ;?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_insert_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_insert_fail');?>  
        </div>	
<?php } ?>



<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">                 
<div class="panel panel-default">
  <div class="panel-heading" id="label_head_panel" style="color:#B66F03; font-size:18px;">Add In Stock Unit Vehicle PT.<?php echo $this->session->userdata('company'); ?></div>                 
     <?php  echo form_open_multipart('maserati_stock_sales/c_slsstock_mas/c_crud_stock_mas/multiple_submit',array('class'=>'form-multi'));  ?> 
  
       <!-- Responsive Form for Mobile and Web Start -->
       <div class="container">

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Stock Number</label>

               <input type="text" id="txtnostock" name="txtnostock" class="form-control" 
               readonly value="<?php echo $stocknumber ;?>"  />
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Branch</label>

               <input type="text" id="txtbranch" name="txtbranch" class="form-control"  
               value="<?php echo $this->session->userdata('name_branch'); ?>"  readonly />
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Location Stock</label>

               <select id="cboloc" name="cboloc"  class="form-control">
                   <!--  <option> <?php //echo $this->session->userdata('name_branch') ;?></option> -->
	               <?php foreach($tampil_lokasi as $row){ ?>                         
       		         <option ><?php echo $row->name_loc ;?></option>
                   <?php } ?>  
               </select>
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Stock keeper</label>

               <input type="text" id="txtuser" name="txtuser" class="form-control" 
               value="<?php echo $this->session->userdata('name');  ?>"  readonly />
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">VIN *</label>

               <input name="txtvin" type="text" class="form-control" id="txtvin"  
               placeholder="Vin"  maxlength="40"  />
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Type *</label>

               <select id="cbotype" name="cbotype" class="form-control">                 	
               <option value="">--Choose--</option>	 
                    <?php
                      foreach ($tampil_type_vehicle as $row_type) :						
                        echo '<option value="'.$row_type->id_type.'">'.$row_type->desc_type.'</option>';
                      endforeach;
                    ?>
                 </select>
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Invoice Date*</label>

               <input type="text" id="cbointake" name="cbointake"  data-date-format="dd-mm-yyyy" 
               class="form-control datepicker" readonly>
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Colour *</label>

               <select id="cbowarna" name="cbowarna" class="form-control">                 	
                <option value="">--Choose--</option>	
                                  
                 </select>   
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Receive Date Unit</label>

               <input type="text" id="txtdatericived" name="txtdatericived"  
                 data-date-format="dd-mm-yyyy" class="form-control datepicker" readonly>
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Colour Interior</label>

               <input type="text" id="txtinterior" name="txtinterior" 
                     class="form-control" readonly />   
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Bank Facility</label>

               <select id="cbobank" name="cbobank" class="form-control">                    
                    <?php
                      foreach ($tampil_bank as $row_bank) :						
                          echo '<option value="'.$row_bank->id_bank.'">'.$row_bank->name_bank.'</option>';
                      endforeach;
                    ?>                     
                </select>
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Engine *</label>

               <input type="text" id="txtengine" name="txtengine" 
                 class="form-control"  placeholder="Engine"   />
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900" >Stock Status</label>

               <select  name="cbo_demo_unit" class = "form-control" > 
                  	<option value="0">STOCK UNIT</option>
                    <option value="1">DEMO CAR</option>
                  </select>
             </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-2">
             <div class="form-group">
               <label class="control-label" style="color:#900">Invoice  B.I</label>

               <input type="text" id="txtcsiyear" name="txtcsiyear" 
               class="form-control" onkeypress="return isNumberKey(event)"  
               placeholder="2 digit" maxlength="2"/>
                      
             </div>
           </div>

           <div class="col-sm-1">
             
           </div>

           <div class="col-sm-2">
             <div class="form-group">
               <label class="control-label" style="color:#900">/</label>

               <input type="text" id="txtcsi" name="txtcsi" 
               class="form-control"  placeholder="Invoice B.I" />

             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
           <div class="form-group">
            <label class="control-label" style="color:#900">DPP *</label>

            <div class="input-group">
              <span class="input-group-addon">
                <label id="txt_curr"> </label>
              </span>
              
              <input type="hidden"  maxlength="20" id="curr" name="curr" 
              class="form-control" readonly  />
              <input  maxlength="70" id="price_unit" name="price_unit" 
              class="cost_price form-control"  style="color:#A6090D"  />
            </div>
          </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Payment Date To B.I*</label>

               <input type="text" id="txtdatetommi" name="txtdatetommi"  
               data-date-format="dd-mm-yyyy" class="form-control datepicker" readonly>
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
           <div class="form-group">
            <label class="control-label" style="color:#007E0B">VAT/ PPN</label>

            <input  maxlength="70" id="txtvat" name="txtvat" class="vat form-control"  
            style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00"   />

          </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Year</label>

               <input name="txtyear" type="text" class="form-control" id="txtyear"  
                placeholder="Year" onkeypress="return isNumberKey(event)"  maxlength="4"  />
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
           <div class="form-group">
            <label class="control-label" style="color:#007E0B">PPH 22</label>

            <input  maxlength="70" id="txtother" name="txtpph" class="pph form-control"  
         style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00"  />

          </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#006">PPNBM : (Note)</label>

               <input  maxlength="70" id="txtppnbm" name="txtppnbm" class="ppnbm form-control"  
         style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00"  />
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
           <div class="form-group">
            <label class="control-label" style="color:#007E0B">TOTAL</label>

            <input  maxlength="70" id="txttotal" name="txttotal" class="total form-control"  
           style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" readonly />

          </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label" style="color:#900">Price  Off Road</label>

               <input  maxlength="70" id="price_offroad" name="price_offroad" 
           class="form-control price_offroad" style="color:#A6090D"  placeholder="0.00" />   
             </div>
           </div>

         </div>
         <!-- Row End -->

         <!-- Row Start -->
         <div class="row">
           <div class="col-sm-5">
           <div class="form-group">
            <label class="control-label" >Remarks</label>

            <textarea id="txtremarks" class="form-control" name="txtremarks" ></textarea>

          </div>
           </div>

           <div class="col-sm-1">


           </div>

           <div class="col-sm-5">
             <div class="form-group">
               <label class="control-label">Upload VIN/Engine Unit</label>

               <input type="file" id="txtupload" name="userfile" class="btn btn-success"/>
             </div>
           </div>

         </div>
         <!-- Row End -->

       </div>
       <!-- Responsive Form for Mobile and Web End -->
      
       <div class="form-group text-center">
                <button id="btnsave" name="btnsave" value="Save" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
                    <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                    Save
                </button>


                <a href="<?php echo base_url('maserati_stock_sales/c_slsstock_mas/c_slsstock_mas');?>" class="btn btn-app btn-danger btn-xs radius-4">
                    <i class="ace-icon fa fa-close bigger-160"></i>
                    Cancel
                </a>

            </div>
                                            	                     					           			 
 		
		<?php form_close(); ?> 
      
   </div> <!-- /panel -->             
</div>  <!-- /div class="col-lg-12 -->   
 
 <?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>    
 
<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- jQuery_ui -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->

<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 


<script type="text/javascript">
//--------------function number only
 
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	
//--------------function back only 
 
function isbackspacerKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if ( charCode > 31) {
			return false;
		} else {
			return true;
		}      
}	


//----------------set focus----
 $(function() {
  $("#txtpayfrom").focus();    
});	

</script>

<script type="text/javascript">
//automatic multiple combo.

var htmlobjek;
$(document).ready(function(){  
	
	  $("#cbotype").change(function(){
		var cbottype_val = $("#cbotype").val();
		var url = '<?php echo site_url("maserati_stock_sales/c_slsstock_mas/c_crud_stock_mas/select_warna_combo_jquery"); ?>';
		
		$.ajax({
			type:'POST',
			url: url,		
			data:{id_type_post:cbottype_val},	
			cache: false,          
			success: function(msg){            
				$("#cbowarna").html(msg);							
			}
		});
	  });	  	   
	    
});

</script>

<script type="text/javascript">
//automatic show price 

var htmlobjek;
$(document).ready(function(){  
	 $("#cbowarna").change(function(){
			var colour_id= $("#cbowarna").val();
			var cbottype_price = $("#cbotype").val();
			var url = '<?php echo site_url("maserati_stock_sales/c_slsstock_mas/c_crud_stock_mas/doget_interior_colour_jquery"); ?>';
			//alert(colour_id); 
			//alert(cbottype_price);   
			 
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{id_type_post:cbottype_price,id_colour_post:colour_id},	
				cache: false,          
				success: function(data, textStatus, jqXHR){  	          
					$('[name="txtinterior"]').val(data.interior_colour);												
				}
			});
		  });
});	  
</script>

<script type="text/javascript">
//automatic show price 

var htmlobjek;
$(document).ready(function(){  
	 $("#cbotype").change(function(){
			var cbottype_price = $("#cbotype").val();
			var url = '<?php echo site_url("maserati_stock_sales/c_slsstock_mas/c_crud_stock_mas/doget_price_unit_jquery"); ?>';
			
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{id_type_post:cbottype_price},	
				cache: false,          
				success: function(data, textStatus, jqXHR){            
					$('[name="curr"]').val(data.currency);	
				    $('[name="txtyear"]').val(data.production);				
					$('[name="price_unit"]').val(numeral(data.price_cost).format('0,0.00'));			
			   var cost_price= numeral().unformat($('.cost_price').val()); 			  
			   var pph=numeral().unformat($('.pph').val());
			  
			   var vatcalculate  = cost_price * '0.1';	
			   var hasil_total = cost_price+vatcalculate + pph ; 
			   
			   $('.vat').val(numeral(vatcalculate).format('0,0.00'));
			   $('.total').val(numeral(hasil_total).format('0,0.00'));
				 
				}
			});
		  });
});	  
</script>

<script type="text/javascript">
$(function(){		
     $('body').delegate('.cost_price','change',function(){  		
	       var cost_price= numeral().unformat($('.cost_price').val()); 			
		   var vat=numeral().unformat($('.vat').val());
		   var pph=numeral().unformat($('.pph').val());
		  
	       var vatcalculate  = cost_price * '0.1';	
		   var hasil_total = cost_price + vatcalculate + pph ; 
		   	
		   $('.vat').val(vatcalculate);			   
		   $('.total').val(hasil_total);
		 
 	 });
	 
	$('body').delegate('.cost_price','blur',function(){  		   
		   var cost_price=$('.cost_price').val();
		   var vat=$('.vat').val();
		   var pph=$('.pph').val();
		   var total=$('.total').val();
		  
		   $('.cost_price').val(numeral(cost_price).format('0,0.00')) ;
		   $('.vat').val(numeral(vat).format('0,0.00'));
		   $('.pph').val(numeral(pph).format('0,0.00'));
		   $('.total').val(numeral(total).format('0,0.00'));		
 	 });  	
});

$(function(){		
     $('body').delegate('.pph','change',function(){  		
	       var cost_price= numeral().unformat($('.cost_price').val()); 			
		   var vat=numeral().unformat($('.vat').val());
		   var pph=numeral().unformat($('.pph').val());
		  
	       var vatcalculate  = cost_price * '0.1';	
		   var hasil_total = cost_price + vatcalculate + pph ; 
		   	
		   $('.vat').val(vatcalculate);			   
		   $('.total').val(hasil_total);
		 
		  
 	 });
	 
	$('body').delegate('.pph','blur',function(){  		   
		   var cost_price=$('.cost_price').val();
		   var vat=$('.vat').val();
		   var pph=$('.pph').val();
		   var total=$('.total').val();
		  
		   $('.cost_price').val(numeral(cost_price).format('0,0.00')) ;
		   $('.vat').val(numeral(vat).format('0,0.00'));
		   $('.pph').val(numeral(pph).format('0,0.00'));
		   $('.total').val(numeral(total).format('0,0.00'));		
	});
	 
	 $('body').delegate('.ppnbm','blur',function(){  		   
		   var ppnbm=$('.ppnbm').val();
		   $('.ppnbm').val(numeral(ppnbm).format('0,0.00'));		
 	 }); 
	  	
	$('body').delegate('.price_offroad','blur',function(){  		   
		   var price_offroad=$('.price_offroad').val();
		   $('.price_offroad').val(numeral(price_offroad).format('0,0.00')) ;
		 	
    });  
  	
});

	
</script>     