<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js');?>" ></script>

<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<script>

// fungsi untik checklist semua otomatis
function CheckAll()
{
    l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = true;		
		}	
		document.getElementById('btnedit').disabled = true;	
		document.getElementById('btndel').disabled = false;	
		document.getElementById('btnsendtab').disabled = true;	
}

function UncheckAll()
{
   l =document.forms[0].length;
		for(i=0;i<l;i++) 	
		{
		 document.forms[0].elements[i].checked = false;			 
		}
		document.getElementById('btnedit').disabled = true;
		document.getElementById('btndel').disabled = true;
		document.getElementById('btnsendtab').disabled = true;
}


//--------------------function disbaled enabled button with check book

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_edit').removeAttr('disabled');			
        }
        else{
            $('.btn_edit').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length == 1){
            $('.btn_send').removeAttr('disabled');			
        }
        else{
            $('.btn_send').attr('disabled', 'disabled');								
        }
    });
});

$(function() {  //.btn hnya untuk tombol delete yang disabled.
    $('input[type="checkbox"]').change(function(){
        if($('input[type="checkbox"]:checked').length >= 1){
            $('.btn_delete').removeAttr('disabled');			
        }
        else{
            $('.btn_delete').attr('disabled', 'disabled');								
        }
    });
});

//-------------------------------------------------------------------------		 	

//-------------------------------------------------------------------------		 	
  
//set focus ----------------------------
$(function() {
  $("#txtcari").focus();
});		  
//set focus ----------------------------  
</script>

 <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
 
<?php 
	If ( $this->session->flashdata('pesan') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan');	?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_succes') != ""){ ?>     
        <div class="alert alert-info" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_succes');	?>  
        </div>	
<?php } ?>
      
<?php   	
	If ($this->session->flashdata('pesan_aproval') == "1"){ ?>      
		  <div class="alert alert-info" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Info:</span>                        
                <?php  echo "Send Approval Successfully"	?>  
		  </div>		
<?php } ?>

<?php   	
	If ($this->session->flashdata('pesan_aproval') == "0"){ ?>                
		<div class="alert alert-danger" role="alert">
			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			<span class="sr-only">info:</span>                        
			<?php  echo "Send Approval failed"	?>  
        </div>		      
<?php } ?>	
  
<div class="panel panel-default">
  <div class="panel-heading"  id="label_head_panel" style="background:url(<?php echo base_url('asset/images/child-panel-bg.png')?>);color:#B66F03; font-size:16px;" ><?php echo "Controll Selling Unit "." | ".$header ; ?> </div>
   <div class="panel-body">               
   
          
 <?php echo form_open('maserati_stock_sales/c_control_sell_mas/c_control_sell_mas/multiple_submit');  ?>
 
  
    <!-- Menu Button Start -->
			<div class="row text-right">
          <button id="btnrefresh" name="btnrefresh" class="btn btn-app btn-warning btn-xs radius-4" value="Refresh" type="submit">
            <i class="ace-icon fa fa-refresh bigger-160"></i>
              Refresh
          </button>

          <button id="btnrptexcel" name="btnrptexcel" class="btn btn-app btn-primary btn-xs radius-4 btnexcel" value="Report Excel" type="button">
            <i class="ace-icon fa fa-file-excel-o bigger-160"></i>
              Excel
          </button>   
				
			
			<!-- Menu Button End -->

      <!-- Search Menu End -->
      <table align="left">
     <tr>
     <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
     <td class="text-center"><label>Branch</label></td>
     <td>&nbsp;
       	
       </td>
      <td>
      <select name="cbobranchutama" id="cbobranchutama" class="form-control" > 
           <?php if( $str_aprovalflag == "3" and $str_idgroup == "headbmsal001") :?>             
             <option value="<?php echo $str_branch_id ?>"><?php echo $str_name_branch ?></option> 
           <?php else: ?>
               <option value="">--ALL--</option> 
               <?php foreach ($tampil_branch as $row_branch) : ?>	   			        
               <option value="<?php echo $row_branch->branch_id ?>"><?php echo $row_branch->place ?></option>
                <?php endforeach ; ?>         			         
             <?php endif; ?>         
       </td>
       <td>&nbsp;
       	
       </td>

        <td>
        <button id="btnshort" name="btnshort" class="btn btn-app btn-primary btn-xs radius-4" value="Short" type="submit">
                  <i class="ace-icon fa fa-search bigger-160"></i>
                   Short
                </button>  
       </td>
      </tr> 
    </table>

    <table align="left">
     <tr>
     <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
     <td class="text-center"><label>Select: </label></td>
     <td>&nbsp;
       	
       </td>
      <td>
      <select name="cbostatus" id="cbostatus" class="form-control">             
             <option value="vin">VIN</option>
             <option value="engine">ENGINE</option>
             <option value="stock_no">STOCK NO</option>            
             <option value="csi">CSI</option>
             <option value="desc_type">TYPE</option>
             <option value="location_stock">LOCATION</option>             
       </select> 
       </td>
       <td>&nbsp;
       	
       </td>

       <td>
       <input type="text" size="15" name="txtcari" class="form-control" placeholder="Search" id="txtcari" value="<?php echo $this->session->flashdata('cari'); ?>" > 
        
       </td>

        <td>
        <button id="btncari" name="btncari" class="btn btn-app btn-primary btn-xs radius-4" value="Search" type="submit">
                  <i class="ace-icon fa fa-search bigger-160"></i>
                   Search
                </button>  
       </td>
      </tr> 
    </table>

    <table align="left">
     <tr>
     <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
     <td class="text-center"><label>Date: </label></td>
     <td>&nbsp;
       	
       </td>
      <td>
      <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
            <input id="txtdatestart" size="15" name="txtdatestart" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />  
       </td>
       <td>&nbsp;
       	
       </td>
       <td class="text-center"><label>S / D </label></td>
       <td>&nbsp;
       	
       </td>
      <td>
      <div class="date" data-date="" data-date-format="dd-mm-yyyy" data-link-field="dtp_input2" data-link-format="dd-mm-yyyy">
          <div class="input-group">   
            <input id="txtdateend" size="15" name="txtdateend" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />                              	         
            
          </div>     
         </div> 
       </td>
       <td>&nbsp;
       	
       </td>
       <td>
       <button id="btncaridate" name="btncaridate" class="btn btn-app btn-primary btn-xs radius-4" value="Search" type="submit">
                  <i class="ace-icon fa fa-search bigger-160"></i>
                   Search
                </button>  
       </td>
      </tr> 
    </table>
    </div>

    <div class="space-12"></div>
    <!-- Search Menu End -->


    <!-- Table Responsive -->
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>SPK No</th>
            <th>Branch</th>
            <th>Type</th>
            <th>Vin</th>
            <th>Engine</th>
            <th>Colour</th>
            <th>Year</th>
            <th>Sales</th>
            <th>Customer</th>
            <th>Curr</th>
            <th>Price Off Road</th>
            <th>BBN</th>
            <th>Price ON Road</th>
            <th>Total Discount</th>
            <th>Price After Disc</th>
            <th>Total Pay</th>
            <th>Remain Pay</th>
            <th>Due Date spk</th>
            <th>Faktur</th>
            <th>View Detail</th>

          </tr>
        </thead>
        <tbody>
          <?php if ($ceck_row >= 1) :		  
		foreach($selling_view as $row):		 		 
			if ($row->status_flag_sell == "2") : 	
				echo '<tr style="font-size:11px;" class="btn-success">';  	
			else:					  
				echo '<tr style="font-size:11px;" class="btn-info">'; 		   								
			endif;

			
		   $str_balanced = "0.00";			  
		   $str_flag_sel = "2" ;
		   
		   if ($row->status_flag_sell == $str_flag_sel && $row->remaining_amount_customer == $str_balanced ) : 		     
			  echo '<tr style="font-size:11px;" class="btn-warning">'; 
		   endif;	  
		   		 
		    if ($row->date_dudate_spk <= date('Y-m-d')) :
				   if ($row->status_flag_sell == "2") : 		
					    echo '<tr style="font-size:11px;" class="btn-danger">' ;		
					endif;
			endif;		
							 
			
		 
     ?>                              		                              
                <td  width="3%" align="center"><?php echo $row->nospk ;?></td>
                <td  width="3%" align="center"><?php echo $row->name_branch ; ?></td>		
                <td  width="5%" align="center"><?php echo $row->desc_type ; ?></td>		
                <td  width="3%" align="center"><?php echo $row->vin ; ?></td>	
                <td  width="3%" align="center"><?php echo $row->engine ; ?></td>	
                <td  width="5%" align="center"><?php echo $row->colour ; ?></td> 
                <td  width="3%" align="center"><?php echo $row->year ; ?></td>               								
                <td  width="3%" align="center"><?php echo $row->sal_name ; ?></td>                       
                <td  width="3%" align="center"><?php echo $row->cust_name ; ?></td>    
                <td  width="3%" align="center" ><?php echo $row->curr_sell ; ?></td> 
                <td  width="6%" align="center" ><?php echo number_format($row->price_curent,2,'.',',') ; ?></td> 
                <td  width="6%" align="center" ><?php echo number_format($row->bbn,2,'.',',') ; ?></td> 
                <td  width="6%" align="center" ><?php echo number_format($row->price_onroad,2,'.',',') ; ?></td> 
                <td  width="6%" align="center" >
				<?php 
					$discount  = $row->discount ;
					$opt_kacfilm  = $row->opt_kacfilm ; 
					$opt_pdi  = $row->opt_pdi ;
					$opt_poles  = $row->opt_poles ; 
					$opt_surtjalan  = $row->opt_surtjalan ; 
					$opt_other  = $row->opt_other ;
					$suport_disc  = $row->suport_disc ; 				    
					$total_discount = $discount + $opt_kacfilm + $opt_pdi + $opt_poles + $opt_surtjalan + $opt_other +$suport_disc;						
					echo number_format($total_discount,2,'.',','); 
				?>
                </td> 
                
                <td  width="6%" align="center" >
				<?php 
					$discount  = $row->discount ;
					$opt_kacfilm  = $row->opt_kacfilm ; 
					$opt_pdi  = $row->opt_pdi ;
					$opt_poles  = $row->opt_poles ; 
					$opt_surtjalan  = $row->opt_surtjalan ; 
					$opt_other  = $row->opt_other ;
					$suport_disc  = $row->suport_disc ; 
				    
					$total_discount = $discount + $opt_kacfilm + $opt_pdi + $opt_poles + $opt_surtjalan + $opt_other +$suport_disc;
					$str_ontheroad	= $row->price_onroad ;
					$hasil_akhir = $str_ontheroad - $total_discount; 
					echo number_format($hasil_akhir,2,'.',','); 
				?>
                </td> 
                <td  width="6%" align="center" ><?php echo number_format($row->total_paymet_customer) ; ?></td>               
                <td width="6%"  align="center" >
				 <?php 
						
					  $str_balanced = "0.00";			  
		   			  $str_flag_sel = "2" ;
					  $str_flag_sold = "0" ;
		             if ($row->status_flag_sell == $str_flag_sel && $row->remaining_amount_customer == $str_balanced ) : 		     
			             echo '<label>'."Pays Off But Not Approval".'</label>'; 
					 else:
					 	 if ($row->status_flag_sell == $str_flag_sold && $row->remaining_amount_customer == $str_balanced ) : 		
					         echo '<label>'."SOLD".'</label>'; 
					     else:	
						 	if ($row->date_dudate_spk <= date('Y-m-d')) :
					     		if ($row->status_flag_sell == "2") : 		
							 		echo 'DUE DATE' ." " ;			 													 	    	
								endif;
					 		 endif;	
							 echo number_format($row->remaining_amount_customer,2,'.',',') ;  	   
						 endif;		
		             endif;							 									 												
				 ?>
                </td>    
                
                
                <td width="5%" align="center">
					<?php 
						echo date('d-m-Y', strtotime($row->date_dudate_spk)) ;
					?>
                </td>   
                
            <?php  if ($row->flag_upload_faktur == "1") { ?>
                 <td align="center" ><a href="<?php echo base_url($row->path_up_fatktur); ?>" style="color:blue;text-decoration:none;"><span class='glyphicon glyphicon-search' aria-hidden='true'></span>View Faktur</a>
		      	</td>  
            <?php }else{  ?>  
                 <td align="center" >x</td>  
            <?php } ?>               
               <!-- <td width="5%" align="center">
                	<?php 
						//echo date('d-m-Y', strtotime($row->date_transaksi)) ; 
					?>
                </td> -->             
                            
                <td width="4%" align="center" >
                	<a href="#" class="btn btn-primary btn-setting print" id="print" style="text-decoration:none" req_id="<?php echo $row->id_trans_sales ; ?>">Detail</a>
                </td>  
                                           
		  </tr> 	           
		 <?php 		    
		  endforeach;
 	   endif;  				
		 ?> 

<?php form_close(); ?> 
        </tbody>
      </table>
    </div>
    <!-- Table Responsive -->
 		
  
         <table  class=" table-bordered">         
         <tr>
         	<td class="btn-success"><?php echo 'TOTAL STILL BOKED'." : ".$count_row_dp."  "  ?></td>
            <td class="btn-info"> <?php echo 'TOTAL SELLING'." : ".$count_row_selling."  "  ?></td>
         </tr>
         </table>     
         
         
		<?php	
		     //echo '<label style="color:#900">'."TOTAL STOCK : ".$ceck_row.'</label>';		   			
		     if (! empty($pagination))			
			 { 
		  	   echo '<center><table>';
			   echo '<tr>';
			   echo '<td>';
			   echo '<div class="row">';
			   echo '<div class="col-md-12 text-center">'.$pagination.'</div>';
			   echo '</div>';
			   echo '</td>';
			   echo '</tr>';
			   echo '</table></center>';	
			  	 
			 }			  	  						
		?>
        
 </div>  
</div> 
      
          
  </div>
 </div>
   
</div>

<!-- Modal For Detail -->
 
      <div class="modal fade" id="ViewDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Selling Unit Info</h4>
                    </div>
                    
                    <div class="modal-body">
                         <!-- body di kosongin buat data yg akan di tampilkan dari database -->            
                    </div> 
                    
                     <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
       
<!-- Modal -->  	

  
  <!-- Modal Buat save and edit -->
    <div id="tampilanform"></div>
        <div class="modal fade" id="Mod-Report-excel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Export To Excel</h4>
                    </div>
                    
                    <div class="modal-body-add">
                   <div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0"> 
                   <br />  
                     <table  class="table-striped  table-hover table-condensed table-bordered" width="100%" style="font-size:12px" >					                   	  
					 <tr>
                          <td style="color:#900"><label for="Date start"  class="col-xs-11">Date Alocation Start * :</label></td>
                          <td>
                           <div class="control-group">
                            <div class="col-xs-11">
                          	<input id="txtrptdtstart" name="txtrptdtstart" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
                            </div>
                           </div> 
                          </td>
                       </tr>
                        <tr>
                          <td style="color:#900"><label for="Date End"  class="col-xs-11" >Date Alocation End *  :</label></td>
                          <td>
                          <div class="control-group">
                            <div class="col-xs-11">
                          	<input id="txtrptdtend" name="txtrptdtend" readonly type='text' data-date-format="dd-mm-yyyy" class="form-control datepicker" />
                            </div>
                           </div>
                          </td>
                         </tr> 
                      <tr>
                      <td ><label for="Date End"  class="col-xs-11" >Branch :</label></td>
					  <td>
                       <div class="control-group">
                         <div class="col-xs-11">
                          <select name="cbobranch" id="cbobranch" class="form-control" > 
                           <option value="">--ALL--</option>   
                               <?php foreach ($tampil_branch as $row_branch) : ?>	   			        
                            		<option value="<?php echo $row_branch->branch_id ?>"><?php echo $row_branch->place ?></option>
                                <?php endforeach ; ?>         			         
                             </select> 
                             </div>
                           </div>  
                        </td>
					  </tr>	 
                     
                     <tr>
                      <td ><label for="Date End"  class="col-xs-11" >Type / Model</label></td>
					  <td>
                       <div class="control-group">
                         <div class="col-xs-11">
                          <select name="cbotype" id="cbotype" class="form-control" > 
                           <option value="">--ALL--</option>   
                               <?php foreach ($tampil_type as $row_type) : ?>	   			        
                            		<option value="<?php echo $row_type->id_type ?>"><?php echo $row_type->desc_type ?></option>
                                <?php endforeach ; ?>         			         
                             </select> 
                             </div>
                            </div> 
                        </td>
					  </tr>	 			  		
					  </table>	  
                      </div>  		   
                    </div>                    
                     <div class="modal-footer">
                      <br />
                        <input type="submit" name="btnexport" id="btnexport" value="Submit " class="btn btn-primary hide-when-done" ata-dismiss="modal"/>
                     
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                       
                    </div>                  
                </div>
            </div>
        </div>
<!-- Modal Buat edit -->  	
 

<!-- jQuery Version 1.11.0 -->
<script src="<?php echo base_url() ?>assets/jquery-1.11.0.js"></script>

<!-- jQuery Version 1.11.3 from https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/jquery.min.js" charset="UTF-8"></script>

<!-- jQuery_ui -->
<script type="text/javascript" src="<?php echo base_url()?>asset/js/jquery-ui.min.js" charset="UTF-8"></script>

<!-- bootstrap Version 3.3.5 from http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js -->
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap.min.js" charset="UTF-8"></script>

<!-- numeral buat format angka -->
<script type="text/javascript" src="<?php echo base_url('asset/js/numeral.min.js');?>" ></script>
  
<!--file include Bootstrap js dan datepickerbootstrap.js-->
<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js"charset="UTF-8"></script>
<!-- Fungsi datepickier yang digunakan -->

<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 

<script>
 // Show modal pop up for edit and then get data table edit wrs to modal popup
$(function(){
  $(document).on('click','.btnexcel',function(e){						
		$("#Mod-Report-excel").modal('show');  			
   });	
});	

$(function(){
  $(document).on('click','.hide-when-done',function(e){						
		$("#Mod-Report-excel").modal('hide');  			
   });	
});	

</script>

<script>
//post dari modal popup
$(function(){
    $('#myform').on('submit', function(e){ 
        e.preventDefault();		
		var url = '<?php echo base_url("maserati_stock_sales/c_control_sell_mas/c_control_sell_mas/multiple_submit"); ?>';	 // url			
        $.ajax({
            url: url, //this is the submit URL
            type: 'POST', //or POST
            data: $('#myform').serialize(), // myform di buat untuk diferentsiasi dengan form open code igniter     
            success: function(data){               				 				
				
            }
        });
    });
}); 
</script>

<script>
//get data table remarks to modal popup.
$(function(){
  $(document).on('click','.print',function(e){
		var req_id = $(this).attr('req_id'); //atribut from <a ref 	php echo $row->id_master ;	
		var url = '<?php echo site_url("maserati_stock_sales/c_control_sell_mas/c_control_sell_mas/get_idtrans_modal"); ?>';
		
		$("#ViewDetailModal").modal('show');    		
		
		$.ajax({			
			type: 'POST',
			url: url,
			data: {id_trans_sales:req_id},
			success: function (msg) {
				$('.modal-body').html(msg);
			}
						
		});				
		
   });	
});
</script>