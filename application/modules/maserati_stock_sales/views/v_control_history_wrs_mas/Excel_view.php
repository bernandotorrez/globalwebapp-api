<?php 
	header("Content-type: application/octet-stream");
	header("Content-Disposition: attachment; filename=report_excel_stock_unit.xls");
	header("Pragma: no-cache");
	header("Expires: 0"); 	
?>
<?php 
		echo '<table width="100%" border="1"  >';		
		echo '<tr style=" color:#144b79;font-weight:bold;font-size: 12px; ">';		
		echo '<td width="8%"align="center">WRS Date</td>';
		echo '<td width="8%"align="center">Stock Number </td>';
		echo '<td width="5%" align="center">Branch</td>';													
		echo '<td width="9%" align="center">Type</td>' ;
		echo '<td width="3%" align="center">Vin</td>' ;
		echo '<td width="7%" align="center">Engine</td>' ;	
		echo '<td width="8%" align="center">Colour</td>' ;				
		echo '<td width="3%" align="center">CSI</td>' ;
		echo '<td width="6%" align="center" >Location Stock</td>';	
		echo '<td width="5%" align="center">Intake</td>' ;	
		echo '<td width="5%" align="center">Year</td>' ;												
		echo '</tr>' ;
?>			
	<?php if(!empty($data_excel)) : ?>
	  <?php foreach($data_excel as $item) {	?>
           <?php
					  if ($item['status']==0) : 	 
					     echo '<tr style=" background:#3C0">';	 
					  else:					    
						 echo '<tr style="background:#B4C9FA">';							
					  endif;
				?>       
                <td><?=$item['date_wrs_bodong'] ?></td>
                <td><?=$item['stock_no']?></td>
                <td><?=$item['name_branch']?></td>
                <td><?=$item['desc_type']?></td>
                <td><?=$item['vin']?></td>
                <td><?=$item['engine']?></td>                                        
                <td><?=$item['colour']?></td> 
                <td><?=$item['csi']?></td>  
                <td><?=$item['location_stock']?></td>               
                <td><?=$item['intake']?></td>
                <td><?=$item['year']?></td>                     
            </tr>
		<?php } ?>
     <?php endif ?>   
</table>
<table>
	<tr>
		<td style=" background:#B4C9FA">TOTAL WRS HISTORY: <?php echo $count_row_history_wrs ?></td>
    	<td style="background:#3C0">TOTAL WRS MOVE TO STOCK  : <?php echo $count_row_wrs_move_to_stock ?></td>
        <td style="background:#F18687">TOTAL ALL WRS  : <?php echo $count_row_all_wrs ?></td>    	
	</tr>    
 </table>   