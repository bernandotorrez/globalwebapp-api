<link href="<?php echo base_url()?>assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<!-- <link href="<?php // echo base_url()?>asset/css/jquery.fileupload-ui.css" rel="stylesheet" media="screen"> -->


<?php 
	If ( $this->session->flashdata('pesan_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo $this->session->flashdata('pesan_fail') ;?>  
        </div>	
<?php } ?>

<?php 
	If ( $this->session->flashdata('pesan_insert_fail') != ""){ ?>     
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Info:</span>                        
            <?php  echo  $this->session->flashdata('pesan_insert_fail');?>  
        </div>	
<?php } ?>



<div class="col-lg-12 col-lg-offset-0 col-md-12 col-md-offset-0 col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0">                 
<div class="panel panel-default ">
  <div class="panel-heading" id="label_head_panel" style="color:#B66F03; font-size:18px;">Selling Unit</div>                 
     <?php  echo form_open_multipart('maserati_stock_sales/c_slssell_unit_mas/c_crud_sell_mas/multiple_submit',array('class'=>'form-multi'));  ?> 
  
		<!-- Responsive Form for Mobile and Web Start -->
		<div class="container">

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label">Company</label>

						<input type="text" id="txtcom" name="txtcom" class="form-control" readonly
							value="<?php echo $this->session->userdata('company') ;?>" />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label">Transaction No</label>

						<input type="text" id="txtnotrans" name="txtnotrans" class="form-control" readonly
							value="<?php echo $transnumber ;?>" />
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label">Branch</label>

						<input type="text" id="txtbranch" name="txtbranch" class="form-control" readonly
							value="<?php echo $this->session->userdata('name_branch') ;?>" />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label">User Created</label>

						<input type="text" id="txtusersell" name="txtusersell" class="form-control " readonly
							value="<?php echo $this->session->userdata('name'); ?>">
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label">Engine</label>

						<input type="text" id="txtengine" name="txtengine" class="form-control" readonly />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#A6090D">VIN / Engine *</label>

						<!-- <input type="text" id="txtvinno" name="txtvinno" class="form-control" /> -->

						<select name="txtvinno" class="select2 form-control" id="txtvinno">
							
							<option value="">- VIN / Engine * - </option>

							<?php foreach($tampil_vin as $vin) { ?>
								<option value="<?=$vin->vin;?>"> <?=$vin->vin;?> </option>
							<?php } ?>

							</select>

						<input name="txtnostock" type="text" id="txtnostock" size="30" hidden="true" />
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label">Model Unit</label>

						<input type="text" id="txtengine" name="txtengine" class="form-control" readonly />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#A6090D">SPK No *</label>

						<div class="input-group">
							<span class="input-group-addon">
								<label id="txt_curr"> <?php echo $this->session->userdata('branch_short') ?> </label>
							</span>

							<input type="text" id="txtspk" name="txtspk" class="form-control col-xs-2 col-xs-offset-0" />

							<input type="hidden" id="txtbrpk" name="txtbrpk" class="form-control"
								value="<?php echo $this->session->userdata('branch_short') ?>" readonly="readonly" />


						</div>
					</div>

				</div>
			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label">Type Unit</label>

						<input type="text" id="txttype" name="txttype" class="form-control" readonly />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#A6090D">Date Alocation</label>

						<input type="text" id="txtdatealoc" name="txtdatealoc" class="form-control" readonly
							value="<?php echo date('d-m-Y') ?>">
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label">Colour</label>

						<input type="text" id="txtwarna" name="txtwarna" class="form-control" readonly />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label">WR Date</label>

						<input type="text" id="txtwrsdate" name="txtwrsdate" class="form-control" readonly="readonly">
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label">Location Stock</label>

						<input type="text" id="txtlokasi" name="txtlokasi" class="form-control" readonly="readonly" />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#A6090D">Customer *</label>

						<!-- <input name="txtcust" type="text" class="form-control" id="txtcust" placeholder="Customer"
							maxlength="40" /> -->

							<select name="txtcust" class="select2 form-control" id="txtcust">
							
							<option value="">- Customer - </option>

							<?php foreach($tampil_customer as $cust) { ?>
								<option value="<?=$cust->cust_name;?>"> <?=$cust->cust_name;?> </option>
							<?php } ?>

							</select>

						<input name="txtidcust" type="text" id="txtidcust" size="20" hidden="true" />
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#A6090D">Lama SPK</label>

						<input name="txtlamaspk" type="text" class="form-control" 
			 			id="txtlamaspk" value="7" maxlength="4"  readonly="readonly"/>

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#A6090D">Sales*</label>

						<select id="cbosales" name="cbosales"  class="select2 form-control">
						<option value="">--Choose--</option>
							<?php foreach($tampil_sales as $row_sal){ ?>               
								<option value="<?php echo $row_sal->id_sales ;?>"><?php echo $row_sal->sal_name ;?></option>
							<?php } ?>  
						</select>
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#A6090D">Payment Type : *</label>

						<select id="cbopaystatus" name="cbopaystatus"  class="form-control">
							<option value="">--Choose--</option>
						<?php foreach($tampil_payment_type as $row_pay){ ?>               
								<option value="<?php echo $row_pay->flag_payment_status ;?>"><?php echo $row_pay->desc_pay ;?></option>
							<?php } ?>           
						</select>

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#900" >Currency</label>

						<input  maxlength="20" id="curr" name="curr" class="form-control" readonly  />
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#007E0B">Poles-Optional</label>

						<input  maxlength="70" id="txtpoles" name="txtpoles" class="poles form-control"  
						style="color:#A6090D" onkeypress="return isNumberKey(event)"  
						placeholder="0.00" disabled="disabled" />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#900" >Price  Off Road</label>

						<input  maxlength="70" id="price_offroad" name="price_offroad" 
		   				class="form-control price_offroad" readonly  style="color:#A6090D"  placeholder="0.00" /> 
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#007E0B">PDI-Optional</label>

						<input  maxlength="70" id="txtpdi" name="txtpdi" class="pdi form-control"  
						style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" 
						disabled="disabled" />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#010096">Discount</label>

						<input  maxlength="70" id="txtdixc" name="txtdixc" class="discount form-control"  
						style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" 
						disabled="disabled" />
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#007E0B">Surat Jalan-Optional</label>

						<input  maxlength="70" id="txtsj" name="txtsj" class="srtj form-control"  
						style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" 
						disabled="disabled" />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#010096">PPH 22</label>

						<input  maxlength="70" id="txtpph" name="txtpph" class="pph form-control"  
						style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" 
						disabled="disabled" />
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#007E0B">Kaca Film-Optional</label>

						<input  maxlength="70" id="txtkafilm" name="txtkafilm" class="kafilm form-control"  
						style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" 
						disabled="disabled" />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#900">BBN</label>

						<input  maxlength="70" id="txtbbn" name="txtbbn" class="bbn form-control" 
						style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" 
						disabled="disabled" />
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#007E0B">Others-Optional</label>

						<input  maxlength="70" id="txtother" name="txtother" class="others form-control"  
						style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00"
						disabled="disabled" />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#900">Selling Price / On Road</label>

						<input  maxlength="70" id="txtonroad" name="txtonroad" class="onroad form-control"  
		 				style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" readonly/>
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#007E0B">Incentive Sales</label>

						<input  maxlength="70" id="txtinsetif" name="txtinsetif" class="insetif form-control"  
						style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" 
						disabled="disabled" />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#A6090D">DPP</label>

						<input  maxlength="70" id="txtdpp" name="txtdpp" class="dpp form-control"  
		 				style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" readonly />
					</div>
				</div>

			</div>
			<!-- Row End -->

			<!-- Row Start -->
			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#010096">Suport B.I</label>

						<input  maxlength="70" id="txtsuport" name="txtsuport" class="suport form-control"  
						style="color:#A6090D" onkeypress="return isNumberKey(event)"  placeholder="0.00" 
						disabled="disabled" />

					</div>
				</div>

				<div class="col-sm-1">


				</div>

				<div class="col-sm-5">
					<div class="form-group">
						<label class="control-label" style="color:#A6090D">Remarks *</label>

						<textarea name="txtnote" class="form-control" id="txtnote"></textarea>
					</div>
				</div>

			</div>
			<!-- Row End -->

		</div>
		<!-- Responsive Form for Mobile and Web End -->
		
<div class="panel panel-default ">
	<div class="panel-heading" id="label_head_panel">

		<!-- Row Start -->
		<div class="row">
			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label" style="color:#010096"> Gross Profit (GP) :</label>

					<input id="txtgp" name="txtgp" class="r_grossprofit" style="color:#A6090D;font-weight:bold;"
						placeholder="0.00" />

				</div>
			</div>

			<div class="col-sm-1">


			</div>

			<div class="col-sm-5">
				<div class="form-group">
					<label class="control-label" style="color:#A6090D">Remaining Payment : </label>

					<input maxlength="70" id="txt_remain" name="txt_remain" class="r_payment" readonly
						style="color:#A6090D;font-weight:bold;" placeholder="0.00" />
				</div>
			</div>

		</div>
		<!-- Row End -->

	</div>

	<div class="panel-body">
		<!-- Table Responsive -->
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>No</th>
						<th style="color:#A6090D;">Payment Description *</th>
						<th style="color:#A6090D;">Date</th>
						<th style="color:#A6090D;">DP Or Payment Customer *</th>
						<th style="color:#A6090D;">Upload Transfer PDF*</th>
					</tr>
				</thead>
				<tbody class="detail">
					<tr>
						<td class="no" align="center">1</td>
						<td>

							<input type="text" maxlength="70" id="txtdescpay" name="txtdescpay"
								placeholder="Description" size="24" class="form-control txtdescpay"
								disabled="disabled" />
						</td>
						<td>
							<input type="text" maxlength="70" name="txtdatedet" placeholder="Date Payment" size="30"
								class="form-control datepicker" data-date-format="dd-mm-yyyy" readonly
								disabled="disabled" />
						</td>

						<td>
							<input maxlength="14" name="txtdppay" type="text" placeholder="0.00" size="20"
								onkeypress="return isNumberKey(event)" class="form-control price" style="color:#A6090D"
								disabled="disabled" id="txtdatedet" />
						</td>
						<td>
							<input type="file" id="txtupload" name="userfile" class="btn btn-success uploadfoto"
								disabled="disabled" />
						</td>
					</tr>


				</tbody>
			</table>
		</div>
		<!-- Table Responsive -->

        </div>   
		
		<div class="row">
            <div class="form-group text-center">
                <button id="btnsave" name="btnsave" value="Save" class="btn btn-app btn-primary btn-xs radius-4" type="submit">
                    <i class="ace-icon fa fa-floppy-o bigger-160"></i>
                    Save
                </button>


                <a href="<?=base_url('maserati_stock_sales/c_slssell_unit_mas/c_slsssell_mas');?>" class="btn btn-app btn-danger btn-xs radius-4">
                    <i class="ace-icon fa fa-close bigger-160"></i>
                    Cancel
                </a>

            </div>
        </div>
		                                                	                     
      </div>                                                                                         				                         
 </div> 
 		
		<?php form_close(); ?> 
      
    </div> <!-- /panel -->             
</div>  <!-- /div class="col-lg-12 -->   
 
 <?php 
 if ($this->session->flashdata('pesan_succces') !="") {	 
	 echo '<script>alert("'.$this->session->flashdata('pesan_succces').'");</script>' ;	
 }
?>    
<script type="text/javascript">
 $('.datepicker').datetimepicker({
        language:  'id',
        weekStart: 1,
        todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
    });
</script> 


<script type="text/javascript">
//--------------function number only
 
$('.select2').select2();

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 44 && charCode != 45 && charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		} else {
			return true;
		}      
}	
//--------------function back only 
 
function isbackspacerKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
		if ( charCode > 31) {
			return false;
		} else {
			return true;
		}      
}	


//----------------set focus----
 $(function() {
  $("#txtpayfrom").focus();    
});	

</script>


<script script type="text/javascript">

//calculate and add field--------------
$(function(){	

//calculate Gross Profit (GP)-------------------------------     
    
     $('body').delegate('.poles','change',function(){  		
	      	
		   var price_offroad= numeral().unformat($('.price_offroad').val()); 	
		   var discount=numeral().unformat($('.discount').val());
		   var poles=numeral().unformat($('.poles').val());
		   var pdi = numeral().unformat($('.pdi').val());
		   var srtj=numeral().unformat($('.srtj').val());
		   var kafilm=numeral().unformat($('.kafilm').val());
		   var others=numeral().unformat($('.others').val());
		   
		   var insetif= numeral().unformat($('.insetif').val());
		   var suport=numeral().unformat($('.suport').val());
		   var pph=numeral().unformat($('.pph').val());
		  	
		   var dpp=numeral().unformat($('.dpp').val());
		   var onroad= numeral().unformat($('.onroad').val());
		   var str_ppn = 1.1;
				  		
  //GP textbox----------------------------------------
  if (pph == "0" || '') {
	 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
	// var hasil_gp =(onroad / str_ppn)-dpp-total_opt-insetif+suport;
	 var hasil_gp =(price_offroad - discount / str_ppn)-dpp-total_opt-insetif+suport;
	 var pembulatan_gp = Math.round(hasil_gp);
	 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
  }else{	 
	 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
	// var hasil_gp =((onroad - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
	 var hasil_gp =((price_offroad-discount - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
	 var pembulatan_gp = Math.round(hasil_gp);
	 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
  }
  //end ---------------------------------------
		  
 	 });
	 
	$('body').delegate('.poles','blur',function(){  		   
		   var poles=$('.poles').val();
		   var r_grossprofit=$('.r_grossprofit').val();
		   
		   $('.poles').val(numeral(poles).format('0,0.00')) ;
		   $('.r_grossprofit').val(numeral(r_grossprofit).format('0,0.00'));		
 	 });  	
	 
	  $('body').delegate('.pdi','change',function(){  		
	       var price_offroad= numeral().unformat($('.price_offroad').val()); 	
		   var discount=numeral().unformat($('.discount').val());
		   var poles=numeral().unformat($('.poles').val());
		   var pdi = numeral().unformat($('.pdi').val());
		   var srtj=numeral().unformat($('.srtj').val());
		   var kafilm=numeral().unformat($('.kafilm').val());
		   var others=numeral().unformat($('.others').val());
		   
		   var insetif= numeral().unformat($('.insetif').val());
		   var suport=numeral().unformat($('.suport').val());
		   var pph=numeral().unformat($('.pph').val());
		  	
		   var dpp=numeral().unformat($('.dpp').val());
		   var onroad= numeral().unformat($('.onroad').val());
		   var str_ppn = 1.1;
				  		
  //GP textbox----------------------------------------
  if (pph == "0" || '') {
	 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
	// var hasil_gp =(onroad / str_ppn)-dpp-total_opt-insetif+suport;
	 var hasil_gp =((price_offroad-discount)/ str_ppn)-dpp-total_opt-insetif+suport;
	 var pembulatan_gp = Math.round(hasil_gp);
	 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
  }else{	 
	 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
	 //var hasil_gp =((onroad - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
	 var hasil_gp =((price_offroad-discount - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
	 var pembulatan_gp = Math.round(hasil_gp);
	 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
  }
  //end ---------------------------------------
		  
 	 });
	 
	$('body').delegate('.pdi','blur',function(){  		   
		   var pdi=$('.pdi').val();
		   var r_grossprofit=$('.r_grossprofit').val();
		   
		   $('.pdi').val(numeral(pdi).format('0,0.00')) ;
		   $('.r_grossprofit').val(numeral(r_grossprofit).format('0,0.00'));
		  	
 	 });  	
	 
	  $('body').delegate('.srtj','change',function(){  		
	       var price_offroad= numeral().unformat($('.price_offroad').val()); 
		   var discount=numeral().unformat($('.discount').val());
		   var poles=numeral().unformat($('.poles').val());
		   var pdi = numeral().unformat($('.pdi').val());
		   var srtj=numeral().unformat($('.srtj').val());
		   var kafilm=numeral().unformat($('.kafilm').val());
		   var others=numeral().unformat($('.others').val());
		   
		   var insetif= numeral().unformat($('.insetif').val());
		   var suport=numeral().unformat($('.suport').val());
		   var pph=numeral().unformat($('.pph').val());
		  	
		   var dpp=numeral().unformat($('.dpp').val());
		   var onroad= numeral().unformat($('.onroad').val());
		   var str_ppn = 1.1;
				  		
		  //GP textbox----------------------------------------
		  if (pph == "0" || '') {
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			 //var hasil_gp =(onroad / str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }else{	 
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			// var hasil_gp =((onroad - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }
		  //end ---------------------------------------
		  
 	 });
	 
	$('body').delegate('.srtj','blur',function(){  		   
		   var srtj=$('.srtj').val();
		   var r_grossprofit=$('.r_grossprofit').val();
		   
		   $('.srtj').val(numeral(srtj).format('0,0.00')) ;
		   $('.r_grossprofit').val(numeral(r_grossprofit).format('0,0.00'));
		  		
 	 });  	
	 
	  $('body').delegate('.kafilm','change',function(){  		
	       var price_offroad= numeral().unformat($('.price_offroad').val());  
		    var discount=numeral().unformat($('.discount').val());
		   var poles=numeral().unformat($('.poles').val());
		   var pdi = numeral().unformat($('.pdi').val());
		   var srtj=numeral().unformat($('.srtj').val());
		   var kafilm=numeral().unformat($('.kafilm').val());
		   var others=numeral().unformat($('.others').val());
		   
		   var insetif= numeral().unformat($('.insetif').val());
		   var suport=numeral().unformat($('.suport').val());
		   var pph=numeral().unformat($('.pph').val());
		  	
		   var dpp=numeral().unformat($('.dpp').val());
		   var onroad= numeral().unformat($('.onroad').val());
		   var str_ppn = 1.1;
				  		
		  //GP textbox----------------------------------------
		  if (pph == "0" || '') {
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			 //var hasil_gp =(onroad / str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }else{	 
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			// var hasil_gp =((onroad - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }
		  //end ---------------------------------------
		  
 	 });
	 
	$('body').delegate('.kafilm','blur',function(){  		   
		   var kafilm=$('.kafilm').val();
		   var r_grossprofit=$('.r_grossprofit').val();
		
		   $('.kafilm').val(numeral(kafilm).format('0,0.00')) ;
		   $('.r_grossprofit').val(numeral(r_grossprofit).format('0,0.00'));
			
 	 });  	
	  
     $('body').delegate('.others','change',function(){  		
	       var price_offroad= numeral().unformat($('.price_offroad').val());   
		   var discount=numeral().unformat($('.discount').val());
		   var poles=numeral().unformat($('.poles').val());
		   var pdi = numeral().unformat($('.pdi').val());
		   var srtj=numeral().unformat($('.srtj').val());
		   var kafilm=numeral().unformat($('.kafilm').val());
		   var others=numeral().unformat($('.others').val());
		   
		   var insetif= numeral().unformat($('.insetif').val());
		   var suport=numeral().unformat($('.suport').val());
		   var pph=numeral().unformat($('.pph').val());
		  	
		   var dpp=numeral().unformat($('.dpp').val());
		   var onroad= numeral().unformat($('.onroad').val());
		   var str_ppn = 1.1;
				  		
		  //GP textbox----------------------------------------
		  if (pph == "0" || '') {
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			 //var hasil_gp =(onroad / str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }else{	 
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			// var hasil_gp =((onroad - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }
		  //end ---------------------------------------
		  
 	 });
	 
	$('body').delegate('.others','blur',function(){  		   
		   var others=$('.others').val();
		   var r_grossprofit=$('.r_grossprofit').val();
		  
		   
		   $('.others').val(numeral(others).format('0,0.00')) ;
		   $('.r_grossprofit').val(numeral(r_grossprofit).format('0,0.00'));
		 		
 	 });  	  
	 
	  $('body').delegate('.insetif','change',function(){  		
	       var price_offroad= numeral().unformat($('.price_offroad').val());    
		   var discount=numeral().unformat($('.discount').val());
		   var poles=numeral().unformat($('.poles').val());
		   var pdi = numeral().unformat($('.pdi').val());
		   var srtj=numeral().unformat($('.srtj').val());
		   var kafilm=numeral().unformat($('.kafilm').val());
		   var others=numeral().unformat($('.others').val());
		   
		   var insetif= numeral().unformat($('.insetif').val());
		   var suport=numeral().unformat($('.suport').val());
		   var pph=numeral().unformat($('.pph').val());
		  	
		   var dpp=numeral().unformat($('.dpp').val());
		   var onroad= numeral().unformat($('.onroad').val());
		   var str_ppn = 1.1;
				  		
		  //GP textbox----------------------------------------
		  if (pph == "0" || '') {
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			 //var hasil_gp =(onroad / str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }else{	 
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			// var hasil_gp =((onroad - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }
		  //end ---------------------------------------
		  
 	 });
	 
	$('body').delegate('.insetif','blur',function(){  		   
		   var insetif=$('.insetif').val();
		   var r_grossprofit=$('.r_grossprofit').val();
		 
		   
		   $('.insetif').val(numeral(insetif).format('0,0.00')) ;
		   $('.r_grossprofit').val(numeral(r_grossprofit).format('0,0.00'));
		   
 	 });  	  
	 
	  $('body').delegate('.suport','change',function(){  		
	       var price_offroad= numeral().unformat($('.price_offroad').val());   
		   var discount=numeral().unformat($('.discount').val());
		   var poles=numeral().unformat($('.poles').val());
		   var pdi = numeral().unformat($('.pdi').val());
		   var srtj=numeral().unformat($('.srtj').val());
		   var kafilm=numeral().unformat($('.kafilm').val());
		   var others=numeral().unformat($('.others').val());
		   
		   var insetif= numeral().unformat($('.insetif').val());
		   var suport=numeral().unformat($('.suport').val());
		   var pph=numeral().unformat($('.pph').val());
		  	
		   var dpp=numeral().unformat($('.dpp').val());
		   var onroad= numeral().unformat($('.onroad').val());
		   var str_ppn = 1.1;
				  		
		  //GP textbox----------------------------------------
		  if (pph == "0" || '') {
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			// var hasil_gp =(onroad / str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }else{	 
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			// var hasil_gp =((onroad - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }
		  //end ---------------------------------------
		
		  
 	 });
	 
	$('body').delegate('.suport','blur',function(){  		   
		   var suport=$('.suport').val();
		   var r_grossprofit=$('.r_grossprofit').val();
		
		   $('.suport').val(numeral(suport).format('0,0.00')) ;
		   $('.r_grossprofit').val(numeral(r_grossprofit).format('0,0.00'));
				
 	 });  	
	 
	  $('body').delegate('.pph','change',function(){  		
	       var price_offroad= numeral().unformat($('.price_offroad').val());  
		   var discount=numeral().unformat($('.discount').val());		   
		   var poles=numeral().unformat($('.poles').val());
		   var pdi = numeral().unformat($('.pdi').val());
		   var srtj=numeral().unformat($('.srtj').val());
		   var kafilm=numeral().unformat($('.kafilm').val());
		   var others=numeral().unformat($('.others').val());
		   
		   var insetif= numeral().unformat($('.insetif').val());
		   var suport=numeral().unformat($('.suport').val());
		   var pph=numeral().unformat($('.pph').val());
		  	
		   var dpp=numeral().unformat($('.dpp').val());
		   var onroad= numeral().unformat($('.onroad').val());
		   var str_ppn = 1.1;
				  		
		  //GP textbox----------------------------------------
		  if (pph == "0" || '') {
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			// var hasil_gp =(onroad / str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }else{	 
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			// var hasil_gp =((onroad - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }
		  //end ---------------------------------------
		
		  
 	 });
	 
	$('body').delegate('.pph','blur',function(){  		   
		   var pph=$('.pph').val();
		   var r_grossprofit=$('.r_grossprofit').val();
		
		   $('.pph').val(numeral(pph).format('0,0.00')) ;
		   $('.r_grossprofit').val(numeral(r_grossprofit).format('0,0.00'));
				
 	 });  	
	 
	 
	
	//calculate remain pay customer-------------------------------
	

	 $('body').delegate('.discount','change',function(){ 
	       var price_offroad= numeral().unformat($('.price_offroad').val()); 	
		   var bbn= numeral().unformat($('.bbn').val());
		   var discount=numeral().unformat($('.discount').val());
		   var price = numeral().unformat($('.price').val()); //dp		
		 
		   var hasil_r_payment=(price_offroad+bbn)-discount-price;		
		   var hasil_selling_onroad = (price_offroad + bbn) - discount;
		   
		   $('.onroad').val(hasil_selling_onroad);
		   $('.r_payment').val(hasil_r_payment);
		//gp----------------------------------------------------   
		   var poles=numeral().unformat($('.poles').val());
		   var pdi = numeral().unformat($('.pdi').val());
		   var srtj=numeral().unformat($('.srtj').val());
		   var kafilm=numeral().unformat($('.kafilm').val());
		   var others=numeral().unformat($('.others').val());
		   
		   var insetif= numeral().unformat($('.insetif').val());
		   var suport=numeral().unformat($('.suport').val());
		   var pph=numeral().unformat($('.pph').val());
		  	
		   var dpp=numeral().unformat($('.dpp').val());
		   var onroad_gp= numeral().unformat($('.onroad').val());
		   var str_ppn = 1.1;
				  		
		  if (pph == "0" || '') {
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			// var hasil_gp =(onroad_gp / str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }else{	 
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			 //var hasil_gp =((onroad_gp - pph)/str_ppn)-dpp-total_opt-insetif+suport;
			 var  hasil_gp =((price_offroad-discount - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }
		  //end ---------------------------------------
		  
 	 });
	 
	$('body').delegate('.discount','blur',function(){  		   
		   var discount=$('.discount').val();
		   var selling_onroad=$('.onroad').val();
		   var r_payment=$('.r_payment').val();
		
	       $('.onroad').val(numeral(selling_onroad).format('0,0.00')) ;	
		   $('.discount').val(numeral(discount).format('0,0.00')) ;
		   $('.r_payment').val(numeral(r_payment).format('0,0.00'));
			
 	 });  	
	 
	 $('body').delegate('.price','change',function(){  			 
		   var price_offroad= numeral().unformat($('.price_offroad').val()); 	
		   var bbn= numeral().unformat($('.bbn').val());
		   var discount=numeral().unformat($('.discount').val());
		   var price = numeral().unformat($('.price').val());//dp		
		 
		   var hasil_r_payment=(price_offroad+bbn)-discount-price;		
		   
		   $('.r_payment').val(hasil_r_payment);
		
 	 });
	 
	 	$('body').delegate('.price','blur',function(){  		 
		   var price=$('.price').val();
		   var r_payment=$('.r_payment').val();
		 	   
		   $('.price').val(numeral(price).format('0,0.00')) ;		  	  
		   $('.r_payment').val(numeral(r_payment).format('0,0.00')) ;
				  		 		 		 
 	 });  	
	 
	 $('body').delegate('.bbn','change',function(){  		
	       var price_offroad= numeral().unformat($('.price_offroad').val()); 
           var discount=numeral().unformat($('.discount').val());		   
		   var onroad = numeral().unformat($('.onroad').val()); 
		   var bbn= numeral().unformat($('.bbn').val());
		   var discount=numeral().unformat($('.discount').val());
		   var price = numeral().unformat($('.price').val()); //dp		f
			
		   var hasil_selling_onroad = (price_offroad+bbn)-discount; ;
		   var hasil_r_payment=(price_offroad+bbn)-discount-price; //remain 		
		   
		   $('.onroad').val(hasil_selling_onroad);
		   $('.r_payment').val(hasil_r_payment);
		   
		  //gp--------------------------------------------------------
		   var poles=numeral().unformat($('.poles').val());
		   var pdi = numeral().unformat($('.pdi').val());
		   var srtj=numeral().unformat($('.srtj').val());
		   var kafilm=numeral().unformat($('.kafilm').val());
		   var others=numeral().unformat($('.others').val());
		   
		   var insetif= numeral().unformat($('.insetif').val());
		   var suport=numeral().unformat($('.suport').val());
		   var pph=numeral().unformat($('.pph').val());
		  	
		   var dpp=numeral().unformat($('.dpp').val());
		   var onroad_gp= numeral().unformat($('.onroad').val());
		   var str_ppn = 1.1;
				  		
		  
		  if (pph == "0" || '') {
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			// var hasil_gp =(onroad_gp / str_ppn)-dpp-total_opt-insetif+suport;
			 var hasil_gp =((price_offroad-discount)/ str_ppn)-dpp-total_opt-insetif+suport;
			 //var hasil_gp =(onroad_gp / str_ppn);
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }else{	 
			 var total_opt=(poles+pdi+srtj+kafilm+others) ;		  
			 var hasil_gp =((price_offroad-discount - pph)/ str_ppn)-dpp-total_opt-insetif+suport;
			 var pembulatan_gp = Math.round(hasil_gp);
			 $('.r_grossprofit').val(numeral(pembulatan_gp).format('0,0.00'));
		  }
		  //end ---------------------------------------
 	 });	
	 
	 $('body').delegate('.bbn','blur',function(){  		   
		   var bbn=$('.bbn').val();
		   var selling_onroad = $('.onroad').val();
		   var r_payment=$('.r_payment').val();
		   
		   $('.onroad').val(numeral(selling_onroad).format('0,0.00')) ;	
		   $('.bbn').val(numeral(bbn).format('0,0.00')) ;
		   $('.r_payment').val(numeral(r_payment).format('0,0.00'));	
 	 });  	
	
	 	  	
});


</script>

<script type="text/javascript">
//---------function autocomplite jquery
$(document).ready(function () {
		$(function () {
			$("#txtvinno").autocomplete({
				minLength:0,
                delay:0,
				source: function(request, response) {
					
					var str_url = '<?php echo site_url('maserati_stock_sales/c_slssell_unit_mas/c_crud_sell_mas/suggest_vin_engine'); ?>';					
					var str_novin =$("#txtvinno").val();										  					
					$.ajax({ 
						url: str_url ,												
						data: {txtvinno:str_novin},																																		
						dataType: "json",
						type: "POST",																	
						success: function(data){response(data);																																										
						}						
					});
				},	
				    select:function(event, ui){
					   $('#txtnostock').val(ui.item.txtnostock);
					   $('#txtengine').val(ui.item.txtengine);
					   $('#txtmodel').val(ui.item.txtmodel);
					   $('#txttype').val(ui.item.txttype);
					   $('#txtwarna').val(ui.item.txtwarna);
					   $('#txtlokasi').val(ui.item.txtlokasi);
					   $('#curr').val(ui.item.curr);					 
					   
					   if(ui.item.temp_date_wrs != '01-01-1970') {
					     $('#txtwrsdate').val(ui.item.temp_date_wrs);
					   }else{
						 $('#txtwrsdate').val('');
					   }
					   
					   $('#txtpoles').val('0.00');
					   $('#txtpdi').val('0.00');
					   $('#txtsj').val('0.00');
					   $('#txtkafilm').val('0.00');
					   $('#txtinsetif').val('0.00');
					   $('#txtother').val('0.00');
					   $('#txtsuport').val('0.00');
					   $('#txtdixc').val('0.00');
					   $('#txtbbn').val('0.00');
					   $('#txtpph').val('0.00');
					   $('#txtdpp').val(ui.item.dpp);
					   $('#price_offroad').val(ui.item.price_offroad);
					   $('#txtonroad').val(ui.item.price_offroad);					
					   $('#txt_remain').val(ui.item.result);
					   
					   //GP textbox----------------------------------------
						var str_seling_on = numeral().unformat(ui.item.price_offroad);
						var str_ppn = 1.1;
						var strdpp = numeral().unformat(ui.item.dpp);
						var hasil_gp =(str_seling_on / str_ppn)-strdpp   ;
						var pembulatan_gp = Math.round(hasil_gp);
						$('#txtgp').val(numeral(pembulatan_gp).format('0,0.00'));
					   //end ---------------------------------------
					   
					   
					   //enabled detail
					   $('.txtdescpay').removeAttr('disabled');
					   $('.datepicker').removeAttr('disabled');
					   $('.price').removeAttr('disabled');
					   $('.bbn').removeAttr('disabled');				
					   $('.poles').removeAttr('disabled');	
					   $('.pdi').removeAttr('disabled');	
					   $('.srtj').removeAttr('disabled');	
					   $('.kafilm').removeAttr('disabled');	
					   $('.others').removeAttr('disabled');	
					   $('.discount').removeAttr('disabled');			   				
					   $('.suport').removeAttr('disabled');
					   $('.pph').removeAttr('disabled');	
					   $('.insetif').removeAttr('disabled');	
					   $('.uploadfoto').removeAttr('disabled');
					}						                  				                     
			});		
				
		});
	
	});	  
</script>    

<script type="text/javascript">
//automatic show price 

var htmlobjek;
$(document).ready(function(){  
	 $("#txtvinno").change(function(){
			var txtvinno_full = $("#txtvinno").val();
			var url = '<?php echo site_url('maserati_stock_sales/c_slssell_unit_mas/c_crud_sell_mas/get_vin_engine_full'); ?>';
			
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{txtvinno:txtvinno_full},	
				cache: false,          
				success: function(data, textStatus, jqXHR){           
				    $('[name="txtnostock"]').val(data.txtnostock);	
				 	$('[name="txtvinno"]').val(data.txtvin);			
					$('[name="txtengine"]').val(data.txtengine);
					$('[name="txtmodel"]').val(data.txtmodel);
					$('[name="txttype"]').val(data.txttype);
				    $('[name="txtwarna"]').val(data.txtwarna);
				    $('[name="txtlokasi"]').val(data.txtlokasi);
				    $('[name="curr"]').val(data.curr);
					
					if (data.temp_date_wrs != '01-01-1970') {
						$('[name="txtwrsdate"]').val(data.temp_date_wrs);
					}else{	
					    $('[name="txtwrsdate"]').val('');						
					} 
					
					$('[name="txtpoles"]').val('0.00'); 
					$('[name="txtpdi"]').val('0.00');
					$('[name="txtsj"]').val('0.00');
					$('[name="txtkafilm"]').val('0.00');
					$('[name="txtinsetif"]').val('0.00');
					$('[name="txtother"]').val('0.00');
					$('[name="txtsuport"]').val('0.00');
					$('[name="txtdixc"]').val('0.00');
					$('[name="txtbbn"]').val('0.00');
					$('[name="txtpph"]').val('0.00');
				    $('[name="txtdpp"]').val(data.dpp);
					$('[name="price_offroad"]').val(data.price_offroad);
					$('[name="txtonroad"]').val(data.price_offroad);					
					$('[name="txt_remain"]').val(data.result);
					
					//GP(gross profit) textbox----------------------------------------
					var str_seling_on = numeral().unformat(data.price_offroad);
					var str_ppn = "1.1";
					var strdpp = numeral().unformat(data.dpp);
					var hasil_gp =(str_seling_on/str_ppn)-strdpp;
					var pembulatan_gp = Math.round(hasil_gp);
					$('#txtgp').val(numeral(pembulatan_gp).format('0,0.00'));
					//end ---------------------------------------
					
					//enabled detail
				    $('.txtdescpay').removeAttr('disabled');
					$('.datepicker').removeAttr('disabled');
				    $('.price').removeAttr('disabled');
				    $('.bbn').removeAttr('disabled');				
					$('.poles').removeAttr('disabled');	
					$('.pdi').removeAttr('disabled');	
					$('.srtj').removeAttr('disabled');	
					$('.kafilm').removeAttr('disabled');	
					$('.others').removeAttr('disabled');	
					$('.discount').removeAttr('disabled');
					$('.pph').removeAttr('disabled');	
					$('.insetif').removeAttr('disabled');			   				
					$('.suport').removeAttr('disabled');	
					$('.uploadfoto').removeAttr('disabled');
								
				}
			});
		  });
});	  
</script>


<script type="text/javascript">
//---------function autocomplite jquery
$(document).ready(function () {
		$(function () {
			$("#txtcust").autocomplete({
				minLength:0,
                delay:0,
				source: function(request, response) {
					
					var str_url = '<?php echo site_url('maserati_stock_sales/c_slssell_unit_mas/c_crud_sell_mas/suggest_customer'); ?>';					
					var str_customer =$("#txtcust").val();
															
					$.ajax({ 
						url: str_url ,												
						data: {txtcust:str_customer},																																		
						dataType: "json",
						type: "POST",																	
						success: function(data){response(data);																																										
						}						
					});
				},	
				    select:function(event, ui){					  
					   $('#txtidcust').val(ui.item.txtidcust);					  
					}						                  				                     
			});		
				
		});
	
	});	  
</script>    

<script type="text/javascript">
//automatic show price 

var htmlobjek;
$(document).ready(function(){  
	 $("#txtcust").change(function(){
			var txtcust_full = $("#txtcust").val();
			var url = '<?php echo site_url('maserati_stock_sales/c_slssell_unit_mas/c_crud_sell_mas/get_customer_full'); ?>';
			
			$.ajax({
				type:'POST',
				url: url,		
				dataType: "json",
				data:{txtcust:txtcust_full},	
				cache: false,          
				success: function(data, textStatus, jqXHR){           
				 	$('[name="txtcust"]').val(data.txtcust);			
					$('[name="txtidcust"]').val(data.txtidcust);													
				}
			});
		  });
});	  
</script>
