<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_jabatan extends MY_Controller {

	public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_jabatan');
			$this->load->model('login/M_login','',TRUE);
			$time = time();
			$sesiuser =  $this->M_jabatan->get_value('id',$this->session->userdata('id'),'tbl_user');
			$expiresession = $sesiuser[0]->waktu;
			$session_update = 7200;
			if (($expiresession+$session_update) < $time){
			$this->disconnect();
			}
	 	}
		
	public function index()
		{

		$data['golongan']=$this->M_jabatan->get_all_golongan();
		$data['department']=$this->M_jabatan->get_all_dept();
		$data['total']=$this->M_jabatan->get_count_id();
		$data['show_view'] = 'jabatan_form/V_jabatan';
		$this->load->view('dashboard/Template',$data);		
		}
		
	public function ajax_list()
	{
		$list = $this->M_jabatan->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $qv_jabatan) {
			$chk_idmaster ='<div align="center"><input id="checkjabatan" name="checkjabatan" type="checkbox" value='.$qv_jabatan->id_jabatan.' class="editRow ace" />
           <span class="lbl"></span> ';
			$no++;
			$row = array();
			$row[] = $chk_idmaster;
			$row[] = $no;
			$row[] = $qv_jabatan->id_jabatan;
			$row[] = $qv_jabatan->dept;
			$row[] = $qv_jabatan->name_gol;
			$row[] = $qv_jabatan->name_jabatan;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->M_jabatan->count_all(),
						"recordsFiltered" => $this->M_jabatan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
	function ambil_data(){


		/*Menagkap semua data yang dikirimkan oleh client*/

		/*Sebagai token yang yang dikrimkan oleh client, dan nantinya akan
		server kirimkan balik. Gunanya untuk memastikan bahwa user mengklik paging
		sesuai dengan urutan yang sebenarnya */
		$draw=$_REQUEST['draw'];

		/*Jumlah baris yang akan ditampilkan pada setiap page*/
		$length=$_REQUEST['length'];

		/*Offset yang akan digunakan untuk memberitahu database
		dari baris mana data yang harus ditampilkan untuk masing masing page
		*/
		$start=$_REQUEST['start'];

		/*Keyword yang diketikan oleh user pada field pencarian*/
		$search=$_REQUEST['search']["value"];


		/*Menghitung total desa didalam database*/
		$this->db->from('tbl_dept');
		$this->db->where('status = 1');
		$total=$this->db->count_all_results();

		/*Mempersiapkan array tempat kita akan menampung semua data
		yang nantinya akan server kirimkan ke client*/
		$output=array();

		/*Token yang dikrimkan client, akan dikirim balik ke client*/
		$output['draw']=$draw;

		/*
		$output['recordsTotal'] adalah total data sebelum difilter
		$output['recordsFiltered'] adalah total data ketika difilter
		Biasanya kedua duanya bernilai sama, maka kita assignment 
		keduaduanya dengan nilai dari $total
		*/
		$output['recordsTotal']=$output['recordsFiltered']=$total;

		/*disini nantinya akan memuat data yang akan kita tampilkan 
		pada table client*/
		$output['data']=array();


		/*Jika $search mengandung nilai, berarti user sedang telah 
		memasukan keyword didalam filed pencarian*/
		if($search!=""){
		$this->db->like("id_dept",$search);
		$this->db->where('status = 1');
		$this->db->or_like("dept",$search);
		$this->db->where('status = 1');
		}


		/*Lanjutkan pencarian ke database*/
		//$this->db->limit($length,$start);
		//$this->db->where('status = 1');
		/*Urutkan dari alphabet paling terkahir*/
		//$this->db->order_by('id_dept','ASC');
		//$query=$this->db->get('tbl_dept');


		/*Ketika dalam mode pencarian, berarti kita harus
		'recordsTotal' dan 'recordsFiltered' sesuai dengan jumlah baris
		yang mengandung keyword tertentu
		*/
		if($search!=""){
		$this->db->like("id_dept",$search);
		$jum=$this->db->get('tbl_dept');
		$output['recordsTotal']=$output['recordsFiltered']=$jum->num_rows();
		}

		// Datatables Variables
        $order = $this->input->get("order");

        $col = 0;
        $dir = "";
        if(!empty($order)) {
            foreach($order as $o) {
                $col = $o['column'];
                $dir= $o['dir'];
            }
        }

        if($dir != "asc" && $dir != "desc") {
            $dir = "asc";
        }

        $columns_valid = array(
            "tbl_dept.id_dept", 
            "tbl_dept.dept"
        );

        if(!isset($columns_valid[$col])) {
            $order = null;
        } else {
            $order = $columns_valid[$col];
        }
		$this->db->where('status = 1');
        $query = $this->M_dept->get_data($start, $length, $order, $dir);
		
		foreach ($query->result_array() as $tbl_dept) {
			$chk_idmaster ='<div align="center"><input id="checkdept" name="checkdept" type="checkbox" value='.$tbl_dept["id_dept"].' class="editRow ace" req_id_del='.$tbl_dept["dept"].' />
           <span class="lbl"></span> ';
		   
			$output['data'][]=array($tbl_dept['id_dept'],$tbl_dept['dept'],
									$chk_idmaster);

		}

		echo json_encode($output);


	}
	
	public function add_jabatan()
		{
		if(empty($_POST["id_dept"])){
			die ("Field Department must be filled in!! ");
		}
		elseif(empty($_POST["id_gol"])){
			die ("Field Golongan must be filled in!! ");
		}
		elseif(empty($_POST["name_jabatan"])){
			die ("Field Name Jabatan must be filled in!! ");
		}
		else{
			$data = array(
					'id_jabatan' => $this->input->post('id_jabatan'),
					'id_dept' => $this->input->post('id_dept'),
					'id_gol' => $this->input->post('id_gol'),
					'name_jabatan' => $this->input->post('name_jabatan'),
					'status' => '1',
				);
			$insert = $this->M_jabatan->add_jabatan($data);
		  }
		  echo 'Insert';
		}
		
	public function ajax_edit($id)
		{
			$data = $this->M_jabatan->get_by_id($id);
			echo json_encode($data);
		}

	public function update_jabatan()
		{
		if(empty($_POST["id_dept"])){
			die ("Field Department must be filled in!! ");
		}
		elseif(empty($_POST["id_gol"])){
			die ("Field Golongan must be filled in!! ");
		}
		elseif(empty($_POST["name_jabatan"])){
			die ("Field Name Jabatan must be filled in!! ");
		}
		else{
		$data = array(
					'id_dept' => $this->input->post('id_dept'),
					'id_gol' => $this->input->post('id_gol'),
					'name_jabatan' => $this->input->post('name_jabatan'),
			);
		$this->M_jabatan->update_jabatan(array('id_jabatan' => $this->input->post('id_jabatan')), $data);
		 }
		 echo 'Insert';
	}
	
	public function deletedata (){
		$data_ids = $_REQUEST['ID'];
		$data_id_array = explode(",", $data_ids); 
		if(!empty($data_id_array)) {
			foreach($data_id_array as $id) {
				$data = array(
						'status' => '0',
				);
				$this->db->where('id_jabatan', $id);
				$this->db->update('tbl_jabatan', $data);
			}
		}
	}
	
	public function disconnect()
	{
		$ddate = date('d F Y');	
		$strfudate= $ddate.", ".date('H:i:s');
		$location = $_SERVER['PHP_SELF'];		
		$strid_user =$this->session->userdata('id');

		$data=array('login_terakhir'=>$strfudate, 'waktu'=>'', 'location' => $location);	
		$this->db->where('id',$strid_user);
		$this->db->update('tbl_user',$data); // update login terakhir user
		
		$this->session->sess_destroy();
        $this->session->unset_userdata(array('username' => '', 'login' => FALSE));
		redirect(base_url().'login/login','refresh');    
	}


}
