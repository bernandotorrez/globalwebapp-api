﻿$(function () {
    var dtformat = {
        dateFormat: "dd-MM-yy"
    };
    $(".datepickerCompleted").datepicker(dtformat);
});

// Solution for Jquery inside updatepanel
function pageLoad(sender, args) 
{
    if (args.get_isPartialLoad()) {
        var dtformat = {
            dateFormat: "dd-M-yy"
        };
        $(".datepickerCompleted").datepicker(dtformat);
    }
}