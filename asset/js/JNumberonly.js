﻿$(document).ready(function () {
    $(".numbersOnly").keypress(function (event) {
        var charCode = (event.which) ? event.which : event.keyCode
        if ((charCode >= 48 && charCode <= 57)
                || charCode == 46
                || charCode == 44)
            return true;
        return false;
    });
});